<?php

require_once('cron_load.php');
require_once('application/config/database.php');

$link = mysql_connect($db['default']['hostname'], $db['default']['username'], $db['default']['password']);
mysql_select_db($db['default']['database'], $link);

$s = $_GET['s'];
$sql_select = "SELECT search_results FROM temp_search_data WHERE session_id = '".$s."' ";
$exe_select = mysql_query($sql_select) or die(mysql_error());
$data_select = mysql_fetch_array($exe_select, MYSQL_ASSOC);
$search_results = $data_select['search_results'];
if($search_results == "") $search_results = '0';

$sql_latlong = "SELECT b.lat, b.long, b.user_id, a.id, a.offer_title, a.offer_description, a.price, a.price_normally, b.profile_picture, b.business_name FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id WHERE b.lat != '' AND b.long != '' AND a.id IN (".$search_results." ) ";
$exe_latlong = mysql_query($sql_latlong) or die(mysql_error());
//$num_latlong = mysql_num_rows($exe_latlong);

$sql_latlong_center = "SELECT b.lat, b.long FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id WHERE b.lat != '' AND b.long != '' AND a.id IN (".$search_results." ) LIMIT 0, 1 ";
$exe_latlong_center = mysql_query($sql_latlong_center) or die(mysql_error());
$data_latlong_center = mysql_fetch_array($exe_latlong_center, MYSQL_ASSOC);

$array_latlongs = array();
$array_latlongs_business = array();
while($data_latlong = mysql_fetch_array($exe_latlong, MYSQL_ASSOC))
{
	if($data_latlong['lat'] != '' && $data_latlong['long'] != '')
	{
		$latlongstr = $data_latlong['lat']."~~~~".$data_latlong['long'];
		if(!in_array($latlongstr, $array_latlongs))
		{
			array_push($array_latlongs, $latlongstr);
			$arr_business['lat'] = $data_latlong['lat'];
			$arr_business['long'] = $data_latlong['long'];
			$arr_business['business_name'] = $data_latlong['business_name'];
			$arr_business['profile_picture'] = $data_latlong['profile_picture'];
			$arr_business['user_id'] = $data_latlong['user_id'];
			array_push($array_latlongs_business, $arr_business);
		}
	}
}
$num_latlong = sizeof($array_latlongs_business);
//print_r($array_latlongs_business);
//die;
?>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<title>Search Map</title>
<link href="<?php echo $config['base_url']; ?>public/css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
#map_wrapper {
    height: 400px;
}

#map_canvas {
    width: 100%;
    height: 100%;
}
</style>
</head>
<body style="background:none;">

<div id="map_wrapper">
    <div id="map_canvas" class="mapping"></div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script language="javascript">
jQuery(function($) {
    var script = document.createElement('script');
    script.src = "http://maps.googleapis.com/maps/api/js?sensor=false&callback=initialize";
    document.body.appendChild(script);
});

function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
		zoom: 12,
        center: new google.maps.LatLng(<?=$data_latlong_center['lat']?>, <?=$data_latlong_center['long']?>),
        mapTypeId: 'roadmap'
    };
                    
    // Display a map on the page
    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
    map.setTilt(45);
        
    // Multiple Markers
    var markers = [
        <?php 
		$i = 0;
		foreach($array_latlongs_business as $the_business)
		{
			if($the_business['lat'] != '' && $the_business['long'] != '')
			{
			$i++;
			?>
			['<?=$the_business["business_name"]?>', <?=$the_business['lat']?>,<?=$the_business['long']?>]<?php if($i<$num_latlong){?>,<?php } ?>
			<?php
			}
		}
		?>
    ];
                        
    // Info Window Content
    var infoWindowContent = [
		<?php 
		$j = 0;
		foreach($array_latlongs_business as $the_business)
		{
			$j++;
			?>
			['<div class="info_content">' +
			'<div id="content" class="google_map">'+
			'<div class="thumb"><img src="<?php echo $config['base_url']; ?><?=$the_business["profile_picture"]?>" alt="" width="112" height="110" /></div>'+
			'<div id="bodyContent">'+
			'<p><b><?=$the_business["business_name"]?></b></p>'+
			<?php
			$sql_latlong_k1 = "SELECT a.id, a.offer_title, a.offer_description, a.price, a.price_normally FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id WHERE b.lat = '".$the_business["lat"]."' AND b.long = '".$the_business["long"]."' AND a.id IN (".$search_results." ) AND user_id = '".$the_business["user_id"]."' ";

			$exe_latlong_1 = mysql_query($sql_latlong_k1) or die(mysql_error());

			while($data_latlong_1 = mysql_fetch_array($exe_latlong_1, MYSQL_ASSOC))
			{
				?>
				 '<h3 id="firstHeading" class="firstHeading" style="float:left;"><?=$data_latlong_1["offer_title"]?></h3>'+
				 '<div style="float:left;width:100%;"><p style="float:left; width:100%;">$<?=$data_latlong_1["price"]?> (Value $<?=$data_latlong_1["price_normally"]?>)</p>'+
				 '<div class="book" style="float:left; margin-top:5px;"><a target="_parent" href="<?php echo $config['base_url']; ?>booking/index/?oid=<?=base64_encode($data_latlong_1["id"])?>"><img src="<?php echo $config['base_url']; ?>public/images/book_btn_map.jpg" alt=""></a></div></div>'+
				 '<p style="float:left;  border-bottom:1px solid #868686; width:100%; ">&nbsp;</p>'+		
				<?php
			}
			?>
			'</div>'+
		  '</div>' +        '</div>']<?php if($j<$num_latlong){?>,<?php } ?>
			<?php
		}
		?>
    ];
        
    // Display multiple markers on a map
    var infoWindow = new google.maps.InfoWindow(), marker, i;
    
    // Loop through our array of markers & place each one on the map  
    for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent(infoWindowContent[i][0]);
                infoWindow.open(map, marker);
            }
        })(marker, i));

        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }

    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(14);
        google.maps.event.removeListener(boundsListener);
    });
    
}
</script>
</body>
</html>