$(document).ready(function() {

/*========FOOTER BOTTOM FIXED=========*/
	if ($(document).width() > 450) 
		{
			var screen_inner_height2=$(document).height();
			$(".merchant_main").css('min-height',screen_inner_height2-388+'px' );
			
			var screen_inner_height3=$(document).height();
			$(".inner_content_bg").css('min-height',screen_inner_height3-492+'px' );
			
			var screen_inner_height4=$(document).height();
			$(".customer_main").css('min-height',screen_inner_height4-388+'px' );
			
			var screen_inner_height5=$(document).height();
			$(".inner_content_bg3").css('min-height',screen_inner_height5-1387+'px' );
			
			var screen_inner_height6=$(document).height();
			$(".inner_content_bg4").css('min-height',screen_inner_height6-2253+'px' );
			
			var screen_inner_height7=$(document).height();
			$(".inner_content_bg5").css('min-height',screen_inner_height7-1740+'px' );
		}
	
	else
	
		{
			var screen_inner_height2_1=$(document).height();
			//alert(screen_inner_height2_1);
			$(".merchant_main").css('min-height',screen_inner_height2_1-571+'px' );
		}


	$('.merchant_mobile_menu').click(function(e) {		        
        $(".merchant_main_left").slideToggle();
        $(".customer_main_left").slideToggle();
		
    });
	
	$('.mobile_nav').click(function(e) {		        
        $(".nav").slideToggle();
    });
	
	
/*========FOOTER BOTTOM FIXED=========*/

	var kwick_height=$('.panel').height();
	//alert(kwick_height)
	
	var kwick_window_height=$(window).width();

	//alert(kwick_window_height)
		
	if ( kwick_window_height > 1600) {
		
		$(".kwicks").css('min-height',kwick_height+'px' );
	}
	else
	{
		
	}
	
	
	$('#up').click(function(e) {
        e.preventDefault();
        $('html, body').stop().animate({ scrollTop: 0 }, 1000, 'linear');
        // replace 'linear' with 'swing' to see different easing
    });
	
});
	
	


$(document).ready(function() {
	
	var notH = 1,
    $pop = $('.auto_options').hover(function(){notH^=1;});

	$(document).on('mouseup', function( e ){
	  if(notH||e.which==27) $pop.stop().slideUp();
	});
	
	$('.auto_options').on('click', function( e ){
	   $pop.stop().slideUp();
	});
	
	/////// CALL POPUP 
	$('#search_keyword').click(function(){
	  $pop.stop().slideDown(); 
	});
	

	
    $('#logo').click(function(e) {
        e.preventDefault();
        $('html, body').stop().animate({ scrollTop: 0 }, 1000, 'linear');
        // replace 'linear' with 'swing' to see different easing
    });
	
	$("#slide5_but").click(function() {
		$('html, body').animate({
			scrollTop: $("#slide5").offset().top
		}, 1500);
	});
	
	$("#slide6_but").click(function() {
		$('html, body').animate({
			scrollTop: $(".get_local_news_text").offset().top
		}, 1500);
	});
	
	
	
	
	
	$('.refine_search').click(function(){
		$(".filter1_bg").toggle();          
	});
	
	$('.more_options').click(function(){
		$(".more_option").toggle();          
	});
	
	$('.select_city').click(function(){
		$(".city").toggle(); 
		
		if($(".city").css('display') == 'block')
		{
			$('.nav_bg').css({'height':'auto'});
		} 
		else 
		{
			$('.nav_bg').css({'height':'100px'});	
		}         
	});
	
	
	$('.log_in').ready(function(){
		$(".nav_bg").css('bottom','0px');
		$(".nav_bg").css('position','absolute');
	});
	
	
	
	$('.log_in').click(function(){
		
		var height=$(window).height();
		$(window).scrollTop(height);			
		
		$(".login_dd_bg").css('display','block');
		
			if($(".login_dd_bg").css('display') == 'block')
			{
				$('.nav_bg').css({'height':'100px'});
			} 
			else 
			{
				$('.nav_bg').css({'height':'100px'});	
			}			
			
	});
	
	
	
	$('.log_in_c').click(function(){
		
		var height=$(window).height();
		$(window).scrollTop(height);
		
		
		$(".login_dd_bg").css('display','block');
		
			if($(".login_dd_bg").css('display') == 'block')
			{
				$('.nav_bg').css({'height':'100px'});
			} 
			else 
			{
				$('.nav_bg').css({'height':'100px'});	
			}			
			
	});
	
	 
		
	$('#login_close').click(function(){
		$(".login_dd_bg").css('display','none');
		$(".nav_bg").css('bottom','0px');			  
	});
	
	
	/*MERCHANT 9*/	
	
	$('#collapse_1').click(function(){
		$('#table_customer_data1').toggle();
		
		if($("#table_customer_data1").css('display') == 'block')
		{
			$('#collapse_1').text('- Collapse');
		} 
		else 
		{
			$('#collapse_1').text('+ Expand');	
		}	
		
	});	
	
	
	$('#arrow_1').click(function(){
		$('#add_calander_1').toggle();
		
		if($("#add_calander_1").css('display') == 'block')
		{
			$('#arrow_1').addClass('active');
		} 
		else 
		{
			$('#arrow_1').removeClass('active');	
		}	
		
	});
	
});

/*=======================TOP HEADER==========================*/

$(document).ready(function() {

		$(function() {
			$( "#datepicker" ).datepicker({
				changeMonth: true,
				changeYear: true
			});
			$( "#anim" ).change(function() {
				$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
			});
		});
	
		$(function() {
			$( "#datepicker2" ).datepicker({
				dateFormat: 'dd/mm/y',
				yearRange: 'c-80:c+1',
				changeMonth: true,
				changeYear: true
			});
			$( "#anim" ).change(function() {
				$( "#datepicker2" ).datepicker( "option", "showAnim", $( this ).val() );
			});
		});
	
		$(function() {
			$( "#offer_running_from" ).datepicker({
			minDate: '<?=date("m/d/Y")?>'			
		});
			$( "#anim" ).change(function() {
				$( "#offer_running_from" ).datepicker( "option", "showAnim", $( this ).val() );
			});
		});
		
		$(function() {
				$( "#offer_running_to" ).datepicker({ minDate: '<?=date("m/d/Y", strtotime("+1 days"))?>' });
				$( "#anim" ).change(function() {
					$( "#offer_running_to" ).datepicker( "option", "showAnim", $( this ).val() );
				});
		});
		
});



$(document).ready(function(){
	var height=$('.kwicks').height();
	var height2=$('.services_bg ul li').height();
	
	$(".kwicks").css('height',height2+'px');
	
	if ( height2 > 600) {
		
		$('.services_bg ul li').mouseenter(function(){	
			$(".kwicks").css('height',parseInt(height2+100)+'px');	
		});
	
		$('.services_bg ul li').mouseleave(function(){	
			$(".kwicks").css('height',height2+'px');
		});
		
	} else {
		
	}	

});


//alert(height)



 

$(window).resize(function() {
	var height=$(window).height();
	$("#slide1").css('min-height',height+'px');
});


$(window).scroll(function() {   
   var scrollVal = $(this).scrollTop();
   var height_w=$(window).height();
   
	if( scrollVal > height_w-100) {
		$('.nav_bg').css({'position':'fixed','top' :'0px'});
	} else {
		$('.nav_bg').css({'position':'absolute','top':'auto'});
	}
	
});


$(window).scroll(function() {
   var scrollVal = $(this).scrollTop();
   var height_y=$(window).height();
   
	if ( scrollVal > 0) {
		$('.nav_bg2').css({'position':'fixed','top' :'0px'});
	} else {
		$('.nav_bg2').css({'position':'relative','top':'auto'});
	}
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.signup_top_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.signup_top_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.customer_yello_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.customer_yello_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.search_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.search_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.merchant_header_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.merchant_header_bg').css({'margin-top':'0px'});
		
	}
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.popolo_banner_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.popolo_banner_bg').css({'margin-top':'0px'});
		
	}
	
	
	
	
});





/*=======================TOP HEADER==========================*/




