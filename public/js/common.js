function valid_email(eml)
{
	//declare the required variables
	var mint_len;
	var mstr_eml=eml;
	var mint_at=0;
	var mint_atnum=0;
	var mint_dot=0;
	var mint_dotnum=0;

	mint_len = eml.length; //takes the length of the email address entered
	//checking for the symbol single quote. If found replace it with its html code
	if (mstr_eml.indexOf("'")!=-1)
	{	
		mstr_eml=mstr_eml.replace("'","'");
	}
	//checking for the (@) & (.) symbol
	for(var iloop=0;iloop<mint_len;iloop++)
	{
		if(mstr_eml.charAt(iloop)=="@")
		{
			mint_at=iloop+1;
			mint_atnum=mint_atnum+1;
		}
		if(mstr_eml.charAt(iloop)==".")
		{
			mint_dot=iloop+1;
			mint_dotnum=mint_dotnum+1;
		}
	}
	//if nothing entered in the field
	if (mstr_eml=="")
	{
		return true;
	}
	//if @ entered more than once & dot (.) entered more than 4 times
	else if((mint_atnum!=1)||(mint_dotnum>4)||((mint_dot-mint_at)<2)||((mint_len-mint_dot)<2)||(mint_at<3))
	{
		return true;
	}
	//if any blank space is entered in the email address
	else if (mstr_eml.indexOf(" ")!=-1)
	{
		return true;
	}
	return false;
}
function checkBlankField (txt)
{
	var mint_txt = txt.length;
	var mstr_txt = txt;
	var mint_count = 0;
	for (var iloop = 0; iloop<mint_txt; iloop++)
	{
        if (mstr_txt.charAt(iloop) == " ")
        {
           mint_count = mint_count+1;
        }
	}    
// if nothing entered in the field
	if (txt == "")
   	{
		return false;
	}
	else if (mint_count == mint_txt)
	{
		return false;
	}
	return true;
}
function IsNumeric(strString)
{
	var strValidChars = "0123456789.";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) return false;
		for (i = 0; i < strString.length && blnResult == true; i++)
		{
			strChar = strString.charAt(i);
			if (strValidChars.indexOf(strChar) == -1)
			{
			blnResult = false;
			}
		}
   		return blnResult;
 }

function GetXmlHttpObject()
{
	if (window.XMLHttpRequest)
	{
	// code for IE7+, Firefox, Chrome, Opera, Safari
	return new XMLHttpRequest();
	}
	if (window.ActiveXObject)
	{
	// code for IE6, IE5
	return new ActiveXObject("Microsoft.XMLHTTP");
	}
	return null;
}
function uncheckall(divid)
{
	var collection = document.getElementById(divid).getElementsByTagName('input');
	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		collection[x].checked = false;
	}
	if(divid=='checkboxesgroup_confirmation')
	{
		document.getElementById('div_confirmation_methods').innerHTML = 'Please choose at least 1';
	}
	if(divid=='checkboxesgroup')
	{
		document.getElementById('div_categories').innerHTML = 'Category';
	}
	if(divid=='checkboxesgroupso')
	{
		document.getElementById('div_service_offered').innerHTML = ' *Please select';
	}

}

function uncheckall_bp(divid)
{
	var collection = document.getElementById(divid).getElementsByTagName('input');
	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		collection[x].checked = false;
	}
	if(divid=='checkboxesgroup_confirmation')
	{
		document.getElementById('div_confirmation_methods').innerHTML = 'Please choose at least 1';
	}
	if(divid=='checkboxesgroup')
	{
		document.getElementById('div_service_industries').innerHTML = ' *Please select';
		document.getElementById('choosen_business_types').value = "";
		
		document.getElementById('service_drop3').innerHTML = '<ul id="checkboxesgroupso"></ul>';
		document.getElementById('div_service_offered').innerHTML = " *Please select";
		change_perfect_for();
	}
	if(divid=='checkboxesgroupso')
	{
		document.getElementById('div_service_offered').innerHTML = ' *Please select';
	}

}


function uncheckall_pf(divid)
{
	var collection = document.getElementById(divid).getElementsByTagName('input');
	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		collection[x].checked = false;
	}
	if(divid=='checkboxesgrouppf')
	{
		document.getElementById('div_perfect_for').innerHTML = '*Perfect For';
	}
}



function uncheckall_merchant(divid)
{
	var collection = document.getElementById(divid).getElementsByTagName('input');
	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		{
			if(collection[x].id !='confirmation_methods_email')
			{
				collection[x].checked = false;
			}
		}
	}
	if(divid=='checkboxesgroup_confirmation')
	{
		document.getElementById('div_confirmation_methods').innerHTML = 'Email';
		document.getElementById('bp_mobile_number').value = "";
		document.getElementById('bp_mobile_number').style.display="none";
	}
	if(divid=='checkboxesgroup')
	{
		document.getElementById('div_categories').innerHTML = 'Category';
	}
}


function add_another_social_media()
{
	var num_extra_social = document.getElementById('num_extra_social').value;
}
function show_more_social()
{
	document.getElementById('li_more_social').style.display="block";
}
function showsocial(val)
{
	if(val == '3')
	{
		document.getElementById('li_linkedin').style.display="block";
	}
	if(val == '4')
	{
		document.getElementById('li_googleplus').style.display="block";
	}

	if ($('#li_linkedin').css('display')=='block' && $('#li_googleplus').css('display')=='block') {
		$('#li_show_more_social').css("display", "none");
	} else {
		$('#li_show_more_social').css("display", "block");
	}
}
function choose_business_type(val, ischecked)
{
	var choosen_business_types = document.getElementById('choosen_business_types').value;
	if(ischecked)
	{
		if(choosen_business_types != '')
		{
			var array_business_types = choosen_business_types.split(","); 
			var a = array_business_types.indexOf(val); 
			if(a === -1)
			{
				array_business_types.push(val);
			}
			var array_length = array_business_types.length;
			if(array_length >1)
			{
				var new_business_types = array_business_types.join(); 
			}
			else
			{
				var new_business_types = val; 
			}
		}
		else
		{
			var new_business_types = val; 
		}
	}
	else
	{
		if(choosen_business_types != '')
		{
			var array_business_types = choosen_business_types.split(","); 
			var index = array_business_types.indexOf(val);
			if (index > -1) {
				array_business_types.splice(index, 1);
			}
			var new_business_types = array_business_types.join(); 
		}
	}
	document.getElementById('choosen_business_types').value = new_business_types;
	//alert(new_business_types);
	// AJAX CALL
	var httpj1;
	url=''+$baseURL+'merchantaccount/getservices/';
	if(window.XMLHttpRequest){
		httpj1=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpj1=new ActiveXObject('msxml2.XMLHTTP');
	}
	if(httpj1){
		httpj1.open('post',url,true);
		httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpj1.send("bt="+new_business_types+'&time='+Math.random());
		httpj1.onreadystatechange=function(){
			if(httpj1.readyState==4){
				if(httpj1.status==200){
					//alert(httpj1.responseText);
					//document.getElementById('td_service_offered').innerHTML = httpj1.responseText;
					//document.getElementById('num_services_offered').value = '1';
					//$(".revostyled").selectbox();
					//document.getElementById('div_service_industries3').innerHTML = httpj1.responseText;
					document.getElementById('service_drop3').innerHTML = httpj1.responseText;
					document.getElementById('div_service_offered').innerHTML = " *Please select";
					document.getElementById('div_perfect_for').innerHTML = "*Perfect For";
					change_perfect_for();
				}
			}
		}
	}
}


function change_perfect_for()
{
	var choosen_business_types = document.getElementById('choosen_business_types').value;
	//alert(choosen_business_types);
	var httppf;
	url=''+$baseURL+'merchantaccount/getperfectfor/';
	if(window.XMLHttpRequest){
		httppf=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httppf=new ActiveXObject('msxml2.XMLHTTP');
	}
	if(httppf){
		httppf.open('post',url,true);
		httppf.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httppf.send("bt="+choosen_business_types+'&time='+Math.random());
		httppf.onreadystatechange=function(){
			if(httppf.readyState==4){
				if(httppf.status==200){
					//alert(httppf.responseText);
					//document.getElementById('li_perfect_for').innerHTML = httppf.responseText;
					//$(".revostyled").selectbox();
					//document.getElementById('div_perfect_for3').innerHTML = httppf.responseText;
					document.getElementById('checkboxesgrouppf').innerHTML = httppf.responseText;
				}
			}
		}
	}
}


function show_more_open_time()
{
	var num_open_time = parseInt(document.getElementById('num_open_time').value);
	num_open_time = num_open_time + 1;
	document.getElementById('opening_hours_'+num_open_time).style.display="block";
	document.getElementById('num_open_time').value = num_open_time;
	if(num_open_time == 25)
	{
		document.getElementById('anc_show_more_open_time').style.display="none";
	}
}

function show_more_open_time_bp()
{
	var num_open_time = parseInt(document.getElementById('num_open_time').value);
	num_open_time = num_open_time + 1;
	document.getElementById('opening_hours_'+num_open_time).style.display="block";
	document.getElementById('num_open_time').value = num_open_time;
	if(num_open_time == 7)
	{
		document.getElementById('anc_show_more_open_time').style.display="none";
	}
}

/*function add_more_service_offered()
{
	var num_services_offered = parseInt(document.getElementById('num_services_offered').value);
	num_services_offered = num_services_offered + 1;
	document.getElementById('ul_service_offered_'+num_services_offered).style.display="block";
	document.getElementById('num_services_offered').value = num_services_offered;
	if(num_services_offered == 10)
	{
		document.getElementById('anc_add_more_service_offered').style.display="none";
	}
}*/


function add_more_service_offered()
{
	var num_category = parseInt(document.getElementById('num_so').value);
	num_category = num_category + 1;
	document.getElementById('div_so_'+num_category).style.display="block";
	document.getElementById('num_so').value = num_category;
	if(num_category == 10)
	{
		document.getElementById('anc_add_more_service_offered').style.display="none";
	}
}

function show_access_options(id,access_options)
{				
		
	if(id=='3'){
		var httpj1;
		url=''+$baseURL+'merchantaccount/getaccessoptions';
		if(window.XMLHttpRequest){
			httpj1=new XMLHttpRequest();
		}else if(window.ActiveXObject){
			httpj1=new ActiveXObject('msxml2.XMLHTTP');
		}		
		if(httpj1){
			httpj1.open('post',url,true);
			httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			//httpj1.send("type_id="+id+'&time='+Math.random());
			httpj1.send("type_id="+id+ '&access_options='+access_options+'&time='+Math.random());
			httpj1.onreadystatechange=function(){
				if(httpj1.readyState==4){
					if(httpj1.status==200){
						//alert(httpj1.responseText);
						document.getElementById('access_options_id').innerHTML = httpj1.responseText;
						
					}
				}
			}
		}
	}else if(document.getElementById('access_options_id')){
		document.getElementById('access_options_id').innerHTML = "";
	}

	if(id=='1')
	{
		document.getElementById('accessmsg').innerHTML = "This user will be able to see/add/edit everything";
	}
	else if(id=='2')
	{
		document.getElementById('accessmsg').innerHTML = "This user will be able to see/add/edit offers, bookings and stats";
	}
	else if(id=='3')
	{
		document.getElementById('accessmsg').innerHTML = "Select what sections of this merchant area this user will be able to see/add/edit.";
	}
}

function anc_add_more_cards()
{
	var num_cards = parseInt(document.getElementById('num_cards').value);
	num_cards = num_cards + 1;
	document.getElementById('ul_cards_'+num_cards).style.display="block";
	document.getElementById('num_cards').value = num_cards;
	if(num_cards == 10)
	{
		document.getElementById('anc_add_more_cards').style.display="none";
	}
}
function remove_shortlist(oid)
{
	/*var conf = confirm("Do you want to remove it from the shortlists?");
	if(conf)
	{
		window.location.href = ''+$baseURL+'customeraccount/removeshortlist/?oid='+oid;
	}*/
	window.location.href = ''+$baseURL+'customeraccount/removeshortlist/?oid='+oid;
}
function unsubscribefromall()
{
	var chk = document.getElementById('unsubscribefromall').checked;
	if(!chk)
	{
		var collection = document.getElementById('checkboxesdiv').getElementsByTagName('input');
		for (var x=0; x<collection.length; x++) {
			if (collection[x].type.toUpperCase()=='CHECKBOX')
			collection[x].checked = false;
		}

		var collection2 = document.getElementById('checkboxesdiv').getElementsByClassName('styledCheckbox');
		for (var y=0; y<collection2.length; y++) {
			collection2[y].style.backgroundPosition="0px 0px";
		}
	}
}

function uncheckallsubs()
{
	var chk = document.getElementById('unsubscribefromall').checked;
	if(!chk)
	{
		var collection = document.getElementById('checkboxesdiv').getElementsByTagName('input');
		for (var x=0; x<collection.length; x++) {
			if (collection[x].type.toUpperCase()=='CHECKBOX')
			collection[x].checked = false;
		}
		if (document.querySelector)
		{
			var checkedhobbies=document.querySelectorAll('#checkboxesdiv .styledCheckbox')
			for (var i=0; i<checkedhobbies.length; i++){
				checkedhobbies[i].style.backgroundPosition = '0px 0px';
			}
		}
		else
		{
			var collection2 = document.getElementById('checkboxesdiv').getElementsByClassName('styledCheckbox');
			for (var y=0; y<collection2.length; y++) {
				collection2[y].style.backgroundPosition="0px 0px";
			}
		}
	}
}

function resetunsubscribe(obj)
{
	document.getElementById(obj).value = '1';
	var collection = document.getElementsByClassName('upper');
	var collection2 = collection[0].getElementsByClassName('styledCheckbox');
	collection2[0].style.backgroundPosition="0px 0px";
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function calculatetotal(val)
{
	if(val != '')
	{
		var offer_price = parseFloat(document.getElementById('offerprice').value);
		var total = parseInt(val)* offer_price;
		document.getElementById('total_price').value = "$"+total.toFixed(2);
	}
	else
	{
		document.getElementById('total_price').value = '';
	}
}

/*function calculatediscount(code)
{				
	var httpdiscount;
	url=''+$baseURL+'booking/getdiscount';
	if(window.XMLHttpRequest){
		httpdiscount=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpdiscount=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpdiscount){
		httpdiscount.open('post',url,true);
		httpdiscount.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpdiscount.send("code="+code+'&time='+Math.random());
		httpdiscount.onreadystatechange=function(){
			if(httpdiscount.readyState==4){
				if(httpdiscount.status==200){
					//alert(httpdiscount.responseText);
					if(httpdiscount.responseText != '')
					{
						var prevdicsountvalue = document.getElementById('discount').value;

						document.getElementById('lidiscount').style.display="block";
						document.getElementById('discount').value = httpdiscount.responseText;
						//alert(httpdiscount.responseText);
						if(document.getElementById('total_price').value != '')
						{
							var old_total = parseFloat(document.getElementById('total_price').value);
							var discount = parseFloat(document.getElementById('discount').value);
							if(discount=='0')
							{
								var new_price = parseFloat(old_total) + parseFloat(prevdicsountvalue);
							}
							else
							{
								if(prevdicsountvalue == '' || prevdicsountvalue=='0')
								{
									var new_price = old_total - discount;
								}
								else
								{
									var new_price = old_total;
								}
							}
							document.getElementById('total_price').value = new_price;
						}
					}
					else
					{
						var old_total = parseFloat(document.getElementById('total_price').value);
						if(document.getElementById('discount').value != '')
						{
							var discount = parseFloat(document.getElementById('discount').value);
						}
						else
						{
							var discount = 0;
						}
						var new_price = old_total + discount;
						document.getElementById('total_price').value = new_price;
						document.getElementById('lidiscount').style.display="none";
					}
				}
			}
		}
	}
}*/

function calculatediscount()
{				
	document.getElementById('anccalculatediscount').style.display="none";
	var code = document.getElementById('promotion_code').value;
	//var subtotal = document.getElementById('total_price').value;
	var val = document.getElementById('number_of_people').value;
	if(val == '')
	{
		alert('Please enter the number of people');
		return false;
	}
	var offer_price = parseFloat(document.getElementById('offerprice').value);
	var subtotal = parseInt(val)* offer_price;
	document.getElementById('total_price').value = "$"+subtotal.toFixed(2);

	/*if(document.getElementById('lidiscount').style.display=="block")
	{
		var discount = document.getElementById('discount').value;
		if(discount != '')
		{
			subtotal = parseInt(subtotal) + parseInt(discount);
		}
	}*/

	if(subtotal=='')
	{
		alert('Please enter the number of people');
		return false;
	}
	else
	{
		if(code=='')
		{
			alert('Please enter the promo code');
			return false;
		}
		else
		{
			var httpdiscount;
			url=''+$baseURL+'booking/getdiscount';
			if(window.XMLHttpRequest){
				httpdiscount=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				httpdiscount=new ActiveXObject('msxml2.XMLHTTP');
			}		
			if(httpdiscount){
				httpdiscount.open('post',url,true);
				httpdiscount.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				httpdiscount.send("code="+code+"&subtotal="+subtotal+"&time="+Math.random());
				httpdiscount.onreadystatechange=function(){
					if(httpdiscount.readyState==4){
						if(httpdiscount.status==200){
							//alert(httpdiscount.responseText);
							if(httpdiscount.responseText != '')
							{
								document.getElementById('anccalculatediscount').style.display="block";
								var prevdicsountvalue = document.getElementById('discount').value;

								document.getElementById('lidiscount').style.display="block";
								//document.getElementById('discount').value = httpdiscount.responseText;
								//alert(parseFloat(httpdiscount.responseText));
								document.getElementById('discount').value = "$"+httpdiscount.responseText;
								
								if(document.getElementById('total_price').value != '')
								{
									//var old_total = parseFloat(document.getElementById('total_price').value);
									//var discount = parseFloat(document.getElementById('discount').value);
									
									var old_total = parseFloat(document.getElementById('total_price').value.replace("$", "")); 
									var discount = parseFloat(document.getElementById('discount').value.replace("$", "")); 

									var new_price = old_total - discount;
									//document.getElementById('total_price').value = new_price;
									document.getElementById('total_price').value = "$"+new_price.toFixed(2);


									/*if(discount=='0')
									{
										var new_price = parseFloat(old_total) + parseFloat(prevdicsountvalue);
									}
									else
									{
										if(prevdicsountvalue == '' || prevdicsountvalue=='0')
										{
											var new_price = old_total - discount;
										}
										else
										{
											var new_price = old_total;
										}
									}
									document.getElementById('total_price').value = new_price;*/
								}
							}
							else
							{
								var old_total = parseFloat(document.getElementById('total_price').value);
								if(document.getElementById('discount').value != '')
								{
									var discount = parseFloat(document.getElementById('discount').value);
								}
								else
								{
									var discount = 0;
								}
								var new_price = old_total + discount;
								//document.getElementById('total_price').value = new_price;
								document.getElementById('total_price').value = "$"+new_price.toFixed(2);
								document.getElementById('lidiscount').style.display="none";
							}
						}
					}
				}
			}
		}
	}
}


function calculatediscount_paymentoptions()
{				
	var code = document.getElementById('promotion_code').value;
	var val = document.getElementById('number_of_people').value;
	var offer_price = parseFloat(document.getElementById('offerprice').value);
	var subtotal = parseInt(val)* offer_price;
	
	var httpdiscount;
	url=''+$baseURL+'booking/getdiscountpaymentoptions';
	if(window.XMLHttpRequest){
		httpdiscount=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpdiscount=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpdiscount){
		httpdiscount.open('post',url,true);
		httpdiscount.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpdiscount.send("code="+code+"&subtotal="+subtotal+"&time="+Math.random());
		httpdiscount.onreadystatechange=function(){
			if(httpdiscount.readyState==4){
				//alert(httpdiscount.status);
				if(httpdiscount.status==200){
					//alert(httpdiscount.responseText);
					if(httpdiscount.responseText != '')
					{
						document.getElementById('li_calc_discount').innerHTML = httpdiscount.responseText;
					}
					else
					{
						document.getElementById('li_calc_discount').innerHTML = "";
					}
				}
			}
		}
	}
}


function changeusertype(type)
{
	if(type=='1')
	{
		document.getElementById('ul_returninguser').style.display="none";
		document.getElementById('ul_guestuser').style.display="block";
		document.getElementById('li_guest').className = "active";
		document.getElementById('li_returning').className = "";
	}
	else
	{
		document.getElementById('ul_returninguser').style.display="block";
		document.getElementById('ul_guestuser').style.display="none";
		document.getElementById('li_returning').className = "active";
		document.getElementById('li_guest').className = "";
	}
}

function changeusertype_li(type, obj)
{
	if(type=='1')
	{
		document.getElementById('guest').checked=true;
		var divx = document.getElementById('li_guest').getElementsByClassName('styledRadio');
		divx[0].style.backgroundPosition = '0px -18px';

		var divy = document.getElementById('li_returning').getElementsByClassName('styledRadio');
		divy[0].style.backgroundPosition = '0px 0px';
	}
	else
	{
		document.getElementById('returning').checked=true;
		var divx = document.getElementById('li_guest').getElementsByClassName('styledRadio');
		divx[0].style.backgroundPosition = '0px 0px';

		var divy = document.getElementById('li_returning').getElementsByClassName('styledRadio');
		divy[0].style.backgroundPosition = '0px -18px';
	}
	changeusertype(type);
}
function validatecheckout()
{
	if(document.getElementById('guest').checked)
	{
		if(checkBlankField(document.getElementById('guest_email').value) == false)
		{
			alert("Please enter your email!");
			document.getElementById('guest_email').focus();
			return false;
		}
		if(valid_email(document.getElementById('guest_email').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('guest_email').focus();
			return false
		}
	}
	else
	{
		if(checkBlankField(document.getElementById('returning_email').value) == false)
		{
			alert("Please enter your email!");
			document.getElementById('returning_email').focus();
			return false;
		}
		if(valid_email(document.getElementById('returning_email').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('returning_email').focus();
			return false
		}
		if(checkBlankField(document.getElementById('returning_password').value) == false)
		{
			alert("Please enter your password!");
			document.getElementById('returning_password').focus();
			return false;
		}
	}
	//return false;
}
function changepaymentmethod(method)
{
	if(method == '1')
	{
		document.getElementById('li_cc').className = 'active';
		document.getElementById('li_pp').className = '';
		document.getElementById('li_mp').className = '';
		document.getElementById('li_save_card').style.display = 'block';
	}
	else if(method == '2')
	{
		document.getElementById('li_cc').className = '';
		document.getElementById('li_pp').className = 'active';
		document.getElementById('li_mp').className = '';
		document.getElementById('li_save_card').style.display = 'none';
	}
	else if(method == '3')
	{
		document.getElementById('li_cc').className = '';	
		document.getElementById('li_pp').className = '';
		document.getElementById('li_mp').className = 'active';
		document.getElementById('li_save_card').style.display = 'none';
	}
}
function viewmoreaboutorder(oid)
{
	if(document.getElementById('moredetailorder_'+oid).style.display=="none")
	{
		document.getElementById('moredetailorder_'+oid).style.display= "block";
		document.getElementById('ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/cus_share1.png" alt="" />';
	}
	else
	{
		document.getElementById('moredetailorder_'+oid).style.display= "none";
		document.getElementById('ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/up_cus_share1.png" alt="" />';
	}
}

function viewmoreaboutorder_myorders(oid, ty)
{
	if(document.getElementById(ty+'_moredetailorder_'+oid).style.display=="none")
	{
		document.getElementById(ty+'_moredetailorder_'+oid).style.display= "block";
		document.getElementById(ty+'_ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/cus_share1.png" alt="" />';
	}
	else
	{
		document.getElementById(ty+'_moredetailorder_'+oid).style.display= "none";
		document.getElementById(ty+'_ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/up_cus_share1.png" alt="" />';
	}
}


function sortrecord(column, type, orderby)
{				
	var httpsort;
	url=''+$baseURL+'customeraccount/sortrecord';
	if(window.XMLHttpRequest){
		httpsort=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpsort=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpsort){
		httpsort.open('post',url,true);
		httpsort.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		if(type == 'current')
		{
			httpsort.send('column='+column+'&type='+type+'&orderby='+orderby+'&time='+Math.random());
		}
		else
		{
			var keyword = document.getElementById('keyword').value;
			var filter_month = document.getElementById('filter_month').value;
			var datepicker = document.getElementById('datepicker').value;
			var datepicker2 = document.getElementById('datepicker2').value;

			httpsort.send('column='+column+'&type='+type+'&orderby='+orderby+'&keyword='+keyword+'&filter_month='+filter_month+'&date_from='+datepicker+'&date_to='+datepicker2+'&time='+Math.random());
		}
		httpsort.onreadystatechange=function(){
			if(httpsort.readyState==4){
				if(httpsort.status==200){
					//alert(httpsort.responseText);
					if(type == 'current')
					{
						document.getElementById('current_booking').innerHTML = httpsort.responseText;
					}
					else
					{
						document.getElementById('historydiv').innerHTML = httpsort.responseText;
					}
				}
			}
		}
	}
}
function isNumberKey(evt, obj)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode != 58)
	{
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		{
			return false;
		}
	}
	if(charCode == 8)
	{
		return true;
	}
	var val = document.getElementById(obj).value;
	var len = val.length;
	if(len==2)
	{
		document.getElementById(obj).value = document.getElementById(obj).value + ":";
	}	
	if(len>=5)
	{
		return false;
	}
	return true;
}

function checksubsnewsletter()
{
	if(checkBlankField(document.getElementById('nl_email').value) == false)
	{
		alert("Please enter your email!");
		document.getElementById('nl_email').focus();
		return false;
	}
	else if(valid_email(document.getElementById('nl_email').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('nl_email').focus();
		return false
	}
	if(checkBlankField(document.getElementById('nl_gender').value) == false)
	{
		alert("Please choose your gender!");
		document.getElementById('nl_gender').focus();
		return false;
	}
	ajaxSubscribeNewsletter();
	return false;
}

function ajaxSubscribeNewsletter()
{				
	var nl_email = document.getElementById('nl_email').value;
	var nl_gender = document.getElementById('nl_gender').value;
	var httpnl;
	//alert(nl_email);
	url=''+$baseURL+'home/subscribenewsletter';
	if(window.XMLHttpRequest){
		httpnl=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpnl=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpnl){
		httpnl.open('post',url,true);
		httpnl.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpnl.send('nl_email='+nl_email+'&nl_gender='+nl_gender+'&time='+Math.random());
		httpnl.onreadystatechange=function(){
			if(httpnl.readyState==4){
				if(httpnl.status==200){
					//alert(httpnl.responseText);
					//document.getElementById('newsletter_p').innerHTML = httpnl.responseText;
					//document.getElementById('div_success_message').innerHTML = httpnl.responseText;
					document.getElementById('div_success_message').innerHTML = "<div class='sign_up_main2'>"+httpnl.responseText+"</div>";
				}
			}
		}
	}
}

function showcontentofsection(sectionname)
{
	document.getElementById('content_'+sectionname).style.visibility="visible";
	document.getElementById('content_'+sectionname).style.height="auto";
	document.getElementById('content_'+sectionname).style.overflow="visible";
}

function deletecc(cid)
{
	window.location.href = ''+$baseURL+'customeraccount/removecc/?cid='+cid;
}

function change_confirmation_methods(val, chk)
{
	var returnstring = '';
	var smschecked = '0';
	if(document.getElementById('confirmation_methods_sms').checked)
	{
		returnstring += 'SMS, ';
		smschecked = '1';
	}
	if(document.getElementById('confirmation_methods_email').checked)
	{
		returnstring += 'Email, ';
	}
	if(document.getElementById('confirmation_methods_phone').checked)
	{
		returnstring += 'Phone Call, ';
	}
	if(returnstring != '')
	{
		returnstring = returnstring.substring(0, returnstring.length - 2); 
		document.getElementById('div_confirmation_methods').innerHTML = returnstring;
	}
	else
	{
		document.getElementById('div_confirmation_methods').innerHTML = 'Please choose at least 1';
	}

	if(smschecked=='1')
	{
		document.getElementById('bp_mobile_number').style.display="block";
	}
	else
	{
		document.getElementById('bp_mobile_number').value="";
		document.getElementById('bp_mobile_number').style.display="none";
	}
}

function listyourbusiness_validate()
{
	if(checkBlankField(document.getElementById('business_name').value) == false)
	{
		alert("Please enter your business name!");
		document.getElementById('business_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_website_address').value) == false)
	{
		alert("Please enter your website address!");
		document.getElementById('business_website_address').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_street_address').value) == false)
	{
		alert("Please enter your street address!");
		document.getElementById('business_street_address').focus();
		return false;
	}
	/*if(checkBlankField(document.getElementById('business_category_1').value) == false)
	{
		alert("Please select your business category!");
		document.getElementById('business_category_1').focus();
		return false;
	}*/
	/*
	var num_category = parseInt(document.getElementById('num_category').value);
	for(var i=1; i<=num_category; i++)
	{
		if(checkBlankField(document.getElementById('business_category_'+i).value) == false)
		{
			alert("Please select your business category!");
			document.getElementById('business_category_'+i).focus();
			return false;
		}
	}
	*/
	if(checkBlankField(document.getElementById('business_contact_name').value) == false)
	{
		alert("Please enter your contact name!");
		document.getElementById('business_contact_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_email_address').value) == false)
	{
		alert("Please enter your email address!");
		document.getElementById('business_email_address').focus();
		return false;
	}
	else if(valid_email(document.getElementById('business_email_address').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('business_email_address').focus();
		return false
	}
	/*if(checkBlankField(document.getElementById('business_password').value) == false)
	{
		alert("Please enter your password!");
		document.getElementById('business_password').focus();
		return false;
	}*/
	if(document.getElementById('is_authority').checked == false)
	{
		alert("Do you have authority to register this business?");
		document.getElementById('is_authority').focus();
		return false;
	}
	
}

function add_more_category()
{
	var num_category = parseInt(document.getElementById('num_category').value);
	num_category = num_category + 1;
	document.getElementById('div_business_category_'+num_category).style.display="block";
	document.getElementById('num_category').value = num_category;
	if(num_category == 10)
	{
		document.getElementById('anc_add_more_category').style.display="none";
	}
}
function popupLogin()
{
	var is_merchant = '0';
	var rememberme = '0';
	if(document.getElementById('is_merchant').checked)
	{
		is_merchant = '1';	
	}
	if(document.getElementById('rememberme').checked)
	{
		rememberme = '1';	
	}
	if(checkBlankField(document.getElementById('popuplogin_email').value) == false)
	{
		alert("Please enter email address!");
		document.getElementById('popuplogin_email').focus();
		return false;
	}
	else if(valid_email(document.getElementById('popuplogin_email').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('popuplogin_email').focus();
		return false
	}
	if(checkBlankField(document.getElementById('popuplogin_pwd').value) == false)
	{
		alert("Please enter password!");
		document.getElementById('popuplogin_pwd').focus();
		return false;
	}
	var popuplogin_email = document.getElementById('popuplogin_email').value;
	var popuplogin_pwd = document.getElementById('popuplogin_pwd').value;
	ajaxLogin(popuplogin_email, popuplogin_pwd, is_merchant, rememberme);
	return false;
}

function ajaxLogin(popuplogin_email, popuplogin_pwd, is_merchant, rememberme)
{
	//alert(popuplogin_email+popuplogin_pwd+is_merchant+rememberme);
	var httppopuplogin;
	url=''+$baseURL+'signup/ajaxlogin';
	if(window.XMLHttpRequest){
		httppopuplogin=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httppopuplogin=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httppopuplogin){
		httppopuplogin.open('post',url,true);
		httppopuplogin.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httppopuplogin.send('popuplogin_email='+escape(popuplogin_email)+'&popuplogin_pwd='+escape(popuplogin_pwd)+'&is_merchant='+escape(is_merchant)+'&rememberme='+escape(rememberme)+'&time='+Math.random());
		httppopuplogin.onreadystatechange=function(){
			if(httppopuplogin.readyState==4){
				if(httppopuplogin.status==200){
					//alert(httppopuplogin.responseText);
					var res = httppopuplogin.responseText.split("~~~"); 
					//alert(res[0]);
					if(res[0]=="success")
					{
							if(is_merchant == '1')
							{
								if(parseInt(res[1])>0)
								{
									window.location.href = ''+$baseURL+'merchantaccount/viewcontactinfo';
								}
								else
								{
									window.location.href = ''+$baseURL+'merchantaccount/contactinfo';
								}
							}
							else
							{
								window.location.href = ''+$baseURL+'customeraccount/contactinfo';
							}
					}
					else if(res[0]=="successcookie")
					{
							document.remembermeform.remember_email.value = popuplogin_email;
							document.remembermeform.remember_pw.value = popuplogin_pwd;
							if(is_merchant == '1')
							{
								if(parseInt(res[1])>0)
								{
									//window.location.href = ''+$baseURL+'merchantaccount/viewcontactinfo?remember=me';
									document.remembermeform.action = ""+$baseURL+"merchantaccount/viewcontactinfo";
								}
								else
								{
									//window.location.href = ''+$baseURL+'merchantaccount/contactinfo?remember=me';
									document.remembermeform.action = ""+$baseURL+"merchantaccount/contactinfo";
								}
							}
							else
							{
								//window.location.href = ''+$baseURL+'customeraccount/contactinfo?remember=me';
								document.remembermeform.action = ""+$baseURL+"customeraccount/contactinfo";
							}
							document.remembermeform.submit();
					}
					else
					{
						//alert(httppopuplogin.responseText);
						document.getElementById('ajaxloginmsg').style.display = "block";
						document.getElementById('ajaxloginmsg').innerHTML = httppopuplogin.responseText;
					}
					/*if(httppopuplogin.responseText!="")
					{
						alert(httppopuplogin.responseText);
					}*/
				}
			}
		}
	}
}

function forgotPassword()
{
	var is_merchant = '0';
	if(document.getElementById('forgot_is_merchant').checked)
	{
		is_merchant = '1';	
	}
	if(checkBlankField(document.getElementById('forgot_email').value) == false)
	{
		alert("Please enter email address!");
		document.getElementById('forgot_email').focus();
		return false;
	}
	else if(valid_email(document.getElementById('forgot_email').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('forgot_email').focus();
		return false
	}
	var forgot_email = document.getElementById('forgot_email').value;
	ajaxForgotPassword(forgot_email, is_merchant);
	return false;
}

function ajaxForgotPassword(forgot_email, is_merchant)
{
	var httppwd;
	url=''+$baseURL+'signup/ajaxforgotpassword';
	if(window.XMLHttpRequest){
		httppwd=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httppwd=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httppwd){
		httppwd.open('post',url,true);
		httppwd.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httppwd.send('forgot_email='+escape(forgot_email)+'&is_merchant='+escape(is_merchant)+'&time='+Math.random());
		httppwd.onreadystatechange=function(){
			if(httppwd.readyState==4){
				if(httppwd.status==200){
					/*if(httppwd.responseText=="success")
					{
						alert(httppwd.responseText);
					}
					else
					{
						alert(httppwd.responseText);
					}*/
					alert(httppwd.responseText);
				}
			}
		}
	}
}
function search_offers(sorttby)
{
	var search_pgArray = [];
	$(".search_pg:checked").each(function() {
        search_pgArray.push($(this).val());
    });
    var search_pg;
    search_pg = search_pgArray.join(',');


	var search_distanceArray = [];
	$(".search_distance:checked").each(function() {
        search_distanceArray.push($(this).val());
    });
    var search_distance;
    search_distance = search_distanceArray.join(',');

	
	var search_cuArray = [];
	$(".search_cu:checked").each(function() {
        search_cuArray.push($(this).val());
    });
    var search_cu;
    search_cu = search_cuArray.join(',');
	
	
	var search_beautyArray = [];
	$(".search_beauty:checked").each(function() {
        search_beautyArray.push($(this).val());
    });
    var search_beauty;
    search_beauty = search_beautyArray.join(',');


	var search_fitnessArray = [];
	$(".search_fitness:checked").each(function() {
        search_fitnessArray.push($(this).val());
    });
    var search_fitness;
    search_fitness = search_fitnessArray.join(',');


	var search_hairArray = [];
	$(".search_hair:checked").each(function() {
        search_hairArray.push($(this).val());
    });
    var search_hair;
    search_hair = search_hairArray.join(',');


	var search_makeupArray = [];
	$(".search_makeup:checked").each(function() {
        search_makeupArray.push($(this).val());
    });
    var search_makeup;
    search_makeup = search_makeupArray.join(',');


	var search_massageArray = [];
	$(".search_massage:checked").each(function() {
        search_massageArray.push($(this).val());
    });
    var search_massage;
    search_massage = search_massageArray.join(',');


	var search_mensArray = [];
	$(".search_mens:checked").each(function() {
        search_mensArray.push($(this).val());
    });
    var search_mens;
    search_mens = search_mensArray.join(',');



	var search_nailsArray = [];
	$(".search_nails:checked").each(function() {
        search_nailsArray.push($(this).val());
    });
    var search_nails;
    search_nails = search_nailsArray.join(',');



	var search_tanningArray = [];
	$(".search_tanning:checked").each(function() {
        search_tanningArray.push($(this).val());
    });
    var search_tanning;
    search_tanning = search_tanningArray.join(',');


	var search_teethArray = [];
	$(".search_teeth:checked").each(function() {
        search_teethArray.push($(this).val());
    });
    var search_teeth;
    search_teeth = search_teethArray.join(',');

	

	var search_waxingArray = [];
	$(".search_waxing:checked").each(function() {
        search_waxingArray.push($(this).val());
    });
    var search_waxing;
    search_waxing = search_waxingArray.join(',');


	var search_pfArray = [];
	$(".search_pf:checked").each(function() {
        search_pfArray.push($(this).val());
    });
    var search_pf;
    search_pf = search_pfArray.join(',');

	
	var user_ratingArray = [];
	$(".user_rating:checked").each(function() {
        user_ratingArray.push($(this).val());
    });
    var user_rating;
    user_rating = user_ratingArray.join(',');


	//alert(search_pf);
	

	//document.main_search_form.submit();
	var search_term = document.main_search_form.search_term.value;
	var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	var search_date = document.main_search_form.search_date.value;
	if(hidden_option_identifier == '1')
	{
		var search_time = document.main_search_form.search_time_restaurant.value;
	}
	else
	{
		var search_time = document.main_search_form.search_time_other.value;
	}
	
	//alert(search_time);
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;
	//var hidden_option_identifier = document.main_search_form.hidden_option_identifier.value;

	
	var httpsearch;
	url=''+$baseURL+'home/ajaxsearchresults';
	if(window.XMLHttpRequest){
		httpsearch=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpsearch=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpsearch){
		httpsearch.open('post',url,true);
		httpsearch.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpsearch.send('searchact=1&search_term='+search_term+'&hidden_option_identifier='+hidden_option_identifier+'&search_date='+search_date+'&search_time='+search_time+'&search_pg='+search_pg+'&search_distance='+search_distance+'&search_cu='+search_cu+'&search_beauty='+search_beauty+'&search_fitness='+search_fitness+'&search_hair='+search_hair+'&search_makeup='+search_makeup+'&search_massage='+search_massage+'&search_mens='+search_mens+'&search_nails='+search_nails+'&search_tanning='+search_tanning+'&search_teeth='+search_teeth+'&search_waxing='+search_waxing+'&search_pf='+search_pf+'&user_rating='+user_rating+'&sorttby='+sorttby+'&time='+Math.random());


		httpsearch.onreadystatechange=function(){
			if(httpsearch.readyState==4){
				if(httpsearch.status==200){
					//alert(httpsearch.responseText);
					document.getElementById('ajaxsearchresults').innerHTML = httpsearch.responseText;
					$(".revostyled").selectbox();
				}
			}
			else
			{
				document.getElementById('ajaxsearchresults').innerHTML = "Please wait...";
			}
		}
	}
}

function fill_keyword(kw, idx)
{
	document.getElementById('hidden_option_identifier').value = idx;
	
	document.getElementById('search_term').value = kw;
	document.getElementById('filter_3').style.display="none";
	document.getElementById('filter_4').style.display="none";
	document.getElementById('filter_7').style.display="none";
	document.getElementById('filter_8').style.display="none";
	document.getElementById('filter_9').style.display="none";
	document.getElementById('filter_10').style.display="none";
	document.getElementById('filter_11').style.display="none";
	document.getElementById('filter_12').style.display="none";
	document.getElementById('filter_13').style.display="none";
	document.getElementById('filter_14').style.display="none";
	document.getElementById('filter_15').style.display="none";
	if(idx=='1')
	{
		document.getElementById('filter_3').style.display="block";
	}
	else if(idx=='3')
	{
		document.getElementById('filter_4').style.display="block";
	}
	else if(idx=='4')
	{
		document.getElementById('filter_7').style.display="block";
	}
	else if(idx=='5')
	{
		document.getElementById('filter_8').style.display="block";
	}
	else if(idx=='6')
	{
		document.getElementById('filter_9').style.display="block";
	}
	else if(idx=='7')
	{
		document.getElementById('filter_10').style.display="block";
	}
	else if(idx=='8')
	{
		document.getElementById('filter_11').style.display="block";
	}
	else if(idx=='9')
	{
		document.getElementById('filter_12').style.display="block";
	}
	else if(idx=='10')
	{
		document.getElementById('filter_13').style.display="block";
	}
	else if(idx=='11')
	{
		document.getElementById('filter_14').style.display="block";
	}
	else if(idx=='12')
	{
		document.getElementById('filter_15').style.display="block";
	}


	if(idx=='1')
	{
		document.getElementById('filter_17').style.display="block";
		document.getElementById('filter_16').style.display="none";
	}
	else
	{
		document.getElementById('filter_17').style.display="none";
		document.getElementById('filter_16').style.display="block";
	}

	change_perfect_for_search(idx);
}


function change_perfect_for_search(idx)
{
	var httppf;
	url=''+$baseURL+'merchantaccount/getperfectforsearch/';
	if(window.XMLHttpRequest){
		httppf=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httppf=new ActiveXObject('msxml2.XMLHTTP');
	}
	if(httppf){
		httppf.open('post',url,true);
		httppf.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httppf.send("bt="+idx+'&time='+Math.random());
		httppf.onreadystatechange=function(){
			if(httppf.readyState==4){
				if(httppf.status==200){
					//alert(httppf.responseText);
					document.getElementById('submenu_nav5').innerHTML = httppf.responseText;
				}
			}
		}
	}
}



function addto_shortlist(offer_id)
{
	//alert(offer_id);
	var httpadd;
	url=''+$baseURL+'home/addtoshortlist';
	if(window.XMLHttpRequest){
		httpadd=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpadd=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpadd){
		httpadd.open('post',url,true);
		httpadd.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpadd.send("oid="+offer_id+'&time='+Math.random());
		httpadd.onreadystatechange=function(){
			if(httpadd.readyState==4){
				if(httpadd.status==200){
					if(httpadd.responseText=='added')
					{
						document.getElementById('anc_removefrom_shortlist_'+offer_id).style.display="block";
						document.getElementById('anc_addto_shortlist_'+offer_id).style.display="none";
					}
				}
			}
		}
	}
}

function removefrom_shortlist(offer_id)
{
	//alert(offer_id);
	var httpremove;
	url=''+$baseURL+'home/removefromshortlist';
	if(window.XMLHttpRequest){
		httpremove=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpremove=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpremove){
		httpremove.open('post',url,true);
		httpremove.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpremove.send("oid="+offer_id+'&time='+Math.random());
		httpremove.onreadystatechange=function(){
			if(httpremove.readyState==4){
				if(httpremove.status==200){
					//alert(httpremove.responseText);
					if(httpremove.responseText=='removed')
					{
						document.getElementById('anc_removefrom_shortlist_'+offer_id).style.display="none";
						document.getElementById('anc_addto_shortlist_'+offer_id).style.display="block";
					}
				}
			}
		}
	}
}

function validatebusinessprofile()
{
	if(checkBlankField(document.getElementById('bp_business_name').value) == false)
	{
		alert("Please enter your business name!");
		document.getElementById('bp_business_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('bp_website_address').value) == false)
	{
		alert("Please enter your website address!");
		document.getElementById('bp_website_address').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('bp_street_address').value) == false)
	{
		alert("Please enter your street address!");
		document.getElementById('bp_street_address').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('bp_suburb').value) == false)
	{
		alert("Please enter your suburb!");
		document.getElementById('bp_suburb').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('bp_post_code').value) == false)
	{
		alert("Please enter your post code!");
		document.getElementById('bp_post_code').focus();
		return false;
	}
	if(checkBlankField(document.bp_form.city.value) == false)
	{
		alert("Please select your city!");
		document.bp_form.city.focus();
		return false;
	}
	if(checkBlankField(document.bp_form.state.value) == false)
	{
		alert("Please select your state!");
		document.bp_form.state.focus();
		return false;
	}
	if(checkBlankField(document.getElementById('bp_business_phone').value) == false)
	{
		alert("Please enter your mobile or landline number!");
		document.getElementById('bp_business_phone').focus();
		return false;
	}
	if(document.getElementById('bp_mobile_number').style.display=="block" && checkBlankField(document.getElementById('bp_mobile_number').value) == false)
	{
		alert("Please enter your mobile number!");
		document.getElementById('bp_mobile_number').focus();
		return false;
	}
	if(checkBlankField(document.bp_form.price_guide.value) == false)
	{
		alert("Please select your price guide!");
		document.bp_form.price_guide.focus();
		return false;
	}
	/*if(checkBlankField(document.bp_form.perfect_for.value) == false)
	{
		alert("Please select your price for!");
		document.bp_form.perfect_for.focus();
		return false;
	}*/
	/*alert('dfsdf');
	alert(document.bp_form.temp_profile_picture.value);
	alert(document.bp_form.old_profile_picture.value);
	if(document.bp_form.temp_profile_picture.value == "" && document.bp_form.old_profile_picture.value == "")
	{
		alert("Please upload the profile picture!");
		return false;
	}*/

	var buss_hours_monday = 0;
	var buss_hours_tuesday = 0;
	var buss_hours_wednesday = 0;
	var buss_hours_thursday = 0;
	var buss_hours_friday = 0;
	var buss_hours_saturday = 0;
	var buss_hours_sunday = 0;
	for(var i=1;i<=7;i++)
	{
		if(document.getElementById("oh_"+i+"_mon").checked)
		{
			buss_hours_monday++;
		}
		if(document.getElementById("oh_"+i+"_tue").checked)
		{
			buss_hours_tuesday++;
		}
		if(document.getElementById("oh_"+i+"_wed").checked)
		{
			buss_hours_wednesday++;
		}
		if(document.getElementById("oh_"+i+"_thu").checked)
		{
			buss_hours_thursday++;
		}
		if(document.getElementById("oh_"+i+"_fri").checked)
		{
			buss_hours_friday++;
		}
		if(document.getElementById("oh_"+i+"_sat").checked)
		{
			buss_hours_saturday++;
		}
		if(document.getElementById("oh_"+i+"_sun").checked)
		{
			buss_hours_sunday++;
		}
	}
	if(buss_hours_monday>1 || buss_hours_tuesday>1 || buss_hours_wednesday>1 || buss_hours_thursday>1 || buss_hours_friday>1 || buss_hours_saturday>1 || buss_hours_sunday>1)
	{
		alert("Please check your opening hours days.");
		return false;
	}
}

function show_rating_popup(offer_id)
{	
	document.getElementById('rating_box').style.display="block";
	document.getElementById('rating_offer_id').value = offer_id;
}

function close_rating_popup()
{	
	document.getElementById('rating_offer_id').value = "";
	document.getElementById('rating_box').style.display="none";
}

function validatebookingform()
{
	if(checkBlankField(document.booking_form.number_of_people.value) == false)
	{
		alert("Please enter number of people!");
		document.booking_form.number_of_people.focus();
		return false;
	}
	if(checkBlankField(document.booking_form.booking_date.value) == false)
	{
		alert("Please enter booking date!");
		document.booking_form.booking_date.focus();
		return false;
	}
	if(checkBlankField(document.booking_form.booking_time.value) == false)
	{
		alert("Please select booking time!");
		document.booking_form.booking_time.focus();
		return false;
	}
}

function map_view(s)
{
	document.getElementById('mapsearchresults').style.display="block";
	document.getElementById('map_iframe').src=""+$baseURL+"search_map.php?s="+s;
	document.getElementById('listsearchresults').style.display="none";
	document.getElementById('div_sorted_by').style.display="none";
}
function list_view()
{
	document.getElementById('mapsearchresults').style.display="none";
	document.getElementById('listsearchresults').style.display="block";
	document.getElementById('div_sorted_by').style.display="block";
}

function ajax_ratings(rating)
{
	if(rating == '0')
	{
		alert("Please provide your ratings.");
	}
	else
	{
		var offer_id = document.getElementById('rate_offer_id').value;
		var httprate;
		url=''+$baseURL+'customeraccount/ajaxratings';
		if(window.XMLHttpRequest){
			httprate=new XMLHttpRequest();
		}else if(window.ActiveXObject){
			httprate=new ActiveXObject('msxml2.XMLHTTP');
		}		
		if(httprate){
			httprate.open('post',url,true);
			httprate.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			httprate.send('oid='+offer_id+'&rating='+rating+'&time='+Math.random());
			httprate.onreadystatechange=function(){
				if(httprate.readyState==4){
					if(httprate.status==200){
						//alert(httprate.responseText);
						if(httprate.responseText=="success")
						{
							//alert("Successfully rated!");
							document.getElementById('dialog_success').style.display="block";
							document.getElementById('dialog').style.display="none";
							//window.location.href = ''+$baseURL+'customeraccount/mybookings';
						}
						else
						{
							alert("An error occured. Please try again!");	
						}
					}
				}
			}
		}
	}
}

function ajaxGetOfferTimingsByDate(booking_date, offer_id)
{
	//alert(booking_date);
	var httptimings;
	url=''+$baseURL+'booking/ajaxtimings';
	//alert(url);
	//alert(offer_id);
	//alert(booking_date);
	if(window.XMLHttpRequest){
		httptimings=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httptimings=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httptimings){
		httptimings.open('post',url,true);
		httptimings.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httptimings.send('oid='+offer_id+'&booking_date='+booking_date+'&time='+Math.random());
		httptimings.onreadystatechange=function(){
			if(httptimings.readyState==4){
				if(httptimings.status==200){
					//alert(httptimings.responseText);
					if(httptimings.responseText != '')
					{
						document.getElementById('li_timings').innerHTML = httptimings.responseText;
						$(".revostyled").selectbox();
					}
				}
			}
		}
	}
}

function remove_shortlist_home(oid)
{
	window.location.href = ''+$baseURL+'home/removeshortlisthome/?oid='+oid;
}

function change_position(ps)
{
	if(ps=='-1')
	{	
		document.getElementById('li_position_other').style.display="block";
	}
	else
	{
		document.getElementById('li_position_other').style.display="none";
	}
}


function checkdate(input)
{
	var validformat=/^\d{2}\/\d{2}\/\d{4}$/ //Basic check for format validity
	var returnval=false
	if (!validformat.test(input.value))
	returnval=false
	else{ //Detailed check for valid date ranges
	var dayfield=input.value.split("/")[0]
	var monthfield=input.value.split("/")[1]
	var yearfield=input.value.split("/")[2]
	var dayobj = new Date(yearfield, monthfield-1, dayfield)
	if ((dayobj.getMonth()+1!=monthfield)||(dayobj.getDate()!=dayfield)||(dayobj.getFullYear()!=yearfield))
	returnval=false
	else
	returnval=true
	}
	return returnval
}


function validatecustomerinfo()
{
	if(checkBlankField(document.getElementById('cinfo_first_name').value) == false)
	{
		alert("Please enter your first name!");
		document.getElementById('cinfo_first_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('cinfo_last_name').value) == false)
	{
		alert("Please enter your last name!");
		document.getElementById('cinfo_last_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('cinfo_email').value) == false)
	{
		alert("Please enter your email!");
		document.getElementById('cinfo_email').focus();
		return false;
	}
	else if(valid_email(document.getElementById('cinfo_email').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('cinfo_email').focus();
		return false
	}

	if(checkBlankField(document.getElementById('cinfo_mobile_number').value) == false)
	{
		alert("Please enter your mobile number!");
		document.getElementById('cinfo_mobile_number').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('cinfo_postcode').value) == false)
	{
		alert("Please enter your post code!");
		document.getElementById('cinfo_postcode').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('gender').value) == false)
	{
		alert("Please select gender!");
		return false;
	}

	if(checkBlankField(document.getElementById('datepicker5').value) != false)
	{
		if(!checkdate(document.getElementById('datepicker5')))
		{
			alert("Please enter date in DD/MM/YYYY format");
			document.getElementById('datepicker5').focus();
			return false;
		}
	}
}



function change_lyb_categories(val, chk)
{
	var returnstring = document.getElementById('div_categories').innerHTML;
	if(returnstring =='Category') returnstring = '';
	returnstring = returnstring.replace(new RegExp("&amp;", 'g'), "&");
	var arr = returnstring.split(", ");
	if(chk)
	{
		if(returnstring != '')
		{
			var a = returnstring.indexOf(val); 
			if(a == '-1')
			{
				arr.push(val);
			}
		}
		else
		{
			arr.push(val);
		}
	}
	else
	{
		var a = returnstring.indexOf(val); 
		if(a != '-1')
		{
			for (var i = 0; i < arr.length; i++) {
			   var c = arr[i];
			   if (c == val || (val.equals && val.equals(c))) {
				  arr.splice(i, 1);
				  break;
			   }
			}	
		}
	}
	if(arr[0]=="")  arr.splice(0, 1);

	var returnstring = arr.join(", "); 
	if(returnstring=="") returnstring = 'Category';
	document.getElementById('div_categories').innerHTML = returnstring;
}

function validatepaymentoptions()
{
	if(document.getElementById('user_saved_cc'))
	{
		if(document.getElementById('user_saved_cc').checked)
		{
			if(checkBlankField(document.getElementById('saved_cc').value) == false)
			{
				alert("Please select the credit card!");
				document.getElementById('saved_cc').focus();
				return false;
			}
			else
			{
				return true;
			}
		}
	}
	
	if(checkBlankField(document.getElementById('cc_num').value) == false)
	{
		alert("Please enter card number!");
		document.getElementById('cc_num').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('cc_owner').value) == false)
	{
		alert("Please enter card owner name!");
		document.getElementById('cc_owner').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('exp_month').value) == false)
	{
		alert("Please select card expiration month!");
		document.getElementById('exp_month').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('exp_year').value) == false)
	{
		alert("Please select card expiration year!");
		document.getElementById('exp_year').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('cvv').value) == false)
	{
		alert("Please enter cvv code!");
		document.getElementById('cvv').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('cc_address').value) == false)
	{
		alert("Please enter address!");
		document.getElementById('cc_address').focus();
		return false;
	}

	if(checkBlankField(document.getElementById('cc_city').value) == false)
	{
		alert("Please enter city!");
		document.getElementById('cc_city').focus();
		return false;
	}


	if(checkBlankField(document.getElementById('cc_country').value) == false)
	{
		alert("Please enter country!");
		document.getElementById('cc_country').focus();
		return false;
	}


	if(document.getElementById('save_card').checked)
	{
		if(checkBlankField(document.getElementById('cc_category').value) == false)
		{
			alert("Please select card category!");
			document.getElementById('cc_category').focus();
			return false;
		}
	}
	
}

function trim_value(obj)
{
	var vvv = obj.value;
	obj.value = vvv.trim();
}


function validatecontactform()
{
	if(checkBlankField(document.ct_form.contact_name.value) == false || document.ct_form.contact_name.value == "*Contact Name")
	{
		alert("Please enter contact name!");
		document.ct_form.contact_name.focus();
		return false;
	}
	if(checkBlankField(document.ct_form.email_address.value) == false || document.ct_form.email_address.value == "*Email Address")
	{
		alert("Please enter email address!");
		document.ct_form.email_address.focus();
		return false;
	}
	else
	{
		if(valid_email(document.ct_form.email_address.value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.ct_form.email_address.focus();
			return false
		}
	}
	if(checkBlankField(document.ct_form.contact_message.value) == false || document.ct_form.contact_message.value == "*Message")
	{
		alert("Please enter message!");
		document.ct_form.contact_message.focus();
		return false;
	}
}

function validatecommentformloggedin()
{
	if(checkBlankField(document.ct_form.comment.value) == false || document.ct_form.comment.value == "*Comment")
	{
		alert("Please enter comment!");
		document.ct_form.comment.focus();
		return false;
	}
}

function validatecommentformnonloggedin()
{
	if(checkBlankField(document.ct_form.uname.value) == false || document.ct_form.uname.value == "*Name")
	{
		alert("Please enter name!");
		document.ct_form.uname.focus();
		return false;
	}
	if(checkBlankField(document.ct_form.email.value) == false || document.ct_form.email.value == "*Email address")
	{
		alert("Please enter email!");
		document.ct_form.email.focus();
		return false;
	}
	else
	{
		if(valid_email(document.ct_form.email.value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.ct_form.email.focus();
			return false
		}	
	}
	if(checkBlankField(document.ct_form.comment.value) == false || document.ct_form.comment.value == "*Comment")
	{
		alert("Please enter comment!");
		document.ct_form.comment.focus();
		return false;
	}
}

function gotoreply(pr)
{
	document.ct_form.parent_comment_id.value = pr;
	document.ct_form.comment.focus();
}


function check_twitter_email_form()
{
	if(checkBlankField(document.twitter_email_form.twitter_email.value) == false || document.twitter_email_form.twitter_email.value == "Email address")
	{
		alert("Please enter email!");
		document.twitter_email_form.twitter_email.focus();
		return false;
	}
	else
	{
		if(valid_email(document.twitter_email_form.twitter_email.value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.twitter_email_form.twitter_email.focus();
			return false
		}	
	}
}


function use_existing_cc()
{
	
	var bool = document.getElementById('user_saved_cc').checked;
	
	if(!bool)
	{
		document.getElementById('ul_new_cc').style.display="none";
		document.getElementById('ul_saved_cc').style.display="block";
	}
	else
	{
		document.getElementById('ul_new_cc').style.display="block";
		document.getElementById('ul_saved_cc').style.display="none";
	}
}

function validatereviewform()
{
	//document.getElementById('loading-image').style.display="block";
	return true;
}

function change_oh(val, chk, idx)
{
	var currdays = document.getElementById('date_'+idx).innerHTML;
	currdays = currdays.replace("Day", "");
	var res = currdays.split(","); 
	if(chk)
	{
		var a = res.indexOf(val);
		if(a=='-1')
		{
			res.push(val);
		}
		if(currdays != "")
		{
			var newstr = res.join(",");
		}
		else
		{
			var newstr = val;
		} 
	}
	else
	{
		var a = res.indexOf(val);
		if (a > -1) {
			res.splice(a, 1);
		}
		var newstr = res.join(",");
	}
	if(newstr == '')
	{
		newstr = "Day";
	}
	document.getElementById('date_'+idx).innerHTML = newstr;
}

function validaterestpwdform()
{
	if(checkBlankField(document.ct_form.reset_pwd.value) == false)
	{
		alert("Please enter password!");
		document.ct_form.reset_pwd.focus();
		return false;
	}
	if(checkBlankField(document.ct_form.reset_cpwd.value) == false)
	{
		alert("Please confirm password!");
		document.ct_form.reset_cpwd.focus();
		return false;
	}
	if(document.ct_form.reset_pwd.value != document.ct_form.reset_cpwd.value)
	{
		alert("Password not matched!");
		document.ct_form.reset_cpwd.focus();
		return false;
	}
}