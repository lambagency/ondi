function valid_email(eml)
{
	//declare the required variables
	var mint_len;
	var mstr_eml=eml;
	var mint_at=0;
	var mint_atnum=0;
	var mint_dot=0;
	var mint_dotnum=0;

	mint_len = eml.length; //takes the length of the email address entered
	//checking for the symbol single quote. If found replace it with its html code
	if (mstr_eml.indexOf("'")!=-1)
	{	
		mstr_eml=mstr_eml.replace("'","'");
	}
	//checking for the (@) & (.) symbol
	for(var iloop=0;iloop<mint_len;iloop++)
	{
		if(mstr_eml.charAt(iloop)=="@")
		{
			mint_at=iloop+1;
			mint_atnum=mint_atnum+1;
		}
		if(mstr_eml.charAt(iloop)==".")
		{
			mint_dot=iloop+1;
			mint_dotnum=mint_dotnum+1;
		}
	}
	//if nothing entered in the field
	if (mstr_eml=="")
	{
		return true;
	}
	//if @ entered more than once & dot (.) entered more than 4 times
	else if((mint_atnum!=1)||(mint_dotnum>4)||((mint_dot-mint_at)<2)||((mint_len-mint_dot)<2)||(mint_at<3))
	{
		return true;
	}
	//if any blank space is entered in the email address
	else if (mstr_eml.indexOf(" ")!=-1)
	{
		return true;
	}
	return false;
}
function checkBlankField (txt)
{
	var mint_txt = txt.length;
	var mstr_txt = txt;
	var mint_count = 0;
	for (var iloop = 0; iloop<mint_txt; iloop++)
	{
        if (mstr_txt.charAt(iloop) == " ")
        {
           mint_count = mint_count+1;
        }
	}    
// if nothing entered in the field
	if (txt == "")
   	{
		return false;
	}
	else if (mint_count == mint_txt)
	{
		return false;
	}
	return true;
}
function IsNumeric(strString)
{
	var strValidChars = "0123456789.";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) return false;
		for (i = 0; i < strString.length && blnResult == true; i++)
		{
			strChar = strString.charAt(i);
			if (strValidChars.indexOf(strChar) == -1)
			{
			blnResult = false;
			}
		}
   		return blnResult;
 }

function GetXmlHttpObject()
{
	if (window.XMLHttpRequest)
	{
	// code for IE7+, Firefox, Chrome, Opera, Safari
	return new XMLHttpRequest();
	}
	if (window.ActiveXObject)
	{
	// code for IE6, IE5
	return new ActiveXObject("Microsoft.XMLHTTP");
	}
	return null;
}
function uncheckall(divid)
{
	var collection = document.getElementById(divid).getElementsByTagName('input');
	for (var x=0; x<collection.length; x++) {
		if (collection[x].type.toUpperCase()=='CHECKBOX')
		collection[x].checked = false;
	}
}
function add_another_social_media()
{
	var num_extra_social = document.getElementById('num_extra_social').value;
}
function show_more_social()
{
	document.getElementById('li_more_social').style.display="block";
}
function showsocial(val)
{
	if(val == '3')
	{
		document.getElementById('li_linkedin').style.display="block";
	}
	if(val == '4')
	{
		document.getElementById('li_googleplus').style.display="block";
	}

	if ($('#li_linkedin').css('display')=='block' && $('#li_googleplus').css('display')=='block') {
		$('#li_show_more_social').css("display", "none");
	} else {
		$('#li_show_more_social').css("display", "block");
	}
}
function choose_business_type(val, ischecked)
{
	var choosen_business_types = document.getElementById('choosen_business_types').value;
	if(ischecked)
	{
		if(choosen_business_types != '')
		{
			var array_business_types = choosen_business_types.split(","); 
			var a = array_business_types.indexOf(val); 
			if(a === -1)
			{
				array_business_types.push(val);
			}
			var array_length = array_business_types.length;
			if(array_length >1)
			{
				var new_business_types = array_business_types.join(); 
			}
			else
			{
				var new_business_types = val; 
			}
		}
		else
		{
			var new_business_types = val; 
		}
	}
	else
	{
		if(choosen_business_types != '')
		{
			var array_business_types = choosen_business_types.split(","); 
			var index = array_business_types.indexOf(val);
			if (index > -1) {
				array_business_types.splice(index, 1);
			}
			var new_business_types = array_business_types.join(); 
		}
	}
	document.getElementById('choosen_business_types').value = new_business_types;
	//alert(new_business_types);
	// AJAX CALL
	var httpj1;
	url=''+$baseURL+'merchantaccount/getservices/';
	if(window.XMLHttpRequest){
		httpj1=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpj1=new ActiveXObject('msxml2.XMLHTTP');
	}
	if(httpj1){
		httpj1.open('post',url,true);
		httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpj1.send("bt="+new_business_types+'&time='+Math.random());
		httpj1.onreadystatechange=function(){
			if(httpj1.readyState==4){
				if(httpj1.status==200){
					//alert(httpj1.responseText);
					document.getElementById('td_service_offered').innerHTML = httpj1.responseText;
					document.getElementById('num_services_offered').value = '1';
					$(".revostyled").selectbox();
				}
			}
		}
	}
}

function show_more_open_time()
{
	var num_open_time = parseInt(document.getElementById('num_open_time').value);
	num_open_time = num_open_time + 1;
	document.getElementById('opening_hours_'+num_open_time).style.display="block";
	document.getElementById('num_open_time').value = num_open_time;
	if(num_open_time == 7)
	{
		document.getElementById('anc_show_more_open_time').style.display="none";
	}
}

function add_more_service_offered()
{
	var num_services_offered = parseInt(document.getElementById('num_services_offered').value);
	num_services_offered = num_services_offered + 1;
	document.getElementById('ul_service_offered_'+num_services_offered).style.display="block";
	document.getElementById('num_services_offered').value = num_services_offered;
	if(num_services_offered == 10)
	{
		document.getElementById('anc_add_more_service_offered').style.display="none";
	}
}
function show_access_options(id,access_options)
{				
		
	if(id=='3'){
		var httpj1;
		url=''+$baseURL+'merchantaccount/getaccessoptions';
		if(window.XMLHttpRequest){
			httpj1=new XMLHttpRequest();
		}else if(window.ActiveXObject){
			httpj1=new ActiveXObject('msxml2.XMLHTTP');
		}		
		if(httpj1){
			httpj1.open('post',url,true);
			httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			//httpj1.send("type_id="+id+'&time='+Math.random());
			httpj1.send("type_id="+id+ '&access_options='+access_options+'&time='+Math.random());
			httpj1.onreadystatechange=function(){
				if(httpj1.readyState==4){
					if(httpj1.status==200){
						//alert(httpj1.responseText);
						document.getElementById('access_options_id').innerHTML = httpj1.responseText;
						
					}
				}
			}
		}
	}else if(document.getElementById('access_options_id')){
		document.getElementById('access_options_id').innerHTML = "";
	}
}

function anc_add_more_cards()
{
	var num_cards = parseInt(document.getElementById('num_cards').value);
	num_cards = num_cards + 1;
	document.getElementById('ul_cards_'+num_cards).style.display="block";
	document.getElementById('num_cards').value = num_cards;
	if(num_cards == 10)
	{
		document.getElementById('anc_add_more_cards').style.display="none";
	}
}
function remove_shortlist(oid)
{
	/*var conf = confirm("Do you want to remove it from the shortlists?");
	if(conf)
	{
		window.location.href = ''+$baseURL+'customeraccount/removeshortlist/?oid='+oid;
	}*/
	window.location.href = ''+$baseURL+'customeraccount/removeshortlist/?oid='+oid;
}
function unsubscribefromall()
{
	var chk = document.getElementById('unsubscribefromall').checked;
	if(!chk)
	{
		var collection = document.getElementById('checkboxesdiv').getElementsByTagName('input');
		for (var x=0; x<collection.length; x++) {
			if (collection[x].type.toUpperCase()=='CHECKBOX')
			collection[x].checked = false;
		}

		var collection2 = document.getElementById('checkboxesdiv').getElementsByClassName('styledCheckbox');
		for (var y=0; y<collection2.length; y++) {
			collection2[y].style.backgroundPosition="0px 0px";
		}
	}
}

function resetunsubscribe(obj)
{
	document.getElementById(obj).value = '1';
	var collection = document.getElementsByClassName('upper');
	var collection2 = collection[0].getElementsByClassName('styledCheckbox');
	collection2[0].style.backgroundPosition="0px 0px";
}

function isNumberKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if (charCode > 31 && (charCode < 48 || charCode > 57))
	{
		return false;
	}
	else
	{
		return true;
	}
}

function calculatetotal(val)
{
	if(val != '')
	{
		var offer_price = parseFloat(document.getElementById('offerprice').value);
		var total = parseInt(val)* offer_price;
		document.getElementById('total_price').value = total;
	}
	else
	{
		document.getElementById('total_price').value = '';
	}
}

function calculatediscount(code)
{				
	var httpdiscount;
	url=''+$baseURL+'booking/getdiscount';
	if(window.XMLHttpRequest){
		httpdiscount=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpdiscount=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpdiscount){
		httpdiscount.open('post',url,true);
		httpdiscount.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpdiscount.send("code="+code+'&time='+Math.random());
		httpdiscount.onreadystatechange=function(){
			if(httpdiscount.readyState==4){
				if(httpdiscount.status==200){
					//alert(httpdiscount.responseText);
					if(httpdiscount.responseText != '')
					{
						document.getElementById('lidiscount').style.display="block";
						document.getElementById('discount').value = httpdiscount.responseText;
						//alert(httpdiscount.responseText);
						if(document.getElementById('total_price').value != '')
						{
							var old_total = parseFloat(document.getElementById('total_price').value);
							var discount = parseFloat(document.getElementById('discount').value);
							var new_price = old_total - discount;
							document.getElementById('total_price').value = new_price;
						}
					}
					else
					{
						var old_total = parseFloat(document.getElementById('total_price').value);
						if(document.getElementById('discount').value != '')
						{
							var discount = parseFloat(document.getElementById('discount').value);
						}
						else
						{
							var discount = 0;
						}
						var new_price = old_total + discount;
						document.getElementById('total_price').value = new_price;
						document.getElementById('lidiscount').style.display="none";
					}
				}
			}
		}
	}
}
function changeusertype(type)
{
	if(type=='1')
	{
		document.getElementById('ul_returninguser').style.display="none";
		document.getElementById('ul_guestuser').style.display="block";
	}
	else
	{
		document.getElementById('ul_returninguser').style.display="block";
		document.getElementById('ul_guestuser').style.display="none";
	}
}
function validatecheckout()
{
	if(document.getElementById('guest').checked)
	{
		if(checkBlankField(document.getElementById('guest_email').value) == false)
		{
			alert("Please enter your email!");
			document.getElementById('guest_email').focus();
			return false;
		}
		if(valid_email(document.getElementById('guest_email').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('guest_email').focus();
			return false
		}
	}
	else
	{
		if(checkBlankField(document.getElementById('returning_email').value) == false)
		{
			alert("Please enter your email!");
			document.getElementById('returning_email').focus();
			return false;
		}
		if(valid_email(document.getElementById('returning_email').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('returning_email').focus();
			return false
		}
		if(checkBlankField(document.getElementById('returning_password').value) == false)
		{
			alert("Please enter your password!");
			document.getElementById('returning_password').focus();
			return false;
		}
	}
	//return false;
}
function changepaymentmethod(method)
{
	if(method == '1')
	{
		document.getElementById('li_cc').className = 'active';
		document.getElementById('li_pp').className = '';
		document.getElementById('li_mp').className = '';
	}
	else if(method == '2')
	{
		document.getElementById('li_cc').className = '';
		document.getElementById('li_pp').className = 'active';
		document.getElementById('li_mp').className = '';
	}
	else if(method == '3')
	{
		document.getElementById('li_cc').className = '';	
		document.getElementById('li_pp').className = '';
		document.getElementById('li_mp').className = 'active';
	}
}
function viewmoreaboutorder(oid)
{
	if(document.getElementById('moredetailorder_'+oid).style.display=="none")
	{
		document.getElementById('moredetailorder_'+oid).style.display= "block";
		document.getElementById('ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/cus_share1.png" alt="" />';
	}
	else
	{
		document.getElementById('moredetailorder_'+oid).style.display= "none";
		document.getElementById('ancviewmoreaboutorder_'+oid).innerHTML = '<img src="'+$baseURL+'public/images/up_cus_share1.png" alt="" />';
	}
}


function sortrecord(column, type, orderby)
{				
	var httpsort;
	url=''+$baseURL+'customeraccount/sortrecord';
	if(window.XMLHttpRequest){
		httpsort=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpsort=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpsort){
		httpsort.open('post',url,true);
		httpsort.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		if(type == 'current')
		{
			httpsort.send('column='+column+'&type='+type+'&orderby='+orderby+'&time='+Math.random());
		}
		else
		{
			var keyword = document.getElementById('keyword').value;
			var filter_month = document.getElementById('filter_month').value;
			var datepicker = document.getElementById('datepicker').value;
			var datepicker2 = document.getElementById('datepicker2').value;

			httpsort.send('column='+column+'&type='+type+'&orderby='+orderby+'&keyword='+keyword+'&filter_month='+filter_month+'&date_from='+datepicker+'&date_to='+datepicker2+'&time='+Math.random());
		}
		httpsort.onreadystatechange=function(){
			if(httpsort.readyState==4){
				if(httpsort.status==200){
					//alert(httpsort.responseText);
					if(type == 'current')
					{
						document.getElementById('current_booking').innerHTML = httpsort.responseText;
					}
					else
					{
						document.getElementById('historydiv').innerHTML = httpsort.responseText;
					}
				}
			}
		}
	}
}
function isNumberKey(evt, obj)
{
	var charCode = (evt.which) ? evt.which : event.keyCode;
	if(charCode != 58)
	{
		if (charCode > 31 && (charCode < 48 || charCode > 57))
		{
			return false;
		}
	}
	if(charCode == 8)
	{
		return true;
	}
	var val = document.getElementById(obj).value;
	var len = val.length;
	if(len==2)
	{
		document.getElementById(obj).value = document.getElementById(obj).value + ":";
	}	
	if(len>=5)
	{
		return false;
	}
	return true;
}

function checksubsnewsletter()
{
	if(checkBlankField(document.getElementById('nl_email').value) == false)
	{
		alert("Please enter your email!");
		document.getElementById('nl_email').focus();
		return false;
	}
	else if(valid_email(document.getElementById('nl_email').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('nl_email').focus();
		return false
	}
	if(checkBlankField(document.getElementById('nl_gender').value) == false)
	{
		alert("Please choose your gender!");
		document.getElementById('nl_gender').focus();
		return false;
	}
	ajaxSubscribeNewsletter();
	return false;
}

function ajaxSubscribeNewsletter()
{				
	var nl_email = document.getElementById('nl_email').value;
	var nl_gender = document.getElementById('nl_gender').value;
	var httpnl;
	//alert(nl_email);
	url=''+$baseURL+'home/subscribenewsletter';
	if(window.XMLHttpRequest){
		httpnl=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpnl=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpnl){
		httpnl.open('post',url,true);
		httpnl.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpnl.send('nl_email='+nl_email+'&nl_gender='+nl_gender+'&time='+Math.random());
		httpnl.onreadystatechange=function(){
			if(httpnl.readyState==4){
				if(httpnl.status==200){
					alert(httpnl.responseText);
				}
			}
		}
	}
}

function showcontentofsection(sectionname)
{
	document.getElementById('content_'+sectionname).style.visibility="visible";
	document.getElementById('content_'+sectionname).style.height="auto";
	document.getElementById('content_'+sectionname).style.overflow="visible";
}

function deletecc(cid)
{
	window.location.href = ''+$baseURL+'customeraccount/removecc/?cid='+cid;
}

function change_confirmation_methods(val, chk)
{
	var returnstring = '';
	if(document.getElementById('confirmation_methods_sms').checked)
	{
		returnstring += 'SMS, ';
	}
	if(document.getElementById('confirmation_methods_email').checked)
	{
		returnstring += 'Email, ';
	}
	if(document.getElementById('confirmation_methods_phone').checked)
	{
		returnstring += 'Phone, ';
	}
	if(returnstring != '')
	{
		returnstring = returnstring.substring(0, returnstring.length - 2); 
		document.getElementById('div_confirmation_methods').innerHTML = returnstring;
	}
	else
	{
		document.getElementById('div_confirmation_methods').innerHTML = 'Please choose at least 1';
	}
	
}

function listyourbusiness_validate()
{
	if(checkBlankField(document.getElementById('business_name').value) == false)
	{
		alert("Please enter your business name!");
		document.getElementById('business_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_website_address').value) == false)
	{
		alert("Please enter your website address!");
		document.getElementById('business_website_address').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_street_address').value) == false)
	{
		alert("Please enter your street address!");
		document.getElementById('business_street_address').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_category_1').value) == false)
	{
		alert("Please select your business category!");
		document.getElementById('business_category_1').focus();
		return false;
	}
	/*
	var num_category = parseInt(document.getElementById('num_category').value);
	for(var i=1; i<=num_category; i++)
	{
		if(checkBlankField(document.getElementById('business_category_'+i).value) == false)
		{
			alert("Please select your business category!");
			document.getElementById('business_category_'+i).focus();
			return false;
		}
	}
	*/
	if(checkBlankField(document.getElementById('business_contact_name').value) == false)
	{
		alert("Please enter your contact name!");
		document.getElementById('business_contact_name').focus();
		return false;
	}
	if(checkBlankField(document.getElementById('business_email_address').value) == false)
	{
		alert("Please enter your email address!");
		document.getElementById('business_email_address').focus();
		return false;
	}
	else if(valid_email(document.getElementById('business_email_address').value)==true)
	{
		alert ( "Please enter the correct email address." );
		document.getElementById('business_email_address').focus();
		return false
	}
	if(checkBlankField(document.getElementById('business_password').value) == false)
	{
		alert("Please enter your password!");
		document.getElementById('business_password').focus();
		return false;
	}
	if(document.getElementById('is_authority').checked == false)
	{
		alert("Do you have authority to register this business?");
		document.getElementById('is_authority').focus();
		return false;
	}
}

function add_more_category()
{
	var num_category = parseInt(document.getElementById('num_category').value);
	num_category = num_category + 1;
	document.getElementById('div_business_category_'+num_category).style.display="block";
	document.getElementById('num_category').value = num_category;
	if(num_category == 10)
	{
		document.getElementById('anc_add_more_category').style.display="none";
	}
}