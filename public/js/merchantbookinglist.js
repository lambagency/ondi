function show_hide_booking_detail(st)
{		
	var total_date_rows = document.getElementById("totalbookingdates").value;
	$('#table_customer_data'+st).toggle();	
	if($('#table_customer_data'+st).css('display') != 'none'){  $('#date_expand_anchor_'+st).text('- Collapse'); }else{ $('#date_expand_anchor_'+st).text('+ Expand'); }	
}
function show_save_order_span(st)
{	

	if($('#edit_customer_order_'+st).length) {

		$('#edit_customer_order_'+st).hide();
	}
	if($('#save_customer_order_'+st).length) {
		
		$('#save_customer_order_'+st).show();
	}

	
	//--------- For Booking calendar start -------------//
	if($('#save_customer_order_li_'+st).length) {
		
		$("#save_customer_order_li_"+st).show();
	}
	if($('#save_customer_order_btn_li_'+st).length) {
		
		$("#save_customer_order_btn_li_"+st).show();
	}
	//--------- For Booking calendar end --------------//

}
function update_booking_info(order_id,offer_id)
{
	var booking_date = document.getElementById("booking_date_"+order_id).value;
	
	//var booking_time = document.getElementById("booking_time_"+order_id).value;

	var booking_time_name = document.getElementsByName("booking_time_"+order_id);
	var booking_time = booking_time_name[0].value;

	var booking_time_ampm = document.getElementById("booking_time_ampm_"+order_id).value;
	if(booking_date!="" && booking_time!="" && booking_time_ampm!="")
	{
		
		
		var booking_data = "";
		booking_data = booking_data + "booking_date=" + booking_date + "&booking_time=" + booking_time + "&booking_time_ampm=" + booking_time_ampm + "&id=" + order_id + "&offer_id=" + offer_id + "&time="+Math.random();		
		
		var httpj1;
		url=''+$baseURL+'merchantaccount/editbookingtime/';
		if(window.XMLHttpRequest){
			httpj1=new XMLHttpRequest();
		}else if(window.ActiveXObject){
			httpj1=new ActiveXObject('msxml2.XMLHTTP');
		}		
		if(httpj1){			
						
			httpj1.open('post',url,true);
			httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");		
			httpj1.send(booking_data);
			
			httpj1.onreadystatechange=function(){			
				//alert(httpj1.readyState);
				if(httpj1.readyState==4){
					if(httpj1.status==200){		
						
						//alert(httpj1.responseText);						
						document.getElementById('booking_upd_msg_'+order_id).innerHTML = httpj1.responseText;
						document.getElementById('booking_upd_msg_'+order_id).style.display="block";
						window.location.reload();					
					}
				}
			}
		}

	}else{
		
		alert("Please check your details.");

	}
	
}
function show_offer_rest_detail(pos)
{	
		
	$('#add_calander_'+pos).toggle();
	if($("#add_calander_"+pos).css('display') == 'block')
	{
		$('#arrow_'+pos).addClass('active');
	} 
	else 
	{
		$('#arrow_'+pos).removeClass('active');	
	}
	
}
function expand_all_div()
{
	
	$('.table_customer_data').css('display','block');	
	$('.add_calander').css('display','block');
	var total_date_rows = $('#totalbookingdates').val();	
	for(var i=1;i<=total_date_rows;i++){ $('#date_expand_anchor_'+i).text('- Collapse'); }

}
function collapse_all_div()
{	
	$('.table_customer_data').css('display','none');	
	$('.add_calander').css('display','none');
	var total_date_rows = $('#totalbookingdates').val();	
	for(var i=1;i<=total_date_rows;i++){ $('#date_expand_anchor_'+i).text('+ Expand'); }
}