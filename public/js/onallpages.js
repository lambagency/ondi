function change_read_status(id)
{
	var httpnotification;
	url=''+$baseURL+'home/changereadstatus';
	if(window.XMLHttpRequest){
		httpnotification=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpnotification=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpnotification){
		httpnotification.open('post',url,true);
		httpnotification.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpnotification.send('id='+id+'&time='+Math.random());
		httpnotification.onreadystatechange=function(){
			//alert(httpnotification.readyState);
			if(httpnotification.readyState==4){
				//alert(httpnotification.status);
				if(httpnotification.status==200){
					//alert(httpnotification.responseText);
					document.getElementById('span_notification_'+id).innerHTML = httpnotification.responseText;
					
					if(httpnotification.responseText =='Mark as unread')
					{
						document.getElementById('span_notification_'+id).className = "normal";
						//document.getElementById('span_notification_'+id).innerHTML = "Mark as unread";
					}
					else
					{
						document.getElementById('span_notification_'+id).className = "bold";
						//document.getElementById('span_notification_'+id).innerHTML = "Mark as read";
					}
					//alert(document.getElementById('span_notification_'+id).className);
					//alert(document.getElementById('span_notification_'+id).innerHTML);
				}
				else
				{
					document.getElementById('span_notification_'+id).innerHTML = "Please wait...";
				}
			}
			else
			{
				document.getElementById('span_notification_'+id).innerHTML = "Please wait...";
			}
		}
	}

}


function change_read_status_detail(id)
{
	var httpnotificationdetail;
	url=''+$baseURL+'home/changereadstatus';
	if(window.XMLHttpRequest){
		httpnotificationdetail=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpnotificationdetail=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpnotificationdetail){
		httpnotificationdetail.open('post',url,true);
		httpnotificationdetail.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpnotificationdetail.send('id='+id+'&time='+Math.random());
		httpnotificationdetail.onreadystatechange=function(){
			if(httpnotificationdetail.readyState==4){
				if(httpnotificationdetail.status==200){
					//alert(httpnotificationdetail.responseText);
					document.getElementById('detail_span_notification_'+id).innerHTML = httpnotificationdetail.responseText;
					if(httpnotificationdetail.responseText =='Mark as unread')
					{
						document.getElementById('detail_span_notification_'+id).className = "normal";
						//document.getElementById('detail_span_notification_'+id).innerHTML = "Mark as unread";
					}
					else
					{
						document.getElementById('detail_span_notification_'+id).className = "bold";
						//document.getElementById('detail_span_notification_'+id).innerHTML = "Mark as read";
					}
				}
				else
				{
					document.getElementById('detail_span_notification_'+id).innerHTML = "Please wait...";
				}
			}
			else
			{
				document.getElementById('detail_span_notification_'+id).innerHTML = "Please wait...";
			}
		}
	}

}

function makeNotificationRequest()
{
	var httpajaxnotification;
	url=''+$baseURL+'home/makenotificationrequest';
	if(window.XMLHttpRequest){
		httpajaxnotification=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpajaxnotification=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpajaxnotification){
		httpajaxnotification.open('post',url,true);
		httpajaxnotification.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpajaxnotification.send('time='+Math.random());
		httpajaxnotification.onreadystatechange=function(){
			if(httpajaxnotification.readyState==4){
				if(httpajaxnotification.status==200){
					//alert(httpajaxnotification.responseText);
					var resptxt = httpajaxnotification.responseText;
					var splittedarr = resptxt.split('~~~~~');
					
					var maybeObject = document.getElementById('num_unreadnotify');
					//alert(maybeObject);
					if (maybeObject != null) 
					{
						var prevnumnotif = parseInt(document.getElementById('num_unreadnotify').innerHTML);
						document.getElementById('li_ajax_notifications').innerHTML = splittedarr[0];
						var newnumnotif = parseInt(splittedarr[1]);
						//alert(splittedarr[0]);
						//alert(prevnumnotif);
						//alert(newnumnotif);
						if(newnumnotif>prevnumnotif)
						{
							var mp3snd 	= "'+$baseURL+'public/beep.mp3";	// MP3 FILE
							var oggsnd 	= "'+$baseURL+'public/beep.ogg";	// OGG FILE
							var audiowidth	= "0"			// WIDTH OF PLAYER
							var borderw	= "2"			// BORDER AROUND PLAYER WIDTH
							var bordcolor	= "F3F3F3"		// BORDER COLOR
							var loopsong	= "no"			// LOOP MUSIC | yes | no |
							var looping5="";
							var loopingE="false";
							
							var str_music = '<table style="border-collapse:collapse; border-spacing: 0; margin: 0; padding: 0; border: #'+bordcolor+' '+borderw+'px solid;background-color: #F0F0F0;"><tr><td style="vertical-align: middle; text-align: center; padding: 0;"><img height="1px" width="1px" src="images/dot.png"/><audio autoplay="autoplay" controls '+looping5+' style="width:'+audiowidth+'px;"><source src="'+mp3snd+'" type="audio/mpeg"><source src="'+oggsnd+'" type="audio/ogg"><object classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" type="application/x-mplayer2" width="'+audiowidth+'" height="45"><param name="filename" value="'+mp3snd+'"><param name="autostart" value="1"><param name="loop" value="'+loopingE+'"></object></audio></td></tr></table>';
							

							document.getElementById('music_player').innerHTML = str_music;
						}
						else
						{
							document.getElementById('music_player').innerHTML = "";
						}
					}

				}
			}
		}
	}
}


function makeFooterAlerts()
{
	//alert('Testing..');
	var httpajaxnotificationfooter;
	url=''+$baseURL+'home/makefooteralerts';
	if(window.XMLHttpRequest){
		httpajaxnotificationfooter=new XMLHttpRequest();
	}else if(window.ActiveXObject){
		httpajaxnotificationfooter=new ActiveXObject('msxml2.XMLHTTP');
	}		
	if(httpajaxnotificationfooter){
		httpajaxnotificationfooter.open('post',url,true);
		httpajaxnotificationfooter.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		httpajaxnotificationfooter.send('time='+Math.random());
		httpajaxnotificationfooter.onreadystatechange=function(){
			if(httpajaxnotificationfooter.readyState==4){
				if(httpajaxnotificationfooter.status==200){
					//alert(httpajaxnotificationfooter.responseText);
					if(httpajaxnotificationfooter.responseText !='')
					{
						document.getElementById('footer_alerts').style.display="block";
						document.getElementById('footer_alerts').innerHTML = httpajaxnotificationfooter.responseText;
					}
				}
			}
		}
	}
}