

$('#offer_running_today_div').click(function () {

    hide_all_opening_hours();




    if ($('#offer_running_today').is(':checked')) {

        show_available_time_slots_and_capacity_li();
        hide_different_days_different_time();
        //$('#main_con_diff_days_time').hide(); // We are hiding Different days at different times when user select Todays date

        $("#dddt_span_id").html("Same day at different times");

    }
    $('#offer_running_from').val("");
    $('#offer_running_to').val("");

    $('#offer_multiple_dates').attr('checked', false);
    $('#offer_multiple_dt_container_div .styledCheckbox').css('background-position', '0px 0px');

    //$('#everyday_of_week').attr('checked', false);
    //$('#container_everyday .styledCheckbox').css('background-position', '0px 0px');
    uncheck_everyday();

    $('#select_specific_days').attr('checked', false);
    $('#container_specific_days .styledCheckbox').css('background-position', '0px -29px');


    $('#offer_running_from').val(todayFormattedDate());

    $("#multidate_from_to_div").show();
    $("#offer_running_from_container").show();
    $("#offer_running_to_container").hide();
    $("#what_day_of_week_li").hide();

    if (if_today_only() == false) {
        $("#offer_running_from_container").hide();
    }

});

$('#offer_multiple_dt_container_div').click(function () {


    uncheck_offer_running_today();
    hide_available_time_slots_and_capacity_li();
    uncheck_everyday();
    uncheck_select_specific_days();

    if ($('#offer_multiple_dates').is(':checked')) {

        $('#offer_running_from').val(todayFormattedDate());
        show_multiple_dates_and_what_day_of_week();
        $("#dddt_span_id").html("Different days at different times");

        $('#offer_running_to').val("");

    } else {

        $("#multidate_from_to_div").hide();
        $("#what_day_of_week_li").hide();

        $('#offer_running_from').val("");
        $('#offer_running_to').val("");

        $('#offer_multiple_dates').attr('checked', false);
        $('#offer_multiple_dt_container_div .styledCheckbox').css('background-position', '0px 0px');
    }


});



function todayFormattedDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}

































function alert_select_specific_days() {
    alert("Please select specific days!");
}
function uncheck_offer_running_today() {
    $('#offer_running_today').attr('checked', false);
    $('#offer_running_today_div .styledCheckbox').css('background-position', '0px 0px');
}
function uncheck_everyday() {
    $('#everyday_of_week').attr('checked', false);
    $('#container_everyday .styledCheckbox').css('background-position', '0px 0px');
}
function uncheck_select_specific_days() {
    $("#select_specific_days").prop('checked', false);
    $('#container_specific_days .styledCheckbox').css('background-position', '0px 0px');

//    hide_container_daywise();
//    for (var i = 1; i <= 7; i++) {
//        $('#daywise_' + i).hide();
//        $('#daywise_' + i + ' .styledCheckbox').css('background-position', '0px 0px');
//        $('#daywise_' + i + ' .specific_days_week_class').prop('checked', false);
//    }
}

function uncheck_different_days() {

    $("#allthe_same_time_div").hide();
    $("#all_day_time_from").val("");
    //document.getElementsByName("all_day_time_from_am_pm")[0].value=""; //12-05-2014
    //document.getElementsByName("all_day_time_to_am_pm")[0].value="";
    $("#all_day_time_to").val("");
    $("#all_day_quantity").val("");
    $("#all_day_max_pax").val("");

    $('#container_all_day_same_time .styledCheckbox').css('background-position', '0px 0px');
    $("#all_day_same_time").prop('checked', false);

}
function uncheck_all_day_same_time() {
    $('#all_day_same_time').attr('checked', false);
    $('#container_all_day_same_time .styledCheckbox').css('background-position', '0px 0px');
    $("#allthe_same_time_div").hide();

}
function uncheck_all_opening_hours() {
    $('#all_opening_hours').attr('checked', false);
    $('#all_opening_hours_left_div .styledCheckbox').css('background-position', '0px 0px');
    $("#all_opening_hours_div").hide();
}


function show_multiple_dates_and_what_day_of_week() {
    $("#multidate_from_to_div").show();

    $("#offer_running_from_container").show();
    $("#offer_running_to_container").show();

    $("#what_day_of_week_li").show();
}
function show_all_the_same_time() {
    $("#allthe_same_time_div").show();
}
function hide_all_the_same_time() {

    $("#allthe_same_time_div").hide();
    $("#all_day_time_from").val("");
    //document.getElementsByName("all_day_time_from_am_pm")[0].value=""; // 12-05-2014
    //document.getElementsByName("all_day_time_to_am_pm")[0].value=""; // 12-05-2014
    $("#all_day_time_to").val("");
    $("#all_day_quantity").val("");
    $("#all_day_max_pax").val("");

    $('#container_all_day_same_time .styledCheckbox').css('background-position', '0px 0px');
    $("#all_day_same_time").prop('checked', false);

}
function show_different_days_different_time() {
    $("#all_diff_days_div").show();
}
function hide_different_days_different_time() {
    $('#main_con_diff_days_time_container .styledCheckbox').css('background-position', '0px 0px');
    $('#different_days_different_time').attr('checked', false);
    $("#all_diff_days_div").hide();
}
function show_all_opening_hours() {
    $("#all_opening_hours_div").show();
}
function hide_all_opening_hours() {
    //if ($('#all_opening_hours').is(':checked')) {

    $('#container_all_opening_hours .styledCheckbox').css('background-position', '0px 0px');
    $('#all_opening_hours').attr('checked', false);
    $("#all_opening_hours_div").hide();
    //}

}
function show_available_time_slots_and_capacity_li() {
    $("#available_time_slots_and_capacity_li").show();
}
function hide_available_time_slots_and_capacity_li() {
    $("#available_time_slots_and_capacity_li").hide();
}
function show_container_daywise() {
    $("#container_daywise").show();

    if (check_blank_multiple_dates()) {

        var offer_running_from = $("#offer_running_from").val();
        var offer_running_to = $("#offer_running_to").val();

        offer_running_from_arr = offer_running_from.split("/");
        offer_running_to_arr = offer_running_to.split("/");

        from_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
        to_date = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

        var days = (to_date - from_date) / (60 * 60 * 24 * 1000);


        if (to_date < from_date) {
            alert("Please check offer dates!");

        } else {

            var weekday = new Array(7);
            weekday[0] = "sunday";
            weekday[1] = "monday";
            weekday[2] = "tuesday";
            weekday[3] = "wednesday";
            weekday[4] = "thursday";
            weekday[5] = "friday";
            weekday[6] = "saturday";

            var monday = 0;
            var tuesday = 0;
            var wednesday = 0;
            var thursday = 0;
            var friday = 0;
            var saturday = 0;
            var sunday = 0;

            while (from_date <= to_date) {

                daysname = weekday[from_date.getDay()];
                if (daysname == 'monday') {
                    monday++;
                } else if (daysname == 'tuesday') {

                    tuesday++;
                }
                else if (daysname == 'wednesday') {

                    wednesday++;
                }
                else if (daysname == 'thursday') {
                    thursday++;
                }
                else if (daysname == 'friday') {
                    friday++;

                }
                else if (daysname == 'saturday') {
                    saturday++;

                }
                else if (daysname == 'sunday') {
                    sunday++;

                }
                from_date.setDate(from_date.getDate() + 1);
            }

            /*
             alert(monday);
             alert(tuesday);
             alert(wednesday);
             alert(thursday);
             alert(friday);
             alert(saturday);
             alert(sunday);
             */

            if (days > 0 || monday > 0 || tuesday > 0 || wednesday > 0 || thursday > 0 || friday > 0 || saturday > 0 || sunday > 0) {
                $("#container_daywise").show();
            }

            if (if_select_specific_days() == true) {

                if (days >= 7) {
                    // display all day divs
                    for (var p = 1; p <= 7; p++) {
                        if ($("#daywise_" + p).length) {
                            $("#daywise_" + p).show();
                        }

                    }

                } else {


                    if (monday > 0) {
                        if ($("#daywise_1").length) {
                            $("#daywise_1").show();
                        }
                    }
                    if (tuesday > 0) {
                        if ($("#daywise_2").length) {
                            $("#daywise_2").show();
                        }

                    }
                    if (wednesday > 0) {
                        if ($("#daywise_3").length) {
                            $("#daywise_3").show();
                        }

                    }
                    if (thursday > 0) {
                        if ($("#daywise_4").length) {
                            $("#daywise_4").show();
                        }

                    }
                    if (friday > 0) {
                        if ($("#daywise_5").length) {
                            $("#daywise_5").show();
                        }

                    }
                    if (saturday > 0) {
                        if ($("#daywise_6").length) {
                            $("#daywise_6").show();
                        }

                    }
                    if (sunday > 0) {
                        if ($("#daywise_7").length) {
                            $("#daywise_7").show();
                        }
                    }

                }


            }


        }


    }

}
function hide_container_daywise() {
    $("#container_daywise").hide();
}

$(function () {


    $('.merchant_newoffer ul li input:checkbox').screwDefaultButtons({
        image: 'url("' + baseUrl + 'public/images/agree.jpg")',
        width: 29,
        height: 29
    });


});
function showhide_different_day() {
    var i;
    for (i = 4; i <= 21; i++) {
        if (document.getElementById(i + "_diff_days_div_id")) {
            if (document.getElementById(i + "_diff_days_div_id").style.display == "none") {
                document.getElementById(i + "_diff_days_div_id").style.display = "block";
                return false;
            }
        }

    }
}

function set_available_day(days, daysview, ids) {
    document.getElementById("available_day_" + ids).value = daysview;
    document.getElementById("available_day_view_" + ids).value = days;

    if (document.getElementById("available_day_option_" + ids)) {
        document.getElementById("available_day_option_" + ids).style.display = "none";
    }
}

function set_available_day_edt(days, daysview, ids, m) {
    //alert(days);
    //alert(daysview);
    //alert(ids);

    document.getElementById("available_day_edt_" + ids).value = daysview;
    document.getElementById("available_day_edt_view_" + ids).value = days;

    if (document.getElementById("available_day_edt_option_" + m)) {
        document.getElementById("available_day_edt_option_" + m).style.display = "none";
    }

}


function showDaysOptions(ids) {
    for (var i = 1; i <= 21; i++) {
        if (document.getElementById("available_day_option_" + i)) {
            document.getElementById("available_day_option_" + i).style.display = "none";
        }
    }

    if (document.getElementById("available_day_option_" + ids)) {
        document.getElementById("available_day_option_" + ids).style.display = "block";
    }
}

function showDaysOptionsEdt(ids) {

    var count_different_days_data = document.getElementById("count_different_days_data").value;
    for (var i = 1; i <= count_different_days_data; i++) {
        if (document.getElementById("available_day_edt_option_" + i)) {
            document.getElementById("available_day_edt_option_" + i).style.display = "none";
        }
    }

    if (document.getElementById("available_day_edt_option_" + ids)) {
        document.getElementById("available_day_edt_option_" + ids).style.display = "block";
    }
}

function today_only_action() {

    if (document.getElementById("offer_multiple_dates")) {
        document.getElementById("offer_multiple_dates").checked = false;

        //document.getElementByClass("styledCheckbox").style.backgroundPosition="0px 0px";
    }
    if (document.getElementById("offer_running_from")) {
        document.getElementById("offer_running_from").value = "";
    }
    if (document.getElementById("offer_running_to")) {
        document.getElementById("offer_running_to").value = "";
    }

}





$('.date_con').click(function () {

    /*
     $('#offer_running_today').attr('checked', false);
     $('#offer_running_today_div .styledCheckbox').css('background-position', '0px 0px');

     $('#offer_multiple_dates').attr('checked', true);
     $('#offer_multiple_dt_container_div .styledCheckbox').css('background-position', '0px -29px');
     */

});

function check_valid_multiple_dates() {

    if (check_blank_multiple_dates()) {

        var offer_running_from = $("#offer_running_from").val();
        var offer_running_to = $("#offer_running_to").val();

        offer_running_from_arr = offer_running_from.split("/");
        offer_running_to_arr = offer_running_to.split("/");

        from_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
        to_date = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

        if (to_date < from_date) {
            //alert("Please check offer dates!");
            return false;

        } else {

            return true;
        }

    }

}

// Action : All the same time
$('#container_all_day_same_time').click(function () {


    if (if_multiple_dates() == true) {
        if (check_valid_multiple_dates() == false) {
            alert("Please check offer dates!");
            hide_all_the_same_time();
        }
    }


    if ($('#all_day_same_time').is(':checked')) {

        show_all_the_same_time();
        hide_different_days_different_time();
        hide_all_opening_hours();

        /*

         var all_day_time_from = $("#all_day_time_from").val();
         var all_day_time_from_am_pm = document.getElementsByName("all_day_time_from_am_pm")[0].value;
         var all_day_time_to_am_pm = document.getElementsByName("all_day_time_to_am_pm")[0].value;
         var all_day_time_to = $("#all_day_time_to").val();
         var all_day_quantity = $("#all_day_quantity").val();
         var all_day_max_pax = $("#all_day_max_pax").val();


         for(var i=1;i<=7;i++)
         {
         $("#time_from_"+i).val(all_day_time_from);
         document.getElementsByName("time_from_am_pm_"+i)[0].value = all_day_time_from_am_pm;

         $("#time_to_"+i).val(all_day_time_to);
         document.getElementsByName("time_to_am_pm_"+i)[0].value = all_day_time_to_am_pm;

         $("#quantity_"+i).val(all_day_quantity);

         $("#max_pax_"+i).val(all_day_quantity);

         }

         var obj =  document.getElementById('main_con_diff_days_time').getElementsByClassName('from_date');
         for(var j=0; j<obj.length; j++)
         {
         var obj2 = obj[j].getElementsByClassName('selected');
         for(var k=0; k<obj2.length; k++)
         {
         obj2[k].innerHTML = all_day_time_from_am_pm;
         }
         }

         var obj =  document.getElementById('main_con_diff_days_time').getElementsByClassName('to_date');
         for(var j=0; j<obj.length; j++)
         {
         var obj2 = obj[j].getElementsByClassName('selected');
         for(var k=0; k<obj2.length; k++)
         {
         obj2[k].innerHTML = all_day_time_to_am_pm;
         }
         }
         */


    } else {

        hide_all_the_same_time();
    }
});


function check_blank_multiple_dates() {
    return true;



/*
    var offer_running_from = $("#offer_running_from").val();
    var offer_running_to = $("#offer_running_to").val();
    if (offer_running_from == "" || offer_running_to == "") {
        alert("Please select multiple from and to dates!");
        return false;
    } else {

        return true;
    }
*/
}
function if_blank_multiple_dates() {
    var offer_running_from = $("#offer_running_from").val();
    var offer_running_to = $("#offer_running_to").val();

    if (checkBlankField(offer_running_from) == false || checkBlankField(offer_running_to) == false) {
        return false;

    } else {

        return true;
    }
}
$('#container_everyday').click(function () {


    hide_all_the_same_time();
    hide_different_days_different_time();
    hide_all_opening_hours();

    show_all_days_select_dropdown();

    if (check_blank_multiple_dates()) {
        if ($('#everyday_of_week').is(':checked')) {

            show_available_time_slots_and_capacity_li();
            $('#main_con_diff_days_time').show(); //************************ ???
            // show_different_days_different_time();
//            hide_container_daywise();

            /*
             $('#select_specific_days').attr('checked', false);
             $('#container_specific_days .styledCheckbox').css('background-position', '0px 0px');
             for(var i=1;i<=7;i++){ $('#daywise_'+i+' .styledCheckbox').css('background-position', '0px 0px'); }
             */

            uncheck_select_specific_days();

        }

    } else {

        uncheck_everyday();

    }


});
$('#container_specific_days').click(function () {

    hide_all_the_same_time();
    hide_different_days_different_time();
    hide_all_opening_hours();

    if (check_blank_multiple_dates()) {
        if ($('#select_specific_days').is(':checked')) {

            show_available_time_slots_and_capacity_li();
            $('#main_con_diff_days_time').show();
            //show_different_days_different_time();
            uncheck_everyday();
//            show_container_daywise();

        } else {

            uncheck_select_specific_days();
//            hide_container_daywise();
        }

    } else {

        uncheck_select_specific_days();
    }


});


function count_selected_specific_days() {
    var cssd = 0;
    $(".specific_days_week_class").each(function (i) {
        if ($(this).is(':checked')) {
            cssd++;
        }
    })
    return cssd;
}


$('#container_daywise .daywise').click(function () {

    k = 0;
    $(".specific_days_week_class").each(function (i) {
        if ($(this).is(':checked')) {
            k++;
        }
    })
    if (k > 0) {
        if (check_blank_multiple_dates()) {
            $("#select_specific_days").prop('checked', true);
            $('#container_specific_days .styledCheckbox').css('background-position', '0px -29px');

            $("#everyday_of_week").prop('checked', false);
            $('#container_everyday .styledCheckbox').css('background-position', '0px 0px');

        } else {

            uncheck_select_specific_days();
        }


    } else {

        //$("#everyday_of_week").prop('checked', true);
        //$('#container_everyday .styledCheckbox').css('background-position', '0px -29px');

        uncheck_select_specific_days();
    }
});

function if_today_only() {
    if ($('#offer_running_today').is(':checked')) {

        return true;

    } else {
        return false;
    }

}

function if_multiple_dates() {
    if ($('#offer_multiple_dates').is(':checked')) {

        return true;

    } else {
        return false;
    }
}

function if_everyday() {
    if ($('#everyday_of_week').is(':checked')) {

        return true;

    } else {
        return false;
    }
}
function if_select_specific_days() {
    if ($('#select_specific_days').is(':checked')) {

        return true;

    } else {

        return false;
    }
}
function if_all_the_same_time() {

    if ($('#all_day_same_time').is(':checked')) {

        return true;

    } else {

        return false;
    }

}
function if_different_days_different_time() {

    if ($('#different_days_different_time').is(':checked')) {

        return true;

    } else {

        return false;
    }

}
function if_all_opening_hours() {

    if ($('#all_opening_hours').is(':checked')) {

        return true;

    } else {

        return false;
    }
}
function show_all_days_select_dropdown() {
    var days_array = new Array(7);

    days_array[0] = "monday";
    days_array[1] = "tuesday";
    days_array[2] = "wednesday";
    days_array[3] = "thursday";
    days_array[4] = "friday";
    days_array[5] = "saturday";
    days_array[6] = "sunday";

    for (var k = 1; k <= 21; k++) {

        var new_select_dropdown = '';
        new_select_dropdown += '<select name="available_day_' + k + '" id="available_day_' + k + '" class="revostyled">';
        new_select_dropdown += '<option value="">Day</option>';

        if (days_array.length > 0) {
            for (var y = 0; y < days_array.length; y++) {
                var day_shot_name = '';
                if (days_array[y] == 'monday') {
                    day_shot_name = 'Mon';
                }
                if (days_array[y] == 'tuesday') {
                    day_shot_name = 'Tue';
                }
                if (days_array[y] == 'wednesday') {
                    day_shot_name = 'Wed';
                }
                if (days_array[y] == 'thursday') {
                    day_shot_name = 'Thu';
                }
                if (days_array[y] == 'friday') {
                    day_shot_name = 'Fri';
                }
                if (days_array[y] == 'saturday') {
                    day_shot_name = 'Sat';
                }
                if (days_array[y] == 'sunday') {
                    day_shot_name = 'Sun';
                }

                new_select_dropdown += '<option value="' + days_array[y] + '">' + day_shot_name + '</option>';

            }
        }
        new_select_dropdown += '</select>';

        //alert(new_select_dropdown);

        $("#available_day_option_" + k).html(new_select_dropdown);
        $(".revostyled").selectbox();
    }

}

function show_specific_days_select_dropdown() {

    var weekday = new Array(7);
    weekday[0] = "sunday";
    weekday[1] = "monday";
    weekday[2] = "tuesday";
    weekday[3] = "wednesday";
    weekday[4] = "thursday";
    weekday[5] = "friday";
    weekday[6] = "saturday";


    var selected_days_array = new Array();

    var m = 0;
    $(".specific_days_week_class").each(function (i) {


        if ($(this).is(':checked')) {
            var selected_days = $(this).val();
            selected_days_array[m] = selected_days;
            m++;
        }
    })

    //-------------------------- filtering as per between days ----------------------------------//
    var offer_running_from = $("#offer_running_from").val();
    var offer_running_to = $("#offer_running_to").val();
    if (offer_running_from != "" && offer_running_to != "" && if_multiple_dates() == true) {

        //alert('...1......');

        offer_running_from_arr = offer_running_from.split("/");
        offer_running_to_arr = offer_running_to.split("/");

        from_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
        to_date = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

        var days = (to_date - from_date) / (60 * 60 * 24 * 1000);


        //alert(days);


        if (to_date < from_date) {
            alert("Please check offer dates!");
            return false;

        } else {

            var monday = 0;
            var tuesday = 0;
            var wednesday = 0;
            var thursday = 0;
            var friday = 0;
            var saturday = 0;
            var sunday = 0;

            //alert(from_date);
            //alert(to_date);

            while (from_date <= to_date) {

                daysname = weekday[from_date.getDay()];
                if (daysname == 'monday') {
                    monday++;
                } else if (daysname == 'tuesday') {

                    tuesday++;
                }
                else if (daysname == 'wednesday') {

                    wednesday++;
                }
                else if (daysname == 'thursday') {
                    thursday++;
                }
                else if (daysname == 'friday') {
                    friday++;

                }
                else if (daysname == 'saturday') {
                    saturday++;

                }
                else if (daysname == 'sunday') {
                    sunday++;

                }
                from_date.setDate(from_date.getDate() + 1);

            }

            /*
             alert(monday+' monday');
             alert(tuesday+' tuesday');
             alert(wednesday+' wednesday');
             alert(thursday+' thursday');
             alert(friday+' friday');
             alert(saturday+' saturday');
             alert(sunday+' sunday');
             */

            if (days >= 7) {

                if (if_everyday() == true && if_select_specific_days() == false) {
                    var selected_days_array = new Array();
                    selected_days_array.push("monday");
                    selected_days_array.push("tuesday");
                    selected_days_array.push("wednesday");
                    selected_days_array.push("thursday");
                    selected_days_array.push("friday");
                    selected_days_array.push("saturday");
                    selected_days_array.push("sunday");

                } else if (if_everyday() == false && if_select_specific_days() == true) {


                    //******************************** when specific days has been selected **********************//
                    if (selected_days_array.length > 0) {
                        if (monday > 0 || tuesday > 0 || wednesday > 0 || thursday > 0 || friday > 0 || saturday > 0 || sunday > 0) {
                            var selected_days_array_temp = new Array();

                            //alert(selected_days_array.length);
                            for (var u = 0; u < selected_days_array.length; u++) {

                                if (selected_days_array[u] == 'monday') {
                                    if (monday > 0) {
                                        selected_days_array_temp.push("monday");
                                    }
                                }
                                if (selected_days_array[u] == 'tuesday') {
                                    if (tuesday > 0) {
                                        selected_days_array_temp.push("tuesday");
                                    }
                                }
                                if (selected_days_array[u] == 'wednesday') {
                                    if (wednesday > 0) {
                                        selected_days_array_temp.push("wednesday");
                                    }
                                }
                                if (selected_days_array[u] == 'thursday') {
                                    if (thursday > 0) {
                                        selected_days_array_temp.push("thursday");
                                    }
                                }
                                if (selected_days_array[u] == 'friday') {
                                    if (friday > 0) {
                                        selected_days_array_temp.push("friday");
                                    }
                                }
                                if (selected_days_array[u] == 'saturday') {
                                    if (saturday > 0) {
                                        selected_days_array_temp.push("saturday");
                                    }
                                }
                                if (selected_days_array[u] == 'sunday') {
                                    if (sunday > 0) {
                                        selected_days_array_temp.push("sunday");
                                    }
                                }


                            }


                            //if(selected_days_array_temp.length>0)
                            //{
                            var selected_days_array = new Array();
                            selected_days_array = selected_days_array_temp;
                            //}

                        }
                    }
                    //******************************** when specific days has been selected **********************//


                    //alert(selected_days_array.length);

                }


            } else {


                if (if_everyday() == true && if_select_specific_days() == false) {

                    var selected_days_array = new Array();

                    if (monday > 0) {
                        selected_days_array.push("monday");
                    }
                    if (tuesday > 0) {
                        selected_days_array.push("tuesday");
                    }
                    if (wednesday > 0) {
                        selected_days_array.push("wednesday");
                    }
                    if (thursday > 0) {
                        selected_days_array.push("thursday");
                    }
                    if (friday > 0) {
                        selected_days_array.push("friday");
                    }
                    if (saturday > 0) {
                        selected_days_array.push("saturday");
                    }
                    if (sunday > 0) {
                        selected_days_array.push("sunday");
                    }

                }


                if (selected_days_array.length > 0) {
                    if (monday > 0 || tuesday > 0 || wednesday > 0 || thursday > 0 || friday > 0 || saturday > 0 || sunday > 0) {
                        var selected_days_array_temp = new Array();

                        //alert(selected_days_array.length);
                        for (var u = 0; u < selected_days_array.length; u++) {

                            if (selected_days_array[u] == 'monday') {
                                if (monday > 0) {
                                    selected_days_array_temp.push("monday");
                                }
                            }
                            if (selected_days_array[u] == 'tuesday') {
                                if (tuesday > 0) {
                                    selected_days_array_temp.push("tuesday");
                                }
                            }
                            if (selected_days_array[u] == 'wednesday') {
                                if (wednesday > 0) {
                                    selected_days_array_temp.push("wednesday");
                                }
                            }
                            if (selected_days_array[u] == 'thursday') {
                                if (thursday > 0) {
                                    selected_days_array_temp.push("thursday");
                                }
                            }
                            if (selected_days_array[u] == 'friday') {
                                if (friday > 0) {
                                    selected_days_array_temp.push("friday");
                                }
                            }
                            if (selected_days_array[u] == 'saturday') {
                                if (saturday > 0) {
                                    selected_days_array_temp.push("saturday");
                                }
                            }
                            if (selected_days_array[u] == 'sunday') {
                                if (sunday > 0) {
                                    selected_days_array_temp.push("sunday");
                                }
                            }


                        }


                        //if(selected_days_array_temp.length>0)
                        //{
                        var selected_days_array = new Array();
                        selected_days_array = selected_days_array_temp;
                        //}

                    }
                }


                //alert(selected_days_array.length);
                /*
                 for(var j=0;j<selected_days_array.length;j++)
                 {
                 alert(selected_days_array[j]);
                 }
                 */

            }
        }

    }
    //-------------------------- filtering as per between days ----------------------------------//

    if (if_today_only() == true) {

        var today_current_date = new Date();

        var offer_running_from = $("#offer_running_from").val();
        var offer_running_from_arr = offer_running_from.split("/");
        today_current_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day

        var daysname = weekday[today_current_date.getDay()];

        //alert(today_day_name);

        var monday = 0;
        var tuesday = 0;
        var wednesday = 0;
        var thursday = 0;
        var friday = 0;
        var saturday = 0;
        var sunday = 0;

        if (daysname == 'monday') {
            monday++;
        } else if (daysname == 'tuesday') {

            tuesday++;
        }
        else if (daysname == 'wednesday') {

            wednesday++;
        }
        else if (daysname == 'thursday') {
            thursday++;
        }
        else if (daysname == 'friday') {
            friday++;

        }
        else if (daysname == 'saturday') {
            saturday++;

        }
        else if (daysname == 'sunday') {
            sunday++;

        }

        var selected_days_array = new Array();
        if (monday > 0) {
            selected_days_array.push("monday");
        }
        if (tuesday > 0) {
            selected_days_array.push("tuesday");
        }
        if (wednesday > 0) {
            selected_days_array.push("wednesday");
        }
        if (thursday > 0) {
            selected_days_array.push("thursday");
        }
        if (friday > 0) {
            selected_days_array.push("friday");
        }
        if (saturday > 0) {
            selected_days_array.push("saturday");
        }
        if (sunday > 0) {
            selected_days_array.push("sunday");
        }
    }

    for (var k = 1; k <= 21; k++) {

        var new_select_dropdown = '';
        new_select_dropdown += '<select name="available_day_' + k + '" id="available_day_' + k + '" class="revostyled">';

        //if(if_today_only()==false)
        //{
        new_select_dropdown += '<option value="">Day</option>';
        //}

        if (selected_days_array.length > 0) {
            for (var y = 0; y < selected_days_array.length; y++) {
                var day_shot_name = '';
                if (selected_days_array[y] == 'monday') {
                    day_shot_name = 'Mon';
                }
                if (selected_days_array[y] == 'tuesday') {
                    day_shot_name = 'Tue';
                }
                if (selected_days_array[y] == 'wednesday') {
                    day_shot_name = 'Wed';
                }
                if (selected_days_array[y] == 'thursday') {
                    day_shot_name = 'Thu';
                }
                if (selected_days_array[y] == 'friday') {
                    day_shot_name = 'Fri';
                }
                if (selected_days_array[y] == 'saturday') {
                    day_shot_name = 'Sat';
                }
                if (selected_days_array[y] == 'sunday') {
                    day_shot_name = 'Sun';
                }
                new_select_dropdown += '<option value="' + selected_days_array[y] + '">' + day_shot_name + '</option>';
            }
        }
        new_select_dropdown += '</select>';

        //alert(new_select_dropdown);

        $("#available_day_option_" + k).html(new_select_dropdown);
        $(".revostyled").selectbox();
    }


    // for edit mode start

    if ($("#count_different_days_data").length > 0) {

        var count_different_days_data = $("#count_different_days_data").val();
        if (count_different_days_data > 0) {

            for (var k = 1; k <= count_different_days_data; k++) {

                var moat_edt_id = $("#moat_edt_id_" + k).val();

                var available_day_name_edt = $("#available_day_name_edt_" + moat_edt_id).val();

                //alert(moat_edt_id);

                var new_select_dropdown = '';
                new_select_dropdown += '<select name="available_day_edt_' + moat_edt_id + '" id="available_day_edt_' + moat_edt_id + '" class="revostyled">';
                new_select_dropdown += '<option value="">Day</option>';
                if (selected_days_array.length > 0) {
                    for (var y = 0; y < selected_days_array.length; y++) {
                        var day_shot_name = '';
                        if (selected_days_array[y] == 'monday') {
                            day_shot_name = 'Mon';
                        }
                        if (selected_days_array[y] == 'tuesday') {
                            day_shot_name = 'Tue';
                        }
                        if (selected_days_array[y] == 'wednesday') {
                            day_shot_name = 'Wed';
                        }
                        if (selected_days_array[y] == 'thursday') {
                            day_shot_name = 'Thu';
                        }
                        if (selected_days_array[y] == 'friday') {
                            day_shot_name = 'Fri';
                        }
                        if (selected_days_array[y] == 'saturday') {
                            day_shot_name = 'Sat';
                        }
                        if (selected_days_array[y] == 'sunday') {
                            day_shot_name = 'Sun';
                        }

                        var selectedval = "";
                        if (selected_days_array[y] == available_day_name_edt) {
                            selectedval = "selected";
                        } else {
                            selectedval = "";
                        }

                        // alert(available_day_name_edt);
                        // new_select_dropdown += '<option value="'+selected_days_array[y]+'">'+day_shot_name+'</option>';

                        new_select_dropdown += '<option value="' + selected_days_array[y] + '" ' + selectedval + ' >' + day_shot_name + '</option>';

                    }
                }
                new_select_dropdown += '</select>';

                //alert(new_select_dropdown);

                $("#available_day_edt_option_" + k).html(new_select_dropdown);
                $(".revostyled").selectbox();

            }

        }

    }

    // for edit mode start

}
$('#main_con_diff_days_time_container').click(function () {

    if (if_select_specific_days() == true) {

        if (count_selected_specific_days() == 0) {
            alert_select_specific_days();
            //uncheck_different_days();
            hide_different_days_different_time();
            return false;
        } else {

            show_specific_days_select_dropdown();
        }
    }

    if (if_everyday() == true || if_today_only() == true) {

        show_specific_days_select_dropdown();
    }


    if ($('#different_days_different_time').is(':checked')) {

        show_different_days_different_time();
        hide_all_the_same_time();
        hide_all_opening_hours();

        if ($('#offer_multiple_dates').is(':checked')) {

            check_and_display_different_days();
        }

        /*
         for(var i=1;i<=7;i++)
         {
         $("#time_from_"+i).val("");
         document.getElementsByName("time_from_am_pm_"+i)[0].value = "";
         $("#time_to_"+i).val("");
         document.getElementsByName("time_to_am_pm_"+i)[0].value = "";
         $("#quantity_"+i).val("");
         $("#max_pax_"+i).val("");
         $("#available_day_"+i).val("");

         }
         */


    } else {

        $("#all_diff_days_div").hide();
    }
});


function hideDaysOptions() {
    for (var i = 1; i <= 21; i++) {
        document.getElementById("available_day_option_" + i).style.display = "none";
    }
}
function hideDaysOptionsEdt() {
    var count_different_days_data = document.getElementById("count_different_days_data").value;
    for (var i = 1; i <= count_different_days_data; i++) {
        if (document.getElementById("available_day_edt_option_" + i)) {
            document.getElementById("available_day_edt_option_" + i).style.display = "none";
        }
    }
}

// Need to remove the below function.
function set_all_opening_hours() {
    if (document.getElementById("all_opening_hours").checked == true) {
        for (var i = 1; i <= 7; i++) {
            document.getElementById("available_day_" + i).value = document.getElementById("buss_hrs_available_day_" + i).value;
            document.getElementById("time_from_" + i).value = document.getElementById("buss_hrs_time_from_" + i).value;
            document.getElementsByName("time_from_am_pm_" + i)[0].value = document.getElementById("buss_hrs_time_from_am_pm_" + i).value;
            document.getElementById("time_to_" + i).value = document.getElementById("buss_hrs_time_to_" + i).value;
            document.getElementsByName("time_to_am_pm_" + i)[0].value = document.getElementById("buss_hrs_time_to_am_pm_" + i).value;

            if (document.getElementById("different_day_div_" + i)) {
                document.getElementById("different_day_div_" + i).style.display = "block";
            }
        }
    }

}

function display_opening_hours_days() {

    if ($('.monday_open_hrs_div_id').length > 0) {
        $(".monday_open_hrs_div_id").hide();
    }

    if ($('.tuesday_open_hrs_div_id').length > 0) {
        $(".tuesday_open_hrs_div_id").hide();
    }

    if ($('.wednesday_open_hrs_div_id').length > 0) {
        $(".wednesday_open_hrs_div_id").hide();
    }

    if ($('.thursday_open_hrs_div_id').length > 0) {
        $(".thursday_open_hrs_div_id").hide();
    }

    if ($('.friday_open_hrs_div_id').length > 0) {
        $(".friday_open_hrs_div_id").hide();
    }

    if ($('.saturday_open_hrs_div_id').length > 0) {
        $(".saturday_open_hrs_div_id").hide();
    }

    if ($('.sunday_open_hrs_div_id').length > 0) {
        $(".sunday_open_hrs_div_id").hide();
    }


    if ($('#offer_running_today').is(':checked')) {

        // If Today only! Selected

        show_all_opening_hours();

        var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "sunday";
        weekday[1] = "monday";
        weekday[2] = "tuesday";
        weekday[3] = "wednesday";
        weekday[4] = "thursday";
        weekday[5] = "friday";
        weekday[6] = "saturday";

        var offer_running_from = $("#offer_running_from").val();
        if (offer_running_from == "") {
            alert("Please select One day date.");
            return false;
        }
        var offer_running_from_arr = offer_running_from.split("/");
        d = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day

        var n = weekday[d.getDay()];

        // alert(n);


        $(".monday_open_hrs_div_id").hide();
        $(".tuesday_open_hrs_div_id").hide();
        $(".wednesday_open_hrs_div_id").hide();
        $(".thursday_open_hrs_div_id").hide();
        $(".friday_open_hrs_div_id").hide();
        $(".saturday_open_hrs_div_id").hide();
        $(".sunday_open_hrs_div_id").hide();

        if ($("." + n + "_open_hrs_div_id").length > 0) {
            $("." + n + "_open_hrs_div_id").show();

        } else {

            // alert(n+" bussiness opening hours not available.");
        }


        /*
         $("#sunday_open_hrs_div_id").hide();
         $("#monday_open_hrs_div_id").hide();
         $("#tuesday_open_hrs_div_id").hide();
         $("#wednesday_open_hrs_div_id").hide();
         $("#thursday_open_hrs_div_id").hide();
         $("#friday_open_hrs_div_id").hide();
         $("#saturday_open_hrs_div_id").hide();
         $("#"+n+"_open_hrs_div_id").show();
         */

    }
    else if ($('#offer_multiple_dates').is(':checked')) {


        if (check_blank_multiple_dates()) {

            var offer_running_from = $("#offer_running_from").val();
            var offer_running_to = $("#offer_running_to").val();

            offer_running_from_arr = offer_running_from.split("/");
            offer_running_to_arr = offer_running_to.split("/");

            from_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
            to_date = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

            var days = (to_date - from_date) / (60 * 60 * 24 * 1000);


            if (to_date < from_date) {
                alert("Please check offer dates!");

            } else {

                var weekday = new Array(7);
                weekday[0] = "sunday";
                weekday[1] = "monday";
                weekday[2] = "tuesday";
                weekday[3] = "wednesday";
                weekday[4] = "thursday";
                weekday[5] = "friday";
                weekday[6] = "saturday";

                var monday = 0;
                var tuesday = 0;
                var wednesday = 0;
                var thursday = 0;
                var friday = 0;
                var saturday = 0;
                var sunday = 0;

                while (from_date <= to_date) {

                    daysname = weekday[from_date.getDay()];
                    if (daysname == 'monday') {
                        monday++;
                    } else if (daysname == 'tuesday') {

                        tuesday++;
                    }
                    else if (daysname == 'wednesday') {

                        wednesday++;
                    }
                    else if (daysname == 'thursday') {
                        thursday++;
                    }
                    else if (daysname == 'friday') {
                        friday++;

                    }
                    else if (daysname == 'saturday') {
                        saturday++;

                    }
                    else if (daysname == 'sunday') {
                        sunday++;

                    }
                    from_date.setDate(from_date.getDate() + 1);
                }

                /*
                 alert(monday);
                 alert(tuesday);
                 alert(wednesday);
                 alert(thursday);
                 alert(friday);
                 alert(saturday);
                 alert(sunday);

                 var buss_opening_hours_monday = $("#buss_opening_hours_monday").val();
                 var buss_opening_hours_tuesday = $("#buss_opening_hours_tuesday").val();
                 var buss_opening_hours_wednesday = $("#buss_opening_hours_wednesday").val();
                 var buss_opening_hours_thursday = $("#buss_opening_hours_thursday").val();
                 var buss_opening_hours_friday = $("#buss_opening_hours_friday").val();
                 var buss_opening_hours_saturday = $("#buss_opening_hours_saturday").val();
                 var buss_opening_hours_sunday = $("#buss_opening_hours_sunday").val();

                 alert(buss_opening_hours_monday);
                 alert(buss_opening_hours_tuesday);
                 alert(buss_opening_hours_wednesday);
                 alert(buss_opening_hours_thursday);
                 alert(buss_opening_hours_friday);
                 alert(buss_opening_hours_saturday);
                 */

                if (days > 0 || monday > 0 || tuesday > 0 || wednesday > 0 || thursday > 0 || friday > 0 || saturday > 0 || sunday > 0) {
                    show_all_opening_hours();
                }

                if (if_everyday() == true) {

                    if (days >= 7) {
                        // display all day divs

                        /*
                         $("#monday_open_hrs_div_id").show();
                         $("#tuesday_open_hrs_div_id").show();
                         $("#wednesday_open_hrs_div_id").show();
                         $("#thursday_open_hrs_div_id").show();
                         $("#friday_open_hrs_div_id").show();
                         $("#saturday_open_hrs_div_id").show();
                         $("#sunday_open_hrs_div_id").show();
                         */

                        if ($('.monday_open_hrs_div_id').length > 0) {
                            $(".monday_open_hrs_div_id").show();
                        }

                        if ($('.tuesday_open_hrs_div_id').length > 0) {
                            $(".tuesday_open_hrs_div_id").show();
                        }

                        if ($('.wednesday_open_hrs_div_id').length > 0) {
                            $(".wednesday_open_hrs_div_id").show();
                        }

                        if ($('.thursday_open_hrs_div_id').length > 0) {
                            $(".thursday_open_hrs_div_id").show();
                        }

                        if ($('.friday_open_hrs_div_id').length > 0) {
                            $(".friday_open_hrs_div_id").show();
                        }

                        if ($('.saturday_open_hrs_div_id').length > 0) {
                            $(".saturday_open_hrs_div_id").show();
                        }

                        if ($('.sunday_open_hrs_div_id').length > 0) {
                            $(".sunday_open_hrs_div_id").show();
                        }


                    } else {


                        if (monday > 0) {
                            //$("#monday_open_hrs_div_id").show();

                            if ($('.monday_open_hrs_div_id').length > 0) {
                                $(".monday_open_hrs_div_id").show();
                            }

                        }
                        if (tuesday > 0) {
                            //$("#tuesday_open_hrs_div_id").show();

                            if ($('.tuesday_open_hrs_div_id').length > 0) {
                                $(".tuesday_open_hrs_div_id").show();
                            }

                        }
                        if (wednesday > 0) {
                            //$("#wednesday_open_hrs_div_id").show();

                            if ($('.wednesday_open_hrs_div_id').length > 0) {
                                $(".wednesday_open_hrs_div_id").show();
                            }

                        }
                        if (thursday > 0) {
                            //$("#thursday_open_hrs_div_id").show();

                            if ($('.thursday_open_hrs_div_id').length > 0) {
                                $(".thursday_open_hrs_div_id").show();
                            }


                        }
                        if (friday > 0) {
                            //$("#friday_open_hrs_div_id").show();

                            if ($('.friday_open_hrs_div_id').length > 0) {
                                $(".friday_open_hrs_div_id").show();
                            }


                        }
                        if (saturday > 0) {
                            //$("#saturday_open_hrs_div_id").show();

                            if ($('.saturday_open_hrs_div_id').length > 0) {
                                $(".saturday_open_hrs_div_id").show();
                            }

                        }
                        if (sunday > 0) {
                            //$("#sunday_open_hrs_div_id").show();

                            if ($('.sunday_open_hrs_div_id').length > 0) {
                                $(".sunday_open_hrs_div_id").show();
                            }
                        }

                    }


                } else if (if_select_specific_days() == true) {

                    var days_array = new Array(7);
                    days_array[1] = "monday";
                    days_array[2] = "tuesday";
                    days_array[3] = "wednesday";
                    days_array[4] = "thursday";
                    days_array[5] = "friday";
                    days_array[6] = "saturday";
                    days_array[7] = "sunday";

                    for (var j = 1; j < days_array.length; j++) {

                        if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'monday' && monday > 0) {
                            //$("#monday_open_hrs_div_id").show();
                            if ($('.monday_open_hrs_div_id').length > 0) {
                                $(".monday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'tuesday' && tuesday > 0) {
                            //$("#tuesday_open_hrs_div_id").show();
                            if ($('.tuesday_open_hrs_div_id').length > 0) {
                                $(".tuesday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'wednesday' && wednesday > 0) {
                            //$("#wednesday_open_hrs_div_id").show();
                            if ($('.wednesday_open_hrs_div_id').length > 0) {
                                $(".wednesday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'thursday' && thursday > 0) {
                            //$("#thursday_open_hrs_div_id").show();
                            if ($('.thursday_open_hrs_div_id').length > 0) {
                                $(".thursday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'friday' && friday > 0) {
                            //$("#friday_open_hrs_div_id").show();
                            if ($('.friday_open_hrs_div_id').length > 0) {
                                $(".friday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'saturday' && saturday > 0) {
                            //$("#saturday_open_hrs_div_id").show();
                            if ($('.saturday_open_hrs_div_id').length > 0) {
                                $(".saturday_open_hrs_div_id").show();
                            }
                        }
                        else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'sunday' && sunday > 0) {
                            //$("#sunday_open_hrs_div_id").show();
                            if ($('.sunday_open_hrs_div_id').length > 0) {
                                $(".sunday_open_hrs_div_id").show();
                            }
                        }

                    }

                }


            }


        }

    }

}


function edit_business_opening_hours() {
    if (document.getElementById("buss_opening_hrs_div").style.display == "none") {
        document.getElementById("buss_opening_hrs_div").style.display = "block";
    }
    else if (document.getElementById("buss_opening_hrs_div").style.display == "block") {
        document.getElementById("buss_opening_hrs_div").style.display = "none";
    }
    document.getElementById("buss_hrs_upd_msg").innerHTML = "";
}


$('#all_opening_hours_left_div').click(function () {

    if ($('#all_opening_hours').is(':checked')) {


        if (if_select_specific_days() == true) {

            if (count_selected_specific_days() == 0) {
                alert_select_specific_days();
                //uncheck_all_opening_hours()
                hide_all_opening_hours();
                return false;
            }
        }

        hide_all_the_same_time();
        hide_different_days_different_time();
        display_opening_hours_days();

    } else {
        hide_all_opening_hours();
    }
    //set_all_opening_hours(); do not enable

});


function check_and_display_different_days() {
    if (check_blank_multiple_dates()) {

        //var offer_running_from = $('#offer_running_from').val();
        //var offer_running_to = = $('#offer_running_to').val();


        var offer_running_from = $("#offer_running_from").val();
        var offer_running_to = $("#offer_running_to").val();

        offer_running_from_arr = offer_running_from.split("/");
        offer_running_to_arr = offer_running_to.split("/");

        //alert(offer_running_from_arr[0]); // Month
        //alert(offer_running_from_arr[1]); // Day
        //alert(offer_running_from_arr[2]); // Year

        //alert(offer_running_to_arr[0]); // Month
        //alert(offer_running_to_arr[1]); // Day
        //alert(offer_running_to_arr[2]); // Year

        from_date = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
        to_date = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

        //from_date = new Date(2014, 3, 4 ); // Year Month Day
        //var from_date = new Date(2014,3,4,0,0,0)
        //to_date = new Date(2014, 3, 7 ); // Year Month Day


        //alert(from_date);


        if (to_date < from_date) {
            alert("Please check offer dates!");

        } else {


            var days = (to_date - from_date) / (60 * 60 * 24 * 1000);

            //------------------------- COMMON CONDITION START --------------------------------//

            if (days >= 7) {


                if (if_everyday() == true) {
                    //$("#monday_diff_days_div_id").show();
                    //$("#tuesday_diff_days_div_id").show();
                    //$("#wednesday_diff_days_div_id").show();
                    //$("#thursday_diff_days_div_id").show();
                    //$("#friday_diff_days_div_id").show();
                    //$("#saturday_diff_days_div_id").show();
                    //$("#sunday_diff_days_div_id").show();

                }
                else if (if_select_specific_days() == true) {

                    $(".specific_days_week_class").each(function (i) {

                        if ($(this).is(':checked')) {
                            var selected_days = $(this).val();
                            //alert(selected_days);
                            //$("#"+selected_days+"_diff_days_div_id").show();
                        }

                    })

                }


            } else {


                var weekday = new Array(7);
                weekday[0] = "sunday";
                weekday[1] = "monday";
                weekday[2] = "tuesday";
                weekday[3] = "wednesday";
                weekday[4] = "thursday";
                weekday[5] = "friday";
                weekday[6] = "saturday";

                var monday = 0;
                var tuesday = 0;
                var wednesday = 0;
                var thursday = 0;
                var friday = 0;
                var saturday = 0;
                var sunday = 0;

                while (from_date <= to_date) {

                    daysname = weekday[from_date.getDay()];
                    if (daysname == 'monday') {
                        monday++;
                    } else if (daysname == 'tuesday') {

                        tuesday++;
                    }
                    else if (daysname == 'wednesday') {

                        wednesday++;
                    }
                    else if (daysname == 'thursday') {

                        thursday++;
                    }
                    else if (daysname == 'friday') {
                        friday++;

                    }
                    else if (daysname == 'saturday') {
                        saturday++;

                    }
                    else if (daysname == 'sunday') {
                        sunday++;

                    }
                    from_date.setDate(from_date.getDate() + 1);
                }

            }

            //------------------------- COMMON CONDITION END --------------------------------//

            if ($('#everyday_of_week').is(':checked')) {

                if (monday > 0) {
                    //$("#monday_diff_days_div_id").show();

                }
                if (tuesday > 0) {
                    //$("#tuesday_diff_days_div_id").show();

                }
                if (wednesday > 0) {
                    //$("#wednesday_diff_days_div_id").show();

                }
                if (thursday > 0) {
                    //$("#thursday_diff_days_div_id").show();


                }
                if (friday > 0) {
                    //$("#friday_diff_days_div_id").show();


                }
                if (saturday > 0) {
                    //$("#saturday_diff_days_div_id").show();

                }
                if (sunday > 0) {
                    //$("#sunday_diff_days_div_id").show();

                }

            } else if ($('#select_specific_days').is(':checked')) {


                //alert(monday);
                //alert(tuesday);
                //alert(wednesday);
                //alert(thursday);
                //alert(friday);
                //alert(saturday);
                //alert(sunday);

                var days_array = new Array(7);
                days_array[1] = "monday";
                days_array[2] = "tuesday";
                days_array[3] = "wednesday";
                days_array[4] = "thursday";
                days_array[5] = "friday";
                days_array[6] = "saturday";
                days_array[7] = "sunday";

                for (var j = 1; j < days_array.length; j++) {

                    if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'monday' && monday > 0) {
                        //$("#monday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'tuesday' && tuesday > 0) {
                        //$("#tuesday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'wednesday' && wednesday > 0) {
                        //$("#wednesday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'thursday' && thursday > 0) {
                        //$("#thursday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'friday' && friday > 0) {
                        //$("#friday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'saturday' && saturday > 0) {
                        //$("#saturday_diff_days_div_id").show();
                    }
                    else if ($('#specific_days_week_' + days_array[j]).is(':checked') && days_array[j] == 'sunday' && sunday > 0) {
                        //$("#sunday_diff_days_div_id").show();
                    }

                }

            }

        }

    }

}


$('.date_con').click(function () {

    //uncheck_everyday();
    //uncheck_select_specific_days();

    //uncheck_different_days();
    //hide_all_the_same_time();
    //hide_different_days_different_time();
    //hide_all_opening_hours();
    //hide_available_time_slots_and_capacity_li();
    //hide_container_daywise();
    //$('#main_con_diff_days_time').hide();

    uncheck_everyday();
    uncheck_select_specific_days();
    uncheck_different_days();

    hide_different_days_different_time();

    uncheck_all_day_same_time();
    uncheck_all_opening_hours();

    if (if_today_only() == true) {
        $('#allthe_same_time_div').hide();
        $('#all_diff_days_div').hide();
        $('#all_opening_hours_div').hide();

        $('#container_all_day_same_time').show();
        $('#main_con_diff_days_time_container').show();
        $('#container_all_opening_hours').show();
    }
    else if (if_multiple_dates() == true) {

        hide_all_the_same_time();
        hide_all_opening_hours();
        hide_available_time_slots_and_capacity_li();
//        hide_container_daywise();
        $('#main_con_diff_days_time').hide();
    }


});

$('.daywise').click(function () {

    hide_all_the_same_time();
    hide_different_days_different_time();
    hide_all_opening_hours();

});

function check_all_the_same_time_validation() {
    var all_day_quantity = $("#all_day_quantity").val();
    var all_day_max_pax = $("#all_day_max_pax").val();

    var all_day_time_from_dropdown = $(".all_day_time_from_dropdown").val();
    var all_day_time_to_dropdown = $(".all_day_time_to_dropdown").val();

    if (checkBlankField(all_day_time_from_dropdown) == false) {
        alert("Please enter select times From.");
        $(".all_day_time_from_dropdown").focus();
        return false;
    }
    if (checkBlankField(all_day_time_to_dropdown) == false) {
        alert("Please enter select times To.");
        $(".all_day_time_to_dropdown").focus();
        return false;
    }

    if (checkBlankField(all_day_quantity) == false) {
        alert("Please enter quantity.");
        $("#all_day_quantity").focus();
        return false
    }

    if (IsNumeric(all_day_quantity) == false) {
        alert("Please enter only numeric value for quantity.");
        $("#all_day_quantity").focus();
        return false
    }
    if (parseInt(all_day_quantity) <= 0) {
        alert("Quantity should be atleast 1.");
        $("#all_day_quantity").focus();
        return false
    }


    if (checkBlankField(all_day_max_pax) == true) {
        if (IsNumeric(all_day_max_pax) == false) {
            alert("Please enter only numeric value for max pax.");
            $("#all_day_max_pax").focus();
            return false;

        } else {

            if (parseInt(all_day_max_pax) <= 0) {
                alert("Max pax should be more than 0.");
                $("#all_day_max_pax").focus();
                return false
            }

        }

    }
    return true;

}

function check_different_days_different_time_validation() {

    var cddd = document.getElementById("count_different_days_data").value;

    var all_diff_days_data_ids = document.getElementById("all_different_days_data_ids").value;

    //alert(all_diff_days_data_ids);
    //alert(cddd);

    var all_diff_days_data_ids_arr_count = 0;

    if (all_diff_days_data_ids != "") {
        var all_diff_days_data_ids_arr = all_diff_days_data_ids.split("|");
        all_diff_days_data_ids_arr_count = all_diff_days_data_ids_arr.length;
    }
    //alert(all_diff_days_data_ids_arr.length);

    if (parseInt(all_diff_days_data_ids_arr_count) > 0 && cddd > 0) {

        //alert('1.................');
        for (var t = 0; t < all_diff_days_data_ids_arr_count; t++) {
            //alert('2.................');
            var moat_edt_auto_id = all_diff_days_data_ids_arr[t];

            var available_day = "";
            var time_from = "";
            var time_to = "";
            var quantity = "";
            var max_pax = "";

            var available_day_edt_nodelist = document.getElementsByName("available_day_edt_" + moat_edt_auto_id);
            available_day = available_day_edt_nodelist[0].value
            //alert(available_day);

            var time_from_edt_nodelist = document.getElementsByName("time_from_edt_" + moat_edt_auto_id);
            time_from = time_from_edt_nodelist[0].value;
            //alert(time_from);

            var time_to_edt_nodelist = document.getElementsByName("time_to_edt_" + moat_edt_auto_id);
            time_to = time_to_edt_nodelist[0].value;
            //alert(time_to);

            var quantity_edt_nodelist = document.getElementsByName("quantity_edt_" + moat_edt_auto_id);
            quantity = quantity_edt_nodelist[0].value;
            //alert(quantity);

            var max_pax_edt_nodelist = document.getElementsByName("max_pax_edt_" + moat_edt_auto_id);
            max_pax = max_pax_edt_nodelist[0].value;
            //alert(max_pax);

            if (checkBlankField(available_day) == false && checkBlankField(time_from) == true && checkBlankField(time_to) == true && checkBlankField(quantity) == true) {
                alert("Please select days.");
                //available_day_edt_nodelist[0].focus();
                return false;
            }

            if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true) {

                if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true) {
                    if (checkBlankField(quantity) == false) {
                        alert("Please enter quantity.");
                        $(".quantity_edt_" + k).focus();
                        return false;
                    }


                    if (checkBlankField(quantity) == true) {

                        if (IsNumeric(quantity) == false) {
                            alert("Please enter only numeric value for quantity.");
                            $(".quantity_edt_" + k).focus();
                            return false;
                        }
                        if (parseInt(quantity) <= 0) {
                            alert("Quantity should be atleast 1.");
                            $(".quantity_edt_" + k).focus();
                            return false;
                        }

                    }
                    if (checkBlankField(max_pax) == true) {
                        if (IsNumeric(max_pax) == false) {
                            alert("Please enter only numeric value for max pax.");
                            $(".max_pax_edt_" + k).focus();
                            return false;

                        } else {

                            if (parseInt(max_pax) <= 0) {
                                alert("Max pax should be more than 0.");
                                $(".max_pax_edt_" + k).focus();
                                return false;
                            }

                        }

                    }

                }


            }


        }

    }


    var entered_value_check = 0;
    for (var i = 1; i <= 21; i++) {
        var available_day = $("#available_day_" + i).val();
        var time_from = $(".time_from_dropdown_" + i).val();
        var time_to = $(".time_to_dropdown_" + i).val();

        var quantity = $("#quantity_" + i).val();
        var max_pax = $("#max_pax_" + i).val();

        if (document.getElementById(i + "_diff_days_div_id").style.display == "block") {

            if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true && cddd == 0) {
                entered_value_check++;
            }


            if ((checkBlankField(time_from) == true || checkBlankField(time_to) == true) && checkBlankField(available_day) == false) {

                alert("Please select days.");
                $("#available_day_" + i).focus();
                return false;
            }

            if (checkBlankField(time_from) == true && checkBlankField(time_to) == false) {
                alert("Please select To time.");
                $(".time_to_dropdown_" + i).focus();
                return false;

            }
            if (checkBlankField(time_from) == false && checkBlankField(time_to) == true) {
                alert("Please select From time.");
                $(".time_from_dropdown_" + i).focus();
                return false;
            }

            if (checkBlankField(available_day) == true && (checkBlankField(time_from) == false || checkBlankField(time_to) == false)) {
                alert("Please select from and to time.");
                $(".time_from_dropdown_" + i).focus();
                return false;
            }

            if (checkBlankField(time_from) == true && checkBlankField(time_to) == true) {

                if (checkBlankField(available_day) == false) {
                    alert("Please select days.");
                    $("#available_day_" + i).focus();
                    return false;
                } // 09-04-2014

                if (checkBlankField(quantity) == false) {
                    alert("Please enter quantity.");
                    $("#quantity_" + i).focus();
                    return false;
                }

                if (checkBlankField(quantity) == true) {

                    if (IsNumeric(quantity) == false) {
                        alert("Please enter only numeric value for quantity.");
                        $("#quantity_" + i).focus();
                        return false;
                    }
                    if (parseInt(quantity) <= 0) {
                        alert("Quantity should be atleast 1.");
                        $("#quantity_" + i).focus();
                        return false;
                    }

                }
                if (checkBlankField(max_pax) == true) {
                    if (IsNumeric(max_pax) == false) {
                        alert("Please enter only numeric value for max pax.");
                        $("#max_pax_" + i).focus();
                        return false;

                    } else {

                        if (parseInt(max_pax) <= 0) {
                            alert("Max pax should be more than 0.");
                            $("#max_pax_" + i).focus();
                            return false;
                        }

                    }

                }

            }


        }


    }
    if (entered_value_check == 0 && cddd == 0) {
        alert("Please select different days at different times.");
        return false;
    }
    return true;
}

function check_all_opening_hours_validation() {

    var days_array = new Array(7);
    days_array[1] = "monday";
    days_array[2] = "tuesday";
    days_array[3] = "wednesday";
    days_array[4] = "thursday";
    days_array[5] = "friday";
    days_array[6] = "saturday";
    days_array[7] = "sunday";


    for (var j = 1; j < days_array.length; j++) {

        var day_obj_class = document.getElementsByClassName(days_array[j] + '_open_hrs_div_id');
        var day_obj_from_dropdown = document.getElementsByClassName(days_array[j] + '_from_dropdown');
        var day_obj_to_dropdown = document.getElementsByClassName(days_array[j] + '_to_dropdown');
        var day_obj_qty_val_open_hrs = document.getElementsByClassName(days_array[j] + '_qty_val_open_hrs');
        var day_obj_max_open_hrs = document.getElementsByClassName(days_array[j] + '_max_open_hrs');

        if (day_obj_class.length > 0) {
            for (var i = 0; i < day_obj_class.length; i++) {

                if (day_obj_class[i].style.display == "block" || day_obj_class[i].style.display == "") {

                    var from_dropdown = day_obj_from_dropdown[i].value;
                    var to_dropdown = day_obj_to_dropdown[i].value;
                    var qty_val_open_hrs = day_obj_qty_val_open_hrs[i].value;
                    var max_open_hrs = day_obj_max_open_hrs[i].value;

                    if (checkBlankField(qty_val_open_hrs) == false) {
                        alert("Please enter quantity.");
                        $(day_obj_qty_val_open_hrs[i]).focus();
                        return false;
                    }

                    if (checkBlankField(from_dropdown) == true && checkBlankField(to_dropdown) == true && checkBlankField(qty_val_open_hrs) == true) {
                        if (IsNumeric(qty_val_open_hrs) == false) {
                            alert("Please enter only numeric value for quantity.");
                            $(day_obj_qty_val_open_hrs[i]).focus();
                            return false;
                        }
                        if (parseInt(qty_val_open_hrs) <= 0) {
                            alert("Quantity should be atleast 1.");
                            $(day_obj_qty_val_open_hrs[i]).focus();
                            return false;
                        }
                    }

                    if (checkBlankField(from_dropdown) == true && checkBlankField(to_dropdown) == true && checkBlankField(max_open_hrs) == true) {

                        if (IsNumeric(max_open_hrs) == false) {
                            alert("Please enter only numeric value for max pax.");
                            $(day_obj_max_open_hrs[i]).focus();
                            return false;
                        }
                        if (parseInt(max_open_hrs) <= 0) {
                            alert("Max pax should be more than 0.");
                            $(day_obj_max_open_hrs[i]).focus();
                            return false;
                        }
                    }


                }


            }
        }

    }
    return true;

}

function submit_offer_form(act) {

    document.getElementById("display_status").value = act;

    var offer_title = $("#offer_title").val();
    var price = $("#price").val();
    price = price.replace("$", "");

    var price_normally = $("#price_normally").val();
    price_normally = price_normally.replace("$", "");

    if (checkBlankField(offer_title) == false) {
        alert("Please enter title.");
        $("#offer_title").focus();
        return false;
    }
    if (checkBlankField(price) == false) {
        alert("Please enter price.");
        $("#price").focus();
        return false;
    }
    if (IsNumeric(price) == false) {
        alert("Please enter only numeric value for price.");
        $("#price").focus();
        return false
    }
    if (checkBlankField(price_normally) == false) {
        alert("Please enter normal price.");
        $("#price_normally").focus();
        return false;
    }
    if (IsNumeric(price_normally) == false) {
        alert("Please enter only numeric value for normal price.");
        $("#price_normally").focus();
        return false
    }

    if (parseFloat(price) >= parseFloat(price_normally)) {
        alert("Normally price always needs to be higher than the now price.");
        $("#price_normally").focus();
        return false
    }


    if (if_today_only() == false && if_multiple_dates() == false) {
        alert("Please select Offer running (dates)");
        return false;
    }
    if (if_today_only() == true && if_all_the_same_time() == false && if_all_opening_hours() == false && if_different_days_different_time() == false) {
        alert("Please select available time slots and capacity.");
        return false;
    }

    if (if_today_only() == true && if_different_days_different_time() == true) {
        if (check_different_days_different_time_validation() == true) {

        } else {

            return false;
        }
    }
    if (if_multiple_dates() == true && if_blank_multiple_dates() == false) {
        alert("Please select multiple dates");
        return false;
    }
    if (if_multiple_dates() == true && if_blank_multiple_dates() == true && check_valid_multiple_dates() == false) {
        alert("Please check multiple dates!");
        return false;
    }
    if (if_multiple_dates() == true && if_everyday() == false && if_select_specific_days() == false) {
        alert("Please select what day of the week");
        return false;
    }
    if (if_multiple_dates() == true && if_everyday() == true && if_all_the_same_time() == false && if_different_days_different_time() == false && if_all_opening_hours() == false) {
        alert("Please select Available time slots and capacity");
        return false;
    }
    var cnt_ssd = 0;
    cnt_ssd = count_selected_specific_days();
    if (if_multiple_dates() == true && if_select_specific_days() == true && cnt_ssd == 0) {
        alert("Please select atleast one specific days");
        return false;
    }
    if (if_multiple_dates() == true && if_select_specific_days() == true && cnt_ssd > 0 && if_all_the_same_time() == false && if_different_days_different_time() == false && if_all_opening_hours() == false) {
        alert("Please select Select times or Different days at different times or All opening hours");
        return false;
    }


    // var all_day_time_from_am_pm = document.getElementsByName("all_day_time_from_am_pm")[0].value="";
    // var all_day_time_to_am_pm = document.getElementsByName("all_day_time_to_am_pm")[0].value="";

    if (if_today_only() == true && if_all_the_same_time() == true) {

        if (check_all_the_same_time_validation() == true) {

        } else {

            return false;
        }
    }


    if (if_today_only() == true && if_all_opening_hours() == true) {
        var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "sunday";
        weekday[1] = "monday";
        weekday[2] = "tuesday";
        weekday[3] = "wednesday";
        weekday[4] = "thursday";
        weekday[5] = "friday";
        weekday[6] = "saturday";


        var offer_running_from = $("#offer_running_from").val();
        if (offer_running_from == "") {
            alert("Please select One day date.");
            return false;
        }
        var offer_running_from_arr = offer_running_from.split("/");
        d = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day

        var n = weekday[d.getDay()];

        if ($("." + n + "_open_hrs_div_id").length > 0) {

            //alert("."+n+"_open_hrs_div_id ."+n+"_from_dropdown");

            var from_time_val = $("." + n + "_open_hrs_div_id ." + n + "_from_dropdown").val();
            var to_time_val = $("." + n + "_open_hrs_div_id ." + n + "_to_dropdown").val();

            if (checkBlankField(from_time_val) == false) {
                alert("Please select all opening hours from time.");
                $("." + n + "_open_hrs_div_id ." + n + "_from_dropdown").focus();
                return false;
            }
            if (checkBlankField(to_time_val) == false) {
                alert("Please select all opening hours to time.");
                $("." + n + "_open_hrs_div_id ." + n + "_to_dropdown").focus();
                return false;
            }


            var qty_val = $("." + n + "_open_hrs_div_id .qty_val").val();
            if (IsNumeric(qty_val) == false) {
                alert("Please enter only numeric value for quantity.");
                $("." + n + "_open_hrs_div_id .qty_val").focus();
                return false
            }
            if (parseInt(qty_val) <= 0) {
                alert("Quantity should be atleast 1.");
                $("." + n + "_open_hrs_div_id .qty_val").focus();
                return false
            }

            var max_pax = $("." + n + "_open_hrs_div_id .max").val();

            if (checkBlankField(max_pax) == true) {

                if (IsNumeric(max_pax) == false) {
                    alert("Please enter only numeric value for max pax.");
                    $("." + n + "_open_hrs_div_id .max").focus();
                    return false;

                } else {

                    if (parseInt(max_pax) <= 0) {
                        alert("Max pax should be more than 0.");
                        $("." + n + "_open_hrs_div_id .max").focus();
                        return false
                    }

                }


            }


        }

    }

    if (if_multiple_dates() == true && (if_everyday() == true || if_select_specific_days() == true) && if_all_the_same_time() == true) {
        if (check_all_the_same_time_validation() == true) {

        } else {

            return false;
        }
    }

    if (if_multiple_dates() == true && (if_everyday() == true || if_select_specific_days() == true ) && if_different_days_different_time() == true) {
        if (check_different_days_different_time_validation() == true) {

        } else {

            return false;
        }
    }

    if (if_multiple_dates() == true && (if_everyday() == true || if_select_specific_days() == true ) && if_all_opening_hours() == true) {


        if (check_all_opening_hours_validation() == true) {

        } else {

            return false;
        }


    }

    document.getElementById("add_offer_frm").submit();
}


function display_suggestion_box(id, advise) {
    var txt = $('#' + id).val();

    //if(txt=="")
    //{
    $('#' + id).attr('placeholder', "");
    $("#" + advise).css('display', 'block');
    //}
}

function hide_suggestion_box(id, content, advise) {
    var txt = $('#' + id).val();
    //if(txt=="")
    //{
    $('#' + id).attr('placeholder', content);
    $("#" + advise).css('display', 'none');
    //}
}
