

//
// Prices
//

function add_dollar(str) {
    if (str.length == 0) {
        document.getElementById("price").value = "$";
    }
}

function add_dollar2(str) {
    if (str.length == 0) {
        document.getElementById("price_normally").value = "$";
    }
}



//
// Descriptions
//

function display_suggestion_box(id, advise) {
    var txt = $('#' + id).val();
    $('#' + id).attr('placeholder', "");
    $("#" + advise).css('display', 'block');
}

function hide_suggestion_box(id, content, advise) {
    var txt = $('#' + id).val();
    $('#' + id).attr('placeholder', content);
    $("#" + advise).css('display', 'none');
}






//
// Offer running (dates)
//


$(function () {

    $('.merchant_newoffer ul li input:checkbox').screwDefaultButtons({
        image: 'url("' + baseUrl + 'public/images/agree.jpg")',
        width: 29,
        height: 29
    });

});



$('#offer_running_today_div, #offer_multiple_dt_container_div').click(function () {
    var multipleDates = $(this).attr('id') == 'offer_multiple_dt_container_div';



    // Toggle date checkboxes
    if(multipleDates) {

        $('#offer_running_today').attr('checked', false);
        $('#offer_running_today_div .styledCheckbox').css('background-position', '0px 0px');

        $('#offer_multiple_dates').attr('checked', true);
        $('#offer_multiple_dt_container_div .styledCheckbox').css('background-position', '0px -29px');

    } else {

        $('#offer_running_today').attr('checked', true);
        $('#offer_running_today_div .styledCheckbox').css('background-position', '0px -29px');

        $('#offer_multiple_dates').attr('checked', false);
        $('#offer_multiple_dt_container_div .styledCheckbox').css('background-position', '0px 0px');

    }



    // Toggle dates
    $('#offer_running_from').val(todayFormattedDate());
    $("#multidate_from_to_div").show();
    $("#offer_running_from_container").show();

    if(multipleDates) {
        $("#offer_running_to_container").show();
    } else {
        $("#offer_running_to_container").hide();
    }



    // Toggle day of week
    if(multipleDates) {
        $("#what_day_of_week_li").show();

    } else {
        $("#what_day_of_week_li").hide();
    }



    // Toggle time slots
    $("#available_time_slots_and_capacity_li").show();
    $("#all_diff_days_div").show();

    if(multipleDates) {
        $("#all_diff_days_div .days_drop").show();

    } else {
        $("#all_diff_days_div .days_drop").hide();
    }


});


function showhide_different_day() {
    var i;
    for (i = 4; i <= 21; i++) {
        if (document.getElementById(i + "_diff_days_div_id")) {
            if (document.getElementById(i + "_diff_days_div_id").style.display == "none") {
                document.getElementById(i + "_diff_days_div_id").style.display = "block";

                if(i >= 20)
                    document.getElementById("add_time_slot").style.display = "none";

                return false;
            }
        }
    }


}


function todayFormattedDate(date) {
    var d = new Date(date || Date.now()),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('/');
}




//
// Page Submission
//


function submit_offer_form(act) {

    document.getElementById("display_status").value = act;

    var offer_title     = $("#offer_title").val();
    var price           = $("#price").val().replace("$", "");
    var price_normally  = $("#price_normally").val().replace("$", "");



    if (checkBlankField(offer_title) == false) {
        alert("Please enter title.");
        $("#offer_title").focus();
        return false;
    }
    
    if (checkBlankField(price) == false) {
        alert("Please enter price.");
        $("#price").focus();
        return false;
    }
    
    if (IsNumeric(price) == false) {
        alert("Please enter only numeric value for price.");
        $("#price").focus();
        return false
    }
    
    if (checkBlankField(price_normally) == false) {
        alert("Please enter normal price.");
        $("#price_normally").focus();
        return false;
    }
    
    if (IsNumeric(price_normally) == false) {
        alert("Please enter only numeric value for normal price.");
        $("#price_normally").focus();
        return false
    }

    if (parseFloat(price) >= parseFloat(price_normally)) {
        alert("Normally price always needs to be higher than the now price.");
        $("#price_normally").focus();
        return false
    }

    if (if_today_only() == false && if_multiple_dates() == false) {

        // No days selected
        alert("Please select Offer running (dates)");
        return false;


    } else if(if_today_only()) {


        //
        // Today Only
        //

        var offer_running_from = $("#offer_running_from").val();
        if (offer_running_from == "") {
            alert("Please select a date.");
            return false;
        }







        if (!check_different_days_different_time_validation()) {
            return false;
        }

        if (!check_all_the_same_time_validation()) {
            return false;
        }


        var d = new Date();
        var weekday = new Array(7);
        weekday[0] = "sunday";
        weekday[1] = "monday";
        weekday[2] = "tuesday";
        weekday[3] = "wednesday";
        weekday[4] = "thursday";
        weekday[5] = "friday";
        weekday[6] = "saturday";



        var offer_running_from_arr = offer_running_from.split("/");
        d = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day

        var n = weekday[d.getDay()];

        if ($("." + n + "_open_hrs_div_id").length > 0) {

            var from_time_val = $("." + n + "_open_hrs_div_id ." + n + "_from_dropdown").val();
            var to_time_val = $("." + n + "_open_hrs_div_id ." + n + "_to_dropdown").val();

            if (checkBlankField(from_time_val) == false) {
                alert("Please select all opening hours from time.");
                $("." + n + "_open_hrs_div_id ." + n + "_from_dropdown").focus();
                return false;
            }
            if (checkBlankField(to_time_val) == false) {
                alert("Please select all opening hours to time.");
                $("." + n + "_open_hrs_div_id ." + n + "_to_dropdown").focus();
                return false;
            }


            var qty_val = $("." + n + "_open_hrs_div_id .qty_val").val();
            if (IsNumeric(qty_val) == false) {
                alert("Please enter only numeric value for quantity.");
                $("." + n + "_open_hrs_div_id .qty_val").focus();
                return false
            }
            if (parseInt(qty_val) <= 0) {
                alert("Quantity should be atleast 1.");
                $("." + n + "_open_hrs_div_id .qty_val").focus();
                return false
            }

            var max_pax = $("." + n + "_open_hrs_div_id .max").val();

            if (checkBlankField(max_pax) == true) {

                if (IsNumeric(max_pax) == false) {
                    alert("Please enter only numeric value for max pax.");
                    $("." + n + "_open_hrs_div_id .max").focus();
                    return false;

                } else {

                    if (parseInt(max_pax) <= 0) {
                        alert("Max pax should be more than 0.");
                        $("." + n + "_open_hrs_div_id .max").focus();
                        return false
                    }

                }


            }


        }



    } else if(if_multiple_dates()) {


        //
        // Multiple Dates
        //

        if ( if_blank_multiple_dates() == false) {
            alert("Please select multiple dates");
            return false;

        } else if (check_valid_multiple_dates() == false) {
            alert("Please check multiple dates!");
            return false;
        }

        if (!count_selected_specific_days()) {
            alert("Please select at least one specific day");
            return false;
        }





        if (!check_all_the_same_time_validation()) {
            return false;
        }

        if(!check_different_days_different_time_validation()) {
            return false;
        }

        if(!check_all_opening_hours_validation()) {
            return false;
        }

    }

    document.getElementById("add_offer_frm").submit();
}


function if_today_only() {
    return $('#offer_running_today').is(':checked');
}

function if_multiple_dates() {
    return $('#offer_multiple_dates').is(':checked');
}




function if_blank_multiple_dates() {
    var offer_running_from = $("#offer_running_from").val();
    var offer_running_to = $("#offer_running_to").val();

    return checkBlankField(offer_running_from) && checkBlankField(offer_running_to);
}





function check_different_days_different_time_validation() {

    var cddd = document.getElementById("count_different_days_data").value;

    var all_diff_days_data_ids = document.getElementById("all_different_days_data_ids").value;

    //alert(all_diff_days_data_ids);
    //alert(cddd);

    var all_diff_days_data_ids_arr_count = 0;

    if (all_diff_days_data_ids != "") {
        var all_diff_days_data_ids_arr = all_diff_days_data_ids.split("|");
        all_diff_days_data_ids_arr_count = all_diff_days_data_ids_arr.length;
    }
    //alert(all_diff_days_data_ids_arr.length);

    if (parseInt(all_diff_days_data_ids_arr_count) > 0 && cddd > 0) {

        //alert('1.................');
        for (var t = 0; t < all_diff_days_data_ids_arr_count; t++) {
            //alert('2.................');
            var moat_edt_auto_id = all_diff_days_data_ids_arr[t];

            var available_day = "";
            var time_from = "";
            var time_to = "";
            var quantity = "";
            var max_pax = "";

            var available_day_edt_nodelist = document.getElementsByName("available_day_edt_" + moat_edt_auto_id);
            available_day = available_day_edt_nodelist[0].value
            //alert(available_day);

            var time_from_edt_nodelist = document.getElementsByName("time_from_edt_" + moat_edt_auto_id);
            time_from = time_from_edt_nodelist[0].value;
            //alert(time_from);

            var time_to_edt_nodelist = document.getElementsByName("time_to_edt_" + moat_edt_auto_id);
            time_to = time_to_edt_nodelist[0].value;
            //alert(time_to);

            var quantity_edt_nodelist = document.getElementsByName("quantity_edt_" + moat_edt_auto_id);
            quantity = quantity_edt_nodelist[0].value;
            //alert(quantity);

            var max_pax_edt_nodelist = document.getElementsByName("max_pax_edt_" + moat_edt_auto_id);
            max_pax = max_pax_edt_nodelist[0].value;
            //alert(max_pax);

            if (checkBlankField(available_day) == false && checkBlankField(time_from) == true && checkBlankField(time_to) == true && checkBlankField(quantity) == true) {
                alert("Please select days.");
                //available_day_edt_nodelist[0].focus();
                return false;
            }

            if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true) {

                if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true) {
                    if (checkBlankField(quantity) == false) {
                        alert("Please enter quantity.");
                        $(".quantity_edt_" + k).focus();
                        return false;
                    }


                    if (checkBlankField(quantity) == true) {

                        if (IsNumeric(quantity) == false) {
                            alert("Please enter only numeric value for quantity.");
                            $(".quantity_edt_" + k).focus();
                            return false;
                        }
                        if (parseInt(quantity) <= 0) {
                            alert("Quantity should be atleast 1.");
                            $(".quantity_edt_" + k).focus();
                            return false;
                        }

                    }
                    if (checkBlankField(max_pax) == true) {
                        if (IsNumeric(max_pax) == false) {
                            alert("Please enter only numeric value for max pax.");
                            $(".max_pax_edt_" + k).focus();
                            return false;

                        } else {

                            if (parseInt(max_pax) <= 0) {
                                alert("Max pax should be more than 0.");
                                $(".max_pax_edt_" + k).focus();
                                return false;
                            }

                        }

                    }

                }


            }


        }

    }


    var entered_value_check = 0;
    for (var i = 1; i <= 21; i++) {
        var available_day = $("#available_day_" + i).val();
        var time_from = $(".time_from_dropdown_" + i).val();
        var time_to = $(".time_to_dropdown_" + i).val();

        var quantity = $("#quantity_" + i).val();
        var max_pax = $("#max_pax_" + i).val();

        if (document.getElementById(i + "_diff_days_div_id").style.display == "block") {

            if (checkBlankField(available_day) == true && checkBlankField(time_from) == true && checkBlankField(time_to) == true && cddd == 0) {
                entered_value_check++;
            }


            if ((checkBlankField(time_from) == true || checkBlankField(time_to) == true) && checkBlankField(available_day) == false) {

                alert("Please select days.");
                $("#available_day_" + i).focus();
                return false;
            }

            if (checkBlankField(time_from) == true && checkBlankField(time_to) == false) {
                alert("Please select To time.");
                $(".time_to_dropdown_" + i).focus();
                return false;

            }
            if (checkBlankField(time_from) == false && checkBlankField(time_to) == true) {
                alert("Please select From time.");
                $(".time_from_dropdown_" + i).focus();
                return false;
            }

            if (checkBlankField(available_day) == true && (checkBlankField(time_from) == false || checkBlankField(time_to) == false)) {
                alert("Please select from and to time.");
                $(".time_from_dropdown_" + i).focus();
                return false;
            }

            if (checkBlankField(time_from) == true && checkBlankField(time_to) == true) {

                if (checkBlankField(available_day) == false) {
                    alert("Please select days.");
                    $("#available_day_" + i).focus();
                    return false;
                } // 09-04-2014

                if (checkBlankField(quantity) == false) {
                    alert("Please enter quantity.");
                    $("#quantity_" + i).focus();
                    return false;
                }

                if (checkBlankField(quantity) == true) {

                    if (IsNumeric(quantity) == false) {
                        alert("Please enter only numeric value for quantity.");
                        $("#quantity_" + i).focus();
                        return false;
                    }
                    if (parseInt(quantity) <= 0) {
                        alert("Quantity should be atleast 1.");
                        $("#quantity_" + i).focus();
                        return false;
                    }

                }
                if (checkBlankField(max_pax) == true) {
                    if (IsNumeric(max_pax) == false) {
                        alert("Please enter only numeric value for max pax.");
                        $("#max_pax_" + i).focus();
                        return false;

                    } else {

                        if (parseInt(max_pax) <= 0) {
                            alert("Max pax should be more than 0.");
                            $("#max_pax_" + i).focus();
                            return false;
                        }

                    }

                }

            }


        }


    }
    if (entered_value_check == 0 && cddd == 0) {
        alert("Please select different days at different times.");
        return false;
    }
    return true;
}


function check_all_the_same_time_validation() {
    var all_day_quantity = $("#all_day_quantity").val();
    var all_day_max_pax = $("#all_day_max_pax").val();

    var all_day_time_from_dropdown = $(".all_day_time_from_dropdown").val();
    var all_day_time_to_dropdown = $(".all_day_time_to_dropdown").val();

    if (checkBlankField(all_day_time_from_dropdown) == false) {
        alert("Please enter select times From.");
        $(".all_day_time_from_dropdown").focus();
        return false;
    }
    if (checkBlankField(all_day_time_to_dropdown) == false) {
        alert("Please enter select times To.");
        $(".all_day_time_to_dropdown").focus();
        return false;
    }

    if (checkBlankField(all_day_quantity) == false) {
        alert("Please enter quantity.");
        $("#all_day_quantity").focus();
        return false
    }

    if (IsNumeric(all_day_quantity) == false) {
        alert("Please enter only numeric value for quantity.");
        $("#all_day_quantity").focus();
        return false
    }
    if (parseInt(all_day_quantity) <= 0) {
        alert("Quantity should be atleast 1.");
        $("#all_day_quantity").focus();
        return false
    }


    if (checkBlankField(all_day_max_pax) == true) {
        if (IsNumeric(all_day_max_pax) == false) {
            alert("Please enter only numeric value for max pax.");
            $("#all_day_max_pax").focus();
            return false;

        } else {

            if (parseInt(all_day_max_pax) <= 0) {
                alert("Max pax should be more than 0.");
                $("#all_day_max_pax").focus();
                return false
            }

        }

    }
    return true;

}


function check_valid_multiple_dates() {

    var offer_running_from  = $("#offer_running_from").val();
    var offer_running_to    = $("#offer_running_to").val();

    if (!offer_running_from || !offer_running_to) {
        alert("Please select multiple from and to dates!");
        return false;
    }


    var offer_running_from_arr  = offer_running_from.split("/");
    var offer_running_to_arr    = offer_running_to.split("/");

    var from_date   = new Date(offer_running_from_arr[2], offer_running_from_arr[1] - 1, offer_running_from_arr[0]); // Year Month Day
    var to_date     = new Date(offer_running_to_arr[2], offer_running_to_arr[1] - 1, offer_running_to_arr[0]); // Year Month Day

    if(to_date < from_date) {
        alert("Please check offer dates!");
        return false;
    }

    return true;
}


function count_selected_specific_days() {
    var cssd = 0;
    $(".specific_days_week_class").each(function (i) {
        if ($(this).is(':checked')) {
            cssd++;
        }
    })
    return cssd;
}


function check_all_opening_hours_validation() {

    var days_array = new Array(7);
    days_array[1] = "monday";
    days_array[2] = "tuesday";
    days_array[3] = "wednesday";
    days_array[4] = "thursday";
    days_array[5] = "friday";
    days_array[6] = "saturday";
    days_array[7] = "sunday";


    for (var j = 1; j < days_array.length; j++) {

        var day_obj_class = document.getElementsByClassName(days_array[j] + '_open_hrs_div_id');
        var day_obj_from_dropdown = document.getElementsByClassName(days_array[j] + '_from_dropdown');
        var day_obj_to_dropdown = document.getElementsByClassName(days_array[j] + '_to_dropdown');
        var day_obj_qty_val_open_hrs = document.getElementsByClassName(days_array[j] + '_qty_val_open_hrs');
        var day_obj_max_open_hrs = document.getElementsByClassName(days_array[j] + '_max_open_hrs');

        if (day_obj_class.length > 0) {
            for (var i = 0; i < day_obj_class.length; i++) {

                if (day_obj_class[i].style.display == "block" || day_obj_class[i].style.display == "") {

                    var from_dropdown = day_obj_from_dropdown[i].value;
                    var to_dropdown = day_obj_to_dropdown[i].value;
                    var qty_val_open_hrs = day_obj_qty_val_open_hrs[i].value;
                    var max_open_hrs = day_obj_max_open_hrs[i].value;

                    if (checkBlankField(qty_val_open_hrs) == false) {
                        alert("Please enter quantity.");
                        $(day_obj_qty_val_open_hrs[i]).focus();
                        return false;
                    }

                    if (checkBlankField(from_dropdown) == true && checkBlankField(to_dropdown) == true && checkBlankField(qty_val_open_hrs) == true) {
                        if (IsNumeric(qty_val_open_hrs) == false) {
                            alert("Please enter only numeric value for quantity.");
                            $(day_obj_qty_val_open_hrs[i]).focus();
                            return false;
                        }
                        if (parseInt(qty_val_open_hrs) <= 0) {
                            alert("Quantity should be atleast 1.");
                            $(day_obj_qty_val_open_hrs[i]).focus();
                            return false;
                        }
                    }

                    if (checkBlankField(from_dropdown) == true && checkBlankField(to_dropdown) == true && checkBlankField(max_open_hrs) == true) {

                        if (IsNumeric(max_open_hrs) == false) {
                            alert("Please enter only numeric value for max pax.");
                            $(day_obj_max_open_hrs[i]).focus();
                            return false;
                        }
                        if (parseInt(max_open_hrs) <= 0) {
                            alert("Max pax should be more than 0.");
                            $(day_obj_max_open_hrs[i]).focus();
                            return false;
                        }
                    }


                }


            }
        }

    }
    return true;

}