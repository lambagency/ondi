<div class="blog_page_right">
	<div class="blog_search">
	<form name="blogsearchform" method="post" action="<?php echo $this->config->item('base_url'); ?>blog/" >
		<input type="text" placeholder="Search" class="blog_position" name="blogsearch" />
	</form>
	</div>
	<div class="blog_social">
		<h3>Follow us</h3>
		<ul>
			<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/twitter_checkout.png" alt="" /></a></li>
			<li class="no_gap"><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/fb_checkout.png" alt="" /></a></li>
		</ul>
	</div>
	<div class="blog_tweets">
		<h3>Latest tweets</h3>
		<ul>
			<li>Popolo just launched an unbelieveable lunch deal: Wagyu and wine for only $20 ... take a look at the live deal now! <a href="http://www.ondi.com/popolo/livedeals">www.ondi.com/popolo/livedeals</a></li>
			<li>Popolo just launched an unbelieveable lunch deal: Wagyu and wine for only $20 ... take a look at the live deal now! <a href="http://www.ondi.com/popolo/livedeals">www.ondi.com/popolo/livedeals</a></li>
			<li>Popolo just launched an unbelieveable lunch deal: Wagyu and wine for only $20 ... take a look at the live deal now! <a href="http://www.ondi.com/popolo/livedeals">www.ondi.com/popolo/livedeals</a></li>
		</ul>
	</div>
	<div class="blog_archives">
		<h3>Archives</h3>
		<ul>
			<?php 
			$months = $this->ondiblog->getarchievemonths();
			foreach($months as $month)
			{
			?>
			<li><a href="<?php echo $this->config->item('base_url'); ?>blog/?month=<?=$month->arcmonths?>"><?=$month->arcmonths?></a></li>
			<?php
			}
			?>
		</ul>
	</div>
</div>