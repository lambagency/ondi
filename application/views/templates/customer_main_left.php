<div class="merchant_mobile_menu"></div>
<div class="customer_main_left">
	<?php if (!$this->agent->is_mobile())
    {
    ?>
    
    <div class="sidebar">
		<ul>
			<li><a href="<?php echo base_url("customeraccount/contactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_user.png" alt="" />About Me</a></li>
			<li><a href="<?php echo base_url("customeraccount/mybookings/"); ?>"  <?php if($this->uri->segment(2)=='mybookings'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_calender.png" alt="" />My Bookings</a></li>
			<li><a href="<?php echo base_url("customeraccount/shortlists/"); ?>"  <?php if($this->uri->segment(2)=='shortlists'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_star.png" alt="" />My Shortlist</a></li>
			<li><a href="<?php echo base_url("customeraccount/emailpreferences/"); ?>" <?php if($this->uri->segment(2)=='emailpreferences'){?> class="active" <?php } ?>  ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_msg.png" alt="" />Email preferences</a></li>
			<li><a href="<?php echo base_url("signup/logout/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_arrow.png" alt="" />Log out</a></li>
		</ul>
	</div>
    
    
    <?php	
    }
    else
    {
    
    ?>
    
    <div class="sidebar">
		<ul>
        
        <?php
			$user_notifications_data = $this->users->get_user_notification_data();
			$unread_notifications = $user_notifications_data['unread'];
			$notifications = $user_notifications_data['notifications_data'];
			//print_r($notifications);
			
			if($sess_user_data['user_type']=='1')
			{
				$notifications_page_url = base_url("customeraccount/mynotifications/");
			} 
			else 
			{
				$notifications_page_url = base_url("merchantaccount/mynotifications/");
			} 
			
		?>
        <li class="last" id="li_ajax_notifications">
				<?php if($unread_notifications>0){?>
				<a href="javascript:void(0)" class="comment_nav"><font id="num_unreadnotify"><?=$unread_notifications?></font><span>Notification</span></a>
				<?php } else{ ?>
				<a href="<?=$notifications_page_url?>" class="comment_nav_inactive"><font id="num_unreadnotify">&nbsp;</font><span>Notification</span></a>
				<?php } ?>
                
                

				<div class="notification" <?php if($unread_notifications==0){ echo "style='display:none;'"; } ?> >
					<div class="dd_arrow_small"><img src="<?php echo $this->config->item('base_url'); ?>public/images/dd_arrow_small.png" alt="" /></div>
					<h3>Notifications</h3>
					<ul>
						<?php 
						$ni=0;
						foreach($notifications as $this_notification)
						{
							$ni++;
							if($sess_user_data['user_type']=='1')
							{
								$read_status = $this_notification->customer_read_status;
							}
							else
							{
								$read_status = $this_notification->merchant_read_status;
							}
						?>
						<li <?php if($read_status=='0'){ echo 'class="active"'; } ?>>
							<?php if($this_notification->profile_message == '1'){?>
							<div class="left_note"><img src="<?php echo $this->config->item('base_url'); ?>public/images/notification.png" alt="" /></div>
							<?php } else { ?>
							<div class="left_note"><div class="calender">23</div></div>
							<?php } ?>

							<div class="right_note"><?=$this_notification->message?></div>
							<div class="date_note"><?=$this->customclass->_ago(strtotime($this_notification->added_date))?>  / <span <?php if($read_status=='1'){ echo 'class="normal"'; } else { echo 'class="bold"'; } ?> onclick="change_read_status(<?=$this_notification->id?>);" id="span_notification_<?=$this_notification->id?>" >Mark as <?php if($read_status=='1'){ echo 'unread'; } else { echo 'Read'; } ?></span> </div>
						</li>
						<?php 
						if($ni ==5) break;
						} ?>
					</ul>
					<?php if(sizeof($notifications)>5){?>
						<span class="read_more">
							<?php if($sess_user_data['user_type']=='1'){?>
								<a href="<?php echo base_url("customeraccount/mynotifications/"); ?>">Load More</a>
							<?php } else {?>
								<a href="<?php echo base_url("merchantaccount/mynotifications/"); ?>">Load More</a>
							<?php } ?>
						</span>
					<?php } ?>
				</div>
			</li>
            
            
            
            
            
        
			<li><a href="<?php echo base_url("customeraccount/contactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_user.png" alt="" /><span>About</span></a></li>
			<li><a href="<?php echo base_url("customeraccount/mybookings/"); ?>"  <?php if($this->uri->segment(2)=='mybookings'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_calender.png" alt="" /><span>Bookings</span></a></li>
			<li><a href="<?php echo base_url("customeraccount/shortlists/"); ?>"  <?php if($this->uri->segment(2)=='shortlists'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_star.png" alt="" /><span>Shortlist</span></a></li>
			<li><a href="<?php echo base_url("customeraccount/emailpreferences/"); ?>" <?php if($this->uri->segment(2)=='emailpreferences'){?> class="active" <?php } ?>  ><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_msg.png" alt="" /><span>preferences</span></a></li>
			<li><a href="<?php echo base_url("signup/logout/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/side_arrow.png" alt="" /><span>Log out</span></a></li>
		</ul>
	</div>
    
    
    
    
    
    
        
	
    
    <?php
    }
    
    ?>
    
    
    
</div>