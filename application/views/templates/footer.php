<script language="javascript">
setInterval(makeNotificationRequest, (5 * 1000));
</script>

<form name="remembermeform" method="POST" action="">
	<input type="hidden" name="remember" value="me" />
	<input type="hidden" name="remember_email" value="" />
	<input type="hidden" name="remember_pw" value="" />
</form>
<?php 
$about_data = $this->Admin_Model->get_content_pages_detail('1');
$contact_data = $this->Admin_Model->get_content_pages_detail('2');		
$terms_data = $this->Admin_Model->get_content_pages_detail('3');		
$privacy_data = $this->Admin_Model->get_content_pages_detail('4');		

?>
<div class="footer_bg slide" id="slide7" data-slide="7">
  <div class="footer_main">
    <div class="footer_left">
      <ul>
        <?php if($contact_data[0]->page_status=='1'){?><li><a href="<?php echo base_url("home/contact/"); ?>">Contact</a></li><?php } ?>
        <li><a href="<?php echo base_url("home/faq/"); ?>">FAQs</a></li>
        <li><a href="<?php echo base_url("blog/"); ?>">Blog</a></li>
        <?php if($about_data[0]->page_status=='1'){?><li><a href="<?php echo base_url("home/about/"); ?>">About</a></li><?php } ?>
        <?php if($terms_data[0]->page_status=='1'){?><li><a href="<?php echo base_url("home/terms/"); ?>">T&C’s </a></li><?php } ?>
        <?php if($privacy_data[0]->page_status=='1'){?><li><a href="<?php echo base_url("home/privacy/"); ?>">Privacy</a></li><?php } ?>
        <li class="last"><a href="<?php echo base_url("home/listyourbusiness/"); ?>">Merchant: List your Business</a></li>
      </ul>
      <p>&copy; Copyright <?=date("Y")?> Ondi Trading Pty Ltd. <a href="http:///www.ruby6.com.au" target="_blank">Website design and development by Ruby6</a></p>
    </div>
    <!--End of footer_left-->

	<?php		
		$admin_data = $this->Admin_Model->get_admin_detail('1');		
		$twitter_link = $admin_data[0]->twitter_link;
		$facebook_link = $admin_data[0]->facebook_link;
		$youtube_link = $admin_data[0]->youtube_link;	
		$gacode = $admin_data[0]->gacode;	
	?>
    <div class="footer_right"> 
	<a href="<?php echo $twitter_link; ?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/twitter.jpg" alt="" /></a> 
	<a href="<?php echo $facebook_link; ?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/facebook.jpg" alt="" /></a> 
	<a href="<?php echo $youtube_link ?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/you_tube.jpg" alt="" /></a> 
	</div>
    <!--End of footer_right--> 
  </div>
  <!--End of footer_main--> 
</div>
<!--End of footer_bg-->
<div id="music_player"></div>

<?=$gacode?>

<div class="alert" id="footer_alerts" style="display:none;"></div>
<script language="javascript">
function hide_footer_notifications()
{
	document.getElementById('footer_alerts').style.display="none";
}
makeFooterAlerts();
/*setTimeout(function() {
    $('#footer_alerts').fadeOut('slow');
}, 10000);*/
setTimeout(function() {
    document.getElementById('footer_alerts').style.display="none";
}, 10000);
setInterval(makeFooterAlerts, (120 * 1000));
</script>
