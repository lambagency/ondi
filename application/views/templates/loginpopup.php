<?php
$params = array(
  'scope' => 'email,user_birthday',
  'redirect_uri' => base_url("signup/facebooklogin/")
);
$fbloginurl =  $this->facebook->getLoginUrl($params);
$gplusurl = "";
$openid = new Openid('ondi.com');
if(!$openid->mode)
{
	$openid->identity = 'https://www.google.com/accounts/o8/id';
	$openid->required = array('contact/email', 'namePerson/first');
	$gplusurl = $openid->authUrl();
}
elseif($_GET['openid_mode'] == 'cancel')
{
	//echo 'User has canceled authentication!';
}
else 
{	
	if($_GET['openid_mode'] == 'id_res')
	{
		$openid->validate();
		$userinfo = $openid->getAttributes();
		$email = $userinfo['contact/email'];
		$firstName = $userinfo['namePerson/first'];
		$this->customclass->googlepluslogin($email, $firstName);
	}
}
?>
<!-- Start Login_Dropdown -->
<div class="login_dd_bg">
  <div class="login_dd_main">
	<div class="login_top">
	  <div class="login_dd_left">
		<p>Login with...</p>
		<ul>
		  <li><a href="<?=$fbloginurl?>" target="_blank" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/login_fb.jpg" alt="" /></a></li>
		  <li><a href="<?php echo $this->config->item('base_url'); ?>signup/twitterlogin/"><img src="<?php echo $this->config->item('base_url'); ?>public/images/login_twitter.jpg" alt="" /></a></li>
		  <li><a href="<?=$gplusurl?>" target="_blank" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/login_g+.jpg" alt="" /></a></li>
		  <li>
			<p>or</p>
		  </li>
		</ul>
	  </div>
	 
	  <!--End of login_dd_left-->
	  <div class="login_dd_right">
	  <div id="ajaxloginmsg" style="display:none;"></div>
      	<div class="close"><img src="<?php echo $this->config->item('base_url'); ?>public/images/login_close.jpg" alt="" id="login_close" /></div>
		<input type="text" placeholder="Email address" class="position_login" value="<?php if(isset($_COOKIE['ondi_login_email'])) echo $_COOKIE['ondi_login_email']; ?>" name="popuplogin_email" id="popuplogin_email"  onkeyup="trim_value(this);" />
		<input type="password" placeholder="Password" class="position_login" value="<?php if(isset($_COOKIE['ondi_login_password'])) echo $_COOKIE['ondi_login_password']; ?>"  name="popuplogin_pwd" id="popuplogin_pwd" />
		<div class="forgot_password">
		  <div class="forgot_password_left">
			<input name="is_merchant" id="is_merchant" type="checkbox" onClick="var j = jQuery('#check').attr('disabled', jQuery('#check').attr('disabled') ? false : true)">
			<p>Merchant</p>
		  </div>
		  <div class="forgot_password_right">
			<p><a href="<?php echo base_url("signup/forgotpassword/"); ?>">Forgot password?</a></p>
		  </div>
		</div>
		<!--End of forgot_password--> 
		
	  </div>
	  <!--End of login_dd_right--> 
	</div>
	<!--End of login_top-->
	
	<div class="login_bottom">
	  <div class="login_dd_left">
		<h3>DON'T HAVE AN ACCOUNT? <a href="<?php echo base_url("signup/"); ?>">SIGN UP</a></h3>
	  </div>
	  <div class="login_dd_right">
		<div class="remember_pass">
		  <input name="rememberme" id="rememberme"  type="checkbox" checked="checked" onClick="var j = jQuery('#check').attr('disabled', jQuery('#check').attr('disabled') ? false : true)">
		  <p>remember me</p>
		</div>
		<div class="sign_in"><a href="javascript:void(0);" onclick="popupLogin();" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/sign_in.jpg" alt="" /></a> </div>
	  </div>
	</div>
	<!--End of login_bottom-->
	<div class="dd_arrow"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/dd_arrow.png" alt="" /> </div>
  </div>
  <!--End of login_dd_main--> 
</div>
<!--End of login_dd_bg--> 
<!-- End Login_Dropdown --> 