<?php

$this->customclass->commonclassfunction();
$sess_user_data = $this->session->all_userdata();

/*if ($this->agent->is_mobile())
{
	echo "mobile";
}
else
{
	echo "desktop";
}*/


if($this->CI_auth->check_logged()===TRUE)
{
	$loggedinuserdata = $this->customclass->getLoggedInUserData(); 
}
else
{
?>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/safari.js" type="text/javascript"></script> 
<?php
}
//print_r($sess_user_data); die;
if(!isset($sess_user_data['user_city_id']))
{
	$this->customclass->getusercity(); 
}

if($sess_user_data['user_type']=='admin')
{	
	$this->session->sess_destroy();
    redirect('/', 'refresh');
}	
//print_r($sess_user_data);
//die;


?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/onallpages.js"></script>



<div class="header">
	<div class="logo">
		<a href="<?php echo $this->config->item('base_url'); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/logo.png" alt="" /></a>
	</div>
    <div class="mobile_nav">
    	<img src="<?php echo $this->config->item('base_url'); ?>public/images/mobile_menu.jpg" alt="" />
    </div>
    
    <div class="select_city_bg_mobile">
            <p>Select your city</p>
			<?php 
			if(isset($sess_user_data['user_city_id']))
			{
				$user_city_name = $sess_user_data['user_city_name'];
				$user_city_id = $sess_user_data['user_city_id'];
			}
			else
			{
				$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
				$default_city_data = $default_city_query->result();
				$user_city_name = $default_city_data[0]->city_name;
				$user_city_id = $default_city_data[0]->id;
			}
			?>
            <span class="select_city"><img src="<?php echo $this->config->item('base_url'); ?>public/images/city_arrow.jpg" alt=""> <?=$user_city_name?></span>
            <ul class="city">
              <?php 
			  $cities = $this->customclass->getcities(); 
			  foreach($cities as $thecity){ ?>
			  <li><a href="<?php echo $this->config->item('base_url'); ?>?set_city=<?=$thecity->id?>"><?=$thecity->city_name?></a></li>
			  <?php } ?>
            </ul>
            
        </div>
        
        
	<div class="nav">
		<div class="select_city_bg">
            <p>Select your city</p>
			<?php 
			if(isset($sess_user_data['user_city_id']))
			{
				$user_city_name = $sess_user_data['user_city_name'];
				$user_city_id = $sess_user_data['user_city_id'];
			}
			else
			{
				$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
				$default_city_data = $default_city_query->result();
				$user_city_name = $default_city_data[0]->city_name;
				$user_city_id = $default_city_data[0]->id;
			}
			?>
            <span class="select_city"><img src="<?php echo $this->config->item('base_url'); ?>public/images/city_arrow.jpg" alt=""> <?=$user_city_name?></span>
            <ul class="city">
              <?php 
			  $cities = $this->customclass->getcities(); 
			  foreach($cities as $thecity){ ?>
			  <li><a href="<?php echo $this->config->item('base_url'); ?>?set_city=<?=$thecity->id?>"><?=$thecity->city_name?></a></li>
			  <?php } ?>
            </ul>
        </div>
        
        
        
        
		<ul class="navigation" id="nav2">
			<li class="how"><a id="slide5_but" href="<?php echo $this->config->item('base_url'); ?>#slide5">How it works</a></li>
			<li class="business"><a id="slide6_but" href="<?php echo $this->config->item('base_url'); ?>#get_local_news_text">For Businesses</a></li>
			
            <li class="<?php if($this->CI_auth->check_logged()!==TRUE){ echo "nonlog_shortlist";} else { echo "shortlist"; } ?> ">
				<?php if($this->CI_auth->check_logged()===TRUE){ ?>
					<?php if($sess_user_data['user_type']=='1'){?>
						<!-- <a onclick="window.location = '<?php echo base_url("customeraccount/shortlists/"); ?>';" class="short_list">SHORT LIST</a> -->
					<?php } else { ?>
						<a onclick="window.location = '<?php echo base_url("home/myshortlists/"); ?>';" class="short_list">SHORT LIST</a>
					<?php } ?>
				<?php } else { ?>
						<a onclick="window.location = '<?php echo base_url("home/myshortlists/"); ?>';" class="short_list">SHORT LIST</a>
				<?php } ?>
			</li>
			
			
			
			<?php if($this->CI_auth->check_logged()===TRUE){ ?>
			<li class="user <?php if($sess_user_data['user_type']=='1') { echo "cust_user"; } ?>" >
				<a href="javascript:void(0);" class="log_inner" >
					<span class="member_img">
					<?php
					if($sess_user_data['user_type']=='1'){
						if(isset($loggedinuserdata[0]) && $loggedinuserdata[0]->customer_picture != ''){
						?>
						<img src="<?php echo $this->config->item('base_url'); ?><?=$loggedinuserdata[0]->customer_picture?>" alt="" width="34" height="34"/>
						<?php
						}
						else
						{
							$facebook_id = $loggedinuserdata[0]->facebook_id;
							if($facebook_id != '')
							{
								$profile_picture = "http://graph.facebook.com/".$facebook_id."/picture?type=small";
								?>
								<img src="<?php echo $profile_picture; ?>" alt="" width="34" height="34" />
								<?php
							}
							else
							{
								if($loggedinuserdata[0]->gender=='M')
								{
								?>
								<img src="<?php echo $this->config->item('base_url'); ?>public/images/male.jpg" alt="" />
								<?php	
								}
								else if($loggedinuserdata[0]->gender=='F')
								{
								?>
								<img src="<?php echo $this->config->item('base_url'); ?>public/images/female.jpg" alt="" />
								<?php
								}
								else 
								{
								?>
								<img src="<?php echo $this->config->item('base_url'); ?>public/images/male.jpg" alt="" />
								<?php
								}
							}
						}
					}
					else
					{
						if(isset($loggedinuserdata[0]) && $loggedinuserdata[0]->profile_picture != ''){
						?>
						<img src="<?php echo $this->config->item('base_url'); ?><?=$loggedinuserdata[0]->profile_picture?>" alt="" width="34" height="34"/>
						<?php
						}
						else
						{
							if($loggedinuserdata[0]->gender=='M')
							{
							?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/male.jpg" alt="" />
                            <?php	
							}
							else if($loggedinuserdata[0]->gender=='F')
							{
							?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/female.jpg" alt="" />
                            <?php
							}
							else 
							{
							?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/merchant-noimage.jpg" alt="" width="34" height="34" />
                            <?php
							}
						}
					}
					?>
					</span>
                    
                    <span class="name_title">
                    
                    
					<?php
					if($this->CI_auth->check_logged()===TRUE)
					{	
						$sess_user_data = $this->session->all_userdata();	
						$loggedinuserdata = $this->customclass->getLoggedInUserData(); 
						//print_r($loggedinuserdata);
						if($loggedinuserdata[0]->contact_name != '') 
						{
							if(strlen($loggedinuserdata[0]->contact_name)>6)
							{
								echo 'Hi, '.substr($loggedinuserdata[0]->contact_name, 0, 6)."..";
							}
							else
							{
								echo 'Hi, '.$loggedinuserdata[0]->contact_name;
							}
						}	
						else if($loggedinuserdata[0]->first_name != '') 
						{
							if(strlen($loggedinuserdata[0]->first_name)>6)
							{
								echo 'Hi, '.substr($loggedinuserdata[0]->first_name, 0, 6)."..";
							}
							else
							{
								echo 'Hi, '.$loggedinuserdata[0]->first_name;
							}
						}
					}
					?>
                    
                    </span>
				</a> 
                
                
				<div class="logout_sub">
					<ul>
						<?php
						if($this->CI_auth->check_logged()===TRUE)
						{
							$logged_user_access_type = $this->users->get_user_access_type();
							if($sess_user_data['user_type']=='1')
							{
							?>
								<li class=""><a onclick="window.location = '<?php echo base_url("customeraccount/mybookings/"); ?>';">My Bookings</a></li>
								<li class=""><a onclick="window.location = '<?php echo base_url("customeraccount/shortlists/"); ?>';">My Shortlist</a></li>
							<?php
							}
							else
							{
								//echo "logged_user_access_type-->".$logged_user_access_type;
							?>
								<?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("6",$logged_user_access_sections) || in_array("7",$logged_user_access_sections)  || in_array("8",$logged_user_access_sections) ){ ?>
									<li><a onclick="window.location = '<?php echo base_url("merchantaccount/bookingslist/"); ?>';" >Bookings</a></li>
								<?php } ?>

								<?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
									<li><a onclick="window.location = '<?php echo base_url("merchantaccount/addoffers/"); ?>';" >Add offer</a></li>
								<?php } ?>
							<?php
							}
							?>
								<li><a onclick="window.location = '<?php echo base_url("signup/logout/"); ?>';">Log out</a></li>
							<?php
						}
						?>
					</ul>
				</div>       
			</li>
			<?php
			$user_notifications_data = $this->users->get_user_notification_data();
			$unread_notifications = $user_notifications_data['unread'];
			$notifications = $user_notifications_data['notifications_data'];
			//print_r($notifications);
			
			
			if($sess_user_data['user_type']=='1')
			{
				$notifications_page_url = base_url("customeraccount/mynotifications/");
			} 
			else 
			{
				$notifications_page_url = base_url("merchantaccount/mynotifications/");
			} 
			?>
			
				<?php if (!$this->agent->is_mobile()){?>
				<li class="last <?php if($sess_user_data['user_type']=='1') { echo "cust_notification"; } ?>" id="li_ajax_notifications">
					<?php if($unread_notifications>0){?>
					<a href="<?=$notifications_page_url?>" class="comment_nav"><font id="num_unreadnotify"><?=$unread_notifications?></font></a>
					<?php } else{ ?>
					<a href="<?=$notifications_page_url?>" class="comment_nav_inactive"><font id="num_unreadnotify">&nbsp;</font></a>
					<?php } ?>

					<div class="notification" <?php if($unread_notifications==0){ echo "style='display:none;'"; } ?> >
						<div class="dd_arrow_small"><img src="<?php echo $this->config->item('base_url'); ?>public/images/dd_arrow_small.png" alt="" /></div>
						<h3>Notifications</h3>
						<ul>
							<?php 
							$ni=0;
							foreach($notifications as $this_notification)
							{
								$ni++;
								if($sess_user_data['user_type']=='1')
								{
									$read_status = $this_notification->customer_read_status;
								}
								else
								{
									$read_status = $this_notification->merchant_read_status;
								}
							?>
							<li <?php if($read_status=='0'){ echo 'class="active"'; } ?>>
								<?php if($this_notification->profile_message == '1'){?>
								<div class="left_note"><img src="<?php echo $this->config->item('base_url'); ?>public/images/notification.png" alt="" /></div>
								<?php } else { ?>
								<div class="left_note"><div class="calender">23</div></div>
								<?php } ?>

								<div class="right_note"><?=$this_notification->message?></div>
								<div class="date_note"><?=$this->customclass->_ago(strtotime($this_notification->added_date))?>  / <span <?php if($read_status=='1'){ echo 'class="normal"'; } else { echo 'class="bold"'; } ?> onclick="change_read_status(<?=$this_notification->id?>);" id="span_notification_<?=$this_notification->id?>" >Mark as <?php if($read_status=='1'){ echo 'unread'; } else { echo 'Read'; } ?></span> </div>
							</li>
							<?php 
							if($ni ==5) break;
							} ?>
						</ul>
						<?php if(sizeof($notifications)>5){?>
							<span class="read_more">
								<?php if($sess_user_data['user_type']=='1'){?>
									<a href="<?php echo base_url("customeraccount/mynotifications/"); ?>">Load More</a>
								<?php } else {?>
									<a href="<?php echo base_url("merchantaccount/mynotifications/"); ?>">Load More</a>
								<?php } ?>
							</span>
						<?php } ?>
					</div>
				</li>
				<?php } ?>



			<?php }else{ ?>
				<?php
				$firsttimeuser = $this->customclass->checkfirsttimeuser();
				if($firsttimeuser == 'yes')
				{
				?>
				<li class="log_in_outer"><a class="log_in">LOG IN</a></li>
				<?php
				}
				else
				{
				?>
				<li class="log_in_outer"><a class="log_in_c">LOG IN</a></li>
				<?php 
				}
				?>
				<li class="last sign_up"><a class="sign_up" onclick="window.location = '<?php echo base_url("signup/"); ?>';" >SIGN UP</a></li>

			<?php } ?>


			<!-- <li style="display:none;"><a href="#" class="log_in">Log Out</a></li>			
			<li class="last"><a href="#" class="comment_nav">3</a></li> -->

		</ul>
	</div>
</div>
<?php $this->load->view('templates/loginpopup');?>
<!-- <script language="javascript">
setInterval(makeNotificationRequest, (5 * 1000));
</script>
 -->

