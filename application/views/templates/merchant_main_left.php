<?php
/*
echo $logged_user_access_type;
if(isset($logged_user_access_sections))
{
	echo '<pre>';
	print_r($logged_user_access_sections);
	echo '</pre>';

}
*/
$session_user_data = $this->session->all_userdata();	
$user_steps_completed = $this->users->user_steps_completed($session_user_data['logged_user']);
?>

<div class="merchant_mobile_menu"></div>
<div class="merchant_main_left">


<?php if (!$this->agent->is_mobile())
    {
    ?>
    
    <div class="sidebar">
	<ul>
	  
	  <?php  if($logged_user_access_type=='1' || in_array("1",$logged_user_access_sections)){ ?>
	  <li>
		 <?php if($user_steps_completed>0){?>
			<a href="<?php echo base_url("merchantaccount/viewcontactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo' || $this->uri->segment(2)=='viewcontactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Contact Info</a>
		 <?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/contactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo' || $this->uri->segment(2)=='viewcontactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Contact Info</a>
		 <?php } ?>
		</li>
	  <?php } ?>

	  <?php  if($logged_user_access_type=='1' || in_array("2",$logged_user_access_sections)){ ?>
	  <li>
		 <?php if($user_steps_completed>1){?>
			<a href="<?php echo base_url("merchantaccount/viewbusinessprofile/"); ?>" <?php if($this->uri->segment(2)=='viewbusinessprofile' || $this->uri->segment(2) ==  'businessprofile'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon2.png" alt="" />Business Profile</a>
		<?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/businessprofile/"); ?>" <?php if($this->uri->segment(2)=='viewbusinessprofile' || $this->uri->segment(2) ==  'businessprofile'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon2.png" alt="" />Business Profile</a>
		<?php } ?>
	  </li>
	  <?php } ?>


	  <?php  if($logged_user_access_type=='1' || in_array("3",$logged_user_access_sections)){ ?>
	  <li>
		<?php if($user_steps_completed>2){?>
			<a href="<?php echo base_url("merchantaccount/viewbankingdetails/"); ?>" <?php if($this->uri->segment(2)=='bankingdetails' || $this->uri->segment(2)=='viewbankingdetails'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon3.png" alt="" />Banking Details</a>
		<?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/bankingdetails/"); ?>" <?php if($this->uri->segment(2)=='bankingdetails' || $this->uri->segment(2)=='viewbankingdetails'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon3.png" alt="" />Banking Details</a>
		<?php } ?>
	  </li>
	  <?php } ?>
	  
	  
	  <?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("4",$logged_user_access_sections) || in_array("5",$logged_user_access_sections)){ ?>

	  <li><a href="<?php echo base_url("merchantaccount/addoffers/"); ?>" <?php if($this->uri->segment(2)=='addoffers'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon4.png" alt="" />Offers</a>

		<?php if($this->uri->segment(2)=='profilecompleted'){?><div class="offer_tip">Start by adding your offers here!</div><?php } ?>	
		
		  <div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>

				  <?php  if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/addoffers/"); ?>"  <?php if($this->uri->segment(2)=='addoffers'){?> class="active" <?php } ?> >Add new offer</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("4",$logged_user_access_sections)){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/liveoffers/"); ?>"  <?php if($this->uri->segment(2)=='liveoffers'){?> class="active" <?php } ?> >Live Offers</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1'){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/offerhistory/"); ?>"  <?php if($this->uri->segment(2)=='offerhistory'){?> class="active" <?php } ?> >Offer history</a></li>
				  <?php } ?>


				</ul>
		  </div>
	  </li>
	  <?php } ?>
	 
	  
	  
	   <?php  if($logged_user_access_type=='1' || in_array("6",$logged_user_access_sections) || in_array("7",$logged_user_access_sections)  || in_array("8",$logged_user_access_sections) ){ ?>
	  <li><a href="<?php echo base_url("merchantaccount/bookingscalender/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon5.png" alt="" />Bookings</a>
		<div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>
				  
				  <?php  if($logged_user_access_type=='1' || in_array("6",$logged_user_access_sections) || in_array("7",$logged_user_access_sections) ){ ?>
						<li><a href="<?php echo base_url("merchantaccount/bookingscalender/"); ?>"  <?php if($this->uri->segment(2)=='bookingscalender'){?> class="active" <?php } ?> >Calendar view</a></li>
						<li><a href="<?php echo base_url("merchantaccount/bookingslist/"); ?>"  <?php if($this->uri->segment(2)=='bookingslist'){?> class="active" <?php } ?> >List view</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1' || in_array("8",$logged_user_access_sections)){ ?>	
					<li><a href="<?php echo base_url("merchantaccount/bookingshistory/"); ?>"  <?php if($this->uri->segment(2)=='bookingshistory'){?> class="active" <?php } ?> >Booking history</a></li>
				  <?php } ?>

				</ul>
		  </div>
	  </li>
	  <?php } ?>


	  <?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2'  || in_array("9",$logged_user_access_sections)){ ?>
		 <li><a href="<?php echo base_url("merchantaccount/statistics/"); ?>" <?php if($this->uri->segment(2)=='statistics'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon6.png" alt="" />Statistics</a></li>
	  <?php } ?>

	  <?php  if($logged_user_access_type=='1'){ ?>
	  <li><a href="<?php echo base_url("merchantaccount/addusers/"); ?>" <?php if($this->uri->segment(2)=='userslist'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon7.png" alt="" />Users</a>	  
		<div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>
				  <li><a href="<?php echo base_url("merchantaccount/addusers/"); ?>"  <?php if($this->uri->segment(2)=='addusers'){?> class="active" <?php } ?> >Add Users</a></li>
				  <li><a href="<?php echo base_url("merchantaccount/userslist/"); ?>"  <?php if($this->uri->segment(2)=='addusers'){?> class="active" <?php } ?> >Users List</a></li>
				</ul>
		  </div>
	  </li>
	  <?php } ?>

	  <li><a href="<?php echo base_url("signup/logout/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon8.png" alt="" />Log out</a></li>
	</ul>
  </div>
  
  
    <?php	
    }
    else
    {
    
    ?>
       
       <div class="sidebar">
	<ul <?php  if($logged_user_access_type=='1'){ ?> class="main_user" <?php } ?>>
	  
	  <?php  if($logged_user_access_type=='1' || in_array("1",$logged_user_access_sections)){ ?>
	  <li>
		 <?php if($user_steps_completed>0){?>
			<a href="<?php echo base_url("merchantaccount/viewcontactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo' || $this->uri->segment(2)=='viewcontactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" /><span>Info</span></a>
		 <?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/contactinfo/"); ?>" <?php if($this->uri->segment(2)=='contactinfo' || $this->uri->segment(2)=='viewcontactinfo'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" /><span>Info</span></a>
		 <?php } ?>
		</li>
	  <?php } ?>

	  <?php  if($logged_user_access_type=='1' || in_array("2",$logged_user_access_sections)){ ?>
	  <li>
		 <?php if($user_steps_completed>1){?>
			<a href="<?php echo base_url("merchantaccount/viewbusinessprofile/"); ?>" <?php if($this->uri->segment(2)=='viewbusinessprofile' || $this->uri->segment(2) ==  'businessprofile'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon2.png" alt="" /><span> Profile</span></a>
		<?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/businessprofile/"); ?>" <?php if($this->uri->segment(2)=='viewbusinessprofile' || $this->uri->segment(2) ==  'businessprofile'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon2.png" alt="" /><span> Profile</span></a>
		<?php } ?>
	  </li>
	  <?php } ?>


	  <?php  if($logged_user_access_type=='1' || in_array("3",$logged_user_access_sections)){ ?>
	  <li>
		<?php if($user_steps_completed>2){?>
			<a href="<?php echo base_url("merchantaccount/viewbankingdetails/"); ?>" <?php if($this->uri->segment(2)=='bankingdetails' || $this->uri->segment(2)=='viewbankingdetails'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon3.png" alt="" /><span>Banking </span></a>
		<?php } else { ?>
			<a href="<?php echo base_url("merchantaccount/bankingdetails/"); ?>" <?php if($this->uri->segment(2)=='bankingdetails' || $this->uri->segment(2)=='viewbankingdetails'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon3.png" alt="" /><span>Banking </span></a>
		<?php } ?>
	  </li>
	  <?php } ?>
	  
	  
	  <?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("4",$logged_user_access_sections) || in_array("5",$logged_user_access_sections)){ ?>

	  <li><a href="javascript:void(0)" <?php if($this->uri->segment(2)=='addoffers'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon4.png" alt="" /><span>Offers</span></a>

		<?php if($this->uri->segment(2)=='profilecompleted'){?><div class="offer_tip">Start by adding your offers here!</div><?php } ?>	
		
		  <div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>

				  <?php  if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/addoffers/"); ?>"  <?php if($this->uri->segment(2)=='addoffers'){?> class="active" <?php } ?> >Add new offer</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("4",$logged_user_access_sections)){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/liveoffers/"); ?>"  <?php if($this->uri->segment(2)=='liveoffers'){?> class="active" <?php } ?> >Live Offers</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1'){ ?>
				  <li><a href="<?php echo base_url("merchantaccount/offerhistory/"); ?>"  <?php if($this->uri->segment(2)=='offerhistory'){?> class="active" <?php } ?> >Offer history</a></li>
				  <?php } ?>


				</ul>
		  </div>
	  </li>
	  <?php } ?>
	 
	  
	  
	   <?php  if($logged_user_access_type=='1' || in_array("6",$logged_user_access_sections) || in_array("7",$logged_user_access_sections)  || in_array("8",$logged_user_access_sections) ){ ?>
	  <li><a href="javascript:void(0)"><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon5.png" alt="" /><span>Bookings</span></a>
		<div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>
				  
				  <?php  if($logged_user_access_type=='1' || in_array("6",$logged_user_access_sections) || in_array("7",$logged_user_access_sections) ){ ?>
						<li><a href="<?php echo base_url("merchantaccount/bookingscalender/"); ?>"  <?php if($this->uri->segment(2)=='bookingscalender'){?> class="active" <?php } ?> >Calendar view</a></li>
						<li><a href="<?php echo base_url("merchantaccount/bookingslist/"); ?>"  <?php if($this->uri->segment(2)=='bookingslist'){?> class="active" <?php } ?> >List view</a></li>
				  <?php } ?>

				  <?php  if($logged_user_access_type=='1' || in_array("8",$logged_user_access_sections)){ ?>	
					<li><a href="<?php echo base_url("merchantaccount/bookingshistory/"); ?>"  <?php if($this->uri->segment(2)=='bookingshistory'){?> class="active" <?php } ?> >Booking history</a></li>
				  <?php } ?>

				</ul>
		  </div>
	  </li>
	  <?php } ?>


	  <?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2'  || in_array("9",$logged_user_access_sections)){ ?>
		 <li><a href="<?php echo base_url("merchantaccount/statistics/"); ?>" <?php if($this->uri->segment(2)=='statistics'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon6.png" alt="" /><span>Stats</span></a></li>
	  <?php } ?>

	  <?php  if($logged_user_access_type=='1'){ ?>
	  <li><a href="<?php echo base_url("merchantaccount/addusers/"); ?>" <?php if($this->uri->segment(2)=='userslist'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon7.png" alt="" /><span>Users</span></a>	  
		<div class="submenu">
				<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
				<ul>
				  <li><a href="<?php echo base_url("merchantaccount/addusers/"); ?>"  <?php if($this->uri->segment(2)=='addusers'){?> class="active" <?php } ?> >Add Users</a></li>
				  <li><a href="<?php echo base_url("merchantaccount/userslist/"); ?>"  <?php if($this->uri->segment(2)=='addusers'){?> class="active" <?php } ?> >Users List</a></li>
				</ul>
		  </div>
	  </li>
	  <?php } ?>

	  <li><a href="<?php echo base_url("signup/logout/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon8.png" alt="" /><span>Log out</span></a></li>
	</ul>
  </div>
    
    <?php
    }
    
    ?>

</div>