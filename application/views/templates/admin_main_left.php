<div class="merchant_main_left">
	<div class="sidebar">
		<ul>
			<li>
				<a href="<?php echo $this->config->item('base_url'); ?>admin/dashboard/" <?php if($this->uri->segment(2)=='dashboard'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Dashboard</a>
			</li>
			<li>
				<a href="javascript:void(0);" <?php if($this->uri->segment(2)=='globalsettings'  || $this->uri->segment(2)=='lybinfo'  || $this->uri->segment(2)=='signupinfo'  || $this->uri->segment(2)=='viewpromocode'  || $this->uri->segment(2)=='addpromocode'  || $this->uri->segment(2)=='viewcity'  || $this->uri->segment(2)=='addcity'  || $this->uri->segment(2)=='whatsondemand'  || $this->uri->segment(2)=='viewslideshowimages'  || $this->uri->segment(2)=='addcityimage'  || $this->uri->segment(2)=='viewinterests'  || $this->uri->segment(2)=='addinterests'  || $this->uri->segment(2)=='viewsubscribers' || $this->uri->segment(2)=='viewuserpositions' || $this->uri->segment(2)=='adduserposition'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Website Settings</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/globalsettings/">Global Settings</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/lybinfo/">List your business info</a>
						</li>

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/signupinfo/">Sign up info</a>
						</li>

						<!-- <li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/whatsondemand/">Manage Whats on Demand</a>
						</li> -->

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewpromocode/">View Promo Code</a>
						</li>

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addpromocode/">Add Promo Code</a>
						</li>


						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcity/">View Cities</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addcity/">Add City</a>
						</li>

						
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewslideshowimages/">View Slideshow Image</a>
						</li>

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addcityimage/">Add Slideshow Image</a>
						</li>

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewinterests/">View Things I Love</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addinterests/">Add Things I Love</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewsubscribers/">View Subscribers</a>
						</li>

						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewuserpositions/">View Positions</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/adduserposition/">Add Position</a>
						</li>

					</ul>
				</div>	
			</li>
			<li>
				<a href="javascript:void(0);" <?php if($this->uri->segment(2)=='managepages'  || $this->uri->segment(2)=='editcontentpage'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Manage Pages</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/managepages/">View Pages</a>
						</li>
					</ul>
				</div>	
			</li>
			<li>
				<a href="javascript:void(0);"  <?php if($this->uri->segment(2)=='managefaqcategory' || $this->uri->segment(2)=='addeditfaqcategory' || $this->uri->segment(2)=='managefaq' || $this->uri->segment(2)=='addeditfaq'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Manage FAQ</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/managefaqcategory/">View Categories</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addeditfaqcategory/">Add category</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/managefaq/">View FAQs</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addeditfaq/">Add FAQ</a>
						</li>
					</ul>
				</div>	
			</li>
			<li>
				<a href="javascript:void(0);" <?php if($this->uri->segment(2)=='viewblogcategories' || $this->uri->segment(2)=='addblogcategory' || $this->uri->segment(2)=='editblogcategory' || $this->uri->segment(2)=='viewblogposts'|| $this->uri->segment(2)=='addblogpost' || $this->uri->segment(2)=='editblogpost'|| $this->uri->segment(2)=='viewblogtags'|| $this->uri->segment(2)=='addblogtag'|| $this->uri->segment(2)=='editblogtag'|| $this->uri->segment(2)=='viewblogcomments'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Manage Blog</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewblogcategories/">View Category</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addblogcategory/">Add category</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewblogposts/">View Posts</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addblogpost/">Add Post</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewblogtags/">View Tags</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addblogtag/">Add Tag</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewblogcomments/">View Comments</a>
						</li>
					</ul>
				</div>	
			</li>
			<li>
				<a href="javascript:void(0);" <?php if($this->uri->segment(2)=='approveoffers' || $this->uri->segment(2)=='assignpassword' || $this->uri->segment(2)=='viewmerchantlist' || $this->uri->segment(2)=='viewbusinesstypes' || $this->uri->segment(2)=='addbusinesstype'|| $this->uri->segment(2)=='editbusinesstype' || $this->uri->segment(2)=='viewserviceoffered'|| $this->uri->segment(2)=='addserviceoffered'|| $this->uri->segment(2)=='ediserviceoffered'|| $this->uri->segment(2)=='viewperfectfor'|| $this->uri->segment(2)=='addperfectfor'|| $this->uri->segment(2)=='editperfectfor'){?> class="active" <?php } ?> ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Merchant Settings</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/assignpassword/">Assign Password</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addmerchant/">Add Merchant</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/approveoffers/">Offers</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewmerchantlist/">View Merchant List</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewbusinesstypes/">View Business Types</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addbusinesstype/">Add Business Type</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewserviceoffered/">View Service Offered</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addserviceoffered/">Add Service Offered</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewperfectfor/">View Perfect For</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addperfectfor/">Add Perfect For</a>
						</li>
					</ul>
				</div>	
			</li>
			<li>
				<a <?php if($this->uri->segment(2)=='viewcustomerlist' || $this->uri->segment(2)=='viewpaymentcategories' || $this->uri->segment(2)=='editpaymentcategory' || $this->uri->segment(2)=='addpaymentcategory'){?> class="active" <?php } ?>  href="javascript:void(0);" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/m_icon1.png" alt="" />Customer Settings</a>
				<div class="submenu">
					<span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/triangle.png" alt="" /></span>
					<ul>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/">View Customer List</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/viewpaymentcategories/">View Payment Categories</a>
						</li>
						<li>
							<a href="<?php echo $this->config->item('base_url'); ?>admin/addpaymentcategory/">Add Payment Category</a>
						</li>
					</ul>
				</div>	
			</li>
		</ul>
	</div>
</div>