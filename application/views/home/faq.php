<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>





<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_faq.js"></script>

<?php
}
?>



</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>

<!--Start of FAQ Page_Content-->
<div class="inner_content_bg6">
	<div class="faq_content">
    	<div class="heading">
        	<h1>FREQUENTLY ASKED QUESTIONS</h1>
        </div>
        <div class="sub_heading">
        	<h3>Below are some questions and answers that will hopefully guide you through our unique process.</h3>
        </div>
        <div class="faq_content_detail">
			<?php 
			$f=0;
			foreach($catdata as $thisfaqcat)
			{
				$f++;
				if($f>3) $f=1;
				$faqs = $this->faq->faqofcat($thisfaqcat->cat_id);
				?>
				<div class="faq<?=$f?>">
					<h2><?=$thisfaqcat->category?></h2>
					<ul class="accordion">
						<?php 
						foreach($faqs as $thisfaq)
						{
						?>
						<li><a href="javascript:void(0);"><?=$thisfaq->faq_question?></a>
							<div>
								<p><?=$thisfaq->faq_answer?></p>
							</div>
						</li>
						<?php
						}
						?>
					</ul>
				</div>
			<?php 
			} 
			?>
        </div>
    </div><!--End of my_short_list_content-->
</div>
<!--End of FAQ Page_Content-->

<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/faq/dependencies/jquery-1.3.2.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/faq/jquery.accordion.source.js" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		// <![CDATA[
			
		$(document).ready(function () {
			$('.accordion').accordion();
		});
				
		// ]]>
	</script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 

    
</body>
</html>
