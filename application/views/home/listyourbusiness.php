<?php
$lybinfo_data = $this->Admin_Model->get_lybinfo();		
$box1 = $lybinfo_data[0]->box1;
$box2 = $lybinfo_data[0]->box2;
$box3 = $lybinfo_data[0]->box3;
$box4 = $lybinfo_data[0]->box4;
$box5 = $lybinfo_data[0]->box5;
$box6 = $lybinfo_data[0]->box6;
$box7 = $lybinfo_data[0]->box7;
?>
<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Services on demand - Ondi.com</title>
        <script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
		<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
		<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
		<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
		<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
		<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
		<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
		<!--[if lt IE 8]>
		<div style=' clear: both; text-align:center; position: relative;'>
		<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
		</div>
		<![endif]-->
		<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<!--FOR SEARCH DROPDOWN-->
		<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
		<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
		<!--CUSTOM FUNCTION-->
		<?php if ($this->agent->is_mobile())
		{
		?>
			<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
		<?php	
			}
		else
		{
		?>
		
		
		<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
		
		<?php
		}
		?>
		<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
		<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
        
		<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

	</head>
	<body>
		<?php $this->load->view('templates/header');?>
		<?php $this->load->view('templates/booking_top');?>
		<div class="list_business_banner slide" data-stellar-background-ratio="0.5">
			<div class="list_business_text">
				<img src="<?php echo $this->config->item('base_url'); ?>public/images/list_your_business_text.png" alt="" />
			</div>
			<div class="banner_con">
				<ul>
					<li class="purple"><?=$box1?></li>
					<li class="green gray_box"><?=$box2?></li>
					<li class="skyblue"><?=$box3?></li>
					<li class="yellow gray_box"><?=$box4?></li>
					<li class="orenge"><?=$box5?></li>
					<li class="pink gray_box"><?=$box6?></li>
					<li class="white gray_box fill_length"><?=$box7?></li>
				</ul>
			</div>
		</div>
		<!--End of list_business Banner Section-->
		<!--Start of list_your_business Page_Content-->
		<div class="inner_content_bg3">
			<div class="list_business_content">
				<div class="heading">
					<h1>LIST YOUR BUSINESS HERE</h1>
				</div>
				<div class="sub_heading">
					<h3>Enter your details below to sumbit an application to become one of our merchants.<br/>We will contact you within 48hrs regarding your registration request.</h3>
				</div>
				<a name="msg"></a>
				<div class="list_business_detail">
					<?php echo form_open_multipart(base_url()."home/listyourbusiness/", 'onsubmit="return listyourbusiness_validate();"')?>
						<input type="hidden" name="submit" value="1" />
						<div class="heading">
							<h1>Business Information</h1>
						</div>
						<!-- <div class="valid_errors">
							<font color="red">
							<?php 
							echo validation_errors(); 
							//echo $this->session->flashdata('signup_message');
							echo $signup_message;
							if(isset($_GET['act']) && $_GET['act']=='suc') echo "Thanks! We will contact you soon.";
							?>
							</font>
						</div> -->
						<div class="sub_heading" >
							<h3 style="font-size:20px;">
								<font color="red">
									<?php 
									echo validation_errors(); 
									//echo $this->session->flashdata('signup_message');
									echo $signup_message;
									if(isset($_GET['act']) && $_GET['act']=='suc') echo "Thanks! We will contact you soon.";
									?>
								</font>
							</h3>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>
						<ul>
							<li><input type="text" placeholder="*Business Name" class="position_binfo" id="business_name" name="business_name" value="<?=set_value('business_name')?>" /></li>
							<li><input type="text" placeholder="*Website Address" class="position_binfo" id="business_website_address" name="business_website_address" value="<?=set_value('business_website_address')?>"  /></li>
							<li><input type="text" placeholder="*Street Address" class="position_binfo" id="business_street_address" name="business_street_address" value="<?=set_value('business_street_address')?>"/></li>
							<li>
								<table width="100%">
									<tr>
										<td >
											<div class="select2">
												<div id="div_categories">Category</div>
												<img src="<?php echo $this->config->item('base_url'); ?>public/images/select_flex_arrow.png" border="0"/>
												<div class="drop" id="div_categories_drop">
													<span>
														<a href="javascript:void(0);" onclick="uncheckall('checkboxesgroup');">Clear all..</a>
													</span>
													<ul id="checkboxesgroup">
														<?php foreach($business_types as $the_type){?>	
														<li><input class="class_business_type" type="checkbox" value="<?=$the_type->type_id?>" name="business_type[]" <?php if(in_array($the_type->type_id, $array_selected_business_types)){ echo "checked='checked'"; } ?> onclick="change_lyb_categories('<?=$the_type->type_name?>', this.checked);" ><?=$the_type->type_name?></li>
														<?php } ?>
													</ul>
													<input type="hidden" id="choosen_business_types" name="choosen_business_types" value="<?=$userdata[0]->business_type?>" />
												</div>
											</div>
										</td>
										<td valign="bottom" align="right">
											<p><a id="anc_add_more_category" href="javascript:void(0);" onclick="add_more_category();" >+ add another category</a></p> 
										</td>
									</tr>
									<tr>
										<td colspan="2">
											<?php for($ac=0; $ac<=25; $ac++){?>
											<div class="lyb_add_cats" id="div_business_category_<?=$ac?>" style="display:none;" >
												<input type="text" class="position_binfo" id="additional_cat_<?=$ac?>" name="additional_cat_<?=$ac?>" value="" />
											</div>
											<?php } ?>
											<input type="hidden" id="num_category" name="num_category" value="0" />
										</td>
									</tr>
								</table>
							</li>
						</ul>
						<div class="heading">
							<h1>Contact Information</h1>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>
						<ul>
							<li><input type="text" placeholder="*Contact Name" class="position_binfo" id="business_contact_name" name="business_contact_name" value="<?=set_value('business_contact_name')?>" /></li>
							<li><input type="text" placeholder="*Email Address" class="position_binfo" id="business_email_address" name="business_email_address" value="<?=set_value('business_email_address')?>" /></li>
							<!-- <li><input type="password" placeholder="*Password" class="position_binfo" id="business_password" name="business_password" value="" /></li> -->
							<li class="checkbox_custom"><input type="checkbox" name="is_authority" id="is_authority" value="1" class="check_binfo"><label class="label">I have authority to register this business</label></li>
						</ul>
						<div class="submit_info_btn">
							<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/submit_info_btn.jpg" alt="" />
						</div>
					<?php echo form_close()?>
				</div>
			</div><!--End of my_short_list_content-->
		</div>
		<!--End of list_your_business Page_Content-->
		<?php $this->load->view('templates/footer');?>
		<script type="text/javascript" src="<?=base_url('/public/assets/js/jquery.js'); ?>"></script>
		<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>
		<script type="text/javascript">
			$(function(){
				$('.list_business_content .list_business_detail ul li.checkbox_custom input:checkbox').screwDefaultButtons({
					image: 'url("<?=base_url('/public/images/agree.jpg'); ?>")',
					width: 29,
					height: 29
				});	
			});
		</script>
		<script type="text/javascript">
		/*$(document).ready(function(e) {
		$('.class_business_type').change(function () {
		choose_business_type(this.value, this.checked);
		});
		});*/
		</script>
		<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>
		<!--FOR GENDER SLIDER-->
		<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
		<!--Price Selector content--> 
		
        
        
        <?php if ($this->agent->is_mobile())
		{
			
		?>
        
		<?php	
		}
		else
		{		
		?>
		
           

           <link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('#nav2').localScroll(800);	
                    //.parallax(xPosition, speedFactor, outerHeight) options:
                    //xPosition - Horizontal position of the element
                    //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                    //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                    $('.list_business_banner').parallax("50%", -0.7);
                })
            </script>
		
		
		<?php
		}
		
		?>
        
        
        
		<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>
		<script type="text/javascript">
		$('input[type=text], input[type=password], textarea').placeholder();	
		</script>
	</body>
</html>