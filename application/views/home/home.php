<?php
$whatsondemand = $this->customclass->whatsondemand();
$openid = new Openid('ondi.com');

if(!$openid->mode)
{
	$openid->identity = 'https://www.google.com/accounts/o8/id';
	$openid->required = array('contact/email', 'namePerson/first');
	$gplusurl = $openid->authUrl();
}
elseif($_GET['openid_mode'] == 'cancel')
{
	//echo 'User has canceled authentication!';
}
else 
{	
	if($_GET['openid_mode'] == 'id_res')
	{
		$openid->validate();
		$userinfo = $openid->getAttributes();
		//print_r($_GET); die;
		$email = $userinfo['contact/email'];
		$firstName = $userinfo['namePerson/first'];
		if($email=='')
		{
			$email = $_GET['openid_ext1_value_contact_email'];
		}
		if($firstName=='')
		{
			$firstName = $_GET['openid_ext1_value_namePerson_first'];
		}
		$this->customclass->googlepluslogin($email, $firstName);
	}
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/filter/css/demo.css" rel="stylesheet" type="text/css" />

<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min10.js"></script>

<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<!--CUSTOM FUNCTION-->

<?php if ($this->agent->is_mobile())
{
?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<?php
}
?>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/css/bgstretcher.css" />
<link rel='stylesheet' type='text/css' href='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.css' />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/ajax-dynamic-list.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/filter/hide.js"></script>

<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />
<link type="text/css" rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/full_slide/css/style.css" media="all" />

<!--<script>$(window).load(function() {
		//alert(1)
		$('#loading-image').hide();
	});
</script>-->

<script type="text/javascript">
	
</script>
</head>

<body class="home">
<!--<div id="loading-image">
  <div class="pleasewait"> 
    <img src="<?php echo $this->config->item('base_url'); ?>public/images/ajax-loader.gif"  alt="Loading..." />
    <p>Loading please Wait</p>
  </div>
</div>-->

<div id="bg-body"> </div>
<!--end of bg-body-line--> 
<?php
$sess_user_data = $this->session->all_userdata();
if(!isset($sess_user_data['user_city_id']))
{
	$this->customclass->getusercity(); 
}
if(isset($sess_user_data['user_city_id']))
{
	$user_city_name = $sess_user_data['user_city_name'];
	$user_city_id = $sess_user_data['user_city_id'];
}
else
{
	$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
	$default_city_data = $default_city_query->result();
	$user_city_name = $default_city_data[0]->city_name;
	$user_city_id = $default_city_data[0]->id;
}
$default_city_slides = $this->db->get_where('slideshow_images', array('city' => $user_city_id));
$data_city_slides = $default_city_slides->result();
$str_slides = "";
foreach($data_city_slides as $the_slide)
{
	$the_slide_image = $the_slide->slideshow_image;
	if($the_slide_image != '')
	{
		if(file_exists($the_slide_image))
		{
			$str_slides .= '"'.$this->config->item('base_url').$the_slide_image.'",';
		}
	}
}
if($str_slides != "")
{
	$str_slides = substr($str_slides, 0, -1);
}

?>
<script type="text/javascript">
var srcBgArray = [<?=$str_slides?>];

//images: [<?=$slide_string?>],

$(document).ready(function() {
  $('#bg-body').bcatBGSwitcher({
    urls: srcBgArray,
    alt: 'Full screen background image',
    links: false,
    prevnext: false
  });
});
</script>
<?php
$firsttimeuser = $this->customclass->checkfirsttimeuser();
if($_SERVER['HTTP_HOST'] =='192.168.1.22')
{
	//$firsttimeuser = "yes";
}
$firsttimeuser = "yes";
if($firsttimeuser == 'yes')
{
?>
<div class="header_top" id="slide1" data-slide="1">
<?php
}
else
{
?>
<div class="header_top" id="" data-slide="1" >
  <?php
}
?>
  
  <!--<div class="scroll"><img src="<?php echo $this->config->item('base_url'); ?>public/images/scroll.png" alt="" /></div>--> 
  <!--START of nav_bg-->
  <div class="nav_bg<?php if($firsttimeuser != 'yes'){ echo "2"; }?>">
    <div class="nav_inner">
      <?php $this->load->view('templates/header_common');?>
      <!--End of header--> 
    </div>
  </div>
  <!--End of nav_bg--> 
  
</div>
<form name="main_search_form" method="get" action="<?php echo base_url("home/searchresults/"); ?>">
  <input type="hidden" name="searchact" value="1" />
  <input type="hidden" name="hidden_option_identifier" id="hidden_option_identifier" value="" />
  <!--START of whats_hot_bg-->
  <div class="search_bg slide" id="slide3" data-slide="3">
    <div class="search_main">
      <div class="search_con1">
        <p>Get last minute specials from your local beauty salons, hair dressers, gyms & more - on demand!</p>
      </div>
      <!--End of search_con-->
      <div class="search_con2"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/zoom.png" alt="" class="left_zoom" />
        <div class="search_box_outer">
          <?php if ($this->agent->is_mobile())
    {
    ?>
          <input type="text" value="" placeholder="Enter your search here" class="position_search_mobile" name="search_term" id="search_term"  onkeyup="ajax_showOptions(this,'getCountriesByLetters',event,2);" autocomplete="off" />
          <?php	
    }
    else
    {
    
    ?>
          <input type="text" value="" placeholder="I need a massage, hair cut, PT session..." class="position_search" name="search_term" id="search_term"  onkeyup="ajax_showOptions(this,'getCountriesByLetters',event,2);" autocomplete="off" />
          <?php
    }
    
    ?>
          <div class="auto_options">
            <ul>
              <li onclick="fill_keyword('All', '');">All</li>
              <?php foreach($business_types as $thebt){?>
              <li onclick="fill_keyword('<?=$thebt->type_name?>', <?=$thebt->type_id?>);">
                <?=$thebt->type_name?>
              </li>
              <?php } ?>
            </ul>
            <!-- <ul class="second">
				<?php foreach($services as $theservice){?>
            	<li onclick="fill_keyword('<?=$theservice->service?>', 0);"><?=$theservice->service?></li>
				<?php } ?>
            </ul> --> 
          </div>
        </div>
      </div>
      <!--End of search_con-->
      <div class="search_con3">
        <ul>
  
          <li>
         	<img src="<?php echo $this->config->item('base_url'); ?>public/images/when2.jpg" >
            <input type="text" value="Today" size="30" id="datepickerj" name="search_date">
            <!-- <select class="dropdown" name="search_date">
			<option value="<?=date("Y-m-d")?>">Today</option>
			<?php 
			for($d=1; $d<=7; $d++)
			{
			?>
            <option value="<?=date('Y-m-d', strtotime("+".$d." day"))?>"><?=date('d M, Y', strtotime("+".$d." day"))?></option>
			<?php 
			} 
			?>
          </select> --> 
          </li>
          
        </ul>
        <div class="right"> <a href="javascript:void(0)" class="search_button" onclick="javascript:document.main_search_form.submit();">SEARCH</a></div>
        <!-- <div class="right"> <a href="javascript:void(0)" class="search_button" onclick="search_offers();" class="search_button"></a>  -->
        
        
      </div>
      <div class="refine_search">
        <p><a href="javascript:void(0)">Refine your search <img src="<?php echo $this->config->item('base_url'); ?>public/images/down_arrow.png" alt="" /></a> </p>
      </div>
      <!--End of refine_search-->
      
      <div class="right_ques"> <span>I dont’t get it</span> <img src="<?php echo $this->config->item('base_url'); ?>public/images/question_mark.png" alt="" />
        <div class="tip">
          <div class="arrow"></div>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
      </div>
      
      
      <!--Start of Filter Section 1-->
      <div class="filter1_bg">
        <div class="filter2">
          <div class="click-nav filter_nav"  id="filter_1">
            <ul class="no-js">
              <li> <a class="clicker">Price</a>
                <ul class="submenu_nav" id="submenu_nav1">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav1');">Clear all</a></h4>
                  </li>
                  <?php foreach($price_guide_list_options as $thepg){?>
                  <li>
                    <input type="checkbox" class="dd_check search_pg" value="<?=$thepg->pg_id?>" name="search_pg[]">
                    <p>
                      <?=$thepg->pg_name?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav2 filter_nav" id="filter_2">
            <ul class="no-js2">
              <li> <a class="clicker2">Distance</a>
                <ul class="submenu_nav" id="submenu_nav2">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav2');">Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_distance" value="2" name="search_distance[]">
                    <p>2km</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_distance" value="4" name="search_distance[]">
                    <p>4km</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_distance" value="8" name="search_distance[]">
                    <p>8km</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check" value="10" name="search_distance[]">
                    <p>10km</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav3 filter_nav" id="filter_3" style="display:none;">
            <ul class="no-js3">
              <li> <a class="clicker3">Cuisine</a>
                <ul class="submenu_nav" id="submenu_nav3">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav3');">Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_cu" value="Cuisine 1" name="search_cu[]" >
                    <p>Cuisine 1</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_cu" value="Cuisine 2" name="search_cu[]" >
                    <p>Cuisine 2</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav4 filter_nav" id="filter_4"  style="display:none;">
            <ul class="no-js4">
              <li> <a class="clicker4">Options</a>
                <ul class="submenu_nav" id="submenu_nav4">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav4');">Clear all</a></h4>
                  </li>
                  <?php foreach($beauty_options as $thebeauty){?>
                  <li>
                    <input type="checkbox" class="dd_check search_beauty" value="<?=$thebeauty->id?>" name="search_beauty[]">
                    <p>
                      <?=$thebeauty->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav5 filter_nav" id="filter_7"  style="display:none;">
            <ul class="no-js5">
              <li> <a class="clicker5">Options</a>
                <ul class="submenu_nav" id="submenu_nav7">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav7');">Clear all</a></h4>
                  </li>
                  <?php foreach($fitness_options as $thefitness){?>
                  <li>
                    <input type="checkbox" class="dd_check search_fitness" value="<?=$thefitness->id?>" name="search_fitness[]">
                    <p>
                      <?=$thefitness->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav6 filter_nav" id="filter_8"  style="display:none;">
            <ul class="no-js6">
              <li> <a class="clicker6">Options</a>
                <ul class="submenu_nav" id="submenu_nav8">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav8');">Clear all</a></h4>
                  </li>
                  <?php foreach($hair_options as $hair){?>
                  <li>
                    <input type="checkbox" class="dd_check search_hair" value="<?=$hair->id?>" name="search_hair[]">
                    <p>
                      <?=$hair->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav7 filter_nav" id="filter_9"  style="display:none;">
            <ul class="no-js7">
              <li> <a class="clicker7">Options</a>
                <ul class="submenu_nav" id="submenu_nav9">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav9');">Clear all</a></h4>
                  </li>
                  <?php foreach($makeup_options as $makeup){?>
                  <li>
                    <input type="checkbox" class="dd_check search_makeup" value="<?=$makeup->id?>" name="search_makeup[]">
                    <p>
                      <?=$makeup->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav8 filter_nav" id="filter_10"  style="display:none;">
            <ul class="no-js8">
              <li> <a class="clicker8">Options</a>
                <ul class="submenu_nav" id="submenu_nav10">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav10');">Clear all</a></h4>
                  </li>
                  <?php foreach($massage_options as $massage){?>
                  <li>
                    <input type="checkbox" class="dd_check search_massage" value="<?=$massage->id?>" name="search_massage[]">
                    <p>
                      <?=$massage->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav9 filter_nav" id="filter_11"  style="display:none;">
            <ul class="no-js9">
              <li> <a class="clicker9">Options</a>
                <ul class="submenu_nav" id="submenu_nav11">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav11');">Clear all</a></h4>
                  </li>
                  <?php foreach($mens_options as $mens){?>
                  <li>
                    <input type="checkbox" class="dd_check search_mens" value="<?=$mens->id?>" name="search_mens[]">
                    <p>
                      <?=$mens->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav10 filter_nav" id="filter_12"  style="display:none;">
            <ul class="no-js10">
              <li> <a class="clicker10">Options</a>
                <ul class="submenu_nav" id="submenu_nav12">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav12');">Clear all</a></h4>
                  </li>
                  <?php foreach($nails_options as $nails){?>
                  <li>
                    <input type="checkbox" class="dd_check search_nails" value="<?=$nails->id?>" name="search_nails[]">
                    <p>
                      <?=$nails->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav11 filter_nav" id="filter_13"  style="display:none;">
            <ul class="no-js11">
              <li> <a class="clicker11">Options</a>
                <ul class="submenu_nav" id="submenu_nav13">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav13');">Clear all</a></h4>
                  </li>
                  <?php foreach($tanning_options as $tanning){?>
                  <li>
                    <input type="checkbox" class="dd_check search_tanning" value="<?=$tanning->id?>" name="search_tanning[]">
                    <p>
                      <?=$tanning->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav12 filter_nav" id="filter_14"  style="display:none;">
            <ul class="no-js12">
              <li> <a class="clicker12">Options</a>
                <ul class="submenu_nav" id="submenu_nav14">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav14');">Clear all</a></h4>
                  </li>
                  <?php foreach($teeth_options as $teeth){?>
                  <li>
                    <input type="checkbox" class="dd_check search_teeth" value="<?=$teeth->id?>" name="search_teeth[]">
                    <p>
                      <?=$teeth->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav13 filter_nav" id="filter_15"  style="display:none;">
            <ul class="no-js13">
              <li> <a class="clicker13">Options</a>
                <ul class="submenu_nav" id="submenu_nav15">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav15');">Clear all</a></h4>
                  </li>
                  <?php foreach($waxing_options as $waxing){?>
                  <li>
                    <input type="checkbox" class="dd_check search_waxing" value="<?=$waxing->id?>" name="search_waxing[]">
                    <p>
                      <?=$waxing->option_value?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav14 filter_nav" id="filter_5">
            <ul class="no-js14">
              <li> <a class="clicker14">Perfect for</a>
                <ul class="submenu_nav" id="submenu_nav5">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav5');">Clear all</a></h4>
                  </li>
                  <?php foreach($perfect_for_list_options as $thepf){?>
                  <li>
                    <input type="checkbox" class="dd_check search_pf" value="<?=$thepf->pf_id?>" name="search_pf[]" >
                    <p>
                      <?=$thepf->pf_name?>
                    </p>
                  </li>
                  <?php } ?>
                </ul>
              </li>
            </ul>
          </div>
          <div class="click-nav15 filter_nav" id="filter_6">
            <ul class="no-js15">
              <li> <a class="clicker15">User Rating</a>
                <ul class="submenu_nav" id="submenu_nav6">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav6');" >Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check user_rating" value="1" name="user_rating[]">
                    <p>*</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check user_rating" value="2" name="user_rating[]">
                    <p>**</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check user_rating" value="3" name="user_rating[]">
                    <p>***</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check user_rating" value="4" name="user_rating[]">
                    <p>****</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check user_rating" value="5" name="user_rating[]">
                    <p>*****</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
		  <div class="click-nav16 filter_nav" id="filter_16">
            <ul class="no-js16">
              <li> <a class="clicker16">Time</a>
                <ul class="submenu_nav" id="submenu_nav16">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav16');" >Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="" name="search_time_other[]">
                    <p>Any Time</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Morning" name="search_time_other[]">
                    <p>Morning</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Around Lunch" name="search_time_other[]">
                    <p>Around Lunch</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Afternoonish" name="search_time_other[]">
                    <p>Afternoonish</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Evening" name="search_time_other[]">
                    <p>Evening</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

		  <div class="click-nav17 filter_nav" id="filter_17" style="display:none;">
            <ul class="no-js17">
              <li> <a class="clicker17">Time</a>
                <ul class="submenu_nav" id="submenu_nav17" >
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav17');" >Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="" name="search_time_restaurant[]">
                    <p>Any Time</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Breakfast" name="search_time_restaurant[]">
                    <p>Breakfast</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Lunch" name="search_time_restaurant[]">
                    <p>Lunch</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Dinner" name="search_time_restaurant[]">
                    <p>Dinner</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

          <div class="filter_btn"><a href="javascript:void(0)" class="search_button" onclick="javascript:document.main_search_form.submit();" >Filter</a></div>
        </div>
      </div>
      <!--End of Filter Section 1-->
      
      
      
      
    </div>
    <!--End of search_con--> 
    <div class="search_arrow"></div>
  </div>
  <!--End of search_main-->
  
  </div>
  <!--End of search_bg--> 
  
  
</form>
<div class="inner_content_bg2" id="ajaxsearchresults"></div>
<div class="inner_content_bg2" id="mapsearchresults" style="display:none;">
  <?php
	$sessiondata = $this->session->all_userdata();
	$session_id = $sessiondata['session_id'];
	?>
  <iframe height="400" frameborder="0" width="100%" scrolling="no" src="" id="map_iframe"></iframe>
</div>
<div class="whats_hot_main slide" id="slide4" >
  <div class="whats_hot_bg">
    <div class="whats_hot_text"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/whats_hot_text.png" alt="" /> </div>
  </div>
  <!--End of whats_hot_bg--> 
  
  <!--START of services_bg-->
  <div class="services_bg">
    <div class="hot_container">
      <ul>
        <li id='panel-1'>
          <div class="heading">
            <h2>
              <!--<?=substr(str_replace(",", " / ", $this->users->display_merchant_business($whatsondemand[0]->business_type)), 0, 30)?>-->
              <!-- <?=stripslashes($whatsondemand[0]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[0]->price)+2;
			  $strlen_title = strlen($whatsondemand[0]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[0]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[0]) && $whatsondemand[0]->offer_title != '')
				  {
					echo substr($whatsondemand[0]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[0]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[0]->price != '') echo "$".$whatsondemand[0]->price; ?>
            </h2>
          </div>
          <!--End of massage_heading-->
          <div class="service_image"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
			<?php
			if($whatsondemand[0]->profile_picture != "")
			{
				$search_profile_picture = $whatsondemand[0]->profile_picture;
			}
			else
			{
				$search_profile_picture = "public/images/merchant-noimage.jpg";
			}
			?>
          <a href="<?=$this->offers->get_offer_url($whatsondemand[0]->offer_id)?>">
          	<img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a> </td>
          </tr>
        </table>
</div>
          <!--End of service_image-->
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[0]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[0]->suburb)?>
              <?php if(isset($whatsondemand[0])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[0]->business_type)?>
                <?php if(isset($whatsondemand[0])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[0]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[0]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
          
          <!--End of service_desc--> 
          
        </li>
        <li id='panel-2'>
          <div class="heading">
            <h2>
              <!-- <?=stripslashes($whatsondemand[1]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[1]->price)+2;
			  $strlen_title = strlen($whatsondemand[1]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[1]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[1]) && $whatsondemand[1]->offer_title != '')
				  {
					echo substr($whatsondemand[1]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[1]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[1]->price != '') echo "$".$whatsondemand[1]->price; ?>
            </h2>
          </div>
          <!--End of tanning_heading-->
          <div class="service_image"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
			<?php
			if($whatsondemand[1]->profile_picture != "")
			{
				$search_profile_picture = $whatsondemand[1]->profile_picture;
			}
			else
			{
				$search_profile_picture = "public/images/merchant-noimage.jpg";
			}
			?>
			<a href="<?=$this->offers->get_offer_url($whatsondemand[1]->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a> 
           </td> 
           </tr>
           </table>
           </div>
          <!--End of service_image-->
          
         
            
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[1]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[1]->suburb)?>
              <?php if(isset($whatsondemand[1])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[1]->business_type)?>
                <?php if(isset($whatsondemand[1])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[1]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[1]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
          
          
          <!--End of service_desc--> 
        </li>
        <li id='panel-3' class="end">
          <div class="heading">
            <h2>
              <!-- <?=stripslashes($whatsondemand[2]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[2]->price)+2;
			  $strlen_title = strlen($whatsondemand[2]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[2]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[2]) && $whatsondemand[2]->offer_title != '')
				  {
					echo substr($whatsondemand[2]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[2]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[2]->price != '') echo "$".$whatsondemand[2]->price; ?>
            </h2>
          </div>
          <!--End of yoga_heading-->
          <div class="service_image"> 
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
				<?php
				if($whatsondemand[2]->profile_picture != "")
				{
					$search_profile_picture = $whatsondemand[2]->profile_picture;
				}
				else
				{
					$search_profile_picture = "public/images/merchant-noimage.jpg";
				}
				?>
            	<a href="<?=$this->offers->get_offer_url($whatsondemand[2]->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a>
                </td>
                </tr>
                </table>
             </div>
          <!--End of service_image-->
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[2]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[2]->suburb)?>
              <?php if(isset($whatsondemand[2])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[2]->business_type)?>
                <?php if(isset($whatsondemand[2])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[2]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[2]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
          
          <!--End of service_desc--> 
        </li>
        
		<li class="panel_clear"></li>
        
        <li id='panel-4'>
          <div class="heading">
            <h2>
              <!-- <?=stripslashes($whatsondemand[3]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[3]->price)+2;
			  $strlen_title = strlen($whatsondemand[3]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[3]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[3]) && $whatsondemand[3]->offer_title != '')
				  {
					echo substr($whatsondemand[3]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[3]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[3]->price != '') echo "$".$whatsondemand[3]->price; ?>
            </h2>
          </div>
          <!--End of restaurant1_heading-->
          <div class="service_image"> 
          
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
				<?php
				if($whatsondemand[3]->profile_picture != "")
				{
					$search_profile_picture = $whatsondemand[3]->profile_picture;
				}
				else
				{
					$search_profile_picture = "public/images/merchant-noimage.jpg";
				}
				?>
            	<a href="<?=$this->offers->get_offer_url($whatsondemand[3]->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a> 
            </td>
                </tr>
                </table>    
            </div>
          <!--End of service_image-->
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[3]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[3]->suburb)?>
              <?php if(isset($whatsondemand[3])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[3]->business_type)?>
                <?php if(isset($whatsondemand[3])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[3]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[3]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
          
          <!--End of service_desc--> 
        </li>
        <li id="panel-5">
          <div class="heading">
            <h2>
              <!-- <?=stripslashes($whatsondemand[4]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[4]->price)+2;
			  $strlen_title = strlen($whatsondemand[4]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[4]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[4]) && $whatsondemand[4]->offer_title != '')
				  {
					echo substr($whatsondemand[4]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[4]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[4]->price != '') echo "$".$whatsondemand[4]->price; ?>
            </h2>
          </div>
          <!--End of restaurant2_heading-->
          <div class="service_image"> 
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
            	<?php
				if($whatsondemand[4]->profile_picture != "")
				{
					$search_profile_picture = $whatsondemand[4]->profile_picture;
				}
				else
				{
					$search_profile_picture = "public/images/merchant-noimage.jpg";
				}
				?>
          <a href="<?=$this->offers->get_offer_url($whatsondemand[4]->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a> 
            </td>
                </tr>
                </table>   
                 </div>
          <!--End of service_image-->
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[4]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[4]->suburb)?>
              <?php if(isset($whatsondemand[4])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[4]->business_type)?>
                <?php if(isset($whatsondemand[4])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[4]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[4]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
        <li id="panel-6" class="end">
          <div class="heading">
            <h2>
              <!-- <?=stripslashes($whatsondemand[5]->offer_title)?> -->
			  <?php
			  $total_max_char = 28;
			  $strlen_price = strlen($whatsondemand[5]->price)+2;
			  $strlen_title = strlen($whatsondemand[5]->offer_title);
			  //$available_space = $total_max_char - $strlen_price - $strlen_title-2;
			  $available_space = $total_max_char - $strlen_price -2;
			  if(strlen($whatsondemand[5]->offer_title)>$available_space) 
			  {
				  if(isset($whatsondemand[5]) && $whatsondemand[5]->offer_title != '')
				  {
					echo substr($whatsondemand[5]->offer_title, 0, $available_space)."..";
				  }
			  }
			  else
			  {
				  echo $whatsondemand[5]->offer_title;
			  }
			  ?>
              <?php if($whatsondemand[5]->price != '') echo "$".$whatsondemand[5]->price; ?>
            </h2>
          </div>
          <!--End of restaurant2_heading-->
          <div class="service_image"> 
          
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="180">
			<?php
				if($whatsondemand[5]->profile_picture != "")
				{
					$search_profile_picture = $whatsondemand[5]->profile_picture;
				}
				else
				{
					$search_profile_picture = "public/images/merchant-noimage.jpg";
				}
				?>
          <a href="<?=$this->offers->get_offer_url($whatsondemand[5]->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?><?=stripslashes($search_profile_picture)?>" alt="" /></a> 
            </td>
                </tr>
                </table>  
                
                </div>
          <!--End of service_image-->
          <div class="service_desc">
            <h4>
              <?=stripslashes($whatsondemand[1]->business_name)?>
             </h4>
           
            <div class="suburb">
              <p>
              <?=stripslashes($whatsondemand[5]->suburb)?>
             <?php if(isset($whatsondemand[5])){?><span class="dot"></span><?php } ?>
                <?=$this->users->display_merchant_business($whatsondemand[5]->business_type)?>
                <?php if(isset($whatsondemand[5])){?><span class="dot"></span><?php } ?>
                <?=$whatsondemand[5]->pg_name?>
              </p>
             
            </div>
            
             <?php
			  $numstars = $this->offers->get_offer_star_ratings($whatsondemand[5]->offer_id);
			  if($numstars>0)
			  {
			  ?>
              <span class="stars"><img src="<?php echo $this->config->item('base_url'); ?>public/images/star_<?=$numstars?>.png" alt="" /></span>
              <?php
			  }
			  ?>
              
              
          </div>
          
          <!--End of service_desc--> 
        </li>
      </ul>
      
      <div class="hot_arrow"></div>
    </div>
  </div>
</div>
<!--End of services_bg--> 

<!--HOW IT WORKS START-->
<div class="how_it_works_main slide" id="slide5" >
  <div class="how_it_works_bg" >
    <div class="how_it_works_text"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/how_it_work_text.png" alt="" /> </div>
    <!--End of how_it_works_text--> 
  </div>
  <!--End of how_it_works_bg-->
  
  <div class="compare_box_bg" id="slide7" data-slide="7">
    <div class="compare_box">
      <?php
	$admin_data = $this->Admin_Model->get_admin_detail('1');		
	$searchbox_heading = $admin_data[0]->searchbox_heading;
	$searchbox_content = $admin_data[0]->searchbox_content;
	$compare_heading = $admin_data[0]->compare_heading;
	$compare_content = $admin_data[0]->compare_content;
	$book_heading = $admin_data[0]->book_heading;
	$book_content = $admin_data[0]->book_content;
	?>
      <ul>
        <li>
          <div class="search_area"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/search_icon.png" alt="" />
            <h2>
              <?=$searchbox_heading?>
            </h2>
            <p>
              <?=$searchbox_content?>
            </p>
          </div>
          <!--End of search_area--> 
        </li>
        <li>
          <div class="compare_area"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/compare_icon.png" alt="" />
            <h2>
              <?=$compare_heading?>
            </h2>
            <p>
              <?=$compare_content?>
            </p>
          </div>
          <!--End of compare_area--> 
        </li>
        <li class="last">
          <div class="book_area"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/book_icon.png" alt="" />
            <h2>
              <?=$book_heading?>
            </h2>
            <p>
              <?=$book_content?>
            </p>
          </div>
          <!--End of book_area--> 
        </li>
      </ul>
    </div>
    <!--End of compare_box_bg--> 
  </div>
  <!--End of compare_box--> 
  
</div>
<!--HOW IT WORKS START-->

<div class="get_local_news_bg slide" id="slide6" >
  <div class="get_local_news_text"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/ger_local_news_text.png" alt="" id="get_local_news_text"/> </div>
  <!--End of get_local_news_text-->
  <div class="sign_up_bg" id="div_success_message">
    <?php if ($this->agent->is_mobile())
	{
	?>
    <div class="sign_up_main">
      <div class="sign_up_con1">
        <p id="newsletter_p">Stay in the loop</p>
      </div>
      <!--End of sign_up_con1-->
      
      <div class="sign_up_con2">
        <input type="text" name="nl_email" id="nl_email"  value="" class="position_search" placeholder="Enter your email here..."/>
        <input type="hidden" name="nl_gender" id="nl_gender" value=""  />
        <div class="gender">
          <p>Gender</p>
          
              <div id="chooseplan">
                <div class="cleaner h10"></div>
                <div id="hostpslider"></div>
                <div class="cleaner h10"></div>
              </div> 
             
          <div class="info"> Click left or right</div>
        </div>
        <!--End of gender--> 
      </div>
      <!--End of sign_up_con2-->
      
      <div class="sign_up_con3"> <a href="javascript:void(0);" onclick="checksubsnewsletter();"><img src="<?php echo $this->config->item('base_url'); ?>public/images/submit_btn.jpg" alt="" class="right" /></a> </div>
      <!--End of sign_up_con3--> 
    </div>
    <?php	
		}
		else
		{
	?>
    <div class="sign_up_main">
      <div class="sign_up_con1">
        <p id="newsletter_p">Stay in the loop</p>
      </div>
      <!--End of sign_up_con1-->
      
      <div class="sign_up_con2">
        <input type="text" name="nl_email" id="nl_email"  value="" class="position_search" placeholder="Enter your email here..."/>
        <input type="hidden" name="nl_gender" id="nl_gender" value=""  />
        <div class="gender">
          <p>Gender</p>
          
              <div id="chooseplan">
                <div class="cleaner h10"></div>
                <div id="hostpslider"></div>
                <div class="cleaner h10"></div>
              </div> 
             
          <div class="info"> Click left or right</div>
        </div>
        <!--End of gender--> 
      </div>
      <!--End of sign_up_con2-->
      
      <div class="sign_up_con3"> <a href="javascript:void(0);" onclick="checksubsnewsletter();"><img src="<?php echo $this->config->item('base_url'); ?>public/images/submit_btn.jpg" alt="" class="right" /></a> </div>
      <!--End of sign_up_con3--> 
    </div>
    <?php
}

?>
    
    <!--End of search_main--> 
  </div>
  <!--End of sign_up_bg--> 
</div>
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min1.8.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script> 

<!--FOR WHATS HOTS SECTION-->

<?php if ($this->agent->is_mobile())
{
?>
<?php	
}
else
{

?>
<script src='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.js' type='text/javascript'></script> 
<script type='text/javascript'>
	$(function() {
		$('.kwicks').kwicks({
			maxSize: '30%',
			behavior: 'menu'
		});
	});
</script>
<?php
}

?>

<!--FOR DATE PICKER--> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script> 
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
			
	});
</script> 

<!--FOR LOGIN CHECKBOX--> 

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 

<!--FOR GENDER SLIDER--> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script>
<?php
$slide_string = "";
foreach($slides as $theslide)
{
	$slide_string .= '"'.stripslashes($this->config->item('base_url').$theslide->slide_image).'", ';	
}
if($slide_string != '')
{
	$slide_string = substr($slide_string, 0, -2);
}
?>
<!--Price Selector content--> 

<script type="text/javascript">
   var DS = new Array("M", "", "F");
        $(function() {
			$("#hostpslider").slider({
				range: "min",
				value: 1,
				min: 0,
				max: 2,
				step: 1,
				slide: function(event, ui) {
					$("#DS").val(DS[ui.value]);
					document.getElementById('nl_gender').value = DS[ui.value];

				}
			});
			$("#DS").val(DS[$("#hostpslider").slider("value")]);
		});
		
		
		 $(function() {
			$("#hostpslider2").slider({
				range: "min",
				value: 1,
				min: 0,
				max: 2,
				step: 1,
				slide: function(event, ui) {
					$("#DS").val(DS[ui.value]);
					document.getElementById('nl_gender').value = DS[ui.value];

				}
			});
			$("#DS").val(DS[$("#hostpslider2").slider("value")]);
		});


		<?php if ($this->agent->is_mobile())
		{
			
		?>
	
		
		
		
		
		<?php	
		}
		else
		{		
		?>
		
			<?php 
			if($firsttimeuser == 'yes')
			{
			?>
			/*$(document).ready(function() {
				$("body").bgStretcher({
					images: [<?=$slide_string?>],
					slideShow: true,
					transitionEffect: "none",
					//sequenceMode:"random",
					//sliderCallbackFunc:"pause",
					//slideDirection: "E",
					//imageWidth: 1500,
					//imageHeight: 700
					imageWidth: 2000,
					imageHeight: 1000
					
				});
			});*/
			<?php
			}
			?>
			
			
			$(document).ready(function(){
				var height=$(window).height();
				$("#slide1").css('min-height',height+'px');		
			}); 
			
			
		
		
		<?php
		}
		
		?>






		
</script> 
<script>
	$(function() {
		$( "#datepickerj" ).datepicker({
			dateFormat: 'dd/mm/yy',
			minDate: '<?=date("d/m/Y")?>',
			maxDate: '<?=date("d/m/Y", strtotime("+7 days"))?>'
		});
		$( "#anim" ).change(function() {
			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
		});
	});
</script>
<?php if ($this->agent->is_mobile())
		{
			
		?>
<?php	
		}
		else
		{		
		?>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script> 
<script type="text/javascript">
            $(document).ready(function(){
                $('#nav2').localScroll(800);	
                //.parallax(xPosition, speedFactor, outerHeight) options:
                //xPosition - Horizontal position of the element
                //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                $('.whats_hot_bg').parallax("50%", 0.1);
                $('.how_it_works_bg').parallax("50%", 0.1);
                $('#slide6').parallax("50%", 0.4);
                $('#third').parallax("50%", 0.3);
            
            })
            </script>
<?php
		}
		
		?>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script> 
<script type="text/javascript">
	$('input[type=text], input[type=password], textarea').placeholder();	
</script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/full_slide/js/jquery.bcat.bgswitcher.js"></script>
</body>
</html>