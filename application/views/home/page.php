<?php
//print_r($pagedata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--CUSTOM FUNCTION-->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><!--FOR PARALAX SCROLLING-->


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body class="page_<?=$pagedata[0]->page_id?>">
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of about_banner Section-->
<div class="about_banner_bg slide" data-stellar-background-ratio="0.5">
	<div class="about_banner_text">
    
    	<img src="<?php echo $this->config->item('base_url'); ?>public/images/about_ondi_text.png" alt="" />
        
    	<!--<img src="<?php echo $this->config->item('base_url'); ?>public/images/about_ondi_text.png" alt="" />
        
    	<img src="<?php echo $this->config->item('base_url'); ?>public/images/about_ondi_text.png" alt="" />-->
    </div>
</div>
<!--End of about_banner Section-->
<!--Start of innerPage_Content-->
<div class="inner_content_bg5">
	<div class="inner_content">
    	<!--<h1></h1>-->
        <h2><?=$pagedata[0]->page_title?></h2>
		<p><?=$pagedata[0]->page_desc?></p>
    </div>
</div>
<!--End of innerPage_Content Section-->
<!--Start of Search, Compare And Book Section-->
<div class="compare_box_bg">
	<div class="compare_box">
    	<ul>
        	<li>
            	<div class="search_area">
                    <img src="<?php echo $this->config->item('base_url'); ?>public/images/search_icon.png" alt="" />
                    <h2>SEARCH</h2>
                    <p>So easy... just search for the service you want 'right now' or a little later.</p>
                </div>
            </li>
            <li>
            	<div class="compare_area">
                    <img src="<?php echo $this->config->item('base_url'); ?>public/images/compare_icon.png" alt="" />
                    <h2>COMPARE</h2>
                    <p>Compare the offereings and select the time slot you want</p>
                </div>
            </li>
            <li class="last">
            	<div class="book_area">
                    <img src="<?php echo $this->config->item('base_url'); ?>public/images/book_icon.png" alt="" />
                    <h2>BOOK</h2>
                    <p>Enter your credit card details to compeltely lock in your booking. Now just rock up - no voucher required!</p>
                </div>
            </li>
        </ul>
    </div>
</div>
<!--End of Search, Compare And Book Section-->
<!--Start of Bottom Baneer-->
<div class="about_bottom_banner slide" data-stellar-background-ratio="0.5" ></div>
<!--End of Bottom Baneer-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 
<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 




		<?php if ($this->agent->is_mobile())
		{
			
		?>
        
        
        
		<?php	
		}
		else
		{		
		?>
		
           

            <link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script>
            
            <script type="text/javascript">
            $(document).ready(function(){
                $('#nav2').localScroll(800);	
                //.parallax(xPosition, speedFactor, outerHeight) options:
                //xPosition - Horizontal position of the element
                //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                $('.about_banner_bg').parallax("60%", -0.7);
                $('.about_bottom_banner').parallax("60%", -0.7);
                
            
            })
            </script>
		
		
		<?php
		}
		
		?>
        
        
        
        


</body>
</html>
