<?php
//print_r($searchdata);
$sorttbynewest = "";
$sorttbyclosest = "";
$sorttbyaz = "";
$sorttbyza = "";
$sorttbyrating = "";
if($sorttby == 'Newest')
{
	$sql .= " ORDER BY a.id DESC ";
	$sorttbynewest = " selected='selected' ";
}
else if($sorttby == 'Closest')
{
	$sql .= " ORDER BY distance ASC ";
	$sorttbyclosest = " selected='selected' ";
}
else if($sorttby == 'A-Z')
{
	$sql .= " ORDER BY a.offer_title ASC ";
	$sorttbyaz = " selected='selected' ";
}
else if($sorttby == 'Z-A')
{
	$sql .= " ORDER BY a.offer_title DESC ";
	$sorttbyza = " selected='selected' ";
}
else if($sorttby == 'Rating')
{
	$sql .= " ORDER BY a.average_rating DESC ";
	$sorttbyrating = " selected='selected' ";
}
$sessiondata = $this->session->all_userdata();
$session_id = $sessiondata['session_id'];
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/filter/css/demo.css" rel="stylesheet" type="text/css" />


<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min10.js"></script>

<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<!--CUSTOM FUNCTION-->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/css/bgstretcher.css" />
<link rel='stylesheet' type='text/css' href='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.css' />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/ajax.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/ajax-dynamic-list.js"></script>

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/filter/hide.js"></script>



<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />
</head>
<body>
<?php $this->load->view('templates/header');?>
<!--Start of innerPage_Content-->

<!--Start of Compare 1 Section-->
<form name="main_search_form" method="get" action="<?php echo base_url("home/searchresults/"); ?>">
<input type="hidden" name="searchact" value="1" />
<input type="hidden" name="hidden_option_identifier" id="hidden_option_identifier" value="" />
<!--START of whats_hot_bg-->
<div class="search_bg slide" id="slide3" data-slide="3">
  <div class="search_main">
    <div class="search_con1">
      <p>Get last minute specials from your local beauty salons, hair dressers, gyms & more - on demand!</p>
    </div>
    <!--End of search_con-->
    <div class="search_con2"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/zoom.png" alt="" class="left_zoom" />
    
    <div class="search_box_outer">
    
    
	<?php if ($this->agent->is_mobile())
    {
    ?>
    
    <input type="text" value="" placeholder="Enter your search here" class="position_search_mobile" name="search_term" id="search_term"  onkeyup="ajax_showOptions(this,'getCountriesByLetters',event,2);" autocomplete="off" />    
    
    <?php	
    }
    else
    {
    
    ?>
        
	<input type="text" value="" placeholder="I need a massage, hair cut, PT session..." class="position_search" name="search_term" id="search_term"  onkeyup="ajax_showOptions(this,'getCountriesByLetters',event,2);" autocomplete="off" />
    
    <?php
    }
    
    ?>


    

        <div class="auto_options">
        	<ul>
				<li onclick="fill_keyword('All', '');">All</li>
				<?php foreach($business_types as $thebt){?>
            	<li onclick="fill_keyword('<?=$thebt->type_name?>', <?=$thebt->type_id?>);"><?=$thebt->type_name?></li>
				<?php } ?>
            </ul>
			<!-- <ul class="second">
				<?php foreach($services as $theservice){?>
            	<li onclick="fill_keyword('<?=$theservice->service?>', 0);"><?=$theservice->service?></li>
				<?php } ?>
            </ul> -->
        </div>
    </div>
    
    
      
      
      
      
      
    </div>
    <!--End of search_con-->
    <div class="search_con3">
      <ul>
      	<li>
        <img src="<?php echo $this->config->item('base_url'); ?>public/images/when2.jpg" >
		  <input type="text" value="Today" size="30" id="datepickerj" name="search_date">
          <!-- <select class="dropdown" name="search_date">
			<option value="<?=date("Y-m-d")?>">Today</option>
			<?php 
			for($d=1; $d<=7; $d++)
			{
			?>
            <option value="<?=date('Y-m-d', strtotime("+".$d." day"))?>"><?=date('d M, Y', strtotime("+".$d." day"))?></option>
			<?php 
			} 
			?>
          </select> -->
        </li>
        <!--<li id="time_restaurant" style="display:none;"> 
          <select class="dropdown" name="search_time_restaurant" >
            <option value="">Any Time</option>
			<option value="Breakfast">Breakfast</option>
			<option value="Lunch">Lunch</option>
			<option value="Dinner">Dinner</option>
          </select>
		 </li>
		 <li id="time_others" style="display:block;"> 
		  <select class="dropdown" name="search_time_other" >
            <option value="">Any Time</option>
			<option value="Morning">Morning</option>
			<option value="Around Lunch">Around Lunch</option>
			<option value="Afternoonish">Afternoonish</option>
			<option value="Evening">Evening </option>
          </select>
        </li>-->
        
      </ul>
       <div class="right"> <a href="javascript:void(0)" class="search_button" onclick="javascript:document.main_search_form.submit();">SEARCH</a> </div>
      <!-- <div class="right"> <a href="javascript:void(0)" class="search_button" onclick="search_offers();" class="search_button"></a> </div> -->
      
     
      
    </div>
    <!--End of search_con-->
    <?php if(sizeof($searchdata)>0){?>
	<div class="refine_search">
      <p><a href="javascript:void(0)">Refine your search <img src="<?php echo $this->config->item('base_url'); ?>public/images/down_arrow.png" alt="" /></a>
      </p>
    </div>
	<?php } ?>
    
      <div class="right_ques"> <span>I dont’t get it</span> <img src="<?php echo $this->config->item('base_url'); ?>public/images/question_mark.png" alt="" />
        <div class="tip">
          <div class="arrow"></div>
          <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
          <p>It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
      </div>
      
      
    <!--End of refine_search--> 
    
    <!--Start of Filter Section 1-->
    <div class="filter1_bg">
    
      <div class="filter2">
            <div class="click-nav filter_nav"  id="filter_1">
            <ul class="no-js">
                <li>
                    <a class="clicker">Price</a>
                    <ul class="submenu_nav" id="submenu_nav1">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav1');">Clear all</a></h4>
                      </li>
                      <?php foreach($price_guide_list_options as $thepg){?>
                      <li>
                        <input type="checkbox" class="dd_check search_pg" value="<?=$thepg->pg_id?>" name="search_pg[]">
                        <p><?=$thepg->pg_name?></p>
                      </li>
                      <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav2 filter_nav" id="filter_2">
            <ul class="no-js2">
                <li>
                    <a class="clicker2">Distance</a>
                    <ul class="submenu_nav" id="submenu_nav2">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav2');">Clear all</a></h4>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check search_distance" value="2" name="search_distance[]">
                        <p>2km</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check search_distance" value="4" name="search_distance[]">
                        <p>4km</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check search_distance" value="8" name="search_distance[]">
                        <p>8km</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check" value="10" name="search_distance[]">
                        <p>10km</p>
                      </li>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        
        <div class="click-nav3 filter_nav" id="filter_3" style="display:none;">
            <ul class="no-js3">
                <li>
                    <a class="clicker3">Cuisine</a>
                    <ul class="submenu_nav" id="submenu_nav3">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav3');">Clear all</a></h4>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check search_cu" value="Cuisine 1" name="search_cu[]" >
                        <p>Cuisine 1</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check search_cu" value="Cuisine 2" name="search_cu[]" >
                        <p>Cuisine 2</p>
                      </li>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="click-nav4 filter_nav" id="filter_4"  style="display:none;">
            <ul class="no-js4">
                <li>
                    <a class="clicker4">Options</a>
                    <ul class="submenu_nav" id="submenu_nav4">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav4');">Clear all</a></h4>
                      </li>
                       <?php foreach($beauty_options as $thebeauty){?>
                      <li>
                        <input type="checkbox" class="dd_check search_beauty" value="<?=$thebeauty->id?>" name="search_beauty[]">
                        <p><?=$thebeauty->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="click-nav5 filter_nav" id="filter_7"  style="display:none;">
            <ul class="no-js5">
                <li>
                    <a class="clicker5">Options</a>
                    <ul class="submenu_nav" id="submenu_nav7">
                          <li>
                            <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav7');">Clear all</a></h4>
                          </li>
                           <?php foreach($fitness_options as $thefitness){?>
                          <li>
                            <input type="checkbox" class="dd_check search_fitness" value="<?=$thefitness->id?>" name="search_fitness[]">
                            <p><?=$thefitness->option_value?></p>
                          </li>
                         <?php } ?>
                        </ul>
                </li>
            </ul>
        </div>
        
        
        
        <div class="click-nav6 filter_nav" id="filter_8"  style="display:none;">
            <ul class="no-js6">
                <li>
                    <a class="clicker6">Options</a>
                    <ul class="submenu_nav" id="submenu_nav8">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav8');">Clear all</a></h4>
                      </li>
                       <?php foreach($hair_options as $hair){?>
                      <li>
                        <input type="checkbox" class="dd_check search_hair" value="<?=$hair->id?>" name="search_hair[]">
                        <p><?=$hair->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav7 filter_nav" id="filter_9"  style="display:none;">
            <ul class="no-js7">
                <li>
                    <a class="clicker7">Options</a>
                    <ul class="submenu_nav" id="submenu_nav9">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav9');">Clear all</a></h4>
                      </li>
                       <?php foreach($makeup_options as $makeup){?>
                      <li>
                        <input type="checkbox" class="dd_check search_makeup" value="<?=$makeup->id?>" name="search_makeup[]">
                        <p><?=$makeup->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav8 filter_nav" id="filter_10"  style="display:none;">
            <ul class="no-js8">
                <li>
                    <a class="clicker8">Options</a>
                    <ul class="submenu_nav" id="submenu_nav10">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav10');">Clear all</a></h4>
                      </li>
                       <?php foreach($massage_options as $massage){?>
                      <li>
                        <input type="checkbox" class="dd_check search_massage" value="<?=$massage->id?>" name="search_massage[]">
                        <p><?=$massage->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="click-nav9 filter_nav" id="filter_11"  style="display:none;">
            <ul class="no-js9">
                <li>
                    <a class="clicker9">Options</a>
                    <ul class="submenu_nav" id="submenu_nav11">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav11');">Clear all</a></h4>
                      </li>
                       <?php foreach($mens_options as $mens){?>
                      <li>
                        <input type="checkbox" class="dd_check search_mens" value="<?=$mens->id?>" name="search_mens[]">
                        <p><?=$mens->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav10 filter_nav" id="filter_12"  style="display:none;">
            <ul class="no-js10">
                <li>
                    <a class="clicker10">Options</a>
                    <ul class="submenu_nav" id="submenu_nav12">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav12');">Clear all</a></h4>
                      </li>
                       <?php foreach($nails_options as $nails){?>
                      <li>
                        <input type="checkbox" class="dd_check search_nails" value="<?=$nails->id?>" name="search_nails[]">
                        <p><?=$nails->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        <div class="click-nav11 filter_nav" id="filter_13"  style="display:none;">
            <ul class="no-js11">
                <li>
                    <a class="clicker11">Options</a>
                    <ul class="submenu_nav" id="submenu_nav13">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav13');">Clear all</a></h4>
                      </li>
                       <?php foreach($tanning_options as $tanning){?>
                      <li>
                        <input type="checkbox" class="dd_check search_tanning" value="<?=$tanning->id?>" name="search_tanning[]">
                        <p><?=$tanning->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav12 filter_nav" id="filter_14"  style="display:none;">
            <ul class="no-js12">
                <li>
                    <a class="clicker12">Options</a>
                    <ul class="submenu_nav" id="submenu_nav14">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav14');">Clear all</a></h4>
                      </li>
                       <?php foreach($teeth_options as $teeth){?>
                      <li>
                        <input type="checkbox" class="dd_check search_teeth" value="<?=$teeth->id?>" name="search_teeth[]">
                        <p><?=$teeth->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav13 filter_nav" id="filter_15"  style="display:none;">
            <ul class="no-js13">
                <li>
                    <a class="clicker13">Options</a>
                    <ul class="submenu_nav" id="submenu_nav15">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav15');">Clear all</a></h4>
                      </li>
                       <?php foreach($waxing_options as $waxing){?>
                      <li>
                        <input type="checkbox" class="dd_check search_waxing" value="<?=$waxing->id?>" name="search_waxing[]">
                        <p><?=$waxing->option_value?></p>
                      </li>
                     <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav14 filter_nav" id="filter_5">
            <ul class="no-js14">
                <li>
                    <a class="clicker14">Perfect for</a>
                    <ul class="submenu_nav" id="submenu_nav5">
                       <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav5');">Clear all</a></h4>
                      </li>
                      <?php foreach($perfect_for_list_options as $thepf){?>
                      <li>
                        <input type="checkbox" class="dd_check search_pf" value="<?=$thepf->pf_id?>" name="search_pf[]" >
                        <p><?=$thepf->pf_name?></p>
                      </li>
                      <?php } ?>
                    </ul>
                </li>
            </ul>
        </div>
        
        
        <div class="click-nav15 filter_nav" id="filter_6">
            <ul class="no-js15">
                <li>
                    <a class="clicker15">User Rating</a>
                    <ul class="submenu_nav" id="submenu_nav6">
                      <li>
                        <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav6');" >Clear all</a></h4>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check user_rating" value="1" name="user_rating[]">
                        <p>*</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check user_rating" value="2" name="user_rating[]">
                        <p>**</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check user_rating" value="3" name="user_rating[]">
                        <p>***</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check user_rating" value="4" name="user_rating[]">
                        <p>****</p>
                      </li>
                      <li>
                        <input type="checkbox" class="dd_check user_rating" value="5" name="user_rating[]">
                        <p>*****</p>
                      </li>
                    </ul>
                </li>
            </ul>
        </div>


		<div class="click-nav16 filter_nav" id="filter_16">
            <ul class="no-js16">
              <li> <a class="clicker16">Time</a>
                <ul class="submenu_nav" id="submenu_nav16">
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav16');" >Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="" name="search_time_other[]">
                    <p>Any Time</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Morning" name="search_time_other[]">
                    <p>Morning</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Around Lunch" name="search_time_other[]">
                    <p>Around Lunch</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Afternoonish" name="search_time_other[]">
                    <p>Afternoonish</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Evening" name="search_time_other[]">
                    <p>Evening</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>

		  <div class="click-nav17 filter_nav" id="filter_17" style="display:none;">
            <ul class="no-js17">
              <li> <a class="clicker17">Time</a>
                <ul class="submenu_nav" id="submenu_nav17" >
                  <li>
                    <h4><a href="javascript:void(0);" onclick="uncheckall('submenu_nav17');" >Clear all</a></h4>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="" name="search_tm[]">
                    <p>Any Time</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Breakfast" name="search_time_restaurant[]">
                    <p>Breakfast</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Lunch" name="search_time_restaurant[]">
                    <p>Lunch</p>
                  </li>
                  <li>
                    <input type="checkbox" class="dd_check search_tm" value="Dinner" name="search_time_restaurant[]">
                    <p>Dinner</p>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        
        <div class="filter_btn"><a href="javascript:void(0)" class="search_button" onclick="javascript:document.main_search_form.submit();" >Filter</a></div>
        
    
    
    </div>
     
    </div>
    <!--End of Filter Section 1-->

  </div>
  <!--End of search_main--> 
  
</div>
<!--End of compare red Section-->


<input type="hidden" name="hidden_sort_by" id="hidden_sort_by" value="" />
</form>

<!--Start of Booking Page_Content-->
<div class="inner_content_bg2">
	<div class="compare1_content">
		<?php if(sizeof($searchdata)>0){?>
    	<div class="compare_view">
        	<div class="sorted_by" id="div_sorted_by">
            	<p>Sort by:</p>
                <span class="sort_dd">
				<?php
				echo '<div class="custom_select_ajax">

					<select class="revostyled" tabindex="4" id="sortby" name="sortby" onchange="javascript:document.main_search_form.hidden_sort_by.value=this.value; document.main_search_form.submit();">
                        <option value="Newest" '.$sorttbynewest.'>Newest</option>
                        <option value="Closest" '.$sorttbyclosest.' >Closest</option>
                        <option value="A-Z" '.$sorttbyaz.' >A-Z</option>
						<option value="Z-A" '.$sorttbyza.' >Z-A</option>
						<option value="Rating" '.$sorttbyrating.' >Rating</option>
					</select>
					</div>';
				?>
				</span>
            </div>
            <div class="list_view">
            	<?php
				echo '<ul>
                	<li><a href="javascript:void(0);" onclick="list_view();"><img src="'.$this->config->item('base_url').'public/images/list_view_image.png" alt=""> List view</a></li>';

					if(sizeof($searchdata)>0)
					{
						echo '<li><a href="javascript:void(0);" onclick="map_view(\''.$session_id.'\');"><img src="'.$this->config->item('base_url').'public/images/map_view.png" alt=""> Map view</a></li>';
					}
				echo '</ul>';
				?>
            </div>
        </div>
		<?php } ?>			
        <div class="compare1_content_detail" id="listsearchresults">
        	
				<?php
				if(sizeof($searchdata)>0)
				{
					?>
					<ul>
					<?php
					$numshort = 0;
					foreach($searchdata as $search)
					{
						$numshort++; 
						$how_many_left = $this->offers->how_many_left($search->offer_id);
						$merchant_data = $this->offers->get_merchant_id($search->offer_id);
						
						if($search->profile_picture != "")
						{
							$search_profile_picture = $search->profile_picture;
						}
						else
						{
							$search_profile_picture = "public/images/merchant-noimage.jpg";
						}
						?>
						<li <?php if($numshort%2==0){ ?> class="gray" <?php } ?> >
								<div class="image">
								<?php 
								echo '<a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'"><img src="'.$this->config->item('base_url').$search_profile_picture.'" alt="" width="150" height="125" /></a>';
								?>
								</div>
								<div class="address">
									<?php
									if(strlen($search->business_name)>20) $bname=substr($search->business_name, 0, 20)."..";
									else $bname = $search->business_name;
									?>
									<h3>
										<?php
										echo '<a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'">'.$bname.'</a>';
										?>
									</h3>
									<?php
									$suburb_x = $search->suburb;
									if(strlen($suburb_x)>10) $suburb_x=substr($suburb_x, 0, 10)."..";
									?>
									<h4><?=$suburb_x?></h4>
									<?php
									$categs = $this->users->display_merchant_business($search->business_type);
									if(strlen($categs)>30) $categs=substr($categs, 0, 30)."..";
									?>
									<p><?=$categs?></p>
									<div class="cradit"><p><?=$search->pg_name?><span></span></p>
									<?php echo '<img src="'.$this->config->item('base_url').'public/images/'.$this->offers->get_offer_star_ratings($search->offer_id).'.png" alt="" />'; ?>
									</div>
								</div>
								<div class="name">
									 <table>
									  <tr>
										<td><?php echo '<a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'">'.$search->offer_title.'</a>';?></td>
									  </tr>
									</table>
								</div>
								<div class="value"><p>$<?=$search->price?><span>(Value $<?=$search->price_normally?>)</span></p></div>
								<div class="book">
									<?php 
									echo '<a href="'.base_url("booking/index/?oid=".base64_encode($search->offer_id)).'"><img src="'.$this->config->item('base_url').'public/images/book_btn.jpg" alt=""></a>';
									?>
									<?php
									if($how_many_left>0)
									{
									?>
									<div class="only_left">Only <?=$how_many_left?> left</div>
									<?php
									}
									?>
								</div>
								<?php
								$return_str = '<div class="remove"><div class="share"><p>Share</p><span class="nav_social" id="nav_social_'.$numshort.'"><a target="_blank" href="http://www.facebook.com/sharer.php?u='.$this->offers->get_offer_url($search->offer_id).'&t=<'.urlencode($search->offer_title).'"><img src="'.$this->config->item('base_url').'public/images/cus_fb.png" alt="" /></a><a target="_blank" href="https://twitter.com/home?status='.urlencode($search->offer_title).'%20-%20'.$this->offers->get_offer_url($search->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_twitter.png" alt="" /></a><a target="_blank" href="https://plus.google.com/share?url='.$this->offers->get_offer_url($search->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_g+.png" alt="" /></a></span></div><a class="remove_s" id="anc_removefrom_shortlist_'.$search->offer_id.'" href="javascript:void(0);" onclick="removefrom_shortlist('.$search->offer_id.');" ';
					
								if($this->offers->check_offer_inshortlist($search->offer_id))
								{
									$return_str .= ' style="display:block;" ';
								} 
								else 
								{ 
									$return_str .= ' style="display:none;" ';
								} 

								$return_str .= ' >Remove</a><a class="add_s" id="anc_addto_shortlist_'.$search->offer_id.'" href="javascript:void(0);" onclick="addto_shortlist('.$search->offer_id.');" ';
								
								if($this->offers->check_offer_inshortlist($search->offer_id))
								{
									$return_str .= ' style="display:none;" ';
								} 
								else
								{ 
									$return_str .= ' style="display:block;" ';
								} 
								$return_str .= ' >Shortlist</a></div>';
								echo $return_str;
								?>
						</li>
					<?php
					}
					?>
					</ul>
				<?php
				}
				else
				{
					?>
					<div class="nores">Sorry! No Matching record found. Please change your search criteria.</div>
					<?php
				}
				?>
            
        </div>
        
        
    </div><!--End of my_short_list_content-->
</div><!--End of inner_content_bg-->

<div class="inner_content_bg2" id="mapsearchresults" style="display:none;">
	<?php
	$sessiondata = $this->session->all_userdata();
	$session_id = $sessiondata['session_id'];
	?>
	<iframe height="400" frameborder="0" width="100%" scrolling="no" src="" id="map_iframe"></iframe>
</div>

<!--End of innerPage_Content Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>



<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min1.8.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script>


<!--FOR WHATS HOTS SECTION-->

<?php if ($this->agent->is_mobile())
{
?>




<?php	
}
else
{

?>
    
<script src='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.js' type='text/javascript'></script> 
<script type='text/javascript'>
	$(function() {
		$('.kwicks').kwicks({
			maxSize: '30%',
			behavior: 'menu'
		});
	});
</script> 

<?php
}

?>


<!--FOR DATE PICKER-->

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>



<!--FOR LOGIN CHECKBOX-->


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 

<!--FOR GENDER SLIDER-->

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<?php
$slide_string = "";
foreach($slides as $theslide)
{
	$slide_string .= '"'.stripslashes($this->config->item('base_url').$theslide->slide_image).'", ';	
}
if($slide_string != '')
{
	$slide_string = substr($slide_string, 0, -2);
}
?>
<!--Price Selector content--> 

<script type="text/javascript">
   var DS = new Array("M", "", "F");
        $(function() {
			$("#hostpslider").slider({
				range: "min",
				value: 1,
				min: 0,
				max: 2,
				step: 1,
				slide: function(event, ui) {
					$("#DS").val(DS[ui.value]);
					document.getElementById('nl_gender').value = DS[ui.value];

				}
			});
			$("#DS").val(DS[$("#hostpslider").slider("value")]);
		});
		
		
		 $(function() {
			$("#hostpslider2").slider({
				range: "min",
				value: 1,
				min: 0,
				max: 2,
				step: 1,
				slide: function(event, ui) {
					$("#DS").val(DS[ui.value]);
					document.getElementById('nl_gender').value = DS[ui.value];

				}
			});
			$("#DS").val(DS[$("#hostpslider2").slider("value")]);
		});


		<?php if ($this->agent->is_mobile())
		{
			
		?>
	
		
		
		
		
		<?php	
		}
		else
		{		
		?>
		
			<?php 
			if($firsttimeuser == 'yes')
			{
			?>
			$(document).ready(function() {
				$("body").bgStretcher({
					images: [<?=$slide_string?>],
					slideShow: true,
					transitionEffect: "none",
					//sequenceMode:"random",
					//sliderCallbackFunc:"pause",
					//slideDirection: "E",
					//imageWidth: 1500,
					//imageHeight: 700
					imageWidth: 1500,
					imageHeight: 700
					
				});
			});
			<?php
			}
			?>
			
			
			$(document).ready(function(){
				var height=$(window).height();
				$("#slide1").css('min-height',height+'px');	
				
				
			}); 
		
		
		<?php
		}
		
		?>






		
</script>

<script>
	$(function() {
		$( "#datepickerj" ).datepicker({
			dateFormat: 'dd/mm/yy',
			minDate: '<?=date("d/m/Y")?>',
			maxDate: '<?=date("d/m/Y", strtotime("+7 days"))?>'
		});
		$( "#anim" ).change(function() {
			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
		});

		$(".revostyled").selectbox();
	});
</script>

<?php if ($this->agent->is_mobile())
		{
			
		?>
	
		
		
		
		
		<?php	
		}
		else
		{		
		?>
		
                        
            <link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script>
            
            <script type="text/javascript">
            $(document).ready(function(){
                $('#nav2').localScroll(800);	
                //.parallax(xPosition, speedFactor, outerHeight) options:
                //xPosition - Horizontal position of the element
                //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                $('.whats_hot_bg').parallax("50%", 0.1);
                $('.how_it_works_bg').parallax("50%", 0.1);
                $('#slide6').parallax("50%", 0.4);
                $('#third').parallax("50%", 0.3);
            
            })
            </script>
		
		
		<?php
		}
		
		?>
        
        



<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>
<script type="text/javascript">
	$('input[type=text], input[type=password], textarea').placeholder();	
</script>
<!--<script language="javascript">
function show_hide_nav_social(idx)
{
	
	alert(idx);
	if(document.getElementById('nav_social_'+idx).style.display=="block")
	{
		document.getElementById('nav_social_'+idx).style.display="none";
	}
	else if(document.getElementById('nav_social_'+idx).style.display=="none")
	{
		document.getElementById('nav_social_'+idx).style.display="block";
	}
}

</script>-->
</body>
</html>
