<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of Booking Page_Content-->
<div class="inner_content_bg">
	<div class="my_short_list_content">
    	<div class="heading">
        	<h1>MY SHORTLIST</h1>
        </div>
        <div class="sub_heading">
        	<h3>Here are your favourites that you have shortlisted</h3>
        </div>
        <div class="my_short_list_detail">
        	<?php
			if(sizeof($shortlistdata)>0){
			?>
			<ul>
				<?php 
				$numshort = 0;
				foreach($shortlistdata as $shortlist)
				{
					$numshort++; 
					$how_many_left = $this->offers->how_many_left($shortlist->offer_id);
					//$how_many_left = sizeof($how_many_left_data);
					?>
					<li <?php if($numshort%2==0){?> class="gray" <?php } ?>>
							<div class="image">
							<?php
							if($shortlist->profile_picture != "")
							{
								$shortlist_profile_picture = $shortlist->profile_picture;
							}
							else
							{
								$shortlist_profile_picture = "public/images/merchant-noimage.jpg";
							}
							?>
							<img src="<?php echo $this->config->item('base_url'); ?><?=$shortlist_profile_picture?>" alt="" width="150" height="125" /></div>
							<div class="address">
								<?php
								if(strlen($shortlist->business_name)>20) $bname=substr($shortlist->business_name, 0, 20)."..";
								else $bname = $shortlist->business_name;
								?>
								<h3><?=$bname?></h3>
								<?php
								$suburb_x = $shortlist->suburb;
								if(strlen($suburb_x)>10) $suburb_x=substr($suburb_x, 0, 10)."..";
								?>
								<h4><?=$suburb_x?></h4>
								<?php
								$categs = $this->users->display_merchant_business($shortlist->business_type);
								if(strlen($categs)>30) $categs=substr($categs, 0, 30)."..";
								?>
								<p><?=$categs?></p>
								<div class="cradit"><p><?=$shortlist->pg_name?></p><img src="<?php echo $this->config->item('base_url'); ?>public/images/<?=$this->offers->get_offer_star_ratings($shortlist->offer_id)?>.png" alt="" /></div>
							</div>
							<div class="name">
								<table>
								  <tr>
									<td><?=$shortlist->offer_title?></td>
								  </tr>
								</table>
							</div>
                            
                            	
                                   
                                   
                                   
                                   <div class="value"><p>$<?=$shortlist->price?> <span>(Value $<?=$shortlist->price_normally?>)</span></p></div>
                                    <div class="book"><a href="<?php echo base_url("booking/index/?oid=".base64_encode($shortlist->offer_id)); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/book_btn.jpg" alt=""></a>
                                        <?php if($how_many_left>0){?><div class="only_left">Only <?=$how_many_left?> left</div><?php } ?>
                                    </div>
                                    
                                    <div class="remove">
                                        <div class="share">
                                            <a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/share.png" alt=""></a>
                                            <p>Share</p>
                                            <span class="nav_social">
                                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($shortlist->offer_id)?>&t=<?=urlencode($shortlist->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
                                                <a target="_blank" href="https://twitter.com/home?status=<?=urlencode($shortlist->offer_title)?>%20-%20<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
                                                <a target="_blank" href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
                                            </span>
                                        
                                        </div>
                                        
                                        <a href="javascript:void(0);" onclick="remove_shortlist_home(<?=$shortlist->offer_id?>);"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cancel.png" alt=""></a>
                                        <p>Remove</p>
        
                                    </div>
                                    
                                 
                                    
								
                            
                            
							
                            
                            
                            
					</li>
					<?php 
				}// end foreach
				?>
            </ul>
			<?php
			} else{ ?>
						<p class="no_record">No records found</p>
			<?php } ?>
        </div>
        
        
    </div><!--End of my_short_list_content-->
</div><!--End of inner_content_bg-->
<!--End of Booking Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->
<?php $this->load->view('templates/footer');?>




<script type="text/javascript" src="<?=base_url('/public/assets/js/jquery.js'); ?>"></script>
<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<script type="text/javascript">
$(function(){
	$('.list_business_content .list_business_detail ul li input:checkbox').screwDefaultButtons({
		image: 'url("<?=base_url('/public/images/agree.jpg'); ?>")',
		width: 29,
		height: 29
	});	
});
</script>




<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 



</body>
</html>
