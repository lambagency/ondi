<?php
//print_r($pagedata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--CUSTOM FUNCTION-->

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
    
<!--FOR PARALAX SCROLLING-->


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of about_banner Section-->
<div class="about_banner_bg slide" data-stellar-background-ratio="0.5">
	<div class="about_banner_text">
    
    	<img src="<?php echo $this->config->item('base_url'); ?>public/images/contact_ondi_text.png" alt="" />
        
    	<!--<img src="<?php echo $this->config->item('base_url'); ?>public/images/about_ondi_text.png" alt="" />
        
    	<img src="<?php echo $this->config->item('base_url'); ?>public/images/about_ondi_text.png" alt="" />-->
    </div>
</div>
<!--End of about_banner Section-->
<!--Start of innerPage_Content-->
<div class="inner_content_bg5">
	<div class="inner_content">
    	<div class="contact_main">
            <div class="contact_main_detail">
				<?php $attributes = array('onsubmit' => 'return validatecontactform()', 'name' => 'ct_form'); ?>
				<?php echo form_open_multipart(base_url()."home/contact/", $attributes)?>
				<input type="hidden" name="submit" value="1" />
					<div class="heading">
						<h1>SEND A MESSAGE</h1>
					</div>
					<div class="sub_heading">
						<h3>Fill out the form below with your details and we'll get back to you as quick as humanly possible!</h3>
						<?php if(isset($success_message) && $success_message != ''){?>
						<h3><?=$success_message?></h3>
						<?php } ?>
					</div>
					<ul>
						<li><input type="text" value="*Contact Name" class="position_binfo" name="contact_name" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" /></li>
						<li><input type="text" value="*Email Address" class="position_binfo" name="email_address" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;" /></li>
						<li><textarea name="contact_message" cols="" rows="" class="position_area" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;">*Message</textarea></li>
					   
					</ul>
					<div class="phone">
						<p><span>Or phone:</span><br>
						1800 XXX XXX</p>
					</div>
					<div class="submit_info_btn">
						<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/contact_send.jpg" alt="" />
					</div>
                    
				 <?php echo form_close()?>
                 
            </div>
            
		</div>
       
    </div>
</div>
<!--End of innerPage_Content Section-->
<!--Start of Search, Compare And Book Section-->

<!--End of Search, Compare And Book Section-->
<!--Start of Bottom Baneer-->
<!--End of Bottom Baneer-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 
<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 




		<?php if ($this->agent->is_mobile())
		{
			
		?>
        
        
        
		<?php	
		}
		else
		{		
		?>
		
           

            <link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script>
            
            <script type="text/javascript">
            $(document).ready(function(){
                $('#nav2').localScroll(800);	
                //.parallax(xPosition, speedFactor, outerHeight) options:
                //xPosition - Horizontal position of the element
                //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                $('.about_banner_bg').parallax("60%", -0.7);
                $('.about_bottom_banner').parallax("60%", -0.7);
                
            
            })
            </script>
		
		
		<?php
		}
		
		?>
        
        
        
        


</body>
</html>
