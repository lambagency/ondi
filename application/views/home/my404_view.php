<?php
$randomnum = rand(1, 3);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>

<!--Start of FAQ Page_Content-->
<div class="inner_content_bg">
	<div class="error_content">
    	<div class="heading">
        	<h1>WOOPS!</h1>
        </div>
        <div class="sub_heading">
        	<h3>Error 404. I know, this is inconvenient...</h3>
        </div>

        <div class="error_detail_1" <?php if($randomnum== '1') { echo ' style="display:block;" '; } else { echo ' style="display:none;" '; } ?> >
        	<p>This page must have stepped out for lunch.</p>
            <span class="error_btn">
            	<a href="<?php echo $this->config->item('base_url'); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/back_to_home_red.jpg" alt="" /></a>
            </span>
        </div>

		<div class="error_detail_2"  <?php if($randomnum== '2') { echo ' style="display:block;" '; } else { echo ' style="display:none;" '; } ?> >
        	<p>This page is off getting a massage...</p>
            <span class="error_btn">
            	<a href="<?php echo $this->config->item('base_url'); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/back_to_home_pink.jpg" alt="" /></a>
            </span>
        </div>

		<div class="error_detail_3" <?php if($randomnum== '3') { echo ' style="display:block;" '; } else { echo ' style="display:none;" '; } ?> >
        	<p>This page had to get a trim, back in 5... maybe :/</p>
            <span class="error_btn">
            	<a href="<?php echo $this->config->item('base_url'); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/back_to_home_blue.jpg" alt="" /></a>
            </span>
        </div>
        
       
    </div><!--End of my_short_list_content-->
</div>
<!--End of FAQ Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?>
</body>
</html>