<?php
$monday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "monday");
$tuesday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "tuesday");
$wednesday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "wednesday");
$thursday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "thursday");
$friday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "friday");
$saturday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "saturday");
$sunday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "sunday");
//print_r($merchant_offers); die;
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<meta name="og:type" content="website" />
<meta name="og:image" content="<?php echo $this->config->item('base_url'); ?>public/images/logo.png"/>
<meta name="og:title" content="<?=$merchantdata->business_name?>" />
<meta name="og:description" content="<?=$merchantdata->business_name?>" />
<meta name="og:url" content="<?=base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->user_id))?>"/>


<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide/bjqs.css">
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&amp;sensor=false"></script>
<script type="text/javascript" src="http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.8/src/markerwithlabel.js"></script>
<style type="text/css">
   .labels {
     color: red;
     background-color: white;
     font-family: "Lucida Grande", "Arial", sans-serif;
     font-size: 12px;
     font-weight: bold;
     text-align: center;
     width: auto;
     border: 1px solid black;
     white-space: nowrap;
	 padding:0px 10px;
   }
 </style>

 <script type="text/javascript">
   function initMap() {
     var latLng = new google.maps.LatLng(<?=$merchantdata->lat?>,<?=$merchantdata->long?>);

     var map = new google.maps.Map(document.getElementById('map_canvas'), {
       zoom: 16,
       center: latLng,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     });

     var marker1 = new MarkerWithLabel({
       position: latLng,
       draggable: true,
       raiseOnDrag: true,
       map: map,
       labelContent: "<?=$merchantdata->business_name?>",
       labelAnchor: new google.maps.Point(22, 0),
       labelClass: "labels", // the CSS class for the label
       labelStyle: {opacity: 0.75}
     });

    

     var iw1 = new google.maps.InfoWindow({
       content: "<?=$merchantdata->business_name?>"
     });
     google.maps.event.addListener(marker1, "click", function (e) { iw1.open(map, this); });
   }
 </script>





</head>


<body onload="initMap()">
<?php $this->load->view('templates/header');?>

<!--Start of Popolo Banner-->
<?php 
if($merchantdata->widescreen_picture != "") 
{
	$bg_widescreen_image = $merchantdata->widescreen_picture;
}
else
{
	if($bg_image != "")
	{
		$bg_widescreen_image = $bg_image;
	}
	else
	{
		$bg_widescreen_image = "public/images/popolo_banner.jpg";
	}
}
//else $widescreen_picture = $merchantdata->widescreen_picture;


?>
<div class="popolo_banner_bg" style="background:url('<?php echo $this->config->item('base_url'); ?><?=$bg_widescreen_image?>') no-repeat scroll center top" >
  <div class="popolo_slider_bg">
    <div class="popolo_slider_main">
      <div class="popolo_left_slider">
        <div class="name_area">
          <h2><?=$merchantdata->business_name?></h2>
        </div>
        <div class="slider_details">
        	
            
            <div class="address">
          <h3><?=$merchantdata->street_address?>, <?=$merchantdata->suburb?> <?=$merchantdata->city_name?> <?=$merchantdata->state?> <?=$merchantdata->post_code?></h3>
          <?php if($merchantdata->business_phone !=""){?><h3>P : <?=$merchantdata->business_phone?></h3><?php } ?>
          <p><?=$merchantdata->suburb?> <span>•  <?=$str_business_types?>  •  <?=$merchantdata->pg_name?></span></p>
        </div>
        <div class="star_rating">
        
         	<?php if($merchant_reviews>0){?>
                <span class="rating"> 
                    <?php for($r=1; $r<=$merchant_reviews; $r++){?>
                    <img src="<?php echo $this->config->item('base_url'); ?>public/images/orange_star.png" alt="" /> 
                    <?php } ?>
                </span> 
            <?php } ?>
            
            
			<span class="web_link">
				<a href="<?="http://".str_replace("http://", "", $merchantdata->website_address)?>" target="_blank"><?=str_replace("http://", "", $merchantdata->website_address)?></a>
			</span> 
		</div>
        
        	
        </div>
        
      </div>
      <div class="popolo_right_slider">
        <div id="banner-slide"> 
        
        
        <div id="slides">
				<div class="slides_container">
					<?php 
			if($merchantdata->profile_picture == "") $profile_picture = "public/images/merchant-noimage.jpg";
			else $profile_picture = $merchantdata->profile_picture;

			if($merchantdata->supporting_picture == "") $supporting_picture = "";
			else $supporting_picture = $merchantdata->supporting_picture;

			if($merchantdata->widescreen_picture == "") $widescreen_picture = "";
			else $widescreen_picture = $merchantdata->widescreen_picture;
			?>
            
			<div>
            	<img src="<?php echo $this->config->item('base_url'); ?><?=$profile_picture?>" alt="" width="440" height="367"  title="profile_picture" />
            </div>
			
			<?php if($supporting_picture != ""){?>
            <div>
            	<img src="<?php echo $this->config->item('base_url'); ?><?=$supporting_picture?>" alt="" width="440" height="367" title="supporting_picture"/>
            </div>
			<?php } ?>
			
			<?php if($widescreen_picture != ""){?>
            <div>
            	<img src="<?php echo $this->config->item('base_url'); ?><?=$widescreen_picture?>" alt="" width="440" height="367"  title="widescreen_picture"/>
            </div>
			<?php } ?>

			</div>

				<?php if($supporting_picture != "" || $widescreen_picture != ""){?>
					<a href="#" class="prev"><img src="<?php echo $this->config->item('base_url'); ?>public/assets/detail_slide/img/arrow_l.png" alt="Arrow Prev"></a>
					<a href="#" class="next"><img src="<?php echo $this->config->item('base_url'); ?>public/assets/detail_slide/img/arrow_r.png" alt="Arrow Next"></a>
				<?php  } ?>
			</div>
            
            
            
          
        
          
        </div>
      </div>
    </div>
  </div>
</div>

<!--End of Popolo Banner--> 

<!--Start of Popolo Page_Content-->
<div class="inner_content_bg6">
  <div class="popolo_content">
    <div class="current_offers">
      <h2>Current offers available:</h2>
      <div class="current_offers_detail">
        <?php 
		if(sizeof($merchant_offers)>0){
			foreach($merchant_offers as $thisoffer){
				
				
				$how_many_left = $this->offers->how_many_left($thisoffer->id);
				$this->users->update_details_viewed_offer($thisoffer->id);

				//$how_many_left = sizeof($how_many_left_data);
				?>
				<ul>
				  <li>
					<div class="name">
					  <table class="table_resort">
						<tr>
						  <td><?=$thisoffer->offer_title?></td>
						</tr>
					  </table>
					</div>
					<div class="value">
					  <p>$<?=$thisoffer->price?> <span>(Value $<?=$thisoffer->price_normally?>)</span></p>
					</div>

					<?php if(strtotime($thisoffer->offer_running_to)>=strtotime(date("Y-m-d"))){?>
					<div class="book"><a href="<?php echo base_url("booking/index/?oid=".base64_encode($thisoffer->id)); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/book_btn.jpg" alt=""></a>
					  <?php if($how_many_left>0){?><div class="only_left">Only <?=$how_many_left?> left</div><?php } ?>
					</div>
					<?php } ?>


					<div class="remove"> 
					<div class="share">
					  <p>Share</p>
					  <span class="nav_social">
						<a href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($thisoffer->id)?>&t=<?=urlencode($thisoffer->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
						<a href="https://twitter.com/home?status=<?=urlencode($thisoffer->offer_title)?>%20-%20<?=$this->offers->get_offer_url($thisoffer->id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
						<a href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($thisoffer->id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
					  </span>
					</div>
					
					<a class="remove_s" id="anc_removefrom_shortlist_<?=$thisoffer->id?>" href="javascript:void(0);" onclick="removefrom_shortlist(<?=$thisoffer->id?>);" <?php if($this->offers->check_offer_inshortlist($thisoffer->id)){?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> >Remove</a>
								
								
					<a class="add_s" id="anc_addto_shortlist_<?=$thisoffer->id?>" href="javascript:void(0);" onclick="addto_shortlist(<?=$thisoffer->id?>);" <?php if($this->offers->check_offer_inshortlist($thisoffer->id)){?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?> >Shortlist</a>

					</div>
				  </li>
				  <li class="offer_gray">
					<p><?=$thisoffer->offer_description?></p>
				  </li>
				</ul>
			<?php 
			}
		}
		?>
        
      </div>
      <!--End of current_offers_detail--> 
    </div>
    <div class="popolo_contact">
      <div class="popolo_contact_left">
        <div class="overview">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/overview_img.png" alt="" />Overview</h2>
          <p><?=$merchantdata->about_business?></p>
        </div>
        <!--End of overview-->
        <div class="map_bg">
          <div class="map" id="map_canvas">
            <!-- <iframe width="532" height="265" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?=$merchantdata->lat?>,<?=$merchantdata->long?> (<?=$merchantdata->business_name?>)&amp;output=embed"></iframe> -->
			<!-- <iframe width="532" height="265" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?=urlencode($merchantdata->business_name)?>,<?=urlencode($merchantdata->street_address)?>,<?=urlencode($merchantdata->suburb)?>,<?=urlencode($merchantdata->city_name)?>,<?=urlencode($merchantdata->state)?>,<?=urlencode($merchantdata->post_code)?>&amp;output=embed"></iframe> -->
			
			<!-- <iframe width="532" height="265" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?=urlencode($merchantdata->business_name)?>,<?=urlencode($merchantdata->street_address)?>, <?=urlencode($merchantdata->suburb)?>, <?=urlencode($merchantdata->city_name)?>, <?=urlencode($merchantdata->state)?>,  <?=urlencode($merchantdata->post_code)?>&amp;output=embed"></iframe> -->

			<!-- <iframe width="532" height="265" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=<?=$merchantdata->lat?>,<?=$merchantdata->long?> (<?=$merchantdata->business_name?>)&amp;output=embed"></iframe> -->

          </div>
          <div class="address"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/address_img.png" alt="" />
            <h3>Address</h3>
            <p><?=$merchantdata->street_address?>, <?=$merchantdata->suburb?> <?=$merchantdata->city_name?> <?=$merchantdata->state?> <?=$merchantdata->post_code?></p>
          </div>
          <!--End of address--> 
        </div>
        <!--End of map_bg-->
        <div class="social_bg">
		  <?php if($merchantdata->facebook != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->facebook)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_fb.jpg" alt="" /><p><?=str_replace("http://", "", $merchantdata->facebook)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->twitter != ''){?>
          <div class="social_bg_right">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->twitter)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_twitter.jpg" alt="" /><p><?=str_replace("http://", "", $merchantdata->twitter)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->linkedin != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->linkedin)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_linkedin.gif" alt="" /><p><?=str_replace("http://", "", $merchantdata->linkedin)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->googleplus != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->googleplus)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_googleplus.png" alt="" /><p><?=str_replace("http://", "", $merchantdata->googleplus)?></p></a>
		  </div>
		  <?php } ?>

        </div>
        <!--End of social_bg--> 
      </div>
      <div class="popolo_contact_right">
        <div class="opening_hours">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/opening_hour.png" alt="" />Opening hours</h2>
		  <ul>
			<?php if(sizeof($monday_opening_hours)>0){?>
            <li><span>Monday</span> <?=$monday_opening_hours[0]->from_hour?> <?=$monday_opening_hours[0]->from_ampm?>-<?=$monday_opening_hours[0]->to_hour?> <?=$monday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($tuesday_opening_hours)>0){?>
            <li><span>Tuesday</span> <?=$tuesday_opening_hours[0]->from_hour?> <?=$tuesday_opening_hours[0]->from_ampm?>-<?=$tuesday_opening_hours[0]->to_hour?> <?=$tuesday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($wednesday_opening_hours)>0){?>
            <li><span>Wednesday</span> <?=$wednesday_opening_hours[0]->from_hour?> <?=$wednesday_opening_hours[0]->from_ampm?>-<?=$wednesday_opening_hours[0]->to_hour?> <?=$wednesday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($thursday_opening_hours)>0){?>
            <li><span>Thursday</span> <?=$thursday_opening_hours[0]->from_hour?> <?=$thursday_opening_hours[0]->from_ampm?>-<?=$thursday_opening_hours[0]->to_hour?> <?=$thursday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($friday_opening_hours)>0){?>
            <li><span>Friday</span> <?=$friday_opening_hours[0]->from_hour?> <?=$friday_opening_hours[0]->from_ampm?>-<?=$friday_opening_hours[0]->to_hour?> <?=$friday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($saturday_opening_hours)>0){?>
            <li><span>Saturday</span> <?=$saturday_opening_hours[0]->from_hour?> <?=$saturday_opening_hours[0]->from_ampm?>-<?=$saturday_opening_hours[0]->to_hour?> <?=$saturday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($sunday_opening_hours)>0){?>
            <li><span>Sunday</span> <?=$sunday_opening_hours[0]->from_hour?> <?=$sunday_opening_hours[0]->from_ampm?>-<?=$sunday_opening_hours[0]->to_hour?> <?=$sunday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 

          </ul>
        </div>
        <div class="parking">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/parking.png" alt="" />Parking</h2>
          <ul>
            <li><span><?=$merchantdata->parking_details?></li>
          </ul>
        </div>
        <div class="menu">
		<?php
		$findme1   = 'Restaurant';
		$findme2   = 'Cafe';
		$pos1 = strpos($str_business_types, $findme1);
		$pos2 = strpos($str_business_types, $findme2);
		if((bool)$this->config->item('test_mode'))
		{
			//echo "str_business_types-->".$str_business_types;
			//echo "pos1-->".$pos1;
			//echo "pos2-->".$pos2;
		}

		if ($pos1 === false && $pos2 === false)
		{
			$str_menu = "Price List";	
		} 
		else 
		{
			$str_menu = "Menu";
		}

		  ?>
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/menu.png" alt="" /><?=$str_menu?></h2>
          <ul>
            <li><a href="<?php echo $this->config->item('base_url'); ?><?=$merchantdata->pdf_menu?>" target="_blank">Take a look at our <?=strtolower($str_menu)?></a></li>
          </ul>
        </div>
        <div class="questions">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/questions.png" alt="" />Have questions?</h2>
          <ul>
            <li><a href="<?php echo base_url("home/faq/"); ?>"><span>Read through our FAQs</span> 
            	<img src="<?php echo $this->config->item('base_url'); ?>public/images/question_arrow.png" alt="" /></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!--End of popolo_contact--> 
  </div>
  <!--End of Popolo_content--> 
</div>
<!--End of Popolo Page_Content--> 

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?>
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide//js/bjqs-1.3.min.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide//js/libs/jquery.secret-source.min.js"></script>

<!-- attach the plug-in to the slider parent element and adjust the settings as required --> 
<script class="secret-source">
        jQuery(document).ready(function($) {
          
          $('#banner-slide').bjqs({
            animtype      : 'slide',
            height        : 367,
            width         : 440,
            responsive    : true,
            randomstart   : false
          });
          
        });
</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/detail_slide/js/slides.min.jquery.js"></script>
	<script>
		$(function(){
			$('#slides').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				play: 5000,
				pause: 2500,
				hoverPause: true,
				animationStart: function(){
					$('.caption').animate({
						bottom:-35
					},100);
				},
				animationComplete: function(current){
					$('.caption').animate({
						bottom:0
					},200);
					if (window.console && console.log) {
						// example return of current slide number
						console.log(current);
					};
				}
			});
		});
	</script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/detail_slide/css/global.css">
    
    
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


</body>
</html>
