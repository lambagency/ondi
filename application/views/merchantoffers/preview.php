<?php
$monday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "monday");
$tuesday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "tuesday");
$wednesday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "wednesday");
$thursday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "thursday");
$friday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "friday");
$saturday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "saturday");
$sunday_opening_hours = $this->offers->get_merchant_opening_hours($merchantdata->user_id, "sunday");

//print_r($merchant_offers); die;
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide/bjqs.css">
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

</head>


<body>
<?php $this->load->view('templates/header');?>

<!--Start of Popolo Banner-->
<div class="popolo_banner_bg">
  <div class="popolo_slider_bg">
    <div class="popolo_slider_main">
      <div class="popolo_left_slider">
        <div class="name_area">
          <h2><?=$merchantdata->business_name?></h2>
        </div>
        <div class="address">
          <h3><?=$merchantdata->street_address?>, <?=$merchantdata->suburb?> <?=$merchantdata->city_name?> <?=$merchantdata->state?> <?=$merchantdata->post_code?></h3>
          <p><?=$merchantdata->suburb?> <span>•  Italian  •  <?=$merchantdata->pg_name?></span></p>
        </div>
        <div class="star_rating"> 
			<span class="rating"> 
				<?php for($r=1; $r<=$merchant_reviews; $r++){?>
				<img src="<?php echo $this->config->item('base_url'); ?>public/images/white_star.png" alt="" /> 
				<?php } ?>
			</span> 
			<span class="web_link">
				<a href="<?="http://".str_replace("http://", "", $merchantdata->website_address)?>" target="_blank"><?=str_replace("http://", "", $merchantdata->website_address)?></a>
			</span> 
		</div>
      </div>
      <div class="popolo_right_slider">
        <div id="banner-slide"> 
          
          <!-- start Basic Jquery Slider -->
          <ul class="bjqs">
            <li><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_slider_img.jpg" alt="" /></li>
            <li><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_slider_img2.jpg" alt="" /></li>
            <li><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_slider_img3.jpg" alt="" /></li>
          </ul>
          <!-- end Basic jQuery Slider --> 
          
        </div>
      </div>
    </div>
  </div>
</div>

<!--End of Popolo Banner--> 

<!--Start of Popolo Page_Content-->
<div class="inner_content_bg">
  <div class="popolo_content">
    <div class="current_offers">
      <h2>Current offers available:</h2>
      <div class="current_offers_detail">
        <?php 
		if(sizeof($merchant_offers)>0){
			foreach($merchant_offers as $thisoffer){
				$how_many_left = $this->offers->how_many_left($thisoffer->id);
				//$how_many_left = sizeof($how_many_left_data);
				?>
				<ul>
				  <li>
					<div class="name">
					  <table class="table_resort">
						<tr>
						  <td><?=$thisoffer->offer_title?></td>
						</tr>
					  </table>
					</div>
					<div class="value">
					  <p>$<?=round($thisoffer->price)?> <span>(Value $<?=round($thisoffer->price_normally)?>)</span></p>
					</div>
					<div class="book">
					<img src="<?php echo $this->config->item('base_url'); ?>public/images/book_btn.jpg" alt="">					  
					</div>
					<div class="remove"> 
					<div class="share">
					<!-- <a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/share.png" alt=""></a> -->
					  <p>Share</p>
					  <span class="nav_social">						
						<img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" />
						<img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" />
						<img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" />						
						</a>
					  </span>
					</div>
					
					<!-- <a href="javascript:void(0);"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cancel.png" alt=""><p>Remove</p></a> -->
								
								
					<a href="javascript:void(0);"><img src="<?php echo $this->config->item('base_url'); ?>public/images/short_list_icon.png" alt=""><p>Shortlist</p></a>

					</div>
				  </li>
				  <li class="offer_gray">
					<?=$thisoffer->offer_description?>
				  </li>
				</ul>
			<?php 
			}
		}
		?>
        
      </div>
      <!--End of current_offers_detail--> 
    </div>
    <div class="popolo_contact">
      <div class="popolo_contact_left">
        <div class="overview">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/overview_img.png" alt="" />Overview</h2>
          <p><?=$merchantdata->about_business?></p>
        </div>
        <!--End of overview-->
        <div class="map_bg">
          <div class="map">
            <iframe width="532" height="265" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=<?=$merchantdata->lat?>,<?=$merchantdata->long?> (<?=$merchantdata->business_name?>)&amp;output=embed"></iframe>
          </div>
          <div class="address"> <img src="<?php echo $this->config->item('base_url'); ?>public/images/address_img.png" alt="" />
            <h3>Address</h3>
            <p><?=$merchantdata->street_address?>, <?=$merchantdata->suburb?> <?=$merchantdata->city_name?> <?=$merchantdata->state?> <?=$merchantdata->post_code?></p>
          </div>
          <!--End of address--> 
        </div>
        <!--End of map_bg-->
        <div class="social_bg">
		  <?php if($merchantdata->facebook != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->facebook)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_fb.jpg" alt="" /><p><?=str_replace("http://", "", $merchantdata->facebook)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->twitter != ''){?>
          <div class="social_bg_right">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->twitter)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_twitter.jpg" alt="" /><p><?=str_replace("http://", "", $merchantdata->twitter)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->linkedin != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->linkedin)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_linkedin.gif" alt="" /><p><?=str_replace("http://", "", $merchantdata->linkedin)?></p></a>
		  </div>
		  <?php } ?>

		  <?php if($merchantdata->googleplus != ''){?>
          <div class="social_bg_left">
			<a href="<?="http://".str_replace("http://", "", $merchantdata->googleplus)?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/popolo_googleplus.png" alt="" /><p><?=str_replace("http://", "", $merchantdata->googleplus)?></p></a>
		  </div>
		  <?php } ?>

        </div>
        <!--End of social_bg--> 
      </div>
      <div class="popolo_contact_right">
        <div class="opening_hours">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/opening_hour.png" alt="" />Opening hours</h2>
		  <ul>
			<?php if(sizeof($monday_opening_hours)>0){?>
            <li><span>Monday</span> <?=$monday_opening_hours[0]->from_hour?> <?=$monday_opening_hours[0]->from_ampm?>-<?=$monday_opening_hours[0]->to_hour?> <?=$monday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($tuesday_opening_hours)>0){?>
            <li><span>Tuesday</span> <?=$tuesday_opening_hours[0]->from_hour?> <?=$tuesday_opening_hours[0]->from_ampm?>-<?=$tuesday_opening_hours[0]->to_hour?> <?=$tuesday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($wednesday_opening_hours)>0){?>
            <li><span>Wednesday</span> <?=$wednesday_opening_hours[0]->from_hour?> <?=$wednesday_opening_hours[0]->from_ampm?>-<?=$wednesday_opening_hours[0]->to_hour?> <?=$wednesday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($thursday_opening_hours)>0){?>
            <li><span>Thursday</span> <?=$thursday_opening_hours[0]->from_hour?> <?=$thursday_opening_hours[0]->from_ampm?>-<?=$thursday_opening_hours[0]->to_hour?> <?=$thursday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($friday_opening_hours)>0){?>
            <li><span>Friday</span> <?=$friday_opening_hours[0]->from_hour?> <?=$friday_opening_hours[0]->from_ampm?>-<?=$friday_opening_hours[0]->to_hour?> <?=$friday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($saturday_opening_hours)>0){?>
            <li><span>Saturday</span> <?=$saturday_opening_hours[0]->from_hour?> <?=$saturday_opening_hours[0]->from_ampm?>-<?=$saturday_opening_hours[0]->to_hour?> <?=$saturday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 
			
			<?php if(sizeof($sunday_opening_hours)>0){?>
            <li><span>Sunday</span> <?=$sunday_opening_hours[0]->from_hour?> <?=$sunday_opening_hours[0]->from_ampm?>-<?=$sunday_opening_hours[0]->to_hour?> <?=$sunday_opening_hours[0]->to_ampm?></li>
			<?php  } ?> 

          </ul>
        </div>
        <div class="parking">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/parking.png" alt="" />Parking</h2>
          <ul>
            <li><span><?=$merchantdata->parking_details?></li>
          </ul>
        </div>
        <div class="menu">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/menu.png" alt="" />Menu</h2>
          <ul>
            <li><a href="#">Take a look at our menu</a></li>
          </ul>
        </div>
        <div class="questions">
          <h2><img src="<?php echo $this->config->item('base_url'); ?>public/images/questions.png" alt="" />Have questions?</h2>
          <ul>
            <li><a href="javascript:void(0)">Read through our FAQ's <img src="<?php echo $this->config->item('base_url'); ?>public/images/question_arrow.png" alt="" /></a></li>
          </ul>
        </div>
      </div>
    </div>
    <!--End of popolo_contact--> 
  </div>
  <!--End of Popolo_content--> 
</div>
<!--End of Popolo Page_Content--> 

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?>
<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide//js/bjqs-1.3.min.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/easy_slide//js/libs/jquery.secret-source.min.js"></script>

<!-- attach the plug-in to the slider parent element and adjust the settings as required --> 
<script class="secret-source">
        jQuery(document).ready(function($) {
          
          $('#banner-slide').bjqs({
            animtype      : 'slide',
            height        : 333,
            width         : 400,
            responsive    : true,
            randomstart   : true
          });
          
        });
      </script>
</body>
</html>
