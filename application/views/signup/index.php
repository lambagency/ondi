<?php
$lybinfo_data = $this->Admin_Model->get_signupinfo();		
$box1 = $lybinfo_data[0]->box1;
$box2 = $lybinfo_data[0]->box2;
$box3 = $lybinfo_data[0]->box3;
$box4 = $lybinfo_data[0]->box4;
$box5 = $lybinfo_data[0]->box5;
$box6 = $lybinfo_data[0]->box6;
$box7 = $lybinfo_data[0]->box7;

$bg_image = $lybinfo_data[0]->bg_image;
if(empty($bg_image))
{
	$bg_image = 'public/images/signup_bottom_banner.jpg';
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/sliderstyle1.css'); ?>" />
<script type="text/javascript" src="<?=base_url('/public/assets/js/jquery.js'); ?>"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<!--CUSTOM FUNCTION-->

<!--FOR PARALAX SCROLLING-->
<!--<script type="text/javascript" src="<?=base_url('/public/assets/stellar/jquery.stellar.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('/public/assets/stellar/waypoints.min.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('/public/assets/stellar/jquery.easing.1.3.js'); ?>"></script>
<script type="text/javascript" src="<?=base_url('/public/assets/stellar/scripts.js'); ?>"></script>-->

<link href="<?=base_url('/public/assets/gender/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/assets/gender/jquery-ui.css'); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?=base_url('/public/assets/ui/demos.css'); ?>">


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />

<style>
	.signup_bottom_banner { width: 102%; height: 784px; float: left; background: url(<?php echo base_url() ?><?=$bg_image?>) no-repeat center top; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; }
</style>

</head>
<body>
<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav Section-->


<!--Start of signup_top Section-->
<div class="signup_top_bg">
	<div class="signup_top">
    	<div class="signup_top_main">
    		<p>I want it. I want it on demand. Gimme, gimme, gimme...</p>
            <div class="signup_pointer">
    		<img src="<?php echo base_url() ?>public/images/dd_arrow.png" alt="" />
    		</div>
        </div>
        
    </div>
    
</div>
<!--End of signup_top Section-->

<!--Start of signup_banner Section-->
<div class="signup_banner_bg slide" data-stellar-background-ratio="0.5">
	<div class="signup_banner_text">
    	<img src="<?php echo base_url() ?>public/images/signup_banner_text.png" alt="" />
    </div>
</div>
<!--End of signup_banner Section-->

<!--Start of innerPage_Content-->
<div class="inner_content_bg4">
	<div class="signup_content">
    	<div class="heading">
        	<h1>ENTER YOUR DETAILS HERE</h1>
        </div>
        
        <div class="sub_heading">
        	<h3>Sign up with Social media or email address below</h3>
        </div>
        
        	
			<div class="valid_errors">
			<?php 
				echo validation_errors(); 
				echo $this->session->flashdata('signup_message');
			?>
			</div>
	
			
			<?php echo form_open(base_url()."signup/", array('id'=>'customer_signup_frm'))?>
			<input type="hidden" name="signup_action" id="signup_action" value="">
			<div class="signup_main_top">
                <div class="signup_social_left">
						<?php
						$params = array(
						  'scope' => 'email',
						  'redirect_uri' => base_url("signup/facebooklogin/")
						);
						$fbloginurl =  $this->facebook->getLoginUrl($params);
						$gplusurl = "";
						$openid = new Openid('ondi.com');
						if(!$openid->mode)
						{
							$openid->identity = 'https://www.google.com/accounts/o8/id';
							$openid->required = array('contact/email', 'namePerson/first');
							$gplusurl = $openid->authUrl();
						}
						elseif($_GET['openid_mode'] == 'cancel')
						{
							//echo 'User has canceled authentication!';
						}
						else 
						{	
							if($_GET['openid_mode'] == 'id_res')
							{
								$openid->validate();
								$userinfo = $openid->getAttributes();
								print_r($userinfo); die;
								$email = $userinfo['contact/email'];
								$firstName = $userinfo['namePerson/first'];
								$this->customclass->googlepluslogin($email, $firstName);
							}
						}
						?>
                        <ul>
                            <li><a href="<?=$fbloginurl?>" target="_blank" ><img src="<?php echo base_url() ?>public/images/login_fb.jpg" alt="" /></a></li>
                            <li><a href="<?php echo $this->config->item('base_url'); ?>signup/twitterlogin/" target="_blank"><img src="<?php echo base_url() ?>public/images/login_twitter_blue.jpg" alt="" /></a></li>
                            <li><a href="<?=$gplusurl?>" target="_blank" ><img src="<?php echo base_url() ?>public/images/login_g+.jpg" alt="" /></a></li>
                            <li><p>or</p></li>
                        </ul>
                </div><!--End of login_dd_left-->
                <div class="signup_social_right">
                	<input type="text" value="" class="position_login" name="customer_email" id="customer_email" placeholder="Email address" />
                    <input type="password" name="customer_password" id="customer_password" class="position_login" placeholder="Password" />

					<a href="javascript:void(0)" onclick="show_create_account()"><img src="<?php echo base_url() ?>public/images/signup_btn.jpg" alt="" /></a>


                </div><!--End of login_dd_right-->
        	</div>
            
            <div class="signup_form" id="create_account_div" style="display:none;">
            	<ul>
                	<li>
                    	<div class="signup_form_left"></div>
                        <div class="signup_form_right"><input type="checkbox" name="subscribe_newsletter" id="ex2_a" value="1" >
						<p>eNewsletter sign up</p></div>

                    </li>
                    
                    <li>
                    	<div class="signup_form_left"><input type="text" value="" class="position_signup_code" name="customer_postcode" id="customer_postcode" placeholder="*Postcode" />

							<div class="signup_gender gender"><p>Gender</p>
                            	 <div id="chooseplan">
                                    <div class="cleaner h10"></div>
                                    <div id="hostpslider"></div>
                                    <div class="cleaner h10"></div>
                                  </div>
                                 <div class="info">(Men click left. Ladies click right)</div>    
								 <input type="hidden" name="gender" id="gender" value="">
                            </div>


                        </div>
                        <div class="signup_form_right"></div>
                    </li>
                    <li>
                    	<div class="signup_form_left"><input type="text" value="" class="position_signup_form" name="customer_referral_key" id="customer_referral_key" placeholder="Referrer" /></div>
                        <div class="signup_form_right"></div>
                    </li>
                    <li>
                    	<div class="create_account">                        	
							<a href="javascript:void(0)" onclick="submit_signup_form('register')"><img src="<?php echo base_url() ?>public/images/create_account_btn.jpg" alt="" /></a>						
							<!-- <input type="image" src="<?php echo base_url() ?>public/images/create_account_btn.jpg"> -->                            
							<p>By creating an account, you agree to Ondis Terms of Service and Privacy Policy.</p>
                        </div>
                    </li>
                    <li>
                    	<div class="exist">ALREADY ON ONDI? <a class="log_in_c">LOG IN</a></div>
                    </li>
                </ul>
            </div>
			<?php echo form_close()?>








            
   </div><!--End of inner_content_bg-->
</div><!--End of signup_content-->
<!--End of innerPage_Content Section-->


<!--Start of Bottom Baneer-->
<div class="signup_bottom_banner slide" data-stellar-background-ratio="0.5">
    <div class="signup_banner_con">
    	<ul>
        	<li class="purple">
            	<h2><img src="<?php echo base_url() ?>public/images/sign_up_banner_clock.png" alt="" /></h2>
                <p>
					<!-- <span>Speed up the booking</span> process with one-click booking -->
					<?php echo $box1; ?>
				</p>
            </li>
            <li class="green gray_box">
            	<h2><img src="<?php echo base_url() ?>public/images/signup_banner_user.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_calender.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_star.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_dimond.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_msg.png" alt="" /></h2>
                <p>
					<!-- <span>Update</span> your profile, manage your email marketing subscriptions, and save a list of your favourites with Wish List -->
					<?php echo $box2; ?>
				</p>
            </li>
            <li class="skyblue">
            	<h2><img src="<?php echo base_url() ?>public/images/signup_banner_share.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_fb.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_twitter.png" alt="" />
                <img src="<?php echo base_url() ?>public/images/signup_banner_g+.png" alt="" /></h2>
                
				<!-- <p><span>Easily share</span> <br/>with your friends</p>
                <p>...life is better <br/>with company</p> -->
				<p><?php echo $box3; ?></p>
            </li>
            <li class="yellow gray_box">
            	<h2><img src="<?php echo base_url() ?>public/images/signup_banner_manage.png" alt="" /></h2>
                <p>
					<!-- <span>Manage all your <br/>bookings online,</span> <br/>including current <br/>and future bookings -->
					<?php echo $box4; ?>
				</p>
            </li>
            <li class="orenge" style=" background: url(<?php echo base_url() ?>public/images/signup_banner_direction.png) no-repeat 110px 28px #fff; ">
            	<!-- <p><span>Easily</span> get <br/>directions</p> -->
				<?php echo $box5; ?>
            </li>
            <li class="pink gray_box">
            	<h2><img src="<?php echo base_url() ?>public/images/signup_banner_big_dimond.png" alt="" /></h2>
                <p>
					<!-- <span>Earn rewards</span> <br/>every time <br/>you book! -->
					<?php echo $box6; ?>
				</p>
            </li>
            <li class="white gray_box fill_length">
            	<div class="white_box_text">
				
					<!-- <span>Register</span> to receive e-newsletter updates from ondi.com, <br/>including special offers direct to your inbox -->
					<?php echo $box7; ?>
				
				</div>
                <div class="white_box_msg"><img src="<?php echo base_url() ?>public/images/signup_banner_msg2.png" alt="" /></div>
                <div class="white_box_comment">3</div>
            </li>
        </ul>
    </div>
</div>
<!--End of Bottom Baneer-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?><!--End of footer_bg-->


<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?=base_url('/public/assets/bgstretcher/js/jquery-1.11.0.min.js'); ?>"></script>

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="<?=base_url('/public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js'); ?>"></script>

<!--FOR LOGIN CHECKBOX-->


<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<!--FOR GENDER SLIDER-->

<script src="<?=base_url('/public/assets/gender/jquery-ui.js'); ?>" type="text/javascript"></script>

<!--Price Selector content--> 
<script type="text/javascript"> 
	
	var DS = new Array("M", "", "F");            
		$(function() {
            $("#hostpslider").slider({
                range: "min",
                value: 1,
                min: 0,
                max: 2,
                step: 1,
                slide: function(event, ui) {
                    $("#DS").val(DS[ui.value]);

					$("#DS").val(DS[ui.value]);
					document.getElementById('gender').value = DS[ui.value];					           
                }
            });			

        });
		
		$('.signup_form ul li .signup_form_right input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo base_url() ?>public/images/agree.jpg")',
				width: 29,
				height: 29
		});
				
			
function show_create_account()
{		
	var customer_email = $("#customer_email").val();
	var customer_password = $("#customer_password").val();
	if(checkBlankField(customer_email) == false)
	{
		alert("Please enter email address.");
		$("#customer_email").focus();
		return false;
	}else{
		
		if(valid_email(customer_email)==true)
		{
			alert ( "Please enter the correct Email address." );
			$("#customer_email").focus();
			return false;
		}

	}
	if(checkBlankField(customer_password) == false)
	{
		alert("Please enter password.");
		$("#customer_password").focus();
		return false;
	}
	$("#create_account_div").show();	
}
function submit_signup_form(act)
{
	
	var customer_email = $("#customer_email").val();
	var customer_password = $("#customer_password").val();
	var customer_postcode = $("#customer_postcode").val();
	
	if(checkBlankField(customer_email) == false)
	{
		alert("Please enter email address.");
		$("#customer_email").focus();
		return false;
	}else{
		
		if(valid_email(customer_email)==true)
		{
			alert ( "Please enter the correct Email address." );
			$("#customer_email").focus();
			return false;
		}

	}
	if(checkBlankField(customer_password) == false)
	{
		alert("Please enter password.");
		$("#customer_password").focus();
		return false;
	}
	if(checkBlankField(customer_postcode) == false)
	{
		alert("Please enter postcode.");
		$("#customer_postcode").focus();
		return false;
	}
	document.getElementById("signup_action").value=act;
	document.getElementById("customer_signup_frm").submit();

}
</script>





<?php if ($this->agent->is_mobile())
		{
			
		?>
	
		
		
		
		
		<?php	
		}
		else
		{		
		?>
		
                        
           <link href="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/style.css" rel="stylesheet" type="text/css" />
			<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.parallax-1.1.3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.localscroll-1.2.7-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/paralax/jquery.scrollTo-1.4.2-min.js"></script>
            <script type="text/javascript">
            $(document).ready(function(){
                $('#nav2').localScroll(800);	
                //.parallax(xPosition, speedFactor, outerHeight) options:
                //xPosition - Horizontal position of the element
                //inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
                //outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
                $('.signup_banner_bg').parallax("50%", -0.5);
                $('.signup_bottom_banner').parallax("50%", -0.5);
                
                 
            })
            </script>
		
		
		<?php
		}
		
		?>
        
        


</body>
</html>
