<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<!--CUSTOM FUNCTION-->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><!--FOR PARALAX SCROLLING-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/jquery.stellar.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/scripts.js"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of Forgot Password Page_Content-->
<div class="inner_content_bg">
	<div class="forgot_pass_content">
    	<div class="heading">
        	<h1>OOPS! FORGOT YOUR PASSWORD?</h1>
        </div>
        <div class="sub_heading">
        	<h3>Don’t worry, it happens to the best of us!</h3>
        </div>
        <div class="forgot_pass_detail">
				<?php if(!empty($message)){?><div class="valid_errors"><?=$message?></div><?php } ?>
            	<ul>
                	<li>Enter your email address below for instructions on how to restore your password</li>
                    <li><input type="text" placeholder="Email address" class="position_forgot"  name="forgot_email" id="forgot_email"  value="" /></li>
                    <li><span class="yes_no">
                    <input name="forgot_is_merchant" id="forgot_is_merchant" type="checkbox" onClick="var j = jQuery('#check').attr('disabled', jQuery('#check').attr('disabled') ? false : true)">
                    </span><p>Merchant</p></li>
                    <li><span class="send_btn"><a href="javascript:void(0);" onclick="forgotPassword();"><img src="<?php echo $this->config->item('base_url'); ?>public/images/send_btn.jpg" alt="" /></a></span></li>
                </ul>
            <div class="forgot_social">
            	<p>Or you can also login with...</p>
				<?php
				$params = array(
				  'scope' => 'email',
				  'redirect_uri' => base_url("signup/facebooklogin/")
				);
				$fbloginurl =  $this->facebook->getLoginUrl($params);
				$gplusurl = "";
				$openid = new Openid('ondi.com');
				if(!$openid->mode)
				{
					$openid->identity = 'https://www.google.com/accounts/o8/id';
					$openid->required = array('contact/email', 'namePerson/first');
					$gplusurl = $openid->authUrl();
				}
				elseif($_GET['openid_mode'] == 'cancel')
				{
					//echo 'User has canceled authentication!';
				}
				else 
				{	
					if($_GET['openid_mode'] == 'id_res')
					{
						$openid->validate();
						$userinfo = $openid->getAttributes();
						print_r($userinfo); die;
						$email = $userinfo['contact/email'];
						$firstName = $userinfo['namePerson/first'];
						$this->customclass->googlepluslogin($email, $firstName);
					}
				}
				?>
                <ul>
                	<li><a href="<?=$fbloginurl?>" target="_blank" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/forgot_fb.jpg" alt="" /></a></li>
                    <li><a href="<?php echo $this->config->item('base_url'); ?>signup/twitterlogin/" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/forgot_twitter.jpg" alt="" /></a></li>
                    <li><a href="<?=$gplusurl?>" target="_blank"><img src="<?php echo $this->config->item('base_url'); ?>public/images/forgot_g+.jpg" alt="" /></a></li>
                </ul>
            </div>
        </div>
    </div><!--End of my_short_list_content-->
</div>
<!--End of Forgot Password Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
