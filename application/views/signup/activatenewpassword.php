<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<!--CUSTOM FUNCTION-->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><!--FOR PARALAX SCROLLING-->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/jquery.stellar.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/waypoints.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/stellar/scripts.js"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.safari-checkbox.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of Forgot Password Page_Content-->
<div class="inner_content_bg">
	<div class="forgot_pass_content">
    	<div class="heading">
        	<h1>RESTORE YOUR PASSWORD</h1>
        </div>
        <!-- <div class="sub_heading">
        	<h3>Don’t worry, it happens to the best of us!</h3>
        </div> -->
        <div class="forgot_pass_detail">
			<?php if(!empty($message)){?><div class="valid_errors"><?=$message?></div><?php } ?>
				<?php $attributes = array('onsubmit' => 'return validaterestpwdform()', 'name' => 'ct_form'); ?>
				<?php echo form_open_multipart(base_url()."signup/activatenewpassword/?key=".$_GET['key'], $attributes)?>
				<input type="hidden" name="act" value="restore" />
				<ul>
                    <li><input type="password" placeholder="Password" class="position_forgot"  name="reset_pwd" id="reset_pwd"  value="" /></li>
                    <li><input type="password" placeholder="Confirm Password" class="position_forgot"  name="reset_cpwd" id="reset_cpwd"  value="" /></li>
                    <li><span class="send_btn"><input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/submit_info_btn.jpg" alt="" /></span></li>
                </ul>
				<?php echo form_close()?>
        </div>
    </div><!--End of my_short_list_content-->
</div>
<!--End of Forgot Password Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
