<?php
$sess_user_data = $this->session->all_userdata();
//print_r($hoursdata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/booking.jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.theme_booking.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />




</head>
<body class="booking">
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>

<div class="inner_content_bg">
	<?php $attributes = array('onsubmit' => 'return validatebookingform()', 'name' => 'booking_form'); ?>
	<?php echo form_open_multipart(base_url()."booking/index/?oid=".base64_encode($offerdata->offer_id), $attributes)?>
	<input type="hidden" name="submit" value="1" />
	<input type="hidden" name="oid" value="<?=$offerdata->offer_id?>" />
	<input type="hidden" name="offerprice" id="offerprice" value="<?=$offerdata->price?>" />
	<div class="booking_content">
    	<div class="heading">
        	<h1>BOOKING</h1>
        </div>
        <div class="sub_heading">
        	<h3>So lets get this straight...</h3>
        </div>
        <div class="booking_detail">
        	<h2>You’re keen to lock in the following:</h2>
            <div class="booking_box">
				<?php
				if($offerdata->profile_picture != "")
				{
					$offerdata_profile_picture = $offerdata->profile_picture;
				}
				else
				{
					$offerdata_profile_picture = "public/images/merchant-noimage.jpg";
				}
				?>
            	<div class="image"><img src="<?php echo $this->config->item('base_url'); ?><?=$offerdata_profile_picture?>" width="150" height="125" alt="" /></div>
                <div class="address">
					<?php
					if(strlen($offerdata->business_name)>20) $bname=substr($offerdata->business_name, 0, 20)."..";
					else $bname = $offerdata->business_name;
					?>
                	<h3><?=$bname?></h3>
					<?php
					$suburb_x = $offerdata->suburb;
					if(strlen($suburb_x)>10) $suburb_x=substr($suburb_x, 0, 10)."..";
					?>
                    <h4><?=$suburb_x?></h4>
					<?php
					$categs = $this->users->display_merchant_business($offerdata->business_type);
					if(strlen($categs)>30) $categs=substr($categs, 0, 30)."..";
					?>
                    <p><?=$categs?></p>
                    <div class="cradit"><p><?=$offerdata->pg_name?><span></span></p><img src="<?php echo $this->config->item('base_url'); ?>public/images/<?=$this->offers->get_offer_star_ratings($offerdata->offer_id)?>.png" alt="" /></div>
                </div>
                <div class="name">
                        	<table class="small_table">
                              <tr>
                                <td><?=$offerdata->offer_title?></td>
                              </tr>
                            </table>
                 </div>
                <div class="value"><p>$<?=$offerdata->price?> <span>(Value $<?=$offerdata->price_normally?>)</span></p></div>
            </div>

			<p>Please note: Once the payment process is completed all changes/cancellations to the booking are at the discretion of the merchant, unless within a 24hr window of the booking date in which no changes/cancellations are permitted. </p>
        </div>
		<?php
		$validationerrors = validation_errors();
		$error_message = $this->session->flashdata('error_message');
		if(trim($validationerrors)!= ''|| trim($error_message)!= ''){?>
		<div class="valid_errors"><?php echo $validationerrors; ?><?php echo $error_message; ?></div>
		<?php } ?>

        <div class="booking_form">
        	<ul>
            	
                <li>
                	<p>on</p>
					<?php 
					if(isset($sess_user_data['search_date']))
					{
						$search_date = date("d/m/Y", strtotime($sess_user_data['search_date']));
					}
					else
					{
						$search_date = set_value('booking_date');
					}

					?>
                    <input type="text" id="datepicker" size="30" name="booking_date" value="<?=$search_date?>" />
                   
                </li>
                <li id="li_timings">
                	<p>at</p>
					<?php $current_time = date("Gi"); ?>
					<?php //echo $sess_user_data['search_date']; echo date("Y-m-d"); ?>
                    <div class="custom_select_ajax">
					<?php if(sizeof($hoursdata)>0){?>
                        <select name="booking_time" id="booking_time" class="revostyled" >
                            <option value="" >Select Time</option>
                            <?php 
							foreach($hoursdata as $thehoursdata)
							{
								?>
								<option value="<?=$thehoursdata['bookingtime']?>" ><?=date("h:i A", strtotime($thehoursdata['bookingtime']))?> (<?=$thehoursdata['seats_remain']?> Avail)</option>
								<?php } ?>
                        </select>
                        <?php } else { ?>
                            <select name="booking_time" id="booking_time" class="revostyled" >
                                <option value="" >Not available</option>
                            </select>
                        <?php } ?>
                        
                    </div>
                </li>

				<li>
                	<p>for</p>
                    <input type="text" placeholder="Number of People (e.g. 2)" class="price" name="number_of_people" id="number_of_people" value="<?php echo set_value('number_of_people'); ?>" onkeypress="return isNumberKey(event);" onkeyup="calculatetotal(this.value);" onblur="calculatetotal(this.value);" />
                </li>
                
                
				<li id="lidiscount" <?php if(isset($_POST['discount']) && $_POST['discount'] != ''){ echo 'style="display:block;"';  } else { echo 'style="display:none;"'; } ?> >
                	<p>discount </p>
                    <input type="text" class="price" readonly id="discount" name="discount" value="<?php if(isset($_POST['discount']) && $_POST['discount'] != ''){ echo $_POST['discount'];  }  ?>" />
                </li>
                <li>
                	<p>totalling </p>
                    <input type="text" class="price" readonly id="total_price" name="total_price" value="<?php echo set_value('total_price'); ?>" />
                </li>
            </ul>
        </div>
        <div class="got_promotion_code">
        	<input type="button" value="code" class="promotion_code_btn" />
            <input type="text" placeholder="We'll see :) type it in here..." class="promotion_code" name="promotion_code" id="promotion_code" value="<?php echo set_value('promotion_code'); ?>"   />
			<a href="javascript:void(0);" onclick="calculatediscount();" id="anccalculatediscount"><img src="<?php echo $this->config->item('base_url'); ?>public/images/redeem.jpg" alt=""></a>
         </div>
         <div class="book_it_in_btn">
         	<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/book_it_in.jpg" alt="" />
         </div>
         
         <div class="payment_mode">
         	<div class="full_security">
            	<p>Full security</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/full_security.jpg" alt="">
            </div>
            <div class="payment_method">
            	<p>Payment Methods</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/payment_metnod.jpg" alt="">
            </div>
            <div class="authentic"><img src="<?php echo $this->config->item('base_url'); ?>public/images/authentic.jpg" alt=""></div>
         </div>
    </div><!--End of inner_content_bg-->
	<?php echo form_close()?>
</div><!--End of inner_content_bg-->
<!--End of Booking Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->
<?php $this->load->view('templates/footer');?>




<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery-1.8.3.js"></script>
    
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
		
    
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script>
		
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.core.js"></script>
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.datepicker.js"></script>
    

<script>
	$(function() {
		var $c1 = jQuery.noConflict();
		var $c2 = jQuery.noConflict();
		$c1( "#datepicker" ).datepicker({
			dateFormat: 'dd/mm/yy',
			minDate: '<?=date("d/m/Y")?>',
			maxDate: '<?=date("d/m/Y", strtotime("+7 days"))?>',
			onSelect: function(dateText, inst) { 
				ajaxGetOfferTimingsByDate(dateText, <?=$offerdata->offer_id?>);
			}
		});
		
		$c2( "#anim" ).change(function() {
			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
		});
	
	
		$(".revostyled").selectbox();
	});
</script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


<?php if(isset($_GET['q']) && $_GET['q'] == 'na' && $_GET['s'] != ''){?>
<script language="javascript">
<?php if($_GET['s']=='0'){?>
alert("Sorry! No quantity available");
<?php } else {?>
alert("Sorry! This offer only allowed to <?=$_GET['s']?> number of people with one booking");
<?php } ?>
</script>
<?php } ?>

</body>
</html>
