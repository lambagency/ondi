<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<div class="inner_content_bg">
	<div class="checkout_guest_content">
    	<div class="heading">
        	<h1>CHECKOUT AS GUEST OR LOGIN</h1>
        </div>
        <div class="sub_heading">
        	<h3>Checkout</h3>
        </div>
		<div class="valid_errors">
		<?php if(isset($_GET['login']) && $_GET['login']=='failed' ){ echo "Incorrect login details!"; } ?>
		</div>
		
		<?php echo form_open_multipart(base_url()."booking/checkout/", array('onSubmit'=> "return validatecheckout()", 'name' => 'checkoutform')); ?>
		<input type="hidden" name="submit" value="1" />
		<input type="hidden" name="order_id" value="<?=$order_id?>" />
        <div class="checkout_guest_detail">
        	<div class="progress_bar_bg">
            	<div class="progress_bar">
            		<ul>
                    	<li class="pink"></li>
                        <li class="gray2"></li>
                        <li class="light_gray"></li>
                    </ul>
                    <div class="green_pointer"><img src="<?php echo $this->config->item('base_url'); ?>public/images/check_green.png" alt=""></div>
          	    </div>
                <div class="step_number">
            		<p>1</p>
 		        </div>
            </div>
            
            
                <div class="option">
                    <ul>
                        <li class="active" id="li_guest" onClick="changeusertype_li(1, this);" ><input type="radio" name="login_mode" id="guest" class="guest_btn" value="guest" onClick="changeusertype(1);" checked="checked" /><p>Guest</p></li>
                        <li id="li_returning" onClick="changeusertype_li(2, this);"  ><input type="radio" name="login_mode" id="returning" class="guest_btn" value="returning" onClick="changeusertype(2);" /><p>Returning customer</p></li>
                    </ul>
                </div>
                <div class="check_out_form">
					<ul id="ul_returninguser" style="display:none;">
						<li>
							<input type="email" placeholder="Email address" class="email_pos" name="returning_email" id="returning_email" value="<?php if(isset($_COOKIE['ondi_login_email'])) echo $_COOKIE['ondi_login_email']; ?>" />
						</li>
						<li>
							<input type="password" placeholder="Password" class="email_pos" name="returning_password" id="returning_password" value="<?php if(isset($_COOKIE['ondi_login_password'])) echo $_COOKIE['ondi_login_password']; ?>" />
						</li>
						<li>
							<div class="social">
								<ul>
									<li><p>or</p></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/fb_checkout.png" alt="" /></a></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/twitter_checkout.png" alt="" /></a></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/g+_checkout.png" alt="" /></a></li>
								</ul>
							 </div>
							 <div class="forgot_text_checkout"><a href="<?php echo $this->config->item('base_url'); ?>signup/forgotpassword">Forgot password?</a></div>
						</li>
						<li>
							<div class="continue">
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/continue_btn.png" alt="" class="fl">
								<div class="remember_login">
									<input type="checkbox" name="rememberme" id="rememberme" value="1" class="cleck_remember" checked="checked" ><p>Remember my login</p>
								</div>
							</div>
						</li>
					</ul>
					<ul id="ul_guestuser" style="display:block;">
						<li>
							<input type="email" placeholder="Email address" class="email_pos" name="guest_email" id="guest_email" />
						</li>
						<li>
							<div class="social">
								<ul>
									<li><p>or</p></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/fb_checkout.png" alt="" /></a></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/twitter_checkout.png" alt="" /></a></li>
									<li><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/g+_checkout.png" alt="" /></a></li>
							</ul>
							</div>
							
						</li>
						<li>
							<div class="continue">
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/continue_btn.png" alt="" >
							</div>
						</li>
					</ul>
				</div>	
        </div><!--End of checkout_guest_detail-->
		<?php echo form_close()?>

         <div class="payment_mode">
         	<div class="full_security">
            	<p>Full security</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/full_security.jpg" alt="">
            </div>
            <div class="payment_method">
            	<p>Payment Methods</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/payment_metnod.jpg" alt="">
            </div>
            <div class="authentic"><img src="<?php echo $this->config->item('base_url'); ?>public/images/authentic.jpg" alt=""></div>
         </div>
    </div><!--End of inner_content_bg-->
</div><!--End of inner_content_bg-->
<!--End of Booking Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>

<script type="text/javascript">
		$(function(){
				
			$('.payment_option_content .payment_option_detail .option ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
			
			$('.checkout_guest_content .checkout_guest_detail .option ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
			
			$('.signup_form_right input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/news_check.jpg")',
				width: 28,
				height: 25
			});
			$('.checked_comment input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/check2.jpg")',
				width: 17,
				height: 17
			});
			
			$('.remember_login input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/check2.jpg")',
				width: 17,
				height: 17
			});	
		
		});
</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


</body>
</html>
