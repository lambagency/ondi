<?php
//print_r($dataorders);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/slider/style1.css" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/payment_easydropdown.css"/>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

<!-- <script>$(window).load(function() {
		//alert(1)
		$('#loading-image').hide();
	});
</script> -->
</head>
<body>
<!-- <div id="loading-image" style="display:block;">
  <div class="pleasewait"> 
	<div class="border">
	One moment please while we process your payment. <br/>
	Please do not push 'cancel' or 'reload' during the process. <br/>
	This may take up to 1 minute

		<div class="loader"><img src="<?php echo $this->config->item('base_url'); ?>public/images/ajax-loader.gif"  alt="Loading..." /></div>
		
	</div>
  </div>
</div> -->
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<div class="inner_content_bg">
  <div class="payment_option_content">
    <div class="heading">
      <h1>PAYMENT OPTIONS</h1>
    </div>
    <div class="sub_heading">
      <h3>Checkout</h3>
    </div>

	<?php echo form_open_multipart(base_url()."booking/paymentoptions/", array('onSubmit'=> "return validatepaymentoptions()", 'name' => 'paymentoptionsform')); ?>
	<input type="hidden" name="submit" value="1" />
	<input type="hidden" name="order_id" value="<?=$dataorders[0]->order_id?>" />
	<input type="hidden" name="number_of_people" id="number_of_people" value="<?=$dataorders[0]->number_of_people?>" />
	<input type="hidden" name="offerprice" id="offerprice" value="<?=$dataorders[0]->offerprice?>" />
    <div class="payment_option_detail">
      <div class="progress_bar_bg">
        <div class="progress_bar">
          <ul>
            <li class="pink"></li>
            <li class="blue"></li>
            <li class="light_gray"></li>
          </ul>
          <div class="green_pointer"><img src="<?php echo $this->config->item('base_url'); ?>public/images/check_green.png" alt=""></div>
        </div>
        <div class="step_number">
          <p>2</p>
        </div>
      </div>



        <?php if($error_messages) : ?>
        <div class="singleAlert">
            <ul>
            <?php foreach($error_messages as $message) : ?>
                <li><?php echo $message['message']; ?></li>
            <?php endforeach; ?>
            </ul>
        </div>
        <?php endif; ?>



        <div class="member_name">
        <?php if(isset($usersessiondata['contact_name']) &&  trim($usersessiondata['contact_name']) != ''){?>
		<p>Hi <?=$usersessiondata['contact_name']?>,</p>
		<?php } else if(isset($usersessiondata['first_name']) &&  $usersessiondata['first_name'] != '' && isset($usersessiondata['last_name']) &&  $usersessiondata['last_name'] != ''){?>
		<p>Hi <?=$usersessiondata['first_name']?> <?=$usersessiondata['last_name']?>,</p>
		<?php } ?>
      </div>
      <div class="option">
        <ul>
          <li id="li_cc" class="active">
            <input type="radio" name="payment_method" id="cc" class="guest_btn" value="1" checked="checked" onclick="changepaymentmethod(1);"/>
            <p>Credit Card</p>
            <img src="<?php echo $this->config->item('base_url'); ?>public/images/master_card.jpg" alt="" /> <img src="<?php echo $this->config->item('base_url'); ?>public/images/visa.jpg" alt="" /> <img src="<?php echo $this->config->item('base_url'); ?>public/images/american_exp.jpg" alt="" /> </li>
          <!-- <li id="li_pp" >
            <input type="radio" name="payment_method" id="paypal" value="2" class="guest_btn" onclick="changepaymentmethod(2);" />
            <p>Paypal</p>
            <img src="<?php echo $this->config->item('base_url'); ?>public/images/paypal.jpg" alt="" /> </li> -->
          <!-- <li id="li_mp" class="last">
            <input type="radio" name="payment_method" id="masterpass" value="3" class="guest_btn" onclick="changepaymentmethod(3);" />
            <p>Masterpass</p>
            <img src="<?php echo $this->config->item('base_url'); ?>public/images/master_pass.jpg" alt="" /> </li> -->
        </ul>
      </div>

	  <div class="existing">
		<?php //if((bool)$this->config->item('test_mode')){?>
		<?php if(sizeof($data_savedcc)>0){?>
			<input type="checkbox" name="user_saved_cc" id="user_saved_cc" value="1" onclick="use_existing_cc();" /> Use existing saved credit cards
		<?php } ?>
		<?php //} ?>
	  </div>
      <div class="payment_option_form">
        <ul id="ul_new_cc">
          <li>
            <div class="left_field">
              <input type="text" placeholder="Card number" class="position_payment" name="cc_num" id="cc_num" value="" />
            </div>
            <div class="right_field">
              <input type="text" placeholder="Name on card" class="position_payment" name="cc_owner" id="cc_owner" value=""  />
            </div>
          </li>
          <li>
            <div class="left_field">
              <p>Expiration date</p>
              <span class="exp_date">
              <select tabindex="4" class="revostyled" name="exp_month" id="exp_month" >
                <option value="" class="label">Month</option>
				<option value="01">January</option>
                <option value="02">February</option>
                <option value="03">March</option>
                <option value="04">April</option>
                <option value="05">May</option>
                <option value="06">June</option>
                <option value="07">July</option>
                <option value="08">August</option>
                <option value="09">September</option>
                <option value="10">October</option>
                <option value="11">November</option>
                <option value="12">December</option>
              </select>
              </span>
              <span class="exp_date">
              	<select tabindex="4" class="revostyled" name="exp_year" id="exp_year">
                    <option value="" class="label" >Year</option>
                    <?php for($i=date("Y"); $i<=date("Y")+15; $i++){?>
					<option value="<?=$i?>" ><?=$i?></option>
					<?php } ?>
                  </select>
              </span> 
              </div>
            <div class="right_field">
              <input type="text" placeholder="Security code / CVV" class="position_security" name="cvv" id="cvv" value="" />
              <p><em>what is this?</em>
              	<span class="ccv">
                	<img src="<?php echo $this->config->item('base_url'); ?>public/images/ccv.png" alt="" />
                </span>
              </p>
            </div>
          </li>

		  <li>
            <div class="left_field">
              <input type="text" placeholder="Address" class="position_payment" name="cc_address" id="cc_address" value="" />
            </div>
            <div class="right_field">
              <input type="text" placeholder="City" class="position_payment" name="cc_city" id="cc_city" value=""  />
            </div>
          </li>

		  <li>
            <div class="left_field">
              <input type="text" placeholder="Country" class="position_payment" name="cc_country" id="cc_country" value="" />
            </div>
          </li>


		  <?php if($this->CI_auth->check_logged()===TRUE){?>
          <li id="li_save_card" >
            <div class="checked_comment">
              <input type="checkbox" name="save_card" id="save_card" value="1" class="check_payment_opt">
              <h3>Save my card details for future purchases</h3>
              <img src="<?php echo $this->config->item('base_url'); ?>public/images/lock_img.png" alt="" /> </div>
          </li>
		  <li id="li_save_card" >
            <div class="payment_left">
				<p>Card Category</p>
				<span class="drop_down">
					<select tabindex="4" class="revostyled" name="cc_category" id="cc_category">
						<option value="" class="label">Select</option>
						<?php foreach($cccategoryquery as $thecccategory){?>
						<option value="<?=$thecccategory->id?>" <?php if($cccarddata[$printed_cards-1]->category==$thecccategory->cc_category) echo 'selected="selected"';?> ><?=$thecccategory->cc_category?></option>
						<?php } ?>
					</select>
				</span>
			</div>
          </li>
		  <?php } ?>
	</ul>

	 <ul id="ul_saved_cc" style="display:none;">
		<li>
            <div class="left_field" id="big">
				<select name="saved_cc" id="saved_cc" class="revostyled">
					<option value="">Select Card</option>
					<?php foreach($data_savedcc as $cc){?>
					<option value="<?=$cc->id?>"><?=$cc->scheme?> <?=$cc->display_number?> Expiry  <?=(strlen($cc->expiry_month)<2?"0".$cc->expiry_month:$cc->expiry_month)?>/<?=substr($cc->expiry_year, 2, strlen($cc->expiry_year))?></option>
					<?php } ?>
				</select>
            </div>
          </li>
	 </ul>
	
	 <ul>
		 <?php if($dataorders[0]->discount=='0.00'){?>
          <li>
            <div class="got_promotion_code">
				<input type="button" value="code" class="promotion_code_btn" />
				<input type="text" placeholder="We'll see :) type it in here..." class="promotion_code" name="promotion_code" id="promotion_code" value="<?php echo set_value('promotion_code'); ?>"   />
				<a href="javascript:void(0);" onclick="calculatediscount_paymentoptions();"><img src="<?php echo $this->config->item('base_url'); ?>public/images/redeem.jpg" alt=""></a>
            </div>
          </li>
		  <li id="li_calc_discount"></li>
		  <?php } ?>
		 
          <li>
            <div class="checked_comment">
              <input type="checkbox" name="subscribe_newsletter" id="subscribe_newsletter" value="1" class="check_payment_opt" checked="checked" />
              <h3>Want the latest offers? <span>Subscribe me now!</span></h3>
            </div>
          </li>
          <li>
            <div class="continue_payment"><input type="image" class="img" src="<?php echo $this->config->item('base_url'); ?>public/images/continue_blue.jpg" alt="" />
              <p>By pressing continue you accept our terms & conditions</p>
            </div>
          </li>
        </ul>
      </div>
      <!--End of payment_option_form--> 
    </div>
	<?php echo form_close()?>


    <!--End of checkout_guest_detail-->
    <div class="payment_mode">
      <div class="full_security">
        <p>Full security</p>
        <img src="<?php echo $this->config->item('base_url'); ?>public/images/full_security.jpg" alt=""> </div>
      <div class="payment_method">
        <p>Payment Methods</p>
        <img src="<?php echo $this->config->item('base_url'); ?>public/images/payment_metnod.jpg" alt=""> </div>
      <div class="authentic"><img src="<?php echo $this->config->item('base_url'); ?>public/images/authentic.jpg" alt=""></div>
    </div>
  </div>
  <!--End of inner_content_bg--> 
</div>
<!--End of inner_content_bg--> 
<!--End of Payment Option Page_Content--> 

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->

<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->






<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();		
	});
</script>


<script type="text/javascript">
		$(function(){
				
			$('.payment_option_content .payment_option_detail .option ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
			
			$('.checkout_guest_content .checkout_guest_detail .option ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
			
			$('.signup_form_right input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/news_check.jpg")',
				width: 28,
				height: 25
			});
			$('.checked_comment input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/check2.jpg")',
				width: 17,
				height: 17
			});
			
			$('.remember_login input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/check2.jpg")',
				width: 17,
				height: 17
			});	

			$('.existing input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/check2.jpg")',
				width: 17,
				height: 17
			});	

				
		
		});
</script>

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>
<script type="text/javascript">
	$('input[type=text], input[type=password], textarea').placeholder();	
</script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 




</body>
</html>
