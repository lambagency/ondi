<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<?php
}
?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
</head>
<body>


<div id="loading-image" style="display:none;">
  <div class="pleasewait"> 
	<div class="border">
	One moment please while we process your payment. <br/>
	Please do not push 'cancel' or 'reload' during the process. <br/>
	This may take up to 1 minute
		<div class="loader"><img src="<?php echo $this->config->item('base_url'); ?>public/images/ajax-loader.gif"  alt="Loading..." /></div>
	</div>
  </div>
</div>


<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<div class="inner_content_bg">
	<div class="review_content">
    	<div class="heading">
        	<h1>REVIEW</h1>
        </div>
        <div class="sub_heading">
        	<h3>Checkout</h3>
        </div>

		<?php //echo form_open_multipart(base_url()."booking/review/", array('name' => 'paymentoptionsform')); ?>
		<?php echo form_open_multipart(base_url()."booking/review/", array('onSubmit'=> "return validatereviewform()", 'name' => 'paymentoptionsform')); ?>

		<input type="hidden" name="submit" value="1" />
		<input type="hidden" name="order_id" value="<?=$dataorders[0]->order_id?>" />
        <div class="review_detail">
        	<div class="progress_bar_bg">
            	<div class="progress_bar">
            		<ul>
                    	<li class="pink"></li>
                        <li class="blue"></li>
                        <li class="orenge"></li>
                    </ul>
                    <div class="green_pointer"><img src="<?php echo $this->config->item('base_url'); ?>public/images/check_green.png" alt=""></div>
          	    </div>
                <div class="step_number">
            		<p>3</p>
 		        </div>
            </div>
            
            <div class="member_name"><p>Ok, so we're locking you in for the following:</p></div>
            
            <div class="review_order">
            	<div class="review_order_head"><?=$dataorders[0]->offer_title?> <span>for</span> $<?=$dataorders[0]->price?></div>
                <div class="review_order_tagline"><?=$dataorders[0]->offer_description?></div>
                <div class="review_order_main">
                	<ul>
                    	<li>
                        	<div class="left_main">at</div>
                            <div class="right_main"><?=$dataorders[0]->business_name?></div>
                        </li>
                        <li>
                        	<div class="left_main">for</div>
                            <div class="right_main"><?=$dataorders[0]->number_of_people?> people</div>
                        </li>
                        <li>
                        	<div class="left_main">on</div>
                            <div class="right_main"><?=date("l d/m/Y", strtotime($dataorders[0]->booking_date))?></div>
                        </li>
                        <li>
                        	<div class="left_main">time</div>
                            <div class="right_main"><?=date("h:i A", strtotime($dataorders[0]->booking_time))?></div>
                        </li>
                        <li>
                        	<div class="left_main">totalling</div>
                            <div class="right_main">$<?=$dataorders[0]->total_price?></div>
                        </li>
                    </ul>
                </div>
            </div>
            
            <div class="order_confirm">
            	<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/order_confirm.jpg" alt="" />
            </div>
            
            	
                
                
        </div><!--End of review_detail-->
		<?php echo form_close()?>

         <!-- <div class="payment_mode">
         	<div class="full_security">
            	<p>Full security</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/full_security.jpg" alt="">
            </div>
            <div class="payment_method">
            	<p>Payment Methods</p>
                <img src="<?php echo $this->config->item('base_url'); ?>public/images/payment_metnod.jpg" alt="">
            </div>
            <div class="authentic"><img src="<?php echo $this->config->item('base_url'); ?>public/images/authentic.jpg" alt=""></div>
         </div> -->
    </div><!--End of inner_content_bg-->
</div><!--End of inner_content_bg-->
<!--End of Payment Option Page_Content-->

<!--Start of list_you_business Section-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<!--End of list_you_business Section-->
<?php $this->load->view('templates/footer');?>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


</body>
</html>
