<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?> | ONDI FOOD</title>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<script>
$(document).ready(function() {
	/*========FOOTER BOTTOM FIXED=========*/
	var screen_inner_height2=$(document).height();
	//alert(screen_inner_height2)
	$(".merchant_main").css('min-height',screen_inner_height2-128+'px' );
});

function delete_perfect_for(id)
{
	var where_to = confirm("Do you really want to delete this record?");
	if (where_to == true)
	{		
		link= "<?php echo $this->config->item('base_url'); ?>admin/deleteperfectfor/?id="+id;
		document.location.href=link;
	}
	else
	{	
		return false;	
	}
}
function filterbytype(v)
{
	window.location.href="<?php echo $this->config->item('base_url'); ?>admin/viewperfectfor/?business_type="+v;
}
</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<form name="admin_dashboard_form" id="admin_dashboard_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/dashboard" onsubmit="return submit_admin_dashboard_form()">
				<input type="hidden" name="admin_dashboard_action" id="admin_dashboard_action" value="dashboard">	
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1>Perfect For</h1>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>
					
					<div class="service">
					<ul>
						<li>
							<div class="custom_select_ajax2">
								<select name="business_type" class="revostyled" id="business_type" onchange="filterbytype(this.value);" >
									<option value="">Business Type</option>
									<?php foreach($business_types as $the_type){?>	
										<option value="<?=$the_type->type_id?>" <?php if($business_type_id ==$the_type->type_id){ echo 'selected="selected"';} ?> ><?=$the_type->type_name?></option>
									<?php } ?>
								</select>
							</div>
						</li>
					</ul>
				</div>

					<?php echo $this->session->flashdata('content_page_update_message'); ?>
					<div class="admin_detail">
						<table width="595" border="1" class="table_customer"> 
							<tr class="table_head">
								<th align="left" scope="col" width="130" height="25">Title</th>
								<th align="left" scope="col" width="50" class="no_bg">&nbsp;</th>
								<th align="center" scope="col" width="100" class="no_bg">Action</th>
							</tr>
							<?php
							if(count($records)>0)
							{ 
								foreach($records as $content_val)
									{?>
									<tr>
										<td align="left" scope="col" width="130" height="30">
											<?php echo stripslashes($content_val->pf_name); ?>
										</td>
										<td align="left" scope="col" width="50" class="permission">&nbsp;</td>
										<td align="left" scope="col" width="100">											
											
											<div class="delete_merchant">
												<a href="javascript:void(0)" onclick="delete_perfect_for('<?php echo $content_val->pf_id; ?>')" ><img src="<?php echo base_url() ?>public/images/delete_faq.png" alt="" />Delete</a>
											</div>
											
											<!-- style="width:175px;" -->
											<div class="edit_merchant" >
												<a href="<?php echo $this->config->item('base_url'); ?>admin/editperfectfor/?id=<?php echo $content_val->pf_id; ?>"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Edit</a>
											</div>


										</td>
									</tr>
								<?php 
									} 
							}else{
						?>
							
							<tr>
								<td colspan="3">No records found</td>
							</tr>

						<?php
							}							
							?>
						</table>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--Start of customer Area-->
	<?php $this->load->view('templates/admin_footer');?>	
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$(".revostyled").selectbox();
		});		
		
	</script>

</body>
</html>