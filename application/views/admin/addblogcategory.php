<?php
/*
echo '<pre>';
print_r($admin_data);
echo '</pre>';
*/
if(isset($faq_cat_detail[0]->cat_id) && $faq_cat_detail[0]->cat_id!=""){ $page_title = ' Update '; }else{ $page_title = ' Add ';  }
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_form()
		{
			var category = document.getElementById("category").value;	
			if(checkBlankField(category) == false)
			{
				alert("Please enter category.");
				document.getElementById("category").focus();
				return false;
			}
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
				<form name="admin_faq_form" id="admin_faq_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/addblogcategory" onsubmit="return submit_admin_form()">
					<input type="hidden" name="admin_action" id="admin_action" value="1">
					<div class="merchant_main_right">	  
						<div class="heading">
							<h1>Blog Category</h1>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>
						<div class="merchant_contact">
							<ul>							
								<li>
									<input type="text" name="category" id="category" class="input_box" placeholder="Blog Category*" value="" />
								</li>
							</ul>
						</div>
						<div class="save_bg"> 
							<span class="save">				
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
							</span>  
						</div>
					</div>
				</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>