<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?> | ONDI FOOD</title>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<script>
$(document).ready(function() {
	/*========FOOTER BOTTOM FIXED=========*/
	var screen_inner_height2=$(document).height();
	//alert(screen_inner_height2)
	$(".merchant_main").css('min-height',screen_inner_height2-138+'px' );
});
function askapproveoffer(offerid, nds)
{
	var conf = confirm("Do you want to change the status of this offer?");
	if(conf)
	{
		window.location='<?php echo $this->config->item('base_url'); ?>admin/approveoffers/?offerid='+offerid+'&nds='+nds;
	}
}
</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1>Offers</h1>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>	
					<?php echo $this->session->flashdata('content_page_update_message'); ?>
					<div class="admin_detail">
						<table width="595" border="1" class="table_customer"> 
							<tr>
								<td align="center" colspan="4">
                                	<form name="offer_search_form" id="offer_search_form" method="get" action="<?php echo $this->config->item('base_url'); ?>admin/approveoffers/" >
										<div class="merchant_search_form">
											<h2>Search</h2>
											<div class="search_input" style="margin:0px; width:38%;">
												<input type="text" name="offer_search" id="offer_search" value="<?php if(isset($offer_search)){ echo $offer_search; } ?>" placeholder="Offer Title, Business Name">
											</div>
											<div class="search_button" style="margin:0px 10px;">
												<input type="submit" name="submit" value="submit" class="merchant_search_submit" >
											</div>
										</div>
									</form>
                                </td>
							</tr>

							<tr class="table_head">
								<th align="left" scope="col" width="130" height="25">Offer</th>
								<th align="left" scope="col" width="130" height="25">Price</th>
								<th align="left" scope="col" width="130" height="25">Merchant</th>
								<th align="center" scope="col" width="100" class="no_bg">Action</th>
							</tr>
							<?php
							if(count($offers)>0)
							{ 
								foreach($offers as $content_val)
								{?>
									<tr>
										<td align="left" scope="col" width="130" height="30"><?php echo stripslashes($content_val->offer_title); ?></td>
										<td align="left" scope="col" width="130" height="30">$<?php echo stripslashes($content_val->price); ?></td>
										<td align="left" scope="col" width="130" height="30"><?php echo stripslashes($content_val->business_name); ?></td>
										<td align="center" scope="col" width="100">
											<div class="edit_merchant" style="width:175px;">
											<?php if($content_val->display_status=='4' || $content_val->display_status=='0'){?>
												<a href="javascript:void(0);" onclick="askapproveoffer(<?=$content_val->offerid?>, 1)" style="color:#9BC83C;">Approve</a>
											<?php } ?>
											</div>
											<div class="edit_merchant" style="width:175px;">
											<?php if($content_val->display_status!='0'){?>
												<a href="javascript:void(0);" onclick="askapproveoffer(<?=$content_val->offerid?>, 0)" style="color:#FF0000;">Hide</a>
											<?php } ?>
											</div>
										</td>
									</tr>
								<?php 
								} 
							} 
							else 
							{ ?>
								<tr>
									<td align="center" scope="col" colspan="4">No Offers to approve!</td>
								</tr>
							<?php 
							} ?>
						</table>
					</div>
				
			</div>
		</div>
	</div>
	<!--Start of customer Area-->
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>