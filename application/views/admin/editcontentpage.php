<?php
/*
echo '<pre>';
print_r($content_detail);
echo '</pre>';
*/
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_content_form()
		{
			/*
			var page_title = document.getElementById("page_title").value;	
			if(checkBlankField(page_title) == false)
			{
				alert("Please enter page title.");
				document.getElementById("page_title").focus();
				return false;
			}		
			*/
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
		<?php $this->load->view('templates/admin_main_left');?>
		<form name="admin_content_form" id="admin_content_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/editcontentpage" onsubmit="return submit_admin_content_form()">
			<input type="hidden" name="admin_content_action" id="admin_content_action" value="edit">
			<input type="hidden" name="page_id" id="page_id" value="<?php echo stripslashes($content_detail[0]->page_id); ?>">
			<div class="merchant_main_right">	  
				<div class="heading">
					<h1>Edit Page</h1>
				</div>
				<div class="valid_errors">
					<?php 
					echo validation_errors();
					echo $this->session->flashdata('admin_content_upd_err_msg');
					?>
				</div>
				<div class="sub_heading">
					<h3>Fields marked (*) are mandatory.</h3>
				</div>
				<div class="merchant_contact admin">
					<ul>							
						<li>
							<input type="text" name="page_title" id="page_title" class="input_box" placeholder="Page Title*" value="<?php echo stripslashes($content_detail[0]->page_title); ?>" />
						</li>
						<li>
							<textarea name="page_desc" id="page_desc" class="input_box" placeholder="Page Description" style="width:590px; height:200px; "><?php echo stripslashes($content_detail[0]->page_desc); ?></textarea>
						</li>
						<li>
							<input type="text" name="meta_title" id="meta_title" class="input_box" placeholder="Meta Title" value="<?php echo stripslashes($content_detail[0]->meta_title); ?>" />
						</li>
						<li>
							<input type="text" name="meta_desc" id="meta_desc" class="input_box" placeholder="Meta Description" value="<?php echo stripslashes($content_detail[0]->meta_desc); ?>" />
						</li>
						<li>
							<input type="text" name="meta_keyword" id="meta_keyword" class="input_box" placeholder="Meta Keyword" value="<?php echo stripslashes($content_detail[0]->meta_keyword); ?>" />
						</li>
						<li>
							<div class="left">
								<input type="radio" name="page_status" id="page_status_show" value="1" <?php if($content_detail[0]->page_status=='1'){ echo 'checked'; } ?> /> Show 
							</div>
							<div class="left">
								<input type="radio" name="page_status" id="page_status_hide" value="0" <?php if($content_detail[0]->page_status=='0'){ echo 'checked'; } ?> /> Hide
							</div>
						</li>
					</ul>
				</div>
				<div class="save_bg"> 
					<span class="save">				
						<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/update_data.jpg" name="" >
					</span>  
				</div>
			</div>
		</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
		});
	</script>
</body>
</html>