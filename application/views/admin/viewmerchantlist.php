<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?> | ONDI FOOD</title>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<script>
$(document).ready(function() {
	/*========FOOTER BOTTOM FIXED=========*/
	var screen_inner_height2=$(document).height();
	//alert(screen_inner_height2)
	$(".merchant_main").css('min-height',screen_inner_height2-138+'px' );
});
function Action1(pid, nw)
{ 
	var conf = confirm("Do you want to change status?");
	if(conf)
	{ 
		document.location.href="<?php echo $this->config->item('base_url'); ?>admin/viewmerchantlist/?id="+pid+"&action3=st&nw="+nw;
	}
}
function Action2(pid, nw)
{ 
	var conf = confirm("Do you want to change offer approval setting?");
	if(conf)
	{ 
		document.location.href="<?php echo $this->config->item('base_url'); ?>admin/viewmerchantlist/?id="+pid+"&action4=st&nw="+nw;
	}
}
function openapprovepopup(idx)
{
	document.getElementById('dialog').style.display="block";
	document.approvefrm.user_id.value=idx;
}

function closeapprovepopup()
{
	document.approvefrm.user_id.value = "";
	document.getElementById('dialog').style.display="none";
}
function checkapprovefrm()
{
	if(document.approvefrm.merchant_commission.value=="")
	{
		alert("Please enter merchant commission");
		return false;
	}
}
</script>
</head>
<body>

<div class="popup_bg" id="dialog" title="Approve it" style="display:none;">
	<div class="popup_window">
		<div class="popup_bg_top">
			<a href="javascript:void(0)" onclick="closeapprovepopup();" ><img src="<?php echo base_url() ?>public/images/cancel-popup.png" /></a>
		</div>
		<div class="popup_bg_main" style="padding-top:0px;">
			<form name="approvefrm" method="get" action="<?php echo $this->config->item('base_url'); ?>admin/approvemerchantcommission/" onsubmit="return checkapprovefrm();" >
				<input type="hidden" name="user_id" id="user_id" value="" />
				Enter Commission <input type="text" name="merchant_commission" id="merchant_commission" value="" />
				<input type="submit" name="submit" value="Submit" />
			</form>
		</div>
	</div>
</div>

	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			
			<div>
					
			</div>
			
			
			
				
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1><?php if(isset($city_name_search) && $city_name_search != '') echo $city_name_search; ?> Merchants</h1>
						<a href="<?php echo $this->config->item('base_url'); ?>admin/viewmerchantlist/?action2=export" target="_blank" style="float:right;" ><b>Export Merchants</b></a>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>	
					<?php echo $this->session->flashdata('content_page_update_message'); ?>
					<div class="admin_detail">
						<table width="595" border="1" class="table_customer" cellpadding="10" cellspacing="10" >							
							
							<form name="merchant_search_form" id="merchant_search_form" method="get" action="<?php echo $this->config->item('base_url'); ?>admin/viewmerchantlist/" >
							<tr>
								<td align="center" colspan="8">
                                	<div class="merchant_search_form">
                                    	<h2>Search</h2>
                                        <div class="search_input" style="margin:0px; width:38%;"><input type="text" name="merchant_search" id="merchant_search" value="<?php if(isset($merchant_search)){ echo $merchant_search; } ?>" placeholder="Contact Name, Business Name"></div>
										
											<select name="city" class="filtercitydropdown">
												<option value="">Select City</option>
												<?php 
												$cities = $this->customclass->getcities();
												foreach($cities as $thecity){ ?>
												<option value="<?=$thecity->id?>" <?php if($city_search==$thecity->id){?>selected="selected" <?php } ?> ><?=$thecity->city_name?></option>
												<?php } ?>
											</select>
										
                                        <div class="search_button"><input type="submit" name="submit" value="submit" class="merchant_search_submit" ></div>
                                       
                                    </div>
                                </td>
							</tr>
                            
                            
							</form>
							
							
							<tr class="table_head">
								<td class="one" align="left" scope="col" height="25" width="50" >Merchant</td>
								<td class="four" align="left" scope="col" height="25" width="50" colspan="2">Commission</td>
								
								<td class="five" align="left" scope="col" height="25" width="50" >Date joined</td>
								<td class="six" align="left" scope="col" class="no_bg" width="50" >Status</td>
								<td class="seven" align="left" scope="col" class="no_bg" width="50" >Offer Approval</td>
								<td class="eight" align="left" scope="col" class="no_bg" width="50" >Login</td>
								<td class="nine" align="center" scope="col" class="no_bg" width="50" >Details</td>
							</tr>
							<?php
							if(count($merchants)>0)
							{ 
								foreach($merchants as $content_val)
									{?>
									<tr>
										<td align="left" scope="col" height="30">
										<?php echo stripslashes($content_val->business_name); ?><br />
										<?php echo stripslashes($content_val->contact_name); ?><br />
										<?php echo stripslashes($content_val->email); ?>
										</td>
										<td align="left" scope="col" height="30" colspan="2">
											<?php if($content_val->merchant_commission == '0.00'){?>
											<div class="edit_merchant">
												<a href="javascript:void(0);" onclick="openapprovepopup(<?php echo $content_val->usrid; ?>);">Configure</a>
											</div>
											<?php } else { echo $content_val->merchant_commission; } ?>
										</td>
										</td>
										<td align="left" scope="col" height="30"><?php if($content_val->added_date != '0000-00-00 00:00:00' && $content_val->added_date != ''){ echo date("d-m-Y", strtotime($content_val->added_date));} else { echo "N/A";}  ?></td>
										
										<td align="left" scope="col">
											<?php if($content_val->user_status==1) { ?>
												<a href="#" onClick="return Action1(<?=$content_val->usrid?>, 0)">
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/visible.gif"  border="0" >
												</a>
											<?php }else {?>
												<a href="#" onClick="return Action1(<?=$content_val->usrid?>, 1)">
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/invisible.gif"  border="0">
												</a>
											<?php } ?>
										</td>

										<td align="left" scope="col">
											<?php if($content_val->offer_approval==1 || $content_val->offer_approval==0) { ?>
												<a href="#" onClick="return Action2(<?=$content_val->usrid?>, 2)">
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/required.png"  border="0" >
												</a>
											<?php } ?>
											<?php if($content_val->offer_approval==2 || $content_val->offer_approval==0) { ?>
												<a href="#" onClick="return Action2(<?=$content_val->usrid?>, 1)">
													
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/not-required.png"  border="0" >
												</a>
											<?php } ?>
										</td>

										<td align="left" scope="col" class="permission">
											<div class="edit_merchant">
												<a target="_blank" href="<?php echo $this->config->item('base_url'); ?>admin/loginasuser/?user_id=<?php echo base64_encode($content_val->usrid); ?>"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Login</a>
											</div>
										</td>
										<td align="left" scope="col" >
											<div class="edit_merchant">
												<a href="<?php echo $this->config->item('base_url'); ?>admin/merchantcontactinfo/?user_id=<?php echo base64_encode($content_val->usrid); ?>&merchant_search=<?=$merchant_search?>&city=<?=$city_search?>"><img src="<?php echo base_url() ?>public/images/search_img.png" width="12" height="12" alt="" />Detail</a>
											</div>
										</td>
									</tr>
								<?php 
									} 
							}  else { ?>
							<tr>
								<td align="center" colspan="7">No records found</td>
							</tr>
							<?php } ?>
						</table>
					</div>
				
			</div>
		</div>
	</div>
	<!--Start of customer Area-->
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>