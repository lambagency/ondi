<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $title; ?> | ONDI FOOD</title>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<script>
$(document).ready(function() {
	/*========FOOTER BOTTOM FIXED=========*/
	var screen_inner_height2=$(document).height();
	//alert(screen_inner_height2)
	$(".merchant_main").css('min-height',screen_inner_height2-128+'px' );
});
function Action1(pid, nw)
{ 
	var conf = confirm("Do you want to change status?");
	if(conf)
	{ 
		document.location.href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?id="+pid+"&action3=st&nw="+nw;
	}
}
</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			
					
			
				
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1>Customers</h1>

						<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?action2=export" target="_blank" style="float:right;" ><b>Export Customers</b></a>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>	
					<?php echo $this->session->flashdata('content_page_update_message'); ?>
					<div class="admin_detail">
						<table width="595" border="1" class="table_customer"> 
							
							<form name="customer_search_form" id="customer_search_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/" >
                            <td colspan="10">
                            	<div class="merchant_search_form">
                                    	<h2>Search</h2>
                                        <div class="search_input"><input type="text" name="customer_search" id="customer_search" value="<?php if(isset($customer_search)){ echo $customer_search; } ?>"></div>
                                        <div class="search_button"><input type="submit" name="submit" value="submit" class="merchant_search_submit" ></div>
                               			<p>First Name or Last Name</p>
                                 </div>
                            </td>
                            	
                                        
                           
							
							</form>
							
							
							<tr class="table_head">
								<th align="left" scope="col" height="25">
									User ID<br>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=user_id&orderset=asc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/asc.gif">
									</a>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=user_id&orderset=desc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/desc.gif">
									</a>
								</th>
								<th align="left" scope="col" height="25">First Name<br>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=first_name&orderset=asc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/asc.gif">
									</a>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=first_name&orderset=desc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/desc.gif">
									</a>
								</th>
								<th align="left" scope="col" class="no_bg">Last Name<br>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=last_name&orderset=asc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/asc.gif">
									</a>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=last_name&orderset=desc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/desc.gif">
									</a>
								</th>
								<th align="left" scope="col" class="no_bg">Email<br>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=email&orderset=asc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/asc.gif">
									</a>
									<a href="<?php echo $this->config->item('base_url'); ?>admin/viewcustomerlist/?cols=email&orderset=desc&customer_search=<?=$customer_search?>">
										<img src="<?php echo $this->config->item('base_url'); ?>public/images/desc.gif">
									</a>
								</th>
								<th align="left" scope="col" class="no_bg">Things I love</th>
								<th align="left" scope="col" class="no_bg">Date Joined</th>
								<th align="left" scope="col" class="no_bg">Referral Key </th>
								<th align="left" scope="col" class="no_bg">Status</th>
								<th align="left" scope="col" class="no_bg">enewsletter sign up</th>
								<th align="center" scope="col" class="no_bg">Action</th>
							</tr>
							<?php
							if(count($customers)>0)
							{ 
								foreach($customers as $content_val)
								{
									$customerinterest = $this->Admin_Model->get_things_i_love($content_val->interests);
									?>
									<tr>
										<td align="left" scope="col" height="30"><?php echo stripslashes($content_val->user_id); ?></td>
										<td align="left" scope="col" height="30"><?php echo stripslashes($content_val->first_name); ?></td>
										<td align="left" scope="col" height="30"><?php echo stripslashes($content_val->last_name); ?></td>
										<td align="left" scope="col" height="30"><?php echo stripslashes($content_val->email); ?></td>
										<td align="left" scope="col" height="30"><?php echo stripslashes($customerinterest); ?></td>

										<td align="left" scope="col" class="permission"><?php if($content_val->added_date != '0000-00-00 00:00:00' && $content_val->added_date != ''){ echo date("d-m-Y", strtotime($content_val->added_date));} else { echo "N/A";}  ?></td>

										<td align="left" scope="col" class="permission"><?php if($content_val->referral_key != ''){ echo $content_val->referral_key;}   ?></td>
										
										<td align="left" scope="col">
											<?php if($content_val->user_status==1) { ?>
												<a href="#" onClick="return Action1(<?=$content_val->user_id?>, 0)">
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/visible.gif"  border="0" >
												</a>
											<?php }else {?>
												<a href="#" onClick="return Action1(<?=$content_val->user_id?>, 1)">
													<img src="<?php echo $this->config->item('base_url'); ?>public/images/invisible.gif"  border="0">
												</a>
											<?php } ?>
										</td>

										<td align="left" scope="col" height="30"><?php if($content_val->subscribe_newsletter == "1") {echo "Yes";} else  {echo "No";} ?></td>
										<td align="left" scope="col" >
											

											<div class="edit_merchant">
												<a href="<?php echo $this->config->item('base_url'); ?>admin/customercontactinfo/?user_id=<?php echo base64_encode($content_val->user_id); ?>"><img src="<?php echo base_url() ?>public/images/search_img.png" width="12" height="12" alt="" />Detail</a>
											</div>
											
											
											<div class="edit_merchant">
												<a target="_blank" href="<?php echo $this->config->item('base_url'); ?>admin/loginasuser/?user_id=<?php echo base64_encode($content_val->user_id); ?>"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Login</a>
											</div>
										</td>
									</tr>
								<?php 
									} 
							} else { ?>
							<tr>
								<td align="center" colspan="8">No records found</td>
							</tr>
							<?php } ?>
						</table>
					</div>
				
			</div>
		</div>
	</div>
	<!--Start of customer Area-->
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>