<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_form()
		{			
			var interest = document.getElementById("position_value").value;			
			if(checkBlankField(interest) == false)
			{
				alert("Please enter position title.");
				document.getElementById("position_value").focus();
				return false;
			}
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
		});
	function listyourbusiness_validate1()
	{
		if(checkBlankField(document.getElementById('business_name').value) == false)
		{
			alert("Please enter your business name!");
			document.getElementById('business_name').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('business_website_address').value) == false)
		{
			alert("Please enter your website address!");
			document.getElementById('business_website_address').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('business_street_address').value) == false)
		{
			alert("Please enter your street address!");
			document.getElementById('business_street_address').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('business_contact_name').value) == false)
		{
			alert("Please enter your contact name!");
			document.getElementById('business_contact_name').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('business_email_address').value) == false)
		{
			alert("Please enter your email address!");
			document.getElementById('business_email_address').focus();
			return false;
		}
		else if(valid_email(document.getElementById('business_email_address').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('business_email_address').focus();
			return false
		}
		if(checkBlankField(document.getElementById('business_password').value) == false)
		{
			alert("Please enter your password!");
			document.getElementById('business_password').focus();
			return false;
		}
	}
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<div class="merchant_main_right">	  
				<div class="merchant_contact">
					<?php echo form_open_multipart(base_url()."admin/addmerchant/", 'onsubmit="return listyourbusiness_validate1();"')?>
					<input type="hidden" name="submit" value="1" />
					<ul>
						<li>
							<h3 style="font-size:20px;">
								<font color="red">
									<?php 
									echo validation_errors(); 
									//echo $this->session->flashdata('signup_message');
									echo $signup_message;
									if(isset($_GET['act']) && $_GET['act']=='suc') echo "Thanks! We will contact you soon.";
									?>
								</font>
							</h3>
						</li>
						<li><input type="text" placeholder="*Business Name" class="input_box" id="business_name" name="business_name" value="<?=set_value('business_name')?>" /></li>
						<li><input type="text" placeholder="*Website Address" class="input_box" id="business_website_address" name="business_website_address" value="<?=set_value('business_website_address')?>"  /></li>
						<li><input type="text" placeholder="*Street Address" class="input_box" id="business_street_address" name="business_street_address" value="<?=set_value('business_street_address')?>"/></li>
						<li>
						Category<br/>
							<ul id="checkboxesgroup">
								<?php foreach($business_types as $the_type){?>	
								<li><input class="class_business_type" type="checkbox" value="<?=$the_type->type_id?>" name="business_type[]" <?php if(in_array($the_type->type_id, $array_selected_business_types)){ echo "checked='checked'"; } ?> onclick="change_lyb_categories('<?=$the_type->type_name?>', this.checked);" ><?=$the_type->type_name?></li>
								<?php } ?>
							</ul>
						<input type="hidden" id="choosen_business_types" name="choosen_business_types" value="<?=$userdata[0]->business_type?>" />	
						</li>
						<li><input type="text" placeholder="*Contact Name" class="input_box" id="business_contact_name" name="business_contact_name" value="<?=set_value('business_contact_name')?>" /></li>
						<li><input type="text" placeholder="*Email Address" class="input_box" id="business_email_address" name="business_email_address" value="<?=set_value('business_email_address')?>" /></li>
						<li><input type="password" placeholder="*Password" class="input_box" id="business_password" name="business_password" value="" /></li> 
					</ul>
					<div class="save_bg"> 
						<span class="save">				
						<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
						</span>  
						</div>
					<?php echo form_close()?>
				</div>
				
			</div>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>