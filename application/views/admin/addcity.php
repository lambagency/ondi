<?php
/*
echo '<pre>';
print_r($admin_data);
echo '</pre>';
*/
$bg_image = "";
if(isset($city_fetch_data[0]->id) && $city_fetch_data[0]->id!="")
{ 
	$page_title = ' Update '; 
	$id = $city_fetch_data[0]->id;
}
else
{ 
	$page_title = ' Add '; 
	$id = "";
}
if($city_fetch_data[0]->bg_image != '') $bg_image = $city_fetch_data[0]->bg_image;
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_form()
		{			
			
			var city_name = document.getElementById("city_name").value;	
			var display_order = document.getElementById("display_order").value;
			if(checkBlankField(city_name) == false)
			{
				alert("Please enter city name.");
				document.getElementById("city_name").focus();
				return false;
			}
			
			if(checkBlankField(display_order) == true)
			{
				if(IsNumeric(display_order) == false)
				{
					alert ( "Please enter only numeric value." );
					document.getElementById('display_order').focus();
					return false;
				}
			}		
			
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
				<form name="admin_faq_form" id="admin_faq_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/addcity" onsubmit="return submit_admin_form()" enctype="multipart/form-data">
					<input type="hidden" name="admin_action" id="admin_action" value="1">
					<input type="hidden" name="id" id="id" value="<?php echo $id; ?>">
					
					<input type="hidden" name="bg_image_old" id="bg_image_old" value="<?php echo $bg_image; ?>">
					
					<div class="merchant_main_right">	  
						<div class="heading">
							<h1><?=$page_title?> City</h1>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>					
						

						<div class="merchant_contact">
							<ul>

								<li>
									<?php 
										echo validation_errors(); 
										echo $this->session->flashdata('admin_message'); 
									?>
								</li>
							
								<li>
									<input type="text" name="city_name" id="city_name" class="input_box" placeholder="City Name*" value="<?php if(isset($city_fetch_data[0]->id)){ echo $city_fetch_data[0]->city_name; } ?>" />							
									
								</li>

								<li>
									<input type="text" name="display_order" id="display_order" class="input_box" placeholder="Display Order*" value="<?php if(isset($city_fetch_data[0]->display_order)){ echo $city_fetch_data[0]->display_order; } ?>" />							
									
								</li>

								<li>
									<input type="file" name="bg_image" id="bg_image" >						
								</li>
								<?php if($bg_image!=""){ ?>
								<li>
									<img src="<?php echo $this->config->item('base_url'); ?><?=$bg_image?>" width="200">
								</li>
								<?php } ?>


								<li>
									<input type="checkbox" name="default_city" id="default_city" value="" <?php if(isset($city_fetch_data[0]->default_city) &&$city_fetch_data[0]->default_city=='1'){ echo "checked"; } ?> > Default City
								</li>

							</ul>
						</div>
						<div class="save_bg"> 
							<span class="save">				
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
							</span>  
						</div>
					</div>
				</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>