<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
	function submit_admin_form()
	{
		if(checkBlankField(document.getElementById('post_title').value) == false)
		{
			alert("Please enter post title.");
			document.getElementById('post_title').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('short_desc').value) == false)
		{
			alert("Please enter short description.");
			document.getElementById('short_desc').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('post_desc').value) == false)
		{
			alert("Please enter post description.");
			document.getElementById('post_desc').focus();
			return false;
		}
		if(checkBlankField(document.getElementById('post_category').value) == false)
		{
			alert("Please select post category.");
			document.getElementById('post_category').focus();
			return false;
		}
	}
	function ajaxadd_tag()
	{
		var new_tag = document.getElementById('new_tag').value;
		if(new_tag == '')
		{
			alert('Please enter the tag');
			return false;
		}
		else
		{
			var httpj1;
			url="<?php echo $this->config->item('base_url'); ?>admin/ajaxaddtag/";
			if(window.XMLHttpRequest){
				httpj1=new XMLHttpRequest();
			}else if(window.ActiveXObject){
				httpj1=new ActiveXObject('msxml2.XMLHTTP');
			}
			//alert(url);
			//alert(new_tag);
			if(httpj1){
				httpj1.open('post',url,true);
				httpj1.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				httpj1.send("newtag="+new_tag+'&time='+Math.random());
				httpj1.onreadystatechange=function(){
					if(httpj1.readyState==4){
						if(httpj1.status==200){
							//alert(httpj1.responseText);
							var res = httpj1.responseText.split("~~~"); 
							var tag_id = res[0];
							var tag_title = res[1];
							var newhtml = '<p id="p_tag_'+tag_id+'" ><a href="javascript:void(0);" onclick="ajaxremovetag('+tag_id+');" >'+tag_title+' <img src="<?php echo $this->config->item('base_url'); ?>public/images/cancel.png" /></a> </p>';
							document.getElementById('post_tags').innerHTML = document.getElementById('post_tags').innerHTML + newhtml;
							if(document.getElementById('input_post_tags').value != '')
							{
								document.getElementById('input_post_tags').value = document.getElementById('input_post_tags').value + ','+tag_id;
							}
							else
							{
								document.getElementById('input_post_tags').value = tag_id;
							}
							document.getElementById('new_tag').value = "";
						}
					}
				}
			}
		}
	}
	function ajaxremovetag(tagid)
	{
		var httpj2;
		url="<?php echo $this->config->item('base_url'); ?>admin/ajaxremovetag/";
		if(window.XMLHttpRequest){
			httpj2=new XMLHttpRequest();
		}else if(window.ActiveXObject){
			httpj2=new ActiveXObject('msxml2.XMLHTTP');
		}
		//alert(url);
		//alert(tagid);

		if(httpj2){
			httpj2.open('post',url,true);
			httpj2.setRequestHeader("Content-type","application/x-www-form-urlencoded");
			httpj2.send("tagid="+tagid+'&time='+Math.random());
			httpj2.onreadystatechange=function(){
				if(httpj2.readyState==4){
					if(httpj2.status==200){
						//alert(httpj2.responseText);
						if(httpj2.responseText!='')
						{
							if(document.getElementById('input_post_tags').value != '')
							{
								var input_post_tags = document.getElementById('input_post_tags').value;
						
								var res = input_post_tags.split(","); 
								res.splice(res.indexOf(httpj2.responseText), 1 );
								var newtags = res.join(","); 
								document.getElementById('input_post_tags').value = newtags;
								document.getElementById('p_tag_'+httpj2.responseText).style.display="none";
							}
						}
					}
				}
			}
		}	
	}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
				<form name="admin_faq_form" id="admin_faq_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/addblogpost" onsubmit="return submit_admin_form()" enctype="multipart/form-data" >
					<input type="hidden" name="admin_action" id="admin_action" value="1">
					<input type="hidden" name="post_id" id="post_id" value="<?=$record[0]->post_id?>">
					
					<div class="merchant_main_right">	  
						<div class="heading">
							<h1>Blog Post</h1>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>
						<div class="merchant_contact">
							<ul>							
								<li>
									<input type="text" name="post_title" id="post_title" class="input_box" placeholder="Post Title*" value="" />
								</li>
								<li>
									<textarea name="short_desc" id="short_desc" placeholder="Short Desc*" style="width:590px; height:200px; "></textarea>
								</li>
								<li>
									<textarea name="post_desc" id="post_desc" placeholder="Post Desc*" style="width:590px; height:200px; "></textarea>
								</li>
								<li>
									<!-- <input type="file" name="post_image" id="post_image" /> -->
									<div class="browse4" style=" clear:both; margin:0px 0px 10px 0px;">
									<input type="file" name="post_image" id="post_image" />
									</div>

								</li>
								<li>
									<!-- onchange="filterbytype(this.value);" -->
									<select name="post_category" class="revostyled" id="post_category" >
										<option value="">Category</option>
										<?php foreach($blog_categories as $the_type){?>	
											<option value="<?=$the_type->id?>" ><?=$the_type->category?></option>
										<?php } ?>
									</select>
								</li>
								<li class="add_tags">
                                	<h2>Tags:</h2>
									<!-- <?php foreach($blog_tags as $the_tag){?>	
										<div style="float:left; margin:0px 15px 10px 0px;">
										<input type="checkbox" name="post_tags[]" value="<?=$the_tag->tag_id?>" />		
										<span style="float:left; display:block; margin:5px 0px 0px 5px; font-size:14px;"><?=$the_tag->tag_name?></span>		
                                        </div>
									<?php } ?> -->
                                    
									<input type="hidden" name="input_post_tags" id="input_post_tags" value="">
									<input  class="input_box" type="text" name="new_tag" id="new_tag" value=""  style="width:400px;" />&nbsp;&nbsp;
                                    <a href="javascript:void(0);" onclick="ajaxadd_tag();"><h2>Add</h2></a>
                                    <div class="inserted_tags">
                                    	<div id="post_tags"></div>
                                    </div>
									
								</li>
							</ul>
						</div>
						<div class="save_bg"> 
							<span class="save">				
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
							</span>  
						</div>
					</div>
				</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
		
	 <!-- <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script> -->

	 <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
	 <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
	 <script type="text/javascript">
		$(document).ready(function(e) {
			$(".revostyled").selectbox();
		});
	</script>

	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
		});
		
		$(function(){		
	
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:checkbox').screwDefaultButtons({
				image: 'url("<?php echo base_url() ?>public/images/agree.jpg")',
				width: 29,
				height: 29
			});	
			
		
		});
	</script>

	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>


	<script type="text/javascript" charset="utf-8">
    $(function(){
      // example 3
	  
      $("#post_image").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });	  
    });
  </script>

</body>
</html>