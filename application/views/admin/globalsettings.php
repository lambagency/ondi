<?php
/*
echo '<pre>';
print_r($admin_data);
echo '</pre>';
*/
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
	function submit_admin_dashboard_form()
	{
		var admin_email = document.getElementById("admin_email").value;
		if(checkBlankField(admin_email) == false)
		{
			alert("Please enter email address.");
			document.getElementById("admin_email").focus();
			return false;
		}
		else
		{
			if(valid_email(admin_email)==true)
			{
				alert ( "Please enter the correct Email address." );
				document.admin_login_form.admin_email.focus();
				return false
			}
		}
		if(checkBlankField(document.getElementById("merchant_commission").value) == false)
		{
			alert("Please enter global merchant commission.");
			document.getElementById("merchant_commission").focus();
			return false;
		}
	}
	$(document).ready(function() {
		/*========FOOTER BOTTOM FIXED=========*/
		var screen_inner_height2=$(document).height();
		//alert(screen_inner_height2)
		$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
	});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<form name="admin_dashboard_form" id="admin_dashboard_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/globalsettings" onsubmit="return submit_admin_dashboard_form()">
				<input type="hidden" name="admin_dashboard_action" id="admin_dashboard_action" value="dashboard">
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1>Global Settings</h1>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>
					<div class="sub_heading">
						<h3>Fields marked (*) are mandatory.</h3>
					</div>
					<div class="merchant_contact">
						<ul>							
							<li><input type="text" name="admin_email" id="admin_email" class="input_box" placeholder="Email Address*" value="<?php echo stripslashes($admin_data[0]->mgmt_email); ?>" /></li>
							
							
							<li><input type="text" name="admin_password" id="admin_password" class="input_box" placeholder="Password" /></li>

							<li><input type="text" name="merchant_commission" id="merchant_commission" class="input_box" placeholder="Global Merchant Commission Percentage" value="<?php echo stripslashes($merchant_commission); ?>" /></li>	

							<li><input type="text" name="twitter_link" id="twitter_link" class="input_box" placeholder="Twitter" value="<?php echo stripslashes($admin_data[0]->twitter_link); ?>" /></li>
							<li><input type="text" name="facebook_link" id="facebook_link" class="input_box" placeholder="Facebook" value="<?php echo stripslashes($admin_data[0]->facebook_link); ?>" /></li>
							<li><input type="text" name="youtube_link" id="youtube_link" class="input_box" placeholder="Youtube" value="<?php echo stripslashes($admin_data[0]->youtube_link); ?>" /></li>							


							<li><input type="text" name="searchbox_heading" id="searchbox_heading" class="input_box" placeholder="Search Box Heading" value="<?php echo stripslashes($admin_data[0]->searchbox_heading); ?>" /></li>	
							<li><textarea name="searchbox_content" id="searchbox_content" placeholder="Search Box Content"><?php echo stripslashes($admin_data[0]->searchbox_content); ?></textarea>
							

							<li><input type="text" name="compare_heading" id="compare_heading" class="input_box" placeholder="Compare Box Heading" value="<?php echo stripslashes($admin_data[0]->compare_heading); ?>" /></li>	
							<li><textarea name="compare_content" id="compare_content" placeholder="Compare Box Content"><?php echo stripslashes($admin_data[0]->compare_content); ?></textarea>	

							<li><input type="text" name="book_heading" id="book_heading" class="input_box" placeholder="Book Box Heading" value="<?php echo stripslashes($admin_data[0]->book_heading); ?>" /></li>	
							<li><textarea name="book_content" id="book_content" placeholder="Book Box Content"><?php echo stripslashes($admin_data[0]->book_content); ?></textarea>
							

							<li><textarea name="gacode" id="gacode" placeholder="Google Analytics Code"><?php echo stripslashes($admin_data[0]->gacode); ?></textarea>	
							
							<li> 
								<input type="checkbox" name="offer_check_by_admin" id="offer_check_by_admin" value="1" <?php if($admin_data[0]->offer_check_by_admin == '1'){ echo 'checked="checked"'; } ?> /> Admin approval required for offers
							</li>

						</ul>
					</div>
					<div class="save_bg"> 
						<span class="save">				
							<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/update_data.jpg" name="" >
						</span>  
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>
