<?php
//print_r($faq_categories);
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
	$(document).ready(function() {
		/*========FOOTER BOTTOM FIXED=========*/
		var screen_inner_height2=$(document).height();
		//alert(screen_inner_height2)
		$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
	});
	function filterbytype(v)
	{
		window.location.href="<?php echo $this->config->item('base_url'); ?>admin/managefaq/?cat_id="+v;
	}

function delete_record(id)
{
	var where_to = confirm("Do you really want to delete this faq?");
	if (where_to == true)
	{		
		link= "<?php echo $this->config->item('base_url'); ?>admin/delfaq/?faq_id="+id;
		document.location.href=link;
	}
	else
	{	
		return false;	
	}
}
</script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<div class="merchant_main_right">	  
				<div class="heading">
					<h1>Manage FAQ</h1>
				</div>	
				<div class="service">
					<ul>
						<li>
							<div class="custom_select_ajax">
								<select name="faq_category" class="revostyled" id="faq_category" onchange="filterbytype(this.value);">
									<option value="" >Category</option>
									<?php foreach($faq_categories as $the_category){?>
									<option value="<?=$the_category->cat_id?>" <?php if($cat_id == $the_category->cat_id){ echo 'selected="selected"';} ?> ><?=$the_category->category?></option>
									<?php } ?>								                                  
								</select>
							</div>
						</li>
					</ul>
				</div>
				<?php echo $this->session->flashdata('admin_message'); ?>
				<div class="admin_detail">
					<table width="595" border="1" class="table_customer">
						<tr>
							<th align="left" scope="col" width="130" height="25">Question</th>
							<th align="left" scope="col" width="110" class="no_bg">&nbsp;</th>
							<th align="left" scope="col" width="40" class="no_bg">&nbsp;</th>
						</tr>
						<?php 
						if(count($faq_data)>0)
						{ 
							foreach($faq_data as $faq_data_val)
							{ ?>
						<tr>
							<td align="left" scope="col" width="260" height="30">
								<?php echo stripslashes($faq_data_val->faq_question); ?>
							</td>
							<td align="left" valign="top" scope="col" width="140" class="permission">
								<!-- <a href="<?php echo $this->config->item('base_url'); ?>admin/managefaq/?cat_id=<?php echo stripslashes($faq_category_val->faq_id); ?>">Manage FAQ</a> -->
							</td>
							<td align="left" scope="col" width="110">
								<div class="delete_merchant"> 
									<a href="javascript:void(0)" onclick="delete_record('<?php echo stripslashes($faq_data_val->faq_id); ?>')" >  <img src="<?php echo base_url() ?>public/images/delete_faq.png" 
									alt="" />Delete</a>
								</div>
								<div class="edit_merchant">
									<a href="<?php echo $this->config->item('base_url'); ?>admin/addeditfaq/?faq_id=<?php echo stripslashes($faq_data_val->faq_id); ?>&cat_id=<?php echo stripslashes($faq_data_val->faq_category); ?>"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Edit</a>
								</div>
							</td>
						</tr>
						<?php } 
						} ?>
						<!-- <tr><td colspan="3">&nbsp;</td></tr>
						<tr><td colspan="3"><a href="<?php echo $this->config->item('base_url'); ?>admin/addeditfaq/?cat_id=<?php echo $faq_data_val->faq_category; ?>">Add FAQ</a></td></tr> -->
					</table>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$(".revostyled").selectbox();
		});
	</script>
</body>
</html>