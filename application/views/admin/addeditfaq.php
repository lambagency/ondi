<?php
/*
echo '<pre>';
print_r($admin_data);
echo '</pre>';
*/
if(isset($faq_cat_detail[0]->cat_id) && $faq_cat_detail[0]->cat_id!=""){ $page_title = ' Update '; }else{ $page_title = ' Add ';  }
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_faq_form()
		{
			var faq_question = document.getElementById("faq_question").value;	
			var faq_answer = document.getElementById("faq_answer").value;
			var faq_category = document.getElementById("faq_category").value;
			if(checkBlankField(faq_category) == false)
			{
				alert("Please select faq category.");
				document.getElementById("faq_question").focus();
				return false;
			}
			if(checkBlankField(faq_question) == false)
			{
				alert("Please enter faq question.");
				document.getElementById("faq_question").focus();
				return false;
			}
			if(checkBlankField(faq_answer) == false)
			{
				alert("Please enter faq question.");
				document.getElementById("faq_answer").focus();
				return false;
			}
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-138+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<form name="admin_faq_form" id="admin_faq_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/addeditfaq" onsubmit="return submit_admin_faq_form()">
				<input type="hidden" name="admin_faq_action" id="admin_faq_action" value="1">
				<input type="hidden" name="cat_id" id="cat_id" value="<?php if(isset($cat_id) && $cat_id!=""){ echo $cat_id; } ?>">
				<input type="hidden" name="faq_id" id="faq_id" value="<?php if(isset($faq_data[0]->faq_id) && $faq_data[0]->faq_id!=""){ echo stripslashes($faq_data[0]->faq_id); }; ?>">
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1><?=$page_title?> FAQ</h1>
					</div>
					<div class="sub_heading">
						<h3>Fields marked (*) are mandatory.</h3>
					</div>
					<div class="merchant_contact">
						<ul>		
							<li>
								<!-- <select name="faq_category" class="revostyled" id="faq_category" onchange="filterbytype(this.value);"> -->
                            	
								<select name="faq_category" class="revostyled" id="faq_category">
									<option value="" >Category</option>
									<?php foreach($faq_categories as $the_category){?>
									<option value="<?=$the_category->cat_id?>" <?php if($cat_id == $the_category->cat_id){ echo 'selected="selected"';} ?> ><?=$the_category->category?></option>
									<?php } ?>								                                  
								</select>
                                
							</li>
							<li>
								<input type="text" name="faq_question" id="faq_question" class="input_box" placeholder="FAQ Question*" value="<?php if(isset($faq_data[0]->faq_question) && $faq_data[0]->faq_question!=""){ echo stripslashes($faq_data[0]->faq_question); }; ?>" />
							</li>
							<li>
								<input type="text" name="faq_answer" id="faq_answer" class="input_box" placeholder="FAQ Answer*" value="<?php if(isset($faq_data[0]->faq_answer) && $faq_data[0]->faq_answer!=""){ echo stripslashes($faq_data[0]->faq_answer); }; ?>" />
							</li>
							<li>
								<input type="text" name="display_order" id="display_order" class="input_box" placeholder="Display Order" value="<?php if(isset($faq_data[0]->display_order) && $faq_data[0]->display_order!=""){ echo stripslashes($faq_data[0]->display_order); }; ?>" />
							</li>
							<li>
								<!-- <input type="text" name="display_status" id="display_status" class="input_box" placeholder="Display Status" value="<?php //if(isset($faq_cat_detail[0]->category) && $faq_cat_detail[0]->category!=""){ echo stripslashes($faq_cat_detail[0]->category); }; ?>" /> -->

								<div class="left">
									<input type="radio" name="display_status" id="display_status" value="1" <?php if($faq_data[0]->display_status=='1'){ echo 'checked'; } ?> /> Show &nbsp;
								</div>
								<div class="left">
									<input type="radio" name="display_status" id="display_status" value="0" <?php if($faq_data[0]->display_status=='0'){ echo 'checked'; } ?> /> Hide
								</div>

							</li>
						</ul>
					</div>
					<div class="save_bg"> 
						<span class="save">				
							<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
						</span>  
					</div>
				</div>
			</form>
		</div>
	</div>
    
    
	<?php $this->load->view('templates/admin_footer');?>
    
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$(".revostyled").selectbox();
		});
	</script>
    
    
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
	<script type="text/javascript">
		$(function(){
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:radio').screwDefaultButtons({
				image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
				width: 19,
				height: 18
			});
		});
	</script>
</body>
</html>