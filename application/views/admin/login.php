<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script type="text/javascript">
	function submit_admin_login_form()
	{
		var admin_email = document.getElementById("admin_email").value;
		var admin_password = document.getElementById("admin_password").value;
		if(checkBlankField(admin_email) == false)
		{
			alert("Please enter email address.");
			document.getElementById("admin_email").focus();
			return false;
		}
		else
		{
			if(valid_email(document.admin_login_form.admin_email.value)==true)
			{
				alert ( "Please enter the correct Email address." );
				document.admin_login_form.admin_email.focus();
				return false
			}
		}
		if(checkBlankField(admin_password) == false)
		{
			alert("Please enter password.");
			document.getElementById("admin_password").focus();
			return false;
		}	
	}
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php //$this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main" style=" background:none;">
			<?php //$this->load->view('templates/admin_main_left');?>
			<div class="merchant_main_right" style="width:100%;">
				<form name="admin_login_form" id="admin_login_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/login" onsubmit="return submit_admin_login_form()">
					<input type="hidden" name="admin_login_action" id="admin_login_action" value="1">
					<div class="heading">
						<h1>Login</h1>
					</div>
					<div class="sub_heading">
						<h3>Fields marked (*) are mandatory.</h3>
					</div>
					<div class="valid_errors">
						<?php 
						echo validation_errors(); 
						echo $this->session->flashdata('admin_login_err_msg');
						?>
					</div>
					<div class="merchant_contact">
						<ul>
							<li><input type="text" name="admin_email" id="admin_email" class="input_box" value="" placeholder="Email*" /></li>
							<li><input type="password" name="admin_password" id="admin_password" class="input_box"  value="" placeholder="Password*" /></li>         
						</ul>
					</div>
					<div class="save_bg"> 
						<span class="save">
							<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/sign_in.jpg" >
						</span>  
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>
