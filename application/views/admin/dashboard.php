<?php
/*
echo '<pre>';
print_r($admin_data);
echo '</pre>';
*/
$exportQryStr="";
if(isset($_GET['keyword']) && $_GET['keyword']!="")
{
	$exportQryStr .= "&keyword=".$_GET['keyword'];
}
if(isset($_GET['order_from']) && $_GET['order_from']!="")
{
	$_GET['order_from'];
	
	$exportQryStr .= "&order_from=".$_GET['order_from'];
}
if(isset($_GET['order_to']) && $_GET['order_to']!="")
{
	$_GET['order_to'];
	
	$exportQryStr .= "&order_to=".$_GET['order_to'];
}
if(isset($_GET['amount_from']) && $_GET['amount_from']!="")
{
	$exportQryStr .= "&amount_from=".$_GET['amount_from'];
}
if(isset($_GET['amount_to']) && $_GET['amount_to']!="")
{
	$exportQryStr .= "&amount_to=".$_GET['amount_to'];
}


?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min1.8.js"></script> 

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/cust.jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.theme_cust.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">
	
	
	
	
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>

	<script>
		function dashboard_search_validation()
		{
			var amount_from = document.getElementById("amount_from").value;
			var amount_to = document.getElementById("amount_to").value;

			if(checkBlankField(amount_from) == true)
			{
				if(IsNumeric(amount_from) == false)
				{
					alert ( "Please enter only numeric value for amount." );					
					document.getElementById("amount_from").focus();
					return false;
				}
			}
			if(checkBlankField(amount_to) == true)
			{
				if(IsNumeric(amount_to) == false)
				{
					alert ( "Please enter only numeric value for amount." );					
					document.getElementById("amount_to").focus();
					return false;
				}
			}
		}
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			
							
				<div class="merchant_main_right">	  
					
					
					<div class="heading">
						<h1>Dashboard</h1>

						<a href="<?php echo $this->config->item('base_url'); ?>admin/dashboard/?action2=export<?php echo $exportQryStr; ?>" target="_blank" style="float:right;" ><b>Export Orders</b></a>
					</div>
										
					<div class="sub_heading">
						<h3>Welcome Admin!</h3>
						<?php if($num_pending_offers>0){?>
						<h3><font color="red"><?=$num_pending_offers?> offers needs your approval. <a href="<?php echo $this->config->item('base_url'); ?>admin/approveoffers/?ds=4">Click here</a> to approve.</font></h3>
						<?php } ?>
						<?php if($merchants_without_password>0){?>
						<h3><font color="red"><?=$merchants_without_password?> new merchants registered. <a href="<?php echo $this->config->item('base_url'); ?>admin/assignpassword/">Click here</a> to assign password</font></h3>
						<?php } ?>
					</div>
					
										
					
					
					
					<div class="admin_detail">
						<table width="595" border="1" class="table_customer">							
							
							<form name="dashboard_search" id="dashboard_search" method="get" action="<?php echo $this->config->item('base_url'); ?>admin/dashboard/" onsubmit="return dashboard_search_validation()" >

							<input type="hidden" name="dashboard_action" id="dashboard_action" value="search">
							<tr>
								<td align="center" colspan="7">
                                	<div class="merchant_search_form"> 
                                    	<h2>Search</h2>
                                        <div class="search_input"><input type="text" name="keyword" id="keyword" value="<?php if(isset($keyword)){ echo $keyword; } ?>" placeholder="keyword"></div>

										
										<!-- <div>
											Date From: <input type="text" name="order_from" id="order_from" value="<?php if(isset($order_from)){ echo $order_from; } ?>">
											Date To: <input type="text" name="order_to" id="order_to" value="<?php if(isset($order_to)){ echo $order_to; } ?>">											
										</div> -->
									<div class="search_input_date">
										<div class="date">
											<input type="text" id="datepicker" name="order_from" size="30" value="<?php if(isset($order_from)){ echo $order_from; } ?>" />
										</div>
										<div class="date">
											<input type="text" id="datepicker2" size="30" name="order_to" value="<?php if(isset($order_to)){ echo $order_to; } ?>" />
										</div>
                                        </div>

										<div class="search_input_amount">

										<div class="from">
											<label>Amount From: </label><input type="text" name="amount_from" id="amount_from" value="<?php if(isset($amount_from)){ echo $amount_from; } ?>">
                                            </div>
                                            <div class="from">
											<label>Amount To: </label><input type="text" name="amount_to" id="amount_to" value="<?php if(isset($amount_to)){ echo $amount_to; } ?>">
										</div>
                                        </div>

                                        <div class="search_button">
											<input type="submit" name="submit" value="submit" class="merchant_search_submit" >
										</div>
                                       
                                   </div>
                                </td>
							</tr>
                            
                            
							</form>
							
							
							<tr class="table_head">
								<th align="left" scope="col" height="25">Date</th>
								<th align="left" scope="col" height="25">Amount</th>
								<th align="left" scope="col" height="25">Booking Code</th>
								<th align="left" scope="col" height="25">Customer name</th>
								<th align="left" scope="col" height="25">Name of offer</th>
								<th align="left" scope="col" height="25">Status</th>																
							</tr>

							<?php
							
							if(count($dashboard_search_data)>0)
							{ 
								foreach($dashboard_search_data as $val)
									{	
									
										$name="";
										$order_status="";
										if($val->first_name!="")
										{
											$name = stripslashes($val->first_name);
										}
										else if($val->contact_name!="")
										{
											$name = stripslashes($val->contact_name);
										}

										if($val->order_status=='0')
										{
											$order_status = 'Failed';

										}
										else if($val->order_status=='2')
										{
											$order_status = 'Cancelled';

										}
										else if($val->order_status=='1'){

											$order_status = 'Successful';
										}
							?>
									<tr>
										<td align="left" scope="col" height="30">
											<?php 
												if($val->order_date!='0000-00-00 00:00:00')
												{
													echo date('D d/m/y',strtotime($val->order_date)); 
												}
											?>
										</td>
										<td align="left" scope="col" height="30">
										<?php 
												echo "$".stripslashes($val->total_price); 
										?>
										</td>
										<td align="left" scope="col" height="30">
										<?php 
												echo stripslashes($val->booking_code); 
										?></td>
										<td align="left" scope="col" height="30">
										<?php 
												echo $name;
										?></td>
										<td align="left" scope="col" height="30">
										<?php 
												echo stripslashes($val->offer_title); 
										?></td>
										<td align="left" scope="col" height="30" style="padding-right:5px;"><?php echo $order_status; ?></td>
									</tr>

								<?php 
									
									} 
								}else {
									
								
								?>
							<tr>
								<td align="center" colspan="7">No records found</td>
							</tr>
							<?php } ?>
						</table>
					</div>
						
				</div>			
		</div>
	</div>

	<?php $this->load->view('templates/admin_footer');?>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery-1.8.3.js"></script>    



<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>		




<script src="<?php echo $this->config->item('base_url'); ?>public/js/jquery.rateit.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script>


<?php //echo "TEST1"; ?>


<!-- <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/themes/base/jquery.ui.all.css">
 -->
<?php //echo "TEST2"; die;?> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.core.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.datepicker.js"></script>

   
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">



	<script type="text/javascript">
	
		$(function() {
			var $c1 = jQuery.noConflict();
			$c1( "#datepicker" ).datepicker({
					dateFormat: 'dd/mm/yy',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true
				});
			
		});
		
		
		$(function() {
			var $c2 = jQuery.noConflict();
			$c2( "#datepicker2" ).datepicker({
					dateFormat: 'dd/mm/yy',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true
				});
			
		});

</script>



</body>
</html>
