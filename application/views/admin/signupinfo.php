<?php
/*echo '<pre>';
print_r($signupinfo_data);
echo '</pre>';*/
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/ckeditor/ckeditor.js"></script>
	<!-- <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/ckfinder/ckfinder.js"></script> -->
	<script>
	$(document).ready(function() {
		/*========FOOTER BOTTOM FIXED=========*/
		var screen_inner_height2=$(document).height();
		//alert(screen_inner_height2)
		$(".merchant_main").css('min-height',screen_inner_height2-258+'px' );
	});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
			<form name="admin_dashboard_form" id="admin_dashboard_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/signupinfo" enctype="multipart/form-data">
				<input type="hidden" name="admin_dashboard_action" id="admin_dashboard_action" value="dashboard">
				<div class="merchant_main_right">	  
					<div class="heading">
						<h1>Sign Up Information</h1>
						<?php
						$error = $this->session->flashdata('admin_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
					</div>
					<div class="sub_heading">
						<!-- <h3>Fields marked (*) are mandatory.</h3> -->
					</div>
					<div class="merchant_contact">
						<?php 
						$box1 = "Box1 Content";
						$box2 = "Box2 Content";
						$box3 = "Box3 Content";
						$box4 = "Box4 Content";
						$box5 = "Box5 Content";
						$box6 = "Box6 Content";
						$box7 = "Box7 Content";
						$bg_image = "";
						if($signupinfo_data[0]->box1 != '') $box1 = $signupinfo_data[0]->box1;
						if($signupinfo_data[0]->box2 != '') $box2 = $signupinfo_data[0]->box2;
						if($signupinfo_data[0]->box3 != '') $box3 = $signupinfo_data[0]->box3;
						if($signupinfo_data[0]->box4 != '') $box4 = $signupinfo_data[0]->box4;
						if($signupinfo_data[0]->box5 != '') $box5 = $signupinfo_data[0]->box5;
						if($signupinfo_data[0]->box6 != '') $box6 = $signupinfo_data[0]->box6;
						if($signupinfo_data[0]->box7 != '') $box7 = $signupinfo_data[0]->box7;

						if($signupinfo_data[0]->bg_image != '') $bg_image = $signupinfo_data[0]->bg_image;


						?>

						<input type="hidden" name="bg_image_old" id="bg_image_old" value="<?=$bg_image?>">
						<ul>							
							<li>
								<?php// echo $this->ckeditor->editor("box1",$box1); ?>
								<textarea class="in-textarea" name="box1" id="box1" ><?=stripslashes($box1);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box1',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box2",$box2); ?>
								<textarea class="in-textarea" name="box2" id="box2" ><?=stripslashes($box2);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box2',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box3",$box3); ?>
								<textarea class="in-textarea" name="box3" id="box3" ><?=stripslashes($box3);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box3',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box4",$box4); ?>
								<textarea class="in-textarea" name="box4" id="box4" ><?=stripslashes($box4);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box4',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box5",$box5); ?>
								<textarea class="in-textarea" name="box5" id="box5" ><?=stripslashes($box5);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box5',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box6",$box6); ?>
								<textarea class="in-textarea" name="box6" id="box6" ><?=stripslashes($box6);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box6',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<?php //echo $this->ckeditor->editor("box7",$box7); ?>
								<textarea class="in-textarea" name="box7" id="box7" ><?=stripslashes($box7);?></textarea>
								<script type="text/javascript">
								CKEDITOR.replace('box7',
								 {
									filebrowserBrowseUrl : '../ckfinder/ckfinder.html',
									filebrowserImageBrowseUrl : '../ckfinder/ckfinder.html?type=Images',
									filebrowserFlashBrowseUrl : '../ckfinder/ckfinder.html?type=Flash',
									filebrowserUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
									filebrowserImageUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
									filebrowserFlashUploadUrl : '../ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
								 } 
								 );			
								 </script>
							<li>

							<li>
								<input type="file" name="bg_image" id="bg_image" >
								
								
							</li>
							
							<?php if($bg_image!=""){ ?>
							<li>
								<img src="<?php echo $this->config->item('base_url'); ?><?=$bg_image?>" width="200">
							</li>
							<?php } ?>

						</ul>
					</div>
					<div class="save_bg"> 
						<span class="save">				
							<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/update_data.jpg" name="" >
						</span>  
					</div>
				</div>
			</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
</body>
</html>
