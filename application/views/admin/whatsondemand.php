<?php
/*echo '<pre>';
print_r($records);
echo '</pre>';
*/
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title><?php echo $title; ?> | ONDI FOOD</title>
	<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/admin_style.css" rel="stylesheet" type="text/css" />
	<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
	<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
	<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
	<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
	<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
	<!--[if lt IE 8]>
	<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
	</div>
	<![endif]-->
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>
	<script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
	<script>
		function submit_admin_form()
		{
			if(checkBlankField(document.getElementById("placeholder_1").value) == false)
			{
				alert("Please select offer for placeholder 1.");
				document.getElementById("placeholder_1").focus();
				return false;
			}
			if(checkBlankField(document.getElementById("placeholder_2").value) == false)
			{
				alert("Please select offer for placeholder 2.");
				document.getElementById("placeholder_2").focus();
				return false;
			}
			if(checkBlankField(document.getElementById("placeholder_3").value) == false)
			{
				alert("Please select offer for placeholder 3.");
				document.getElementById("placeholder_3").focus();
				return false;
			}
			if(checkBlankField(document.getElementById("placeholder_4").value) == false)
			{
				alert("Please select offer for placeholder 4.");
				document.getElementById("placeholder_4").focus();
				return false;
			}
			if(checkBlankField(document.getElementById("placeholder_5").value) == false)
			{
				alert("Please select offer for placeholder 5.");
				document.getElementById("placeholder_5").focus();
				return false;
			}
			if(checkBlankField(document.getElementById("placeholder_6").value) == false)
			{
				alert("Please select offer for placeholder 6.");
				document.getElementById("placeholder_6").focus();
				return false;
			}
		}
	</script>
	<script>
		$(document).ready(function() {
			/*========FOOTER BOTTOM FIXED=========*/
			var screen_inner_height2=$(document).height();
			//alert(screen_inner_height2)
			$(".merchant_main").css('min-height',screen_inner_height2-50+'px' );
		});
	</script>
</head>
<body>
	<?php $this->load->view('templates/admin_header');?>
	<?php $this->load->view('templates/admin_header_bg');?>
	<div class="merchant_main_con">
		<div class="merchant_main">
			<?php $this->load->view('templates/admin_main_left');?>
				<form name="admin_faq_form" id="admin_faq_form" method="post" action="<?php echo $this->config->item('base_url'); ?>admin/whatsondemand" onsubmit="return submit_admin_form()">
					<input type="hidden" name="admin_action" id="admin_action" value="1">
					<input type="hidden" name="city_id" id="city_id" value="<?=$records[0]->id?>">
					<div class="merchant_main_right">	  
						<div class="heading">
							<h1>Whats on demand</h1>
							<?php
							$error = $this->session->flashdata('admin_message');
							?>
							<?php if($error) : ?>
								<div id="error_text"><?php echo $error ?></div>
							<?php endif; ?>
						</div>
						<div class="sub_heading">
							<h3>Fields marked (*) are mandatory.</h3>
						</div>
						<div class="merchant_contact">
							<ul>		
								<li>
									<select name="placeholder_1" class="revostyled" id="placeholder_1" >
										<option value="">Placeholder 1</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_1 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
								<li>
									<select name="placeholder_2" class="revostyled" id="placeholder_2" >
										<option value="">Placeholder 2</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_2 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
								<li>
									<select name="placeholder_3" class="revostyled" id="placeholder_3" >
										<option value="">Placeholder 3</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_3 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
								<li>
									<select name="placeholder_4" class="revostyled" id="placeholder_4" >
										<option value="">Placeholder 4</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_4 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
								<li>
									<select name="placeholder_5" class="revostyled" id="placeholder_5" >
										<option value="">Placeholder 5</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_5 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
								<li>
									<select name="placeholder_6" class="revostyled" id="placeholder_6" >
										<option value="">Placeholder 6</option>
										<?php foreach($offers as $the_offer){?>	
											<option value="<?=$the_offer->id?>" <?php if($records[0]->placeholder_6 ==$the_offer->id){ echo 'selected="selected"';} ?> ><?=$the_offer->offer_title?></option>
										<?php } ?>
									</select>
								</li>
							</ul>
						</div>
						<div class="save_bg"> 
							<span class="save">				
								<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/user_save.jpg" name="" >
							</span>  
						</div>
					</div>
				</form>
		</div>
	</div>
	<?php $this->load->view('templates/admin_footer');?>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
	<script type="text/javascript">
		$(document).ready(function(e) {
			$(".revostyled").selectbox();
		});
	</script>
</body>
</html>