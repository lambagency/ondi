<?php
$week_days = $this->users->get_week_days();
/*
echo '<pre>';
print_r($week_days);
echo '</pre>';
*/
$max_pax_display_check = 0;
if (isset($merchant_business_type)) {
    if (in_array('1', $merchant_business_type)) {
        $max_pax_display_check = 1;
    }

}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<title><?php if (isset($title)) {
        echo $title;
    } ?></title>
<link href="<?= base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css"/>
<link href="<?= base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css"/>

<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/offer.jquery.selectbox.css"
      type="text/css" rel="stylesheet"/>


<!--[if lte IE 6]>
<link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css"/><![endif]-->
<!--[if IE 7]>
<link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css"/><![endif]-->
<!--[if IE 8]>
<link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css"/><![endif]-->
<!--[if IE 9]>
<link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css"/><![endif]-->
<!--[if lt IE 9]>
<script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
    <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img
        src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42"
        width="820"
        alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?= base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?= base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile()) {
    ?>
    <script type="text/javascript"
            src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php
}
else
{
?>
    <script type="text/javascript"
            src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<script type="text/javascript" src="<?= base_url('/public/js/common.js'); ?>"></script>
<link href="<?= base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css"/>


<script>
function save_business_hours() {
    var buss_open_hrs = "";
    var entered_buss_hrs_count = 0;
    var buss_upd_hrs_available_day = '';
    var buss_upd_hrs_time_from = '';
    var buss_upd_hrs_time_to = '';
    var buss_upd_hrs_time_from_am_pm = '';
    var buss_upd_hrs_time_to_am_pm = '';

    entered_buss_hrs_count = document.getElementById("entered_buss_hrs_count").value;
    buss_open_hrs = buss_open_hrs + "entered_buss_hrs_count=" + entered_buss_hrs_count;

    var buss_hours_monday = 0;
    var buss_hours_tuesday = 0;
    var buss_hours_wednesday = 0;
    var buss_hours_thursday = 0;
    var buss_hours_friday = 0;
    var buss_hours_saturday = 0;
    var buss_hours_sunday = 0;

    for (var i = 1; i <= entered_buss_hrs_count; i++) {
        if (document.getElementById("buss_upd_hrs_available_day_" + i)) {
            var buss_hour_day = document.getElementById("buss_upd_hrs_available_day_" + i).value;
            if (buss_hour_day == 'monday') {
                buss_hours_monday++;
            }
            if (buss_hour_day == 'tuesday') {
                buss_hours_tuesday++;
            }
            if (buss_hour_day == 'wednesday') {
                buss_hours_wednesday++;
            }
            if (buss_hour_day == 'thursday') {
                buss_hours_thursday++;
            }
            if (buss_hour_day == 'friday') {
                buss_hours_friday++;
            }
            if (buss_hour_day == 'saturday') {
                buss_hours_saturday++;
            }
            if (buss_hour_day == 'sunday') {
                buss_hours_sunday++;
            }
        }
    }

    for (var i = 1; i <= entered_buss_hrs_count; i++) {

        if (buss_hours_monday > 1 || buss_hours_tuesday > 1 || buss_hours_wednesday > 1 || buss_hours_thursday > 1 || buss_hours_friday > 1 || buss_hours_saturday > 1 || buss_hours_sunday > 1) {
            alert("Please check your business hours days.");
            return false;
        }

        if (document.getElementById("buss_upd_hrs_available_day_" + i)) {
            buss_upd_hrs_available_day = document.getElementById("buss_upd_hrs_available_day_" + i).value;
            buss_open_hrs = buss_open_hrs + "&buss_upd_hrs_available_day_" + i + "=" + buss_upd_hrs_available_day;
        }
        if ($(".buss_upd_hrs_time_from_dropdown_" + i).length > 0) {
            buss_upd_hrs_time_from = $(".buss_upd_hrs_time_from_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_upd_hrs_time_from_" + i + "=" + buss_upd_hrs_time_from;
        }
        if ($(".buss_upd_hrs_time_to_dropdown_" + i).length > 0) {
            buss_upd_hrs_time_to = $(".buss_upd_hrs_time_to_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_upd_hrs_time_to_" + i + "=" + buss_upd_hrs_time_to;
        }
        if ($(".buss_upd_hrs_time_from_am_pm_dropdown_" + i).length > 0) {
            buss_upd_hrs_time_from_am_pm = $(".buss_upd_hrs_time_from_am_pm_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_upd_hrs_time_from_am_pm_" + i + "=" + buss_upd_hrs_time_from_am_pm;
        }
        if ($(".buss_upd_hrs_time_to_am_pm_dropdown_" + i).length > 0) {

            buss_upd_hrs_time_to_am_pm = $(".buss_upd_hrs_time_to_am_pm_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_upd_hrs_time_to_am_pm_" + i + "=" + buss_upd_hrs_time_to_am_pm;

        }
        if (document.getElementById("merchant_opening_hours_id_" + i)) {
            merchant_opening_hours_id = document.getElementById("merchant_opening_hours_id_" + i).value;
            buss_open_hrs = buss_open_hrs + "&merchant_opening_hours_id_" + i + "=" + merchant_opening_hours_id;
        }

    }
    var remaining_opening_hours_days = 0;
    remaining_opening_hours_days = document.getElementById("remaining_opening_hours_days").value;


    for (var i = 1; i <= remaining_opening_hours_days; i++) {
        if (document.getElementById("buss_ins_hrs_available_day_" + i)) {
            var buss_ins_hrs_avail_day = "";
            buss_hour_day = document.getElementById("buss_ins_hrs_available_day_" + i).value;

            if (buss_hour_day == 'monday') {
                buss_hours_monday++;
            }
            if (buss_hour_day == 'tuesday') {
                buss_hours_tuesday++;
            }
            if (buss_hour_day == 'wednesday') {
                buss_hours_wednesday++;
            }
            if (buss_hour_day == 'thursday') {
                buss_hours_thursday++;
            }
            if (buss_hour_day == 'friday') {
                buss_hours_friday++;
            }
            if (buss_hour_day == 'saturday') {
                buss_hours_saturday++;
            }
            if (buss_hour_day == 'sunday') {
                buss_hours_sunday++;
            }
        }

    }

    for (var i = 1; i <= remaining_opening_hours_days; i++) {

        if (buss_hours_monday > 1 || buss_hours_tuesday > 1 || buss_hours_wednesday > 1 || buss_hours_thursday > 1 || buss_hours_friday > 1 || buss_hours_saturday > 1 || buss_hours_sunday > 1) {
            alert("Please check your business hours days.");
            return false;
        }

        if (document.getElementById("buss_ins_hrs_available_day_" + i)) {
            buss_ins_hrs_available_day = document.getElementById("buss_ins_hrs_available_day_" + i).value;
            buss_open_hrs = buss_open_hrs + "&buss_ins_hrs_available_day_" + i + "=" + buss_ins_hrs_available_day;
        }
        if ($(".buss_ins_hrs_time_from_dropdown_" + i).length > 0) {
            buss_ins_hrs_time_from = $(".buss_ins_hrs_time_from_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_ins_hrs_time_from_" + i + "=" + buss_ins_hrs_time_from;
        }
        if ($(".buss_ins_hrs_time_to_dropdown_" + i).length > 0) {
            buss_ins_hrs_time_to = $(".buss_ins_hrs_time_to_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_ins_hrs_time_to_" + i + "=" + buss_ins_hrs_time_to;
        }
        if ($(".buss_ins_hrs_time_from_am_pm_dropdown_" + i).length > 0) {
            buss_ins_hrs_time_from_am_pm = $(".buss_ins_hrs_time_from_am_pm_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_ins_hrs_time_from_am_pm_" + i + "=" + buss_ins_hrs_time_from_am_pm;
        }
        if ($(".buss_ins_hrs_time_to_am_pm_dropdown_" + i).length > 0) {

            buss_ins_hrs_time_to_am_pm = $(".buss_ins_hrs_time_to_am_pm_dropdown_" + i).val();
            buss_open_hrs = buss_open_hrs + "&buss_ins_hrs_time_to_am_pm_" + i + "=" + buss_ins_hrs_time_to_am_pm;

        }

    }


    var merchant_liveoffers = document.getElementById("merchant_liveoffers").value;
    if (merchant_liveoffers > 0) {

        var r = confirm("Note: changing your opening hours may affect your current offers.");
        if (r == true) {
            buss_open_hrs = buss_open_hrs + "&update_existing_offers=1";
        }
        else {
            buss_open_hrs = buss_open_hrs + "&update_existing_offers=0";
        }

    } else {

        buss_open_hrs = buss_open_hrs + "&update_existing_offers=0";
    }


    buss_open_hrs = buss_open_hrs + "&time=" + Math.random();
    //alert(buss_open_hrs);	
    var httpj1;
    url = '<?php echo $config['base_url']; ?>merchantaccount/updatebusshours';
    //url='http://192.168.1.16/ondi/merchantaccount/updatebusshours';

    if (window.XMLHttpRequest) {
        httpj1 = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        httpj1 = new ActiveXObject('msxml2.XMLHTTP');
    }
    if (httpj1) {
        httpj1.open('post', url, true);
        httpj1.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpj1.send(buss_open_hrs);

        httpj1.onreadystatechange = function () {
            if (httpj1.readyState == 4) {
                if (httpj1.status == 200) {

                    //alert(httpj1.responseText);

                    document.getElementById('buss_hrs_upd_msg').innerHTML = httpj1.responseText;
                    show_updated_business_hours();

                }
            }
        }
    }

}

function show_updated_business_hours() {
    var httpj1;
    var merchant_business_hours = '1';
    url = '<?php echo $config['base_url']; ?>merchantaccount/viewbusinesshours';
    //url='http://192.168.1.16/ondi/merchantaccount/viewbusinesshours';

    if (window.XMLHttpRequest) {
        httpj1 = new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        httpj1 = new ActiveXObject('msxml2.XMLHTTP');
    }
    if (httpj1) {
        httpj1.open('post', url, true);
        httpj1.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        httpj1.send("merchant_business_hours=" + merchant_business_hours + '&time=' + Math.random());
        httpj1.onreadystatechange = function () {
            if (httpj1.readyState == 4) {
                if (httpj1.status == 200) {

                    // document.getElementById('business_opening_hrs_info_div').innerHTML = httpj1.responseText;

                    //alert(httpj1.responseText);					

                    document.getElementById('all_opening_hours_div').style.display = "none";
                    uncheck_all_opening_hours();

                    document.getElementById('all_opening_hours_div').innerHTML = httpj1.responseText;
                    $(".revostyled").selectbox();

                }
            }
        }
    }
}
</script>
</head>

<body>


<?php echo form_open(base_url() . "merchantaccount/updatebusshours", array('id' => 'buss_hrs_frm')) ?>
<input type="hidden" name="entered_buss_hrs_count" id="entered_buss_hrs_count"
       value="<?php if (isset($merchant_opening_hours)) {
           echo count($merchant_opening_hours);
       } ?>">
<!--Start of popup_bg-->
<div class="popup_bg" style="display:none;" id="buss_opening_hrs_div">
    <div class="popup_window">
        <div class="popup_bg_top">
            <a href="javascript:void(0)" onclick="edit_business_opening_hours()"><img
                    src="<?php echo base_url() ?>public/images/cancel-popup.png"/></a>
        </div>
        <!--End of popup_bg_top-->
        <div class="popup_bg_main">
            <div id="buss_hrs_upd_msg"></div>
            <div class="hours">
                <input type="hidden" name="merchant_liveoffers" id="merchant_liveoffers"
                       value="<?php echo count($merchant_liveoffers); ?>">

                <h2>Opening hours :</h2>

                <?php
                $y = 1;
                if (isset($merchant_opening_hours) && count($merchant_opening_hours) > 0) {

                    foreach ($merchant_opening_hours as $opening_hours_val) {
                        ?>

                        <input type="hidden" name="merchant_opening_hours_id_<?= $y ?>"
                               id="merchant_opening_hours_id_<?= $y ?>" value="<?= $opening_hours_val->id ?>">
                        <ul>
                            <li class="day">

                                <select name="buss_upd_hrs_available_day_<?= $y ?>"
                                        id="buss_upd_hrs_available_day_<?= $y ?>" class="revostyled">
                                    <?php foreach ($week_days as $val) { ?>
                                        <option
                                            value="<?php echo $val[1]; ?>" <?php if (isset($opening_hours_val->day) && $val[1] == $opening_hours_val->day) {
                                            echo "selected";
                                        } ?>  ><?php echo $val[0]; ?></option>
                                    <?php } ?>
                                </select>

                            </li>
                            <li class="from">
                                <label>from</label>

                                <?php echo $time_dropdown = $this->users->get_time_dropdown('buss_upd_hrs_time_from_' . $y, 'revostyled buss_upd_hrs_time_from_dropdown_' . $y, $opening_hours_val->from_hour); ?>

                                <div class="time">
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('buss_upd_hrs_time_from_am_pm_' . $y, 'revostyled buss_upd_hrs_time_from_am_pm_dropdown_' . $y, $opening_hours_val->from_ampm); ?>
                                </div>

                            </li>
                            <li class="to">
                                <label>to</label>
                                <?php echo $time_dropdown = $this->users->get_time_dropdown('buss_upd_hrs_time_to_' . $y, 'revostyled buss_upd_hrs_time_to_dropdown_' . $y, $opening_hours_val->to_hour, 'to'); ?>


                                <div class="time">
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('buss_upd_hrs_time_to_am_pm_' . $y, 'revostyled buss_upd_hrs_time_to_am_pm_dropdown_' . $y, $opening_hours_val->to_ampm); ?>
                                </div>
                            </li>
                        </ul>
                        <?php $y++;
                    }
                } ?>


                <?php
                $total_opening_hours_days = 7;
                //echo '<br>';
                $existing_merchant_opening_hours_days = 0;
                $existing_merchant_opening_hours_days = count($merchant_opening_hours);
                //echo '<br>';
                $remaining_opening_hours_days = $total_opening_hours_days - $existing_merchant_opening_hours_days;

                ?>

                <input type="hidden" name="remaining_opening_hours_days" id="remaining_opening_hours_days"
                       value="<?= $remaining_opening_hours_days ?>">


                <!-- Add new available business hours time -->
                <?php for ($y = 1; $y <= $remaining_opening_hours_days; $y++) { ?>
                    <ul id="buss_ins_ul_<?= $y ?>" style="display:none;">
                        <li class="day">

                            <select name="buss_ins_hrs_available_day_<?= $y ?>"
                                    id="buss_ins_hrs_available_day_<?= $y ?>" class="revostyled">
                                <option value="">Day</option>
                                <?php foreach ($week_days as $val) { ?>
                                    <option value="<?php echo $val[1]; ?>"><?php echo $val[0]; ?></option>
                                <?php } ?>
                            </select>

                        </li>
                        <li class="from">
                            <label>from</label>
                            <?php echo $time_dropdown = $this->users->get_time_dropdown('buss_ins_hrs_time_from_' . $y, 'revostyled buss_ins_hrs_time_from_dropdown_' . $y); ?>

                            <div class="time">
                                <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('buss_ins_hrs_time_from_am_pm_' . $y, 'revostyled buss_ins_hrs_time_from_am_pm_dropdown_' . $y); ?>
                            </div>

                        </li>
                        <li class="to">
                            <label>to</label>
                            <?php echo $time_dropdown = $this->users->get_time_dropdown('buss_ins_hrs_time_to_' . $y, 'revostyled buss_ins_hrs_time_to_dropdown_' . $y, '', 'to'); ?>


                            <div class="time">
                                <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('buss_ins_hrs_time_to_am_pm_' . $y, 'revostyled buss_ins_hrs_time_to_am_pm_dropdown_' . $y); ?>
                            </div>
                        </li>
                    </ul>
                <?php } ?>
                <!-- Add new available business hours time -->

                <a onclick="show_more_opening_hours();" href="javascript:void(0);"
                   id="another_opening_time_anchor" <?php if ($remaining_opening_hours_days > 0) { ?> style="display:;" <?php } else { ?> style="display:none;" <?php } ?> >+
                    add another opening time</a>

                <script>
                    function show_more_opening_hours() {
                        var remaining_opening_hours_days = 0;
                        remaining_opening_hours_days = document.getElementById("remaining_opening_hours_days").value;

                        for (var i = 1; i <= remaining_opening_hours_days; i++) {
                            if (document.getElementById("buss_ins_ul_" + i)) {
                                if (document.getElementById("buss_ins_ul_" + i).style.display == "none") {
                                    document.getElementById("buss_ins_ul_" + i).style.display = "";
                                    if (i == 10) {

                                        document.getElementById("another_opening_time_anchor").style.display = "none";
                                    }
                                    return false;
                                }
                            }

                        }
                    }
                </script>


                <a href="javascript:void()" onclick="save_business_hours()"><img
                        src="<?php echo base_url() ?>public/images/update_data.jpg" alt=""/></a>


            </div>
        </div>
        <!--End of popup_bg_main-->
    </div>
    <!--End of popup_window-->
</div>
<!--End of popup_bg-->
<?php echo form_close() ?>




<div class="popup_bg" id="description_advise" title="Short description of offer" style="display:none;">
    <div class="popup_window">
        <div class="popup_bg_top">


            <a href="javascript:void(0)"
               onclick="hide_suggestion_box('offer_description','Short description of offer','description_advise')"
               id="advise_close"><img src="<?php echo base_url() ?>public/images/cancel-popup.png"/></a>

        </div>
        <!--End of popup_bg_top-->
        <div class="popup_bg_main">
            <!-- <p><strong>Short description of offer</strong></p> -->
            <p>(if necesarry expand on the offer. Maximum 100 words)</p>

            <p>
                1) Example short description of offer<br/>
                2) Another example short description of offer a bit longer<br/>
                3) Example short description of offer a bit longer
            </p>


        </div>
    </div>
</div>


<div class="popup_bg" id="print_advise" title="Fine print (if any)" style="display:none;">
    <div class="popup_window">
        <div class="popup_bg_top">
            <a href="javascript:void(0)"
               onclick="hide_suggestion_box('fine_print','Fine print (if any)','print_advise')" id="print_close"><img
                    src="<?php echo base_url() ?>public/images/cancel-popup.png"/></a>
        </div>
        <!--End of popup_bg_top-->
        <div class="popup_bg_main">
            Fine print (if any) text coming soon.
        </div>
    </div>
</div>


<!--Start of nav Section-->
<?php $this->load->view('templates/header'); ?>
<!--End of nav_selected Section-->

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top'); ?>
<!--End of customer top common  Section-->

<!--Start of customer Area-->

<div class="merchant_main_con">
<div class="merchant_main">

<?php $this->load->view('templates/merchant_main_left'); ?>



<?php echo form_open(base_url() . "merchantaccount/addoffers/", array('id' => 'add_offer_frm')) ?>
<input type="hidden" name="display_status" id="display_status" value="">
<input type="hidden" name="offer_id" id="offer_id"
       value="<?php if (isset($offer_detail[0]->id) && !isset($offer_action_rerun)) {
           echo stripslashes($offer_detail[0]->id);
       } ?>">

<input type="hidden" name="offer_rerun_id" id="offer_rerun_id"
       value="<?php if (isset($offer_rerun_id) && $offer_rerun_id != "") {
           echo $offer_rerun_id;
       } ?>">

<div class="merchant_main_right">
    <div class="heading">
        <h1>Add new offer</h1>
    </div>

    <div class="sub_heading">
        <h3>Please add your offer. Fields marked (*) are mandatory.</h3>
    </div>





    <div class="merchant_newoffer">

        <div class="valid_errors">
            <?php
            echo validation_errors();
            echo $this->session->flashdata('add_offer_message');
            ?>
        </div>

        <h2><img src="<?php echo base_url() ?>public/images/offer_star.png" alt=""/> Offer 1</h2>
        <ul>


            <li>

                <div class="container"> <!-- offer_detail -->
                    <input type="text" value="<?php if (isset($offer_detail[0]->offer_title)) {
                        echo stripslashes($offer_detail[0]->offer_title);
                    } else {
                        echo set_value('offer_title');
                    } ?>" name="offer_title" id="offer_title" class="title" Placeholder="*Title"/>

                    <p>(eg. Pizza and wine. Maximum 26 characters)</p>
                </div>
                <div class="container">
                    <input type="text" value="<?php if (isset($offer_detail[0]->price)) {
                        echo '$' . stripslashes($offer_detail[0]->price);
                    } else {
                        echo set_value('price');
                    } ?>" name="price" id="price" class="price" Placeholder="*Price" onkeydown="add_dollar(this.value)"/>
                    <input type="text" value="<?php if (isset($offer_detail[0]->price_normally)) {
                        echo '$' . stripslashes($offer_detail[0]->price_normally);
                    } else {
                        echo set_value('price_normally');
                    } ?>" name="price_normally" id="price_normally" class="normally" Placeholder="*Normally"
                           onkeydown="add_dollar2(this.value)"/>

                    <p>(value)</p>
                </div>
                <div class="container">

                    <textarea name="offer_description" id="offer_description" class="short_description"
                              Placeholder="Short description of offer"><?php if (isset($offer_detail[0]->offer_description)) {
                            echo stripslashes($offer_detail[0]->offer_description);
                        } else {
                            echo set_value('offer_description');
                        } ?></textarea>

                    <div class="question_mark" id="ques_1"><a href="javascript:void(0)"
                                                              onmouseover="display_suggestion_box('offer_description','description_advise')"><img
                                src="<?php echo base_url() ?>public/images/ques.png" alt="Short description of offer"
                                title="Short description of offer"/></a>

                    </div>


                </div>
                <div class="container">
                    <div class="question_mark"><a href="javascript:void(0)"
                                                  onmouseover="display_suggestion_box('fine_print','print_advise')"><img
                                src="<?php echo base_url() ?>public/images/ques.png" alt=""/></a></div>
                    <input type="text" name="fine_print" id="fine_print" value="<?php if (isset($offer_detail[0]->fine_print)) {
                        echo stripslashes($offer_detail[0]->fine_print);
                    } else {
                        echo set_value('fine_print');
                    } ?>" class="find_print" placeholder="Fine print (if any)"/>
                </div>

                <?php
                $offer_image_exist_or_not = 0;
                if (isset($offer_detail[0]->offer_image) && isset($offer_detail[0]->offer_image) != "") {

                    if (file_exists($offer_detail[0]->offer_image)) {
                        $offer_image_exist_or_not = 1;
                    }
                }
                ?>

                <!-- UPLOAD OFFER IMAGE START -->
                <div class="container">
                    <iframe height="125" frameborder="0" width="100%" scrolling="no"
                            src="<?php echo $this->config->item('base_url'); ?>image_offer.php?offer_id=<?php if (isset($offer_detail[0]->id) && !isset($offer_action_rerun)) {
                                echo stripslashes($offer_detail[0]->id);
                            } ?>"
                            id="iframe_id" <?php if ($offer_image_exist_or_not == 1) { ?> class="add_offer_image" <?php } ?> ></iframe>
                    <input type="hidden" name="temp_offer_picture" id="temp_offer_picture" value=""/>
                    <input type="hidden" id="old_offer_picture" name="old_offer_picture"
                           value="<?php if (isset($offer_detail[0]->offer_image)) {
                               echo stripslashes($offer_detail[0]->offer_image);
                           } ?>"/>
                </div>
                <!-- UPLOAD OFFER IMAGE END -->

            </li>




            <?php
            $is_running_today   = isset($offer_detail[0]->offer_running_today) && (int)$offer_detail[0]->offer_running_today;
            $is_multiple_dates  = isset($offer_detail[0]->offer_multiple_dates) && (int)$offer_detail[0]->offer_multiple_dates;

            $offer_running_from = isset($offer_detail[0]->offer_running_from) ? $this->users->get_date_dmy($offer_detail[0]->offer_running_from) : '';
            $offer_running_to   = isset($offer_detail[0]->offer_running_to) ? $this->users->get_date_dmy($offer_detail[0]->offer_running_to) : '';


            // different days counting end
            if (isset($offer_action_rerun) && (int)$offer_action_rerun) {
                // When offer re-run
                $offer_running_from = date('d/m/Y');
                $offer_running_to   = '';
            }

            ?>

            <li>
                <h4>Offer running (dates):</h4>


                <div class="container" id="offer_running_today_div">
                    <input name="offer_running_today" id="offer_running_today" type="checkbox"<?php echo $is_running_today ? ' checked="checked"' : ''; ?>>

                    <p><span>One day only</span><br/> e.g. Today</p>
                </div>


                <div class="container" id="offer_multiple_dt_container_div">
                    <input name="offer_multiple_dates" id="offer_multiple_dates" type="checkbox"<?php echo $is_multiple_dates ? ' checked="checked"' : ''; ?>>

                    <p><span>Multiple dates</span><br> e.g. for a month or a few days</p>
                </div>


                <div id="multidate_from_to_div" style="<?php echo $is_multiple_dates || $is_running_today ? 'display:block;' : 'display:none;'; ?>">
                    <div class="container" id="offer_running_from_container" style="<?php echo $is_multiple_dates || $is_running_today ? 'display:block;' : 'display:none;'; ?>">

                        <span class="date_con">
                            <input type="text" name="offer_running_from" id="offer_running_from" size="30" value="<?php echo $offer_running_from; ?>" />
                        </span>

                    </div>

                    <div class="container" id="offer_running_to_container" style="<?php echo $is_multiple_dates ? 'display:block;' : 'display:none;'; ?>">
                        <div class="same_width"><p class="">to</p></div>
                        <span class="date_con">
                            <input type="text" name="offer_running_to" id="offer_running_to" size="30" value="<?= $offer_running_to ?>" />
                        </span>
                    </div>
                </div>

            </li>







            <li id="what_day_of_week_li" style="<?php echo $is_multiple_dates ? 'display:block;' : 'display:none;'; ?>">
                <h4>What day of the week?</h4>


                <?php
                $everyday_of_week       = isset($offer_detail[0]->everyday_of_week) && (int)$offer_detail[0]->everyday_of_week;
                $select_specific_days   = isset($offer_detail[0]->select_specific_days) && (int)$offer_detail[0]->select_specific_days;
                $hide_everyday_of_week  = true; // I have no idea why this was done..
                ?>

                <input type="hidden" name="everyday_of_week" id="everyday_of_week" value=""<?php echo $everyday_of_week && !isset($offer_rerun_id) ? " checked='checked'" : ""; ?>>
                <input type="hidden" name="select_specific_days" id="select_specific_days" value="" <?php echo $select_specific_days && !isset($offer_rerun_id) ? " checked='checked'" : ""; ?>>

                <div class="container" id="container_daywise" style="display: block;">
                    <?php

                    $specific_days_week     = isset($offer_detail[0]->specific_days_week) && $offer_detail[0]->specific_days_week;
                    $specific_days_week_arr = explode(",", strtolower($specific_days_week));
                    foreach($specific_days_week_arr as $key => $val)
                        $specific_days_week_arr[$key] = trim($val);



                    $p = 1;
                    foreach ($week_days as $val) {
                        $specific_days_exist = count($days_between_multiple_dates_array) > 0 && in_array($val[1], $days_between_multiple_dates_array) ? 1 : 0;
                        ?>

                        <span class="daywise" id="daywise_<?= $p ?>">
                            <input class="specific_days_week_class" name="specific_days_week[]" id="specific_days_week_<?= $val[1] ?>" type="checkbox" value="<?= $val[1] ?>" <?php echo in_array($val[1], $specific_days_week_arr) ? ' checked="checked"' : ''; ?>>

                            <p><span><?= $val[0] ?></span></p>
                        </span>

                        <?php
                        $p++;
                    }
                    ?>
                </div>
            </li>




            <?php

            $all_day_same_time              = isset($offer_detail[0]->all_day_same_time) && (int)$offer_detail[0]->all_day_same_time;
            $different_days_different_time  = isset($offer_detail[0]->different_days_different_time) && (int)$offer_detail[0]->different_days_different_time;
            $all_opening_hours              = isset($offer_detail[0]->all_opening_hours) && (int)$offer_detail[0]->all_opening_hours;


            $hide = false;
            if($all_day_same_time || $different_days_different_time || $all_opening_hours) {
                if(isset($offer_rerun_id) && $offer_rerun_id != "" && $is_multiple_dates) {
                    $hide = true;
                }

            } else {
                $hide = true;
            }

            ?>



            <li id="available_time_slots_and_capacity_li" style="<?php echo $hide ? "display:none;" : ""; ?>">
                <h4>Available time slots and capacity:</h4>


                <div class="main_con" id="main_con_diff_days_time" style="<?php echo $different_days_different_time ? "display:block;" : ""; ?>">

                    <div id="all_diff_days_div" style="<?php echo $different_days_different_time && !isset($offer_rerun_id) ? "display:block;" : "display:none;"; ?>">
                        <input type="hidden" name="count_different_days_data" id="count_different_days_data" value="<?= $count_different_days_data ?>">


                        <?php
                        $moat_edt_id = '';
                        $diff_days_id_array = array();

                        if(isset($different_days_data) && count($different_days_data) > 0) {
                            $m = 1;
                            $l = 0;

                            foreach ($different_days_data as $different_days_val) {
                                $l++;

                                $moat_edt_id = $different_days_val->id;
                                array_push($diff_days_id_array, $moat_edt_id);

                                $available_day = $different_days_val->available_day;

                                $time_from = $different_days_val->time_from;
                                $time_from_am_pm = $different_days_val->time_from_am_pm;

                                $time_to = $different_days_val->time_to;
                                $time_to_am_pm = $different_days_val->time_to_am_pm;

                                $quantity = $different_days_val->quantity;
                                $max_pax = $different_days_val->max_pax;

                                $dayname = str_replace(
                                    array('monday','tuesday','wednesday','thursday','friday','saturday','sunday'),
                                    array('Mon','Tue','Wed','Thu','Fri','Sat','Sun'),
                                    $available_day);


                                ?>


                                <div class="container" id="diff_days_div_id_edt_<?= $moat_edt_id ?>" style="display:block;">

                                    <div class="all_bg">

                                        <input type="hidden" name="available_day_name_edt_<?= $moat_edt_id ?>" id="available_day_name_edt_<?= $moat_edt_id ?>" value="<?= $available_day ?>"/>
                                        <input type="hidden" name="moat_edt_id_<?= $m ?>" id="moat_edt_id_<?= $m ?>" value="<?= $moat_edt_id ?>">


                                        <div class="days_drop" id="available_day_edt_option_<?= $m ?>" style="display:block;">
                                            <select name="available_day_edt_<?= $moat_edt_id ?>" id="available_day_edt_<?= $moat_edt_id ?>" class="revostyled available_day_edt_view_class_<?= $l ?>">
                                                <?php foreach ($week_days as $val) { ?>

                                                    <option value="<?php echo $val[1]; ?>" <?php echo isset($available_day) && $val[1] == $available_day ? " selected='selected'" : ""; ?>><?php echo $val[0]; ?></option>

                                                <?php } ?>
                                            </select>
                                        </div>

                                    </div>


                                    <span class="from_date">
                                        <div class="from_dd">
                                            <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_edt_' . $moat_edt_id, 'revostyled time_from_edt_dropdown_' . $l, $time_from); ?>
                                        </div>

                                        <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_edt_am_pm_' . $moat_edt_id, 'revostyled', $time_from_am_pm); ?>
                                    </span>


                                    <span class="to_date">
                                        <div class="from_dd">
                                            <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_edt_' . $moat_edt_id, 'revostyled time_to_edt_dropdown_' . $l, $time_to, 'to'); ?>
                                        </div>

                                        <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_edt_am_pm_' . $moat_edt_id, 'revostyled', $time_to_am_pm); ?>
                                    </span>


                                    <span class="qty">
                                        <p class="large">Qty</p>

                                        <input type="text" name="quantity_edt_<?= $moat_edt_id ?>" id="quantity_edt_<?= $moat_edt_id ?>" value="<?= $quantity ?>" class="qty_val quantity_edt_<?= $l ?>"/>
                                    </span>


                                    <span class="max_pax" style="<?php echo $max_pax_display_check == 1 ? "display:block;" : "display:none;"; ?>">
                                        <p class="large2">Max <br/>pax</p>

                                        <input name="max_pax_edt_<?= $moat_edt_id ?>" id="max_pax_edt_<?= $moat_edt_id ?>" type="text" value="<?php if ($max_pax != "0") { echo $max_pax; } ?>" placeholder="-" class="max max_pax_edt_<?= $l ?>"/>
                                    </span>

                                </div>


                                <?php
                                $m++;
                            }
                        }

                        $diff_days_id_str = "";
                        if (count($diff_days_id_array) > 0) {
                            $diff_days_id_str = implode("|", $diff_days_id_array);
                        }



                        ?>

                        <input type="hidden" name="all_different_days_data_ids" id="all_different_days_data_ids" value="<?= $diff_days_id_str ?>">

                        <div class="container" id="1_diff_days_div_id" style="display:block;">

                            <div class="all_bg">
                                <?php $i = 1; ?>

                                <div class="days_drop" id="available_day_option_1" style="display:block;">

                                    <select name="available_day_1" id="available_day_1" class="revostyled timeslotDay">
                                        <option value="">Day</option>
                                        <?php foreach ($week_days as $val) { ?>
                                            <option value="<?php echo $val[1]; ?>"><?php echo $val[0]; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>


                            </div>


                            <span class="from_date">
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_1', 'revostyled time_from_dropdown_1', ''); ?>
                                </div>

                                <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_1', 'revostyled'); ?>
                            </span>


                            <span class="to_date">
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_1', 'revostyled time_to_dropdown_1', '', 'to'); ?>
                                </div>

                                <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_1', 'revostyled'); ?>
                            </span>


                            <span class="qty">
                                <p class="large">Qty</p>

                                <input type="text" name="quantity_1" id="quantity_1" value="" class="qty_val"/>
                            </span>


                            <span class="max_pax" style="<?php echo $max_pax_display_check == 1 ? "display:block;" : "display:none;"; ?>">
                                <p class="large2">Max <br/>pax</p>

                                <input name="max_pax_1" id="max_pax_1" type="text" value="" placeholder="-" class="max"/>
                            </span>

                        </div>




                        <?php
                        $display_style = '';
                        $z = 1;
                        for ($i = 2; $i <= 21; $i++) {

                            if ($i <= 3) {
                                $display_style = 'style="display:block;"'; // for edit mode

                            } else {

                                $display_style = 'style="display:none;"';
                            }

                            ?>

                            <div class="container" id="<?= $i ?>_diff_days_div_id" <?= $display_style ?> >

                                <div class="all_bg">
                                    <div class="days_drop" id="available_day_option_<?= $i ?>" style="display:block;">

                                        <select name="available_day_<?= $i ?>" id="available_day_<?= $i ?>" class="revostyled">
                                            <option value="">Day</option>
                                            <?php foreach ($week_days as $val) { ?>
                                                <option value="<?php echo $val[1]; ?>"><?php echo $val[0]; ?></option>
                                            <?php } ?>
                                        </select>

                                    </div>
                                </div>


                                <span class="from_date">
                                    <div class="from_dd">
                                        <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_' . $i, 'revostyled time_from_dropdown_' . $i, ''); ?>
                                    </div>

                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_' . $i, 'revostyled'); ?>
                                </span>


                                <span class="to_date">
                                    <div class="from_dd">
                                        <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_' . $i, 'revostyled time_to_dropdown_' . $i, '', 'to'); ?>
                                    </div>

                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_' . $i, 'revostyled'); ?>
                                </span>


                                <span class="qty">
                                    <p class="large">Qty</p>

                                    <input type="text" name="quantity_<?= $i ?>" id="quantity_<?= $i ?>" value="" class="qty_val"/>
                                </span>


                                <span class="max_pax" style="<?php echo $max_pax_display_check == 1 ? "display:block;" : "display:none;"; ?>">
                                    <p class="large2">Max <br/>pax</p>

                                    <input name="max_pax_<?= $i ?>" id="max_pax_<?= $i ?>" type="text" value="" placeholder="-" class="max"/>
                                </span>

                            </div>

                            <?php
                            $z++;
                        }
                        ?>


                        <div class="container" id="add_time_slot">
                            <span class="add_time_slot"><a href="javascript:void(0)" onclick="showhide_different_day()">+ add time slot</a></span>
                        </div>


                    </div>
                </div>

            </li>

        </ul>




        <div class="save_bg">
            <span class="left_btn">
                <a href="javascript:void(0)" onclick="submit_offer_form('publish_offer')"><img src="<?php echo base_url() ?>public/images/publish_offer.jpg" alt=""/></a>
                <p><a href="javascript:void(0)" onclick="submit_offer_form('save_draft')">Save draft</a></p>
            </span>

            <span class="upper"><a href="javascript:void" id="up"><img src="<?php echo base_url() ?>public/images/orenge_up.png" alt=""/></a></span>
        </div>


        <div class="save_bg">
            <span class="trash" style="<?php echo isset($offer_detail[0]->id) ? "" : "display:none;"; ?>">
                <a href="<?php echo base_url() ?>merchantaccount/trashoffer/<?php if (isset($offer_detail[0]->id)) { echo stripslashes($offer_detail[0]->id); } ?>">
                    <img src="<?php echo base_url() ?>public/images/trash.png" alt=""/>
                    <p>Trash offer</p>
                </a>
            </span>
        </div>



    </div>


    <div class="save_bg">
        <span class="add_more">
            <a href="javascript:void(0)" onclick="submit_offer_form('save_add_another_offer')">
                <img src="<?php echo base_url() ?>public/images/add_another_offer.jpg" alt=""/>
            </a>
        </span>
    </div>


</div>
<?php echo form_close() ?>


</div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer'); ?>
<!--End of footer_bg-->

<!--FOR DATE PICKER-->

<? //=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>

<script type="text/javascript"
        src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
        src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $(".revostyled").selectbox();

    });

</script>

<link rel="stylesheet" href="<?= base_url('/public/assets/ui/themes/base/jquery.ui.all.css'); ?>">
<script src="<?= base_url('/public/assets/ui/jquery.ui.core.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.widget.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.effect.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-blind.js'); ?>"></script>

<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-bounce.js'); ?>"></script>

<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-clip.js'); ?>"></script>

<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-drop.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-fold.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.effect-slide.js'); ?>"></script>
<script src="<?= base_url('/public/assets/ui/jquery.ui.datepicker.js'); ?>"></script>
<link rel="stylesheet" href="<?= base_url('/public/assets/ui/demos.css'); ?>">

<!--FOR LOGIN CHECKBOX-->
<link rel="stylesheet" href="<?= base_url('/public/assets/radio/jquery.checkbox.css'); ?>"/>
<link rel="stylesheet" href="<?= base_url('/public/assets/radio/jquery.safari-checkbox.css'); ?>"/>
<script type="text/javascript" src="<?= base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<link href="<?= base_url('/public/assets/gender/style.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?= base_url('/public/assets/gender/jquery-ui.js'); ?>" type="text/javascript"></script>

<!--Price Selector content-->
<link href="<?= base_url('/public/assets/gender/jquery-ui.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?= base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<script type="text/javascript"> var baseUrl = '<?php echo base_url() ?>'; </script>
<script src="<?= base_url('/public/assets/js/pages/addoffers.js'); ?>"></script>




<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>
<?php if (isset($offer_detail[0]->id) && !isset($offer_action_rerun)) { ?>
    <script>
        show_specific_days_select_dropdown();
    </script>
<?php } ?>

</body>
</html>