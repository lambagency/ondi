<?php
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />

</head>

<body>	
<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">

   <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
	  <?php
	  $user_id = $this->users->get_main_merchant_id();
	  $user_steps_completed = $this->users->user_steps_completed($user_id);
	  if($user_steps_completed<3)
	  {
	  ?>	  
	  <div class="progress_bar_bg">	  
		<div class="progress_bar">
		  <ul>
			<li class="pink"></li>
			<li class="blue"></li>
			<li class="orng"></li>
		  </ul>
		  <div class="green_pointer3"><img alt="" src="<?php echo base_url() ?>public/images/check_green2.png"></div>
		</div>
		<div class="step_number3">
		  <p>3</p>
		</div>
	  </div>
	  <?php
	  }
	  ?>

      <div class="heading">
        <h1>Bank Account Information</h1>
      </div>
      <div class="sub_heading">
        <h3>Fields marked (*) are mandatory.</h3>
      </div>
     
	<?php echo form_open(base_url()."merchantaccount/bankingdetails/")?>
	<input type="hidden" name="bank_detail_action" value="1">
    <div class="merchant_contact">
        <ul>
			
			<li>
				<?php echo validation_errors(); ?>				
			</li>

            <li><input type="text" value="<?php echo set_value('branch_name', isset($user_data[0]->branch_name)?$user_data[0]->branch_name : ''); ?>" name="branch_name" id="branch_name" class="input_box" placeholder="*Branch name" /></li>
			
			<li><input type="text" class="input_box" name="account_name" id="account_name" value="<?php echo set_value('account_name', isset($user_data[0]->account_name)?$user_data[0]->account_name : ''); ?>" placeholder="*Account name" /></li>

            <li><input type="text" class="input_box" name="bsb" id="bsb" value="<?php echo set_value('bsb', isset($user_data[0]->bsb)?$user_data[0]->bsb : ''); ?>" placeholder="*BSB" /></li>


			<li><input type="text" class="input_box" name="account_number" id="account_number" value="<?php echo set_value('account_number', isset($user_data[0]->account_number)?$user_data[0]->account_number : ''); ?>" placeholder="*Account Number" /></li>

            <li><input type="text" class="input_box" name="abn" id="abn" value="<?=$user_data[0]->abn?>" placeholder="*ABN" /></li>			
			
        </ul>
    </div>    
    <div class="save_bg"> 
		<span class="save">
			<?php 
			$session_user_data = $this->session->all_userdata();	
			$user_steps_completed = $this->users->user_steps_completed($session_user_data['logged_user']);
			if($user_steps_completed<3){?>
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/user_save.jpg">
			<?php } else { ?>
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/m_finish.jpg">
			<?php } ?>
			
		</span>  
	</div>
	<?php echo form_close()?>


    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script src="assets/check/jquery.screwdefaultbuttonsV2.js"></script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>

</html>
