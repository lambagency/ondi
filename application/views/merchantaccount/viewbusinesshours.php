<?php
$max_pax_display_check = 0;
if(isset($merchant_business_type))
{	
	if(in_array('1',$merchant_business_type))
	{
		$max_pax_display_check = 1;
	}

}

if(isset($merchant_opening_hours) && count($merchant_opening_hours)>0)
{
	
	foreach($merchant_opening_hours as $opening_hours_val)
	{
		
		$available_day_open_hrs = $opening_hours_val->day;

		$time_from_open_hrs = $opening_hours_val->from_hour;		
		$time_from_am_pm_open_hrs = $opening_hours_val->from_ampm;

		$time_to_open_hrs = $opening_hours_val->to_hour;
		$time_to_am_pm_open_hrs = $opening_hours_val->to_ampm;
		
		$quantity_open_hrs="";
		$max_pax_open_hrs="";

		if($available_day_open_hrs=='monday'){ $dayname = str_replace("monday","Mon",$available_day_open_hrs); }			
		if($available_day_open_hrs=='tuesday'){ $dayname = str_replace("tuesday","Tue",$available_day_open_hrs); }
		if($available_day_open_hrs=='wednesday'){ $dayname = str_replace("wednesday","Wed",$available_day_open_hrs); }
		if($available_day_open_hrs=='thursday'){ $dayname = str_replace("thursday","Thu",$available_day_open_hrs); }
		if($available_day_open_hrs=='friday'){ $dayname = str_replace("friday","Fri",$available_day_open_hrs); }
		if($available_day_open_hrs=='saturday'){ $dayname = str_replace("saturday","Sat",$available_day_open_hrs); }
		if($available_day_open_hrs=='sunday'){ $dayname = str_replace("sunday","Sun",$available_day_open_hrs); }

		$moh_id = $opening_hours_val->id; // merchant opening hours id

		if($available_day_open_hrs!="")
		{
?>
<div class="container <?=$available_day_open_hrs?>_open_hrs_div_id" style="display:block;">
		<div class="all_bg"> 
			<input type="hidden" name="available_day_open_hrs_hd_<?=$moh_id?>" id="available_day_open_hrs_hd_<?=$moh_id?>" value="<?=$available_day_open_hrs?>" readonly class="all" />
			<input type="text" name="available_day_open_hrs_<?=$moh_id?>" id="available_day_open_hrs_<?=$moh_id?>" value="<?=$dayname?>" readonly class="all <?=$available_day_open_hrs?>_available_day_open_hrs" />
		</div>
		<span class="from_date">
		<!--<p class="large">from</p>-->		
        <div class="from_dd">	
		<?php //echo $time_dropdown = $this->users->get_time_dropdown('time_from_open_hrs_'.$moh_id,'revostyled '.$available_day_open_hrs.'_from_dropdown' ,$time_from_open_hrs); ?>

		<input type="text" name="" id="" value="<?=$time_from_open_hrs?>" readonly class="all" />
		<input type="hidden" name="time_from_open_hrs_<?=$moh_id?>" id="time_from_open_hrs_<?=$moh_id?>" value="<?=$time_from_open_hrs?>" readonly class="all <?php echo $available_day_open_hrs ?>_from_dropdown" />

        </div>
		<?php //echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_open_hrs_'.$moh_id,'revostyled 	',$time_from_am_pm_open_hrs); ?>
		
		<input type="text" name="" id="" value="<?=$time_from_am_pm_open_hrs?>" readonly class="all" />
		<input type="hidden" name="time_from_am_pm_open_hrs_<?=$moh_id?>" id="time_from_am_pm_open_hrs_<?=$moh_id?>" value="<?=$time_from_am_pm_open_hrs?>" readonly class="all" />
		
		</span>
		<span class="to_date"><!--<p class="large">to</p>-->		
        <div class="from_dd">
		<?php //echo $time_dropdown = $this->users->get_time_dropdown('time_to_open_hrs_'.$moh_id,'revostyled '.$available_day_open_hrs.'_to_dropdown',$time_to_open_hrs,'to'); ?>
        
		<input type="text" name="" id="" value="<?=$time_to_open_hrs?>" readonly class="all" />
		<input type="hidden" name="time_to_open_hrs_<?=$moh_id?>" id="time_to_open_hrs_<?=$moh_id?>" value="<?=$time_to_open_hrs?>" readonly class="all <?=$available_day_open_hrs?>_to_dropdown" />
		
		</div>
		<?php //echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_open_hrs_'.$moh_id,'revostyled ',$time_to_am_pm_open_hrs); ?>
		
		<input type="text" name="" id="" value="<?=$time_to_am_pm_open_hrs?>" readonly class="all" />
		<input type="hidden" name="time_to_am_pm_open_hrs_<?=$moh_id?>" id="time_to_am_pm_open_hrs_<?=$moh_id?>" value="<?=$time_to_am_pm_open_hrs?>" readonly class="all" />
		
		</span>
		<span class="qty"><p class="large">Qty</p><input type="text" name="quantity_open_hrs_<?=$moh_id?>" id="quantity_open_hrs_<?=$moh_id?>" value="<?=$quantity_open_hrs?>" placeholder="1" class="qty_val <?=$available_day_open_hrs?>_qty_val_open_hrs" /></span>
		<span class="max_pax" <?php if($max_pax_display_check==1){ ?> style="display:block;" <?php }else{ ?> style="display:none;" <?php } ?> ><p class="large2">Max <br/>pax</p><input name="max_pax_open_hrs_<?=$moh_id?>" id="max_pax_open_hrs_<?=$moh_id?>" type="text" value="<?=$max_pax_open_hrs?>" placeholder="-" class="max <?=$available_day_open_hrs?>_max_open_hrs" /></span>
	</div>
<?php 
				}
			}
		}
?>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		
	});

</script>
