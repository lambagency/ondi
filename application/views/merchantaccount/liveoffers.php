<?php
/*
function show_offer_display_status($sts="")
{
	if($sts=='1')
	{
		return 'Approved';
	}
	else if($sts=='2')
	{
		return 'Draft';
	}
	else if($sts=='3')
	{
		return 'Preview';
	}
	else if($sts=='4')
	{
		return 'Pending from Admin Approval';
	}
}
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<style>
.alert_message{color:#FF0000 !important; }
</style>

<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />


</head>
<body>

<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
	<?php $this->load->view('templates/merchant_main_left');?>
	<div class="merchant_main_right">      
      <div class="heading">
        <h1>Live offers</h1>
      </div>
      <div class="sub_heading">
        <h3>See all of your offers that are currently live and available, or search for a specific offer.</h3>
      </div>
      
    	<div class="merchant_liveoffer">
					
					<div class="valid_success">
					<?php  echo $this->session->flashdata('add_offer_message'); ?>
					</div>

                	<div class="filter_box">						
                        
						<?php echo form_open(base_url()."merchantaccount/liveoffers", array('id'=>'liveoffers_srch_frm'))?>
						<div class="search_box">
                        	<input type="button" class="search_btn"/>
                        	<input type="text" name="offer_search" id="offer_search" value="<?php if(isset($offer_search)){ echo $offer_search; } ?>" class="search" placeholder="Search" />
							
							<?php if(isset($offer_search) && $offer_search!=""){ ?><div class="cross"><a href="<?php echo base_url("merchantaccount/liveoffers/"); ?>">X</a></div><?php } ?>

						</div>
						<?php echo form_close()?>


                        <div class="expand"><a href="javascript:void(0)" onclick="show_hide_all_offers('expand')">Expand all</a> | <a href="javascript:void(0)" onclick="show_hide_all_offers('collapse')">Collapse all</a></div>
                    </div>
                    
                    
                    <?php if (!$this->agent->is_mobile())
					{
					?>
                    
                    
                    
                     <div class="head">
                	<table width="595" border="1" class="table_customer">
                      <tr>
                        <th align="left" scope="col" width="145">Offer</th>
                        <th align="left" scope="col" width="140">Date/s available</th>
                        <th align="left" scope="col" width="40">Consumption</th>
                        <th align="left" scope="col" width="70" class="no_bg">More</th>
                      </tr>
                      </table>
                      </div>
                      
                      
                      <div class="detail">
                          <table width="595" border="1" class="table_customer">	
						  
						  <?php
							  $offer_id_arr = array();
							 
							  foreach($offers_data as $offers_val){								  
								  
								  array_push($offer_id_arr,$offers_val->id);

								  $consumption = 0;
								  //$consumption=round($offers_val->consumption);

								  $total_number_of_people = $offers_val->total_number_of_people; // total number of people booked. Coming from order table.
								  if($total_number_of_people=="")
								  {
									$total_number_of_people=0;
								  }								  
								  $total_quantity = $offers_val->total_quantity;
								  if($total_quantity=='')
								  {
									$total_quantity=0;
								  }
								  $consumption = round(($total_number_of_people/$total_quantity)*100);

								  //print_r($offers_val);
							?>
						  
						  <tr class="new_col_blank">
							<td colspan="4"><div class="blank_div">&nbsp;</div></td>
                          </tr>
                          <tr>
                            <td align="left" scope="col" width="155" class="first">
								<strong><?php echo stripslashes($offers_val->offer_title); ?> $<?php echo stripslashes($offers_val->price); ?></strong>
							</td>
                            <td align="left" scope="col" width="145">								
								<?php
								 if($offers_val->offer_multiple_dates=='1' && $offers_val->offer_running_from!='0000-00-00' && $offers_val->offer_running_to!='0000-00-00')
								 {
										echo date('D d/m/y',strtotime($offers_val->offer_running_from));
										echo ' To ';
										echo date('D d/m/y',strtotime($offers_val->offer_running_to));

								  }else if($offers_val->offer_running_today!='0000-00-00' && $offers_val->offer_multiple_dates!='1')
								  {																														
										echo date('D d/m/y',strtotime($offers_val->offer_running_from));
								  }
								?>
							</td>
                            <td align="center" scope="col" width="80">
							<span class="big_text"><?php echo $consumption; ?>%</span>
							<br/><em>(<?php echo $total_number_of_people; ?>/<?=$total_quantity?>)</em></td>
                            <td align="left" scope="col" width="90">
                            <div class="buttons">
                            
							<?php 
							  //$offer_display_status = show_offer_display_status($offers_val->offer_display_status);

							  $offer_display_status = $offers_val->offer_display_status;
							  
							  if($offer_display_status=='1')
							  {									 							  							
							?>
							
							<span class="share">
                                <span class="nav_social">

                                    <a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($offers_val->id)?>&t=<?=urlencode($offers_val->offer_title)?>"><img src="<?php echo base_url() ?>public/images/cus_fb.png" alt="" /></a>

                                    <a  target="_blank" href="https://twitter.com/home?status=<?=urlencode($offers_val->offer_title)?>%20-%20<?=$this->offers->get_offer_url($offers_val->id)?>"><img src="<?php echo base_url() ?>public/images/cus_twitter.png" alt="" /></a>

                                    <a  target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($offers_val->id)?>"><img src="<?php echo base_url() ?>public/images/cus_g+.png" alt="" /></a>

                                </span>
                            </span>
							<?php }else if($offer_display_status=='2'){ ?>
									<img src="<?php echo base_url() ?>public/images/draft_img.png" alt="" />
							<?php }else if($offer_display_status=='3'){ ?>
									
									<!-- <a href="#"><img src="<?php echo base_url() ?>public/images/draft_img.png" alt="Preview" /></a> -->

							<?php }else if($offer_display_status=='4'){ ?>									
									<a href="#"><img src="<?php echo base_url() ?>public/images/pending.png" alt="" /></a>
							<?php } ?>
							
							<?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
								<a href="<?php echo base_url() ?>merchantaccount/addoffers/<?=$offers_val->id?>"><img src="<?php echo base_url() ?>public/images/edit_img.png" alt="" /></a>
							<?php } ?>
                                
                            
							<?php if($offer_display_status=='2'){ ?>
								
								<!-- <img src="<?php echo base_url() ?>public/images/up_cus_share1.png" class="up_arrow" alt="" id="up_arrow_<?=$offers_val->id?>" /> -->

							<?php }else{ ?>

								<a href="javascript:void(0)" onclick="show_hide_offer_detail('<?=$offers_val->id?>')"><img src="<?php echo base_url() ?>public/images/up_cus_share1.png" class="up_arrow" alt="" id="up_arrow_<?=$offers_val->id?>" /></a>

							<?php } ?>
                            
							
							
							</div>
                            </td>
                          </tr>                          
						  
						  <tr class="new_col" style="display:none;" <?php if($offer_display_status!='2'){ ?> id="offer_detail_tr_<?=$offers_val->id?>" <?php } ?> >
                                <td colspan="4">
                                    <div class="offer_detail">
                                    	<ul>
                                        	<li>
                                            	<div class="consumption alert_message"><p>Consumption:</p> 
                                                <div class="progress"><div class="bar" style="width:<?php echo $consumption; ?>%"></div></div>
                                                <p><span> <?php echo $consumption; ?>% (<?php echo $total_number_of_people; ?>/<?=$total_quantity?>) </span></p></div>
                                                <div class="result_view"><img src="<?php echo base_url() ?>public/images/list_small.png" alt="" /><p>Results viewed: <span><?=$offers_val->results_viewed?></span></p></div>
                                                <div class="detail_view"><img src="<?php echo base_url() ?>public/images/menu_view.png" alt="" /><p>Details viewed: <span><?=$offers_val->details_viewed?></span></p></div>
                                            </li>
                                            <li>
                                            	<div class="text_offer">
                                            
											<p><span>Description // </span><?php echo stripslashes($offers_val->offer_description); ?></p>


                                            	<p><span>Fine print // </span><?php echo stripslashes($offers_val->fine_print); ?></p>
                                                </div>
                                            </li>
                                            <li>
                                            	<div class="run">
                                                	<div class="left"><p>Offer runs:</p></div>
                                                    <div class="right">
														<p>
															<!-- <span>Sunday 01/12/2013</span> to <span>Tuesday 31/12/2013</span> -->
															<?php
															 if($offers_val->offer_multiple_dates=='1' && $offers_val->offer_running_from!='0000-00-00' && $offers_val->offer_running_to!='0000-00-00')
															 {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_from));
																	echo '</span>';
																	echo ' to ';
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_to));
																	echo '</span>';

															  }
															  /*else if($offers_val->offer_running_today!='0000-00-00' && $offers_val->offer_multiple_dates!='1')
															  {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_today));
																	echo '</span>';
															  }*/
															  else if($offers_val->offer_running_today=='1' && $offers_val->offer_multiple_dates!='1')
															  {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_from));
																	echo '</span>';
															  }
															?>
														</p>
													</div>
                                                </div>
                                                <div class="run">
                                                	<div class="left"><p>Days:</p></div>
                                                    <div class="right">
														<p>
															<span>
																<?php 
																	  if($offers_val->everyday_of_week=='1')
																	  { 
																		echo 'Everyday'; 
																	  }
																	  elseif($offers_val->select_specific_days=='1' && $offers_val->specific_days_week!="")
																	  {
																		    $begin=strtotime($offers_val->offer_running_from);
																			$end=strtotime($offers_val->offer_running_to);
																			$array_days = array();
																			$idays = 0;
																			while($begin<=$end)
																			{
																				$what_day = date("l",$begin);
																				array_push($array_days, strtolower($what_day));
																				$idays++;
																				if($idays==7)
																				{
																					break;
																				}
																				$begin+=86400;
																			}
																			$array_specific_days_week = explode(",", $offers_val->specific_days_week);
																			$result_days = array_intersect($array_days, $array_specific_days_week);
																			echo implode(", ", $result_days);
																			//echo $specific_days_week = str_replace(",",", ",$offers_val->specific_days_week);
																	  }
																?>
															</span>
														</p>
													</div>
                                                </div>
                                                
												<?php
													$different_days_data = $this->users->get_different_days_data($offers_val->id);

													/*
													echo '<pre>';
													print_r($different_days_data);
													echo '</pre>';
													*/

													if(count($different_days_data)>0)
													{
												?>												
												<div class="run">
                                                	<div class="left"><p>For these time slots:</p></div>
                                                    
													<div class="right">
													<?php foreach($different_days_data as $diff_days_val){ ?>
													<p><span><?php echo $diff_days_val->available_day; ?> </span><span><?php echo $diff_days_val->time_from; ?><?php echo $diff_days_val->time_from_am_pm; ?> </span>to <span><?php echo $diff_days_val->time_to; ?><?php echo $diff_days_val->time_to_am_pm; ?> </span>qty <span><?php echo $diff_days_val->quantity; ?> </span>max pax <span><?php if($diff_days_val->max_pax>0){ echo $diff_days_val->max_pax;  } ?></span></p>	
													<?php } ?>
													</div>
                                                </div>
												<?php } ?>

                                            </li>
                                            
                                        </ul>
                                    	
                                    </div>
                                </td>
                           </tr>
						   <?php 
								} 
								
								$offer_id_str="";
								if(count($offer_id_arr)>0)
								{
									$offer_id_str=implode("##",$offer_id_arr);
								}
								else{

									echo '<tr><td colspan="4" align="center">No records found!</td></tr>';
								}
							?>

						   <input type="hidden" name="offer_id_str" id="offer_id_str" value="<?=$offer_id_str?>">





                          </table>
                      </div>
                      
                      
                      
                      
                    
                    <?php	
					}
					else
					{
					
					?>
                    
                    
                      
                      <div class="detail table_customer">
                          
						  <?php
							  $offer_id_arr = array();
							 
							  foreach($offers_data as $offers_val){								  
								  
								  array_push($offer_id_arr,$offers_val->id);

								  $consumption = 0;
								  //$consumption=round($offers_val->consumption);

								  $total_number_of_people = $offers_val->total_number_of_people; // total number of people booked. Coming from order table.
								  if($total_number_of_people=="")
								  {
									$total_number_of_people=0;
								  }								  
								  $total_quantity = $offers_val->total_quantity;
								  if($total_quantity=='')
								  {
									$total_quantity=0;
								  }
								  $consumption = round(($total_number_of_people/$total_quantity)*100);

								  //print_r($offers_val);
							?>
						  
						  <div class="live_offer_mobile">
                          
                            <div class="first">
								<strong>
									<?php echo stripslashes($offers_val->offer_title); ?> $<?php echo stripslashes($offers_val->price); ?>
                                </strong>
							</div>
                            
                            <div class="arrow">
                            	<?php if($offer_display_status=='2'){ ?>
								
								<!-- <img src="<?php echo base_url() ?>public/images/up_cus_share1.png" class="up_arrow" alt="" id="up_arrow_<?=$offers_val->id?>" /> -->

								<?php }else{ ?>
    
                                    <a href="javascript:void(0)" onclick="show_hide_offer_detail('<?=$offers_val->id?>')"><img src="<?php echo base_url() ?>public/images/up_cus_share1.png" class="up_arrow" alt="" id="up_arrow_<?=$offers_val->id?>" /></a>
    
                                <?php } ?>
                                
                            </div>
                            
                            
                            
                            
                            <div align="left" scope="col" width="145" class="second">								
								<?php
								 if($offers_val->offer_multiple_dates=='1' && $offers_val->offer_running_from!='0000-00-00' && $offers_val->offer_running_to!='0000-00-00')
								 {
										echo date('D d/m/y',strtotime($offers_val->offer_running_from));
										echo ' To ';
										echo date('D d/m/y',strtotime($offers_val->offer_running_to));

								  }else if($offers_val->offer_running_today!='0000-00-00' && $offers_val->offer_multiple_dates!='1')
								  {																														
										echo date('D d/m/y',strtotime($offers_val->offer_running_from));
								  }
								?>
							</div>
                            <div class="third">
							<span class="big_text"><?php echo $consumption; ?>%</span>
							<br/><em>(<?php echo $total_number_of_people; ?>/<?=$total_quantity?>)</em>
                            </div>
                            
                            <div class="fourth">
                            <div class="buttons">
                            
							<?php 
							  //$offer_display_status = show_offer_display_status($offers_val->offer_display_status);

							  $offer_display_status = $offers_val->offer_display_status;
							  
							  if($offer_display_status=='1')
							  {									 							  							
							?>
							
							<span class="share">
                                <span class="nav_social">

                                    <a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($offers_val->id)?>&t=<?=urlencode($offers_val->offer_title)?>"><img src="<?php echo base_url() ?>public/images/cus_fb.png" alt="" /></a>

                                    <a  target="_blank" href="https://twitter.com/home?status=<?=urlencode($offers_val->offer_title)?>%20-%20<?=$this->offers->get_offer_url($offers_val->id)?>"><img src="<?php echo base_url() ?>public/images/cus_twitter.png" alt="" /></a>

                                    <a  target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($offers_val->id)?>"><img src="<?php echo base_url() ?>public/images/cus_g+.png" alt="" /></a>

                                </span>
                            </span>
							<?php }else if($offer_display_status=='2'){ ?>
									<img src="<?php echo base_url() ?>public/images/draft_img.png" alt="" />
							<?php }else if($offer_display_status=='3'){ ?>
									
									<!-- <a href="#"><img src="<?php echo base_url() ?>public/images/draft_img.png" alt="Preview" /></a> -->

							<?php }else if($offer_display_status=='4'){ ?>									
									<a href="#"><img src="<?php echo base_url() ?>public/images/pending.png" alt="" /></a>
							<?php } ?>
							
							<?php  if($logged_user_access_type=='1' || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
								<a href="<?php echo base_url() ?>merchantaccount/addoffers/<?=$offers_val->id?>"><img src="<?php echo base_url() ?>public/images/edit_img.png" alt="" /></a>
							<?php } ?>
                                
                            
							
                            
							
							
							</div>
                            </div>
                            
                          </div>
                            
                            
                                        
						  
						  <div class="new_col" style="display:none;" <?php if($offer_display_status!='2'){ ?> id="offer_detail_tr_<?=$offers_val->id?>" <?php } ?> >
                                
                                    <div class="offer_detail">
                                    	<ul>
                                        	<li>
                                            	<div class="consumption alert_message"><p>Consumption:</p> 
                                                <div class="progress"><div class="bar" style="width:<?php echo $consumption; ?>%"></div></div>
                                                <p><span> <?php echo $consumption; ?>% (<?php echo $total_number_of_people; ?>/<?=$total_quantity?>) </span></p></div>
                                                <div class="result_view"><img src="<?php echo base_url() ?>public/images/list_small.png" alt="" /><p>Results viewed: <span><?=$offers_val->results_viewed?></span></p></div>
                                                <div class="detail_view"><img src="<?php echo base_url() ?>public/images/menu_view.png" alt="" /><p>Details viewed: <span><?=$offers_val->details_viewed?></span></p></div>
                                            </li>
                                            <li>
                                            	<div class="text_offer">
                                            
											<p><span>Description // </span><?php echo stripslashes($offers_val->offer_description); ?></p>


                                            	<p><span>Fine print // </span><?php echo stripslashes($offers_val->fine_print); ?></p>
                                                </div>
                                            </li>
                                            <li>
                                            	<div class="run">
                                                	<div class="left"><p>Offer runs:</p></div>
                                                    <div class="right">
														<p>
															<!-- <span>Sunday 01/12/2013</span> to <span>Tuesday 31/12/2013</span> -->
															<?php
															 if($offers_val->offer_multiple_dates=='1' && $offers_val->offer_running_from!='0000-00-00' && $offers_val->offer_running_to!='0000-00-00')
															 {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_from));
																	echo '</span>';
																	echo ' to ';
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_to));
																	echo '</span>';

															  }
															  /*else if($offers_val->offer_running_today!='0000-00-00' && $offers_val->offer_multiple_dates!='1')
															  {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_today));
																	echo '</span>';
															  }*/
															  else if($offers_val->offer_running_today=='1' && $offers_val->offer_multiple_dates!='1')
															  {
																	echo '<span>';
																	echo date('D d/m/y',strtotime($offers_val->offer_running_from));
																	echo '</span>';
															  }
															?>
														</p>
													</div>
                                                </div>
                                                <div class="run">
                                                	<div class="left"><p>Days:</p></div>
                                                    <div class="right">
														<p>
															<span>
																<?php 
																	  if($offers_val->everyday_of_week=='1')
																	  { 
																		echo 'Everyday'; 
																	  }
																	  elseif($offers_val->select_specific_days=='1' && $offers_val->specific_days_week!="")
																	  {
																		    $begin=strtotime($offers_val->offer_running_from);
																			$end=strtotime($offers_val->offer_running_to);
																			$array_days = array();
																			$idays = 0;
																			while($begin<=$end)
																			{
																				$what_day = date("l",$begin);
																				array_push($array_days, strtolower($what_day));
																				$idays++;
																				if($idays==7)
																				{
																					break;
																				}
																				$begin+=86400;
																			}
																			$array_specific_days_week = explode(",", $offers_val->specific_days_week);
																			$result_days = array_intersect($array_days, $array_specific_days_week);
																			echo implode(", ", $result_days);
																			//echo $specific_days_week = str_replace(",",", ",$offers_val->specific_days_week);
																	  }
																?>
															</span>
														</p>
													</div>
                                                </div>
                                                
												<?php
													$different_days_data = $this->users->get_different_days_data($offers_val->id);

													/*
													echo '<pre>';
													print_r($different_days_data);
													echo '</pre>';
													*/

													if(count($different_days_data)>0)
													{
												?>												
												<div class="run">
                                                	<div class="left"><p>For these time slots:</p></div>
                                                    
													<div class="right">
													<?php foreach($different_days_data as $diff_days_val){ ?>
													<p><span><?php echo $diff_days_val->available_day; ?> </span><span><?php echo $diff_days_val->time_from; ?><?php echo $diff_days_val->time_from_am_pm; ?> </span>to <span><?php echo $diff_days_val->time_to; ?><?php echo $diff_days_val->time_to_am_pm; ?> </span>qty <span><?php echo $diff_days_val->quantity; ?> </span>max pax <span><?php if($diff_days_val->max_pax>0){ echo $diff_days_val->max_pax;  } ?></span></p>	
													<?php } ?>
													</div>
                                                </div>
												<?php } ?>

                                            </li>
                                            
                                        </ul>
                                    	
                                    </div>
                                
                           </div>
						   <?php 
								} 
								
								$offer_id_str="";
								if(count($offer_id_arr)>0)
								{
									$offer_id_str=implode("##",$offer_id_arr);
								}
								else{

									echo '<div>No records found!</div>';
								}
							?>

						   <input type="hidden" name="offer_id_str" id="offer_id_str" value="<?=$offer_id_str?>">





                        
                      </div>
                      
                    
                     <?php
					}
					
					?>
					
                	
                      
                      
                     
                </div>

    			<div class="save_bg">
                	<?php  if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("5",$logged_user_access_sections)){ ?>
						<span class="add_offer_btn"><a href="<?php echo base_url() ?>merchantaccount/addoffers"><img src="<?php echo base_url() ?>public/images/add_offer.png" alt="" /></a></span>
					<?php } ?>
                        <span class="upper"><a href="javascript:void" id="up"><img src="<?php echo base_url() ?>public/images/orenge_up.png" alt="" /></a></span>
               </div>
	</div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->


<script type="text/javascript">
function show_hide_offer_detail(offer_id)
{		
	
	var offer_id_str=document.getElementById("offer_id_str").value;
	var offer_id_arr = offer_id_str.split("##");		
	for(i=0;i<offer_id_arr.length;i++)
	{				
		if(document.getElementById("offer_detail_tr_"+offer_id_arr[i]))
		{

			if(document.getElementById("offer_detail_tr_"+offer_id_arr[i]).style.display=="" && offer_id!=offer_id_arr[i])
			{
				document.getElementById("offer_detail_tr_"+offer_id_arr[i]).style.display="none";
			}

		}	
		
	}
	
	if(document.getElementById("offer_detail_tr_"+offer_id))
	{

		if(document.getElementById("offer_detail_tr_"+offer_id).style.display=="none")
		{
			document.getElementById("offer_detail_tr_"+offer_id).style.display="";
			document.getElementById("up_arrow_"+offer_id).src="<?php echo base_url() ?>public/images/cus_share1.png";
		}
		else if(document.getElementById("offer_detail_tr_"+offer_id).style.display=="")
		{
			document.getElementById("offer_detail_tr_"+offer_id).style.display="none";
			document.getElementById("up_arrow_"+offer_id).src="<?php echo base_url() ?>public/images/up_cus_share1.png";
		}

	}

		
}

function show_hide_all_offers(st){	
	var act = "";
	if(st=='expand'){  act=""; }else if(st=='collapse'){ act="none"; }	
	var offer_id_str=document.getElementById("offer_id_str").value;
	var offer_id_arr = offer_id_str.split("##");		
	for(i=0;i<offer_id_arr.length;i++)
	{	
		if(document.getElementById("offer_detail_tr_"+offer_id_arr[i]))
		{
			document.getElementById("offer_detail_tr_"+offer_id_arr[i]).style.display=act; 
		}
		
	}
}
$('.search_btn').click(function(){
	$( "#liveoffers_srch_frm" ).submit();
});
</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
