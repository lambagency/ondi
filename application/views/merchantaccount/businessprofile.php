<?php 
	$state_list[''] = '*State';
	$city_list[''] = '*City';
	$social_media_list[''] = 'Select';
	$price_guide_list[''] = '*Price Guide';
	$array_selected_business_types = array();
	$array_selected_confirmation_methods = array();
	$array_service_offered = array();
	$array_perfect_for = array();
	if($userdata[0]->business_type != '')
	{
		$array_selected_business_types = explode(",", $userdata[0]->business_type);
	}
	if($userdata[0]->confirmation_methods != '')
	{
		$array_selected_confirmation_methods = explode(",", $userdata[0]->confirmation_methods);
	}
	if($userdata[0]->service_offered != '')
	{
		$array_service_offered = explode(",", $userdata[0]->service_offered);
	}
	
	foreach($state_options as $row)  {$state_list[$row->state_id] = $row->state;}
	foreach($city_options as $row)  {$city_list[$row->id] = $row->city_name;}
	foreach($price_guide_list_options as $pg_row) {$price_guide_list[$pg_row->pg_id] = $pg_row->pg_name;}
	foreach($perfect_for_list_options as $pf_row) {$perfect_for_list[$pf_row->pf_id] = $pf_row->pf_name;}
	foreach($dataservices as $rowdataservices)  {$services_list[$rowdataservices->id] = $rowdataservices->option_value;}
	
	$selectedbusinesstypes = "";
	foreach($databusinesstypesqueryjk as $thedatabusinesstypesqueryjk)
	{
		$selectedbusinesstypes .= $thedatabusinesstypesqueryjk->type_name.", ";
	}
	if($selectedbusinesstypes != "")
	{
		$selectedbusinesstypes = substr($selectedbusinesstypes, 0, -2);
	}

	$selectedservicesoffered = "";
	if((bool)$this->config->item('test_mode'))
	{
		//print_r($dataservicesoffered);
		//die;
	}
	foreach($dataservicesoffered as $thedataservicesofferedqueryjk)
	{
		$selectedservicesoffered .= $thedataservicesofferedqueryjk->option_value.", ";
	}
	if($selectedservicesoffered != "")
	{
		$selectedservicesoffered = substr($selectedservicesoffered, 0, -2);
	}
	
	//print_r($dataperfectfor);
	$selectedperfectfor = "";
	foreach($dataperfectfor as $thedataperfectforqueryjk)
	{
		$selectedperfectfor .= $thedataperfectforqueryjk->pf_name.", ";
		array_push($array_perfect_for, $thedataperfectforqueryjk->pf_id);
	}
	if($selectedperfectfor != "")
	{
		$selectedperfectfor = substr($selectedperfectfor, 0, -2);
	}

?>

<!doctype html>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/public/js/common.js'); ?>"></script>
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/payment_easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>


<!-- <link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script> -->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />
</head>
<body class="businessprofile">
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/merchant_top');?>

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">    
	<?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
	
	<?php $attributes = array('onsubmit' => 'return validatebusinessprofile()', 'name' => 'bp_form'); ?>
	<?php echo form_open_multipart(base_url()."merchantaccount/businessprofile/", $attributes)?>
    
	<input type="hidden" name="submit" value="1" />
	<input type="hidden" id="old_profile_picture" name="old_profile_picture" value="<?=$userdata[0]->profile_picture?>" />
	<input type="hidden" id="old_supporting_picture" name="old_supporting_picture" value="<?=$userdata[0]->supporting_picture?>" />
	<input type="hidden" id="old_widescreen_picture" name="old_widescreen_picture" value="<?=$userdata[0]->widescreen_picture?>" />
	<input type="hidden" id="old_pdf_menu" name="old_pdf_menu" value="<?=$userdata[0]->pdf_menu?>" />

	  <?php
	  $user_id = $this->users->get_main_merchant_id();
	  $user_steps_completed = $this->users->user_steps_completed($user_id);
	  if($user_steps_completed<3)
	  {
	  ?>      
	  <div class="progress_bar_bg">
        <div class="progress_bar">
          <ul>
            <li class="pink"></li>
            <li class="blue"></li>
            <li class="light_gray"></li>
          </ul>
          <div class="green_pointer2"><img alt="" src="<?php echo $this->config->item('base_url'); ?>public/images/check_green2.png"></div>
        </div>
        <div class="step_number2">
          <p>2</p>
        </div>
      </div>
	  <?php
	  }
	  ?>
      <div class="heading">
        <h1>Business Profile</h1>
      </div>
      <div class="sub_heading">
		<h3>The details entered on this page will appear on your business listing page for all customers to see. Fields marked (*) are mandatory.</h3>
        <!-- <h3>Fields marked (*) are mandatory.</h3> -->
      </div>
      <div class="business_profile">
		<div class="valid_errors"><?php echo validation_errors(); ?></div>
        <ul>
          <li>
            <input type="text" value="<?php echo set_value('business_name', (isset($userdata[0]->business_name) && $userdata[0]->business_name != '') ? $userdata[0]->business_name : ''); ?>" name="business_name" id="bp_business_name" class="input_box" placeholder="*Business Name"  />
          </li>
          <li>
            <input type="text" value="<?php echo set_value('website_address', (isset($userdata[0]->website_address) && $userdata[0]->website_address != '') ? $userdata[0]->website_address : ''); ?>" class="input_box" name="website_address" id="bp_website_address" placeholder="*Website Address" />
          </li>
          <li>
            <input type="text" value="<?php echo set_value('street_address', (isset($userdata[0]->street_address) && $userdata[0]->street_address != '') ? $userdata[0]->street_address : ''); ?>" class="input_box" name="street_address" id="bp_street_address" placeholder="*Street Address" />
          </li>
          <li>
            <input type="text" value="<?php echo set_value('suburb', (isset($userdata[0]->suburb) && $userdata[0]->suburb != '') ? $userdata[0]->suburb : ''); ?>" class="suburb_box" name="suburb" id="bp_suburb"  placeholder="*Suburb" /> 
            <input type="text" value="<?php echo set_value('post_code', (isset($userdata[0]->post_code) && $userdata[0]->post_code != '') ? $userdata[0]->post_code : ''); ?>" class="post_box" name="post_code" id="bp_post_code"  placeholder="*Post Code"  />
            <?php
			$selected = isset($userdata[0]->city) ? $userdata[0]->city : $this->input->post('city');                        
			echo form_dropdown('city', $city_list, $selected, 'class="revostyled"');
			?>
			<?php
			$selected = isset($userdata[0]->state) ? $userdata[0]->state : $this->input->post('state');                        
			echo form_dropdown('state', $state_list, $selected, 'class="revostyled"');
			?>
          </li>
          <li>
            <input type="text" value="<?php echo set_value('business_phone', (isset($userdata[0]->business_phone) && $userdata[0]->business_phone != '') ? $userdata[0]->business_phone : ''); ?>" class="input_box" name="business_phone" id="bp_business_phone" placeholder="*Ph:"  />
          </li>
		  <!-- <li>
            <input type="text" value="<?php echo set_value('mobile_number', (isset($userdata[0]->mobile_number) && $userdata[0]->mobile_number != '') ? $userdata[0]->mobile_number : ''); ?>" class="input_box" name="mobile_number" id="bp_mobile_number" placeholder="*Mobile Number"  />
          </li> -->
        </ul>
      </div>
      <div class="add_social">
        <ul>
          <li> <img src="<?php echo $this->config->item('base_url'); ?>public/images/add_facebook.jpg">
            <input type="text" name="facebook" class="input_box" value="<?php echo set_value('facebook', (isset($userdata[0]->facebook) && $userdata[0]->facebook != '') ? $userdata[0]->facebook : ''); ?>" placeholder="Facebook URL e.g. http://facebook.com/ondiAus" />
          </li>

          <li> <img src="<?php echo $this->config->item('base_url'); ?>public/images/add_twitter.jpg" >
            <input type="text" class="input_box" name="twitter" value="<?php echo set_value('twitter', (isset($userdata[0]->twitter) && $userdata[0]->twitter != '') ? $userdata[0]->twitter : ''); ?>" placeholder="Twitter URL" />
          </li>

		  <li id="li_linkedin" <?php if(isset($userdata[0]->linkedin) && $userdata[0]->linkedin != ''){ echo "style='display:block;'"; } else{ echo "style='display:none;'"; }?> > <img src="<?php echo $this->config->item('base_url'); ?>public/images/linkendin.jpg" >
            <input type="text" class="input_box" name="linkedin" value="<?php echo set_value('linkedin', (isset($userdata[0]->linkedin) && $userdata[0]->linkedin != '') ? $userdata[0]->linkedin : ''); ?>" placeholder="Linkedin URL" />
          </li>

		  <li id="li_googleplus" <?php if(isset($userdata[0]->googleplus) && $userdata[0]->googleplus != ''){ echo "style='display:block;'"; } else{ echo "style='display:none;'"; }?> > <img src="<?php echo $this->config->item('base_url'); ?>public/images/gplus.jpg" >
            <input type="text" class="input_box" name="googleplus" value="<?php echo set_value('googleplus', (isset($userdata[0]->googleplus) && $userdata[0]->googleplus != '') ? $userdata[0]->googleplus : ''); ?>" placeholder="Google Plus URL" />
          </li>

		  <li id="li_more_social" style="display:none;"> 
				<select onchange="showsocial(this.value);" name="sm_extra_1" class="dropdown"  >
					<option value="">Select</option>
					<option value="1">facebook</option>
					<option value="2">twitter</option>
					<option value="3">linkedin</option>
					<option value="4">googleplus</option>
				</select>
          </li>
		  
          <li id="li_show_more_social" > <a href="javascript:void(0);" onclick="show_more_social();">+ add another social media</a> </li>
        </ul>
      </div>
      <div class="what_kind">
        <ul>
          <li>
            <label>What kind of service industry are you?</label>
            <div class="select" id="div_service_industries2">
            <img src="<?php echo $this->config->item('base_url'); ?>public/images/select_flex_arrow.png" border="0"/>
            <div id="div_service_industries"><?php if($selectedbusinesstypes != '') echo $selectedbusinesstypes; else echo " *Please select"; ?></div>
              <div class="drop" id="service_drop">
              	<span><a href="javascript:void(0);" onclick="uncheckall_bp('checkboxesgroup');">Clear all..</a></span>
                <ul id="checkboxesgroup">
				  <?php foreach($business_types as $the_type){?>	
                  <li><input class="class_business_type" type="checkbox" value="<?=$the_type->type_id?>" name="business_type[]" <?php if(in_array($the_type->type_id, $array_selected_business_types)){ echo "checked='checked'"; } ?> title="<?=$the_type->type_name?>" ><?=$the_type->type_name?></li>
                  <?php } ?>
                </ul>
				<input type="hidden" id="choosen_business_types" name="choosen_business_types" value="<?=$userdata[0]->business_type?>" />
              </div>
            </div>

			<a id="anc_add_more_category" href="javascript:void(0);" onclick="add_more_category();" >+ add another category</a>

          </li>
		  <li>
			<?php for($ac=1; $ac<=25; $ac++){?>
			<div class="lyb_add_cats" id="div_business_category_<?=$ac?>" style="display:none;" >
				<input type="text" class="position_binfo" id="additional_cat_<?=$ac?>" name="additional_cat_<?=$ac?>" value="" />
			</div>
			<?php } ?>
			<input type="hidden" id="num_category" name="num_category" value="0" />
		  </li>
		
		   <li>
            <label>Service Offered</label>
            <div class="select" id="div_service_industries3">
            <img src="<?php echo $this->config->item('base_url'); ?>public/images/select_flex_arrow.png" border="0"/>
            <div id="div_service_offered"><?php if($selectedservicesoffered != '') echo $selectedservicesoffered; else echo " *Please select"; ?></div>
              <div class="drop" id="service_drop3">
              	<?php if(sizeof($services_list)>0){?>
					<span><a href="javascript:void(0);" onclick="uncheckall_bp('checkboxesgroupso');">Clear all..</a></span>
				<?php } ?>
                <ul id="checkboxesgroupso">
				  <?php foreach($services_list as $key=>$val){?>
                  <li><input class="class_services_offered" type="checkbox" value="<?=$key?>" name="services_offered[]" <?php if(in_array($key, $perfectfor_list)){ echo "checked='checked'"; } ?> title="<?=$val?>" onclick="change_so(this);"><?=$val?></li>
                  <?php } ?>
                </ul>
              </div>
            </div>
			<a id="anc_add_more_service_offered" href="javascript:void(0);" onclick="add_more_service_offered();">+ add another service</a>
          </li>

		  <li>
			<?php for($so=1; $so<=10; $so++){?>
			<div class="lyb_add_cats" id="div_so_<?=$so?>" style="display:none;" >
				<input type="text" class="position_binfo" id="additional_so_<?=$so?>" name="additional_so_<?=$so?>" value="" />
			</div>
			<?php } ?>
			<input type="hidden" id="num_so" name="num_so" value="0" />
		  </li>

        </ul>
      </div>
	  
      <div class="service">
        <ul>
          <li>
			<?php
			$selected = isset($userdata[0]->price_guide) ? $userdata[0]->price_guide : $this->input->post('price_guide');                        
			echo form_dropdown('price_guide', $price_guide_list, $selected, 'class="revostyled"');
			?>
          </li>
          <li id="li_perfect_for">
            <div class="select" id="div_perfect_for3">
				<img src="<?php echo $this->config->item('base_url'); ?>public/images/select_flex_arrow.png" border="0"/>
				<div id="div_perfect_for"><?php if($selectedperfectfor != '') echo $selectedperfectfor; else echo "*Perfect For"; ?></div>
				  <div class="drop" id="perfectfor_drop3">
					<?php if(sizeof($perfect_for_list)>0){?>
						<span><a href="javascript:void(0);" onclick="uncheckall_pf('checkboxesgrouppf');">Clear all..</a></span>
					<?php } ?>
					<ul id="checkboxesgrouppf">
					  <?php foreach($perfect_for_list as $key=>$val){?>
					  <li><input class="class_services_offered" type="checkbox" value="<?=$key?>" name="perfect_for[]" <?php if(in_array($key, $array_perfect_for)){ echo "checked='checked'"; } ?> title="<?=$val?>" onclick="change_pf(this);"><?=$val?></li>
					  <?php } ?>
					</ul>
				  </div>
            </div>

          </li>
        </ul>
      </div>
      
		<?php
		$loggedinuserdata = $this->customclass->getLoggedInUserData(); 
		?>

		<?php 
		if($userdata[0]->profile_picture == "" && $userdata[0]->supporting_picture == "" && $userdata[0]->widescreen_picture == "")
		{
			$iframeheight=520;
		}
		else if($userdata[0]->profile_picture != "" && $userdata[0]->supporting_picture == "" && $userdata[0]->widescreen_picture == "")
		{
			$iframeheight=578;
		}
		else if($userdata[0]->profile_picture != "" && $userdata[0]->supporting_picture != "" && $userdata[0]->widescreen_picture == "")
		{
			$iframeheight=636;
		}
		else if($userdata[0]->profile_picture != "" && $userdata[0]->supporting_picture != "" && $userdata[0]->widescreen_picture != "")
		{
			$iframeheight=694;
		}
		?>
		<iframe height="<?=$iframeheight?>" frameborder="0" width="100%" scrolling="no" src="<?php echo $this->config->item('base_url'); ?>image.php?user=<?=$loggedinuserdata[0]->user_id?>" id="iframe_id"></iframe>
		<input type="hidden" name="temp_profile_picture" id="temp_profile_picture" value="" />
		<input type="hidden" name="temp_supporting_picture" id="temp_supporting_picture" value="" />
		<input type="hidden" name="temp_widescreen_picture" id="temp_widescreen_picture" value="" />

		<input type="hidden" name="old_profile_picture" id="old_profile_picture" value="<?=$userdata[0]->profile_picture?>" />
		<input type="hidden" name="old_supporting_picture" id="old_supporting_picture" value="<?=$userdata[0]->supporting_picture?>" />
		<input type="hidden" name="old_widescreen_picture" id="old_widescreen_picture" value="<?=$userdata[0]->widescreen_picture?>" />

      
      <div class="upload">
        <h2>Upload Menu/Price List :</h2>
        <ul>
          <li>
            <div class="browse4">
              <input type="file" name="pdf_menu" id="pdf_menu"/>
            </div>
            <div class="upload_info">
              <p>Upload a PDF of your menu or price list here<br>
                <span>(This is included in your business profile )</span> 
			  </p>
			  <?php if($userdata[0]->pdf_menu != ''){?>
				<a href="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->pdf_menu?>" target="_blank">View</a>
			  <?php } ?>

            </div>
          </li>
        </ul>
      </div>
      <div class="upload">
        <h2>Confirmation Method for bookings:</h2>
        <ul>
          <li>
          <div class="select">
          
		  <?php 
		  if($userdata[0]->confirmation_methods != '') 
		  {
			  $str_confirmation_methods = str_replace(",", ", ", $userdata[0]->confirmation_methods);
			  $str_confirmation_methods = str_replace("phone", "Phone Call", $str_confirmation_methods);
			  $str_confirmation_methods = str_replace("sms", "SMS", $str_confirmation_methods);
			  $str_confirmation_methods = ucwords($str_confirmation_methods);
		  }
		  else
		  {
			  $str_confirmation_methods =  "Please choose at least 1"; 
		  }
		  ?>
          <div id="div_confirmation_methods"><?php echo $str_confirmation_methods; ?></div>
              <div class="drop" id="sms">
              	<span><a href="javascript:void(0);" onclick="uncheckall_merchant('checkboxesgroup_confirmation');">Clear all..</a></span>
                <ul id="checkboxesgroup_confirmation">
                  <li><input id="confirmation_methods_sms" name="confirmation_methods[]" type="checkbox" value="sms" <?php if(in_array('sms', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?> onclick="change_confirmation_methods(this.value, this.checked);" >SMS</li>

                  <!-- <li><input id="confirmation_methods_email"  name="confirmation_methods[]" type="checkbox" value="email" <?php if(in_array('email', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?>  onclick="change_confirmation_methods(this.value, this.checked);"   >Email</li> -->

				  <li><input id="confirmation_methods_email"  name="confirmation_methods[]" type="checkbox" value="email" <?php if(in_array('email', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?>  onclick="return false"   >Email</li>

                  <li><input id="confirmation_methods_phone"  name="confirmation_methods[]" type="checkbox" value="phone" <?php if(in_array('phone', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?>  onclick="change_confirmation_methods(this.value, this.checked);"  >Phone Call</li>
                </ul>
              </div>
            </div>

			<input type="text" value="<?php echo set_value('mobile_number', (isset($userdata[0]->mobile_number) && $userdata[0]->mobile_number != '') ? $userdata[0]->mobile_number : ''); ?>" class="input_box" name="mobile_number" id="bp_mobile_number" placeholder="*Enter a mobile number for the SMS notifications" <?php if(in_array('sms', $array_selected_confirmation_methods)){ echo 'style="display:block;"'; } else { echo 'style="display:none;"'; }  ?>  />

          </li>
        </ul>
      </div>
      <div class="hours">
        <h2>Opening hours :</h2>
		<?php 
		$printed_days = 0;
		//print_r($opening_hours_query);
		
		$time_group = '';
		$array_oh_groups = array();
		$i = 0;
		$j=0;
		foreach($opening_hours_query as $this_oh)
		{
			if($time_group == '')
			{
				$this_oh_array = (array)$this_oh;
				$array_oh_groups[$i] = $this_oh_array;
				$time_group = $this_oh->time_group;
			}
			else
			{
				if($time_group != $this_oh->time_group)
				{
					$i++;
					$this_oh_array = (array)$this_oh;
					$array_oh_groups[$i] = $this_oh_array;
					$time_group = $this_oh->time_group;
				}
				else
				{
					$array_oh_groups[$i]['day'] = $array_oh_groups[$i]['day'].",".$this_oh->day;
				}
				
			}
		}
		//echo '<pre>';
		//print_r($array_oh_groups);
		//echo '</pre>';

		$ccjk=0;
		for($dboh=0; $dboh<sizeof($array_oh_groups); $dboh++)
		{
			$printed_days++;
			$ccjk++;
			$array_selected_oh = explode(",", $array_oh_groups[$dboh]['day']);
			$str_to_display = "";
			if(in_array('monday', $array_selected_oh))
			{
				$str_to_display .= "Mo,";
			}
			if(in_array('tuesday', $array_selected_oh))
			{
				$str_to_display .= "Tu,";
			}
			if(in_array('wednesday', $array_selected_oh))
			{
				$str_to_display .= "We,";
			}
			if(in_array('thursday', $array_selected_oh))
			{
				$str_to_display .= "Th,";
			}
			if(in_array('friday', $array_selected_oh))
			{
				$str_to_display .= "Fr,";
			}
			if(in_array('saturday', $array_selected_oh))
			{
				$str_to_display .= "Sa,";
			}
			if(in_array('sunday', $array_selected_oh))
			{
				$str_to_display .= "Su,";
			}
			if($str_to_display != "")
			{
				$str_to_display = substr($str_to_display, 0, -1);
			}
			else
			{
				$str_to_display = "Day";
			}

			?>
			<input type="hidden" name="time_group_<?=$printed_days?>" value="<?=$array_oh_groups[$dboh]['time_group']?>" />
			<ul id="opening_hours_<?=$printed_days?>" >
			  <li class="day">
              <div class="select">
                  <div class="day_drop_check" id="date_<?=$printed_days?>"><?=$str_to_display?></div>
                      <div  class="day_drop_check_ul" id="date_drop_<?=$printed_days?>">
                        <!-- <span><a onclick="uncheckall('checkboxesgroup_oh_<?=$printed_days?>');" href="javascript:void(0);">Clear all..</a></span> -->
                        <ul id="checkboxesgroup_oh_<?=$printed_days?>">
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Mo" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_mon" <?php if(in_array('monday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Monday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Tu" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_tue" <?php if(in_array('tuesday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Tuesday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="We" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_wed" <?php if(in_array('wednesday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Wednesday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Th" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_thu" <?php if(in_array('thursday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Thursday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Fr" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_fri" <?php if(in_array('friday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Friday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Sa" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_sat" <?php if(in_array('saturday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Saturday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$printed_days?>);" value="Su" name="oh_<?=$printed_days?>[]" id="oh_<?=$printed_days?>_sun" <?php if(in_array('sunday', $array_selected_oh)){ echo "checked='checked'"; } ?>  >Sunday</li>
                        </ul>
                      </div>
                    </div>
			  </li>
			  <li class="from">
				<label>from</label>
                <div class="drop1">
                	<select name="from_<?=$printed_days?>" class="revostyled">
                        <option value="">From</option>
						<?php 
						for($bt=0000; $bt<=2345; $bt=$bt+15)
						{  
							if((substr($bt, strlen($bt)-2, strlen($bt))==60))
							{
								$bt = $bt+40;
							}
							if(strlen($bt)==3) $bt = "0".$bt;
							if(strlen($bt)==2) $bt = "00".$bt;
							if(strlen($bt)==1) $bt = "000".$bt;

							$strselected = "";
							if(isset($array_oh_groups[$dboh]['from_hour']))
							{ 
								if( date("h:i", strtotime($array_oh_groups[$dboh]['from_hour'])) == date("h:i", strtotime($bt)))
								{ 
									$strselected = 'selected="selected"'; 
								} 
							}
						?>
						<option value="<?=date("h:i", strtotime($bt))?>" <?=$strselected?> ><?=date("h:i", strtotime($bt))?></option>
						<?php } ?>
                    </select>
                </div>
				
				<div class="drop2">
                    <select name="from_ampm_<?=$printed_days?>" class="revostyled">
                      <option value="AM" <?php if($opening_hours_query[$dboh]->from_ampm=='AM') echo 'selected="selected"';?> >AM</option>
                      <option value="PM" <?php if($opening_hours_query[$dboh]->from_ampm=='PM') echo 'selected="selected"';?> >PM</option>
                    </select>
                </div>
			  </li>
			  <li class="to">
				<label>to</label>
                <div class="drop1">
                    <select name="to_<?=$printed_days?>" class="revostyled">
                        <option value="">To</option>
                        <?php 
						for($bt=0000; $bt<=2345; $bt=$bt+15)
						{  
							if((substr($bt, strlen($bt)-2, strlen($bt))==60))
							{
								$bt = $bt+40;
							}
							if(strlen($bt)==3) $bt = "0".$bt;
							if(strlen($bt)==2) $bt = "00".$bt;
							if(strlen($bt)==1) $bt = "000".$bt;

							$strselected = "";
							if(isset($array_oh_groups[$dboh]['to_hour']))
							{ 
								if( date("h:i", strtotime($array_oh_groups[$dboh]['to_hour'])) == date("h:i", strtotime($bt)))
								{ 
									$strselected = 'selected="selected"'; 
								} 
							}
						?>
						<option value="<?=date("h:i", strtotime($bt))?>" <?=$strselected?>  ><?=date("h:i", strtotime($bt))?></option>
						<?php } ?>
                    </select>
                </div>
				<div class="drop2">
                     <select name="to_ampm_<?=$printed_days?>" class="revostyled">
                      <option value="AM" <?php if($opening_hours_query[$dboh]->to_ampm=='AM') echo 'selected="selected"';?> >AM</option>
                      <option value="PM" <?php if($opening_hours_query[$dboh]->to_ampm=='PM') echo 'selected="selected"';?> >PM</option>
                     </select>
                </div>
			  </li>
			</ul>
			<input type="hidden" name="timinings_id_<?=$printed_days?>" value="<?=$opening_hours_query[$dboh]->id?>" />
		<?php } ?>



		<?php for($i=$printed_days+1; $i<=7; $i++){ $ccjk++; ?>
		<ul id="opening_hours_<?=$i?>" <?php if($i>1){ echo 'style="display:none;"'; } ?>>
			  <li class="day">
              <div class="select">
                  <div class="day_drop_check" id="date_<?=$i?>">Day</div>
                      <div  class="day_drop_check_ul" id="date_drop_<?=$i?>">
                        <ul id="checkboxesgroup_oh_<?=$i?>">
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Mo" name="oh_<?=$i?>[]" id="oh_<?=$i?>_mon">Monday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Tu" name="oh_<?=$i?>[]" id="oh_<?=$i?>_tue">Tuesday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="We" name="oh_<?=$i?>[]" id="oh_<?=$i?>_wed">Wednesday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Th" name="oh_<?=$i?>[]" id="oh_<?=$i?>_thu">Thursday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Fr" name="oh_<?=$i?>[]" id="oh_<?=$i?>_fri">Friday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Sa" name="oh_<?=$i?>[]" id="oh_<?=$i?>_sat">Saturday</li>
                          <li><input type="checkbox" onclick="change_oh(this.value, this.checked, <?=$i?>);" value="Su" name="oh_<?=$i?>[]" id="oh_<?=$i?>_sun">Sunday</li>
                        </ul>
                      </div>
                    </div>
			  </li>
			  <li class="from">
				<label>from</label>
                <div class="drop1">
                	<select name="from_<?=$i?>" class="revostyled">
                        <option value="">From</option>
						<?php 
						for($bt=0000; $bt<=2345; $bt=$bt+15)
						{  
							if((substr($bt, strlen($bt)-2, strlen($bt))==60))
							{
								$bt = $bt+40;
							}
							if(strlen($bt)==3) $bt = "0".$bt;
							if(strlen($bt)==2) $bt = "00".$bt;
							if(strlen($bt)==1) $bt = "000".$bt;
						?>
						<option value="<?=date("h:i", strtotime($bt))?>"><?=date("h:i", strtotime($bt))?></option>
						<?php } ?>
                    </select>
                </div>
				
				<div class="drop2">
                    <select name="from_ampm_<?=$i?>" class="revostyled">
                      <option value="AM" >AM</option>
                      <option value="PM" >PM</option>
                    </select>
                </div>
			  </li>
			  <li class="to">
				<label>to</label>
                <div class="drop1">
                    <select name="to_<?=$i?>" class="revostyled">
                        <option value="">To</option>
                        <?php 
						for($bt=0000; $bt<=2345; $bt=$bt+15)
						{  
							if((substr($bt, strlen($bt)-2, strlen($bt))==60))
							{
								$bt = $bt+40;
							}
							if(strlen($bt)==3) $bt = "0".$bt;
							if(strlen($bt)==2) $bt = "00".$bt;
							if(strlen($bt)==1) $bt = "000".$bt;
						?>
						<option value="<?=date("h:i", strtotime($bt))?>" ><?=date("h:i", strtotime($bt))?></option>
						<?php } ?>
                    </select>
                </div>
				<div class="drop2">
                     <select name="to_ampm_<?=$i?>" class="revostyled">
                      <option value="AM" >AM</option>
                      <option value="PM" >PM</option>
                     </select>
                </div>
			  </li>
		</ul>
		<?php } ?>
	 
	 <?php if(sizeof($array_oh_groups)<7) {?>
     <a id="anc_show_more_open_time" href="javascript:void(0);" onclick="show_more_open_time_bp();">+ add another opening time</a> 
	 <?php }?>

	 <input type="hidden" name="num_open_time" id="num_open_time" <?php if(sizeof($array_oh_groups)>1){?>value="<?=sizeof($array_oh_groups)?>"<?php } else {?>value="1" <?php } ?> /> 
	 </div>
      <div class="additional">
        <h2>Additional information:</h2>
        <ul>
          <li>
            <textarea id="about_business" name="about_business" cols="" rows="" class="desc" placeholder="*Overview-Short blurb about your business. What customers will love about it!" ><?php echo set_value('about_business', (isset($userdata[0]->about_business) && $userdata[0]->about_business != '') ? $userdata[0]->about_business : ''); ?></textarea>
            <p>(Maximum 200 characters)</p>
          </li>
          <li>
            <textarea name="parking_details" cols="" rows="" class="parking" placeholder="Parking details" ><?php echo set_value('parking_details', (isset($userdata[0]->parking_details) && $userdata[0]->parking_details != '') ? $userdata[0]->parking_details : ''); ?></textarea>
          </li>
        </ul>
      </div>
      <div class="save_bg">
		<span class="save">
			<?php 
			$user_id = $this->users->get_main_merchant_id();
			$user_steps_completed = $this->users->user_steps_completed($user_id);
			if($user_steps_completed>2){?>
				<input type="image" name="submit" src="<?php echo base_url() ?>public/images/user_save.jpg">
			<?php } else { ?>
				<input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/saved_continue2.jpg" >
			<?php } ?>
		</span> 
	  </div>
	  <?php echo form_close()?>
    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
 

<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>




<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<!--<script src="assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> -->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>





<script type="text/javascript" charset="utf-8">
    $(function(){
      // example 3
	  
      $("#profile_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#supporting_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#widescreen_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#pdf_menu").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
    });
  </script>


  <script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		$('.class_business_type').change(function () {
			choose_business_type(this.value, this.checked);
			//display_selected_business_types();
			var currtitle = this.title;
			var div_service_industries_innerhtml = document.getElementById('div_service_industries').innerHTML;
			//div_service_industries_innerhtml = div_service_industries_innerhtml.replace("&amp;", "&"); 
			div_service_industries_innerhtml = div_service_industries_innerhtml.replace(new RegExp("&amp;", 'g'), "&");

			div_service_industries_innerhtml = div_service_industries_innerhtml.replace("*Perfect For", "");
			var res = div_service_industries_innerhtml.split(", "); 

			if(this.checked)
			{
				var a = res.indexOf(currtitle);
				if(a=='-1')
				{
					res.push(currtitle);
				}
				if(div_service_industries_innerhtml != "")
				{
					var newstr = res.join(", ");
				}
				else
				{
					var newstr = currtitle;
				} 
			}
			else
			{
				//alert(res);
				//alert(currtitle);
				//currtitle = currtitle.replace("&amp;", "&");
				//res = res.replace("&amp;", "&"); 
				//alert(res);
				//alert(currtitle);
				var a = res.indexOf(currtitle);
				if (a > -1) {
					res.splice(a, 1);
				}
				var newstr = res.join(", ");
			}
			//alert(newstr);
			if(newstr == '')
			{
				newstr = "*Perfect For";
			}
			document.getElementById('div_service_industries').innerHTML = newstr;
		});


	  
		
		if ($('#li_linkedin').css('display')=='block' && $('#li_googleplus').css('display')=='block') {
			$('#li_show_more_social').css("display", "none");
		} else {
			$('#li_show_more_social').css("display", "block");
		}
	});
	
	/*$(document).ready(function(){
		//alert(1)
		//$('.from').css('overflow','hidden');
		
		$('.from').click(function(){
			$('.from').toggleClass('active');
		});
		
		$('.to').click(function(){
			$('.to').toggleClass('active');
		});
				
	}); */
	
	function change_so(obj)
	{
			//alert('dfsdf');
			var currtitle_so = obj.title;
			var div_services_offered_innerhtml = document.getElementById('div_service_offered').innerHTML;
			div_services_offered_innerhtml = div_services_offered_innerhtml.replace(" *Please select", "");
			var res = div_services_offered_innerhtml.split(", "); 

			if(obj.checked)
			{
				var a = res.indexOf(currtitle_so);
				if(a=='-1')
				{
					res.push(currtitle_so);
				}
				if(div_services_offered_innerhtml != "")
				{
					var newstr = res.join(", ");
				}
				else
				{
					var newstr = currtitle_so;
				} 
			}
			else
			{
				var a = res.indexOf(currtitle_so);
				if (a > -1) {
					res.splice(a, 1);
				}
				var newstr = res.join(", ");
			}
			//alert(newstr);
			if(newstr == '')
			{
				newstr = " *Please select";
			}
			document.getElementById('div_service_offered').innerHTML = newstr;
		}


		function change_pf(obj)
		{
				//alert('dfsdf');
				var currtitle_so = obj.title;
				var div_services_offered_innerhtml = document.getElementById('div_perfect_for').innerHTML;
				div_services_offered_innerhtml = div_services_offered_innerhtml.replace("*Perfect For", "");
				var res = div_services_offered_innerhtml.split(", "); 

				if(obj.checked)
				{
					var a = res.indexOf(currtitle_so);
					if(a=='-1')
					{
						res.push(currtitle_so);
					}
					if(div_services_offered_innerhtml != "")
					{
						var newstr = res.join(", ");
					}
					else
					{
						var newstr = currtitle_so;
					} 
				}
				else
				{
					var a = res.indexOf(currtitle_so);
					if (a > -1) {
						res.splice(a, 1);
					}
					var newstr = res.join(", ");
				}
				//alert(newstr);
				if(newstr == '')
				{
					newstr = "*Perfect For";
				}
				document.getElementById('div_perfect_for').innerHTML = newstr;
			}
</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>