<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/public/js/common.js'); ?>"></script>
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/payment_easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>


<!-- <link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script> -->
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
</head>
<body>
	<?php $this->load->view('templates/header');?>
	<?php $this->load->view('templates/merchant_top');?>
	<!--Start of customer Area-->
	<div class="merchant_main_con">
		<div class="merchant_main customer_main">    
			<?php $this->load->view('templates/merchant_main_left');?>
			<div class="merchant_main_right">
				<div class="customer_main_right">
					<div class="heading">
						<h1>Bank Account Information</h1>
					</div>
					<div class="member_detail">
						<ul>
							<li>
                        	<div class="photo_left">
                                <p>
									Branch name: <?=$user_data[0]->branch_name?><br />
									Account name: <?=$user_data[0]->account_name?><br />
									BSB: <?=$user_data[0]->bsb?><br />
									Account Number: <?=$user_data[0]->account_number?><br />
									ABN: <?=$user_data[0]->abn?><br />
								</p>
                            </div>
                        </li>
                    </ul>
            </div>
      <div class="save_bg"> <span class="save"><a href="<?php echo base_url("merchantaccount/bankingdetails/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/saved_edit.jpg" /></a></span> </div>
    </div>
  </div>
</div>
</div>
<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
 

<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<!--<script src="assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> -->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>