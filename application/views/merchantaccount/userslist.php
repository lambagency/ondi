<?php
/*
echo '<pre>';
print_r($users_data);
echo '</pre>';
*/
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<script>
function edit_merchant_user(id)
{	
	if(document.getElementById("muid"))
	{
		document.getElementById("muid").value=id;	
	}
	if(document.getElementById("edit_merchant_user"))
	{
		document.getElementById("edit_merchant_user").submit();
	}
	
}
</script>
</head>

<body>
<?php
$attributes = array('id' => 'edit_merchant_user');
?>
<?php echo form_open(base_url()."merchantaccount/addusers/", $attributes)?>
<input type="hidden" name="muid" id="muid" value="">
<?php echo form_close()?>

<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">

   <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
	
      
      <div class="heading">
        <h1>Users</h1>
      </div>
      <div class="sub_heading">
        <h3>People in your account and what they can access.</h3>
      </div>
      
    <div class="merchant_user">
    
    	<div class="head">

									
					<?php echo $this->session->flashdata('user_message'); ?>

                	<table width="595" border="1" class="table_customer">
                      <tr>
                        <th align="left" scope="col" width="130">Person</th>
                        <th align="left" scope="col" width="110" class="no_bg">Permission</th>
                        <th align="left" scope="col" width="40" class="no_bg">&nbsp;</th>
                      </tr>
                      </table>
                      </div>
                      <div class="detail">
                      <table width="595" border="1" class="table_customer">
					  <?php if(count($users_data)>0){ foreach($users_data as $users_val){ ?>
					  <tr>
                        <td align="left" scope="col" width="160"><?=stripslashes($users_val->email)?></td>
                        <td align="left" scope="col" width="140" class="permission"><?=stripslashes($users_val->access_type_title)?></td>
                        <td align="left" scope="col" width="30">
                        	<div class="edit_merchant"><a href="javascript:void(0)" onclick="edit_merchant_user('<?php echo $users_val->id; ?>')"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Edit</a></div>
                        </td>
                      </tr>
					  <?php } }else{ ?>
					  <tr>
                        <td align="center" scope="col" width="160" colspan="3">No records found.</td>                        
                      </tr>
					  <?php } ?>
                      </table>
                 </div>
        
    </div>
    

	</div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script src="assets/check/jquery.screwdefaultbuttonsV2.js"></script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>
</body>

</html>
