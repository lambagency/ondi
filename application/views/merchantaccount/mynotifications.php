<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
</head>
<body>

<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/merchant_top');?>

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
    <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
      
      <div class="heading_alone">
        <h1 class="notice">Notifications</h1>
      </div>
     
    	<div class="notification">
			<ul>
						<?php 
						$ni=0;
						foreach($notifications as $this_notification)
						{
							$ni++;
							if($sess_user_data['user_type']=='1')
							{
								$read_status = $this_notification->customer_read_status;
							}
							else
							{
								$read_status = $this_notification->merchant_read_status;
							}
						?>
						<li <?php if($read_status=='0'){ echo 'class="active"'; } ?>>
							<?php if($this_notification->profile_message == '1'){?>
							<div class="left_note"><img src="<?php echo $this->config->item('base_url'); ?>public/images/notification.png" alt="" /></div>
							<?php } else { ?>
							<div class="left_note"><div class="calender">23</div></div>
							<?php } ?>

							<div class="right_note"><?=$this_notification->message?></div>
							<div class="date_note"><?=$this->customclass->_ago(strtotime($this_notification->added_date))?>  / <span <?php if($read_status=='1'){ echo 'class="normal"'; } else { echo 'class="bold"'; } ?> onclick="change_read_status_detail(<?=$this_notification->id?>);" id="detail_span_notification_<?=$this_notification->id?>" >Mark as <?php if($read_status=='1'){ echo 'unread'; } else { echo 'Read'; } ?></span> </div>
						</li>
						<?php 
						} ?>
					</ul>

		</div>
                
               
    			<div class="save_bg5">
                    <span class="upper">
                        <a href="javascript:void" id="up"><img src="<?php echo $this->config->item('base_url'); ?>public/images/orenge_up.png" alt="" /></a>
                    </span>
                </div>
    </div>
  </div>
</div>

<?php $this->load->view('templates/footer');?>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>