<?php
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><script type="text/javascript" src="<?=base_url('/public/js/merchantbookinglist.js'); ?>"></script>

</head>
<body>

<!-- Start of nav Section-->
<?php $this->load->view('templates/header'); ?>
<!--End of nav_selected Section-->

<?php $this->load->view('templates/merchant_top');?>

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
    
	<?php $this->load->view('templates/merchant_main_left');?>

    <div class="merchant_main_right">
      
      <div class="heading">
        <h1>Booking History</h1>
      </div>

      <div class="sub_heading">
        <h3>See all of your current bokings as a list.</h3>
      </div>
      
    	<div class="merchant_booking">
                	<div class="filter_box">                        
						
						<?php echo form_open(base_url()."merchantaccount/bookingshistory", array('id'=>'bookingshistory_srch_frm'))?>
						<div class="search_box">
							<input type="button" class="search_btn"/>
                        	<input type="text" name="offer_search" id="offer_search" value="<?php if(isset($offer_search)){ echo $offer_search; } ?>" class="search" placeholder="Search" />
							<?php if(isset($offer_search) && $offer_search != ''){?>
							<a href="javascript:void(0);" onclick="document.getElementById('offer_search').value=''; document.getElementById('bookingshistory_srch_frm').submit();" class="close_x">X</a>
							<?php } ?>
					    </div>
						<?php echo form_close()?>                        
						
						<div class="expand">
                            <a href="javascript:void(0);" id="expand_all" onclick="expand_all_div()" >Expand all</a> | <a href="javascript:void(0);" id="collapse_all" onclick="collapse_all_div()" >Collapse all</a>
                        </div>

                    </div>
                    
                    	<?php if (!$this->agent->is_mobile())
						{
					    ?>
                        
                        
                        
                        <div class="head">
                          <table width="595" border="1" class="table_customer">
                              <tr>
                                <th align="left" scope="col" width="230">Offer</th>
                                <th align="left" scope="col" width="25">Time</th>
                                <th align="left" scope="col" width="40">Quantity</th>
                                <th align="left" scope="col" width="25">Total</th>
                                <th align="left" scope="col" width="35" class="no_bg">Status</th>
                                <th align="left" scope="col" width="18" class="no_bg">&nbsp;</th>
                              </tr>
                          </table>
                        </div>
                        
                        <div class="detail">
                      	<ul> 
							<?php
								$i=0;
								if(count($history_bookings_dates)>0)
								{
									foreach($history_bookings_dates as $current_bookings_val)
									{
										$i++;
										$booking_date = strtotime($current_bookings_val->booking_date);									
										$booking_list_view = $this->users->get_bookings_list_by_date($booking_date);
							?>
							<li>                            	
								<div class="datewise">
									<p><?php echo date('D d/m/y',strtotime($current_bookings_val->booking_date)); ?></p>
									<span class="collapse"><a href="javascript:void(0);" onclick="show_hide_booking_detail('<?=$i?>')" id="date_expand_anchor_<?=$i?>">- Collapse</a></span></div>  
									
							<div class="table_customer_data booking_history" id="table_customer_data<?=$i?>" style="display:block;">                                
                                 <table width="595" border="1" class="table_customer">                                    
									 <?php foreach($booking_list_view as $booking_list_val){ ?>
										 <tr>
											<td align="left" scope="col" width="510" class="one"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></td>
											<td align="left" scope="col" width="90" class="two"><?php echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time)));  ?></td>
											<td align="left" scope="col" width="105"class="three"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</td>
											<td align="left" scope="col" width="80" class="four">$<?php echo stripslashes($booking_list_val->total_price);  ?></td>
											<td align="left" scope="col" width="85" class="book_btn2">&nbsp;Completed</td>
											<td align="left" scope="col" width="45" class="five">
											<a href="javascript:void(0);" class="arrow" id="arrow_<?=$booking_list_val->order_id?>" onclick="show_offer_rest_detail(<?=$booking_list_val->order_id?>)"></a>
											</td>                                        
										  </tr>									  
										  <tr class="new_col">
											<td colspan="7">
												<div class="add_calander" id="add_calander_<?=$booking_list_val->order_id?>">
													<div class="left_img"></div>
													<div class="right_img">
														<p>Booking #: <span><!-- OIELXXXXXXXXX --><?php echo stripslashes($booking_list_val->booking_code);  ?></span>  &nbsp;&nbsp;&nbsp; Booked on: <span><?php echo date('d/m/y',strtotime($booking_list_val->order_date)); ?></span>&nbsp;&nbsp;&nbsp;  Invoice: <span><a href="<?php echo base_url() ?>merchantaccount/invoice/?bid=<?=base64_encode($booking_list_val->order_id)?>&print=yes"  target="_blank">Print</a></span></p>
													</div>
												</div>
											</td>
										 </tr>                                     
										 <tr class="new_col_blank">
											<td colspan="7"><div class="blank_div">&nbsp;</div></td>
										 </tr>
									 <?php } ?>
                                  </table>
                                  </div>
                            </li>
							<?php 
									} 
								}else{
									
									echo '<li>No records found.</li>';

								}

							?>
							
							<input type="hidden" name="totalbookingdates" id="totalbookingdates" value="<?php if(isset($history_bookings_dates)){ echo count($history_bookings_dates); } ?>">
	
                        </ul>
                     
                    </div>
                    
                    
                    
                    
						
						
						
						<?php	
						}
						else
						{
						
						?>
                        
                        
                        <div class="detail">
                      	<ul> 
							<?php
								$i=0;
								if(count($history_bookings_dates)>0)
								{
									foreach($history_bookings_dates as $current_bookings_val)
									{
										$i++;
										$booking_date = strtotime($current_bookings_val->booking_date);									
										$booking_list_view = $this->users->get_bookings_list_by_date($booking_date);
							?>
							<li>                            	
								<div class="datewise">
									<p><?php echo date('D d/m/y',strtotime($current_bookings_val->booking_date)); ?></p>
									<span class="collapse"><a href="javascript:void(0);" onclick="show_hide_booking_detail('<?=$i?>')" id="date_expand_anchor_<?=$i?>">- Collapse</a></span></div>  
									
									<!-- id="collapse_<?=$i?>" -->
									

							<div class="table_customer_data booking_history" id="table_customer_data<?=$i?>" style="display:block;">                                
                                 <div class="table_customer">                                    
									 <?php foreach($booking_list_view as $booking_list_val){ ?>
										 <div class="merchant_booking_list">
											<div class="one"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></div>
                                            <div class="five"><a href="javascript:void(0);" class="arrow" id="arrow_<?=$booking_list_val->order_id?>" onclick="show_offer_rest_detail(<?=$booking_list_val->order_id?>)"></a></div>   
                                            
											<div class="two"><?php echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time)));  ?></div>
											<div class="three"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</div>
											<div class="four">$<?php echo stripslashes($booking_list_val->total_price);  ?></div>
											<div class="book_btn2">Status : Completed</div>
											                                     
										  </div>									  
										  
                                          <div class="add_calander" id="add_calander_<?=$booking_list_val->order_id?>">
                                                <div class="left_img"></div>
                                                <div class="right_img">
                                                    <p>Booking #: <span><!-- OIELXXXXXXXXX --><?php echo stripslashes($booking_list_val->booking_code);  ?></span>  &nbsp;&nbsp;&nbsp; Booked on: <span><?php echo date('d/m/y',strtotime($booking_list_val->order_date)); ?></span>&nbsp;&nbsp;&nbsp;  Invoice: <span><a href="<?php echo base_url() ?>merchantaccount/invoice/?bid=<?=base64_encode($booking_list_val->order_id)?>&print=yes"  target="_blank">Print</a></span></p>
                                                </div>
                                          </div>
                                        
									 <?php } ?>
                                  </table>
                                  </div>
                            </li>
							<?php 
									} 
								}else{
									
									echo '<li>No records found.</li>';

								}

							?>
							
							<input type="hidden" name="totalbookingdates" id="totalbookingdates" value="<?php if(isset($history_bookings_dates)){ echo count($history_bookings_dates); } ?>">
	
                        </ul>
                     
                    </div>
                    </div>
                    
                    
                    
                       
                    						   
						   
					<?php
                    }
                    
                    ?>
						
                </div>
    			<div class="save_bg">
                        <span class="upper"><a href="javascript:void" id="up"><img src="<?php echo base_url() ?>public/images/orenge_up.png" alt="" /></a></span>
                    </div>
    </div>
  </div>
</div>
<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
