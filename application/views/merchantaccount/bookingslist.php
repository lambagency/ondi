<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />


<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<?=base_url('/public/js/merchantbookinglist.js'); ?>"></script>


</head>
<body>

<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<?php $this->load->view('templates/merchant_top');?> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
    
	<?php $this->load->view('templates/merchant_main_left');?>

    <div class="merchant_main_right">
      
      <div class="heading">
        <h1>Current Bookings</h1>
      </div>
      
       <div class="sub_heading">
        <h3>See all of your current bookings in list view. You can select individual bookings to see details. </h3>
      </div>
      
    	<div class="merchant_booking">
                	<div class="filter_box">
                        
						
						<?php echo form_open(base_url()."merchantaccount/bookingslist", array('id'=>'bookingslist_srch_frm'))?>
						<div class="search_box">
							<input type="button" class="search_btn"/>
                        	<input type="text" name="offer_search" id="offer_search" value="<?php if(isset($offer_search)){ echo $offer_search; } ?>" class="search" placeholder="Search" />
					    </div>
						<?php echo form_close()?>
                        
						
						<div class="expand">
                            <a href="javascript:void(0);" id="expand_all" onclick="expand_all_div()">Expand all</a> | <a href="javascript:void(0);" id="collapse_all" onclick="collapse_all_div()">Collapse all</a>
                          </div>
                    </div>
                    
                    
                    <?php if (!$this->agent->is_mobile())
					{
					?>
					
                    
                    
                    <div class="head">
                	<table width="595" border="1" class="table_customer">
                      <tr>
                        <th align="left" scope="col" width="230">Offer</th>
                        <th align="left" scope="col" width="25">Time</th>
                        <th align="left" scope="col" width="40">Quantity</th>
                        <th align="left" scope="col" width="25">Total</th>
                        <th align="left" scope="col" width="35" class="no_bg">Status</th>
                        <th align="left" scope="col" width="18" class="no_bg">&nbsp;</th>
                      </tr>
                      </table>
                      </div>
                      
                      
                      <div class="detail">
                      	<ul> 
							<?php
								$i=0;
								$current_date = strtotime(date('Y-m-d'));
								
								if(count($current_bookings_dates)>0)
								{
									foreach($current_bookings_dates as $current_bookings_val)
									{
										$i++;
										$booking_date = strtotime($current_bookings_val->booking_date);									
										$booking_list_view = $this->users->get_bookings_list_by_date($booking_date);

										
							?>
							<li>                            	
								<div class="datewise">
									<p><?php echo date('D d/m/y',strtotime($current_bookings_val->booking_date)); ?></p>
									<span class="collapse">
									
									<a href="javascript:void(0);" onclick="show_hide_booking_detail('<?=$i?>')" id="date_expand_anchor_<?=$i?>" >+ <?php if($current_date==$booking_date){ echo 'Collapse'; }else{ echo 'Expand'; } ?>
									 </a></span></div>  
									
									<!-- id="collapse_<?=$i?>" -->
									

							<div class="table_customer_data" id="table_customer_data<?=$i?>" <?php if($current_date==$booking_date){ ?> style="display:block" <?php } ?> >                                
                                 <table width="595" border="1" class="table_customer">                                    
									 <?php foreach($booking_list_view as $booking_list_val){ ?>
										 <tr>
											<td align="left" scope="col" width="510" class="one"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></td>
											<td align="left" scope="col" width="90" class="two">
											<?php 
												if($booking_list_val->booking_time!="")
												{
													//echo date("h A", strtotime(stripslashes($booking_list_val->booking_time)));  
													echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time)));  
												}
											?></td>
											<td align="left" scope="col" width="105" class="three"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</td>
											<td align="left" scope="col" width="80" class="four">$<?php echo stripslashes($booking_list_val->total_price);  ?></td>
											<td align="left" scope="col" width="85"  class="book_btn five"><a href="#">Booked</a></td>
											<td align="left" scope="col" width="45" class="five">
											<a href="javascript:void(0);" class="arrow" id="arrow_<?=$booking_list_val->order_id?>" onclick="show_offer_rest_detail(<?=$booking_list_val->order_id?>)"></a>
											</td>                                        
										  </tr>									  
										  <tr class="new_col">
											<td colspan="7">
												<div class="add_calander" id="add_calander_<?=$booking_list_val->order_id?>" style="display:none;">
													<div class="left_img">
													
													<span class="edit_merchant2" id="edit_customer_order_<?=$booking_list_val->order_id?>"><a href="javascript:void(0)" onclick="show_save_order_span(<?=$booking_list_val->order_id?>)"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Edit</a></span>

													
													
													
													<span class="edit_merchant3" id="save_customer_order_<?=$booking_list_val->order_id?>" style="display:none;"><a href="javascript:void(0)" onclick="update_booking_info(<?=$booking_list_val->order_id?>, <?=$booking_list_val->offer_id?>)"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Save</a>
													
                                                    
												
													<!-- <input class="edit_box1" type="" name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>">	 -->								
													
                                                    
                                                  <div class="booking_list_cal">
                                                  	 <!--  <input type="text" class="booking_list" size="30" value="Today" /> -->
													  <input type="text" class="booking_list" readonly name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" />
                                                  </div>
                                                    
                                                    
                                                    
                                                    
													<!-- <input class="edit_box2" type="" name="booking_time_<?=$booking_list_val->order_id?>" id="booking_time_<?=$booking_list_val->order_id?>" value="<?=date("h:i", strtotime($booking_list_val->booking_time))?>" onkeypress="return isNumberKey(event, this.id);" > -->			

													
                                                    <div class="drop_new">
                                                    	<!-- <select class="dropdown">
                                                            <option value="AM">AM</option>
                                                            <option value="PM">PM</option>
                                                        </select> -->														
														<?php 
															echo $time_dropdown = $this->users->get_time_dropdown('booking_time_'.$booking_list_val->order_id,'revostyled',date("h:i", strtotime($booking_list_val->booking_time)),'Select'); 
														?>
                                                    </div>

													
                                                    
                                                    
                                                    
													<select class="revostyled" name="booking_time_ampm_<?=$booking_list_val->order_id?>" id="booking_time_ampm_<?=$booking_list_val->order_id?>">
														<option value="AM" <?php if($booking_list_val->booking_time!='' && date("A", strtotime($booking_list_val->booking_time))=='AM'){ echo "selected"; } ?> >AM</option>
														<option value="PM" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time))=='PM'){ echo "selected"; } ?> >PM</option>
													</select>
                                                    
                                                    
													</span>
                                                    
                                                    <span id="booking_upd_msg_<?=$booking_list_val->order_id?>" style="display:none; padding:5px 0px 0px 15px"></span>

													</div>
													<div class="right_img">
														<p>Booking #: <span><!-- OIELXXXXXXXXX --><?php echo stripslashes($booking_list_val->booking_code);  ?></span> &nbsp;&nbsp;&nbsp;  Booked on: <span><?php echo date('d/m/y',strtotime($booking_list_val->order_date)); ?></span>&nbsp;&nbsp;&nbsp;   Invoice: <span><a href="<?php echo base_url() ?>merchantaccount/invoice/?bid=<?=base64_encode($booking_list_val->order_id)?>&print=yes"  target="_blank">Print</a></span></p>
													</div>
												</div>
											</td>
										 </tr>                                     
										 <tr class="new_col_blank">
											<td colspan="7"><div class="blank_div">&nbsp;</div></td>
										 </tr>
									 <?php } ?>
                                  </table>
                                  </div>
                            </li>
							<?php 
									} 
								}else{
									
									echo '<li>No records found.</li>';

								}

							?>
							
							<input type="hidden" name="totalbookingdates" id="totalbookingdates" value="<?php if(isset($current_bookings_dates)){ echo count($current_bookings_dates); } ?>">
	
                        </ul>
                     
                    </div>
                    
                    
                    
                    
					 <?php	
					}
					else
					{
					
					?>
					   
                      <div class="detail">
                      	<ul> 
							<?php
								$i=0;
								$current_date = strtotime(date('Y-m-d'));
								
								if(count($current_bookings_dates)>0)
								{
									foreach($current_bookings_dates as $current_bookings_val)
									{
										$i++;
										$booking_date = strtotime($current_bookings_val->booking_date);									
										$booking_list_view = $this->users->get_bookings_list_by_date($booking_date);
							?>
							<li>                            	
								<div class="datewise">
									<p><?php echo date('D d/m/y',strtotime($current_bookings_val->booking_date)); ?></p>
									<span class="collapse">
									
									<a href="javascript:void(0);" onclick="show_hide_booking_detail('<?=$i?>')" id="date_expand_anchor_<?=$i?>" >+ <?php if($current_date==$booking_date){ echo 'Collapse'; }else{ echo 'Expand'; } ?>
									 </a></span></div>  
									
									<!-- id="collapse_<?=$i?>" -->
									

							<div class="table_customer_data" id="table_customer_data<?=$i?>" <?php if($current_date==$booking_date){ ?> style="display:block" <?php } ?> >                                
                                 <div class="table_customer">                                    
									 <?php foreach($booking_list_view as $booking_list_val){ ?>
										 <div class="merchant_booking_list">
											<div class="one"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></div>
                                            <div class="five">
											<a href="javascript:void(0);" class="arrow" id="arrow_<?=$booking_list_val->order_id?>" onclick="show_offer_rest_detail(<?=$booking_list_val->order_id?>)"></a>
											</div>
                                            
											<div class="two">
											<?php 
												if($booking_list_val->booking_time!="")
												{
													echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time)));  
												}
											?></div>
											<div class="three"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</div>
											<div class="four">$<?php echo stripslashes($booking_list_val->total_price);  ?></div>
											<div class="book_btn">Status : Booked</div>
                                            
                                            </div> 
                                                                                   
										  								  
										  <div class="add_calander" id="add_calander_<?=$booking_list_val->order_id?>">
													<div class="left_img">
													
													<span class="edit_merchant2" id="edit_customer_order_<?=$booking_list_val->order_id?>"><a href="javascript:void(0)" onclick="show_save_order_span(<?=$booking_list_val->order_id?>)"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Edit</a></span>
													<span class="edit_merchant3" id="save_customer_order_<?=$booking_list_val->order_id?>" style="display:none;"><a href="javascript:void(0)" onclick="update_booking_info(<?=$booking_list_val->order_id?>, <?=$booking_list_val->offer_id?>)"><img src="<?php echo base_url() ?>public/images/edit_merchant.png" alt="" />Save</a>
													
                                                    
												
													<!-- <input class="edit_box1" type="" name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>"> -->
													
													<input type="text" class="booking_list" name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" readonly />
                                                    									
													<!-- <input class="edit_box2" type="" name="booking_time_<?=$booking_list_val->order_id?>" id="booking_time_<?=$booking_list_val->order_id?>" value="<?=date("h:i", strtotime($booking_list_val->booking_time))?>" onkeypress="return isNumberKey(event, this.id);" > -->

													<div class="drop_new">
                                                    	<!-- <select class="dropdown">
                                                            <option value="AM">AM</option>
                                                            <option value="PM">PM</option>
                                                        </select> -->														
														<?php 
															echo $time_dropdown = $this->users->get_time_dropdown('booking_time_'.$booking_list_val->order_id,'revostyled',date("h:i", strtotime($booking_list_val->booking_time)),'Select'); 
														?>
                                                    </div>
                                                    
													<select class="revostyled" name="booking_time_ampm_<?=$booking_list_val->order_id?>" id="booking_time_ampm_<?=$booking_list_val->order_id?>">
														<option value="AM" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time))=='AM'){ echo "selected"; } ?> >AM</option>
														<option value="PM" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time))=='PM'){ echo "selected"; } ?> >PM</option>
													</select>
                                                    
                                                    
													</span>
                                                    
                                                    <span id="booking_upd_msg_<?=$booking_list_val->order_id?>" style="display:none; padding:5px 0px 0px 15px"></span>

													</div>
													<div class="right_img">
														<p>Booking #: <span><!-- OIELXXXXXXXXX --><?php echo stripslashes($booking_list_val->booking_code);  ?></span> &nbsp;&nbsp;&nbsp;  Booked on: <span><?php echo date('d/m/y',strtotime($booking_list_val->order_date)); ?></span>&nbsp;&nbsp;&nbsp;   Invoice: <span><a href="<?php echo base_url() ?>merchantaccount/invoice/?bid=<?=base64_encode($booking_list_val->order_id)?>&print=yes"  target="_blank">Print</a></span></p>
													</div>
												</div>
                                                                          
										 
									 <?php } ?>
                                     </div>	
                                  </div>
                                 
                            </li>
							<?php 
									} 
								}else{
									
									echo '<li>No records found.</li>';

								}

							?>
							
							<input type="hidden" name="totalbookingdates" id="totalbookingdates" value="<?php if(isset($current_bookings_dates)){ echo count($current_bookings_dates); } ?>">
	
                        </ul>
                     
                    </div>
                      
					   
					 <?php
					}
					
					?>
					
                	
                      
                </div>
    			<div class="save_bg">
                        <span class="upper"><a href="javascript:void" id="up"><img src="<?php echo base_url() ?>public/images/orenge_up.png" alt="" /></a></span>
                    </div>
    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->




<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		
	});

</script>

<link rel="stylesheet" href="<?=base_url('/public/assets/ui/themes/base/jquery.ui.all.css'); ?>">
<script src="<?=base_url('/public/assets/ui/jquery.ui.core.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.widget.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect.js'); ?>"></script>
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-blind.js'); ?>"></script>

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-bounce.js'); ?>"></script>

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-clip.js'); ?>"></script> 

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-drop.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-fold.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-slide.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.datepicker.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('/public/assets/ui/demos.css'); ?>">

<!--FOR LOGIN CHECKBOX-->
<link rel="stylesheet" href="<?=base_url('/public/assets/radio/jquery.checkbox.css'); ?>" />
<link rel="stylesheet" href="<?=base_url('/public/assets/radio/jquery.safari-checkbox.css'); ?>" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script> 

<!--FOR GENDER SLIDER-->
<link href="<?=base_url('/public/assets/gender/style.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/public/assets/gender/jquery-ui.js'); ?>" type="text/javascript"></script>

<!--Price Selector content--> 
<link href="<?=base_url('/public/assets/gender/jquery-ui.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>






<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
