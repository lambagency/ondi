<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />


</head>

<body>	
<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">

   <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
	  <?php
	  $user_id = $this->users->get_main_merchant_id();
	  $user_steps_completed = $this->users->user_steps_completed($user_id);
	  if($user_steps_completed<3)
	  {
	  ?>
      <div class="progress_bar_bg">
        <div class="progress_bar">
          <ul>
            <li class="pink"></li>
            <li class="gray2"></li>
            <li class="light_gray"></li>
          </ul>
          <div class="green_pointer"><img alt="" src="<?php echo base_url() ?>public/images/check_green2.png"></div>
        </div>
        <div class="step_number">
          <p>1</p>
        </div>
      </div>
	  <?php
	  }
	  ?>
      <div class="heading">
        <h1>Contact Information</h1>
      </div>
      <div class="sub_heading">
        <h3>Fields marked (*) are mandatory.</h3>
      </div>
     
	<?php echo form_open(base_url()."merchantaccount/contactinfo/")?>
	<input type="hidden" name="contact_action" value="1">
    <div class="merchant_contact">
        <ul>
			
			<li>
				<div class="valid_errors"><?php echo validation_errors(); ?></div>
				<?php //echo form_error('contact_name'); ?>
				<?php //echo form_error('position'); ?>
				<?php //echo form_error('email'); ?>
				<?php //echo form_error('confirm_email'); ?>
				<?php //echo form_error('terms_condition'); ?>
			</li>

            <li><input type="text" value="<?php echo set_value('contact_name', isset($user_data[0]->contact_name)?$user_data[0]->contact_name : ''); ?>" name="contact_name" id="contact_name" class="input_box" placeholder="*Contact Name" /></li>
			
			<li>			
				<select class="revostyled" name="position" id="position" onchange="change_position(this.value);" >
					<option value="" class="label">*Position</option>
					<?php foreach($positiondata as $position){?>
						<option value="<?=$position->position_id?>" <?php if($user_data[0]->position==$position->position_id){?> selected="selected" <?php } ?> ><?=$position->position_value?></option>
					<?php } ?>
					<option value="-1"  <?php if($user_data[0]->position=='-1'){?> selected="selected" <?php } ?>>Other-please specify</option>
				</select>
			</li>

			 <li id="li_position_other" <?php if($user_data[0]->position == '-1'){ echo 'style="display:block;"'; }else{ echo 'style="display:none;"'; } ?> >
				<input type="text" class="input_box" name="position_other" id="position_other" value="<?php echo set_value('position_other', isset($user_data[0]->position_other)?$user_data[0]->position_other : ''); ?>"  />
			</li>

            <li><input type="text" class="input_box" name="email" id="email" value="<?php echo set_value('email', isset($user_data[0]->email)?$user_data[0]->email : ''); ?>" placeholder="*Email" /></li>


			<!-- <li><input type="text" class="input_box" name="confirm_email" id="confirm_email" value="<?php echo set_value('confirm_email', isset($user_data[0]->confirm_email)?$user_data[0]->confirm_email : ''); ?>" placeholder="*Confirm Email" /></li> -->


            <li><input type="text" class="input_box" name="phone" id="phone" value="<?=$user_data[0]->phone?>" placeholder="*Phone" /></li>			
            
			<li><input name="terms_condition" id="terms_condition" type="checkbox" value="1" <?php if(isset($user_data[0]->terms_condition) && $user_data[0]->terms_condition=='1'){ echo 'checked';  } ?>> <span class="agree">I agree with the Ondi terms and conditions</span></li>	
			
        </ul>
    </div>    
    <div class="save_bg"> 
		<span class="save">
			<!-- <a href="#"><img src="<?php echo base_url() ?>public/images/saved_continue.jpg" ></a> -->
			<?php 
			$user_id = $this->users->get_main_merchant_id();
			$user_steps_completed = $this->users->user_steps_completed($user_id);
			if($user_steps_completed>2){?>
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/user_save.jpg">
			<?php } else { ?>
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/saved_continue.jpg">
			<?php } ?>
		</span>  
	</div>
	<?php echo form_close()?>


    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->


<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>


<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		
	});

</script>





<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<script type="text/javascript">
		$(function(){
				
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:checkbox').screwDefaultButtons({
				image: 'url("<?=base_url('/public/images/agree.jpg'); ?>")',
				width: 29,
				height: 29
			});	
			

		});
</script>




</body>

</html>
