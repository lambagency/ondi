<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />


<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
<link href="<?=base_url('/public/css/media.css'); ?>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=base_url('/public/js/merchantbookinglist.js'); ?>"></script>
</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/merchant_top');?>

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
    <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right calender_view">
      <div class="heading">
        <h1>Calendar view</h1>
      </div>
      <div class="sub_heading">
        <h3>See all of your bookings in calendar view. You can select individual biikings to see details.</h3>
      </div>
      <div class="calendar_view">
      
      
      	<?php if (!$this->agent->is_mobile())
		{
		?>
        
        <div class="head">
          	<h2><?php echo date('F Y', strtotime( date('Y-m-d'))); ?></h2>
        </div>
          
          
        <div class="cal_detail">
          <table width="595" border="1" class="table_customer">
            <tr>
              <?php						
						//echo $current_date = date('Y-m-d');					
						//$date_after_7_day = date('Y-m-d',strtotime("+7 day", strtotime($current_date)));	
						
						$i=0;						
						for($booking_date=strtotime(date('Y-m-d')); $booking_date<strtotime("+7 day", strtotime(date('Y-m-d'))); $booking_date)
						{
							//echo date('Y-m-d', $booking_date);
							$i++;
							
							$count_merchant_offers = $this->users->count_merchant_offers_by_date($booking_date);
							
							$booking_list_view = $this->users->get_bookings_list_by_date($booking_date); 
							//$booking_date = strtotime("+1 day", $booking_date);

							if($i==6)
							{
								$td_class = 'class="sat"';
								
							}else if($i==7){

								$td_class = 'class="sun"';
								
							}else{
								
								$td_class = '';

							}
							
							//echo $count_merchant_offers->num_rows;
							
				  ?>
                  
              <td valign="top" <?=$td_class?> >
                <div class="calander_day">
                  <h2><?php echo date('D d',$booking_date); ?></h2>
                  <ul>
                    <?php foreach($booking_list_view as $booking_list_val){	?>
                    <li> <a href="#">(<?php echo stripslashes($booking_list_val->number_of_people);  ?>) <span>
                      <?php 
                                                $name = stripslashes($booking_list_val->first_name).' '.stripslashes($booking_list_val->last_name);
                                                if(strlen($name)>5)
                                                { echo substr($name,0,5).'...'; }else{ echo $name; } echo $booking_list_val->offer_title;
                                            ?>
                      </span>
                      <?php	
						  if($booking_list_val->booking_time!="")
						  {
									//echo $booking_time = date("hA", strtotime($booking_list_val->booking_time));
									echo $booking_time = date("h:i A", strtotime($booking_list_val->booking_time));
						  }						
                      ?>
                      </a>
                      <div class="calander_submenu" >
                        <div class="cal_head">
                          <h4><?php echo date('l, M dS',$booking_date); ?></h4>
                        </div>
                        <span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/orenge_arrow.png" alt="" /></span>
                        <ul>
                          <li>
                            <h5>(
                              <?=count($booking_list_view)?>
                              /
                              <?=$count_merchant_offers?>
                              Bookings)</h5>
                          </li>
                          <li>
                            <p>(<?php echo stripslashes($booking_list_val->number_of_people);  ?>pax) <?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?><br/>
                              <strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong><br/>
                              Total: <strong>$<?php echo stripslashes($booking_list_val->total_price); ?></strong><br/>
                              Reference: <?php echo stripslashes($booking_list_val->booking_code);  ?></p>
                          </li>
                          <li id="edit_customer_order_<?=$booking_list_val->order_id?>"><span class="cal_save"><a href="javascript:void(0)" onclick="show_save_order_span(<?=$booking_list_val->order_id?>)"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_merchant.png" alt="" /> Edit</a></span></li>
                          
						  <li id="save_customer_order_li_<?=$booking_list_val->order_id?>" style="display:none;"> <span id="booking_upd_msg_<?=$booking_list_val->order_id?>" style="display:block; padding:5px 0px 0px 15px"></span> <span class="cal_time">
                            
							
							<input class="time" type="text" name="booking_time_<?=$booking_list_val->order_id?>" id="booking_time_<?=$booking_list_val->order_id?>" value="<?php if($booking_list_val->booking_time!=""){ echo date("h:i", strtotime($booking_list_val->booking_time)); } ?>" onkeypress="return isNumberKey(event, this.id);">
                            
							<!-- <div class="drop_new">                            	  
								<?php 
									echo $time_dropdown = $this->users->get_time_dropdown('booking_time_'.$booking_list_val->order_id,'revostyled',date("h:i", strtotime($booking_list_val->booking_time)),'Select'); 
								?>
                            </div> -->

                            
                            <select class="dropdown" name="booking_time_ampm_<?=$booking_list_val->order_id?>" id="booking_time_ampm_<?=$booking_list_val->order_id?>">
                              <option value="am" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time)) == 'AM'){ echo "selected"; } ?> >AM</option>
                              <option value="pm" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time))  == 'PM'){ echo "selected"; } ?> >PM</option>
                            </select>
                            </span> 
                            
                              
							  
							  <!-- <div class="booking_list_cal">
								  <input class="booking_list" type="text" readonly name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" onclick="select_date('<?=$booking_list_val->order_id?>')">
                              </div> -->
                              
                              
                            <span class="cal_date">                            
								<input class="date" type="" name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" onclick="select_date('<?=$booking_list_val->order_id?>')">                          
                            </span>
							</li>
                          <li id="save_customer_order_btn_li_<?=$booking_list_val->order_id?>" style="display:none;" ><span class="cal_save"> <a href="javascript:void(0)" onclick="update_booking_info(<?=$booking_list_val->order_id?>, <?=$booking_list_val->offer_id?>)"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_merchant.png" alt="" /> Save</a></span></li>
                        </ul>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
              </td>
              <?php 
					$booking_date = strtotime("+1 day", $booking_date);					
					} ?>
            </tr>
            <?php 
						
					

			?>
          </table>
        </div>
        
        
        
        
		
		 <?php	
		}
		else
		{
		
		?>
		   
           
           <div class="cal_detail">
          <div class="table_customer">
            <div class="calender_list">
              <?php						
						//echo $current_date = date('Y-m-d');					
						//$date_after_7_day = date('Y-m-d',strtotime("+7 day", strtotime($current_date)));	
						
						$i=0;						
						for($booking_date=strtotime(date('Y-m-d')); $booking_date<strtotime("+7 day", strtotime(date('Y-m-d'))); $booking_date)
						{
							//echo date('Y-m-d', $booking_date);
							$i++;
							
							$count_merchant_offers = $this->users->count_merchant_offers_by_date($booking_date);
							
							$booking_list_view = $this->users->get_bookings_list_by_date($booking_date); 
							//$booking_date = strtotime("+1 day", $booking_date);

							if($i==6)
							{
								$td_class = 'sat';
								
							}else if($i==7){

								$td_class = 'sun';
								
							}else{
								
								$td_class = '';

							}
							
							//echo $count_merchant_offers->num_rows;
							
				  ?>
                  
              <div  class="days_list <?=$td_class?>" >
                <div class="calander_day">
                  <h2><?php echo date('D d',$booking_date); ?></h2>
                  <ul>
                    <?php foreach($booking_list_view as $booking_list_val){	?>
                    <li> <a href="#">(<?php echo stripslashes($booking_list_val->number_of_people);  ?>) <span>
                      <?php 
                                                $name = stripslashes($booking_list_val->first_name).' '.stripslashes($booking_list_val->last_name);
                                                if(strlen($name)>5)
                                                { echo substr($name,0,5).'...'; }else{ echo $name; } echo $booking_list_val->offer_title;
                                            ?>
                      </span>
                      <?php	
						  if($booking_list_val->booking_time!="")
						  {
									// echo $booking_time = date("hA", strtotime($booking_list_val->booking_time)); 
									echo $booking_time = date("h:i A", strtotime($booking_list_val->booking_time)); 
						  }						
                      ?>
                      </a>
                      <div class="calander_submenu" style="display:;">
                        <div class="cal_head">
                          <h4><?php echo date('l, M dS',$booking_date); ?></h4>
                        </div>
                        <span class="triangle"><img src="<?php echo $this->config->item('base_url'); ?>public/images/orenge_arrow.png" alt="" /></span>
                        <ul>
                          <li>
                            <h5>(
                              <?=count($booking_list_view)?>
                              /
                              <?=$count_merchant_offers?>
                              Bookings)</h5>
                          </li>
                          <li>
                            <p>(<?php echo stripslashes($booking_list_val->number_of_people);  ?>pax) <?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?><br/>
                              <strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong><br/>
                              Total: <strong>$<?php echo stripslashes($booking_list_val->total_price); ?></strong><br/>
                              Reference: <?php echo stripslashes($booking_list_val->booking_code);  ?></p>
                          </li>
                          <li id="edit_customer_order_<?=$booking_list_val->order_id?>"><span class="cal_save"><a href="javascript:void(0)" onclick="show_save_order_span(<?=$booking_list_val->order_id?>)"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_merchant.png" alt="" /> Edit</a></span></li>
                          <li id="save_customer_order_li_<?=$booking_list_val->order_id?>" style="display:none;"> <span id="booking_upd_msg_<?=$booking_list_val->order_id?>" style="display:none; padding:5px 0px 0px 15px"></span> <span class="cal_time">
                            
							
							<input class="time" type="text" name="booking_time_<?=$booking_list_val->order_id?>" id="booking_time_<?=$booking_list_val->order_id?>" value="<?php if($booking_list_val->booking_time!=""){ echo date("h:i", strtotime($booking_list_val->booking_time)); } ?>" onkeypress="return isNumberKey(event, this.id);">

							
							<!-- <div class="drop_new">                            	 
								<?php 
									echo $time_dropdown = $this->users->get_time_dropdown('booking_time_'.$booking_list_val->order_id,'revostyled',date("h:i", strtotime($booking_list_val->booking_time)),'Select'); 
								?>
                            </div> -->
                            
							
							<select class="dropdown" name="booking_time_ampm_<?=$booking_list_val->order_id?>" id="booking_time_ampm_<?=$booking_list_val->order_id?>">
                              <option value="am" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time)) == 'AM'){ echo "selected"; } ?> >AM</option>
                              <option value="pm" <?php if($booking_list_val->booking_time!="" && date("A", strtotime($booking_list_val->booking_time)) == 'PM'){ echo "selected"; } ?> >PM</option>
                            </select>
                            </span> 
							
							
							<!-- <div class="booking_list_cal">                                 
								  <input class="booking_list" type="text" readonly name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" onclick="select_date('<?=$booking_list_val->order_id?>')">
                           </div> -->
							
							
								<span class="cal_date">                            
								<input class="date" type="" name="booking_date_<?=$booking_list_val->order_id?>" id="booking_date_<?=$booking_list_val->order_id?>" value="<?php echo date('d/m/Y',strtotime($booking_list_val->booking_date)); ?>" onclick="select_date('<?=$booking_list_val->order_id?>')">
								</span>
							</li>
                          <li id="save_customer_order_btn_li_<?=$booking_list_val->order_id?>" style="display:none;" ><span class="cal_save"> <a href="javascript:void(0)" onclick="update_booking_info(<?=$booking_list_val->order_id?>, <?=$booking_list_val->offer_id?>)"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_merchant.png" alt="" /> Save</a></span></li>
                        </ul>
                      </div>
                    </li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
              <?php 
					$booking_date = strtotime("+1 day", $booking_date);					
					} ?>
            </div>
            <?php 
						
					

			?>
          </div>
        </div>
          
           
        
        
		   
		 <?php
		}
		
		?>
		
		
        
        
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('templates/footer');?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script> 

<!--FOR WHATS HOTS SECTION--> 

<script src='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.js' type='text/javascript'></script> 
<script type='text/javascript'>
	$(function() {
		$('.kwicks').kwicks({
			maxSize: '30%',
			behavior: 'menu'
		});
	});
</script> 

<!--FOR DATE PICKER--> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<script>
$(function() {
		$( "#datepickerj" ).datepicker();
		$( "#anim" ).change(function() {
			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
		});
	});
</script> 

<!--FOR DATE PICKER-->

<? //=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>



<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		
	});

</script>

<link rel="stylesheet" href="<?=base_url('/public/assets/ui/themes/base/jquery.ui.all.css'); ?>">
<script src="<?=base_url('/public/assets/ui/jquery.ui.core.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.widget.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect.js'); ?>"></script>
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-blind.js'); ?>"></script>

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-bounce.js'); ?>"></script>

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-clip.js'); ?>"></script> 

<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-drop.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-fold.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.effect-slide.js'); ?>"></script> 
<script src="<?=base_url('/public/assets/ui/jquery.ui.datepicker.js'); ?>"></script>
<link rel="stylesheet" href="<?=base_url('/public/assets/ui/demos.css'); ?>">

<!--FOR LOGIN CHECKBOX-->
<link rel="stylesheet" href="<?=base_url('/public/assets/radio/jquery.checkbox.css'); ?>" />
<link rel="stylesheet" href="<?=base_url('/public/assets/radio/jquery.safari-checkbox.css'); ?>" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script> 

<!--FOR GENDER SLIDER-->
<link href="<?=base_url('/public/assets/gender/style.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/public/assets/gender/jquery-ui.js'); ?>" type="text/javascript"></script>

<!--Price Selector content--> 
<link href="<?=base_url('/public/assets/gender/jquery-ui.css'); ?>" rel="stylesheet" type="text/css"/>
<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
