<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

</head>
<body>

<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/merchant_top');?>

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">
    <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
      
      <div class="heading_alone">
        <h1>Statistics</h1>
      </div>
     
    	<div class="merchant_statistic">
        
        	<?php if ($this->agent->is_mobile())
			{
			?>
            
       
                      
                      <div class="detail">
                      
                      	<ul>
                        	<li class="s_green">Number of offers run</li>
                            <li><?=$count_last_30days_offers?></li>
                            <li><?=$count_lifetime_offers?></li>
                        </ul>
                        <ul>
                        	<li class="s_orenge">Number of sales</li>
                            <li><?=$count_last_30days_sales?></li>
                            <li><?=$count_lifetime_sales?></li>
                        </ul>
                        <ul>
                        	<li class="s_red">Conversion Rate</li>
                            <li><?php if(isset($conversion_rate_30days)){ echo $conversion_rate_30days; } ?>%</li>
                            <li><?php if(isset($conversion_rate_lifetime)){ echo $conversion_rate_lifetime; } ?>%</li>
                        </ul>
                        <ul>
                        	<li class="s_blue">Total Revenue</li>
                            <li>$<?=$count_last_30days_revenue?></li>
                            <li>$<?=$count_lifetime_revenue?></li>
                        </ul>
                        <ul>
                        	<li class="s_pink">Average order value</li>
                            <li>$<?php echo $count_last_30days_revenue/$count_last_30days_sales; ?></li>
                            <li>$<?php echo $count_lifetime_revenue/$count_lifetime_sales; ?></li>
                        </ul>
                        <ul>
                        	<li class="s_yellow">Average commission</li>
                            <li><?php if(isset($average_commission_30days)){ echo $average_commission_30days; } ?>%</li>
                            <li><?php if(isset($average_commission_lifetime)){ echo $average_commission_lifetime; } ?>%</li>
                        </ul>
                      </div>
                      
			
			 <?php	
			}
			else
			{
			
			?>
            
            
                  <div class="detail"> 
						<div class="head">
						<table width="595" border="1" class="table_customer">
						  <tbody><tr>
							<th width="33%" align="left" scope="col">&nbsp;</th>
							<th width="33%" align="left" scope="col">Last 30 days</th>
							<th width="33%" align="left" class="no_bg" scope="col">Lifetime</th>
						  </tr>
						  </tbody></table>
						  </div>
                      	<ul>
                        	<li class="s_green">Number of offers run</li>
                            <li><?=$count_last_30days_offers?></li>
                            <li><?=$count_lifetime_offers?></li>
                        </ul>
                        <ul>
                        	<li class="s_orenge">Number of sales</li>
                            <li><?=$count_last_30days_sales?></li>
                            <li><?=$count_lifetime_sales?></li>
                        </ul>
                        <ul>
                        	<li class="s_red">Conversion Rate</li>
                            <li><?php if(isset($conversion_rate_30days)){ echo $conversion_rate_30days; } ?>%</li>
                            <li><?php if(isset($conversion_rate_lifetime)){ echo $conversion_rate_lifetime; } ?>%</li>
                        </ul>
                        <ul>
                        	<li class="s_blue">Total Revenue</li>
                            <li>$<?=$count_last_30days_revenue?></li>
                            <li>$<?=$count_lifetime_revenue?></li>
                        </ul>
                        <ul>
                        	<li class="s_pink">Average order value</li>
                            <li>$<?php echo number_format($count_last_30days_revenue/$count_last_30days_sales, 2, '.', ''); ?></li>
                            <li>$<?php echo number_format($count_lifetime_revenue/$count_lifetime_sales, 2, '.', ''); ?></li>
                        </ul>
                        <ul>
                        	<li class="s_yellow">Average commission</li>
                            <li><?php if(isset($average_commission_30days)){ echo $average_commission_30days; } ?>%</li>
                            <li><?php if(isset($average_commission_lifetime)){ echo $average_commission_lifetime; } ?>%</li>
                        </ul>
                  </div>
			   
			 <?php
				}
			 ?>
                  
                </div>
                
                
                
                
                
                
                
                  <div class="heading_alone">
                    <h1>Most Recent Bookings <?php if($recent_booking_date != ''){?><span><?php echo date('l d/m/y',strtotime($recent_booking_date)); ?></span><?php } ?></h1>
                  </div>
     
     
     
     	<?php if (!$this->agent->is_mobile())
		{
		?>
		
        <div class="merchant_statistic">
                	
                	<div class="head2">
                	<table width="595" border="1" class="table_customer">
                      <tr>
                        <th align="left" scope="col" width="270" class="first">Offer</th>
                        <th align="left" scope="col" width="20">Time</th>
                        <th align="left" scope="col" width="25">Quantity</th>
                        <th align="left" scope="col" width="15">Total</th>
                        <th align="left" scope="col" width="50" class="no_bg">Status</th>
                      </tr>
                      </table>
                      </div>
                      
                     <div class="detail">
                      <table width="595" border="1" class="table_customer">
                      
					  <tr class="new_col_blank">
                         <td colspan="5"><div class="blank_div">&nbsp;</div></td>
                      </tr>
					  <?php foreach($booking_list_view as $booking_list_val){ ?>
					  <tr>
                        <td align="left" scope="col" width="168" class="first"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></td>
                        <td align="left" scope="col" width="15" class="second"><?php echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time))) ; ?><?php //echo stripslashes($booking_list_val->booking_time_ampm); ?></td>
                        <td align="left" scope="col" width="30" class="third"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</td>
                        <td align="left" scope="col" width="20" class="fourth">$<?php echo stripslashes($booking_list_val->total_price); ?></td>
                        <td align="left" scope="col" width="45" class="book_btn"><a href="<?php echo $this->config->item('base_url'); ?>merchantaccount/bookingslist">Booked</a></td>
                      </tr>


                      <tr class="new_col_blank">
                          <td colspan="5"><div class="blank_div">&nbsp;</div></td>
                      </tr>
					  <?php } ?>


                      



                      </table>
                          
                      </div>
                     
                </div>
                
		 <?php	
		}
		else
		{
		
		?>
		   <div class="merchant_statistic">
                	
                	 <div class="detail">
                      <div class="table_customer">
                      
					  <?php foreach($booking_list_view as $booking_list_val){ ?>
					  <div class="merchant_statistic_list">
                        <div class="first"><?php echo stripslashes($booking_list_val->first_name); ?> <?php echo stripslashes($booking_list_val->last_name);  ?> <br/><strong><?php echo stripslashes($booking_list_val->offer_title);  ?> $<?php echo stripslashes($booking_list_val->offerprice);  ?></strong></div>
                        <div class="second"><?php echo date("h:i A", strtotime(stripslashes($booking_list_val->booking_time))) ; ?><?php //echo stripslashes($booking_list_val->booking_time_ampm); ?></div>
                        <div class="third"><?php echo stripslashes($booking_list_val->number_of_people);  ?> pax</div>
                        <div class="fourth">$<?php echo stripslashes($booking_list_val->total_price); ?></div>
                        <div class="book_btn">Status : <a href="<?php echo $this->config->item('base_url'); ?>merchantaccount/bookingslist">Booked</a></div>
                      </div>

					  <?php } ?>


                      </div>
                          
                      </div>
                     
                </div>
		   
		 <?php
		}
		
		?>
		
    			
                
                <div class="heading_alone">
                    <h1>Best Sellers</h1>
                  </div>
                  
                  
                  
                  	<?php if (!$this->agent->is_mobile())
					{
					?>
                    
                        <div class="merchant_statistic">
                            
                            <div class="head3">
                            <table width="595" border="1" class="table_customer">
                              <tr>
                                <th align="left" scope="col" width="240">Offer</th>
                                <th align="left" scope="col" width="175">Date/s run</th>
                                <th align="left" scope="col" width="52">Consumption</th>
                                <th align="left" scope="col" width="60" class="no_bg">Rerun</th>
                              </tr>
                              </table>
                              </div>
                              
                             <div class="detail">
                              <table width="595" border="1" class="table_customer">
                                  <tr class="new_col_blank">
                                                <td colspan="4"><div class="blank_div">&nbsp;</div></td>
                                              </tr>
                                  
                                  
                                  <?php 
                                      $total_consumption = 0;						 
                                      foreach($merchant_best_sellers as $merchant_best_sell_val)
                                      { 
                                            //$total_consumption = ($merchant_best_sell_val->total_offer_count/$count_merchant_best_sellers) * 100;
                                            //$consumption=$merchant_best_sell_val->consumption;
                                            // $merchant_best_sell_val->offer_id;
                                            //$merchant_best_sell_val->offer_id;
                                          
                                          $total_number_of_people = $this->users->number_of_people_booked($merchant_best_sell_val->offer_id);								 
                                          if($total_number_of_people=="")
                                          {
                                            $total_number_of_people=0;
                                          }	
                                          $total_quantity = $this->users->total_quantity_available($merchant_best_sell_val->offer_id);
                                          if($total_quantity=='')
                                          {
                                            $total_quantity=0;
                                          }
                                          $consumption = round(($total_number_of_people/$total_quantity)*100);
        
                                 ?>
                                  <tr>
                                    <td align="left" scope="col" width="130"><strong><?php echo stripslashes($merchant_best_sell_val->offer_title);  ?>  $<?php echo stripslashes($merchant_best_sell_val->merchant_offer_price);  ?></strong></td>
                                    <td align="left" scope="col" width="100">
                                    <?php
                                        $offer_running_from = strtotime($merchant_best_sell_val->offer_running_from);
                                        $offer_running_to = strtotime($merchant_best_sell_val->offer_running_to);
                                        if($offer_running_from==$offer_running_to)
                                        {
                                            echo date('D d/m/y',$offer_running_from);
        
                                        }else{
        
                                            echo date('D d/m/y',$offer_running_from); echo ' To '; echo date('D d/m/y',$offer_running_to);
                                        }
                                    ?>
                                    </td>
                                    <td align="left" scope="col" width="45"><span class="big_text"><?=$consumption?>%</span><br/><em>(<?=$total_number_of_people?>/<?=$total_quantity?>)</em></td>
                                    <td align="left" scope="col" width="50" class="book_btn5"><a href="<?php echo base_url() ?>merchantaccount/addoffers/?offer_id=<?=$merchant_best_sell_val->offer_id?>&act=rerun"><img src="<?php echo $this->config->item('base_url');?>public/images/rerun_img.png" alt="" />Rerun Offer</a></td>
                                    </tr>
                                    <tr class="new_col_blank">
                                                <td colspan="4"><div class="blank_div">&nbsp;</div></td>
                                     </tr>
                                  <?php } ?>
                                  
                              </table>
                                  
                              </div>
                             
                        </div>
                
					
					 <?php	
					}
					else
					{
					
					?>
					   <div class="merchant_statistic">
                	
                       
                     <div class="detail">
                          <div class="table_customer">
                              
                              
                              <?php 
                                  $total_consumption = 0;						 
                                  foreach($merchant_best_sellers as $merchant_best_sell_val)
                                  { 
                                        //$total_consumption = ($merchant_best_sell_val->total_offer_count/$count_merchant_best_sellers) * 100;
                                        //$consumption=$merchant_best_sell_val->consumption;
                                        // $merchant_best_sell_val->offer_id;
                                        //$merchant_best_sell_val->offer_id;
                                      
                                      $total_number_of_people = $this->users->number_of_people_booked($merchant_best_sell_val->offer_id);								 
                                      if($total_number_of_people=="")
                                      {
                                        $total_number_of_people=0;
                                      }	
                                      $total_quantity = $this->users->total_quantity_available($merchant_best_sell_val->offer_id);
                                      if($total_quantity=='')
                                      {
                                        $total_quantity=0;
                                      }
                                      $consumption = round(($total_number_of_people/$total_quantity)*100);
    
                             ?>
                              <div class="merchant_statistic_list">
                                <div align="left" scope="col" width="130"><strong><?php echo stripslashes($merchant_best_sell_val->offer_title);  ?>  $<?php echo stripslashes($merchant_best_sell_val->merchant_offer_price);  ?></strong></div>
                                <div align="left" scope="col" width="100">
                                <?php
                                    $offer_running_from = strtotime($merchant_best_sell_val->offer_running_from);
                                    $offer_running_to = strtotime($merchant_best_sell_val->offer_running_to);
                                    if($offer_running_from==$offer_running_to)
                                    {
                                        echo date('D d/m/y',$offer_running_from);
    
                                    }else{
    
                                        echo date('D d/m/y',$offer_running_from); echo ' To '; echo date('D d/m/y',$offer_running_to);
                                    }
                                ?>
                                </div>
                                <div align="left" scope="col" width="45"><span class="big_text"><?=$consumption?>%</span><br/><em>(<?=$total_number_of_people?>/<?=$total_quantity?>)</em></div>
                                <div align="left" scope="col" width="50" class="book_btn5"><a href="<?php echo base_url() ?>merchantaccount/addoffers/?offer_id=<?=$merchant_best_sell_val->offer_id?>&act=rerun"><img src="<?php echo $this->config->item('base_url');?>public/images/rerun_img.png" alt="" />Rerun Offer</a></div>
                                </div>
                              
                              <?php } ?>
    
    
    
                              
    
    
    
                          </div>
                              
                          </div>
                         
                    </div>
					   
					 <?php
					}
					
					?>
    
    
     
    			
                
                
    			<div class="save_bg5">
                    <span class="upper">
                        <a href="javascript:void" id="up"><img src="<?php echo $this->config->item('base_url'); ?>public/images/orenge_up.png" alt="" /></a>
                    </span>
                </div>
    </div>
  </div>
</div>

<?php $this->load->view('templates/footer');?>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>


</body>
</html>