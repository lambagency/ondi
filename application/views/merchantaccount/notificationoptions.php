<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?></head>

<body>	
<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">

   <?php $this->load->view('templates/merchant_main_left');?>
    <div class="merchant_main_right">
      
      <div class="heading">
        <h1>Notification options</h1>
      </div>
      
	  <!-- <div class="sub_heading">
        <h3>Fields marked (*) are mandatory.</h3>
      </div> -->
     
	<?php echo form_open(base_url()."merchantaccount/notificationoptions/")?>
	<input type="hidden" name="notification_action" value="1">
    <div class="merchant_contact">
        <ul>
			
			<li>
				<?php echo validation_errors(); ?>				
			</li>

           
				<?php foreach($notification_options as $notification_options_val){ ?>
					 <li>
						<input type="checkbox" value="<?=$notification_options_val->id;?>" name="notification_options[]" <?php if(count($merchant_notification_options)>0){ foreach($merchant_notification_options as $val){ if($notification_options_val->id==$val){ echo "checked"; } } } ?> > <?=$notification_options_val->option_title;?> &nbsp;&nbsp; 
					
					</li>
				<?php } ?>

				<!-- <input type="text" value="<?php echo set_value('contact_name', isset($user_data[0]->contact_name)?$user_data[0]->contact_name : ''); ?>" name="contact_name" id="contact_name" class="input_box" placeholder="*Contact Name" /> -->            
				
			
        </ul>
    </div>    
    <div class="save_bg"> 
		<span class="save">
			<!-- <a href="#"><img src="<?php echo base_url() ?>public/images/saved_continue.jpg" ></a> -->
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/user_save.jpg" name="submit">			
		</span>  
	</div>
	<?php echo form_close()?>


    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->

<script type="text/javascript" src="<?=base_url('/public/assets/js/jquery.js'); ?>"></script>
<script src="<?=base_url('/public/assets/check/jquery.screwdefaultbuttonsV2.js'); ?>"></script>


<script type="text/javascript">
		$(function(){
				
			$('.merchant_main .merchant_main_right .merchant_contact ul li input:checkbox').screwDefaultButtons({
				image: 'url("<?=base_url('/public/images/agree.jpg'); ?>")',
				width: 29,
				height: 29
			});	
			

		});
</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>

</html>
