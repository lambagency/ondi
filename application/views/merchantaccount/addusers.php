<?php
/*
echo '<pre>';
print_r($access_type_result);
echo '</pre>';
*/

/*
echo '<pre>';
print_r($merchant_user_data);
echo '</pre>';
*/

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?><script type="text/javascript" src="<?=base_url('/public/js/common.js'); ?>"></script>
</head>

<body>	
<!--Start of nav Section-->
<?php $this->load->view('templates/header');?>
<!--End of nav_selected Section--> 

<!--Start of customer top common Section-->
<?php $this->load->view('templates/merchant_top');?>
<!--End of customer top common  Section--> 

<!--Start of customer Area-->

<div class="merchant_main_con">
  <div class="merchant_main">

   <?php $this->load->view('templates/merchant_main_left');?>
    
	<div class="merchant_main_right">
		
      <div class="heading">
        <h1>Users</h1>
      </div>
      <div class="sub_heading">
        <h3>People in your account and what they can access.</h3>
      </div>
      <div class="merchant_users_edit">
        <div class="head">
          <table width="595" border="1" class="table_customer">
            <tr>
              <th align="left" scope="col" width="130">Person</th>
              <th align="left" scope="col" width="110" class="no_bg">Permission</th>
              <th align="left" scope="col" width="40" class="no_bg">&nbsp;</th>
            </tr>
          </table>
        </div>
      </div>	  
	  
	  <?php echo form_open(base_url()."merchantaccount/addusers/")?>
	  <input type="hidden" name="add_user_act" value="add">
	  <input type="hidden" name="muid" id="muid" value="<?php if(isset($muid)){ echo $muid; } ?>">
	  <div class="merchant_users_edit_detail">
		
		<?php 
			echo validation_errors();			
			echo $this->session->flashdata('email_already_exist');
		?>
        <ul>		  
          <li>
            <input name="user_email" id="user_email_1" type="text" class="person" placeholder="*Email" value="<?php if(isset($merchant_user_data[0]->email)){ echo stripslashes($merchant_user_data[0]->email); } ?>">
          </li>
          <li>
            <input name="user_password" id="user_password_1" type="password" class="person_email" <?php if(isset($merchant_user_data[0]->password) && $merchant_user_data[0]->password!=""){ ?> placeholder="Password" <?php }else{ ?> placeholder="*Password" <?php } ?> >
          </li>

          <li class="select_div">
            <select name="access_type" id="access_type_1" class="dropdown" onchange="show_access_options(this.value)">
			  <?php $i=1; foreach($access_type_result as $access_type_val){ ?>
              <option value="<?php echo $access_type_val->id; ?>" <?php if( isset($merchant_user_data[0]->user_access_type) && $merchant_user_data[0]->user_access_type == $access_type_val->id  ){ ?> selected  <?php } ?> ><?php echo stripslashes($access_type_val->access_type_title); ?></option>
			  <?php $i++; } ?>              
            </select>
          </li>

          <li class="paragraph" id="accessmsg" >
			  <?php if($merchant_user_data[0]->user_access_type == '1'){?>
				This user will be able to see/add/edit everything	
			  <?php } else if($merchant_user_data[0]->user_access_type == '2'){?>
				This user will be able to see/add/edit offers, bookings and stats
			  <?php } else if($merchant_user_data[0]->user_access_type == '1'){?>
				Select what sections of this merchant area this user will be able to see/add/edit.
			  <?php } ?>
		  </li>
        </ul>

	  <!-- Access Type Section -->	  
	  <div class="access_options" id="access_options_id"></div>
	  <!-- Access Type Section -->

        <div class="save">
          <div class="but"> 
			<!-- <img src="<?php echo base_url() ?>public/images/user_save.jpg" > -->
			<input type="image" name="submit" src="<?php echo base_url() ?>public/images/user_save.jpg">
		</div>
          <div class="text"> or <a href="<?php echo base_url("merchantaccount/userslist/"); ?>">Cancel</a></div>
        </div>
      </div>
	  <?php echo form_close()?>	        

	</div>

  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
<!--End of footer_bg-->

<script>
function show_access_options2(id)
{

	alert(id);
}

</script>

<?php 
	if(isset($merchant_user_data[0]->user_access_type) && $merchant_user_data[0]->user_access_type=='3')
	{		
		$user_access_sections_str = $merchant_user_data[0]->user_access_sections;
?>
<script>
show_access_options('3','<?=$user_access_sections_str?>');
</script>
<?php } ?>

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script src="assets/check/jquery.screwdefaultbuttonsV2.js"></script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>
