<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php if(isset($title)){ echo $title; } ?></title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?=base_url('/public/css/style.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('/public/css/reset.css'); ?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('/public/js/common.js'); ?>"></script>
<!--[if lte IE 6]><link rel="stylesheet" href="<?=base_url('/public/css/ie6.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?=base_url('/public/css/ie7.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?=base_url('/public/css/ie8.css'); ?>" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?=base_url('/public/css/ie9.css'); ?>" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?=base_url('/public/assets/js/html5.js'); ?>"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/payment_easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>


<!-- <link rel="stylesheet" type="text/css" href="<?=base_url('/public/assets/select/payment_easydropdown.css'); ?>"/>
<script src="<?=base_url('/public/assets/select/jquery.easydropdown.js'); ?>"></script> -->
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
</head>
<body>
	<?php $this->load->view('templates/header');?>
	<?php $this->load->view('templates/merchant_top');?>
	<!--Start of customer Area-->
	<div class="merchant_main_con">
		<div class="merchant_main customer_main">    
			<?php $this->load->view('templates/merchant_main_left');?>
			<div class="merchant_main_right">
				<div class="customer_main_right">
					<div class="heading">
						<h1>Business Profile</h1>
					</div>
					<div class="member_detail">
						<ul>
							<li>
                        	<div class="photo_left">
                            	<h3><?=$userdata[0]->business_name?></h3>
                                <p>
									<?=$userdata[0]->website_address?><br />
									<?=$userdata[0]->street_address?><br />
									<?=$userdata[0]->suburb?><br />
									<?=$userdata[0]->post_code?><br />
									<?=$userdata[0]->state_name?><br />
									<?=$userdata[0]->business_phone?><br />
									<?=$userdata[0]->mobile_number?><br />
								</p>
                            </div>
                        </li>
                    </ul>
            </div>
            
            <div class="add_social">
				<ul>
				  <li> <img src="<?php echo $this->config->item('base_url'); ?>public/images/add_facebook.jpg">
					<input type="text" name="facebook" class="input_box_readonly" value="<?php echo set_value('facebook', (isset($userdata[0]->facebook) && $userdata[0]->facebook != '') ? $userdata[0]->facebook : ''); ?>" readonly />
				  </li>

				  <li> <img src="<?php echo $this->config->item('base_url'); ?>public/images/add_twitter.jpg" >
					<input type="text" class="input_box_readonly" name="twitter" value="<?php echo set_value('twitter', (isset($userdata[0]->twitter) && $userdata[0]->twitter != '') ? $userdata[0]->twitter : ''); ?>" />
				  </li>

				  <li id="li_linkedin" <?php if(isset($userdata[0]->linkedin) && $userdata[0]->linkedin != ''){ echo "style='display:block;'"; } else{ echo "style='display:none;'"; }?> > <img src="<?php echo $this->config->item('base_url'); ?>public/images/linkendin.jpg" >
					<input type="text" class="input_box_readonly" name="linkedin" value="<?php echo set_value('linkedin', (isset($userdata[0]->linkedin) && $userdata[0]->linkedin != '') ? $userdata[0]->linkedin : ''); ?>" />
				  </li>

				  <li id="li_googleplus" <?php if(isset($userdata[0]->googleplus) && $userdata[0]->googleplus != ''){ echo "style='display:block;'"; } else{ echo "style='display:none;'"; }?> > <img src="<?php echo $this->config->item('base_url'); ?>public/images/gplus.jpg" >
					<input type="text" class="input_box_readonly" name="googleplus" value="<?php echo set_value('googleplus', (isset($userdata[0]->googleplus) && $userdata[0]->googleplus != '') ? $userdata[0]->googleplus : ''); ?>"  />
				  </li>
				</ul>
		    </div>
			<div class="what_kind">
				
				  <p>
					<b>What Kind of Business are you?</b>&nbsp;
					<?php 
					//print_r($data_service_offered); die;
					$str_business_types = '';
					foreach($data_business_type as $business_type){ $str_business_types .= $business_type->type_name.", "; }
					if($str_business_types != '') $str_business_types = substr($str_business_types, 0, -2);
					echo $str_business_types;
					?>
					
				 </p>
				<p>
					<b>Service Offered?</b>&nbsp;
					<?php
					$str_services = '';
					foreach($data_service_offered as $service_offered){ $str_services .= $service_offered->option_value.", "; }
					if($str_services != '') $str_services = substr($str_services, 0, -2);
					echo $str_services;
					?>
					
				  </p>
				  <?php
				  $str_confirmation_methods = str_replace(",", ", ", $userdata[0]->confirmation_methods);
				  $str_confirmation_methods = str_replace("phone", "Phone Call", $str_confirmation_methods);
				  $str_confirmation_methods = str_replace("sms", "SMS", $str_confirmation_methods);
				  $str_confirmation_methods = ucwords($str_confirmation_methods);
				  ?>
				  <p>
					<b>Confirmation Method for bookings?</b>&nbsp;<?=$str_confirmation_methods?>
				  </p>
				  <p>
					<b>Price Guide</b> &nbsp;<?=$userdata[0]->pg_name?>
				  </p>
				  <p>
					<b>Perfect For</b> &nbsp;<!-- <?=$userdata[0]->pf_name?> -->
					<?php
					$str_perfect_for = '';
					foreach($data_perfect_for as $perfect_for){ $str_perfect_for .= $perfect_for->pf_name.", "; }
					if($str_perfect_for != '') $str_perfect_for = substr($str_perfect_for, 0, -2);
					echo $str_perfect_for;
					?>
				  </p>
				
			  </div>  
			  
			  <div class="what_kind">
				
				  <p>
					<b>Overview-Short blurb about your business. What customers will love about it!</b>
				  </p>
				  <p>
					<?=$userdata[0]->about_business?>
				   </p>
				   
				   <p>
					<b>Parking details</b>
				  </p>
				  <p>
					<?=$userdata[0]->parking_details?>
				  </p>
				
			  </div> 
        </div>
        <div class="upload">
        <h2>Business' pictures:</h2>
        <ul>
          <li>
            <div class="browse1">
			  <?php if($userdata[0]->profile_picture != ''){?>
					<?php
					if (!file_exists($userdata[0]->profile_picture)) {
						$userdata[0]->profile_picture  = "public/images/0.png";
					} 
					?>
              <img src="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->profile_picture?>" width="120"/>
			  <?php } ?>
            </div>
            <div class="upload_info">
              <p>Profile picture<br>
            </div>
          </li>
          <li>
            <div class="browse2">
			   <?php if($userdata[0]->supporting_picture != ''){?>
					<?php
					if (!file_exists($userdata[0]->supporting_picture)) {
						$userdata[0]->supporting_picture  = "public/images/0.png";
					} 
					?>
               <img src="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->supporting_picture?>" width="120"/>
			   <?php } ?>
            </div>
            <div class="upload_info">
              <p>Supporting Picture<br>
            </div>
          </li>
          <li>
            <div class="browse3">
			   <?php if($userdata[0]->widescreen_picture != ''){?>
					<?php
					if (!file_exists($userdata[0]->widescreen_picture)) {
						$userdata[0]->widescreen_picture  = "public/images/0.png";
					} 
					?>
					<img src="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->widescreen_picture?>" width="120"/>
			   <?php } ?>
            </div>
            <div class="upload_info">
              <p>Widescreen Picture<br>
              <p> 
            </div>
          </li>
        </ul>
      </div>
      <div class="upload">
        <h2>Menu/Price List :</h2>
        <ul>
          <li>
            <div class="browse4">
              <a href="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->pdf_menu?>" target="_blank">View</a>
            </div>
            <div class="upload_info">
              <p>PDF of your Menu/Price List<br>
            </div>
          </li>
        </ul>
      </div>
      <div class="hours">
        <h2>Opening hours :</h2>
		<?php 
		$printed_days = 0;
		
		/*
		echo '<pre>';
		print_r($opening_hours_query);
		echo '</pre>';
		*/

		for($dboh=1; $dboh<=sizeof($opening_hours_query); $dboh++)
		{
			$printed_days++;

			//$opening_hours_query[$dboh-1]->day;
			
			if($dboh%2==0) $cls = 'class="even"';
			else $cls = 'class="odd"';

		?>
			<ul id="opening_hours_<?=$printed_days?>" <?=$cls?> >
			  <li class="day"><label><?=$opening_hours_query[$dboh-1]->day?></label></li>
			  <li class="from"><label>from&nbsp;<?=date("h:i", strtotime($opening_hours_query[$dboh-1]->from_hour))?>&nbsp;<?=$opening_hours_query[$dboh-1]->from_ampm?></label></li>
			  <li class="to"><label>to&nbsp;<?=date("h:i", strtotime($opening_hours_query[$dboh-1]->to_hour))?>&nbsp;<?=$opening_hours_query[$dboh-1]->to_ampm?></label></li>
			</ul>
            
		<?php } ?>
        
	 </div>
      <div class="save_bg"> <span class="save"><a href="<?php echo base_url("merchantaccount/businessprofile/"); ?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/saved_edit.jpg" /></a></span> </div>
    </div>
  </div>
</div>

<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
 

<!--FOR SEARCH DROPDOWN-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>




<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script> 
<!--<script src="assets/browse/jquery-1.3.2.min.js" type="text/javascript" charset="utf-8"></script> -->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>





<script type="text/javascript" charset="utf-8">
    $(function(){
      // example 3
	  
      $("#profile_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#supporting_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#widescreen_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
	  
	   $("#pdf_menu").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Upload</span></strong>"
      });
    });
  </script>


  <script type="text/javascript">
$(document).ready(function(e) {
	$(".revostyled").selectbox();
	$('.class_business_type').change(function () {
		choose_business_type(this.value, this.checked);
	});
	
	if ($('#li_linkedin').css('display')=='block' && $('#li_googleplus').css('display')=='block') {
		$('#li_show_more_social').css("display", "none");
	} else {
		$('#li_show_more_social').css("display", "block");
	}
});

</script>


<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

</body>
</html>