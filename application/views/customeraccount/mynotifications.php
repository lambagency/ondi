<?php
$loggedinuserdata = $this->customclass->getLoggedInUserData(); 
//print_r($loggedinuserdata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/cust.jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.theme_cust.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<link href="<?php echo $this->config->item('base_url'); ?>public/js/rateit.css" rel="stylesheet" type="text/css">


</head>
<body>
	<?php $this->load->view('templates/header');?>
	<?php $this->load->view('templates/customer_top');?>
	<div class="customer_main_con">
		<div class="customer_main">
			<?php $this->load->view('templates/customer_main_left');?>
			<div class="customer_resort_right">
				<div class="heading">
					<h1 class="notice">Notifications</h1>
				</div>
				<div class="notification" id="current_booking" >	
					<ul>
						<?php 
						$sess_user_data = $this->session->all_userdata();
						$ni=0;
						foreach($notifications as $this_notification)
						{
							$ni++;
							if($sess_user_data['user_type']=='1')
							{
								$read_status = $this_notification->customer_read_status;
							}
							else
							{
								$read_status = $this_notification->merchant_read_status;
							}
						?>
						<li <?php if($read_status=='0'){ echo 'class="active"'; } ?>>
							<?php if($this_notification->profile_message == '1'){?>
							<div class="left_note"><img src="<?php echo $this->config->item('base_url'); ?>public/images/notification.png" alt="" /></div>
							<?php } else { ?>
							<div class="left_note"><div class="calender">23</div></div>
							<?php } ?>

							<div class="right_note"><?=$this_notification->message?></div>
							<div class="date_note"><?=$this->customclass->_ago(strtotime($this_notification->added_date))?>  / <span <?php if($read_status=='1'){ echo 'class="normal"'; } else { echo 'class="bold"'; } ?> onclick="change_read_status_detail(<?=$this_notification->id?>);" id="detail_span_notification_<?=$this_notification->id?>" >Mark as <?php if($read_status=='1'){ echo 'unread'; } else { echo 'Read'; } ?></span> </div>
						</li>
						<?php 
						} ?>
					</ul>
				</div>
				<div class="save_bg">
					<span class="upper"><a href="javascript:void(0)" id="up"><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_arrow.jpg" alt="" /></a></span>
				</div>
			</div>
		</div>
	</div>
	
	<div id="dialog" title="Rate it" style="display:none;">
		<input type="hidden" name="rate_offer_id" id="rate_offer_id" value="" />
		<div class="ratecontainer">
			<div class="starrs">
				<select id="backing2b">
					<option value="0">0</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
				</select>
				<div class="rateit" data-rateit-backingfld="#backing2b" data-rateit-resetable="false" id="rateit9" ></div>
				<script src="<?php echo $this->config->item('base_url'); ?>public/js/jquery.rateit.js" type="text/javascript"></script>
			</div>
			<div class="ratesubmit">
				<button onclick="ajax_ratings($('#rateit9').rateit('value'));">Rate it</button>
			</div>
		</div>
	</div>

	<!--Start of customer Area-->
	<?php $this->load->view('templates/footer');?>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script>


<!--FOR WHATS HOTS SECTION-->

<script src='<?php echo $this->config->item('base_url'); ?>public/assets/hot/jquery.kwicks.js' type='text/javascript'></script> 


	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 
	<!--<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>-->

	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<script>
		$(function() {
			$( "#datepicker_d" ).datepicker({
					dateFormat: 'dd/mm/y',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true,
					onSelect: function() {
						$(".ui-datepicker a").removeAttr("href");
						$(this).datepicker('hide');
						document.getElementById('div_filter_month').innerHTML='<select tabindex="4" name="filter_month" id="filter_month" ><option class="label" value="">*Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>';
						$("#filter_month").selectbox();
						document.getElementById('datepicker_d').focus();
					}
				});
			$( "#anim" ).change(function() {
				$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
			});
		});
		$(function() {
			$( "#datepicker2_d" ).datepicker({
					dateFormat: 'dd/mm/y',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true,
					onSelect: function() {
						$(".ui-datepicker a").removeAttr("href");
						$(this).datepicker('hide');
						document.getElementById('div_filter_month').innerHTML='<select tabindex="4" name="filter_month" id="filter_month" ><option class="label" value="">*Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>';
						$("#filter_month").selectbox();
					}
				});
			$( "#anim" ).change(function() {
				$( "#datepicker2" ).datepicker( "option", "showAnim", $( this ).val() );
			});
		});
		
		/*$(function() {
			$( "#dialog" ).dialog();
		});*/
		function show_rateit_popup(offerid)
		{
			$("#dialog" ).css("display", "block");
			$("#dialog" ).dialog();
			$('#rateit9').rateit('reset');
			document.getElementById('rate_offer_id').value = offerid;
		}
	</script>
		<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
		<script type="text/javascript">
			$(function () {
				$("#filter_month").selectbox();
			});
			
			
		</script>
    
    
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>


</body>
</html>
