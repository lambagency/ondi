<?php
$loggedinuserdata = $this->customclass->getLoggedInUserData(); 
//print_r($loggedinuserdata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.min1.8.js"></script> 

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/cust.jquery.ui.datepicker.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.theme_cust.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.core.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>
<link href="<?php echo $this->config->item('base_url'); ?>public/js/rateit.css" rel="stylesheet" type="text/css">

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />


</head>
<body>
	<?php $this->load->view('templates/header');?>
	<?php $this->load->view('templates/customer_top');?>
	<div class="customer_main_con">
		<div class="customer_main">
			<?php $this->load->view('templates/customer_main_left');?>
			<div class="customer_resort_right">
				<div class="heading">
					<h1>Current Bookings</h1>
				</div>
                
                
                <?php if ($this->agent->is_mobile())
				{
				?>
				
                <div class="current_booking" id="current_booking" >
					
                    <div class="detail">
						
							<?php 
							foreach($currentorders as $thecurrent){
								//print_r($thecurrent); ?>
                                
                                <div class="current_booking_list">
                                
                                
							<div class="blank_div">&nbsp;&nbsp;&nbsp;</div>
                            
                            <a class="click_arrow" href="javascript:void(0);" onclick="viewmoreaboutorder_myorders(<?=$thecurrent->order_id?>, 'cb');" id="cb_ancviewmoreaboutorder_<?=$thecurrent->order_id?>" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_cus_share1.png" alt="" /></a>
                            
									<div class="offer_head"><?=$thecurrent->business_name?> <br/><strong><?=$thecurrent->offer_title?> $<?=$thecurrent->price?></strong></div>
								<div class="booked_head"><?=date("l, M jS, Y", strtotime($thecurrent->booking_date))?></div>
									<div class="time_head"><?=date("h:i A", strtotime($thecurrent->booking_time))?></div>
									<div class="quantity_head"><?=$thecurrent->number_of_people?> pax</div>
									<div class="total_head">$<?=$thecurrent->total_price?></div>
								
									<div class="status_head"><a href="javascript:void(0);">Booked</a></div>
								
									<div class="blank_head">
										<div class="buttons">
											<span class="share">
												<span class="nav_social">
													<a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($thecurrent->offer_id)?>&t=<?=urlencode($thecurrent->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
													<a  target="_blank" href="https://twitter.com/home?status=<?=urlencode($thecurrent->offer_title)?>%20-%20<?=$this->offers->get_offer_url($thecurrent->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
													<a  target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($thecurrent->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
												</span>
											</span>
										</div>
										
									</div>
								
                                
                                
									<div class="add_calander" id="cb_moredetailorder_<?=$thecurrent->order_id?>" style="display:none;">
										<div class="left_img">
											<?php
											if($thecurrent->profile_picture != "")
											{
												$thecurrent_profile_picture = $thecurrent->profile_picture;
											}
											else
											{
												$thecurrent_profile_picture = "public/images/merchant-noimage.jpg";
											}
											?>
											<img src="<?php echo $this->config->item('base_url'); ?><?=$thecurrent_profile_picture?>" alt="" width="42" height="35" />
											<p><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates=<?=date("Ymd", strtotime($thecurrent->booking_date))?>T<?=date("Hi", strtotime($thecurrent->booking_time))?>00Z%2F<?=date("Ymd", strtotime($thecurrent->booking_date))?>T<?=date("Hi", strtotime($thecurrent->booking_time))?>00Z&text=<?=urlencode($thecurrent->offer_title)?>&location=&details=" target="_blank" >+ Add to calendar</a> <span><a target="_blank" href="https://maps.google.com/maps?daddr=<?=$thecurrent->street_address?> <?=$thecurrent->suburb?> <?=$thecurrent->state?> <?=$thecurrent->post_code?>">+ Get directions</a></span></p>
										</div>
										<div class="right_img">
											<p>Booking #: <span><?=$thecurrent->booking_code?></span>&nbsp;&nbsp;&nbsp;    <?php if($thecurrent->order_date != '0000-00-00 00:00:00'){?> Booked on: <span><?=date("d/m/Y", strtotime($thecurrent->order_date))?></span><?php } ?> &nbsp;&nbsp;&nbsp; <br/>  
											
											<?php if($thecurrent->invoice_file_name != ''){?>
											Invoice: <span><a href="<?php echo $this->config->item('base_url'); ?><?=$thecurrent->invoice_file_name?>" target="_blank" >Download</a></span>
											<?php } ?>
											&nbsp;&nbsp;&nbsp;   Discount of: <span>$<?=$thecurrent->discount?></span></p>
										</div>
									</div>
								</div>
							<?php } ?>
						
					</div>
				</div>
                
                
				
				<?php	
				}
				else
				{
				
				?>
					
				
                
                <div class="current_booking" id="current_booking" >
					<div class="head">
						<table class="table_customer">
							<tr class="table_head">
								<td><div class="offer_head" onclick="sortrecord('offer', 'current', 'asc');">Offer</div></td>
								<td><div class="booked_head" onclick="sortrecord('booking_date', 'current', 'asc');" >Booked for</div></td>
								<td><div class="time_head" onclick="sortrecord('booking_time', 'current', 'asc');" >Time</div></td>
								<td><div class="quantity_head" onclick="sortrecord('number_of_people', 'current', 'asc');" >Quantity</div></td>
								<td><div class="total_head" onclick="sortrecord('total_price', 'current', 'asc');" >Total</div></td>
								<td class="no_bg"><div class="status_head">Status</div></td>
								<td class="no_bg"><div class="blank_head">&nbsp;</div></td>
							</tr>
						</table>
					</div>
					<div class="detail">
						<table class="table_customer">
							<?php 
							foreach($currentorders as $thecurrent){
								//print_r($thecurrent); ?>
							<tr class="new_col_blank">
								<td colspan="4"><div class="blank_div">&nbsp;&nbsp;&nbsp;</div></td>
							</tr>
							<tr> 
								<td class="left_table_column">
									<div class="offer_head"><?=$thecurrent->business_name?> <br/><strong><?=$thecurrent->offer_title?> $<?=$thecurrent->price?></strong></div>
								</td>    
								<td class="abc">    
									<div class="booked_head"><?=date("l, M jS, Y", strtotime($thecurrent->booking_date))?></div>
									<div class="time_head"><?=date("h:i A", strtotime($thecurrent->booking_time))?></div>
									<div class="quantity_head"><?=$thecurrent->number_of_people?> pax</div>
									<div class="total_head">$<?=$thecurrent->total_price?></div>
								</td>
								<td class="book_btn">
									<div class="status_head"><a href="javascript:void(0);">Booked</a></div>
								</td>
								<td>
									<div class="blank_head">
										<div class="buttons">
											<span class="share">
												<span class="nav_social">
													<a  target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($thecurrent->offer_id)?>&t=<?=urlencode($thecurrent->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
													<a  target="_blank" href="https://twitter.com/home?status=<?=urlencode($thecurrent->offer_title)?>%20-%20<?=$this->offers->get_offer_url($thecurrent->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
													<a  target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($thecurrent->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
												</span>
											</span>
										</div>
										<a href="javascript:void(0);" onclick="viewmoreaboutorder_myorders(<?=$thecurrent->order_id?>, 'cb');" id="cb_ancviewmoreaboutorder_<?=$thecurrent->order_id?>" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_cus_share1.png" alt="" /></a>
									</div>
								</td>
							</tr>
							<tr class="new_col">
								<td colspan="4">
									<div class="add_calander" id="cb_moredetailorder_<?=$thecurrent->order_id?>" style="display:none;">
										<div class="left_img">
											<?php
											if($thecurrent->profile_picture != "")
											{
												$thecurrent_profile_picture = $thecurrent->profile_picture;
											}
											else
											{
												$thecurrent_profile_picture = "public/images/merchant-noimage.jpg";
											}
											?>
											<img src="<?php echo $this->config->item('base_url'); ?><?=$thecurrent_profile_picture?>" alt="" width="42" height="35" />
											<p><a href="http://www.google.com/calendar/event?action=TEMPLATE&dates=<?=date("Ymd", strtotime($thecurrent->booking_date))?>T<?=date("Hi", strtotime($thecurrent->booking_time))?>00Z%2F<?=date("Ymd", strtotime($thecurrent->booking_date))?>T<?=date("Hi", strtotime($thecurrent->booking_time))?>00Z&text=<?=urlencode($thecurrent->offer_title)?>&location=&details=" target="_blank" >+ Add to calendar</a> <span><a target="_blank" href="https://maps.google.com/maps?daddr=<?=$thecurrent->street_address?> <?=$thecurrent->suburb?> <?=$thecurrent->state?> <?=$thecurrent->post_code?>">+ Get directions</a></span></p>
										</div>
										<div class="right_img">
											<p>Booking #: <span><?=$thecurrent->booking_code?></span>&nbsp;&nbsp;&nbsp;    <?php if($thecurrent->order_date != '0000-00-00 00:00:00'){?> Booked on: <span><?=date("d/m/Y", strtotime($thecurrent->order_date))?></span><?php } ?> &nbsp;&nbsp;&nbsp;   
											
											<?php if($thecurrent->invoice_file_name != ''){?>
											Invoice: <span><a href="<?php echo $this->config->item('base_url'); ?><?=$thecurrent->invoice_file_name?>" target="_blank" >Download</a></span>
											<?php } ?>
											&nbsp;&nbsp;&nbsp;   Discount of: <span>$<?=$thecurrent->discount?></span></p>
										</div>
									</div>
								</td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</div>
				
				<?php
				}
				
				?>
				
				
				
                
                
                
                
                
                
                
				<div class="heading">
					<h1>Booking History</h1>
				</div>
				<div class="booking_history">
					<?php echo form_open_multipart(base_url()."customeraccount/mybookings/")?>
					<input type="hidden" name="filterbooking" value="1" />
						<div class="filter_box">
							<div class="search_box">
								<input type="submit" value="search" class="search_btn" />
								<input type="text" placeholder="Search" class="search" name="keyword" id="keyword" value="<?=$search_post_data['keyword']?>" />
							</div>
							<div class="search_month" id="div_filter_month">
								<select tabindex="4" name="filter_month" id="filter_month" >
									<option value="" class="label" >*Month</option>
									<option value="1" <?php if($search_post_data['filter_month'] == '1'){?> selected="selected" <?php } ?> >January</option>
									<option value="2" <?php if($search_post_data['filter_month'] == '2'){?> selected="selected" <?php } ?> >February</option>
									<option value="3" <?php if($search_post_data['filter_month'] == '3'){?> selected="selected" <?php } ?> >March</option>
									<option value="4" <?php if($search_post_data['filter_month'] == '4'){?> selected="selected" <?php } ?> >April</option>
									<option value="5" <?php if($search_post_data['filter_month'] == '5'){?> selected="selected" <?php } ?> >May</option>
									<option value="6" <?php if($search_post_data['filter_month'] == '6'){?> selected="selected" <?php } ?> >June</option>
									<option value="7" <?php if($search_post_data['filter_month'] == '7'){?> selected="selected" <?php } ?> >July</option>
									<option value="8" <?php if($search_post_data['filter_month'] == '8'){?> selected="selected" <?php } ?> >August</option>
									<option value="9" <?php if($search_post_data['filter_month'] == '9'){?> selected="selected" <?php } ?> >September</option>
									<option value="10" <?php if($search_post_data['filter_month'] == '10'){?> selected="selected" <?php } ?> >October</option>
									<option value="11" <?php if($search_post_data['filter_month'] == '10'){?> selected="selected" <?php } ?> >November</option>
									<option value="12" <?php if($search_post_data['filter_month'] == '12'){?> selected="selected" <?php } ?> >December</option>
								</select>
							</div>
							<div class="date">
								<input type="text" id="datepicker" name="date_from" size="30" <?php  if($search_post_data['date_from'] != ''){?> value="<?=date("d/m/Y", strtotime($search_post_data['date_from']))?>" <?php  } ?> />
							</div>
							<div class="date">
								<input type="text" id="datepicker2" size="30" name="date_to" <?php  if($search_post_data['date_to'] != ''){?>value="<?=date("d/m/Y", strtotime($search_post_data['date_to']))?>" <?php } ?> />
							</div>
						</div>
					<?php echo form_close()?>
                    
                    
                    
                    <?php if ($this->agent->is_mobile())
					{
					?>
					
                    
                    
                    
                    
                    
                    <div class="historydiv_mobile">
                    
                    	<div class="detail">
							
								<?php foreach($pastorders as $thepast){
								if($thepast->offer_title == '') continue;
								?>
								
								<div class="historydiv_mobile_list">
                                
                                
                                <a class="click_arrow" href="javascript:void(0);" onclick="viewmoreaboutorder_myorders(<?=$thepast->order_id?>, 'bh');" id="bh_ancviewmoreaboutorder_<?=$thepast->order_id?>" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_cus_share1.png" alt="" /></a>
                                
                                
										<div class="offer_head"><?=$thepast->business_name?> <br/><strong><a href="<?php echo base_url("merchantoffers/detail/?m=".base64_encode($thepast->merchant_id)); ?>"><?=$thepast->offer_title?></a>$<?=$thepast->price?></strong></div>
									  
										<div class="booked_head"><?=date("l, M jS, Y", strtotime($thepast->booking_date))?></div>
										<div class="time_head"><?=date("h:i A", strtotime($thepast->booking_time))?></div>
										<div class="quantity_head"><?=$thepast->number_of_people?> pax</div>
										<div class="total_head">Total : $<?=$thepast->total_price?></div>
								
										<div class="status_head">Status : <a href="javascript:void(0);">Done</a></div>
									
										<div class="blank_head">
											<div class="buttons">
												<?php
												$rating_by_user = $this->offers->get_offer_ratings_by_user($thepast->offer_id, $loggedinuserdata[0]->user_id);
												?>
												<div class="rate_it">
													<?php if($rating_by_user == '0'){?>
													<span class="rate_it_star">
														<!-- <img src="<?php echo $this->config->item('base_url'); ?>public/images/five_star.png" alt="" /> -->
													</span>
													<p><a href="javascript:void(0);" onclick="show_rateit_popup(<?=$thepast->offer_id?>);">Rate it</a></p>
													<?php } else { ?>
													<span class="rate_it_star">
														<img src="<?php echo $this->config->item('base_url'); ?>public/images/star<?=ceil($rating_by_user)?>.png" alt="" />
													</span>
													<?php } ?>
												</div>
											</div>
											
										</div>
									
										<div class="add_calander" id="bh_moredetailorder_<?=$thepast->order_id?>" style="display:none;">
											<div class="left_img">
												<?php
												if($thepast->profile_picture != "")
												{
													$thepast_profile_picture = $thepast->profile_picture;
												}
												else
												{
													$thepast_profile_picture = "public/images/merchant-noimage.jpg";
												}
												?>
												<img src="<?php echo $this->config->item('base_url'); ?><?=$thepast_profile_picture?>" alt="" width="42" height="35" />
											</div>
											<div class="right_img">
												<p>Booking #: <span><?=$thepast->booking_code?></span> &nbsp;&nbsp;&nbsp;   <?php if($thepast->order_date != '0000-00-00 00:00:00'){?>Booked on: <span><?=date("d/m/Y", strtotime($thepast->order_date))?></span><?php } ?> &nbsp;&nbsp;&nbsp;   
												<?php if($thepast->invoice_file_name != ''){?>
												Invoice: <span><a href="<?php echo $this->config->item('base_url'); ?><?=$thepast->invoice_file_name?>" target="_blank" >Download</a></span>
												<?php } ?>
												&nbsp;&nbsp;&nbsp;   Discount of: <span>$<?=$thepast->discount?></span></p>
											</div>
										</div>
                                        
                                    </div>
									
								<?php } ?>
							
						</div>
                        
                        	
                    </div>
                    
					<?php	
					}
					else
					{
					?>
						
					<div id="historydiv">
						<div class="head">
							<table class="table_customer">
								<tr class="table_head">
									<td><div class="offer_head" onclick="sortrecord('offer', 'history', 'asc');" >Offer</div></td>
									<td><div class="booked_head" onclick="sortrecord('offer', 'booking_date', 'asc');" >Booked for</div></td>
									<td><div class="time_head" onclick="sortrecord('offer', 'booking_time', 'asc');" >Time</div></td>
									<td><div class="quantity_head" onclick="sortrecord('offer', 'number_of_people', 'asc');" >Quantity</div></td>
									<td><div class="total_head" onclick="sortrecord('offer', 'total_price', 'asc');" >Total</div></td>
									<td><div class="status_head">Status</div></td>
									<td class="no_bg"><div class="blank_head">Review</div></td>
								</tr>
							</table>
						</div>
						<div class="detail">
							<table class="table_customer">
								<?php foreach($pastorders as $thepast){
								if($thepast->offer_title == '') continue;
								?>
								<tr class="new_col_blank">
									<td colspan="4"><div class="blank_div">&nbsp;&nbsp;&nbsp;</div></td>
								</tr>
								<tr>
									<td class="left_table_column">
										<div class="offer_head"><?=$thepast->business_name?> <br/><strong><a href="<?php echo base_url("merchantoffers/detail/?m=".base64_encode($thepast->merchant_id)); ?>"><?=$thepast->offer_title?></a>$<?=$thepast->price?></strong></div>
									</td>    
									<td class="abc">    
										<div class="booked_head"><?=date("l, M jS, Y", strtotime($thepast->booking_date))?></div>
										<div class="time_head"><?=date("h:i A", strtotime($thepast->booking_time))?></div>
										<div class="quantity_head"><?=$thepast->number_of_people?> pax</div>
										<div class="total_head">$<?=$thepast->total_price?></div>
									</td>
									<td class="book_btn">
										<div class="status_head"><a href="javascript:void(0);">Done</a></div>
									</td>
									<td>
										<div class="blank_head">
											<div class="buttons">
												<?php
												$rating_by_user = $this->offers->get_offer_ratings_by_user($thepast->offer_id, $loggedinuserdata[0]->user_id);
												?>
												<div class="rate_it">
													<?php if($rating_by_user == '0'){?>
													<span class="rate_it_star">
														<!-- <img src="<?php echo $this->config->item('base_url'); ?>public/images/five_star.png" alt="" /> -->
													</span>
													<p><a href="javascript:void(0);" onclick="show_rateit_popup(<?=$thepast->offer_id?>);">Rate it</a></p>
													<?php } else { ?>
													<span class="rate_it_star">
														<img src="<?php echo $this->config->item('base_url'); ?>public/images/star<?=ceil($rating_by_user)?>.png" alt="" />
													</span>
													<?php } ?>
												</div>
											</div>
											<a href="javascript:void(0);" onclick="viewmoreaboutorder_myorders(<?=$thepast->order_id?>, 'bh');" id="bh_ancviewmoreaboutorder_<?=$thepast->order_id?>" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_cus_share1.png" alt="" /></a>
										</div>
									</td>
								</tr>
								<tr class="new_col">
									<td colspan="4">
										<div class="add_calander" id="bh_moredetailorder_<?=$thepast->order_id?>" style="display:none;">
											<div class="left_img">
												<?php
												if($thepast->profile_picture != "")
												{
													$thepast_profile_picture = $thepast->profile_picture;
												}
												else
												{
													$thepast_profile_picture = "public/images/merchant-noimage.jpg";
												}
												?>
												<img src="<?php echo $this->config->item('base_url'); ?><?=$thepast_profile_picture?>" alt="" width="42" height="35" />
											</div>
											<div class="right_img">
												<p>Booking #: <span><?=$thepast->booking_code?></span> &nbsp;&nbsp;&nbsp;   <?php if($thepast->order_date != '0000-00-00 00:00:00'){?>Booked on: <span><?=date("d/m/Y", strtotime($thepast->order_date))?></span><?php } ?> &nbsp;&nbsp;&nbsp;   
												<?php if($thepast->invoice_file_name != ''){?>
												Invoice: <span><a href="<?php echo $this->config->item('base_url'); ?><?=$thepast->invoice_file_name?>" target="_blank" >Download</a></span>
												<?php } ?>
												&nbsp;&nbsp;&nbsp;   Discount of: <span>$<?=$thepast->discount?></span></p>
											</div>
										</div>
									</td>
								</tr>
								<?php } ?>
							</table>
						</div>
					</div>
                    
					
					<?php
					}
					
					?>
					
					
					
                    
                    
                    
                    
                    
                    
					

				</div>
				<div class="save_bg">
					<span class="upper"><a href="javascript:void(0)" id="up"><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_arrow.jpg" alt="" /></a></span>
				</div>
			</div>
		</div>
	</div>
	
    
    <div class="popup_bg" id="dialog" title="Rate it" style="display:none;">
        <div class="popup_window">
            <div class="popup_bg_top">
            <h2>Rate it</h2>
            
                <a href="javascript:void(0)" id="rate_close"><img src="<?php echo base_url() ?>public/images/cancel-popup.png" /></a>
            </div><!--End of popup_bg_top-->
            <div class="popup_bg_main">
            	
            	<input type="hidden" name="rate_offer_id" id="rate_offer_id" value="" />
                    <div class="ratecontainer">
                        <div class="starrs">
                            <select id="backing2b">
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                            <div class="rateit" data-rateit-backingfld="#backing2b" data-rateit-resetable="false" id="rateit9" ></div>
                            
                        </div>
                        <div class="ratesubmit">
                            <button onclick="ajax_ratings($('#rateit9').rateit('value'));">Rate it</button>
                        </div>
                    </div>
            </div>
        </div>
    </div>


	<div class="popup_bg" id="dialog_success" title="Rate it" style="display:none;">
        <div class="popup_window">
            <div class="popup_bg_top">
            <h2>Successfully rated!</h2>
            <a href="javascript:void(0)" id="rate_close_success"><img src="<?php echo base_url() ?>public/images/cancel-popup.png" /></a>
            </div><!--End of popup_bg_top-->
        </div>
    </div>
    
    
	<!--<div id="dialog" title="Rate it" style="display:none;">
		
	</div>-->

	<!--Start of customer Area-->
	<?php $this->load->view('templates/footer');?>
	
    
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery-1.8.3.js"></script>
    
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
		
    <script src="<?php echo $this->config->item('base_url'); ?>public/js/jquery.rateit.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script>
	
    <link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/themes/base/jquery.ui.all.css">	
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.core.js"></script>
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/calendar/jquery.ui.datepicker.js"></script>
	
    
    
    <!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">-->
    
	<script type="text/javascript">
	
		$(function() {
			var $c1 = jQuery.noConflict();
			$c1( "#datepicker" ).datepicker({
					dateFormat: 'dd/mm/yy',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true,
					onSelect: function() {
						document.getElementById('div_filter_month').innerHTML='<select tabindex="4" name="filter_month" id="filter_month" ><option class="label" value="">*Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>';
						$("#filter_month").selectbox();
					}
				});
			
		});
		
		
		$(function() {
			var $c2 = jQuery.noConflict();
			$c2( "#datepicker2" ).datepicker({
					dateFormat: 'dd/mm/yy',
					yearRange: 'c-10:c+1',
					changeMonth: true,
					changeYear: true,
					onSelect: function() {
						document.getElementById('div_filter_month').innerHTML='<select tabindex="4" name="filter_month" id="filter_month" ><option class="label" value="">*Month</option><option value="1">January</option><option value="2">February</option><option value="3">March</option><option value="4">April</option><option value="5">May</option><option value="6">June</option><option value="7">July</option><option value="8">August</option><option value="9">September</option><option value="10">October</option><option value="11">November</option><option value="12">December</option></select>';
						$("#filter_month").selectbox();
					}
				});
			
		});
		
		$(function () {
			$("#filter_month").selectbox();		   
		});	
		
		
	
		
	</script>
    
    
    
    <script type="text/javascript">
			function show_rateit_popup(offerid)
				{
					$( "#dialog" ).css("display", "block");
					//$( "#dialog" ).dialog();
					$('#rateit9').rateit('reset');
					document.getElementById('rate_offer_id').value = offerid;
				}
				
				$('#rate_close').click(function(e) {
                   $( "#dialog" ).css("display", "none"); 
                });

				$('#rate_close_success').click(function(e) {
                   $( "#dialog_success" ).css("display", "none"); 
				   window.location.reload();
                });
	</script>
</body>
</html>
