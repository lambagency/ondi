<!doctype html>

<html>

<head>

<meta charset="utf-8">

<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />

<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->

<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->

<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->

<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->

<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->

<!--[if lt IE 8]>

<div style=' clear: both; text-align:center; position: relative;'>

			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>

</div>

<![endif]-->



<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>



<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/cust.jquery.ui.datepicker.css">

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.theme_cust.css">

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.core.css">

<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />


</head>

<body>

<?php $this->load->view('templates/header');?>

<?php $this->load->view('templates/customer_top');?>









<!--Start of customer Area-->



	<div class="customer_main_con">

    	<div class="customer_main">

        	<?php $this->load->view('templates/customer_main_left');?>

            <div class="customer_main_right">

            	<div class="heading">

                	<h1>My Details</h1>

                </div>

				<?php if(isset($_GET['upload']) && $_GET['upload'] == 'error'){?>

				<div class="valid_errors">Please upload image of Max Size 150px X 150px, Max 500KB</div>

				<?php } ?>



                <div class="member_detail">

                    <ul>

                        <li>

                        	<div class="photo_left">

                            	<h3><?=$userdata[0]->first_name?> <?=$userdata[0]->last_name?></h3>

                                <p><?=$userdata[0]->email?><br />Ph: <?=$userdata[0]->mobile_number?><br/>Post code: <?=$userdata[0]->postcode?>
								<?php if($userdata[0]->birthday != '0000-00-00' && $userdata[0]->birthday != ''  && $userdata[0]->birthday != '1970-01-01'){?>
								<br/><?=date("d/m/y", strtotime($userdata[0]->birthday))?>
								<?php } ?>
								<br/>
								<!-- <?=($userdata[0]->gender=="M"?"Male":"Female")?> -->
								<?php 
								if($userdata[0]->gender=="M") echo "Male";
								elseif($userdata[0]->gender=="F") echo "Female";
								?>
								</p>

                            </div>

                            <div class="photo_right">
								<?php
								if($userdata[0]->customer_picture != ''){?>
                            	<img src="<?php echo $this->config->item('base_url'); ?><?=$userdata[0]->customer_picture?>" alt="" width="120" />
                                <?php 
								} 
								else 
								{ 
									$facebook_id = $userdata[0]->facebook_id;
									if($facebook_id != '')
									{
										$profile_picture = "http://graph.facebook.com/".$facebook_id."/picture?type=small";	
										?>
										<img src="<?php echo $profile_picture; ?>" alt="" width="120" />
										<?php
									}
								}?>

                                <div class="edit_customer"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editdeatils"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div>

                            </div>

                        </li>

                    </ul>

            </div>

            

            <div class="member_password">

                <h2>Password</h2>

                <div class="edit_pwd"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editpassword"><img src="<?php echo $this->config->item('base_url'); ?>public/images/lock_c.png" alt="" />Change password</a></div>

            </div>

            

            <div class="member_newsletter">

                <h2>Newsletter</h2>

                <div class="news_subscription"><p>Yes please!</p><input name="subscribe_newsletter" type="checkbox" value="1" <?php if($userdata[0]->subscribe_newsletter=='1'){?> checked <?php } ?> disabled="disabled"  onclick="javascript:window.location='<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editnewsletter'; " ></div>

                <div class="edit_customer"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editnewsletter"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div>

            </div>

            

             <div class="member_things">

                <h2>Things I love</h2>

                <div class="edit_customer"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editinterests"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div>

            </div>


			<!-- <div class="member_things">
                <h2>Confirmation Method for bookings</h2>
                <div class="edit_customer"><?=$userdata[0]->confirmation_methods?>&nbsp;<a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editnotications"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div>
            </div> -->
            

            <div class="member_payment_detail">

                <h2>Payment Details </h2>

				<div class="edit_customer"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editpayment"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div>


                    <ul>

						<?php 

						$numcc = 0;
						//print_r($cccarddata);
						foreach($cccarddata as $cc){
						
						$numcc++;
						
						if(strtolower($cc->scheme) == 'visa') $cc_icon = $this->config->item('base_url')."public/images/visa_c.jpg";
						else if(strtolower($cc->scheme) == 'master') $cc_icon = $this->config->item('base_url')."public/images/master_card_c.jpg";
						else if(strtolower($cc->scheme) == 'american express') $cc_icon = $this->config->item('base_url')."public/images/american_exp_c.jpg";
						else $cc_icon = "";
						?>

                    	<li>

                            <span class="card2">
							<?php if( $cc_icon != ""){?>
								<img src="<?php echo $cc_icon; ?>" alt="" />
							<?php } ?>
							</span>

                            <span class="payment_detail"><input type="text" value="<?=$cc->scheme?> <?=$cc->display_number?> Expiry  <?=(strlen($cc->expiry_month)<2?"0".$cc->expiry_month:$cc->expiry_month)?>/<?=substr($cc->expiry_year, 2, strlen($cc->expiry_year))?>" class="detail" /></span>

                            <?php if($cc->category != '') echo "<p>(".$cc->category.")</p>"; ?>

                            <!-- <div class="edit_customer"><a href="<?php echo $this->config->item('base_url'); ?>customeraccount/editcontactinfo?act=editpayment"><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a></div> -->

							<?php //if($numcc>1){?>

							<div class="edit_customer"><a href="javascript:void(0);" onclick="deletecc('<?=base64_encode($cc->id)?>');"><img src="<?php echo $this->config->item('base_url'); ?>public/images/delete.png" alt="" />Delete</a></div>

							<?php //} ?>

                        </li>

						<?php } ?>

                    </ul>

                    

            </div>

        		   <div class="save_bg">

                        <span class="upper"><a href="javascript:void(0);" id="up"><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_arrow.jpg" alt="" /></a></span>

                    </div>

            

        </div>

    	</div>

    </div>



<!--Start of customer Area-->

<?php $this->load->view('templates/footer');?>



<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 



<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>



<script>

$(function() {

		$( "#datepicker" ).datepicker();

		$( "#anim" ).change(function() {

			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );

		});

});



$(function() {

		$( "#datepicker2" ).datepicker();

		$( "#anim" ).change(function() {

			$( "#datepicker2" ).datepicker( "option", "showAnim", $( this ).val() );

		});

});



</script>



<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>





<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 



<!--Price Selector content--> 

<script type="text/javascript">

   	$('.customer_main .customer_main_right .member_newsletter input:checkbox').screwDefaultButtons({

			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',

			width: 29,

			height: 29

	});	

		

</script>







<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>


</body>

</html>

