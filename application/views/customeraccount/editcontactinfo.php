<?php
//print_r($userdata[0]);	
//echo date("m/d/Y", strtotime($userdata[0]->birthday));
$array_interests = array();

if($userdata[0]->interests != '')
{
	$array_interests = explode(",", $userdata[0]->interests);
}
$act = "";
if(isset($_GET['act']) && $_GET['act'] != '')
{
	$act = $_GET['act'];
}
if($userdata[0]->confirmation_methods != '')
{
	$array_selected_confirmation_methods = explode(",", $userdata[0]->confirmation_methods);
}
?>
<!doctype html>
<html>
<head>
<meta http-equiv="x-ua-compatible" content="IE=9">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>

<link href="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/css/jquery.selectbox.css" type="text/css" rel="stylesheet" />

<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">

<!--FOR DATE PICKER-->
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/themes/base/jquery.ui.all.css">
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/ui/demos.css">




<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />


</head>
<body>
	<?php $this->load->view('templates/header');?>
	<?php $this->load->view('templates/customer_top');?>
	<!--Start of customer Area-->
	<div class="customer_main_con">
		<div class="customer_main">
			<?php $this->load->view('templates/customer_main_left');?>
			<div class="customer_main_right">
				<?php $attributes = array('onsubmit' => 'return validatecustomerinfo()', 'name' => 'cinfoform'); ?>
				<?php echo form_open_multipart(base_url()."customeraccount/editcontactinfo/", $attributes)?>
				<input type="hidden" name="submit" value="1" />
				<div class="heading">
					<h1>My Details</h1>
					<a name="editdeatils"></a>
				</div>
				
				<div class="sub_heading">
						<h3>Please enter your details as follow. Fields marked (*) are mandatory.</h3>
				</div>

				<div class="customer_about_me" >
					<h2 id="heading_about_me" onclick="showcontentofsection('about_me');" >About Me</h2>
					
					<?php if($act != 'editdeatils'){?>
					<div class="edit_customer">
						<a href="javascript:void(0);" onclick="showcontentofsection('about_me');" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a>
					</div>
					<?php } ?>

					<ul id="content_about_me" <?php if($act == 'editdeatils') echo 'style="visibility:visible; height:auto;"'; else echo 'style="visibility:hidden;height:0px;"'; ?>  >
						<li id="validation_errors">
						<?php echo validation_errors(); ?>
						<?php
						$error = $this->session->flashdata('error_message');
						?>
						<?php if($error) : ?>
							<div id="error_text"><?php echo $error ?></div>
						<?php endif; ?>
						</li>
						<li>
							<input type="text" value="<?php echo set_value('first_name', (isset($userdata[0]->first_name) && $userdata[0]->first_name != '') ? $userdata[0]->first_name : ''); ?>" class="first_name" name="first_name" id="cinfo_first_name" placeholder="*First Name"  />
							<input type="text" value="<?php echo set_value('last_name', (isset($userdata[0]->last_name) && $userdata[0]->last_name != '') ? $userdata[0]->last_name : ''); ?>" class="last_name" name="last_name" id="cinfo_last_name" placeholder="*Last Name" />
						</li>
						<li>
							<input type="email" class="email" value="<?php echo set_value('email', (isset($userdata[0]->email) && $userdata[0]->email != '') ? $userdata[0]->email : ''); ?>" name="email" id="cinfo_email" placeholder="*Email (your login)" />
							<span class="comments">Booking confirmations <br/>will be sent to this email.</span>
						</li>
						<li>
							<input type="text" class="email" value="<?php echo set_value('mobile_number', (isset($userdata[0]->mobile_number) && $userdata[0]->mobile_number != '') ? $userdata[0]->mobile_number : ''); ?>"  name="mobile_number"  id="cinfo_mobile_number" placeholder="*Mobile Number" />
							<span class="comments">Your number is used for <br/>emergency notifications</span>
						</li>
						<li>
							<input type="text" class="pincode" value="<?php echo set_value('postcode', (isset($userdata[0]->postcode) && $userdata[0]->postcode != '') ? $userdata[0]->postcode : ''); ?>"  name="postcode"  id="cinfo_postcode" placeholder="*Postcode" />
							
							<input type="text" class="birthday"  value="<?php echo set_value('bday', (isset($userdata[0]->birthday) && $userdata[0]->birthday != '' && $userdata[0]->birthday != '0000-00-00' && $userdata[0]->birthday != '1970-01-01') ? date("d/m/Y", strtotime($userdata[0]->birthday)) : ''); ?>"   name="bday" id="datepicker5" size="30"  placeholder="Birthday e.g DD/MM/YYYY" autocomplete="off" />

							<div class="gender">
								<p>*Gender</p>
								<div id="chooseplan">
									<div class="cleaner h10"></div>
									<div id="hostpslider"></div>
									<div class="cleaner h10"></div>
								</div>
								<div class="info">(Men click left. Ladies click right)</div> 
								<input type="hidden" name="gender" id="gender" value="<?=$userdata[0]->gender?>" />
							</div>
							
						</li>
						<li>
							<iframe height="125" frameborder="0" width="100%" scrolling="no" src="<?php echo $this->config->item('base_url'); ?>image_customer.php?user=<?=$userdata[0]->user_id?>" id="iframe_id"></iframe>
							<input type="hidden" name="temp_profile_picture" id="temp_profile_picture" value="" />
							<input type="hidden" id="old_profile_picture" name="old_profile_picture" value="<?=$userdata[0]->customer_picture?>" />
							<a name="editpassword"></a>
						</li>
						
					</ul>
				</div>
				<div class="customer_password" >
					<h2 id="heading_password" onclick="showcontentofsection('password');" >Password</h2>

					<?php if($act != 'editpassword'){?>
					<div class="edit_customer">
						<a href="javascript:void(0);" onclick="showcontentofsection('password');" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a>
					</div>
					<?php } ?>


					<ul id="content_password" <?php if($act == 'editpassword') echo 'style="visibility:visible; height:auto;"'; else echo 'style="visibility:hidden;height:0px;"'; ?>  >
						<li>
							<input type="password" placeholder="Password" class="password" name="password" />
							<input type="password" placeholder="Confirm password" class="password2" name="cpassword"/>
						</li>
					</ul>
					<a name="editnewsletter"></a>
				</div>
				<div class="customer_newsletter">
					<h2 id="heading_newsletter" onclick="showcontentofsection('newsletter');" >Newsletter</h2>
					
					<?php if($act != 'editnewsletter'){?>
					<div class="edit_customer">
						<a href="javascript:void(0);" onclick="showcontentofsection('newsletter');" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a>
					</div>
					<?php } ?>

					<a name="editinterests"></a>
					<div class="news_subscription" id="content_newsletter"  <?php if($act == 'editnewsletter') echo 'style="visibility:visible; height:auto;"'; else echo 'style="visibility:hidden;height:0px;"'; ?>  ><p>Yes please!</p><input name="subscribe_newsletter" type="checkbox" value="1" <?php if($userdata[0]->subscribe_newsletter=='1'){?> checked <?php } ?> ></div>
				</div>
				<div class="customer_things" >
					<h2 id="heading_things" onclick="showcontentofsection('things');" >Things I love</h2>
					
					<?php if($act != 'editinterests'){?>
					<div class="edit_customer">
						<a href="javascript:void(0);" onclick="showcontentofsection('things');" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a>
					</div>
					<?php } ?>

					<a name="editpayment"></a>
					<div id="content_things" <?php if($act == 'editinterests') echo 'style="visibility:visible; height:auto; float:left; width:100%;"'; else echo 'style="visibility:hidden;height:0px; overflow:hidden; float:left; width:100%;"'; ?> >
					<h3>What do you like to do? What are you interested in?</h3>
					<ul>
					    <?php
						$rownum = 0;
						$int_num = 1;
						foreach($interestdata as $interest)
						{
							if($int_num %3==1)
							{
								$rownum++;
								?>
								<li <?php if($rownum%2 == 1){?> class="thing_gray" <?php } ?> >
								<?php
							}
						?>
							<div class="thing"><input name="interests[]" type="checkbox" value="<?=$interest->id?>"  <?php if(in_array($interest->id, $array_interests)){ echo "checked='checked'"; } ?> ><?=$interest->interest?></div>
						<?php 
						if($int_num %3==0)
						{
						?>
						</li>
						<?php
						}
						?>
						<?php
						$int_num++;
						}
						?>
					</ul>
					</div>
				</div>

				<!-- <div class="upload">
				<h2 id="heading_notications" onclick="showcontentofsection('notications');">Confirmation Method for bookings:</h2>
				<a name="editnotications"></a>
				<ul>
				  <li>
				  <div class="select"><div id="div_confirmation_methods"><?php if($userdata[0]->confirmation_methods != '') echo str_replace(",", ", ", $userdata[0]->confirmation_methods); else echo "Please choose at least 1"; ?></div>
					  <div class="drop">
						<span><a href="javascript:void(0);" onclick="uncheckall('checkboxesgroup_confirmation');">Clear all..</a></span>
						<ul id="checkboxesgroup_confirmation">
						  <li><input id="confirmation_methods_sms" name="confirmation_methods[]" type="checkbox" value="sms" <?php if(in_array('sms', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?> onclick="change_confirmation_methods(this.value, this.checked);" >SMS</li>
						  <li><input id="confirmation_methods_email"  name="confirmation_methods[]" type="checkbox" value="email" <?php if(in_array('email', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?>  onclick="change_confirmation_methods(this.value, this.checked);"   >Email</li>
						  <li><input id="confirmation_methods_phone"  name="confirmation_methods[]" type="checkbox" value="phone" <?php if(in_array('phone', $array_selected_confirmation_methods)){ echo "checked='checked'"; } ?>  onclick="change_confirmation_methods(this.value, this.checked);"  >Phone</li>
						</ul>
					  </div>
					</div>
				  </li>
				</ul>
			    </div> -->

				<div class="customer_payment_detail">
					<h2 id="heading_payment" onclick="showcontentofsection('payment');" >Payment Details</h2>
					
					<?php if($act != 'editpayment'){?>
					<div class="edit_customer">
						<a href="javascript:void(0);" onclick="showcontentofsection('payment');" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/edit_pencil.png" alt="" />Edit</a>
					</div>
					<?php } ?>

					<div id="content_payment" <?php if($act == 'editpayment') echo 'style="visibility:visible; height:auto;"'; else echo 'style="visibility:hidden; height:0px; overflow:hidden"'; ?> >
						<?php 
						$printed_cards = 0;
						for($s=$printed_cards+1; $s<=10; $s++){?>
							<ul id="ul_cards_<?=$s?>" <?php if($s==1) echo "style='display:block;'"; else echo "style='display:none;'"; ?>>
								<li>
									<p>Credit Card</p>
									<?php foreach($cctypedata as $cctype){?>
									<span class="pay_option"><input type="radio" class="cards" value="<?=$cctype->type_id?>" name="cctype_<?=$s?>"  /><img src="<?php echo $this->config->item('base_url'); ?><?=$cctype->icon?>" alt="" /></span>
									<?php }?>
								</li>
								<li>
									<div class="payment_left"><input type="text" placeholder="*Card number" class="card_no" name="card_no_<?=$s?>" value="" /></div>
									<div class="payment_right"><input type="text" placeholder="*Name on card" class="card_name" name="card_name_<?=$s?>" value="" /></div>
								</li>
								<li>
									<div class="payment_left">
									  <input type="text" placeholder="*Address" class="card_no" name="cc_address_<?=$s?>" value="" />
									</div>
									<div class="payment_right">
									  <input type="text" placeholder="*City"  class="card_name" name="cc_city_<?=$s?>" value=""  />
									</div>
								  </li>

								  <li>
									<div class="payment_left">
									  <input type="text" placeholder="*Country"  class="card_no" name="cc_country_<?=$s?>"  value="" />
									</div>
								  </li>
								<li>
									<div class="payment_left">
										<p>Expiration date</p>
										<span class="drop_down">
											<select tabindex="4" class="revostyled" name="exp_month_<?=$s?>">
												<option value="" class="label">*Month</option>
												<option value="1" >January</option>
												<option value="2" >February</option>
												<option value="3" >March</option>
												<option value="4" >April</option>
												<option value="5" >May</option>
												<option value="6" >June</option>
												<option value="7" >July</option>
												<option value="8" >August</option>
												<option value="9" >September</option>
												<option value="10" >October</option>
												<option value="11" >November</option>
												<option value="12" >December</option>
											</select>
										</span>
									</div>
									<div class="payment_right">
										<span class="drop_down2">
											<select tabindex="4" class="revostyled" name="exp_year_<?=$s?>">
												<option value="" class="label">*Year</option>
												<?php for($i=date("Y"); $i<=date("Y")+5; $i++){?>
												<option value="<?=$i?>" ><?=$i?></option>
												<?php } ?>
											</select>
										</span>
										<input type="text" placeholder="*Security code / CVV" class="ccv_code" name="cvv_<?=$s?>" value=""  />
									</div>
								</li>
								<li>
									<div class="payment_left">
										<p>Category</p>
										<span class="drop_down">
											<select tabindex="4" class="revostyled" name="cc_category_<?=$s?>">
												<option value="" class="label">Select</option>
												<?php foreach($cccategoryquery as $thecccategory){?>
												<option value="<?=$thecccategory->id?>" ><?=$thecccategory->cc_category?></option>
												<?php } ?>
											</select>
										</span>
									</div>
									<div class="payment_right">
                                    	<p class="what">what is this?
                                        	<span class="ccv">
                                                <img src="<?php echo $this->config->item('base_url'); ?>public/images/ccv2.png" alt="" />
                                            </span>
                                        </p>
                                        
                                        
                                    </div>
								</li>
								
							</ul>
						<?php } ?>
						<p class="addmore"><a id="anc_add_more_cards" href="javascript:void(0);" onclick="anc_add_more_cards();" >+ Add another payment option</a></p>
						<input type="hidden" id="num_cards" name="num_cards"  <?php if(sizeof($cccarddata)>1){?>value="<?=sizeof($cccarddata)?>"<?php } else {?>value="1" <?php } ?> />
					</div>
				</div>
				<div class="save_bg">
					<span class="save"><input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/save_btn.jpg" alt="" /></span>
					<span class="upper"><a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/up_arrow.jpg" alt="" /></a></span>
				</div>
				<?php echo form_close()?>
			</div>
		</div>
	</div>
	<!--Start of customer Area-->
	<?php $this->load->view('templates/footer');?>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 
	<!--FOR HOME PAGE SLIDER--> 
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script>
	<!--FOR LOGIN CHECKBOX-->
	<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.min.js"></script> 
    
    
    
    
    <script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.core.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.widget.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-blind.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-bounce.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-clip.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-drop.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-fold.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.effect-slide.js"></script> 
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/ui/jquery.ui.datepicker.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/custom_select/js/jquery.selectbox-0.2.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".revostyled").selectbox();
		
	});

</script>

    
    
	<!--FOR GENDER SLIDER-->
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
	<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
	<!--Price Selector content--> 
	<script type="text/javascript">
		var DS = new Array("M", "", "F");
		$(function() {
			$("#hostpslider").slider({
				range: "min",
				value: <?php if($userdata[0]->gender=='M') echo "0"; else if($userdata[0]->gender=='F') echo "2"; else echo "1"; ?>,
				min: 0,
				max: 2,
				step: 1,
				slide: function(event, ui) {
					$("#DS").val(DS[ui.value]);
					document.getElementById('gender').value = DS[ui.value];

				}
			});
			$("#DS").val(DS[$("#hostpslider").slider("value")]);
		});
		$('.customer_main .customer_main_right .customer_things ul li input:checkbox').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',
			width: 29,
			height: 29
		});	
		$('.customer_main .customer_main_right .customer_newsletter .news_subscription input:checkbox').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',
			width: 29,
			height: 29
		});	
		$('.customer_main .customer_main_right .customer_payment_detail ul li .pay_option input:radio').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/radio.png")',
			width: 19,
			height: 18
		});
	</script>

	

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/browse/jquery.prettyfile.js" type="text/javascript" charset="utf-8"></script>
<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/browse/example.css" type="text/css" media="screen" charset="utf-8"/>

<script type="text/javascript" charset="utf-8">
    $(function(){
      // example 3
	  
      $("#profile_picture").prettyfile({
        html: "<span class='pf_ph'>Select a file</span><strong><span>Browse</span></strong>"
      });
	  
    });
  </script>
  
  





<script src="<?php echo $this->config->item('base_url'); ?>public/assets/placeholder/jquery.placeholder.js"></script>

<?php if(isset($_GET['cust']) && $_GET['cust']== 'new'){?>
<script language="javascript">
showcontentofsection('about_me');
//showcontentofsection('password');
showcontentofsection('newsletter');
showcontentofsection('things');
showcontentofsection('payment');
</script>
<?php } ?>

<script language="javascript">
var verrorcontent = document.getElementById('validation_errors').innerHTML;
if(verrorcontent.trim() != '')
{
	showcontentofsection('about_me');
}
</script>

<?php if($error == 'Please enter all the mandatory card details.'){?>
<script language="javascript">
showcontentofsection('payment');
</script>
<?php } ?>


</body>
</html>