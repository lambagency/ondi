<?php
//print_r($shortlistdata);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/customer_top');?>
<!--Start of customer Area-->
	<div class="customer_main_con">
    	<div class="customer_main">
        	<?php $this->load->view('templates/customer_main_left');?>
            <div class="customer_resort_right">
            	<div class="heading">
                	<h1>My Shortlist</h1>
                </div>
				<?php 
				//print_r($shortlistdata); die;
				if(sizeof($shortlistdata)>0){
					foreach($shortlistdata as $shortlist){
						$how_many_left = $this->offers->how_many_left($shortlist->offer_id);
						//$how_many_left = sizeof($how_many_left_data);
						?>
						<div class="cus_short_list">
							<table class="table_customer">
							  <tr>
								<td class="image">
								<?php
								if($shortlist->profile_picture != "")
								{
									$shortlist_profile_picture = $shortlist->profile_picture;
								}
								else
								{
									$shortlist_profile_picture = "public/images/merchant-noimage.jpg";
								}
								?>
								<img src="<?php echo $this->config->item('base_url'); ?><?=$shortlist_profile_picture?>" alt="" width="121" height="101" /></td>
								<td class="address">
										<?php
										if(strlen($shortlist->business_name)>20) $bname=substr($shortlist->business_name, 0, 20)."..";
										else $bname = $shortlist->business_name;
										?>
										<h3><?=$bname?></h3>
										<?php
										$suburb_x = $shortlist->suburb;
										if(strlen($suburb_x)>10) $suburb_x=substr($suburb_x, 0, 10)."..";
										?>
										<h4><?=$suburb_x?></h4>
										<?php
										$categs = $this->users->display_merchant_business($shortlist->business_type);
										if(strlen($categs)>30) $categs=substr($categs, 0, 30)."..";
										?>
										<p><?=$categs?></p>
										<div class="cradit"><p><?=$shortlist->pg_name?><span></span></p><img src="<?php echo $this->config->item('base_url'); ?>public/images/<?=$this->offers->get_offer_star_ratings($shortlist->offer_id)?>.png" alt="" /></div>
								</td>
								<td class="name"><a href="<?php echo base_url("merchantoffers/detail/?m=".base64_encode($shortlist->merchant_id)); ?>"><?=$shortlist->offer_title?></a></td>
								<td class="value"><p>$<?=$shortlist->price?> <span>(Value $<?=$shortlist->price_normally?>)</span></p></td>
							  </tr>
							  <tr class="left_border">
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td class="remove">
									<ul>
										<li>
											<a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/share.png" alt="">
											<p>Share</p></a>
											<span class="nav_social">
													<a target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($shortlist->offer_id)?>&t=<?=urlencode($shortlist->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
													<a target="_blank"  href="https://twitter.com/home?status=<?=urlencode($shortlist->offer_title)?>%20-%20<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
													<a target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
											</span>
										</li>
										<li class="remove_gray">
											<a href="javascript:void(0);" onclick="remove_shortlist(<?=$shortlist->offer_id?>);"><img src="<?php echo $this->config->item('base_url'); ?>public/images/remove_icon.jpg" alt="">
											<p>Remove</p></a>
										</li>
									</ul>
								</td>
								<td class="book"><a class="book_button" href="<?php echo base_url("booking/index/?oid=".base64_encode($shortlist->offer_id)); ?>">BOOK</a>
									<?php if($how_many_left>0){?><div class="only_left">Only <?=$how_many_left?> left</div><?php } ?>
								</td>
							  </tr>
							</table>
                            
                            
                            
                            
                            
                            
                            
                            <div class="shortlist_mobile">
                            	<div class="list">
                                	<div class="image"><img src="<?php echo $this->config->item('base_url'); ?><?=$shortlist->profile_picture?>" alt="" width="121" height="101" /></div>
                                    
                                    <div class="detail">
                                    	<div class="address">
                                            <h3><?=$shortlist->business_name?></h3>
                                            <h4><?=$shortlist->suburb?></h4>
                                            <p><?=$this->users->display_merchant_business($shortlist->business_type)?></p>
                                            <div class="cradit"><p><?=$shortlist->pg_name?><span></span></p><img src="<?php echo $this->config->item('base_url'); ?>public/images/<?=$this->offers->get_offer_star_ratings($shortlist->offer_id)?>.png" alt="" /></div>
                                            
                                        </div>
                                        
                                    
                                    </div>
                                    
                                    <div class="name"><a href="<?php echo base_url("merchantoffers/detail/?m=".base64_encode($shortlist->merchant_id));?>">
                                        <?=$shortlist->offer_title?></a></div>
                                        <div class="value"><p>$<?=$shortlist->price?> <span>(Value $<?=$shortlist->price_normally?>)</span></p></div>
                                        
                                                                    
                                </div>
                                
                                <div class="click">
                                		<div class="remove">
                                        		<ul>
                                                    <li>
                                                        <a href="#"><img src="<?php echo $this->config->item('base_url'); ?>public/images/share.png" alt="">
                                                        <p>Share</p></a>
                                                        <span class="nav_social">
                                                                <a target="_blank" href="http://www.facebook.com/sharer.php?u=<?=$this->offers->get_offer_url($shortlist->offer_id)?>&t=<?=urlencode($shortlist->offer_title)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_fb.png" alt="" /></a>
                                                                <a target="_blank"  href="https://twitter.com/home?status=<?=urlencode($shortlist->offer_title)?>%20-%20<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_twitter.png" alt="" /></a>
                                                                <a target="_blank"  href="https://plus.google.com/share?url=<?=$this->offers->get_offer_url($shortlist->offer_id)?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/cus_g+.png" alt="" /></a>
                                                        </span>
                                                    </li>
                                                    <li class="remove_gray">
                                                        <a href="javascript:void(0);" onclick="remove_shortlist(<?=$shortlist->offer_id?>);"><img src="<?php echo $this->config->item('base_url'); ?>public/images/remove_icon.jpg" alt="">
                                                        <p>Remove</p></a>
                                                    </li>
                                                </ul>
                                        </div>
                                        
                                        <div class="book">
                                        	<a class="book_button" href="<?php echo base_url("booking/index/?oid=".base64_encode($shortlist->offer_id)); ?>">BOOK</a>
											<?php if($how_many_left>0){?><div class="only_left">Only <?=$how_many_left?> left</div><?php } ?>
                                        </div>
                                </div>
                            	
                            </div>
                            
                            
						</div>
				<?php 
					}// end foreach
				} else{ ?>
						<div class="cus_short_list">
							<p>No records found</p>
						</div>
				<?php } ?>
			</div>
		</div>
    </div>
<!--Start of customer Area-->
<?php $this->load->view('templates/footer');?>
</body>
</html>
