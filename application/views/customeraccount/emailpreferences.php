<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/customer_top');?>



<!--Start of customer Area-->

	<div class="customer_main_con">
    	<div class="customer_main">
        	<?php $this->load->view('templates/customer_main_left');?>
			<?php echo form_open_multipart(base_url()."customeraccount/emailpreferences/")?>
			<input type="hidden" name="submit" value="1" />
            <div class="customer_main_right">
            	<div class="heading">
                	<h1>Newsletter Preferences</h1>
                </div>
                <div class="sub_heading">
                	<h3>Tell us which emails you'd like to receive. You can change these at any time.</h3>
                </div>
                	<div class="cus_newsletter" id="checkboxesdiv">
                    	<ul>
                        	<li>
                            	<h3>Categories</h3>
                                <div class="option_list">
                                	<div class="left_option"><input type="checkbox" value="<?=$userdata[0]->subscribe_lastminuteoffers?>" name="subscribe_lastminuteoffers" id="subscribe_lastminuteoffers" <?php if($userdata[0]->subscribe_lastminuteoffers == '1'){ echo "checked='checked'"; } ?> onclick="resetunsubscribe('subscribe_lastminuteoffers');" ></div>
                                    <div class="right_option">Last minute offers in your area</div>
                                </div>
                                 <div class="option_list">
                                	<div class="left_option"><input type="checkbox" value="<?=$userdata[0]->subscribe_occasions?>" name="subscribe_occasions" id="subscribe_occasions" <?php if($userdata[0]->subscribe_occasions == '1'){ echo "checked='checked'"; } ?> onclick="resetunsubscribe('subscribe_occasions');" ></div>
                                    <div class="right_option">Occasions</div>
                                </div>
                            </li>
                            
                            <li>
                            	<h3>Email Notifications</h3>
                                <div class="option_list">
                                	<div class="left_option"><input type="checkbox" value="<?=$userdata[0]->subscribe_news_updates?>" name="subscribe_news_updates" id="subscribe_news_updates"  <?php if($userdata[0]->subscribe_news_updates == '1'){ echo "checked='checked'"; } ?> onclick="resetunsubscribe('subscribe_news_updates');" ></div>
                                    <div class="right_option">News and updates</div>
                                </div>
                                 <div class="option_list">
                                	<div class="left_option"><input type="checkbox" value="<?=$userdata[0]->subscribe_rewards?>" name="subscribe_rewards" id="subscribe_rewards"  <?php if($userdata[0]->subscribe_rewards == '1'){ echo "checked='checked'"; } ?> onclick="resetunsubscribe('subscribe_rewards');" ></div>
                                    <div class="right_option">Rewards</div>
                                </div>
                                <div class="option_list">
                                	<div class="left_option"><input type="checkbox" value="<?=$userdata[0]->subscribe_surveys?>" name="subscribe_surveys" id="subscribe_surveys" <?php if($userdata[0]->subscribe_surveys == '1'){ echo "checked='checked'"; } ?> onclick="resetunsubscribe('subscribe_surveys');" ></div>
                                    <div class="right_option">Surveys</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="subscribing">
                    	<p>By subscribing, I agree to the ONDI privacy statements.</p>
                    </div>
            
            
            
        		   <div class="save_bg10">
                    	<div class="save"><input type="image" src="<?php echo $this->config->item('base_url'); ?>public/images/save_btn.jpg" alt="" /></div>
                        <div class="upper"><p>Unsubscribe from all ONDI emails</p><input type="checkbox" value="1" name="unsubscribefromall" id="unsubscribefromall" onclick="uncheckallsubs();" ></div>
                    </div>
            
        </div>
		<?php echo form_close()?>
    	
        </div>
    </div>

<!--Start of customer Area-->


<?php $this->load->view('templates/footer');?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 
	

<script src="<?php echo $this->config->item('base_url'); ?>public/assets/check/jquery.screwdefaultbuttonsV2.js"></script>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 

<!--Price Selector content--> 
<script type="text/javascript">
   	$('.cus_newsletter ul li .option_list .left_option input:checkbox').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',
			width: 29,
			height: 29
	});
	
	$('.save_bg10 input:checkbox').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',
			width: 29,
			height: 29
	});	
	
		/*$('.cus_newsletter ul li .option_list .left_option input:checkbox').screwDefaultButtons({
			image: 'url("<?php echo $this->config->item('base_url'); ?>public/images/agree.jpg")',
			width: 29,
			height: 29
	});*/
	
		
</script>


<?php if($userdata[0]->subscribe_updated_on == '0000-00-00' && $userdata[0]->subscribe_newsletter != '0'){?>
<script type="text/javascript">
var collection = document.getElementById('checkboxesdiv').getElementsByTagName('input');
//alert(collection.length);
for (var x=0; x<collection.length; x++) {
	if (collection[x].type.toUpperCase()=='CHECKBOX')
	collection[x].checked = true;
	collection[x].value="1";
}

if (document.querySelector)
{
	var checkedhobbies=document.querySelectorAll('#checkboxesdiv .styledCheckbox')
	for (var i=0; i<checkedhobbies.length; i++){
		checkedhobbies[i].style.backgroundPosition = '0px -28px';
	}
}
else
{
	var divx = document.getElementById('checkboxesdiv').getElementsByClassName('styledCheckbox');
	for (var x=0; x<collection.length; x++) {
		divx[x].style.backgroundPosition = '0px -28px';
	}
}

</script>
<?php } ?>


</body>
</html>
