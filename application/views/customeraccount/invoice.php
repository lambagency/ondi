<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />

<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/jquery.PrintArea.js_4.js"></script>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/core.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>
<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>


<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" media="print" type="text/css" />

</head>
<body >
<div class="inner_content_bg"  style="width: 770px;">
	<div class="confirmation_content" style="width: 750px;">
        <div class="confirmation_detail">
			<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="<?php echo $this->config->item('base_url'); ?>public/mailerimages/mailer_logo2.jpg"></div>
            <div class="confirmation_order" style="width: 100%;  overflow: hidden;">
            	<div class="confirmation_order_head"><?=$dataorders[0]->offer_title?> <span>for</span> $<?=$dataorders[0]->price?></div>
                <div class="confirmation_order_tagline"><?=$dataorders[0]->offer_description?></div>
                <div class="confirmation_order_main">
                	<ul>
                    	<li>
                        	<div class="left_main">at</div>
                            <div class="right_main"><?=$dataorders[0]->business_name?></div>
                        </li>
                        <li>
                        	<div class="left_main">located</div>
                            <div class="right_main"><?=$dataorders[0]->street_address?> <?=$dataorders[0]->suburb?> <?=$dataorders[0]->state?> <?=$dataorders[0]->post_code?></div>
                        </li>
                        <li>
                        	<div class="left_main">for</div>
                            <div class="right_main"><?=$dataorders[0]->number_of_people?> people</div>
                        </li>
                        <li>
                        	<div class="left_main">on</div>
                            <div class="right_main"><?=date("l d/m/Y", strtotime($dataorders[0]->booking_date))?></div>
                        </li>
                        <li>
                        	<div class="left_main">time</div>
                            <div class="right_main"><?=date("h:i A", strtotime($dataorders[0]->booking_time))?></div>
                        </li>
                        <li>
                        	<div class="left_main">totalling</div>
                            <div class="right_main">$<?=$dataorders[0]->total_price?></div>
                        </li>
						<li>
                        	<div class="left_main">booking code</div>
                            <div class="right_main"><?php if($dataorders[0]->booking_code!='') echo $dataorders[0]->booking_code; else echo $bc; ?></div>
                        </li>
                    </ul>
                    
                </div>
            </div>
        </div><!--End of review_detail-->
    </div><!--End of inner_content_bg-->
</div><!--End of inner_content_bg-->
<!--End of Payment Option Page_Content-->
<?php
if(isset($_GET['print']) && $_GET['print'] == 'yes')
{
?>
<script language="javascript">
window.print();
</script>
<?php 
}
?>
</body>
</html>
