<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
    
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />

</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of Blog Page_Content-->
<div class="inner_content_bg">
	<div class="blog_page_content">
    	<div class="heading">
        	<h1>THE ONDI BLOG</h1>
        </div>
        <div class="sub_heading">
        	<h3>News and updates. Plus other stuff that is happening in our world!</h3>
			<?php if(isset($_GET['act']) && $_GET['act']=='suc') echo "<h3 style='color:#FF0000;'>Your comment has been queued for review by site administrators and will be published after approval.</h3>"; ?>
        </div>
        <div class="blog_page_detail">
          <div class="details_top_left_main">	
        	<div class="detail_blog_left">
            	<div class="blog_left_detail">
               	  <?php if($data_post->post_image != ''){?>
					<img src="<?php echo $this->config->item('base_url'); ?><?=$data_post[0]->post_image?>" alt="" />
				  <?php } ?>
              	  <h2><?=$data_post[0]->post_title?></h2>
               	  <h4><?=date("jS F Y", strtotime($data_post[0]->added_date))?></h4>
                  <h3><?=$data_post[0]->short_desc?></h3>
                  <p><?=$data_post[0]->post_desc?></p>
			</div>
			<div class="blog_category">
            	<div class="blog_category_left"><span>Category:</span> <?=$categories?></div>
                <div class="blog_category_right"><span>Tags:</span> <?=$tags?></div>
            </div>
            
            <div class="blog_share">
            	<ul>
                	<li><img src="<?php echo $this->config->item('base_url'); ?>public/images/share_blog.jpg" alt="" /></li>
                    <li><a target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $this->config->item('base_url'); ?>blog/detail/?p=<?=$data_post[0]->post_id?>&t=<?=urlencode($data_post[0]->post_title)?>" ><img src="<?php echo $this->config->item('base_url'); ?>public/images/fb_blog.jpg" alt="" /></a></li>
                    <li><a target="_blank" href="https://twitter.com/home?status=<?=urlencode($data_post[0]->post_title)?>%20-%20<?php echo $this->config->item('base_url'); ?>blog/detail/?p=<?=$data_post[0]->post_id?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/twitter_blog.jpg" alt="" /></a></li>
                    <li><a target="_blank" href="https://plus.google.com/share?url=<?php echo $this->config->item('base_url'); ?>blog/detail/?p=<?=$data_post[0]->post_id?>"><img src="<?php echo $this->config->item('base_url'); ?>public/images/gmail_blog.jpg" alt="" /></a></li>
                </ul>
            </div>
            
            <div class="blog_share_thought">
            	<h3>Share your thoughts</h3>
				<?php 
				$total_comments = sizeof($parent_comments);
				?>
                <h4><?=$total_comments?> Comment<?php if($total_comments>1){?>s<?php } ?> <span><a href="#commentform">+Add a Comment</a></span></h4>
                <ul>
					<?php 
					$c= 0;
					
					foreach($parent_comments as $parent_comment)
					{
					$c++;
					?>
                    <li <?php if($c==$total_comments){ echo 'class="no_border"';} ?> >
                    	<div class="left_thought"><img src="<?php echo $this->config->item('base_url'); ?><?=$this->users->getuserimage($parent_comment->user_id)?>" alt="" width="52" /></div>
                        <div class="right_thought">
                        	<p><?=$parent_comment->post_comment?></p>
                            <p><span><a href="javascript:void(0);" onclick="gotoreply(<?=$parent_comment->comment_id?>);">Reply</a> · <a href="#">Like</a> · </span><?=date("F j", strtotime($parent_comment->added_date))?> at <?=date("g:i A", strtotime($parent_comment->added_date))?></p>
                            <?php
							$child_comments = $this->ondiblog->getchildcomments($parent_comment->comment_id);
							if(sizeof($child_comments)>0)
							{
							?>
							<ul>
								<?php 
								foreach($child_comments as $child_comment)
								{
								?>
                            	<li>
                                    <div class="left_thought"><img src="<?php echo $this->config->item('base_url'); ?><?=$this->users->getuserimage($child_comment->user_id)?>" alt="" width="50" /></div>
                                    <div class="right_thought">
                                    <p><?=$child_comment->post_comment?></p>
                                    <p><span><a href="javascript:void(0);" onclick="gotoreply(<?=$parent_comment->comment_id?>);" >Reply</a> · <a href="#">Like</a> · </span><?=date("F j", strtotime($child_comment->added_date))?> at <?=date("g:i A", strtotime($child_comment->added_date))?></p>
                                    </div>
                                </li>
								<?php
								}
								?>
                            </ul>
							<?php
							}
							?>
                        </div>
                    </li>
					<?php
					}
					?>
                </ul>
            </div>
            
            <a name="commentform"></a>
            <div class="leave_comment">
				<?php
				$sessiondata = $this->session->all_userdata();
				$session_id = $sessiondata['session_id'];
				if(isset($sessiondata['logged_user']))
				{
				?>
				<form name="ct_form" action="" method="post" onsubmit="return validatecommentformloggedin();" >
				<?php
				}
				else
				{
				?>
				<form name="ct_form" action="" method="post" onsubmit="return validatecommentformnonloggedin();" >
				<?php
				}
				?>
				<input type="hidden" name="actcomment" value="1" />
				<input type="hidden" name="parent_comment_id" id="parent_comment_id" value="" />
				
					<h5>Leave a Comment</h5>
                   <h6> Your email address will not be published. Reqired fields are marked*</h6>
					<?php
					if(!isset($sessiondata['logged_user']))
					{
					?>
					<div class="field"><input name="uname" type="text" value="" placeholder="*Name" ></div>
					<div class="field" style="margin:0px;"><input name="email" type="text" placeholder="*Email address"></div>  
					<?php
					}
					?>
					<div class="area">
						<textarea name="comment" cols="" rows="" placeholder="*Comment"></textarea>
					</div>
					<div class="submit_info_btn">
							<input type="image" alt="" src="<?php echo $this->config->item('base_url'); ?>public/images/comment_submit.jpg">
					</div>
				</form>
            </div>
            
            
            
            </div>
            
            </div><!--End of left main-->
            
            <?php $this->load->view('templates/blog_right');?>
        </div>
    </div><!--End of blog_page_content-->
</div>
<!--End of Blog Page_Content-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


</body>
</html>
