<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Services on demand - Ondi.com</title>
<script type="text/javascript">$baseURL = '<?php echo $this->config->item('base_url'); ?>';</script>
<link href="<?php echo $this->config->item('base_url'); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->config->item('base_url'); ?>public/css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="<?php echo $this->config->item('base_url'); ?>public/assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
			<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/sliderstyle1.css" />
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $this->config->item('base_url'); ?>public/assets/select/easydropdown.css"/>
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/select/jquery.easydropdown.js"></script>

<?php if ($this->agent->is_mobile())
{
?>
    <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common_mobile.js"></script>
<?php	
    }
else
{
?>
   <script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/assets/js/common.js"></script>

<?php
}
?>
    
<script type="text/javascript" src="<?php echo $this->config->item('base_url'); ?>public/js/common.js"></script>

<link href="<?php echo $this->config->item('base_url'); ?>public/css/media.css" rel="stylesheet" type="text/css" />


</head>
<body>
<?php $this->load->view('templates/header');?>
<?php $this->load->view('templates/booking_top');?>
<!--Start of Blog Page_Content-->
<div class="inner_content_bg">
	<div class="blog_page_content">
    	<div class="heading">
        	<h1>THE ONDI BLOG</h1>
        </div>
        <div class="sub_heading">
        	<h3>News and updates. Plus other stuff that is happening in our world!</h3>
        </div>
        <div class="blog_page_detail">
        	<div class="blog_page_left">
            	<?php 
				$total_posts = sizeof($data_posts); 
				if($total_posts>0)
				{
				?>
				<ul>
					<?php 
					$f=0;
					$p=0;
					foreach($data_posts as $post)
					{
						$f++;
						$p++;
					?>
                	<li>
						<?php if($post->post_image != ''){?>
                    		<img src="<?php echo $this->config->item('base_url'); ?><?=$post->post_image?>" alt="" />
						<?php } ?>
                        <h2><?=$post->post_title?></h2>
                        <h4><?=date("jS F Y", strtotime($post->added_date))?></h4>
                        <p><?=substr($post->post_desc, 0, 310)?></p>
                        <a href="<?php echo $this->config->item('base_url'); ?>blog/detail/?p=<?=$post->post_id?>" class="continue_reading">
                        	<?php if($p==1){?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/continue_reading_blue.jpg" alt="" />
                            <?php } else if($p==2){?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/continue_reading_pink.jpg" alt="" />
                            <?php } else if($p==3){?>
                            <img src="<?php echo $this->config->item('base_url'); ?>public/images/continue_reading_green.jpg" alt="" />
                            <?php } ?>
                        </a>
                    </li>
                    <?php
					
					if($p==3) $p = 0;
					}
					?>
                </ul>
				<?php
				}
				else
				{
				?>
				<p>No Posts found.</p>
				<?php
				}
				?>
            </div>
            <?php $this->load->view('templates/blog_right');?>
        </div>
    </div><!--End of blog_page_content-->
</div>
<!--End of Blog Page_Content-->
<?php $this->load->view('templates/listyourbusinessbox');?>
<?php $this->load->view('templates/footer');?>


<link rel="stylesheet" href="<?php echo $this->config->item('base_url'); ?>public/assets/radio/jquery.checkbox.css" />
<script type="text/javascript" src="<?=base_url('/public/assets/radio/jquery.checkbox.min.js'); ?>"></script>

<!--FOR GENDER SLIDER-->
<script src="<?php echo $this->config->item('base_url'); ?>public/assets/gender/jquery-ui.js" type="text/javascript"></script> 
<!--Price Selector content--> 


</body>
</html>
