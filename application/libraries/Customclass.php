<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customclass {
    
	public function __construct()
	{
		$ci = & get_instance();
		$ci->input->cookie('test_cookie', TRUE);

		parse_str( $_SERVER['QUERY_STRING'], $_REQUEST );
		$ci->config->load("facebook",TRUE);
		$config = $ci->config->item('facebook');
		$ci->load->library('Facebook', $config);
		parse_str($_SERVER['QUERY_STRING'],$_GET);
		$ci->load->library('openid');
		error_reporting(0);
	}
	public function getcities()
    {
		$ci = & get_instance();
		$ci->db->select('id, city_name');
		//$ci->db->order_by("default_city", "desc");
		$ci->db->order_by("display_order", "asc");
		$query = $ci->db->get('city');
		$cities = $query->result();
		//$data = array('cities' => $cities);
		return $cities;
    }

	public function getusercity()
	{
		$ci = & get_instance();
		$system_ip=$_SERVER['REMOTE_ADDR'];
		$system_ip = "122.177.213.90";
		//$system_ip = "203.26.235.14";
		//echo "TEST";
		$ci->load->library('ip2location');
		$ci->ip2location->open('public/databases/IP-COUNTRY-REGION-CITY.BIN');
		$record = $ci->ip2location->getAll($system_ip);
		//print_r($record); die;
		$city_name = $record->city;
		$latitude = $record->latitude;
		$longitude = $record->longitude;
		if(trim($latitude) == 'This field is not supported in DB3. Please upgrade your IP2Location database.')
		{
			$latitude = "-25.274398";
		}
		if(trim($longitude) == 'This field is not supported in DB3. Please upgrade your IP2Location database.')
		{
			$longitude = "133.775136";
		}

		$ci->db->select('id');
		$cityquery = $ci->db->get_where('city', array('city_name' => $city_name));
		$citydata = $cityquery->result();
		if(sizeof($citydata)>0)
		{
			$newdata = array('user_city_id'  => $citydata[0]->id, 'user_city_name'  => $city_name, 'latitude'  => $latitude, 'longitude'  => $longitude);
			$ci->session->set_userdata($newdata);
		}
		else
		{
			$default_city_query = $ci->db->get_where('city', array('default_city' => '1'));
			$default_city_data = $default_city_query->result();
			$newdata = array('user_city_id'  => $default_city_data[0]->id, 'user_city_name'  => $default_city_data[0]->city_name, 'latitude'  => $latitude, 'longitude'  => $longitude);
			$ci->session->set_userdata($newdata);
		}
	}

	public function checkfirsttimeuser()
	{
		if(isset($_COOKIE['firsttimeuser']))
		{
			return "no";
		}
		else
		{
			setcookie("firsttimeuser", "1", time()+24*60*60*30);
			return "yes";
		}
	}
	public function getLoggedInUserData()
	{
		$ci = & get_instance();
		$user_id=$ci->CI_auth->logged_id();
		$sess_user_data = $ci->session->all_userdata();
		if($user_id != '')
		{
			if($sess_user_data['user_type']=='1'){
				$ci->db->select('user_id, first_name, last_name, email, mobile_number, postcode, birthday, customer_picture, gender, subscribe_newsletter, interests, facebook_id');
				$userquery = $ci->db->get_where('users', array('user_id' => $user_id));
				//print_r($userquery);
				$loggedinuserdata = $userquery->result();
				return $loggedinuserdata;
			}
			else {
				//$ci->db->select('first_name, last_name, email, mobile_number, postcode, birthday, customer_picture, gender, subscribe_newsletter, interests');
				//$userquery = $ci->db->get_where('users', array('user_id' => $user_id));
				//print_r($userquery);
				//$loggedinuserdata = $userquery->result();
				
				if($sess_user_data['user_type']=='2')
				{
					$condition=array('a.user_id' => $user_id);
					$ci->db->select('a.*, b.profile_picture, b.business_name ');
					$ci->db->from('users as a');
					$ci->db->join('merchant_businessprofile as b', 'b.user_id = a.user_id', 'left');
					$ci->db->where($condition);
					$userquery = $ci->db->get();
					$loggedinuserdata = $userquery->result();
				}
				else if($sess_user_data['user_type']=='3')
				{
					$sql_get_parent = "SELECT main_merchant_id FROM merchant_users WHERE user_id = '".$user_id."' ";
					$query_get_parent = $ci->db->query($sql_get_parent);
					$data_get_parent = $query_get_parent->result();
					$main_merchant_id = $data_get_parent[0]->main_merchant_id;
					
					$condition=array('a.user_id' => $main_merchant_id);
					$ci->db->select('a.*, b.profile_picture, b.business_name ');
					$ci->db->from('users as a');
					$ci->db->join('merchant_businessprofile as b', 'b.user_id = a.user_id', 'left');
					$ci->db->where($condition);
					$userquery = $ci->db->get();
					$loggedinuserdata = $userquery->result();
				}



				return $loggedinuserdata;
			}
		}
	}

	public function whatsondemand()
	{	
		$ci = & get_instance();
		$return_array = array();
		$offer_array = array();
		
		/*SELECT COUNT( * ) AS numorders, offer_id
FROM `orders`
GROUP BY offer_id
ORDER BY numorders DESC
LIMIT 0 , 30
		

		$this->db->select("SELECT COUNT(*) AS numorders, offer_id");	
		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');			
		$this->db->where($condition);
		$this->db->group_by('o.offer_id');
		$this->db->order_by('total_offer_count','desc');
*/
		$sess_user_data = $ci->session->all_userdata();
		if(isset($sess_user_data['user_city_id']))
		{
			$user_city_name = $sess_user_data['user_city_name'];
			$user_city_id = $sess_user_data['user_city_id'];
		}
		else
		{
			$default_city_query = $ci->db->get_where('city', array('default_city' => '1'));
			$default_city_data = $default_city_query->result();
			$user_city_name = $default_city_data[0]->city_name;
			$user_city_id = $default_city_data[0]->id;
		}
		
		
		//$sql_orders = "SELECT COUNT(*) AS numorders, a.offer_id, c.city FROM orders a LEFT JOIN merchant_offers b ON a.offer_id = b.id LEFT JOIN merchant_businessprofile c ON b.merchant_id = c.user_id WHERE c.city = '".$user_city_id."' GROUP BY a.offer_id ORDER BY numorders DESC LIMIT 0 , 5";

		/*$sql_orders = "SELECT COUNT(*) AS numorders, a.offer_id, c.city FROM orders a LEFT JOIN merchant_offers b ON a.offer_id = b.id LEFT JOIN merchant_businessprofile c ON b.merchant_id = c.user_id WHERE c.city = '".$user_city_id."' AND b.offer_running_to >= '".date("Y-m-d")."' GROUP BY a.offer_id ORDER BY numorders DESC LIMIT 0 , 6";*/



		$search_date_x = date("Y-m-d");
		$daysearched = strtolower(date("l", strtotime($search_date_x)));
		$current_mil_time = date("Hi");

		$sql_orders = "SELECT COUNT(*) AS numorders, a.offer_id, c.city, (SELECT MAX(x.to_hours)
FROM merchant_offers_available_time x
WHERE x.offer_id = a.offer_id AND x.available_day = '".$daysearched."' 
) AS maxtime

FROM orders a LEFT JOIN merchant_offers b ON a.offer_id = b.id LEFT JOIN merchant_businessprofile c ON b.merchant_id = c.user_id WHERE c.city = '".$user_city_id."' AND b.offer_running_to >= '".date("Y-m-d")."' GROUP BY a.offer_id  HAVING maxtime > ".$current_mil_time." ORDER BY numorders DESC LIMIT 0 , 6";
		
		 
		$query_orders = $ci->db->query($sql_orders);
		$count_orders = $query_orders->num_rows();
		if($count_orders>0)
		{
			foreach($query_orders->result() as $tmp)
			{
				$oid = $tmp->offer_id;
				$this_offer_details = $ci->offers->get_offer_details($oid);
				
				
				array_push($return_array, $this_offer_details);
				array_push($offer_array, $oid);
				
			}
		}
			
		
		
		//$condition=array('id' => '1');
		$ci->db->select('*');
		$ci->db->from('city');
		$condition=array('id' => $user_city_id); // as per new update.
		$ci->db->where($condition);
		$demand_query = $ci->db->get();		
		$demand_data = $demand_query->result();

		$placeholder_1 = $demand_data[0]->placeholder_1;
		$placeholder_2 = $demand_data[0]->placeholder_2;
		$placeholder_3 = $demand_data[0]->placeholder_3;
		$placeholder_4 = $demand_data[0]->placeholder_4;
		$placeholder_5 = $demand_data[0]->placeholder_5;
		$placeholder_6 = $demand_data[0]->placeholder_6;

		if($placeholder_1 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_1." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_1);
				array_push($return_array, $this_offer_details);
			}
		}
		
		if($placeholder_2 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_2." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_2);
				array_push($return_array, $this_offer_details);
			}
		}


		if($placeholder_3 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_3." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_3);
				array_push($return_array, $this_offer_details);
			}
		}


		if($placeholder_4 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_4." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_4);
				array_push($return_array, $this_offer_details);
			}
		}


		if($placeholder_5 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_5." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_5);
				array_push($return_array, $this_offer_details);
			}
		}

		if($placeholder_6 != "")
		{
			$sql_maxtime_1 = "SELECT MAX(to_hours) as maxtime FROM merchant_offers_available_time WHERE offer_id = ".$placeholder_6." AND available_day = '".$daysearched."'";
			$query_maxtime_1 = $ci->db->query($sql_maxtime_1);
			$data_maxtime_1 = $query_maxtime_1->result();
			$maxtime = $data_maxtime_1[0]->maxtime;
			if($maxtime>$current_mil_time)
			{
				$this_offer_details = $ci->offers->get_offer_details($placeholder_6);
				array_push($return_array, $this_offer_details);
			}
		}

		//print_r($return_array); die;
		return $return_array;
	}
	public function commonclassfunction()
	{
		$shortlist_offers = array();
		////// SYNC SHORTLISTS IF ANY ////////
		$ci = & get_instance();
		$user_id=$ci->CI_auth->logged_id();
		$sess_user_data = $ci->session->all_userdata();
		$session_id = $ci->session->userdata('session_id');
		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $ci->session->all_userdata();
		$session_id = $session_user_data['session_id'];

		$sql_count = "SELECT offer_id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND session_id = '".$session_id."' ";
		$query_count = $ci->db->query($sql_count);
		$count = $query_count->num_rows();
		$need_refresh = 0;
		if($count>0)
		{
			$shortlist_offers =  array();
			foreach($query_count->result() as $tmp)
			{
				$oid = $tmp->offer_id;
				//echo "user_id-->".$user_id;
				if($user_id != '')
				{
					$sql_count_shortlist = "SELECT sid FROM user_shortlists WHERE user_id = '".$user_id."' AND offer_id = '".$oid."' ";
					$query_count_shortlist = $ci->db->query($sql_count_shortlist);
					$count_shortlist = $query_count_shortlist->num_rows();
					if($count_shortlist==0)
					{
						$sql_insert = "INSERT INTO user_shortlists(user_id, offer_id ) VALUES ('".$user_id."', '".$oid."')";
						$query_count = $ci->db->query($sql_insert);
						$need_refresh = 1;
					}
				}
				//$sql_delete = "DELETE FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND offer_id = '".$oid."'";
				//$query_delete = $ci->db->query($sql_delete);
			}
		}
		//die;
		
		////// SYNC SHORTLISTS IF ANY ////////
		
		if(isset($_REQUEST['remember']) && $_REQUEST['remember'] == 'me')
		{
			$popuplogin_email = $_REQUEST['remember_email'];
			$popuplogin_pwd = $_REQUEST['remember_pw'];
			setcookie("ondi_login_email", $popuplogin_email, time()+24*60*60*30);
			setcookie("ondi_login_password", $popuplogin_pwd, time()+24*60*60*30);
		}

		if($need_refresh=='1')
		{
			echo '<script>window.location.reload();</script>';
		}
	}

	public function _ago($tm,$rcs = 0) {
	   $cur_tm = time(); 
	   $dif = $cur_tm-$tm;
	   $pds = array('second','minute','hour','day','week','month','year','decade');
	   $lngh = array(1,60,3600,86400,604800,2630880,31570560,315705600);
	   for($v = sizeof($lngh)-1; ($v >= 0)&&(($no = $dif/$lngh[$v])<=1); $v--); if($v < 0) $v = 0; $_tm = $cur_tm-($dif%$lngh[$v]);

	   $no = floor($no); if($no <> 1) $pds[$v] .='s'; $x=sprintf("%d %s ",$no,$pds[$v]);
	   if(($rcs == 1)&&($v >= 1)&&(($cur_tm-$_tm) > 0)) $x .= time_ago($_tm);
	   return $x." ago";
	}

	public function googlepluslogin($customer_email, $first_name)
	{
		$ci = & get_instance();
		$sql_1 =  "SELECT * FROM users WHERE email= '".$customer_email."' AND google_user = '1'  ";
		$query = $ci->db->query($sql_1);
		if ($query->num_rows() == 0)
		{
			$user_type = '1';
			$user_data = array('user_type'=>$user_type, 'email' => $customer_email, 'first_name'=>$first_name, 'google_user'=>'1');

			$ci->db->insert('users', $user_data);	
			$last_user_insert_id = $ci->db->insert_id();	

			/////////// INSERT NOTIFICATION //////////
			$notification_message = "We've noticed you haven't fully completed your profile details. Fill out your profile details, things you love and even save your credit card details for a faster booking experience.";
			$notification_data = array('customer_id'=>$last_user_insert_id, 'message' => $notification_message, 'profile_message' => '1', 'added_date'=>date("Y-m-d H:i:s"));
			$ci->db->insert('user_notifications', $notification_data);	
			/////////// INSERT NOTIFICATION //////////

			$info=array($customer_email, "google");
			$user_login_data = $ci->CI_auth->process_login_social($info);

			if($ci->CI_auth->check_logged()===FALSE)
			{						
				$ci->session->set_flashdata('signup_message', 'Please check your login details.');
				redirect('/signup/', 'refresh');
			}
			else
			{
				$newdata = array(
				   'user_type'  => $user_type,
				   'email'     => $customer_email,
				   'logged_in' => TRUE,
				   'contact_name' => ''
					);
				$ci->session->set_userdata($newdata);							
				redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
				exit();							
			}
		}
		else
		{
			$info=array($customer_email, "google");
			$user_login_data = $ci->CI_auth->process_login_social($info);
			if($ci->CI_auth->check_logged()===FALSE)
			{						
				$ci->session->set_flashdata('signup_message', 'Please check your login details.');
				redirect('/signup/', 'refresh');
			}
			else
			{
				$user_type = '1';
				$newdata = array(
				   'user_type'  => $user_type,
				   'email'     => $customer_email,
				   'logged_in' => TRUE,
				   'contact_name' => ''
					);
				$ci->session->set_userdata($newdata);							
				redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
				exit();							
			}
		}
	}
	
}

/* End of file Someclass.php */