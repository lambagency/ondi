<?php

class Signup extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library(array('form_validation', 'session', 'customclass', 'email'));
        $this->load->helper(array('form', 'url'));
        //$this->load->library('encrypt');
        $this->load->database();
        $this->load->model(array('users', 'CI_auth', 'CI_encrypt', 'Admin_Model', 'offers'));

    }

    public function index()
    {

        $this->form_validation->set_rules('customer_email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('customer_password', 'Password', 'trim|required');
        //$this->form_validation->set_rules('customer_postcode', 'Postcode', 'trim|required');

        if ($this->form_validation->run() == FALSE) {


        } else {


            $customer_email = $this->input->post('customer_email');
            $customer_password = $this->input->post('customer_password');
            $customer_postcode = $this->input->post('customer_postcode');
            $customer_referral_key = $this->input->post('customer_referral_key');
            $signup_action = $this->input->post('signup_action');
            $gender = $this->input->post('gender');

            if ($signup_action == 'login') {

            } else if ($signup_action == 'register') {

                if ((bool)$this->config->item('test_mode')) {
                    //print_r($_POST); die;

                }


                $user_email_count = $this->users->users_count_by_email($customer_email, "", "1"); // second parameter is

                if ($user_email_count > 0) {
                    $this->session->set_flashdata('signup_message', 'Email address already exist. Please try with someother email address.');
                    redirect('/signup/', 'refresh');

                } else {

                    $encrypt_pass = "";
                    $rand_salt = $this->CI_encrypt->genRndSalt();
                    $encrypt_pass = $this->CI_encrypt->encryptUserPwd($customer_password, $rand_salt);

                    if ($this->input->post('subscribe_newsletter') == '1') {
                        $subscribe_newsletter = '1';

                    } else {

                        $subscribe_newsletter = '0';

                    }

                    $user_type = '1';
                    $user_data = array('user_type' => $user_type, 'email' => $customer_email, 'password' => $encrypt_pass, 'salt' => $rand_salt, 'postcode' => $customer_postcode, 'referral_key' => $customer_referral_key, 'gender' => $gender, 'subscribe_newsletter' => $subscribe_newsletter);
                    $this->db->insert('users', $user_data);
                    $last_user_insert_id = $this->db->insert_id();

                    /////////// INSERT NOTIFICATION //////////
                    $notification_message = "We've noticed you haven't fully completed your profile details. Fill out your profile details, things you love and even save your credit card details for a faster booking experience.";
                    $notification_data = array('customer_id' => $last_user_insert_id, 'message' => $notification_message, 'profile_message' => '1', 'added_date' => date("Y-m-d H:i:s"));
                    $this->db->insert('user_notifications', $notification_data);

                    $notification_message1 = "Welcome to Ondi.com. Tell us a bit more about you so we can personalise your experience. Fill out your profile below.";
                    $notification_data1 = array('customer_id' => $last_user_insert_id, 'message' => $notification_message1, 'profile_message' => '1', 'added_date' => date("Y-m-d H:i:s"));
                    $this->db->insert('user_notifications', $notification_data1);

                    /////////// INSERT NOTIFICATION //////////


                    //////// Campaign Monitor Code //////////////
                    $this->load->library('csrestsubscribers');
                    $ResubscribeOption = true;
                    $CustomFields[] = array('Key' => "Custom Key", 'Value' => "Custom Value");
                    $wrap = new csrestsubscribers();
                    $result = $wrap->add(array(
                        'EmailAddress' => $customer_email,
                        'Name' => "",
                        'CustomFields' => array(
                            array(
                                'Key' => 'Field 1 Key',
                                'Value' => 'Field Value'
                            )
                        ),
                        'Resubscribe' => false
                    ));

                    /*echo "Result of POST /api/v3.1/subscribers/{list id}.{format}\n<br />";
                    if($result->was_successful()) {
                        echo "Subscribed with code ".$result->http_status_code;
                    } else {
                        echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
                        var_dump($result->response);
                        echo '</pre>';
                    }*/
                    //////// Campaign Monitor Code //////////////


                    $remember_me = "";
                    $info = array($customer_email, $customer_password, $user_type, $remember_me);
                    $user_login_data = $this->CI_auth->process_login($info);
                    ///echo $login_data = $this->CI_auth->logged_id();

                    if ($this->CI_auth->check_logged() === FALSE) {
                        $this->session->set_flashdata('signup_message', 'Please check your login details.');
                        redirect('/signup/', 'refresh');

                    } else {


                        $newdata = array(
                            'user_type' => $user_type,
                            'email' => $customer_email,
                            'logged_in' => TRUE,
                            'contact_name' => ''
                        );

                        $this->session->set_userdata($newdata);
                        //redirect('/customeraccount/contactinfo', 'refresh');
                        redirect('/customeraccount/editcontactinfo?act=editdeatils&cust=new', 'refresh');
                        exit();

                    }


                }


            } else {

                redirect('/signup/', 'refresh');
            }

        }
        $data['title'] = 'Sign Up';
        $this->load->view('/signup/index', $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('/', 'refresh');
    }

    public function ajaxlogin()
    {
        $popuplogin_email = $this->input->post('popuplogin_email');
        $popuplogin_pwd = $this->input->post('popuplogin_pwd');
        $is_merchant = $this->input->post('is_merchant');
        $rememberme = $this->input->post('rememberme');
        if ($is_merchant == '0') {
            $user_type = '1';
        } else {
            $user_type = '2';
        }
        $info = array($popuplogin_email, $popuplogin_pwd, $user_type, $rememberme);
        //$this->CI_auth->process_login($info);

        $user_login_data = $this->CI_auth->process_login($info);
        if ($this->CI_auth->check_logged() === FALSE) {
            echo "Ooops incorrect log in details!";
        } else {
            $session_user_data = $this->session->all_userdata();
            $logged_user_data = $this->users->get_user_detail($session_user_data['logged_user']);
            //print_r($logged_user_data);
            $newdata = array(
                'user_type' => $logged_user_data[0]->user_type,
                'email' => $logged_user_data[0]->email,
                'logged_in' => TRUE,
                'contact_name' => $logged_user_data[0]->contact_name,
                'first_name' => $logged_user_data[0]->first_name,
                'last_name' => $logged_user_data[0]->last_name
            );

            $this->session->set_userdata($newdata);

            if ($user_type == '2') {
                $user_id = $this->users->get_main_merchant_id();
            } else {
                $user_id = $session_user_data['logged_user'];
            }
            if ($rememberme == '1') {
                $exptime = time() + 24 * 60 * 60 * 30;

                //setcookie("ondi_login_email", $popuplogin_email, $exptime);
                //setcookie("ondi_login_password", $popuplogin_pwd, $exptime);
                //$this->config->set_item('sess_expire_on_close', FALSE);
                //$this->load->library('session');

                $exptime = time() + (3600 * 24 * 7);

                $this->load->helper('cookie');
                $cookie = array(
                    'name' => 'ondi_login_email',
                    'value' => $popuplogin_email,
                    'expire' => $exptime,
                    'domain' => $this->config->item('cookie_domain'),
                    'path' => $this->config->item('cookie_path'),
                    'prefix' => ''
                );
                set_cookie($cookie);

                $cookie = array(
                    'name' => 'ondi_login_password',
                    'value' => $popuplogin_pwd,
                    'expire' => $exptime,
                    'domain' => $this->config->item('cookie_domain'),
                    'path' => $this->config->item('cookie_path'),
                    'prefix' => ''
                );
                set_cookie($cookie);

                echo "successcookie~~~" . $this->users->user_steps_completed($user_id);
            } else {
                echo "success~~~" . $this->users->user_steps_completed($user_id);
            }
        }
    }

    public function forgotpassword()
    {
        $data['title'] = 'ONDI';
        $this->load->view('/signup/forgotpassword', $data);
    }

    public function ajaxforgotpassword()
    {
        $forgot_email = $this->input->post('forgot_email');
        $is_merchant = $this->input->post('is_merchant');
        $sql_1 = "SELECT * FROM users WHERE email= '" . $forgot_email . "' ";
        if ($is_merchant == '0') {
            $user_type = '1';
        } else {
            $user_type = '2';
        }
        if ($user_type != '') {
            if ($user_type == '1') {
                $sql_1 .= " AND user_type = '" . $user_type . "' ";
            } else {
                $sql_1 .= " AND ( user_type = '2' OR user_type = '3' ) ";
            }
        }
        $sql_1 .= " LIMIT 1 ";
        $query = $this->db->query($sql_1);
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $user_id = $row->user_id;
            $user_email = $row->email;

            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $temp_password = '';
            for ($i = 0; $i < 6; $i++) {
                $temp_password .= $characters[rand(0, strlen($characters) - 1)];
            }
            //$temp_password = "123456";


            $random_key = '';
            for ($i = 0; $i < 6; $i++) {
                $random_key .= $characters[rand(0, strlen($characters) - 1)];
            }

            $encrypt_pass = "";
            $rand_salt = $this->CI_encrypt->genRndSalt();
            $encrypt_pass = $this->CI_encrypt->encryptUserPwd($temp_password, $rand_salt);

            /*$data_update = array(
                   'temp_password' => $encrypt_pass,
                   'random_key' => $random_key,
                   'temp_salt' => $rand_salt
                );
            */
            $data_update = array(
                'random_key' => $random_key
            );
            $this->db->where('user_id', $user_id);
            $this->db->update('users', $data_update);

            /*
            // SEND EMAIL TO USER
            $message = "";
            $message .= "Please click on the below link";
            $message .= "<br>";
            $message .= "<?php echo $config['base_url']; ?>signup/activatenewpassword/?key=".$random_key;
            //$message .= "\n";
            //$message .= "\n";
            //$message .= "Username/Email : ".$user_email;
            //$message .= "\n";
            //$message .= "Password : ".$temp_password;
            $message .= "<br>";
            $message .= "<br>";
            $message .= "Thank you<br>";
            $message .= "Ondi Food";

            $html_mailer = '<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;"><body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff"><div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;"><div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;"><div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="<?php echo $config['base_url']; ?>public/mailerimages/mailer_logo2.jpg" ></div><div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;"><div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 5px; border: 1px solid #c0bdbd;">'.$message.'</div></div></div></div></body></html>';*/


            $message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
			 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
			  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear User, <br/>Please click on the below link<br/><br/>' . $config['base_url'] . 'signup/activatenewpassword/?key=' . $random_key . '</div>
			</li>
			</ul>';


            $html_mailer = '<!DOCTYPE html>
			<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
			<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<title>ONDI FOOD</title>
			</head>
			<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
			<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
			  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
				<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="' . $this->config->item('base_url') . 'public/mailerimages/mailer_logo2.jpg" ></div>
				<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
				 <img src="' . $this->config->item('base_url') . 'public/mailerimages/account_detail.jpg">
				</div>
				<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
				  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
					<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">' . $message . '</span></div>
				</div>
			  </div>
			</div>
			</body>
			</html>
			';


            /*
            $this->email->from('info@ondi.com', 'ONDI');
            $this->email->to($user_email);
            $this->email->subject('Forgot Password?');
            $this->email->message($html_mailer);
            $this->email->send();
            */

            $subject = "Forgot Password?";
            $this->offers->send_mail($user_email, $subject, $html_mailer);

            //echo "Instructions to reset the password has been sent to your email address.".$message."-".$temp_password;
            echo "Instructions to reset the password has been sent to your email address.";
        } else {
            echo "This email is not registered with us!";
        }
    }

    public function activatenewpassword()
    {
        $data['title'] = 'ONDI';
        if (!isset($_GET['key'])) {
            redirect('/signup/', 'refresh');
        } else {
            $key = $_GET['key'];
            $sql_1 = "SELECT * FROM users WHERE random_key= '" . addslashes($key) . "' ";
            $query = $this->db->query($sql_1);
            if ($query->num_rows() > 0) {
                $row = $query->row();
                $user_id = $row->user_id;

                if (isset($_POST['act'])) {
                    if ($_POST['act'] == 'restore') {
                        $pwd = $_POST['reset_pwd'];
                        $cpwd = $_POST['reset_cpwd'];
                        if ($pwd == $cpwd) {
                            $encrypt_pass = "";
                            $rand_salt = $this->CI_encrypt->genRndSalt();
                            $encrypt_pass = $this->CI_encrypt->encryptUserPwd($pwd, $rand_salt);

                            $data_update = array(
                                'password' => $encrypt_pass,
                                'salt' => $rand_salt,
                                'random_key' => '',
                                'temp_password' => ''
                            );
                            $this->db->where('user_id', $user_id);
                            $this->db->update('users', $data_update);
                            $data['message'] = 'Password has been successfully reset.';
                        }
                    }
                }
                /*
                $temp_password = $row->temp_password;
                $temp_salt = $row->temp_salt;
                $data_update = array(
                   'password' => $temp_password,
                   'salt' => $temp_salt,
                   'random_key' => '',
                   'temp_password' => ''
                );
                $this->db->where('user_id', $user_id);
                $this->db->update('users', $data_update);
                $data['message'] = 'Password has been successfully reset.';
                */
                $data['enc'] = base64_encode($user_id);
                $this->load->view('/signup/activatenewpassword', $data);
            } else {
                $data['message'] = 'Invalid Access';
            }
        }
        $this->load->view('/signup/forgotpassword', $data);
    }

    public function facebooklogin()
    {
        // Try to get the user's id on Facebook
        $userId = $this->facebook->getUser();
        // If user is not yet authenticated, the id will be zero
        if ($userId == 0) {
            // Generate a login url
            //$data['url'] = $this->facebook->getLoginUrl(array('scope'=>'email'));
            //$this->load->view('main_index', $data);
            $this->session->set_flashdata('signup_message', 'Facebook login failed.');
            redirect('/signup/', 'refresh');
        } else {
            // Get user's data and print it
            $user = $this->facebook->api('/me');
            if ((bool)$this->config->item('test_mode')) {
                //print_r($user);
                //exit();
            }
            $customer_email = $user['email'];
            $gender = $user['gender'];
            $facebook_id = $user['id'];
            $first_name = $user['first_name'];
            $last_name = $user['last_name'];
            $birthday = $user['birthday'];
            $formatted_birthday = "";
            if ($birthday != '') {
                $explode_birthday = explode("/", $birthday);
                if (sizeof($explode_birthday) == 3) {
                    $formatted_birthday = $explode_birthday[2] . "-" . $explode_birthday[1] . "-" . $explode_birthday[0];
                }
            }

            if ($gender == 'male') {
                $customer_gender = "M";
            } else if ($gender == 'female') {
                $customer_gender = "F";
            } else {
                $customer_gender = "";
            }
            $sql_1 = "SELECT * FROM users WHERE email= '" . $customer_email . "' AND facebook_id = '" . $facebook_id . "'  ";
            $query = $this->db->query($sql_1);
            if ($query->num_rows() == 0) {
                $user_type = '1';
                $user_data = array('user_type' => $user_type, 'email' => $customer_email, 'gender' => $customer_gender, 'first_name' => $first_name, 'last_name' => $last_name, 'facebook_id' => $facebook_id, 'birthday' => $formatted_birthday);

                $this->db->insert('users', $user_data);
                $last_user_insert_id = $this->db->insert_id();

                /////////// INSERT NOTIFICATION //////////
                $notification_message = "We've noticed you haven't fully completed your profile details. Fill out your profile details, things you love and even save your credit card details for a faster booking experience.";
                $notification_data = array('customer_id' => $last_user_insert_id, 'message' => $notification_message, 'profile_message' => '1', 'added_date' => date("Y-m-d H:i:s"));
                $this->db->insert('user_notifications', $notification_data);
                /////////// INSERT NOTIFICATION //////////

                $info = array($customer_email, "facebook");
                $user_login_data = $this->CI_auth->process_login_social($info);

                if ($this->CI_auth->check_logged() === FALSE) {
                    $this->session->set_flashdata('signup_message', 'Please check your login details.');
                    redirect('/signup/', 'refresh');
                } else {
                    $newdata = array(
                        'user_type' => $user_type,
                        'email' => $customer_email,
                        'logged_in' => TRUE,
                        'contact_name' => ''
                    );
                    $this->session->set_userdata($newdata);
                    redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
                    exit();
                }
            } else {
                $info = array($customer_email, "facebook");
                $user_login_data = $this->CI_auth->process_login_social($info);
                if ($this->CI_auth->check_logged() === FALSE) {
                    $this->session->set_flashdata('signup_message', 'Please check your login details.');
                    redirect('/signup/', 'refresh');
                } else {
                    $user_type = '1';
                    $newdata = array(
                        'user_type' => $user_type,
                        'email' => $customer_email,
                        'logged_in' => TRUE,
                        'contact_name' => ''
                    );
                    $this->session->set_userdata($newdata);
                    redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
                    exit();
                }
            }
        }
    }


    public function twitterlogin()
    {
        $this->load->library('twitteroauth');
        // Loading twitter configuration.
        $this->config->load('twitter');
        $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
        $request_token = $this->connection->getRequestToken(base_url('/signup/twittercallback'));
        $this->session->set_userdata('request_token', $request_token['oauth_token']);
        $this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);

        //print_r($request_token);
        if ($this->connection->http_code == 200) {
            $url = $this->connection->getAuthorizeURL($request_token);
            redirect($url);
        } else {
            // An error occured. Make sure to put your error notification code here.
            redirect(base_url('/'));
        }
    }

    public function twittercallback()
    {
        $this->load->library('twitteroauth');
        // Loading twitter configuration.
        $this->config->load('twitter');
        //$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
        if ($this->session->userdata('access_token') && $this->session->userdata('access_token_secret')) {
            // If user already logged in
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'), $this->session->userdata('access_token_secret'));
        } elseif ($this->session->userdata('request_token') && $this->session->userdata('request_token_secret')) {
            // If user in process of authentication
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
        } else {
            // Unknown user
            $this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
        }

        if ($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token')) {
            $this->reset_session();
            redirect(base_url('/signup/twitterlogin'));
        } else {
            $access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
            //echo $this->connection->http_code;
            //print_r($access_token);
            //die;


            if ($this->connection->http_code == 200) {
                $this->session->set_userdata('access_token', $access_token['oauth_token']);
                $this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
                $this->session->set_userdata('twitter_user_id', $access_token['user_id']);
                $this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);

                $this->session->unset_userdata('request_token');
                $this->session->unset_userdata('request_token_secret');

                $twitter_user_id = $access_token['user_id'];
                $sql_1 = "SELECT * FROM users WHERE twitter_id = '" . $twitter_user_id . "'  ";
                $query = $this->db->query($sql_1);
                if ($query->num_rows() == 0) {
                    redirect('/signup/twitteremail/', 'refresh');
                } else {
                    $row = $query->row();
                    $customer_email = $row->email;
                    $info = array($customer_email, "twitter");
                    $user_login_data = $this->CI_auth->process_login_social($info);
                    if ($this->CI_auth->check_logged() === FALSE) {
                        $this->session->set_flashdata('signup_message', 'Please check your login details.');
                        redirect('/signup/', 'refresh');
                    } else {
                        $user_type = '1';
                        $newdata = array(
                            'user_type' => $user_type,
                            'email' => $customer_email,
                            'logged_in' => TRUE,
                            'contact_name' => ''
                        );
                        $this->session->set_userdata($newdata);
                        redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
                        exit();
                    }
                }
            } else {
                // An error occured. Add your notification code here.
                redirect(base_url('/'));
            }
        }
    }

    public function twitteremail()
    {
        if (isset($_POST['act'])) {
            $customer_email = $_POST['twitter_email'];
            $sess_user_data = $this->session->all_userdata();
            $twitter_user_id = $sess_user_data['twitter_user_id'];
            $first_name = $sess_user_data['twitter_screen_name'];
            $customer_gender = "";
            $last_name = "";
            $sql_1 = "SELECT * FROM users WHERE email= '" . $customer_email . "' AND twitter_id = '" . $twitter_user_id . "'  ";
            $query = $this->db->query($sql_1);

            if ($query->num_rows() == 0) {
                $user_type = '1';
                $user_data = array('user_type' => $user_type, 'email' => $customer_email, 'gender' => $customer_gender, 'first_name' => $first_name, 'last_name' => $last_name, 'twitter_id' => $twitter_user_id);

                $this->db->insert('users', $user_data);
                $last_user_insert_id = $this->db->insert_id();

                /////////// INSERT NOTIFICATION //////////
                $notification_message = "We've noticed you haven't fully completed your profile details. Fill out your profile details, things you love and even save your credit card details for a faster booking experience.";
                $notification_data = array('customer_id' => $last_user_insert_id, 'message' => $notification_message, 'profile_message' => '1', 'added_date' => date("Y-m-d H:i:s"));
                $this->db->insert('user_notifications', $notification_data);
                /////////// INSERT NOTIFICATION //////////

                $info = array($customer_email, "twitter");
                $user_login_data = $this->CI_auth->process_login_social($info);

                if ($this->CI_auth->check_logged() === FALSE) {
                    $this->session->set_flashdata('signup_message', 'Please check your login details.');
                    redirect('/signup/', 'refresh');
                } else {
                    $newdata = array(
                        'user_type' => $user_type,
                        'email' => $customer_email,
                        'logged_in' => TRUE,
                        'contact_name' => ''
                    );
                    $this->session->set_userdata($newdata);
                    redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
                    exit();
                }
            } else {
                $info = array($customer_email, "twitter");
                $user_login_data = $this->CI_auth->process_login_social($info);
                if ($this->CI_auth->check_logged() === FALSE) {
                    $this->session->set_flashdata('signup_message', 'Please check your login details.');
                    redirect('/signup/', 'refresh');
                } else {
                    $user_type = '1';
                    $newdata = array(
                        'user_type' => $user_type,
                        'email' => $customer_email,
                        'logged_in' => TRUE,
                        'contact_name' => ''
                    );
                    $this->session->set_userdata($newdata);
                    redirect('/customeraccount/editcontactinfo?act=editdeatils', 'refresh');
                    exit();
                }
            }
        }

        $data['title'] = 'ONDI';
        $this->load->view('/signup/twitteremail', $data);
    }

    private function reset_session()
    {
        $this->session->unset_userdata('access_token');
        $this->session->unset_userdata('access_token_secret');
        $this->session->unset_userdata('request_token');
        $this->session->unset_userdata('request_token_secret');
        $this->session->unset_userdata('twitter_user_id');
        $this->session->unset_userdata('twitter_screen_name');
    }

}

?>