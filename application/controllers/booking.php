<?php
class Booking extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass', 'pdfcrowd', 'encryption'));
		$this->load->helper(array('form', 'url'));
		$this->load->database();
		$this->load->model(array('offers', 'users','CI_auth','CI_encrypt','Admin_Model'));
	}
	public function index()
	{		
		$sessiondata = $this->session->all_userdata();
		//print_r($sessiondata); 
		$session_id = $sessiondata['session_id'];
		if(isset($sessiondata['logged_user']))
		{
			$user_id = $sessiondata['logged_user'];
		}
		else
		{
			$user_id = 0;
		}

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */



		

		if(isset($_GET['oid']))
		{
			$oid = base64_decode($_GET['oid']);
			$this_offer_details = $this->offers->get_offer_details($oid);
			
			if($sessiondata['search_date'] == "")
			{
				$search_date = date("Y-m-d");
				$this->session->set_userdata('search_date', $search_date);
			}
			$search_date = $sessiondata['search_date'];
			
			if((bool)$this->config->item('test_mode'))
			{
				//echo "search_date--->".$search_date;
			}
			$search_day = strtolower(date("l", strtotime($search_date)));
			$this->db->select('from_hours, to_hours');
			$condition=array('offer_id' => $oid, 'available_day' => $search_day);
			$this->db->where($condition);
			$this->db->order_by('from_hours','asc');
			$hoursquery = $this->db->get('merchant_offers_available_time');
			//echo $this->db->last_query(); die;
			$hoursdata = $hoursquery->result();

			//print_r($hoursdata); die;

			//////////////  QTY with time added on 13.6.2014 ///////////////////////
			$explode_booking_date = explode("-",$search_date);
			$formatted_booking_date = $explode_booking_date[0]."-".$explode_booking_date[1]."-".$explode_booking_date[2];
			$day = strtolower(date("l", strtotime($formatted_booking_date)));
			$arrayhoursdata = array();
			$lastrecorddisplayed = "";
			$timearrindex = 0;
			foreach($hoursdata as $thehoursdata)
			{
				for($bt=$thehoursdata->from_hours; $bt<=$thehoursdata->to_hours; $bt=$bt+15)
				{  
					if($lastrecorddisplayed == $bt)
					{
						continue;
					}
					$lastrecorddisplayed = $bt;
					if((substr($bt, strlen($bt)-2, strlen($bt))==60))
					{
						$bt = $bt+40;
					}
					if(strlen($bt)==1) $bt = "000".$bt;
					else if(strlen($bt)==2) $bt = "00".$bt;
					else if(strlen($bt)==3) $bt = "0".$bt;

					if($sess_user_data['search_date']==date("Y-m-d"))
					{
						if($bt<$current_time){ continue;}
					}
					
					$db_max_pax = 0;
					$db_quantity = 0;

					$sql_check_day = "SELECT quantity, max_pax, from_hours, to_hours FROM merchant_offers_available_time WHERE offer_id = '".$oid."' AND available_day = '".$day."' AND from_hours <= '".$bt."'  AND to_hours >= '".$bt."'  ";

					$exe_check_day = $this->db->query($sql_check_day);
					$numrows_check_day = $exe_check_day->num_rows();
					if($numrows_check_day>0)
					{
						$data_check_day = $exe_check_day->result();
						$db_max_pax = $data_check_day[0]->max_pax;
						$db_quantity = $data_check_day[0]->quantity;
						$db_from_hours = $data_check_day[0]->from_hours;
						$db_to_hours = $data_check_day[0]->to_hours;
					}
					$sql_check_orders_for_the_day = "SELECT SUM(number_of_people) as seats_booked FROM orders WHERE offer_id = '".$oid."' AND booking_date = '".$formatted_booking_date."' AND booking_time >= '".$bt."' AND booking_time <= '".$bt."' AND order_status = '1' ";

					$exe_check_orders_for_the_day = $this->db->query($sql_check_orders_for_the_day);
					$data_check_orders_for_the_day = $exe_check_orders_for_the_day->result();
					$seats_booked = $data_check_orders_for_the_day[0]->seats_booked;
					
					if($seats_booked == '') $seats_booked = 0;
					$seats_remain = $db_quantity - $seats_booked;
					
					/*if($seats_remain>$db_max_pax)
					{
						$seats_alert = $db_max_pax;
					}
					else if($seats_remain<$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}
					else if($seats_remain==$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}*/
					
					//echo "<br/><br/>db_quantity-->".$db_quantity;
					//echo "seats_booked-->".$seats_booked;
					//echo "db_max_pax-->".$db_max_pax;
					//echo "bt-->".$bt;
					//echo $sql_check_orders_for_the_day;
					
					if($seats_remain>0)
					{
						$arrayhoursdata[$timearrindex]['bookingtime'] = $bt;
						$arrayhoursdata[$timearrindex]['seats_remain'] = $seats_remain;
						$timearrindex++;
					}
				}
			}
			//////////////  QTY with time added on 13.6.2014 ///////////////////////
			//print_r($arrayhoursdata);
			//die;



			

			
			$data = array('offerdata' => $this_offer_details, 'hoursdata' => $arrayhoursdata);	

			$this->form_validation->set_rules('number_of_people', 'number of people', 'trim|required');
			$this->form_validation->set_rules('booking_date', 'booking date', 'trim|required');
			$this->form_validation->set_rules('booking_time', 'booking time', 'trim|required');
			$this->form_validation->set_rules('total_price', 'total price', 'trim|required');
			
			if ($this->form_validation->run() == FALSE) {
				//$this->load->view('/booking/index', $data);
			}
			else{
				if($this->input->post('submit')){
					$number_of_people = $this->input->post('number_of_people');
					$booking_date = $this->input->post('booking_date');
					$booking_time = $this->input->post('booking_time');
					$promotion_code = $this->input->post('promotion_code');
					$offerprice = $this->input->post('offerprice');
					$discount = $this->input->post('discount');
					$oid = $this->input->post('oid');

					$discount = str_replace($discount, "$", "");;


					//print_r($_POST);

					//////////////// STARTS : CHECK MAX PAX AND QUANTITY ///////////////////
					$explode_booking_date = explode("/",$booking_date);
					$formatted_booking_date = $explode_booking_date[2]."-".$explode_booking_date[1]."-".$explode_booking_date[0];
					$day = strtolower(date("l", strtotime($formatted_booking_date)));


					$db_max_pax = 0;
					$db_quantity = 0;
					$sql_check_day = "SELECT quantity, max_pax, from_hours, to_hours FROM merchant_offers_available_time WHERE offer_id = '".$oid."' AND available_day = '".$day."' AND from_hours <= '".$booking_time."'  AND to_hours >= '".$booking_time."'  ";
					$exe_check_day = $this->db->query($sql_check_day);
					$numrows_check_day = $exe_check_day->num_rows();
					if($numrows_check_day>0)
					{
						$data_check_day = $exe_check_day->result();
						$db_max_pax = $data_check_day[0]->max_pax;
						$db_quantity = $data_check_day[0]->quantity;
						$db_from_hours = $data_check_day[0]->from_hours;
						$db_to_hours = $data_check_day[0]->to_hours;
					}

					$sql_check_orders_for_the_day = "SELECT SUM(number_of_people) as seats_booked FROM orders WHERE offer_id = '".$oid."' AND booking_date = '".$formatted_booking_date."' AND booking_time >= '".$db_from_hours."' AND booking_time <= '".$db_to_hours."' AND order_status = '1' ";
					$exe_check_orders_for_the_day = $this->db->query($sql_check_orders_for_the_day);
					$data_check_orders_for_the_day = $exe_check_orders_for_the_day->result();
					$seats_booked = $data_check_orders_for_the_day[0]->seats_booked;
					if($seats_booked == '') $seats_booked = 0;
					$seats_remain = $db_quantity - $seats_booked;

					/*echo "<br>seats_remain-->".$seats_remain;
					echo "<br>number_of_people-->".$number_of_people;
					echo "<br>db_max_pax-->".$db_max_pax;
					die;*/
					
					if($seats_remain>$db_max_pax)
					{
						$seats_alert = $db_max_pax;
					}
					else if($seats_remain<$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}
					else if($seats_remain==$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}

					if($seats_remain>=$number_of_people)
					{
						if($number_of_people<=$db_max_pax || $db_max_pax == 0)
						{
							$total_price = $number_of_people*$offerprice;
							
							$this->db->select('discount_amount, discount_type');
							$this->db->where('code', $promotion_code);
							$discountquery = $this->db->get('promo_code');
							$datadiscount = $discountquery->result();
							/*$discount_amount = "";
							if(sizeof($datadiscount)>0)
							{
								$discount_amount = $datadiscount[0]->discount_amount;
							}*/
							$discount_value = "";
							if(sizeof($datadiscount)>0)
							{
								$discount_amount = $datadiscount[0]->discount_amount;
								$discount_type = $datadiscount[0]->discount_type;
								//echo "<br/>discount_amount-->".$discount_amount;
								//echo "<br/>discount_type-->".$discount_type;
								//die;
								if($discount_type == '1')
								{
									$discount_value = $discount_amount;
								}
								else
								{
									$discount_value = ($total_price*$discount_amount)/100;
								}
							}
							$discount_amount = $discount_value;
							$total_price = $total_price - $discount_amount;
							
							$booking_date_arr = explode("/",$booking_date);
							$d='';
							$m='';
							$y='';
							if(isset($booking_date_arr[0]))
							{
								$d = $booking_date_arr[0];
							}
							if(isset($booking_date_arr[1]))
							{
								$m = $booking_date_arr[1];
							}
							if($booking_date_arr[2])
							{
								$y = $booking_date_arr[2];
							}
							if($d!='' && $m!='' && $y!='')
							{							
								$booking_date = $y.'-'.$m.'-'.$d;
							}
							// merchant commission
							$sql_merchant_commission = " SELECT a.merchant_commission FROM merchant_businessprofile a LEFT JOIN merchant_offers b ON a.user_id = b.merchant_id WHERE b.id = '".$oid."' ";
							$query_merchant_commission = $this->db->query($sql_merchant_commission);
							$data_merchant_commission = $query_merchant_commission->result();
							$merchant_commission = $data_merchant_commission[0]->merchant_commission;
							if($merchant_commission == '0.00')
							{
								$sql_website_settings = " SELECT merchant_commission FROM website_settings WHERE id = '1' ";
								$query_website_settings = $this->db->query($sql_website_settings);
								$data_website_settings = $query_website_settings->result();
								$merchant_commission = $data_website_settings[0]->merchant_commission;
							}
							$data = array(
							   'session_id' => $session_id,
							   'user_id' => $user_id,
							   'offer_id' => $oid,
							   'number_of_people' => $number_of_people,
							   'booking_date' => $booking_date,
							   'booking_time' => $booking_time,
							   'offerprice' => $offerprice,
							   'discount' => $discount,
							   'total_price' => $total_price,
							   'promo_code' => $promotion_code,
							   'merchant_commission' => $merchant_commission,
							   'order_date' => date("Y-m-d H:i:s"),
							   'order_status' => '2'
							);
							$this->db->insert('orders',$data);
							$order_insert_id = $this->db->insert_id();
							$this->session->set_userdata('order_id', $order_insert_id);

							if($user_id == '')
							{
								redirect('/booking/checkout', 'refresh');
							}
							else
							{
								redirect('/booking/paymentoptions', 'refresh');
							}
						}
						else
						{
							redirect('/booking/index/?oid='.base64_encode($oid).'&q=na&s='.$seats_alert, 'refresh');
						}
					}
					else
					{
						redirect('/booking/index/?oid='.base64_encode($oid).'&q=na&s='.$seats_alert, 'refresh');
					}
					//////////////// ENDS: CHECK MAX PAX AND QUANTITY ///////////////////
				}
			}

			/* USER ACCESS TYPE CHECK DATA START */
			$logged_user_access_type = $this->users->get_user_access_type();
			$data['logged_user_access_type'] = $logged_user_access_type;
			$data['logged_user_access_sections'] = array();
			if($logged_user_access_type=='3')
			{
				$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			}
			/* USER ACCESS TYPE CHECK DATA END */
			$this->load->view('/booking/index', $data);
		}
		else
		{
			redirect('/index/index', 'refresh');
		}
	}

	public function getdiscount()
	{
		$code = $this->input->post('code');
		$subtotal = $this->input->post('subtotal');
		//echo "subtotal---->".$subtotal;
		$this->db->select('discount_amount, discount_type');
		$this->db->where('code', $code);
		$discountquery = $this->db->get('promo_code');

		//echo $this->db->last_query();
		//exit();

		$datadiscount = $discountquery->result();
		//print_r($datadiscount);
		$discount_value = "";
		if(sizeof($datadiscount)>0)
		{
			$discount_amount = $datadiscount[0]->discount_amount;
			$discount_type = $datadiscount[0]->discount_type;

			if($discount_type == '1')
			{
				$discount_value = $discount_amount;
			}
			else
			{
				$discount_value = ($subtotal*$discount_amount)/100;
			}
		}
		//echo  round($discount_value);
		echo  number_format($discount_value, 2);
	}

	public function getdiscountpaymentoptions()
	{
		$code = $this->input->post('code');
		$subtotal = $this->input->post('subtotal');
		$this->db->select('discount_amount');
		$this->db->where('code', $code);
		$discountquery = $this->db->get('promo_code');
		$datadiscount = $discountquery->result();

		$discount_value = "";
		$return_str = "";
		if(sizeof($datadiscount)>0)
		{
			$discount_amount = $datadiscount[0]->discount_amount;
			$discount_type = $datadiscount[0]->discount_type;

			if($discount_type == '1')
			{
				$discount_value = $discount_amount;
			}
			else
			{
				$discount_value = ($subtotal*$discount_amount)/100;
			}

			$discount_amount = $discount_value;
			$return_str = '<div class="checked_comment"><h3>Discount: $'.number_format($discount_amount, 2).'</h3></div>';
			$sessiondata = $this->session->all_userdata();
			
			
			$order_id = $sessiondata['order_id'];

			$this->db->select('number_of_people, offerprice');
			$this->db->where('order_id', $order_id);
			$sqlorderdata = $this->db->get('orders');
			$orderdata = $sqlorderdata->result();
			$number_of_people = $orderdata[0]->number_of_people;
			$offerprice = $orderdata[0]->offerprice;
			$subtotal = $number_of_people*$offerprice;
				
			$total_price = $subtotal - $discount_amount;

			$data = array(
				   'discount' => $discount_amount,
				   'promo_code' => $code,
				   'total_price' => $total_price
			);
			$this->db->where('order_id', $order_id);
			$this->db->update('orders', $data);
		}
		
		echo $return_str;
	}

	public function checkout()
	{
		$sessiondata = $this->session->all_userdata();
		$data['order_id'] = $sessiondata['order_id'];
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		
		
		if($this->input->post('submit'))
		{
			$login_mode = $this->input->post('login_mode'); 
			$order_id = $this->input->post('order_id');
			if($login_mode == 'guest')
			{
				$email = $this->input->post('guest_email');
			}
			else
			{
				//print_r($_POST); exit();
				$email = $this->input->post('returning_email');
				$password = $this->input->post('returning_password');
				$rememberme = $this->input->post('rememberme');

				$info=array($email,$password, '1', '0');
				$user_login_data = $this->CI_auth->process_login($info);
				if($this->CI_auth->check_logged()===FALSE)
				{						
					redirect('/booking/checkout?login=failed', 'refresh');
				}
				else
				{	
					$session_user_data = $this->session->all_userdata();	
					$logged_user_data = $this->users->get_user_detail($session_user_data['logged_user']);
					$newdata = array(
					   'user_type'  => $logged_user_data[0]->user_type,
					   'email'     =>  $logged_user_data[0]->email,
					   'logged_in' => TRUE,
					   'contact_name' => $logged_user_data[0]->contact_name,
					   'first_name' => $logged_user_data[0]->first_name,
					   'last_name' => $logged_user_data[0]->last_name
						);
					$this->session->set_userdata($newdata);	

					if($rememberme == '1')
					{
						$exptime = time()+(3600*24*7);
						$this->load->helper('cookie');
						$cookie = array(
									   'name'   => 'ondi_login_email',
									   'value'  => $email,
									   'expire' => $exptime,
									   'domain' => $this->config->item('cookie_domain'),
									   'path'   => $this->config->item('cookie_path'),
									   'prefix' => ''
								   );
						set_cookie($cookie);
						$cookie = array(
									   'name'   => 'ondi_login_password',
									   'value'  => $password,
									   'expire' => $exptime,
									   'domain' => $this->config->item('cookie_domain'),
									   'path'   => $this->config->item('cookie_path'),
									   'prefix' => ''
								   );
						set_cookie($cookie);
					}
				}




			}
			
			$sessiondata = $this->session->all_userdata();
			if(isset($sessiondata['logged_user']))
			{
				$user_id = $sessiondata['logged_user'];
			}
			else
			{
				$user_id = 0;
			}
			$data = array(
				   'user_id' => $user_id,
				   'guest_email' => $email
			);
			$this->db->where('order_id', $order_id);
			$this->db->update('orders', $data);
			redirect('/booking/paymentoptions', 'refresh');
		}
		$this->load->view('/booking/checkout', $data);
	}

	public function paymentoptions()
	{


		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */




		$sessiondata = $this->session->all_userdata();
		$order_id = $sessiondata['order_id'];
		if(isset($sessiondata['logged_user']))
		{
			$user_id = $sessiondata['logged_user'];
			$logged_user_data = $this->users->get_user_detail($user_id);
			$sessiondata = (array)$logged_user_data[0];
		}
		else
		{
			$user_id = 0;
		}
		//echo $sessiondata['order_id'];
		$this->db->select('*');
		$this->db->where('order_id', $order_id);
		$ordersquery = $this->db->get('orders');
		$dataorders = $ordersquery->result();
		//print_r($dataorders);

		$this->db->select('id, cc_category');
		$cccategoryquery = $this->db->get('creditcard_categories');

		// check if the user has saved any card details
		$query_savedcc = "SELECT * FROM user_creditcard_refernces WHERE user_id = '".$user_id."' AND added_date > '".date("Y-m-d", strtotime('-30 days'))."' ";
		$exe_savedcc = $this->db->query($query_savedcc);
		$data_savedcc = $exe_savedcc->result();


		$data = array('dataorders' => $dataorders, 'usersessiondata' => $sessiondata, 'cccategoryquery' => $cccategoryquery->result(), 'data_savedcc' => $data_savedcc);	
		

		

		if($this->input->post('submit'))
		{
			
			
			$payment_method = $this->input->post('payment_method');
			$subscribe_newsletter = $this->input->post('subscribe_newsletter');
			$save_card = $this->input->post('save_card');
			$cc_category = $this->input->post('cc_category');
			$order_id = $this->input->post('order_id');
			
			if(isset($_POST['user_saved_cc']))
			{
				$saved_cc = $this->input->post('saved_cc');
			}
			else
			{
				$cc_num = $this->input->post('cc_num');
				$cc_owner = $this->input->post('cc_owner');
				$exp_month = $this->input->post('exp_month');
				$exp_year = $this->input->post('exp_year');
				$cvv = $this->input->post('cvv');
				$cc_address = $this->input->post('cc_address');
				$cc_city = $this->input->post('cc_city');
				$cc_country = $this->input->post('cc_country');
			}

			

			if($payment_method == '1')
			{
				// CREDIT CARD
				if(isset($_POST['user_saved_cc']))
				{
					$saved_cc = $this->input->post('saved_cc');
					$this->session->set_userdata('saved_cc', $this->encryption->encode($saved_cc));
					$use_saved_cc = '1';
				}
				else
				{
					$this->session->set_userdata('cc_num', $this->encryption->encode($cc_num));
					$this->session->set_userdata('cc_owner', $this->encryption->encode($cc_owner));
					$this->session->set_userdata('exp_month', $this->encryption->encode($exp_month));
					$this->session->set_userdata('exp_year', $this->encryption->encode($exp_year));
					$this->session->set_userdata('cvv', $this->encryption->encode($cvv));
					$this->session->set_userdata('cc_address', $this->encryption->encode($cc_address));
					$this->session->set_userdata('cc_city', $this->encryption->encode($cc_city));
					$this->session->set_userdata('cc_country', $this->encryption->encode($cc_country));
					$this->session->set_userdata('save_card', $this->encryption->encode($save_card));
					$this->session->set_userdata('cc_category', $this->encryption->encode($cc_category));
					$use_saved_cc = '0';
				}
				$this->session->set_userdata('payment_method', $this->encryption->encode($payment_method));
				$this->session->set_userdata('use_saved_cc', $this->encryption->encode($use_saved_cc));
			}
			else if($payment_method == '2')
			{
				// PAYPAL
			}
			else if($payment_method == '3')
			{
				// MASTER PASS
			}

			$data = array(
				   'payment_method' => $payment_method,
				   'subscribe_newsletter' => $subscribe_newsletter
			);
			$this->db->where('order_id', $order_id);
			$this->db->update('orders', $data);

			redirect('booking/review', 'refresh');
		}

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */




        // Payment Errors
        $data['error_messages'] = $this->session->userdata("error_messages");
        $this->session->set_userdata("error_messages", array());






        $this->load->view('/booking/paymentoptions', $data);
	}

	public function review()
	{
		$sessiondata = $this->session->all_userdata();

		//print_r($sessiondata);

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */



		$condition=array('a.order_id' => $sessiondata['order_id']);
		$this->db->select('a.*, b.offer_title, b.price, b.offer_description, c.business_name, c.website_address');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->where($condition);
		$orders_query = $this->db->get();
		//echo $this->db->last_query(); die;
		$dataresult = $orders_query->result();
		//print_r($dataresult);

		$data = array('dataorders' =>  $orders_query->result());
		
		if($this->input->post('submit'))
		{
			$order_id = $this->input->post('order_id');
			//echo '<img src="'.$this->config->item('base_url').'public/images/ajax-loader.gif" />';
			redirect('booking/confirmation', 'refresh');
		}

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('booking/review', $data);
	}

	public function confirmation()
	{
		echo '<div id="loading-image" style="width: 100%; height: 5500px; position: fixed; top: 0px; left: 0px; z-index: 999999999; background: #fff; text-align: center;"><div class="pleasewait" style="margin:220px auto 0px; color: 000; font-family: arial; font-size: 12px; width: 400px; text-align: center; padding:0px;"><div class="border" style="float:left; width:100%; background:#fff; border:1px solid #000; padding:50px 0px; line-height:25px;">One moment please while we process your payment. <br/>Please do not push \'cancel\' or \'reload\' during the process. <br/>This may take up to 1 minute
		<div class="loader" style="width:100%; margin:15px 0px 0px 0px;"><img src="'.$this->config->item('base_url').'public/images/ajax-loader.gif"  alt="Loading..." /></div></div></div></div>'; 
		
		

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}

		$sessiondata = $this->session->all_userdata();
		$payment_method = $this->encryption->decode($sessiondata['payment_method']);
		$order_id = $sessiondata['order_id'];
		$sql_orderdata = "SELECT * FROM orders WHERE order_id = '".$order_id."' ";
		$query_orderdata = $this->db->query($sql_orderdata);
		$orderdata = $query_orderdata->result();
		$total_price = $orderdata[0]->total_price;
		$guest_email = $orderdata[0]->	guest_email;

		
		/* USER ACCESS TYPE CHECK DATA END */
		if(isset($sessiondata['logged_user']))
		{
			$user_id = $sessiondata['logged_user'];
			$logged_user_data = $this->users->get_user_detail($user_id);
			$customer_email = $logged_user_data[0]->email;
			$customer_mobile = $logged_user_data[0]->mobile_number;
			$customer_first_name = $logged_user_data[0]->first_name;
			$customer_last_name = $logged_user_data[0]->last_name;
			$customer_full_name = $customer_first_name." ".$customer_last_name;
		}
		else
		{
			$user_id = 0;
			$customer_email = $guest_email;
			$customer_mobile = "";
			$customer_full_name = "";
		}

		
		
		
		
		

		if($payment_method == '1') // Pin Payment Credit Card
		{
			$use_saved_cc = $this->encryption->decode($sessiondata['use_saved_cc']); 
			if($use_saved_cc == '1')
			{
				$saved_cc = $this->encryption->decode($sessiondata['saved_cc']);  
			}
			else
			{
				$cc_num = $this->encryption->decode($sessiondata['cc_num']);  $cc_num = "4200000000000000";
				$cc_owner = $this->encryption->decode($sessiondata['cc_owner']);
				$exp_month = $this->encryption->decode($sessiondata['exp_month']);
				$exp_year = $this->encryption->decode($sessiondata['exp_year']);
				$cvv = $this->encryption->decode($sessiondata['cvv']);
				$cc_address = $this->encryption->decode($sessiondata['cc_address']);
				$cc_city = $this->encryption->decode($sessiondata['cc_city']);
				$cc_country = $this->encryption->decode($sessiondata['cc_country']);
				$save_card = $this->encryption->decode($sessiondata['save_card']);
				$cc_category = $this->encryption->decode($sessiondata['cc_category']);
			}
			
			//Authentication
			$test = true;
			$auth['u'] = (($test === true) ? 'LctjEveidUI8_GA9JO_r6w' : 'xxxxLivePrivateKeyxxxx'); //Private Key from PIN
			$auth['p'] = ''; //API calls use empty password
			//Set URL
			$url = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/charges';


			// successful response test card  4200000000000000 and  5520000000000000 
			//failyure response test card  4100000000000001    and    5560000000000001 

			//Fields to Post
			$post = array();
			$post['amount'] = ($total_price * 100);
			$post['currency'] = "AUD";
			$post['description'] = urlencode("Payment for ONDI");
			$post['email'] = $customer_email;
			$post['ip_address'] = $_SERVER['REMOTE_ADDR'];
			if($use_saved_cc == '1')
			{
				$query_saved_cc = "SELECT cust_token FROM user_creditcard_refernces WHERE id = '".$saved_cc."'";
				$exe_query_saved_cc = $this->db->query($query_saved_cc);
				$data_offer_saved_cc = $exe_query_saved_cc->result();

				$post['customer_token'] = $data_offer_saved_cc[0]->cust_token;
			}
			else
			{
				$post['card[number]'] = $cc_num;
				$post['card[expiry_month]'] = $exp_month;
				$post['card[expiry_year]'] = $exp_year;
				$post['card[cvc]'] = $cvv;
				$post['card[name]'] = $cc_owner;
				$post['card[address_line1]'] = $cc_address;
				//$post['card[address_line2]'] = "";
				$post['card[address_city]'] = $cc_city;
				//$post['card[address_postcode]'] = "";
				//$post['card[address_state]'] = "";
				$post['card[address_country]'] = $cc_country;
			}
			
			//print_r($post); die;

			// Create a curl handle
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
			curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post); //tell it what to post
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json
			// Execute
			$response = curl_exec($ch);
			// Check if any error occurred
			if(!curl_errno($ch)){$info = curl_getinfo($ch);}
			// Close handle
			curl_close($ch);

			//Decode JSON
			$response = json_decode($response, true);
			
			if((bool)$this->config->item('test_mode'))
			{
//				print_r($response); exit();
			}


            // failure?
            if((!empty($response['response']['success']) && (int)$response['response']['success'] == 1) == false)
            {
                $this->session->set_userdata("error_messages", $response['messages']);

                $data = array(
                    'order_status' => '0',
                    'response_message' => $response['error_description']
                );
                $this->db->where('order_id', $order_id);
                $this->db->update('orders', $data);

                echo '<script>window.location="'.$this->config->item('base_url').'booking/paymentoptions"</script>';
                exit();
            }
            else


            //Success?
			{
				//.... Successful


				$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$randomString = '';
				for ($i = 0; $i < 8; $i++) {
					$randomString .= $characters[rand(0, strlen($characters) - 1)];
				}
				
				$this->db->select('order_id');
				$this->db->where('booking_code', $randomString);
				$duplicatequery = $this->db->get('orders');
				$dataduplicate = $duplicatequery->result();
				$sizeofduplicate = sizeof($dataduplicate);
				while($sizeofduplicate != 0 ) {                      // loops till an unique value is found 
					
					$characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
					$randomString = '';
					for ($i = 0; $i < 7; $i++) {
						$randomString .= $characters[rand(0, strlen($characters) - 1)];
					}
					$this->db->select('order_id');
					$this->db->where('booking_code', $randomString);
					$duplicatequery = $this->db->get('orders');
					$dataduplicate = $duplicatequery->result();
					$sizeofduplicate = sizeof($dataduplicate);
				}
				
				
				////////////// PDF ///////////////
				$invoice_file_name = "public/uploads/invoice/invoice_".strtolower($randomString).".pdf";
				//echo $this->config->item('base_url').'booking/invoice/?bid='.$order_id;
				//$client = new Pdfcrowd("developerjk", "e9df5f25ade82724eced4dce8665773f"); // only 100 tokens are allowed
				$client = new Pdfcrowd("softwaretester47", "900172ceeb77150e7cd22a727cc9f861"); // only 100 tokens are allowed
				$out_file = fopen($invoice_file_name, "wb");
				//$client->setPageHeight('11.0in');
				$client->setPageWidth('10.0in');
				//$client->setHorizontalMargin('12mm');
				//$client->setVerticalMargin('10mm');


				$client->convertURI($this->config->item('base_url').'booking/invoice/?bid='.base64_encode($order_id)."&bc=".$randomString, $out_file);
				fclose($out_file);
				/////////////// PDF ///////////////


				$data = array(
				   'order_status' => '1',
				   'response_message' => $response['response']['status_message'],
				   'order_date' => date("Y-m-d H:i:s"),
				   'booking_code' => $randomString,
				   'invoice_file_name' => $invoice_file_name
				);
				$this->db->where('order_id', $order_id);
				$this->db->update('orders', $data);


				if($save_card == '1')
				{
					$test = true;
					$auth['u'] = (($test === true) ? 'LctjEveidUI8_GA9JO_r6w' : 'xxxxLivePrivateKeyxxxx'); //Private Key from PIN
					$auth['p'] = ''; //API calls use empty password
					$url_card = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/customers';

					$post_card = array();
					$post_card['email'] = $customer_email;
					$post_card['card[number]'] = $cc_num;
					$post_card['card[expiry_month]'] = $exp_month;
					$post_card['card[expiry_year]'] = $exp_year;
					$post_card['card[cvc]'] = $cvv;
					$post_card['card[name]'] = $cc_owner;
					$post_card['card[address_line1]'] = $cc_address;
					//$post_card['address_line2'] = ""
					$post_card['card[address_city]'] = $cc_city;
					//$post_card['address_postcode'] = "";
					//$post_card['address_state'] = "";
					$post_card['card[address_country]'] = $cc_country;
					//print_r($post); die;

					// Create a curl handle
					$ch = curl_init($url_card);
					curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
					curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
					curl_setopt($ch, CURLOPT_POSTFIELDS, $post_card); //tell it what to post
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json
					$response_card = curl_exec($ch);
					if(!curl_errno($ch)){$info = curl_getinfo($ch);}
					curl_close($ch);
					$response_card = json_decode($response_card, true);
					//print_r($response_card);
					//die;
					
					if(!empty($response_card['response']['token']) && $response_card['response']['token'] != "")
					{	
						$cust_token = $response_card['response']['token'];
						$cust_email = $response_card['response']['email'];

						
						$cc_token = $response_card['response']['card']['token'];
						$display_number = $response_card['response']['card']['display_number'];
						$expiry_month = $response_card['response']['card']['expiry_month'];
						$expiry_year = $response_card['response']['card']['expiry_year'];
						$scheme = $response_card['response']['card']['scheme'];
						$card_owner = $response_card['response']['card']['name'];
						$card_address_line1 = $response_card['response']['card']['address_line1'];
						$card_address_line2 = $response_card['response']['card']['address_line2'];
						$card_address_city = $response_card['response']['card']['address_city'];
						$card_address_postcode = $response_card['response']['card']['address_postcode'];
						$card_address_state = $response_card['response']['card']['address_state'];
						$card_address_country = $response_card['response']['card']['address_country'];


						$user_id=$this->CI_auth->logged_id();
						$sql_insert_cc = "INSERT INTO user_creditcard_refernces(user_id, ref_id, cc_category, display_number, expiry_month, expiry_year, scheme, card_owner, added_date, cust_token, cust_email, address_line1, address_line2, address_city, address_postcode, address_state, address_country) VALUES ('".$user_id."', '".$cc_token."', '".$cc_category."', '".$display_number."', '".$expiry_month."', '".$expiry_year."', '".$scheme."', '".$card_owner."', now(), '".$cust_token."', '".$cust_email."', '".$card_address_line1."', '".$card_address_line2."', '".$card_address_city."', '".$card_address_postcode."', '".$card_address_state."', '".$card_address_country."')";
						$query_insert_cc = $this->db->query($sql_insert_cc);
					}
				}



				/////////////////// SEND BOOKING NOTIFICATIONS ///////////

				/*$sql_merchant_data = "SELECT a.offer_id, a.number_of_people, a.booking_date, a.booking_time, b.merchant_id, b.offer_title, b.offer_description, c.business_phone, c.confirmation_methods, d.email, a.offerprice, c.business_name FROM orders a
									  LEFT JOIN merchant_offers b ON a.offer_id = b.id
									  LEFT JOIN merchant_businessprofile c ON b.merchant_id = c.user_id 
									  LEFT JOIN users d ON c.user_id  = d.user_id 
									  WHERE a.order_id = '".$order_id."' ";
				$query_merchant_data  = $this->db->query($sql_merchant_data);
				$merchant_data = $query_merchant_data->result();*/


				$condition=array('a.order_id' => $order_id);
				$this->db->select('a.*, b.merchant_id, b.offer_title, b.price, b.offer_description, c.business_name, c.business_phone, c.mobile_number, c.confirmation_methods, c.website_address, c.street_address, c.suburb, c.post_code, d.state, e.email, a.offerprice');
				$this->db->from('orders as a');
				$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
				$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
				$this->db->join('states as d', 'c.state = d.state_id', 'left');
				$this->db->join('users as e', 'c.user_id = e.user_id', 'left');
				$this->db->where($condition);
				$orders_query = $this->db->get();
				$merchant_data = $orders_query->result();
				
				$merchant_confirmation_methods = $merchant_data[0]->confirmation_methods;
				$merchant_id = $merchant_data[0]->merchant_id;
				$merchant_email = $merchant_data[0]->email;
				$offer_title = $merchant_data[0]->offer_title;
				$number_of_people = $merchant_data[0]->number_of_people;
				$booking_date = $merchant_data[0]->booking_date;
				$booking_time = $merchant_data[0]->booking_time;
				$business_phone = $merchant_data[0]->business_phone;
				$mobile_number = $merchant_data[0]->mobile_number;
				$offerprice = $merchant_data[0]->offerprice;
				$offer_description = $merchant_data[0]->offer_description;
				$business_name = $merchant_data[0]->business_name;

				
				$subject = "Order Confirmation";
				$message = "<table>
				<tr>
					<td>Order Id:</td>
					<td>".$order_id."</td>
				</tr>
				<tr>
					<td>Offer:</td>
					<td>".$offer_title."</td>
				</tr>
				<tr>
					<td>People:</td>
					<td>".$number_of_people."</td>
				</tr>
				<tr>
					<td>Booking Date:</td>
					<td>".date("M d, Y", strtotime($booking_date))."</td>
				</tr>
				<tr>
					<td>Booking Time:</td>
					<td>".date("g A", strtotime($booking_time))."</td>
				</tr>
				</table>";
				
				$customer_email_message = '<!DOCTYPE html>
<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ONDI FOOD</title>
</head>
<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
    
	<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>

	<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
     <img src="'.$this->config->item('base_url').'public/mailerimages/confirmation_head.jpg">
    </div>

    
    <div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
      <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
        <div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; ">'.$merchant_data[0]->offer_title.' <span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">for</span> $'.$merchant_data[0]->price.'</div>
        
        <div style=" width: 95%; height: auto; float: left; margin: 0; padding: 0 20px; border: 0;">
          <ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">at</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->business_name.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">located</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->street_address.' '.$merchant_data[0]->suburb.' '.$merchant_data[0]->state.' '.$merchant_data[0]->post_code.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">for</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->number_of_people.' people</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">on</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.date("l d/m/Y", strtotime($merchant_data[0]->booking_date)).'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">time</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->booking_time.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">totalling</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">$'.$merchant_data[0]->total_price.'</div>
            </li>
          </ul>
        </div>
        <div style=" width: 100%; height: auto; float: left; margin: 5px 0 15px; padding: 0; border: 0;">
          <div style=" width: 465px; height: auto; float: left; margin: 0 0 0 18px; padding: 12px 15px; border: 1px solid #c0bdbd;">
            <div style=" width: 200px; height: auto; float: left; font-size: 19px; color: #6b6766; line-height: 36px; margin: 0; padding: 0; border: 0;">your booking code is</div>
            <div style=" width: auto; height: auto; float: left; font-size: 28px; color: #2d2d2d; line-height: 36px; font-weight: bold; margin: 0; padding: 0; border: 0;">'.$merchant_data[0]->booking_code.'</div>
          </div>
          <div style=" float: left; font-size: 13px; color: #2d2d2d; zoom: 1; background-color: #fdef2f; margin: 0; padding: 15px 17px; border: 0;">No need to quote this booking code, <br style="">
            just show your identification upon arrival.</div>
        </div>
      </div>


	  <div style=" width: 100%; height: auto; float: left; margin: 10px 0; padding: 0; border: 0;">        
        <ul style=" float: left; width: 100%; list-style-type: none; margin: 10px 0; padding: 0px; border: 0;">        	
          <li style=" float: left; width: 100%; height: auto; background-color: #f57f2d; margin: 0 10px 0px 0; padding: 15px 0px; border: 0;">            
	            <p style=" float: left; color: #2d2d2d; font-size: 14px; font-style:italic; font-weight:bold;width: 90%; margin: 0; padding: 0 0 0 5px; border:0;">Don\'t forget to turn up 5mins prior to your booking and show your identification upon arrival.
If you are running late or need to change your booking please call the service provider directly.</p>
				<img src="'.$this->config->item('base_url').'public/mailerimages/clock.jpg" style="float:right; margin:0px 15px 0px 0px;" >
             </li>            
          
        </ul>
      </div>
    </div>

  </div>
</div>
</body>
</html>
';
				


				$merchant_email_message = '<!DOCTYPE html>
<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>ONDI FOOD</title>
</head>
<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
   
	<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>

   <div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
     <img src="'.$this->config->item('base_url').'public/mailerimages/confirmation_head.jpg" >     
    </div>


    <div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
      <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
        <div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; ">'.$merchant_data[0]->offer_title.'<span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">for</span> $'.$merchant_data[0]->price.'</div>
        
        <div style=" width: 95%; height: auto; float: left; margin: 0; padding: 0 20px; border: 0;">
          <ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">at</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->business_name.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">located</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->street_address.' '.$merchant_data[0]->suburb.' '.$merchant_data[0]->state.' '.$merchant_data[0]->post_code.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">for</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->number_of_people.' people</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">on</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.date("l d/m/Y", strtotime($merchant_data[0]->booking_date)).'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">time</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$merchant_data[0]->booking_time.'</div>
            </li>
            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="right">totalling</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">$'.$merchant_data[0]->total_price.'</div>
            </li>
          </ul>
        </div>
        <div style=" width: 100%; height: auto; float: left; margin: 5px 0 15px; padding: 0; border: 0;">
          <div style=" width: 465px; height: auto; float: left; margin: 0 0 0 18px; padding: 12px 15px; border: 1px solid #c0bdbd;">
            <div style=" width: 200px; height: auto; float: left; font-size: 19px; color: #6b6766; line-height: 36px; margin: 0; padding: 0; border: 0;">your booking code is</div>
            <div style=" width: auto; height: auto; float: left; font-size: 28px; color: #2d2d2d; line-height: 36px; font-weight: bold; margin: 0; padding: 0; border: 0;">'.$merchant_data[0]->booking_code.'</div>
          </div>
          <div style=" float: left; font-size: 13px; color: #2d2d2d; zoom: 1; background-color: #fdef2f; margin: 0; padding: 15px 17px; border: 0;">No need to quote this booking code, <br style="">
            just show your identification upon arrival.</div>
        </div>
      </div>
      

	  <div style=" width: 100%; height: auto; float: left; margin: 10px 0; padding: 0; border: 0;">
        
        <ul style=" float: left; width: 100%; list-style-type: none; margin: 10px 0; padding: 0px; border: 0;">
        	
            <li style=" float: left; width: 100%; height: auto; background-color: #f57f2d; margin: 0 10px 0px 0; padding: 15px 0px; border: 0;">
            
            <p style=" float: left; color: #2d2d2d; font-size: 14px; font-style:italic; font-weight:bold;width: 90%; margin: 0; padding: 0 0 0 5px; border:0;">Don\'t forget to turn up 5mins prior to your booking and show your identification upon arrival.
If you are running late or need to change your booking please call the service provider directly.</p>
 <img src="'.$this->config->item('base_url').'public/mailerimages/clock.jpg" style="float:right; margin:0px 15px 0px 0px;" >
             </li>
            
          
        </ul>
      </div>

      
    </div>
  </div>
</div>
</body>
</html>
';

				//echo $message;

				if((bool)$this->config->item('test_mode'))
				{
					//echo $customer_email_message;
					//echo "<br/>";
					//echo $merchant_email_message;
					//exit();
				}

				$customer_confirmation_methods = $this->users->get_notification_method($user_id, 1);
				
				if($customer_confirmation_methods != '')
				{
					$explode_customer_confirmation_methods = explode(",", $customer_confirmation_methods);
					if (in_array("email", $explode_customer_confirmation_methods)) {
						$to = $customer_email;
						$this->offers->send_mail($to, $subject, $customer_email_message);
					}
				}
				
				$this->offers->send_mail("bookings@ondi.com", $subject, $customer_email_message);
				

				$merchant_confirmation_methods = $this->users->get_notification_method($merchant_id, 2);
				
				if($merchant_confirmation_methods != '')
				{
					$explode_merchant_confirmation_methods = explode(",", $merchant_confirmation_methods);
					if (in_array("email", $explode_merchant_confirmation_methods)) {
						$to = $merchant_email;
						$this->offers->send_mail($to, $subject, $merchant_email_message);
					}

					
					if (in_array("sms", $explode_merchant_confirmation_methods) && $mobile_number != '') {
						
						
						$to_mobile = $mobile_number;
						
						$username = 'ondi';
						$password = 'ondi2014';
						$destination = $to_mobile; //Multiple numbers can be entered, separated by a comma
						$source    = 'ONDI';

						if($customer_full_name == "")
						{
							$bookingname = $customer_email;
						}
						else
						{
							$bookingname = $customer_full_name;
						}

						$text = 'NEW BOOKING: ('.$bookingname.', '.$number_of_people.' people, '.date("M d, Y", strtotime($booking_date)).', '.date("g A", strtotime($booking_time)).', '.$offer_title.', AUD '.$total_price.' charged to customer, '.$randomString.').';
						$ref = 'abc123';
							
						$content =  'username='.rawurlencode($username).
									'&password='.rawurlencode($password).
									'&to='.rawurlencode($destination).
									'&from='.rawurlencode($source).
									'&message='.rawurlencode($text).
									'&ref='.rawurlencode($ref);
						$smsbroadcast_response = $this->offers->sendSMS($content);
						$response_lines = explode("\n", $smsbroadcast_response);


						$sql_insert_sms = "INSERT INTO sms_notifications(to_mobile, message_text, sent_date) VALUES ('".$destination."', '".$text."', now())";
						$query_insert_sms = $this->db->query($sql_insert_sms);
						
					}
				}


				////////// INSERT NOTIFICATIONS ///////////////
				$notification_message = "You have a booking for (".$number_of_people." pax) ".$bookingname." at ".date("g A", strtotime($booking_time))." on ".date("D d F", strtotime($booking_date))." for ".$offer_title." at ".$business_name.". Please check your email for booking reference and contact details.";

				$sql_insert_notification = "INSERT INTO user_notifications(merchant_id, customer_id, message, added_date) VALUES ('".$merchant_id."', '".$user_id."', '".$notification_message."', now())";
				$query_insert_notification = $this->db->query($sql_insert_notification);

				////////// INSERT NOTIFICATIONS ///////////////

				
			}

		}







		$condition=array('a.order_id' => $sessiondata['order_id']);
		$this->db->select('a.*, b.offer_title, b.price, b.offer_description, c.business_name, c.website_address, c.street_address, c.suburb, c.post_code, d.state');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->join('states as d', 'c.state = d.state_id', 'left');
		$this->db->where($condition);
		$orders_query = $this->db->get();
		$data = array('dataorders' =>  $orders_query->result());



		$this->session->set_userdata('cc_num', "");
		$this->session->set_userdata('cc_owner', "");
		$this->session->set_userdata('exp_month', "");
		$this->session->set_userdata('exp_year', "");
		$this->session->set_userdata('cvv', "");
		$this->session->set_userdata('cc_address', "");
		$this->session->set_userdata('cc_city', "");
		$this->session->set_userdata('cc_country', "");
		$this->session->set_userdata('payment_method', "");
		$this->session->set_userdata('save_card', "");



		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$this->load->view('/booking/confirmation', $data);
	}

	public function ajaxtimings()
	{
		$return_str = '<p>at</p>';
		//print_r($_POST);
		if(isset($_POST['oid']) && isset($_POST['booking_date']))
		{
			$oid = $_POST['oid'];
			$booking_date = $_POST['booking_date'];
			$explode_date = explode("/", $booking_date);
			//print_r($explode_date);
			$booking_date = $explode_date[2]."-".$explode_date[1]."-".$explode_date[0];
			$search_day = strtolower(date("l", strtotime($booking_date)));
			$this->db->select('from_hours, to_hours');
			$condition=array('offer_id' => $oid, 'available_day' => $search_day);
			$this->db->where($condition);
			$this->db->order_by('from_hours','asc');
			$hoursquery = $this->db->get('merchant_offers_available_time');
			//echo $this->db->last_query(); die;
			$hoursdata = $hoursquery->result();
			//print_r($hoursdata);

			//////////////  QTY with time added on 13.6.2014 ///////////////////////
			$formatted_booking_date = $booking_date;
			$day = $search_day;
			$arrayhoursdata = array();
			$lastrecorddisplayed = "";
			$timearrindex = 0;
			foreach($hoursdata as $thehoursdata)
			{
				for($bt=$thehoursdata->from_hours; $bt<=$thehoursdata->to_hours; $bt=$bt+15)
				{  
					if($lastrecorddisplayed == $bt)
					{
						continue;
					}
					$lastrecorddisplayed = $bt;
					if((substr($bt, strlen($bt)-2, strlen($bt))==60))
					{
						$bt = $bt+40;
					}
					if(strlen($bt)==1) $bt = "000".$bt;
					else if(strlen($bt)==2) $bt = "00".$bt;
					else if(strlen($bt)==3) $bt = "0".$bt;

					if($sess_user_data['search_date']==date("Y-m-d"))
					{
						if($bt<$current_time){ continue;}
					}
					
					$db_max_pax = 0;
					$db_quantity = 0;
					$sql_check_day = "SELECT quantity, max_pax, from_hours, to_hours FROM merchant_offers_available_time WHERE offer_id = '".$oid."' AND available_day = '".$day."' AND from_hours <= '".$bt."'  AND to_hours >= '".$bt."'  ";
					$exe_check_day = $this->db->query($sql_check_day);
					$numrows_check_day = $exe_check_day->num_rows();
					if($numrows_check_day>0)
					{
						$data_check_day = $exe_check_day->result();
						$db_max_pax = $data_check_day[0]->max_pax;
						$db_quantity = $data_check_day[0]->quantity;
						$db_from_hours = $data_check_day[0]->from_hours;
						$db_to_hours = $data_check_day[0]->to_hours;
					}
					$sql_check_orders_for_the_day = "SELECT SUM(number_of_people) as seats_booked FROM orders WHERE offer_id = '".$oid."' AND booking_date = '".$formatted_booking_date."' AND booking_time >= '".$bt."' AND booking_time <= '".$bt."' AND order_status = '1' ";
					$exe_check_orders_for_the_day = $this->db->query($sql_check_orders_for_the_day);
					$data_check_orders_for_the_day = $exe_check_orders_for_the_day->result();
					$seats_booked = $data_check_orders_for_the_day[0]->seats_booked;
					
					if($seats_booked == '') $seats_booked = 0;
					$seats_remain = $db_quantity - $seats_booked;

					/*if($seats_remain>$db_max_pax)
					{
						$seats_alert = $db_max_pax;
					}
					else if($seats_remain<$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}
					else if($seats_remain==$db_max_pax)
					{
						$seats_alert = $seats_remain;
					}*/
					
					//echo "<br><br>db_quantity-->".$db_quantity;
					//echo "seats_booked-->".$seats_booked;
					//echo "bt-->".$bt;
					
					if($seats_remain>0)
					{
						$arrayhoursdata[$timearrindex]['bookingtime'] = $bt;
						$arrayhoursdata[$timearrindex]['seats_remain'] = $seats_remain;
						$timearrindex++;
					}
				}
			}
			//////////////  QTY with time added on 13.6.2014 ///////////////////////

			
			$current_time = date("Gi");
			if(sizeof($arrayhoursdata)>0)
			{
					$return_str .= '<div class="custom_select_ajax"><select name="booking_time" id="booking_time" class="revostyled" ><option value="" >Select Time</option>';
					$lastrecorddisplayed = "";
					foreach($arrayhoursdata as $thehoursdata)
					{
						$return_str .= '<option value="'.$thehoursdata['bookingtime'].'" >'.date("h:i A", strtotime($thehoursdata['bookingtime'])).' ('.$thehoursdata['seats_remain'].' Avail)</option>';
					}
					$return_str .= '</select></div>';
			} 
			else 
			{ 
				$return_str .= '<div class="custom_select_ajax"><select name="booking_time" id="booking_time" class="revostyled" ><option value="" >Not available</option></select></div>';
			} 
			echo $return_str;
		}
		else
		{
			$return_str .= '<div class="custom_select_ajax"><select name="booking_time" id="booking_time" class="revostyled" ><option value="" >Not available</option></select></div>';
		}	
		//echo $return_str;
	}

	public function invoice()
	{
		if(isset($_GET['bid']))
		{
			$bid = base64_decode($_GET['bid']);
		}
		$bc = $_GET['bc'];

		$condition=array('a.order_id' => $bid);
		$this->db->select('a.*, b.offer_title, b.price, b.offer_description, c.business_name, c.website_address, c.street_address, c.suburb, c.post_code, d.state');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->join('states as d', 'c.state = d.state_id', 'left');
		$this->db->where($condition);
		$orders_query = $this->db->get();
		$data = array('dataorders' =>  $orders_query->result(), 'bc' => $bc);
		$this->load->view('/customeraccount/invoice', $data);
	}

	/*public function checkmaxpax()
	{
		$oid = $_POST['oid'];
		$val = $_POST['val'];
		$booking_date = $_POST['booking_date'];
		$booking_time = $_POST['booking_time'];
		//echo "<br>oid--->".$oid;
		//echo "<br>val--->".$val;
		//echo "<br>booking_date--->".$booking_date;
		//echo "<br>booking_time--->".$booking_time;
		
		$explode_booking_date = explode("/",$booking_date);
		$formatted_booking_date = $explode_booking_date[2]."-".$explode_booking_date[1]."-".$explode_booking_date[0];
		$day = strtolower(date("l", strtotime($explode_booking_date)));


		$max_pax = 0;
		$sql_check_day = "SELECT quantity, max_pax FROM merchant_offers_available_time WHERE offer_id = '".$oid."' AND available_day = '".$day."' AND from_hours <= '".$booking_time_hours."'  AND to_hours >= '".$booking_time_hours."'  ";
		$exe_check_day = $this->db->query($sql_check_day);
		$numrows_check_day = $exe_check_day->num_rows();
		if($numrows_check_day>0)
		{
			$data_check_day = $exe_check_day->result();
			$max_pax = $data_check_day[0]->max_pax;
		}
		echo $max_pax;
	}*/
}
?>