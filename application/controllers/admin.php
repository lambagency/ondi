<?php 
class Admin extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass', 'email'));
		$this->load->helper(array('form', 'url'));
		$this->load->database();
		$this->load->model(array('users','CI_auth','CI_encrypt', 'offers', 'Admin_Model'));

		$this->load->library('ckeditor');
		$this->load->library('ckfinder');



		$this->ckeditor->basePath = base_url().'public/assets/ckeditor/';
		$this->ckeditor->config['toolbar'] = array(
						array( 'Source', '-', 'Bold', 'Italic', 'Underline', '-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','NumberedList','BulletedList' )
															);
		$this->ckeditor->config['language'] = 'en';
		$this->ckeditor->config['width'] = '600px';
		$this->ckeditor->config['height'] = '200px';            

		//Add Ckfinder to Ckeditor
		$this->ckfinder->SetupCKEditor($this->ckeditor,'../../public/assets/ckfinder/');

	}
	public function dashboard()
	{
		$session_user_data = $this->session->all_userdata();
		$mgmt_id = $this->CI_auth->admin_logged_id();		
		$admin_data = $this->Admin_Model->get_admin_detail($mgmt_id);				
			
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}			
		
		$data['title']='Admin Dashboard';
		$data['admin_data']=$admin_data;
		
		//if(isset($_REQUEST['dashboard_action']) || isset($_POST['dashboard_action']))
		//{
			//$keyword = trim($this->input->get_post('keyword'));

			/*
			echo '<pre>';
			print_r($_GET);
			echo '</pre>';
			*/
			
			$order_from_val = "";
			$order_to_val = "";
			$amount_from = "";
			$amount_to = "";
			
			$keyword = trim($_GET['keyword']);

			$order_from = trim($_GET['order_from']);
			if($order_from!="")
			{
				$order_from_array = explode("/",$order_from);
				$order_from_val = $order_from_array[2].'-'.$order_from_array[1].'-'.$order_from_array[0];
			}

			//echo '<br>';
			$order_to = trim($_GET['order_to']);
			
			if($order_to!="")
			{
				$order_to_array = explode("/",$order_to);
				$order_to_val = $order_to_array[2].'-'.$order_to_array[1].'-'.$order_to_array[0];
			}

			if(isset($_GET['amount_from']))
			{
				$amount_from = trim($_GET['amount_from']);
			}
			if(isset($_GET['amount_to']))
			{
				$amount_to = trim($_GET['amount_to']);
			}		
			
			$data['keyword'] = $keyword;
			$data['order_from'] = $order_from;
			$data['order_to'] = $order_to;	
			
			$data['amount_from'] = $amount_from;
			$data['amount_to'] = $amount_to;

			$data['dashboard_search_data'] = $this->Admin_Model->get_dashboard_search_data($keyword,$order_from_val,$order_to_val,$amount_from,$amount_to);
			

			$query_offer_pending = "SELECT a.id as offerid, a.offer_title, a.price, a.display_status, b.business_name FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id WHERE a.display_status = '4' ";
			$exe_query_offer_pending = $this->db->query($query_offer_pending);
			$data_exe_query_offer_pending = $exe_query_offer_pending->result();
			$num_pending_offers = sizeof($data_exe_query_offer_pending);
			$data['num_pending_offers'] = $num_pending_offers;

			$merchants_without_password = $this->Admin_Model->get_merchants_without_password();		
			$data['merchants_without_password'] = sizeof($merchants_without_password);
			

		//}
		
		if(isset($_GET['action2']))
		{
			$order_from_val = "";
			$order_to_val = "";
			$amount_from = "";
			$amount_to = "";
			
			$keyword = trim($_GET['keyword']);

			$order_from = trim($_GET['order_from']);
			if($order_from!="")
			{
				$order_from_array = explode("/",$order_from);
				$order_from_val = $order_from_array[2].'-'.$order_from_array[1].'-'.$order_from_array[0];
			}
			$order_to = trim($_GET['order_to']);
			if($order_to!="")
			{
				$order_to_array = explode("/",$order_to);
				$order_to_val = $order_to_array[2].'-'.$order_to_array[1].'-'.$order_to_array[0];
			}

			if(isset($_GET['amount_from']))
			{
				$amount_from = trim($_GET['amount_from']);
			}
			if(isset($_GET['amount_to']))
			{
				$amount_to = trim($_GET['amount_to']);
			}
			
			$dashboard_search_data = $this->Admin_Model->get_dashboard_search_data($keyword,$order_from_val,$order_to_val,$amount_from,$amount_to);
			$date = date("d-M");
			$filename = "order_data_{$date}.csv";
			header("Content-type:text/x-csv");
			header("Content-Disposition:attachment;filename=".$filename);
			$data_pk = array();
			$data_pk[0] = "Date/Time";
			$data_pk[1] = "Amount";
			$data_pk[2] = "Booking Code";
			$data_pk[3] = "Customer name";
			$data_pk[4] = "Name of offer";
			$data_pk[5] = "Status";
			
			print '"' . implode('","', $data_pk) . "\"\n";
			foreach($dashboard_search_data as $val)
			{

				$name="";
				$order_status="";
				$order_date="";
				if($val->first_name!="")
				{
					$name = stripslashes($val->first_name);
				}
				else if($val->contact_name!="")
				{
					$name = stripslashes($val->contact_name);
				}

				if($val->order_status=='0')
				{
					$order_status = 'Failed';

				}else if($val->order_status=='1'){

					$order_status = 'Successful';
				}
				

				if($val->order_date!='0000-00-00 00:00:00')
				{
					$order_date = date('D d/m/y',strtotime($val->order_date)); 
				}

				$total_price = stripslashes($val->total_price);

				$booking_code = stripslashes($val->booking_code); 

				$offer_title = stripslashes($val->offer_title);
				
				$data_pk[0] = $order_date;
				$data_pk[1] = $total_price;
				$data_pk[2] = $booking_code;
				$data_pk[3] = $name;				
				$data_pk[4] = $offer_title;
				$data_pk[5] = $order_status;			
				$i++;
				print '"' . implode('","', $data_pk) . "\"\n";
			}
			 exit;
		}


		if((bool)$this->config->item('test_mode'))
		{			
			/*
			echo '<pre>';
			print_r($session_user_data);
			echo '</pre>';
			echo '...............';
			exit();
			*/
		}

		

		$this->load->view('/admin/dashboard', $data);
	}
	public function globalsettings()
	{
		$session_user_data = $this->session->all_userdata();
		$mgmt_id = $this->CI_auth->admin_logged_id();		
		$admin_data = $this->Admin_Model->get_admin_detail($mgmt_id);
		
		////  Website global commission
		$query_commission = "SELECT merchant_commission FROM website_settings WHERE id = '1' ";
		$exe_query_commission = $this->db->query($query_commission);
		$data_query_commission = $exe_query_commission->result();
		$merchant_commission = $data_query_commission[0]->merchant_commission;
		$gacode = $data_query_commission[0]->gacode;


		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}	
		if($this->input->post('admin_dashboard_action'))
		{
			$this->form_validation->set_rules('admin_email', 'Email', 'trim|required|valid_email');
			if ($this->form_validation->run() == FALSE)
			{			
			}
			else
			{
				$admin_email = $this->input->post('admin_email');				
				$admin_password = $this->input->post('admin_password');	
				$merchant_commission = $this->input->post('merchant_commission');	
				$twitter_link = $this->input->post('twitter_link');	
				$facebook_link = $this->input->post('facebook_link');	
				$youtube_link = $this->input->post('youtube_link');		
				
				$searchbox_heading = $this->input->post('searchbox_heading');	
				$searchbox_content = $this->input->post('searchbox_content');
				
				$compare_heading = $this->input->post('compare_heading');	
				$compare_content = $this->input->post('compare_content');

				$book_heading = $this->input->post('book_heading');
				$book_content = $this->input->post('book_content');

				$offer_check_by_admin = $this->input->post('offer_check_by_admin');
				$gacode = $this->input->post('gacode');	
				
				if(!empty($admin_password))
				{
					$rand_salt = $this->CI_encrypt->genRndSalt();					
					$encrypt_pass = $this->CI_encrypt->encryptUserPwd($admin_password, $rand_salt);
					$admin_update_data = array('mgmt_email' => $admin_email, 'mgmt_pwd' => $encrypt_pass, 'salt'=>$rand_salt, 'twitter_link'=>$twitter_link, 'facebook_link'=>$facebook_link, 'youtube_link'=>$youtube_link, 'searchbox_heading'=>$searchbox_heading, 'searchbox_content'=>$searchbox_content, 'compare_heading'=>$compare_heading, 'compare_content'=>$compare_content, 'book_heading'=>$book_heading, 'book_content'=>$book_content, 'offer_check_by_admin'=>$offer_check_by_admin, 'gacode'=>$gacode);
				}
				else
				{
					$admin_update_data = array('mgmt_email' => $admin_email, 'twitter_link'=>$twitter_link, 'facebook_link'=>$facebook_link, 'youtube_link'=>$youtube_link, 'searchbox_heading'=>$searchbox_heading, 'searchbox_content'=>$searchbox_content, 'compare_heading'=>$compare_heading, 'compare_content'=>$compare_content, 'book_heading'=>$book_heading, 'book_content'=>$book_content, 'offer_check_by_admin'=>$offer_check_by_admin, 'gacode'=>$gacode);							
				}

				$this->db->where('mgmt_id', $mgmt_id);
				$this->db->update('ondi_admin', $admin_update_data);


				$sqlupdcommission = "UPDATE website_settings SET merchant_commission = '".$merchant_commission."' WHERE id = '1' ";		
				$execsqlupdcommission = $this->db->query($sqlupdcommission);


				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');		
				redirect('/admin/globalsettings', 'refresh');
			}
		}	
		$data['title']='Admin Dashboard';
		$data['admin_data']=$admin_data;
		$data['merchant_commission']=$merchant_commission;
		$this->load->view('/admin/globalsettings', $data);
	}
	public function login()
	{	
		
		$session_user_data = $this->session->all_userdata();
		if($this->input->post('admin_login_action'))
		{
			$this->form_validation->set_rules('admin_email', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('admin_password', 'Password', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{			

			}
			else
			{
				$admin_email = $this->input->post('admin_email');
				$admin_password = $this->input->post('admin_password');				
				$info=array($admin_email,$admin_password);					
				$user_login_data = $this->CI_auth->admin_process_login($info);
				if($this->CI_auth->admin_check_logged()===FALSE)
				{						
					$this->session->set_flashdata('admin_login_err_msg', 'Invalid admin details.');	
					redirect('/admin/login/', 'refresh');
				}
				else
				{
					$newdata = array('user_type' => 'admin');
					$this->session->set_userdata($newdata);
					$session_user_data = $this->session->all_userdata();
					redirect('/admin/dashboard', 'refresh');
				}
			}
		}
		$data['title']='Admin Login';
		$this->load->view('/admin/login', $data);
	}
	public function logout()
	{
		$this->session->unset_userdata('user_type');
		$this->session->sess_destroy();	
        redirect($this->config->base_url().'admin/login/', 'refresh');
	}
	public function index()
	{
		redirect($this->config->base_url().'admin/dashboard', 'refresh');
	}
	public function managepages()
	{
		$session_user_data = $this->session->all_userdata();		
		if(trim($session_user_data['user_type'])!='admin')
		{						
			redirect('/admin/login', 'refresh');			
		}		
		$content_pages = $this->Admin_Model->get_content_pages();		
		$data['title']='Admin Manage Pages';
		$data['content_pages']=$content_pages;
		$this->load->view('/admin/managepages', $data);
	}
	public function editcontentpage($id="")
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{						
			redirect('/admin/login', 'refresh');			
		}		
		$content_detail = $this->Admin_Model->get_content_pages_detail($id);	
		if($this->input->post('admin_content_action'))
		{
			$this->form_validation->set_rules('page_title', 'Page Title', 'trim|required');
			$this->form_validation->set_rules('page_desc', 'Page Desc', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{				
				$id = $this->input->post('page_id');
				$this->session->set_flashdata('admin_content_upd_err_msg', 'Page title can not be blank field.');				
				redirect('/admin/editcontentpage/'.$id, 'refresh');

			}
			else
			{
				$page_title = $this->input->post('page_title');
				$page_desc = $this->input->post('page_desc');
				$page_status = $this->input->post('page_status');
				$meta_title = $this->input->post('meta_title');
				$meta_desc = $this->input->post('meta_desc');
				$meta_keyword = $this->input->post('meta_keyword');
				$id = $this->input->post('page_id');
				$content_update_data = array('page_title' => $page_title, 'page_desc'=>$page_desc, 'meta_title'=>$meta_title, 'meta_desc'=>$meta_desc, 'meta_keyword'=>$meta_keyword,  'page_status'=>$page_status);				
				$this->db->where('page_id', $id);
				$this->db->update('content_page', $content_update_data);
				$this->session->set_flashdata('content_page_update_message', 'Page content has been updated successfully..');	
				redirect('/admin/managepages', 'refresh');
			}
		}
		$data['title']='Admin Edit Pages';
		$data['content_detail']=$content_detail;
		$this->load->view('/admin/editcontentpage', $data);
	}
	public function managefaqcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}		
		$faq_category_data = $this->Admin_Model->get_faq_category();
		$data['title']='Admin FAQ Category';
		$data['faq_category_data']=$faq_category_data;
		$this->load->view('/admin/managefaqcategory', $data);
	}
	public function addeditfaqcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$cat_id="";
		$faq_cat_detail="";
		if(isset($_GET['cat_id']) && $_GET['cat_id']!="")
		{
			$cat_id=$_GET['cat_id'];
			$faq_cat_detail = $this->Admin_Model->get_faq_category_detail($cat_id);
			
			/*
			echo '<pre>';
			print_r($faq_cat_detail);
			echo '</pre>';
			*/

			
			$data['faq_cat_detail']=$faq_cat_detail;
		}
		if($this->input->post('admin_faq_cat_action'))
		{
			$this->form_validation->set_rules('faq_category', 'FAQ Category', 'trim|required');
			if ($this->form_validation->run() == FALSE){
			}
			else
			{
				$faq_category = $this->input->post('faq_category');
				$cat_id = $this->input->post('cat_id');
				if(!empty($cat_id))
				{
					$count_category = $this->Admin_Model->count_faq_category($faq_category,$cat_id);
					if($count_category==0)
					{
						$faq_cat_data = array('category' => $faq_category);				
						$this->db->where('cat_id', $cat_id);
						$this->db->update('faq_category', $faq_cat_data);
						$this->session->set_flashdata('admin_message', 'FAQ Category has been updated successfully.');
						redirect('/admin/managefaqcategory', 'refresh');
					}
					else
					{
						$this->session->set_flashdata('admin_message', 'FAQ Category already exist.');
						redirect('/admin/managefaqcategory', 'refresh');
					}
				}
				else
				{
					$count_category = $this->Admin_Model->count_faq_category($faq_category,$cat_id);
					if($count_category==0)
					{
						$faq_cat_data = array('category' => $faq_category);
						$this->db->insert('faq_category', $faq_cat_data);
						$this->session->set_flashdata('admin_message', 'FAQ Category has been added successfully.');
						redirect('/admin/managefaqcategory', 'refresh');
					}
					else
					{
						$this->session->set_flashdata('admin_message', 'FAQ Category already exist.');
						redirect('/admin/managefaqcategory', 'refresh');
					}
				}				
			}
		}
		$data['title']='Admin Add/Edit FAQ Category';
		$this->load->view('/admin/addeditfaqcategory', $data);
	}
	public function managefaq()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$this->db->select('cat_id, category');		
		$faq_categories = $this->db->get('faq_category');
		//$condition=array('display_status' => '1');		
		//$this->db->where($condition);
		
		$cat_id="";
		if(isset($_GET['cat_id']) && $_GET['cat_id']!="")
		{
			$cat_id=$_GET['cat_id'];
		}
		$faq_data = $this->Admin_Model->get_faq_by_category($cat_id);
		
		//$data['faq_cat_data']=$faq_cat_data;
		//$faq_data = $this->Admin_Model->get_faq_by_category($cat_id);	
		//print_r($faq_data); die;

		$data['faq_data']=$faq_data;
		$data['title']='Admin Manage FAQS';
		$data['faq_categories']=$faq_categories->result();
		$data['cat_id']=$cat_id;
		$this->load->view('/admin/managefaq', $data);
	}
	public function addeditfaq()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('cat_id, category');
		$faq_categories = $this->db->get('faq_category');
		
		$cat_id = '';
		$faq_id = '';
		$faq_data = '';
		
		if(isset($_GET['cat_id']) && $_GET['cat_id']!="")
		{
			$cat_id=$_GET['cat_id'];
		}
		if(isset($_GET['faq_id']) && $_GET['faq_id']!="")
		{
			$faq_id=$_GET['faq_id'];			
			$faq_data = $this->Admin_Model->get_faq_detail($faq_id);
		}

		if($this->input->post('admin_faq_action'))
		{
			$this->form_validation->set_rules('faq_question', 'FAQ Question', 'trim|required');
			$this->form_validation->set_rules('faq_answer', 'FAQ Answer', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Add FAQS', 'cat_id'=>$cat_id);
				$this->load->view('/admin/addeditfaq', $data);
			}
			else
			{
				$faq_question = $this->input->post('faq_question');
				$faq_answer = $this->input->post('faq_answer');
				$display_order = $this->input->post('display_order');
				$display_status = $this->input->post('display_status');
				//$cat_id = $this->input->post('cat_id');
				$cat_id = $this->input->post('faq_category');				
				
				$faq_id = $this->input->post('faq_id');
				if(!empty($faq_id))
				{
					// update
					$faq_data = array('faq_question' => $faq_question, 'faq_answer' => $faq_answer, 'display_order' => $display_order, 'display_status'=>$display_status, 'faq_category'=>$cat_id);			
					$this->db->where('faq_id', $faq_id);
					$this->db->update('faq', $faq_data);
					$this->session->set_flashdata('admin_message', 'FAQ has been updated successfully.');
					redirect('/admin/managefaq/?cat_id='.$cat_id, 'refresh');
				}
				else
				{
					// insert
					$faq_data = array('faq_question' => $faq_question, 'faq_answer' => $faq_answer, 'display_order' => $display_order, 'display_status'=>$display_status, 'faq_category'=>$cat_id );
					$this->db->insert('faq', $faq_data);
					$this->session->set_flashdata('admin_message', 'FAQ has been added successfully.');
					redirect('/admin/managefaq/?cat_id='.$cat_id, 'refresh');
				}
			}
		}
		$data=array('title'=>'Add FAQS', 'cat_id'=>$cat_id, 'faq_data'=>$faq_data, 'faq_categories' => $faq_categories->result());
		$this->load->view('/admin/addeditfaq', $data);
	}
	public function delfaq($faq_id="")
	{
		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['cat_id']) && $_GET['cat_id']!="")
		{
			$cat_id=$_GET['cat_id'];
		}
		if(isset($_GET['faq_id']) && $_GET['faq_id']!="")
		{
			$faq_id=$_GET['faq_id'];
		}
		$this->Admin_Model->delete_faq($faq_id);	
		$this->session->set_flashdata('admin_message', 'FAQ has been deleted successfully.');						
		redirect('/admin/managefaq/?cat_id='.$cat_id, 'refresh');
	}
	public function delfaqcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['cat_id']) && $_GET['cat_id']!="")
		{
			$cat_id=$_GET['cat_id'];			
			$this->Admin_Model->delete_faq_category($cat_id);		
			$this->session->set_flashdata('admin_message', 'Category and their faqs has been deleted successfully.');						
			redirect('/admin/managefaqcategory/', 'refresh');
		}
	}
	public function assignpassword()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$merchants = $this->Admin_Model->get_merchants_without_password();		
		$data['title']='Merchant Password';
		$data['merchants']=$merchants;
		$this->load->view('/admin/assignpassword', $data);
	}
	public function editmerchantpaasword()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$user_id = '';
		if(isset($_GET['user_id']) && $_GET['user_id']!="")
		{
			$user_id=$_GET['user_id'];
		}
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('passwd', 'Password', 'trim|required');
			$this->form_validation->set_rules('cpasswd', 'Confirm Password', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Merchant Password', 'user_id'=>$user_id);
				$this->load->view('/admin/editmerchantpaasword', $data);
			}
			else
			{
				$user_id = $this->input->post('user_id');
				$passwd = $this->input->post('passwd');
				
				$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
				$random_key = '';
				for ($i = 0; $i < 6; $i++) {
					$random_key .= $characters[rand(0, strlen($characters) - 1)];
				}

				$encrypt_pass="";
				$rand_salt = $this->CI_encrypt->genRndSalt();					
				$encrypt_pass = $this->CI_encrypt->encryptUserPwd($passwd, $rand_salt);

				$data_update = array(
					   'password' => $encrypt_pass,
					   'salt' => $rand_salt
					);
				$this->db->where('user_id', $user_id);
				$this->db->update('users', $data_update);	
				
				$sql_1 =  "SELECT * FROM users WHERE user_id= '".$user_id."' ";
				$query = $this->db->query($sql_1);
				$row = $query->row();
				$user_email = $row->email;

				/*
				
				// SEND EMAIL TO USER
				$message = "Email: ".$user_email." \n Password: ".$passwd;
				$this->email->from('info@ondi.com', 'ONDI');
				$this->email->to($user_email);
				$this->email->subject('Login Details for ONDI');
				$this->email->message($message);
				$this->email->send();

			*/
			
			$message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
			 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear User, <br/>Your merchant account details are</div>
            </li>

            <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Email</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$user_email.'</div>
            </li>
			<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
              <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Password</div>
              <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$passwd.'</div>
            </li>
			</ul>';

			$html_mailer = '<!DOCTYPE html>
			<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
			<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<title>ONDI FOOD</title>
			</head>
			<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
			<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
			  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
				<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
				<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
				 <img src="'.$this->config->item('base_url').'public/mailerimages/merchant_account.jpg">
				</div>
				<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
				  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
					<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message.'</span></div>
				</div>
			  </div>
			</div>
			</body>
			</html>
			';





			$subject = "Login Details for ONDI";
			$this->offers->send_mail($user_email, $subject, $html_mailer);

				$this->session->set_flashdata('admin_message', 'Password has been updated successfully.');						
				redirect('/admin/assignpassword/', 'refresh');
			}
		}
		$data=array('title'=>'Merchants', 'user_id'=>$user_id, 'merchant_data'=>$merchant_data);
		$this->load->view('/admin/editmerchantpaasword', $data);
	}

	public function viewmerchantlist()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$merchant_search = "";
		if(isset($_POST['merchant_search']) || isset($_GET['merchant_search']))
		{			
			$merchant_search = trim($this->input->get_post('merchant_search'));
			$city_search = trim($this->input->get_post('city'));
			$data['merchant_search']=$merchant_search;
			$data['city_search']=$city_search;

			$city_details = $this->Admin_Model->get_cities($city_search);
			$data['city_name_search']= $city_details[0]->city_name;
		}	

		if(isset($_GET['action3']) && isset($_GET['id']) && isset($_GET['nw']))
		{
			$id = $_GET['id'];
			$nw = $_GET['nw'];
			$updQry = "UPDATE users SET user_status = '".$nw."' WHERE user_id = '".$id."' ";		
			$updResult = $this->db->query($updQry);
		}

		if(isset($_GET['action4']) && isset($_GET['id']) && isset($_GET['nw']))
		{
			$id = $_GET['id'];
			$nw = $_GET['nw'];
			$updQry = "UPDATE users SET offer_approval = '".$nw."' WHERE user_id = '".$id."' ";		
			$updResult = $this->db->query($updQry);
		}

		if(isset($_GET['action2']))
		{
			$merchants = $this->Admin_Model->get_merchants_with_password($merchant_search, $city_search);		
			$date = date("d-M");
			$filename = "merchants_data_{$date}.csv";
			header("Content-type:text/x-csv");
			header("Content-Disposition:attachment;filename=".$filename);
			$data_pk = array();
			$data_pk[0] = "Name";
			$data_pk[1] = "Position";
			$data_pk[2] = "Email";
			$data_pk[3] = "Phone";
			$data_pk[4] = "Business Name";
			$data_pk[5] = "Website";
			$data_pk[6] = "Address";
			$data_pk[7] = "Suburb";
			$data_pk[8] = "Post Code";
			$data_pk[9] = "City";
			$data_pk[10] = "State";
			$data_pk[11] = "Business Phone";
			$data_pk[12] = "Mobile Number";

			print '"' . implode('","', $data_pk) . "\"\n";
			foreach($merchants as $content_val)
			{
				//print_r($content_val); 
				$data_pk[0] = stripslashes($content_val->contact_name);
				if($content_val->position !='-1')
				{ 
					$data_pk[1] = stripslashes($this->users->get_position_value($content_val->position)); 
				} 
				else
				{ 
					$data_pk[1] = stripslashes($content_val->position_other); 
				} 
				$data_pk[2] = stripslashes($content_val->email);
				$data_pk[3] = stripslashes($content_val->phone);
				$data_pk[4] = stripslashes($content_val->business_name);
				$data_pk[5] = stripslashes($content_val->website_address);
				$data_pk[6] = stripslashes($content_val->street_address);
				$data_pk[7] = stripslashes($content_val->suburb);
				$data_pk[8] = stripslashes($content_val->post_code);

				$sql_city = "SELECT city_name FROM city WHERE id = '".$content_val->city."'";
				$query_city = $this->db->query($sql_city);
				$citydata = $query_city->result();
				
				$data_pk[9] = stripslashes($citydata[0]->city_name);
				
				$sql_state = "SELECT state FROM states WHERE state_id = '".$content_val->state."'";
				$query_state = $this->db->query($sql_state);
				$statedata = $query_state->result();
				$data_pk[10] = stripslashes($statedata[0]->state);
				
				$data_pk[11] = stripslashes($content_val->business_phone);
				$data_pk[12] = stripslashes($content_val->mobile_number);
				$i++;
				print '"' . implode('","', $data_pk) . "\"\n";
			}
			 exit;
		}

		

		$merchants = $this->Admin_Model->get_merchants_with_password($merchant_search, $city_search);		
		$data['title']='Merchants';
		$data['merchants']=$merchants;
		$this->load->view('/admin/viewmerchantlist', $data);
	}

	public function viewcustomerlist()
	{
		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$customer_search = "";
		$cols = "";
		$orderset = "";
		
		if(isset($_GET['cols']))
		{
			$cols = $_GET['cols'];		
		}
		if(isset($_GET['orderset']))
		{
			$orderset = $_GET['orderset'];		
		}
		if(isset($_REQUEST['customer_search']) || isset($_POST['customer_search']))
		{
			$customer_search = trim($this->input->get_post('customer_search'));
			$data['customer_search']=$customer_search;
		}

		if(isset($_GET['action3']) && isset($_GET['id']) && isset($_GET['nw']))
		{
			$id = $_GET['id'];
			$nw = $_GET['nw'];
			$updQry = "UPDATE users SET user_status = '".$nw."' WHERE user_id = '".$id."' ";		
			$updResult = $this->db->query($updQry);
		}

		if(isset($_GET['action2']))
		{
			$customers = $this->Admin_Model->get_customers($customer_search, $cols, $orderset);	
			$date = date("d-M");
			$filename = "customers_data_{$date}.csv";
			header("Content-type:text/x-csv");
			header("Content-Disposition:attachment;filename=".$filename);
			$data_pk = array();
			$data_pk[0] = "Name";
			$data_pk[1] = "Email";
			$data_pk[2] = "Mobile";
			$data_pk[3] = "Zip Code";
			$data_pk[4] = "Birthday";
			$data_pk[5] = "Gender";
			$data_pk[6] = "Things I Love";
			print '"' . implode('","', $data_pk) . "\"\n";
			foreach($customers as $content_val)
			{
				$data_pk[0] = stripslashes($content_val->first_name)." ".stripslashes($content_val->last_name);
				$data_pk[1] = stripslashes($content_val->email);
				$data_pk[2] = stripslashes($content_val->mobile_number);
				$data_pk[3] = stripslashes($content_val->postcode);
				if($content_val->birthday != '0000-00-00') 
				{
					$data_pk[4] = stripslashes($content_val->birthday);
				}
				else
				{
					$data_pk[4] = "N/A";
				}
				$data_pk[5] = stripslashes($content_val->gender);
				$interests = $content_val->interests;
				if($interests == "") $interests = '0';
				$sql_interests = "SELECT interest FROM user_interests WHERE id IN (".$interests.")";
				$query_interests = $this->db->query($sql_interests);
				$interestdata = $query_interests->result();
				$str_interests = "";
				foreach($interestdata as $interest)
				{
					$str_interests .= $interest->interest.", ";
				}
				if($str_interests != "")
				{
					$str_interests = substr($str_interests, 0, -2);
				}
				$data_pk[6] = stripslashes($str_interests);
				$i++;
				print '"' . implode('","', $data_pk) . "\"\n";
			}
			 exit;
		}

		
		//echo $customer_search;
		//exit();
		
		$customers = $this->Admin_Model->get_customers($customer_search, $cols, $orderset);		
		$data['title']='Customers';
		$data['customers']=$customers;
		$this->load->view('/admin/viewcustomerlist', $data);
	}

	public function loginasuser()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['user_id']) && $_GET['user_id']!="")
		{
			$user_id=base64_decode($_GET['user_id']);
			$logged_user_data = $this->users->get_user_detail($user_id);
			

			$user_type = $logged_user_data[0]->user_type;
			//$this->session->sess_destroy();	

			$newdata = array(
			   'user_type'  => $logged_user_data[0]->user_type,
			   'email'     =>  $logged_user_data[0]->email,
			   'logged_in' => TRUE,
			   'contact_name' => $logged_user_data[0]->contact_name,
			   'first_name' => $logged_user_data[0]->first_name,
			   'last_name' => $logged_user_data[0]->last_name
				);

			$this->session->set_userdata($newdata);		

			$this->session->set_userdata('logged_user', $user_id);

			$session_user_data = $this->session->all_userdata();	
			//print_r($session_user_data);
			if($user_type == '1')
			{
				redirect('/customeraccount/contactinfo', 'refresh');
			}
			else
			{
				redirect('/merchantaccount/contactinfo', 'refresh');
			}
			
		}
	}
	public function viewpaymentcategories()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$records = $this->Admin_Model->get_paymentcategories();		
		$data['title']='Customers';
		$data['records']=$records;
		$this->load->view('/admin/viewpaymentcategories', $data);
	}
	public function editpaymentcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('cc_category', 'cc_category', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Payment Credit Card Category', 'id'=>$id);
				$this->load->view('/admin/editpaymentcategory', $data);
			}
			else
			{
				$id = $this->input->post('id');
				$cc_category = $this->input->post('cc_category');
				$data_update = array(
					   'cc_category' => $cc_category
					);
				$this->db->where('id', $id);
				$this->db->update('creditcard_categories', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewpaymentcategories/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('creditcard_categories');		
		$condition=array('id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();

		$this->load->view('/admin/editpaymentcategory', $data);
	}
	public function addpaymentcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('cc_category', 'cc_category', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Payment Credit Card Category');
				$this->load->view('/admin/addpaymentcategory', $data);
			}
			else
			{
				$cc_category = $this->input->post('cc_category');
				$data_insert = array(
					   'cc_category' => $cc_category
					);
				$this->db->insert('creditcard_categories', $data_insert);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewpaymentcategories/', 'refresh');
			}
		}
		$data=array('title'=>'Payment Credit Card Category');
		$this->load->view('/admin/addpaymentcategory', $data);
	}

	public function viewbusinesstypes()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$records = $this->Admin_Model->get_businesstypes_admin();		
		$data['title']='Business Types';
		$data['records']=$records;
		$this->load->view('/admin/viewbusinesstypes', $data);
	}
	public function deletebusinesstypes()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		
		$this->Admin_Model->delete_business_type($id);	
		$this->session->set_flashdata('admin_message', 'Business type has been deleted successfully.');						
		redirect('/admin/viewbusinesstypes/','refresh');
	}
	public function editbusinesstype()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('type_name', 'type_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Business Type', 'id'=>$id);
				$this->load->view('/admin/editbusinesstype', $data);
			}
			else
			{
				$type_id = $this->input->post('type_id');
				$type_name = $this->input->post('type_name');
				$data_update = array(
					   'type_name' => $type_name
					);
				$this->db->where('type_id', $type_id);
				$this->db->update('business_types', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewbusinesstypes/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('business_types');		
		$condition=array('type_id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();

		$this->load->view('/admin/editbusinesstype', $data);
	}

	public function addbusinesstype()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('type_name', 'type_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Business Type');
				$this->load->view('/admin/addbusinesstype', $data);
			}
			else
			{
				$type_name = $this->input->post('type_name');
				$data_insert = array(
					   'type_name' => $type_name
					);
				$this->db->insert('business_types', $data_insert);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewbusinesstypes/', 'refresh');
			}
		}
		$data=array('title'=>'Business Type');
		$this->load->view('/admin/addbusinesstype', $data);
	}
	public function approvebusinesstype()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$type_id=$_GET['id'];
			$data_update = array(
				   'display_status' => '1'
				);
			$this->db->where('type_id', $type_id);
			$this->db->update('business_types', $data_update);	
			$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
			redirect('/admin/viewbusinesstypes/', 'refresh');
		}
		else
		{
			redirect('/admin/viewbusinesstypes/', 'refresh');
		}
	}
	public function viewserviceoffered()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');
		
		$business_type = "";
		if(isset($_GET['business_type']))
		{
			$business_type = $_GET['business_type'];
		}
		$records = $this->Admin_Model->get_serviceoffered_admin($business_type);	
		$data['title']='Services Offered';
		$data['records']=$records;
		$data['business_types']=$business_types->result();

		$this->load->view('/admin/viewserviceoffered', $data);
	}
	public function editserviceoffered()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('service', 'service', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Service Offered', 'id'=>$id);
				$this->load->view('/admin/editbusinesstype', $data);
			}
			else
			{
				$service_id = $this->input->post('service_id');
				$service = $this->input->post('service');
				$business_type = $this->input->post('business_type');
				$data_update = array(
					   'option_value' => $service,
					   'business_type' => $business_type,
					   'display_status' => '1'
					);
				$this->db->where('id', $service_id);
				$this->db->update('business_types_options', $data_update);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewserviceoffered/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('business_types_options');		
		$condition=array('id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();
		$data['business_types']=$business_types->result();
		$this->load->view('/admin/editserviceoffered', $data);
	}
	public function addserviceoffered()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('service', 'service', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Service Offered');
				$this->load->view('/admin/addserviceoffered', $data);
			}
			else
			{
				$service = $this->input->post('service');
				$business_type = $this->input->post('business_type');
				$data_update = array(
					   'option_value' => $service,
					   'business_type' => $business_type,
					   'display_status' => '1'
					);
				$this->db->insert('business_types_options', $data_update);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewserviceoffered/', 'refresh');
			}
		}
		$data['business_types']=$business_types->result();
		$this->load->view('/admin/addserviceoffered', $data);
	}
	public function viewperfectfor()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$business_types_query = $this->db->get('business_types');	
		
		$business_type = "";
		$business_type_id = "";
		if(isset($_GET['business_type']))
		{
			$business_type_id = $_GET['business_type'];
		}
		
		$records = $this->Admin_Model->get_perfectfor($business_type_id);		
		$data['title']='Perfect For';
		$data['records']=$records;
		
		$business_types_result = $business_types_query->result();
		$data['business_types']=$business_types_result;
		$data['business_type_id']=$business_type_id;
		
		$this->load->view('/admin/viewperfectfor', $data);
	}
	public function deleteperfectfor()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}		
		if(!empty($id))
		{
			$this->Admin_Model->delete_perfect_for($id);
			$this->session->set_flashdata('admin_message', 'Perfect for has been deleted successfully.');
		}								
		redirect('/admin/viewperfectfor/','refresh');
	}

	public function editperfectfor()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('pf_name', 'pf_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Perfect For', 'id'=>$id);
				$this->load->view('/admin/editperfectfor', $data);
			}
			else
			{
				$pf_id = $this->input->post('pf_id');
				$pf_name = $this->input->post('pf_name');
				$business_type = $this->input->post('business_type');
				$data_update = array(
					   'pf_name' => $pf_name,
					   'business_type' => $business_type
					);
				$this->db->where('pf_id', $pf_id);
				$this->db->update('perfect_for', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewperfectfor/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('perfect_for');		
		$condition=array('pf_id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();

		$business_types_result = $business_types->result();
		$data['business_types'] = $business_types_result;

		$this->load->view('/admin/editperfectfor', $data);
	}


	public function addperfectfor()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');		
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('pf_name', 'pf_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Perfect For');
				$this->load->view('/admin/addperfectfor', $data);
			}
			else
			{
				$pf_name = $this->input->post('pf_name');
				$business_type = $this->input->post('business_type');
				
				
				$data_insert = array(
					   'pf_name' => $pf_name,
					   'business_type' => $business_type
					);
				$this->db->insert('perfect_for', $data_insert);	
				//echo $this->db->last_query(); die;

				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewperfectfor/', 'refresh');
			}
		}
		$business_types_result = $business_types->result();
		
		$data=array('title'=>'Perfect For', 'business_types'=>$business_types_result);
		$this->load->view('/admin/addperfectfor', $data);
	}


	public function viewblogcategories()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$records = $this->Admin_Model->get_blogcategories();		
		$data['title']='Blog Categories';
		$data['records']=$records;
		$this->load->view('/admin/viewblogcategories', $data);
	}
	public function editblogcategory()
	{

		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('category', 'category', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Category', 'id'=>$id);
				$this->load->view('/admin/editblogcategory', $data);
			}
			else
			{
				$id = $this->input->post('id');
				$category = $this->input->post('category');
				$data_update = array(
					   'category' => $category
					);
				$this->db->where('id', $id);
				$this->db->update('blog_category', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewblogcategories/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('blog_category');		
		$condition=array('id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();

		$this->load->view('/admin/editblogcategory', $data);
	}
	public function deleteblogcategory()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}		
		$this->Admin_Model->delete_blog_category($id);	
		$this->session->set_flashdata('admin_message', 'Blog category has been deleted successfully.');						
		redirect('/admin/viewblogcategories/','refresh');

	}
	public function addblogcategory()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('category', 'category', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Category');
				$this->load->view('/admin/addblogcategory', $data);
			}
			else
			{
				$category = $this->input->post('category');
				$data_insert = array(
					   'category' => $category
					);
				$this->db->insert('blog_category', $data_insert);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewblogcategories/', 'refresh');
			}
		}
		$data=array('title'=>'Blog Category');
		$this->load->view('/admin/addblogcategory', $data);
	}
	public function viewblogposts()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('id, category');
		$blog_categories = $this->db->get('blog_category');
		
		$blog_category = "";
		if(isset($_GET['blog_category']))
		{
			$blog_category = $_GET['blog_category'];
		}
		$records = $this->Admin_Model->get_blogposts($blog_category);		
		$data['title']='Blog Posts';
		$data['records']=$records;
		$data['blog_categories']=$blog_categories->result();

		$this->load->view('/admin/viewblogposts', $data);
	}
	public function editblogpost()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('id, category');
		$blog_categories = $this->db->get('blog_category');

		$this->db->select('tag_id, tag_name');
		$tag_condition=array('delete_status' => '0'); // as per new update.
		$this->db->where($tag_condition);
		$blog_tags = $this->db->get('blog_tags');
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			//print_r($_POST); die;
			$this->form_validation->set_rules('post_title', 'post_title', 'trim|required');
			$this->form_validation->set_rules('short_desc', 'short_desc', 'trim|required');
			$this->form_validation->set_rules('post_desc', 'post_desc', 'trim|required');
			$this->form_validation->set_rules('post_category', 'post_category', 'trim|required');

			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Posts', 'id'=>$id);
				$this->load->view('/admin/editblogpost', $data);
			}
			else
			{
				$post_id = $this->input->post('post_id');
				$post_title = $this->input->post('post_title');
				$short_desc = $this->input->post('short_desc');
				$post_desc = $this->input->post('post_desc');
				$post_category = $this->input->post('post_category');
				//$array_post_tags = $this->input->post('post_tags');
				//$post_tags = implode(",", $array_post_tags);
				$post_tags = $this->input->post('input_post_tags');

				$old_post_image = $this->input->post('old_post_image');


				$config_image['upload_path'] = 'public/uploads/blog_images/';
				$config_image['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config_image);
				$this->upload->initialize($config_image);
				$this->upload->set_allowed_types('*');
				$post_image = $old_post_image;
				if($_FILES['post_image']['size'] != '')
				{
					if ($this->upload->do_upload('post_image')) {
						$data_post_image = $this->upload->data();
						$post_image = $config_image['upload_path'].$data_post_image['file_name'];
					}
				}
				else
				{
					$post_image = $old_post_image;
				}


				$data_update = array(
					   'post_title' => $post_title,
					   'short_desc' => $short_desc,
					   'post_desc' => $post_desc,
					   'post_category' => $post_category,
					   'post_tags' => $post_tags,
					   'post_image' => $post_image,
					   'added_date' => Date("Y-m-d H:i:s")
					);
				$this->db->where('post_id', $post_id);
				$this->db->update('blog_posts', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewblogposts/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('blog_posts');		
		$condition=array('post_id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();
		$data['blog_categories']=$blog_categories->result();
		$data['blog_tags']=$blog_tags->result();
		$this->load->view('/admin/editblogpost', $data);
	}
	public function deleteblogposts()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}		
		$this->Admin_Model->delete_post($id);	
		$this->session->set_flashdata('admin_message', 'Post and their comments has been deleted successfully.');						
		redirect('/admin/viewblogposts/','refresh');
	}
	public function addblogpost()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('id, category');
		$blog_categories = $this->db->get('blog_category');

		$this->db->select('tag_id, tag_name');
		$tag_condition=array('delete_status' => '0');
		$this->db->where($tag_condition);
		$blog_tags = $this->db->get('blog_tags');
		
		if($this->input->post('admin_action'))
		{
			//print_r($_POST); die;
			$this->form_validation->set_rules('post_title', 'post_title', 'trim|required');
			$this->form_validation->set_rules('short_desc', 'short_desc', 'trim|required');
			$this->form_validation->set_rules('post_desc', 'post_desc', 'trim|required');
			$this->form_validation->set_rules('post_category', 'post_category', 'trim|required');

			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Posts', 'id'=>$id);
				$this->load->view('/admin/editblogpost', $data);
			}
			else
			{
				$post_title = $this->input->post('post_title');
				$short_desc = $this->input->post('short_desc');
				$post_desc = $this->input->post('post_desc');
				$post_category = $this->input->post('post_category');
				//$array_post_tags = $this->input->post('post_tags');

				$post_tags = $this->input->post('input_post_tags');

				/*$post_tags = "";
				if(sizeof($array_post_tags)>0)
				{
					$post_tags = implode(",", $array_post_tags);
				}*/
				
				
				$config_image['upload_path'] = 'public/uploads/blog_images/';
				$config_image['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config_image);
				$this->upload->initialize($config_image);
				$this->upload->set_allowed_types('*');
				$post_image = "";
				if($_FILES['post_image']['size'] != '')
				{
					if ($this->upload->do_upload('post_image')) {
						$data_post_image = $this->upload->data();
						$post_image = $config_image['upload_path'].$data_post_image['file_name'];
					}
				}
				
				$data_update = array(
					   'post_title' => $post_title,
					   'short_desc' => $short_desc,
					   'post_desc' => $post_desc,
					   'post_category' => $post_category,
					   'post_tags' => $post_tags,
					   'post_image' => $post_image,
					   'added_date' => Date("Y-m-d H:i:s")
					);
				$this->db->insert('blog_posts', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewblogposts/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('blog_posts');		
		$query = $this->db->get();
		$data['record'] = $query->result();
		$data['blog_categories']=$blog_categories->result();
		$data['blog_tags']=$blog_tags->result();
		$this->load->view('/admin/addblogpost', $data);
	}
	public function viewblogtags()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$records = $this->Admin_Model->get_blogtags();		
		$data['title']='Blog Tags';
		$data['records']=$records;
		$this->load->view('/admin/viewblogtags', $data);
	}
	public function deleteblogtags()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}		
		if(!empty($id))
		{
			$this->Admin_Model->delete_blog_tag($id);
			$this->session->set_flashdata('admin_message', 'Blog tag has been deleted successfully.');

		}
								
		redirect('/admin/viewblogtags/','refresh');

	}
	public function editblogtag()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('tag_name', 'tag_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Tag', 'id'=>$id);
				$this->load->view('/admin/editblogtag', $data);
			}
			else
			{
				$tag_id = $this->input->post('tag_id');
				$tag_name = $this->input->post('tag_name');
				$data_update = array(
					   'tag_name' => $tag_name
					);
				$this->db->where('tag_id', $tag_id);
				$this->db->update('blog_tags', $data_update);	
				$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
				redirect('/admin/viewblogtags/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('blog_tags');		
		$condition=array('tag_id ' => $id);	
		$this->db->where($condition);
		$query = $this->db->get();
		$data['record'] = $query->result();

		$this->load->view('/admin/editblogtag', $data);
	}
	public function addblogtag()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('tag_name', 'tag_name', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Blog Tag');
				$this->load->view('/admin/addblogtag', $data);
			}
			else
			{
				$tag_name = $this->input->post('tag_name');
				$data_insert = array(
					   'tag_name' => $tag_name
					);
				$this->db->insert('blog_tags', $data_insert);	
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewblogtags/', 'refresh');
			}
		}
		$data=array('title'=>'Blog Tag');
		$this->load->view('/admin/addblogtag', $data);
	}
	public function viewblogcomments()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$records = $this->Admin_Model->get_blogcomments();		
		$data['title']='Blog Comments';
		$data['records']=$records;
		$this->load->view('/admin/viewblogcomments', $data);
	}
	public function deleteblogcomments()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['c_id']) && $_GET['c_id']!="")
		{
			$c_id=$_GET['c_id'];
		}		
		$this->Admin_Model->delete_post_comment($c_id);	
		$this->session->set_flashdata('admin_message', 'Post comment has been deleted successfully.');						
		redirect('/admin/viewblogcomments/','refresh');
	}	
	
	public function approveblogcomments()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['c_id']) && $_GET['c_id']!="")
		{
			$c_id=$_GET['c_id'];
		}		
		$this->Admin_Model->approve_post_comment($c_id);	
		$this->session->set_flashdata('admin_message', 'Post comment has been approved successfully.');						
		redirect('/admin/viewblogcomments/','refresh');
	}


	public function whatsondemand()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$city_id="";
		if(isset($_REQUEST['city_id']) && $_REQUEST['city_id']!="")
		{
			$city_id=$_REQUEST['city_id'];
		}
		else
		{
			//redirect('/admin/login/','refresh');
		}
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('placeholder_1', 'placeholder_1', 'trim|required');
			$this->form_validation->set_rules('placeholder_2', 'placeholder_2', 'trim|required');
			$this->form_validation->set_rules('placeholder_3', 'placeholder_3', 'trim|required');
			$this->form_validation->set_rules('placeholder_4', 'placeholder_4', 'trim|required');
			$this->form_validation->set_rules('placeholder_5', 'placeholder_5', 'trim|required');
			$this->form_validation->set_rules('placeholder_6', 'placeholder_6', 'trim|required');
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Whats on demand');
				$this->load->view('/admin/viewcity', $data);
			}
			else
			{
				$city_id = $this->input->post('city_id');
				$placeholder_1 = $this->input->post('placeholder_1');
				$placeholder_1 = $this->input->post('placeholder_1');
				$placeholder_2 = $this->input->post('placeholder_2');
				$placeholder_3 = $this->input->post('placeholder_3');
				$placeholder_4 = $this->input->post('placeholder_4');
				$placeholder_5 = $this->input->post('placeholder_5');
				$placeholder_6 = $this->input->post('placeholder_6');

				$data_update = array(
					    'placeholder_1' => $placeholder_1,
						'placeholder_2' => $placeholder_2,
						'placeholder_3' => $placeholder_3,
						'placeholder_4' => $placeholder_4,
						'placeholder_5' => $placeholder_5,
						'placeholder_6' => $placeholder_6
					);
				$this->db->where('id', $city_id);
				$this->db->update('city', $data_update);	
				
				//echo $this->db->last_query(); die;
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/whatsondemand/?city_id='.$city_id, 'refresh');
			}
		}
		
		$records = $this->Admin_Model->get_whatsondemand($city_id);	
		//$offers = $this->Admin_Model->get_alloffers();
		$offers = $this->Admin_Model->get_alloffers_cityspecific($city_id);	
		$data['title']='Whats on demand';
		$data['records']=$records;
		$data['offers']=$offers;
		$this->load->view('/admin/whatsondemand', $data);
	}
	public function addpromocode()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id="";
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id = $_GET['id'];
			$count_record = $this->Admin_Model->count_check_promo_id($id);
			if($count_record>0 && is_numeric($id))
			{
				$promo_code_data=$this->Admin_Model->get_promo_codes($id);
				$data['promo_code_data'] = $promo_code_data;

				/*
				echo '<pre>';
				print_r($promo_code_data);
				echo '</pre>';
				*/

				
			}else{

				$this->load->view('/admin/viewpromocode', $data);
				exit();
			}

		}
		
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('promo_code', 'Promo Code', 'trim|required');
			$this->form_validation->set_rules('discount_amount', 'Discount Amount', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				
				//$this->load->view('/admin/addpromocode', $data);
			}
			else
			{
				$id = $this->input->post('id');
				$promo_code = $this->input->post('promo_code');
				$discount_type = $this->input->post('discount_type');
				$discount_amount = $this->input->post('discount_amount');

				$count_promo_code = $this->Admin_Model->count_check_promo_code($promo_code,$id);				
				
				if($count_promo_code==0)
				{
					$promo_code_data = array('code' => $promo_code, 'discount_type' => $discount_type, 'discount_amount' => $discount_amount);
					if(!empty($id))
					{
						$this->db->where('id', $id);
						$this->db->update('promo_code', $promo_code_data);					
						$this->session->set_flashdata('admin_message', 'Promo code has been updated successfully.');

					}else{

						$this->db->insert('promo_code', $promo_code_data);
						$this->session->set_flashdata('admin_message', 'Promo code has been added successfully.');
					}					

				}else{

					$this->session->set_flashdata('admin_message', 'Promo code already exist.');
				}			
				redirect('/admin/viewpromocode', 'refresh');
			}

		}		
		$data['title']='Add Promo Code';
		$this->load->view('/admin/addpromocode', $data);
	}
	public function viewpromocode()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$promo_code_data = $this->Admin_Model->get_promo_codes();
		$data['title']='View Promo Code';
		$data['promo_code_data'] = $promo_code_data;
		$this->load->view('/admin/viewpromocode', $data);
	}
	public function delpromocode($id="")
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->Admin_Model->delete_promo_code($id);
		$this->session->set_flashdata('admin_message', 'Promo code has been deleted successfully.');
		$data['title']='View Promo Code';
		redirect('/admin/viewpromocode', 'refresh');
	}

	function genRndPromoCode($length = 8) {
		$digits = '';
		$chars = "A9BC1DE8FG3HJK6LMN2PQR5STU4VWXYZ123456789";		 
		 
		for($i = 0; $i < $length; $i++) {
		$x = mt_rand(0, strlen($chars) -1);
		$digits .= $chars{$x};
		}
		 
		return $digits;
	}

	public function generatepromocode()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$promo_code = $this->genRndPromoCode();

		$count_promo_code = $this->Admin_Model->count_check_promo_code($promo_code,"");
		if($count_promo_code==0)
		{

			$data['title']='Add Promo Code';
			$data['generated_promo_code'] = $promo_code;
			$this->load->view('/admin/addpromocode', $data);

		}else{

			redirect('/admin/generatepromocode', 'refresh');
		}

	}
	public function addcity()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id="";
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			
			$id = $_GET['id'];
			$count_record = $this->Admin_Model->count_check_by_id('city','id',$id);
			if($count_record>0 && is_numeric($id))
			{
				$city_fetch_data=$this->Admin_Model->get_cities($id);
				$data['city_fetch_data'] = $city_fetch_data;
				
			}else{

				$this->load->view('/admin/viewcity', $data);
				exit();
			}
			

		}else{

			$id = $this->input->post('id');
		}
		
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('city_name', 'City Name', 'trim|required');
			$this->form_validation->set_rules('display_order', 'Display Order', 'trim|numeric');
						
			if ($this->form_validation->run() == FALSE)
			{
				if(!empty($id))
				{
					redirect('/admin/addcity/?id='.$id, 'refresh');
				}				
			}
			else
			{
				
				$city_name = $this->input->post('city_name');
				$display_order = $this->input->post('display_order');				

				$default_city='';
				if($this->input->post('default_city')===FALSE)
				{
					$default_city = '';

				}else{

					$default_city = '1';
				}

				//echo $default_city;
				

				$count_city = $this->Admin_Model->count_check_city_name($city_name,$id);				
				
				if($count_city==0)
				{
					if($default_city=='1')
					{
						$default_city_data = array('default_city'=>'0');
						$this->db->update('city', $default_city_data);
					}
					
					
								
					
					$store = "public/uploads/background_images/";
					if (!file_exists($store))
					{
						mkdir($store,0777);
					}		
					$img="bg_image";
					if( $_FILES[$img]["size"] >1 && $_FILES[$img]["name"]!="")
					{	
						if (($_FILES[$img]['type']=="image/gif") || ($_FILES[$img]['type']=="image/pjpeg") || ($_FILES[$img]['type']=="image/jpeg") || ($_FILES[$img]['type']=="image/png"))
						{
							if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES[$img]["name"], $r)){}
							$t=0;
							$file= $this->Admin_Model->convert2link($r[1]).".".$r[2];
							while(file_exists($store.$file))
							{
								$file = $this->Admin_Model->convert2link($r[1]).'~'.$t++.'.'.$r[2];					
							}
							$tget=$store.$file;
							
							//$filesize = ceil(filesize($_FILES[$img]['tmp_name'])/1024);
							
							
							//if($filesize<=500)
							//{
									if(move_uploaded_file($_FILES[$img]['tmp_name'], $tget))
									{							
										$uploaddir=$store.$file;					

										
									}	
							//}
							//else
							//{
								//$oversize = "1";
							//}
							
							$bg_image_old = $this->input->post('bg_image_old');
							if(!empty($bg_image_old) && file_exists($bg_image_old))
							{
								@unlink($bg_image_old);
							}
								
						}
						else{

							$uploaddir = $this->input->post('bg_image_old');
						}

					}else{		

						$uploaddir = $this->input->post('bg_image_old');	
					}

					$city_data = array('city_name' => $city_name, 'default_city' => $default_city, 'display_order' => $display_order, 'bg_image' => $uploaddir);		
					
					if(!empty($id))
					{
						$this->db->where('id', $id);
						$this->db->update('city', $city_data);	
						//echo $this->db->last_query();	
						//exit();
						$this->session->set_flashdata('admin_message', 'City name has been updated successfully.');

					}else{

						$this->db->insert('city', $city_data);
						$this->session->set_flashdata('admin_message', 'City name has been added successfully.');
					}					

				}else{

					$this->session->set_flashdata('admin_message', 'City name already exist.');
				}			
				redirect('/admin/viewcity', 'refresh');
			}

		}		
		$data['title']='City';

		if(!empty($id))
		{
						

		}else{
			
		}

		$this->load->view('/admin/addcity', $data);
		
	}
	public function viewcity()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$city_fetch_data = $this->Admin_Model->get_cities();
		$data['title']='City';
		$data['city_fetch_data'] = $city_fetch_data;
		$this->load->view('/admin/viewcity', $data);
	}
	public function viewinterests()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$interest_fetch_data = $this->Admin_Model->get_interests();
		$data['title']='Things I Love';
		$data['interest_fetch_data'] = $interest_fetch_data;
		$this->load->view('/admin/viewinterests', $data);
	}
	public function addinterests()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id="";
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			
			$id = $_GET['id'];
			$count_record = $this->Admin_Model->count_check_by_id('user_interests','id',$id);
			if($count_record>0 && is_numeric($id))
			{
				$interest_fetch_data=$this->Admin_Model->get_interests($id);
				$data['interest_fetch_data'] = $interest_fetch_data;
				
			}else{

				$this->load->view('/admin/viewinterests', $data);
				exit();
			}
			

		}else{

			$id = $this->input->post('id');
		}
		
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('interest', 'things I love', 'trim|required');						
			if ($this->form_validation->run() == FALSE)
			{
				if(!empty($id))
				{
					redirect('/admin/addinterests/?id='.$id, 'refresh');
				}				
			}
			else
			{
				
				$interest = $this->input->post('interest');
				$count_interest = $this->Admin_Model->count_check_by_title('user_interests','interest',$interest,'id',$id);				
				
				if($count_interest==0)
				{
									
					
					$interest_data = array('interest' => $interest);					
					if(!empty($id))
					{
						$this->db->where('id', $id);
						$this->db->update('user_interests', $interest_data);	
						//echo $this->db->last_query();	
						//exit();
						$this->session->set_flashdata('admin_message', 'Things I love has been updated successfully.');

					}else{

						$this->db->insert('user_interests', $interest_data);
						$this->session->set_flashdata('admin_message', 'Things I love has been added successfully.');
					}					

				}else{

					$this->session->set_flashdata('admin_message', 'Things I love already exist.');
				}			
				redirect('/admin/viewinterests', 'refresh');
			}

		}		
		$data['title']='Things I love';

		
		$this->load->view('/admin/addinterests', $data);
		
	}
	public function delcity($id="")
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$bg_image_old = "";
		$city_fetch_data=$this->Admin_Model->get_cities($id);
		if($city_fetch_data[0]->bg_image != '') $bg_image_old = $city_fetch_data[0]->bg_image;
		if(!empty($bg_image_old) && file_exists($bg_image_old))
		{
			@unlink($bg_image_old);
		}
		
		$this->Admin_Model->delete_city($id);
		$this->session->set_flashdata('admin_message', 'City has been deleted successfully.');
		$data['title']='Manage City';
		redirect('/admin/viewcity', 'refresh');
	}	
	public function deleteinterests($id="")
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && isset($_GET['act']) && $_GET['act']=='delinterest')
		{
			$id = $_GET['id'];				
			//$this->Admin_Model->delete_record('user_interests','id',$id);
			$this->Admin_Model->delete_record_temp('user_interests','id',$id);
			$this->session->set_flashdata('admin_message', 'Things I love has been deleted successfully.');
			$data['title']='Manage Interest';
		}
		redirect('/admin/viewinterests', 'refresh');
	}
	public function merchantcontactinfo()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
				
		$user_id='';
		if(isset($_GET['user_id']) && $_GET['user_id']!="")
		{
			$user_id = base64_decode($_GET['user_id']);
		}	
		
		$data['title'] = 'Merchant Contact Information';	
		
		$this->db->select('user_id,email, position, position_other, phone, contact_name, terms_condition, merchant_steps_completed');
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		$data['user_data'] = $query->result();
		$this->load->view('/admin/merchantcontactinfo', $data);
		
	}
	public function customercontactinfo()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$user_id='';
		if(isset($_GET['user_id']) && $_GET['user_id']!="")
		{
			$user_id = base64_decode($_GET['user_id']);
		}
		
		$data['title'] = 'Customer Information';
		

		$this->db->select('user_id,first_name, last_name, email, mobile_number, postcode, birthday, customer_picture, gender, subscribe_newsletter, interests, confirmation_methods');
		$userquery = $this->db->get_where('users', array('user_id' => $user_id));

		$data = array('userdata' => $userquery->result());
		$this->load->view('/admin/customercontactinfo', $data);


	}
	public function lybinfo()
	{
		$session_user_data = $this->session->all_userdata();
		
		/*
		echo '<pre>';
		print_r($session_user_data);
		echo '</pre>';
		*/
		
		if($session_user_data['user_type']!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}		
		if($this->input->post('admin_dashboard_action'))
		{
			$box1 = $this->input->post('box1');				
			$box2 = $this->input->post('box2');	
			$box3 = $this->input->post('box3');	
			$box4 = $this->input->post('box4');	
			$box5 = $this->input->post('box5');		
			$box6 = $this->input->post('box6');	
			$box7 = $this->input->post('box7');	
				
			$admin_update_data = array('box1' => $box1, 'box2'=>$box2, 'box3'=>$box3, 'box4'=>$box4, 'box5'=>$box5, 'box6'=>$box6, 'box7'=>$box7);				
			$this->db->where('id', '1');
			$this->db->update('lyb_boxes', $admin_update_data);
			//echo $this->db->last_query();	
			//exit();
			//$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');		
			//redirect('/admin/lybinfo', 'refresh');
		}	
		$lybinfo_data = $this->Admin_Model->get_lybinfo();
		$data['title']='List your business information';
		$data['lybinfo_data']=$lybinfo_data;		
		$this->load->view('/admin/lybinfo', $data);
	}
	public function signupinfo()
	{
		$session_user_data = $this->session->all_userdata();
		if($session_user_data['user_type']!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}	
		
		if($this->input->post('admin_dashboard_action'))
		{
			$box1 = $this->input->post('box1');				
			$box2 = $this->input->post('box2');	
			$box3 = $this->input->post('box3');	
			$box4 = $this->input->post('box4');	
			$box5 = $this->input->post('box5');		
			$box6 = $this->input->post('box6');	
			$box7 = $this->input->post('box7');
			
			$store = "public/uploads/background_images/";
			if (!file_exists($store))
			{
				mkdir($store,0777);
			}		
			$img="bg_image";
			if( $_FILES[$img]["size"] >1 && $_FILES[$img]["name"]!="")
			{	
				if (($_FILES[$img]['type']=="image/gif") || ($_FILES[$img]['type']=="image/pjpeg") || ($_FILES[$img]['type']=="image/jpeg") || ($_FILES[$img]['type']=="image/png"))
				{
					if (!preg_match("/(.+)\.(.*?)\Z/", $_FILES[$img]["name"], $r)){}
					$t=0;
					$file= $this->Admin_Model->convert2link($r[1]).".".$r[2];
					while(file_exists($store.$file))
					{
						$file = $this->Admin_Model->convert2link($r[1]).'~'.$t++.'.'.$r[2];					
					}
					$tget=$store.$file;
					
					//$filesize = ceil(filesize($_FILES[$img]['tmp_name'])/1024);
					
					
					//if($filesize<=500)
					//{
							if(move_uploaded_file($_FILES[$img]['tmp_name'], $tget))
							{							
								$uploaddir=$store.$file;					

								
							}	
					//}
					//else
					//{
						//$oversize = "1";
					//}
					
					$bg_image_old = $this->input->post('bg_image_old');
					if(!empty($bg_image_old) && file_exists($bg_image_old))
					{
						@unlink($bg_image_old);
					}
						
				}
				else{

					$uploaddir = $this->input->post('bg_image_old');
				}

			}else{		

				$uploaddir = $this->input->post('bg_image_old');	
			}

			//echo $uploaddir;
			//exit();
				
			$admin_update_data = array('box1' => $box1, 'box2'=>$box2, 'box3'=>$box3, 'box4'=>$box4, 'box5'=>$box5, 'box6'=>$box6, 'box7'=>$box7, 'bg_image'=>$uploaddir);				
			$this->db->where('id', '1');
			$this->db->update('signup_boxes', $admin_update_data);
			
			//echo $this->db->last_query();	
			//exit();
			//$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');		
			//redirect('/admin/lybinfo', 'refresh');
		}	
		$lybinfo_data = $this->Admin_Model->get_signupinfo();
		$data['title']='Sign Up';
		$data['signupinfo_data']=$lybinfo_data;
		//print_r($data);
		//exit();
		$this->load->view('/admin/signupinfo',$data);
	}
	public function ajaxaddtag()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$return = '';
		if($this->input->get_post('newtag'))
		{
			$newtag = $this->input->get_post('newtag');	
			if($newtag != '')
			{
				$tagQry = "INSERT INTO blog_tags set tag_name = '".$newtag."'";		
				$tagResult = $this->db->query($tagQry);

				$newtag_id = $this->db->insert_id();
				$return =  $newtag_id."~~~".$newtag;
			}
		}
		echo $return;
	}
	public function ajaxremovetag()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$return = '';
		if($this->input->get_post('tagid'))
		{
			$tagid = $this->input->get_post('tagid');	
			if($tagid != '')
			{
				$tagQry = "DELETE FROM blog_tags WHERE tag_id = '".$tagid."'";		
				$tagResult = $this->db->query($tagQry);
				$return = $tagid;
			}
		}
		echo $return;
	}
	public function approveserviceoffered()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$type_id=$_GET['id'];
			$business_type=$_GET['business_type'];
			$data_update = array(
				   'display_status' => '1',
				   'business_type' => $business_type
				);
			$this->db->where('id', $type_id);
			$this->db->update('business_types_options', $data_update);	
			$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
			redirect('/admin/viewserviceoffered/', 'refresh');
		}
		else
		{
			redirect('/admin/viewserviceoffered/', 'refresh');
		}
	}

	public function approvemerchantcommission()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$id = '';
		if(isset($_GET['user_id']) && $_GET['user_id']!="")
		{
			$user_id=$_GET['user_id'];
			$merchant_commission=$_GET['merchant_commission'];
			$data_update = array(
				   'merchant_commission' => $merchant_commission
				);
			$this->db->where('user_id', $user_id);
			$this->db->update('merchant_businessprofile', $data_update);	
			$this->session->set_flashdata('admin_message', 'Details have been updated successfully.');						
			redirect('/admin/viewmerchantlist/', 'refresh');
		}
		else
		{
			redirect('/admin/viewmerchantlist/', 'refresh');
		}
	}

	public function viewsubscribers()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$subscriber_fetch_data = $this->Admin_Model->get_subscribers();

		if(isset($_GET['action2']))
		{
			$date = date("d-M");
			$filename = "subscribers_data_{$date}.csv";
			header("Content-type:text/x-csv");
			header("Content-Disposition:attachment;filename=".$filename);
			$data_pk = array();
			$data_pk[0] = "Email";
			$data_pk[1] = "Gender";
			$data_pk[2] = "Subscribed Date";

			print '"' . implode('","', $data_pk) . "\"\n";
			foreach($subscriber_fetch_data as $content_val)
			{
				$data_pk[0] = stripslashes($content_val->email);
				$data_pk[1] = stripslashes($content_val->gender);
				$data_pk[2] = stripslashes(date("d-m-Y", strtotime($content_val->added_date)));
				$i++;
				print '"' . implode('","', $data_pk) . "\"\n";
			}
			 exit;
		}

		
		$data['title']='Subscribers';
		$data['subscriber_fetch_data'] = $subscriber_fetch_data;
		$this->load->view('/admin/viewsubscribers', $data);
	}

	public function approveoffers()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		$successmessage = "";
		$offer_search = "";
		if(isset($_GET['offer_search']))
		{
			$offer_search = $_GET['offer_search'];
		}

		$ds = "";
		if(isset($_GET['ds']))
		{
			$ds = $_GET['ds'];
		}

		if(isset($_GET['offerid']) && isset($_GET['nds']))
		{
			$sqlUpdQry = "UPDATE merchant_offers set display_status = '".$_GET['nds']."' WHERE id = '".$_GET['offerid']."' ";		
			$sqlUpdResult = $this->db->query($sqlUpdQry);
			$successmessage = "Offer status successfully changed";
			$this->session->set_flashdata('admin_message', $successmessage);						
			redirect('/admin/approveoffers/', 'refresh');
		}

		
		$offers = $this->Admin_Model->get_all_offers($offer_search, $ds);	
		$data['title']='Approve Offers';
		$data['offers']=$offers;
		$this->load->view('/admin/approveoffers', $data);
	}

	public function addcityimage()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('id, city_name');
		$cities = $this->db->get('city');

		
		
		if($this->input->post('admin_action'))
		{
			//print_r($_POST); print_r($_FILES); die;
			$this->form_validation->set_rules('city', 'city', 'trim|required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$data=array('title'=>'Slideshow Image', 'id'=>$id);
				$this->load->view('/admin/editcityimage', $data);
			}
			else
			{
				$city = $this->input->post('city');

				$config_image['upload_path'] = 'public/uploads/slideshow_images/';
				$config_image['allowed_types'] = 'gif|jpg|png|jpeg';
				$this->load->library('upload', $config_image);
				$this->upload->initialize($config_image);
				$this->upload->set_allowed_types('*');
				$slideshow_image = "";
				if($_FILES['slideshow_image']['size'] != '')
				{
					if ($this->upload->do_upload('slideshow_image')) {
						$data_slideshow_image = $this->upload->data();
						$slideshow_image = $config_image['upload_path'].$data_slideshow_image['file_name'];
					}
				}
				
				$data_insert = array(
					   'city' => $city,
					   'slideshow_image' => $slideshow_image,
					   'added_date' => date("Y-m-d H:i:s")
					);
				$this->db->insert('slideshow_images', $data_insert);	
				$this->session->set_flashdata('admin_message', 'Details have been added successfully.');						
				redirect('/admin/viewslideshowimages/', 'refresh');
			}
		}
		$this->db->select("*");		
		$this->db->from('slideshow_images');		
		$query = $this->db->get();
		$data['record'] = $query->result();
		$data['cities']=$cities->result();
		$this->load->view('/admin/addcityimage', $data);
	}
	
	public function viewslideshowimages()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$this->db->select('id, city_name');
		$cities = $this->db->get('city');
		
		$slide_city = "";
		if(isset($_GET['slide_city']))
		{
			$slide_city = $_GET['slide_city'];
		}
		$records = $this->Admin_Model->get_cityimages($slide_city);		
		$data['title']='Slideshow Images';
		$data['records']=$records;
		$data['cities']=$cities->result();
		$this->load->view('/admin/viewslideshowimages', $data);
	}
	public function deleteslideshowimage()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		if(isset($_GET['id']) && $_GET['id']!="")
		{
			$id=$_GET['id'];
		}		
		$this->Admin_Model->delete_slideimage($id);	
		$this->session->set_flashdata('admin_message', 'Slide has been deleted successfully.');						
		redirect('/admin/viewslideshowimages/','refresh');
	}

	public function viewuserpositions()
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}

		$interest_fetch_data = $this->Admin_Model->get_viewuserpositions();
		$data['title']='Positions';
		$data['interest_fetch_data'] = $interest_fetch_data;
		$this->load->view('/admin/viewuserpositions', $data);
	}
	public function adduserposition()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		$position_id="";
		if(isset($_GET['position_id']) && $_GET['position_id']!="")
		{
			
			$position_id = $_GET['position_id'];
			$count_record = $this->Admin_Model->count_check_by_id('user_position','position_id',$position_id);
			if($count_record>0 && is_numeric($position_id))
			{
				$interest_fetch_data=$this->Admin_Model->get_viewuserpositions($position_id);
				$data['interest_fetch_data'] = $interest_fetch_data;
				
			}else{

				$this->load->view('/admin/viewuserpositions', $data);
				exit();
			}
			

		}else{

			$position_id = $this->input->post('position_id');
		}
		
		
		if($this->input->post('admin_action'))
		{
			$this->form_validation->set_rules('position_value', 'position', 'trim|required');						
			if ($this->form_validation->run() == FALSE)
			{
				if(!empty($position_id))
				{
					redirect('/admin/adduserposition/?position_id='.$position_id, 'refresh');
				}				
			}
			else
			{
				
				$position_value = $this->input->post('position_value');
				$position_id = $this->input->post('position_id');
				$count_interest = $this->Admin_Model->count_check_by_title_escape_del_status('user_position','position_value',$position_value,'position_id',$position_id);			
				
				if($count_interest==0)
				{
									
					
					$interest_data = array('position_value' => $position_value, 'display_status' => '1');					
					if(!empty($position_id))
					{
						$this->db->where('position_id', $position_id);
						$this->db->update('user_position', $interest_data);	
						//echo $this->db->last_query();	
						//exit();
						$this->session->set_flashdata('admin_message', 'Position has been updated successfully.');

					}else{

						$this->db->insert('user_position', $interest_data);
						$this->session->set_flashdata('admin_message', 'Position has been added successfully.');
					}					

				}else{

					$this->session->set_flashdata('admin_message', 'Position already exist.');
				}			
				redirect('/admin/viewuserpositions', 'refresh');
			}

		}		
		$data['title']='Positions';

		
		$this->load->view('/admin/adduserposition', $data);
		
	}
	public function deleteuserposition($id="")
	{
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		
		
		if(isset($_GET['id']) && isset($_GET['act']) && $_GET['act']=='delposition')
		{
			$id = $_GET['id'];				
			$this->Admin_Model->delete_record_temp('user_position','position_id',$id);
			$this->session->set_flashdata('admin_message', 'Position has been deleted successfully.');
			$data['title']='Manage Positions';
		}
		redirect('/admin/viewuserpositions', 'refresh');
	}

	public function addmerchant()
	{		
		$session_user_data = $this->session->all_userdata();
		if(trim($session_user_data['user_type'])!='admin')
		{	
			redirect('/admin/login', 'refresh');			
		}
		$sql_business_types = "SELECT * FROM business_types WHERE display_status = '1' AND delete_status = '0'  ORDER BY type_name ASC ";
		$business_types = $this->db->query($sql_business_types);
		$data = array('business_types' => $business_types->result());


	
		if($this->input->post('submit')){
				$business_name				= $this->input->post('business_name');
				$business_website_address	= $this->input->post('business_website_address');
				$business_street_address	= $this->input->post('business_street_address');
				$business_contact_name		= $this->input->post('business_contact_name');
				$business_email_address		= $this->input->post('business_email_address');
				$business_password			= $this->input->post('business_password');
				
				//print_r($_POST); die;
				
				
				$business_category = '';
				$arr_business_type = $this->input->post('business_type');	
				if(is_array($arr_business_type))
				{
					$business_category = implode(",", $arr_business_type);
				}

				$user_email_count = $this->users->users_count_by_email($business_email_address, "", "2,3");
				if($user_email_count>0)
				{										
					$this->session->set_flashdata('signup_message', 'Used email address is already registered! Please register with different email address!');
					$data['signup_message'] = 'Used email address is already registered! Please register with different email address!';
					$this->load->view('/admin/addmerchant', $data);
				}
				else
				{ 
					$encrypt_pass="";
					$rand_salt = $this->CI_encrypt->genRndSalt();					
					$encrypt_pass = $this->CI_encrypt->encryptUserPwd($business_password, $rand_salt);

					$user_type = '2';
					$user_data = array(
								'user_type'=>$user_type,
								'email' => $business_email_address, 
								'contact_name'=>$business_contact_name,
								'password' => $encrypt_pass, 
								'salt'=>$rand_salt
					);
					$this->db->insert('users', $user_data);	
					$newuserid = $this->db->insert_id();


					/////////// INSERT NOTIFICATION //////////
					$notification_message = "We've noticed that you haven't fully completed your profile details, this can help with users to really get all the information they need.";
					$notification_data = array('merchant_id'=>$newuserid, 'message' => $notification_message, 'profile_message' => '1', 'added_date'=>date("Y-m-d H:i:s"));
					$this->db->insert('user_notifications', $notification_data);	

					$notification_message1 = "Welcome to Ondi.com. Tell us a bit more about you so we can personalise your experience. Fill out your profile below.";
					$notification_data1 = array('merchant_id'=>$newuserid, 'message' => $notification_message1, 'profile_message' => '1', 'added_date'=>date("Y-m-d H:i:s"));
					$this->db->insert('user_notifications', $notification_data1);	
					/////////// INSERT NOTIFICATION //////////


					// INSERT ADDITIONAL CATEGORIES IF ANY /////
					$num_category = $this->input->post('num_category');
					for($nso = 1; $nso<=$num_category; $nso++)
					{
						$additional_business_category = $this->input->post('additional_cat_'.$nso);
						if(trim($additional_business_category) != '')
						{
							$sql_count_duplicate = "SELECT type_id FROM business_types WHERE (type_name = '".$additional_business_category."' AND display_status = '1') OR ( type_name = '".$additional_business_category."' AND merchant_id = '".$newuserid."' )";
							$query_count_duplicate = $this->db->query($sql_count_duplicate);
							$count_duplicate = $query_count_duplicate->num_rows();
							if($count_duplicate==0)
							{						
								$insert_add_cat = array('type_name'=>$additional_business_category, 'display_status' => '0', 'merchant_id' => $newuserid);
								$this->db->insert('business_types', $insert_add_cat);	
								$newcategoryid = $this->db->insert_id();
								if($business_category != '')
								{
									$business_category = $business_category.",".$newcategoryid;
								}
								else
								{
									$business_category = $newcategoryid;
								}
							}
							else
							{
								$data_check_cat = $query_count_duplicate->result();
								$type_id = $data_check_cat[0]->type_id;

								if($business_category != '')
								{
									$explode_business_category = explode(",", $business_category);
									if (!in_array($type_id, $explode_business_category)) {
										$business_category = $business_category.",".$type_id;
									}
								}
								else
								{
									$business_category = $type_id;
								}
							}
						}
					}
					// INSERT ADDITIONAL CATEGORIES IF ANY /////

					$query = $this->db->get_where('merchant_businessprofile', array('user_id' => $newuserid)); 
					$count = $query->num_rows();  
					if($count === 0)
					{ 
						$data_insert = array(
									   'user_id' => $newuserid,
									   'business_name' => $business_name,
									   'website_address' => $business_website_address,
									   'street_address' => $business_street_address,
									   'business_type' => $business_category
									);
						
						$this->db->insert('merchant_businessprofile', $data_insert);
					}
					redirect('/admin/viewmerchantlist/', 'refresh');
				}
			}
		
		$this->load->view('/admin/addmerchant', $data);
	}
}
?>