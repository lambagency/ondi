<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/welcome
	 *	- or -  
	 * 		http://example.com/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass', 'email'));
		$this->load->helper(array('form', 'url'));
		$this->load->database();
		$this->load->model(array('users','CI_auth','CI_encrypt', 'offers', 'faq', 'ondiblog', 'Admin_Model' ));
	}
	public function index()
	{	
		$data['title'] = 'ONDI FOOD';
		
		$sql_posts = "SELECT * FROM blog_posts WHERE 1 = 1 ";
		if(isset($_GET['month']))
		{
			$sql_posts .= " AND DATE_FORMAT(added_date, '%M %Y') = '".$_GET['month']."' ";
		}
		if(isset($_POST['blogsearch']))
		{
			$sql_posts .= " AND ( post_title LIKE '%".$_POST['blogsearch']."%' OR post_desc LIKE '%".$_POST['blogsearch']."%'  OR short_desc LIKE '%".$_POST['blogsearch']."%' )";
		}

		$sql_posts .= " ORDER BY added_date DESC ";
		$exe_posts = $this->db->query($sql_posts);
		$data_posts = $exe_posts->result();
		$data['data_posts'] = $data_posts;

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$this->load->view('blog/blog', $data);
	}

	public function detail()
	{	
		$post_id = $_GET['p'];
		if($post_id == "")
		{
			redirect('/blog/', 'refresh');	
		}
		
		$sessiondata = $this->session->all_userdata();
		$session_id = $sessiondata['session_id'];
		if(isset($sessiondata['logged_user']))
		{
			$user_id = $sessiondata['logged_user'];
		}
		else
		{
			$user_id = 0;
		}
		$name = "";
		$email = "";
		$parent_comment_id = "";
		$display_status = '0';
		//print_r($_POST); die;

		if(isset($_POST['actcomment']))
		{
			if(isset($_POST['uname']))
			{
				$name = $_POST['uname'];
			}
			if(isset($_POST['email']))
			{
				$email = $_POST['email'];
			}
			if(isset($_POST['parent_comment_id']))
			{
				$parent_comment_id = $_POST['parent_comment_id'];
			}
			$comment = $_POST['comment'];
			
			$insQry = "INSERT INTO blog_post_comments SET post_id = '".$post_id."', user_id = '".$user_id."', name = '".$name."', email = '".$email."', post_comment = '".$comment."', added_date = now(), parent_comment_id = '".$parent_comment_id."', display_status = '".$display_status."'";		
			$insResult = $this->db->query($insQry);

			redirect('/blog/detail/?p='.$post_id.'&act=suc', 'refresh');
		}


		$data['title'] = 'ONDI FOOD';
		$sql_posts = "SELECT * FROM blog_posts WHERE post_id = '".$post_id."' ";
		$exe_posts = $this->db->query($sql_posts);
		$data_posts = $exe_posts->result();
		$data['data_post'] = $data_posts;
		
		// GET CATEGORIES
		$post_cats = $data_posts[0]->post_category;
		if($post_cats == '') $post_cats = '0';

		$sql_cats = "SELECT * FROM blog_category WHERE id IN (".$post_cats.") ";
		$exe_cats = $this->db->query($sql_cats);
		$data_cats = $exe_cats->result();
		
		$str_cats = "";
		foreach($data_cats as $thecat)
		{
			$str_cats = $str_cats.$thecat->category.", ";
		}
		if($str_cats != "") $str_cats = substr($str_cats, 0, -2);
		$data['categories'] = $str_cats;

		
		// GET TAGS
		$post_tags = $data_posts[0]->post_tags;
		if($post_tags == '') $post_tags = '0';

		$sql_tags = "SELECT * FROM blog_tags WHERE tag_id IN (".$post_tags.") and delete_status = '0' ";
		$exe_tags = $this->db->query($sql_tags);
		$data_tags = $exe_tags->result();
		
		$str_tags = "";
		foreach($data_tags as $thetag)
		{
			$str_tags = $str_tags.$thetag->tag_name.", ";
		}
		if($str_tags != "") $str_tags = substr($str_tags, 0, -2);
		$data['tags'] = $str_tags;
		
		// GET COMMENTS
		$sql_comments = "SELECT * FROM blog_post_comments WHERE post_id = '".$post_id."' AND parent_comment_id = '0' AND display_status  = '1' ";
		$exe_comments = $this->db->query($sql_comments);
		$data_comments = $exe_comments->result();
		
		$data['parent_comments'] = $data_comments;
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$this->load->view('blog/detail', $data);
	}
	
}

/* End of file blog.php */
/* Location: ./application/controllers/blog.php */