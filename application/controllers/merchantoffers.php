<?php
class Merchantoffers extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass'));	
		$this->load->helper(array('form', 'url'));
		//$this->load->library('encrypt');
		$this->load->database();
		$this->load->model(array('users','CI_auth','CI_encrypt', 'offers','Admin_Model'));
	}	
	public function index()
	{		
		//	
	}
	public function detail()
	{		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		
		if(isset($_GET['m']))
		{
			$m = base64_decode($_GET['m']);
			$this_merchant_details = $this->offers->get_offer_merchant_details($m);
			$merchant_offers = $this->offers->get_merchant_offers($m);
			$merchant_reviews = $this->offers->get_merchant_reviews($m);
			$str_business_types = '';
			if($this_merchant_details->business_type != '')
			{
				$business_type_array = explode(",", $this_merchant_details->business_type);
				$this->db->select('type_name');
				$this->db->from('business_types');
				$this->db->where_in('type_id', $business_type_array);
				$business_typequery = $this->db->get();
				$data_business_type = $business_typequery->result();
				
				foreach($data_business_type as $business_type){ $str_business_types .= $business_type->type_name.", "; }
				if($str_business_types != '') $str_business_types = substr($str_business_types, 0, -2);
			}
			
			// CITY BACKGROUND IMAGE
			$mer_city = $this_merchant_details->city;
			$query_city_bg = "SELECT bg_image FROM city WHERE id = '".$mer_city."' ";
			$exe_query_city_bg = $this->db->query($query_city_bg);
			$data_query_city_bg = $exe_query_city_bg->result();
			$bg_image = $data_query_city_bg[0]->bg_image;
			

			

			$data = array('merchantdata' => $this_merchant_details, 'merchant_offers' => $merchant_offers, 'merchant_reviews' => $merchant_reviews, 'str_business_types' => $str_business_types, 'bg_image' => $bg_image);
			
			/* USER ACCESS TYPE CHECK DATA START */
			$logged_user_access_type = $this->users->get_user_access_type();
			$data['logged_user_access_type'] = $logged_user_access_type;
			$data['logged_user_access_sections'] = array();
			if($logged_user_access_type=='3')
			{
				$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			}
			/* USER ACCESS TYPE CHECK DATA END */

			$this->load->view('/merchantoffers/detail', $data);
		}
		else
		{
			redirect('/index/index', 'refresh');
		}
	}
	public function preview($offer_id="")
	{		
			
			$m="12"; // logged in user id.			
			$m = $this->CI_auth->logged_id();						
			if($offer_id!="" && $m!="")
			{
				$this_merchant_details = $this->offers->get_offer_merchant_details($m);

				$sess_user_data = $this->session->all_userdata();
				if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
				{					
					$table_mo = 'merchant_offers_temp'; // merchant offers				

				}else{
					
					$table_mo = 'merchant_offers'; // merchant offers
				}

				$merchant_offers = $this->offers->get_merchant_offers_preview($m,$offer_id,$table_mo);


				$merchant_reviews = $this->offers->get_merchant_reviews($m);
				$data = array('merchantdata' => $this_merchant_details, 'merchant_offers' => $merchant_offers, 'merchant_reviews' => $merchant_reviews);	
				
				/* USER ACCESS TYPE CHECK DATA START */
				$logged_user_access_type = $this->users->get_user_access_type();
				$data['logged_user_access_type'] = $logged_user_access_type;
				$data['logged_user_access_sections'] = array();
				if($logged_user_access_type=='3')
				{
					$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
				}
				/* USER ACCESS TYPE CHECK DATA END */
				
				$this->load->view('/merchantoffers/preview', $data);	
				
			}else{
				
				redirect('/index/index', 'refresh');
			}
		
			
	}
}
?>