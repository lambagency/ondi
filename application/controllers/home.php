<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/welcome
	 *	- or -  
	 * 		http://example.com/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass', 'email'));
		$this->load->helper(array('form', 'url'));
		//$this->load->library('encrypt');
		$this->load->database();
		//$this->load->model(array('users'));		
		$this->load->model(array('users','CI_auth','CI_encrypt', 'offers', 'faq', 'Admin_Model' ));
	}
	public function index()
	{
		
		$this->db->select('slide_image');
		$this->db->order_by("display_order", "asc");
		$query = $this->db->get('slideshow');
		$slides = $query->result();

		// USED IN SEARCH FORM
		$this->db->select('pg_id, pg_name');
		$this->db->order_by("pg_id", "desc");
		$price_guide_list_options = $this->db->get('price_guide');

		$this->db->select('pf_id, pf_name');
		//$condition=array('pf_name != ' => 'N/A');
		$condition=array('delete_status' => '0', 'pf_name != ' => 'N/A'); //  Note: Zero is for active, 1 is for deleted
		$this->db->where($condition);
		$perfect_for_list_options = $this->db->get('perfect_for');
		

		//$this->db->select('type_id, type_name');
		//$business_types = $this->db->get('business_types');
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$this->db->order_by("type_name", "asc");
		$business_types = $this->db->get('business_types');

		$this->db->select('service_id, service');
		$services = $this->db->get('services');

		$sql_beauty = "SELECT id, option_value FROM business_types_options WHERE business_type = '3' ORDER BY option_value ASC ";
		$query_beauty = $this->db->query($sql_beauty);

		$sql_fitness = "SELECT id, option_value FROM business_types_options WHERE business_type = '4' ORDER BY option_value ASC ";
		$query_fitness = $this->db->query($sql_fitness);

		$sql_hair = "SELECT id, option_value FROM business_types_options WHERE business_type = '5' ORDER BY option_value ASC ";
		$query_hair = $this->db->query($sql_hair);

		$sql_makeup = "SELECT id, option_value FROM business_types_options WHERE business_type = '6' ORDER BY option_value ASC ";
		$query_makeup = $this->db->query($sql_makeup);

		$sql_massage = "SELECT id, option_value FROM business_types_options WHERE business_type = '7' ORDER BY option_value ASC ";
		$query_massage = $this->db->query($sql_massage);

		$sql_mens = "SELECT id, option_value FROM business_types_options WHERE business_type = '8' ORDER BY option_value ASC ";
		$query_mens = $this->db->query($sql_mens);

		$sql_nails = "SELECT id, option_value FROM business_types_options WHERE business_type = '9' ORDER BY option_value ASC ";
		$query_nails = $this->db->query($sql_nails);

		$sql_tanning = "SELECT id, option_value FROM business_types_options WHERE business_type = '10' ORDER BY option_value ASC ";
		$query_tanning = $this->db->query($sql_tanning);

		$sql_teeth = "SELECT id, option_value FROM business_types_options WHERE business_type = '11' ORDER BY option_value ASC ";
		$query_teeth = $this->db->query($sql_teeth);

		$sql_waxing = "SELECT id, option_value FROM business_types_options WHERE business_type = '12' ORDER BY option_value ASC ";
		$query_waxing = $this->db->query($sql_waxing);
		// USED IN SEARCH FORM
		
		$logged_user_access_type = $this->users->get_user_access_type();


		$data = array('slides' => $query->result(), 'price_guide_list_options' => $price_guide_list_options->result(), 'business_types' => $business_types->result(), 'services' => $services->result(), 'perfect_for_list_options' => $perfect_for_list_options->result(), 'beauty_options' => $query_beauty->result(), 'fitness_options' => $query_fitness->result(), 'hair_options' => $query_hair->result(), 'makeup_options' => $query_makeup->result(), 'massage_options' => $query_massage->result(), 'mens_options' => $query_mens->result(), 'nails_options' => $query_nails->result(), 'tanning_options' => $query_tanning->result(), 'teeth_options' => $query_teeth->result(), 'waxing_options' => $query_waxing->result(), 'logged_user_access_type' => $logged_user_access_type);


		
		if(isset($_REQUEST['set_city']) && $_REQUEST['set_city'] != '')
		{
			$city_query = $this->db->get_where('city', array('id' => $_REQUEST['set_city']));
			$city_data = $city_query->result();
			//print_r($city_data);
			//die;
			$newdata = array('user_city_id'  => $city_data[0]->id, 'user_city_name'  => $city_data[0]->city_name);
			//print_r($newdata);
			//die;
			$this->session->set_userdata($newdata);
			redirect('', 'refresh');
		}
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('home/home', $data);
	}
	
	public function myshortlists()
	{	
		$data['title'] = 'ONDI FOOD';
		$shortlist_offers = array();
		
		$user_id = $this->CI_auth->logged_id();
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];
		if($user_id == '')
		{
			$ip_add = $_SERVER['REMOTE_ADDR'];
			$sql_count = "SELECT offer_id FROM temp_shortlists WHERE ip_add = '".$ip_add."' AND session_id = '".$session_id."' ";
			$query_count = $this->db->query($sql_count);
			$shortlist_offers = array();
			foreach($query_count->result() as $this_offer)
			{
				array_push($shortlist_offers, $this_offer->offer_id);
			}
		}
		else
		{
			$condition=array('user_id' => $user_id);
			$this->db->select('offer_id');
			$this->db->from('user_shortlists');
			$this->db->where($condition);
			$shortlist_details_query = $this->db->get();	
			$shortlist_offers_x = $shortlist_details_query->result();
			$shortlist_offers = array();
			foreach($shortlist_offers_x as $this_offer)
			{
				array_push($shortlist_offers, $this_offer->offer_id);
			}
		}
		
		//print_r($shortlist_offers); die;
		$return_array = array();
		if(isset($shortlist_offers))
		{
			foreach($shortlist_offers as $this_offer)
			{
				$this_offer_details = $this->offers->get_offer_details($this_offer);
				array_push($return_array, $this_offer_details);
			}
		}
		$data = array('shortlistdata' => $return_array);

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('/home/myshortlists', $data);
	}

	public function subscribenewsletter()
	{
		/*
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';

		echo '<pre>';
		print_r($_GET);
		echo '</pre>';
		exit();
		*/
		
		
		$nl_email = $this->input->post('nl_email');
		$nl_gender = $this->input->post('nl_gender');
		$query_nl = $this->db->get_where('newsletter_subscribers', array('email' => $nl_email)); 
		$count_nldb = $query_nl->num_rows(); 
		if($count_nldb == '0')
		{ 
			$nlinsert = array(
					'email' => $nl_email,
					'gender' => $nl_gender,
					'added_date' => date("Y-m-d H:i:s")
				);
			//print_r($nlinsert);
			$this->db->insert('newsletter_subscribers', $nlinsert);


			//////// Campaign Monitor Code //////////////
			$this->load->library('csrestsubscribers');
			$ResubscribeOption = true;
			$CustomFields[] = array('Key'=>"Custom Key", 'Value'=>"Custom Value");
			$wrap = new csrestsubscribers();
			$result = $wrap->add(array(
				'EmailAddress' => $nl_email,
				'Name' => "",
				'CustomFields' => array(
					array(
						'Key' => 'Field 1 Key',
						'Value' => 'Field Value'
					)
				),
				'Resubscribe' => false
			));

			/*echo "Result of POST /api/v3.1/subscribers/{list id}.{format}\n<br />";
			if($result->was_successful()) {
				echo "Subscribed with code ".$result->http_status_code;
			} else {
				echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
				var_dump($result->response);
				echo '</pre>';
			}*/
			//////// Campaign Monitor Code //////////////


			echo "You are now successfully subscribed!";
		}
		else
		{
			echo "You are already subscribed!";
		}
	}

	public function listyourbusiness()
	{
		$data['title'] = 'ONDI FOOD';

		/*$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1');
		$this->db->where($condition);
		$business_types = $this->db->get('business_types');*/

		$sql_business_types = "SELECT * FROM business_types WHERE display_status = '1' AND delete_status = '0'  ORDER BY type_name ASC ";
		$business_types = $this->db->query($sql_business_types);

		$data = array('business_types' => $business_types->result());

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('business_website_address', 'Website Address', 'trim|required');
		$this->form_validation->set_rules('business_street_address', 'Street Address', 'trim|required');
		$this->form_validation->set_rules('business_contact_name', 'Contact Name', 'trim|required');
		$this->form_validation->set_rules('business_email_address', 'Email Address', 'trim|required');
		//$this->form_validation->set_rules('business_password', 'Password', 'trim|required');
		
		/*Check if the form passed its validation */
		if ($this->form_validation->run() == FALSE) {
			$this->load->view('/home/listyourbusiness', $data);
		}
		else {
			if($this->input->post('submit')){
				$business_name				= $this->input->post('business_name');
				$business_website_address	= $this->input->post('business_website_address');
				$business_street_address	= $this->input->post('business_street_address');
				$business_contact_name		= $this->input->post('business_contact_name');
				$business_email_address		= $this->input->post('business_email_address');
				//$business_password			= $this->input->post('business_password');
				
				//print_r($_POST); die;
				
				
				$business_category = '';
				$arr_business_type = $this->input->post('business_type');	
				if(is_array($arr_business_type))
				{
					$business_category = implode(",", $arr_business_type);
				}

				$user_email_count = $this->users->users_count_by_email($business_email_address, "", "2,3");
				if($user_email_count>0)
				{										
					$this->session->set_flashdata('signup_message', 'Used email address is already registered! Please register with different email address!');
					//redirect('/home/listyourbusiness/', 'refresh');
					$data['signup_message'] = 'Used email address is already registered! Please register with different email address!';
					$this->load->view('/home/listyourbusiness', $data);
				}
				else
				{ 
					//$encrypt_pass="";
					//$rand_salt = $this->CI_encrypt->genRndSalt();					
					//$encrypt_pass = $this->CI_encrypt->encryptUserPwd($business_password, $rand_salt);

					$user_type = '2';
					/*$user_data = array(
								'user_type'=>$user_type,
								'email' => $business_email_address, 
								'contact_name'=>$business_contact_name,
								'password' => $encrypt_pass, 
								'salt'=>$rand_salt
					);*/
					$user_data = array(
								'user_type'=>$user_type,
								'email' => $business_email_address, 
								'contact_name'=>$business_contact_name
					);
					$this->db->insert('users', $user_data);	
					$newuserid = $this->db->insert_id();


					/////////// INSERT NOTIFICATION //////////
					$notification_message = "We've noticed that you haven't fully completed your profile details, this can help with users to really get all the information they need.";
					$notification_data = array('merchant_id'=>$newuserid, 'message' => $notification_message, 'profile_message' => '1', 'added_date'=>date("Y-m-d H:i:s"));
					$this->db->insert('user_notifications', $notification_data);	

					$notification_message1 = "Welcome to Ondi.com. Tell us a bit more about you so we can personalise your experience. Fill out your profile below.";
					$notification_data1 = array('merchant_id'=>$newuserid, 'message' => $notification_message1, 'profile_message' => '1', 'added_date'=>date("Y-m-d H:i:s"));
					$this->db->insert('user_notifications', $notification_data1);	

					/////////// INSERT NOTIFICATION //////////


					//////// Campaign Monitor Code //////////////
					$this->load->library('csrestsubscribers');
					$ResubscribeOption = true;
					$CustomFields[] = array('Key'=>"Custom Key", 'Value'=>"Custom Value");
					$wrap = new csrestsubscribers();
					$result = $wrap->add(array(
						'EmailAddress' => $business_email_address,
						'Name' => "",
						'CustomFields' => array(
							array(
								'Key' => 'Field 1 Key',
								'Value' => 'Field Value'
							)
						),
						'Resubscribe' => false
					));

					/*echo "Result of POST /api/v3.1/subscribers/{list id}.{format}\n<br />";
					if($result->was_successful()) {
						echo "Subscribed with code ".$result->http_status_code;
					} else {
						echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
						var_dump($result->response);
						echo '</pre>';
					}*/
					//////// Campaign Monitor Code //////////////





					// INSERT ADDITIONAL CATEGORIES IF ANY /////
					$num_category = $this->input->post('num_category');
					for($nso = 1; $nso<=$num_category; $nso++)
					{
						$additional_business_category = $this->input->post('additional_cat_'.$nso);
						if(trim($additional_business_category) != '')
						{
							$sql_count_duplicate = "SELECT type_id FROM business_types WHERE (type_name = '".$additional_business_category."' AND display_status = '1') OR ( type_name = '".$additional_business_category."' AND merchant_id = '".$newuserid."' )";
							$query_count_duplicate = $this->db->query($sql_count_duplicate);
							$count_duplicate = $query_count_duplicate->num_rows();
							if($count_duplicate==0)
							{						
								$insert_add_cat = array('type_name'=>$additional_business_category, 'display_status' => '0', 'merchant_id' => $newuserid);
								$this->db->insert('business_types', $insert_add_cat);	
								$newcategoryid = $this->db->insert_id();
								if($business_category != '')
								{
									$business_category = $business_category.",".$newcategoryid;
								}
								else
								{
									$business_category = $newcategoryid;
								}
							}
							else
							{
								$data_check_cat = $query_count_duplicate->result();
								$type_id = $data_check_cat[0]->type_id;

								if($business_category != '')
								{
									$explode_business_category = explode(",", $business_category);
									if (!in_array($type_id, $explode_business_category)) {
										$business_category = $business_category.",".$type_id;
									}
								}
								else
								{
									$business_category = $type_id;
								}
							}
						}
					}
					// INSERT ADDITIONAL CATEGORIES IF ANY /////


					//$newuserid = $this->CI_auth->logged_id();

					$query = $this->db->get_where('merchant_businessprofile', array('user_id' => $newuserid)); 
					$count = $query->num_rows();  
					if($count === 0)
					{ 
						$data_insert = array(
									   'user_id' => $newuserid,
									   'business_name' => $business_name,
									   'website_address' => $business_website_address,
									   'street_address' => $business_street_address,
									   'business_type' => $business_category
									);
						
						$this->db->insert('merchant_businessprofile', $data_insert);
					}
					//$this->session->set_flashdata('signup_message', 'Thanks! We will contact you soon.');
					//$this->load->view('/home/listyourbusiness', $data);
					//redirect('/home/listyourbusiness/?act=suc', 'refresh');
					redirect('/home/thanks/', 'refresh');

					//$info=array($business_email_address,$business_password);
					//$user_login_data = $this->CI_auth->process_login($info);

					/*if($this->CI_auth->check_logged()!==FALSE)
					{						
						$newdata = array(
						   'user_type'  => '2',
						   'email'     => $business_email_address,
						   'logged_in' => TRUE,
						   'contact_name' => $business_contact_name
							);
						$this->session->set_userdata($newdata);						
						redirect('/merchantaccount/businessprofile', 'refresh');
					}
					exit();*/
				}
			}
		}
	}
	
	/*public function searchresults()
	{	
		$data['title'] = 'ONDI FOOD';
		$return_array = array();

				// USED IN SEARCH FORM
		$this->db->select('pg_id, pg_name');
		$this->db->order_by("pg_id", "desc");
		$price_guide_list_options = $this->db->get('price_guide');
		

		$this->db->select('pf_id, pf_name');
		$perfect_for_list_options = $this->db->get('perfect_for');

		$this->db->select('type_id, type_name');
		$business_types = $this->db->get('business_types');

		$this->db->select('service_id, service');
		$services = $this->db->get('services');

		$sql_beauty = "SELECT id, option_value FROM business_types_options WHERE business_type = '3' ORDER BY option_value ASC ";
		$query_beauty = $this->db->query($sql_beauty);

		$sql_fitness = "SELECT id, option_value FROM business_types_options WHERE business_type = '4' ORDER BY option_value ASC ";
		$query_fitness = $this->db->query($sql_fitness);

		$sql_hair = "SELECT id, option_value FROM business_types_options WHERE business_type = '5' ORDER BY option_value ASC ";
		$query_hair = $this->db->query($sql_hair);

		$sql_makeup = "SELECT id, option_value FROM business_types_options WHERE business_type = '6' ORDER BY option_value ASC ";
		$query_makeup = $this->db->query($sql_makeup);

		$sql_massage = "SELECT id, option_value FROM business_types_options WHERE business_type = '7' ORDER BY option_value ASC ";
		$query_massage = $this->db->query($sql_massage);

		$sql_mens = "SELECT id, option_value FROM business_types_options WHERE business_type = '8' ORDER BY option_value ASC ";
		$query_mens = $this->db->query($sql_mens);

		$sql_nails = "SELECT id, option_value FROM business_types_options WHERE business_type = '9' ORDER BY option_value ASC ";
		$query_nails = $this->db->query($sql_nails);

		$sql_tanning = "SELECT id, option_value FROM business_types_options WHERE business_type = '10' ORDER BY option_value ASC ";
		$query_tanning = $this->db->query($sql_tanning);

		$sql_teeth = "SELECT id, option_value FROM business_types_options WHERE business_type = '11' ORDER BY option_value ASC ";
		$query_teeth = $this->db->query($sql_teeth);

		$sql_waxing = "SELECT id, option_value FROM business_types_options WHERE business_type = '12' ORDER BY option_value ASC ";
		$query_waxing = $this->db->query($sql_waxing);
		// USED IN SEARCH FORM
		
		$postdata = array();

		
		if($this->input->post('searchact')){
			//print_r($_POST); die;
			$search_term	= $this->input->post('search_term');
			$search_date	= $this->input->post('search_date');
			$search_time	= $this->input->post('search_time');
			$search_pg	= $this->input->post('search_pg');
			$search_pf	= $this->input->post('search_pf');
			$hidden_option_identifier	= $this->input->post('hidden_option_identifier');

			$search_distance	= $this->input->post('search_distance');
			$search_cu	= $this->input->post('search_cu');
			$search_beauty	= $this->input->post('search_beauty');
			$search_fitness	= $this->input->post('search_fitness');
			$search_hair	= $this->input->post('search_hair');
			$search_makeup	= $this->input->post('search_makeup');
			$search_massage	= $this->input->post('search_massage');
			$search_mens	= $this->input->post('search_mens');
			$search_nails	= $this->input->post('search_nails');
			$search_tanning	= $this->input->post('search_tanning');
			$search_teeth	= $this->input->post('search_teeth');
			$search_waxing	= $this->input->post('search_waxing');
			$user_rating	= $this->input->post('user_rating');

			
			$postdata['search_term'] = $search_term;
			$postdata['search_date'] = $search_date;
			$postdata['search_time'] = $search_time;
			$postdata['search_pg'] = $search_pg;
			$postdata['search_pf'] = $search_pf;
			$postdata['hidden_option_identifier'] = $hidden_option_identifier;
			
			$postdata['search_distance'] = $search_distance;
			$postdata['search_cu'] = $search_cu;
			$postdata['search_beauty'] = $search_beauty;
			$postdata['search_fitness'] = $search_fitness;
			$postdata['search_hair'] = $search_hair;
			$postdata['search_makeup'] = $search_makeup;
			$postdata['search_massage'] = $search_massage;
			$postdata['search_mens'] = $search_mens;
			$postdata['search_nails'] = $search_nails;
			$postdata['search_tanning'] = $search_tanning;
			$postdata['search_teeth'] = $search_teeth;
			$postdata['search_waxing'] = $search_waxing;
			$postdata['user_rating'] = $user_rating;

			
			$day = date("l", time());

			$sql = "SELECT DISTINCT a.id FROM merchant_offers a  
					LEFT JOIN merchant_offers_available_time b ON a.id = b.offer_id 
					LEFT JOIN merchant_businessprofile c ON a.merchant_id = c.user_id 
					WHERE  1 = 1 ";

			if($search_term != '')
			{
				$sql_part_jk = "";
				if($hidden_option_identifier != '')
				{
					if($hidden_option_identifier != '0')
					{
						$sql_part_jk .= " AND ( FIND_IN_SET(".$hidden_option_identifier.",business_type) OR a.offer_title LIKE '%".$search_term."%' ) " ;
					}
					else
					{
						$sql_part_jk .= " AND a.offer_title LIKE '%".$search_term."%'";
					}
				}
				else
				{
					$sql_part_jk .= " AND a.offer_title LIKE '%".$search_term."%'";
				}
				$sql .= $sql_part_jk;
			}

			if($search_date != '')
			{
				$sql .= " AND ('".$search_date."' BETWEEN a.offer_running_from AND a.offer_running_to )";
				$day = date("l", strtotime($search_date));
			}

			if($search_time != '')
			{
				$sql .= " AND ('".$search_time."' BETWEEN b.from_hours AND b.to_hours )";
			}

			if($search_pg != '')
			{
				if(is_array($search_pg))
				{
					$sql_pg = "";
					foreach($search_pg as $thesearch_pg)
					{
						$sql_pg .= " c.price_guide = '".$thesearch_pg."' OR ";
					}
					if($sql_pg != "")
					{
						$sql_pg = substr($sql_pg, 0, -3);
						$sql .= " AND ( ".$sql_pg." ) ";
					}
				}
			}

			if($search_pf != '')
			{
				if(is_array($search_pf))
				{
					$sql_pf = "";
					foreach($search_pf as $thesearch_pf)
					{
						$sql_pf .= " c.perfect_for = '".$thesearch_pf."' OR ";
					}
					if($sql_pf != "")
					{
						$sql_pf = substr($sql_pf, 0, -3);
						$sql .= " AND ( ".$sql_pf." ) ";
					}
				}
			}

			



			$sql .= " ORDER BY a.offer_title ASC ";
			//echo $sql; die;

			$query = $this->db->query($sql);
			foreach($query->result() as $this_offer)
			{
				$this_offer_details = $this->offers->get_offer_details($this_offer->id);
				array_push($return_array, $this_offer_details);
			}
		}

		$data = array('searchdata' => $return_array, 'postdata' => $postdata, 'price_guide_list_options' => $price_guide_list_options->result(), 'business_types' => $business_types->result(), 'services' => $services->result(), 'perfect_for_list_options' => $perfect_for_list_options->result(), 'beauty_options' => $query_beauty->result(), 'fitness_options' => $query_fitness->result(), 'hair_options' => $query_hair->result(), 'makeup_options' => $query_makeup->result(), 'massage_options' => $query_massage->result(), 'mens_options' => $query_mens->result(), 'nails_options' => $query_nails->result(), 'tanning_options' => $query_tanning->result(), 'teeth_options' => $query_teeth->result(), 'waxing_options' => $query_waxing->result());
		$this->load->view('/home/searchresults', $data);
	}*/


	public function searchresults()
	{
		
		$data['title'] = 'ONDI FOOD';
		$return_array = array();

		// USED IN SEARCH FORM
		$this->db->select('pg_id, pg_name');
		$this->db->order_by("pg_id", "desc");
		$price_guide_list_options = $this->db->get('price_guide');

		$this->db->select('pf_id, pf_name');
		$condition=array('delete_status' => '0', 'pf_name != ' => 'N/A');
		$this->db->where($condition);
		$perfect_for_list_options = $this->db->get('perfect_for');

		//$this->db->select('type_id, type_name');
		//$business_types = $this->db->get('business_types');
		
		$this->db->select('type_id, type_name');
		$condition=array('display_status' => '1', 'delete_status' => '0');
		$this->db->where($condition);
		$this->db->order_by("type_name", "asc");
		$business_types = $this->db->get('business_types');

		$this->db->select('service_id, service');
		$services = $this->db->get('services');

		$sql_beauty = "SELECT id, option_value FROM business_types_options WHERE business_type = '3' ORDER BY option_value ASC ";
		$query_beauty = $this->db->query($sql_beauty);

		$sql_fitness = "SELECT id, option_value FROM business_types_options WHERE business_type = '4' ORDER BY option_value ASC ";
		$query_fitness = $this->db->query($sql_fitness);

		$sql_hair = "SELECT id, option_value FROM business_types_options WHERE business_type = '5' ORDER BY option_value ASC ";
		$query_hair = $this->db->query($sql_hair);

		$sql_makeup = "SELECT id, option_value FROM business_types_options WHERE business_type = '6' ORDER BY option_value ASC ";
		$query_makeup = $this->db->query($sql_makeup);

		$sql_massage = "SELECT id, option_value FROM business_types_options WHERE business_type = '7' ORDER BY option_value ASC ";
		$query_massage = $this->db->query($sql_massage);

		$sql_mens = "SELECT id, option_value FROM business_types_options WHERE business_type = '8' ORDER BY option_value ASC ";
		$query_mens = $this->db->query($sql_mens);

		$sql_nails = "SELECT id, option_value FROM business_types_options WHERE business_type = '9' ORDER BY option_value ASC ";
		$query_nails = $this->db->query($sql_nails);

		$sql_tanning = "SELECT id, option_value FROM business_types_options WHERE business_type = '10' ORDER BY option_value ASC ";
		$query_tanning = $this->db->query($sql_tanning);

		$sql_teeth = "SELECT id, option_value FROM business_types_options WHERE business_type = '11' ORDER BY option_value ASC ";
		$query_teeth = $this->db->query($sql_teeth);

		$sql_waxing = "SELECT id, option_value FROM business_types_options WHERE business_type = '12' ORDER BY option_value ASC ";
		$query_waxing = $this->db->query($sql_waxing);
		// USED IN SEARCH FORM
		
		$postdata = array();
		//print_r($_GET);
		if((bool)$this->config->item('test_mode'))
		{
			//print_r($_POST); die;
		}
		if($this->input->get_post('searchact')){
			//print_r($_GET);  die;
			$search_term		= $this->input->get_post('search_term');
			$search_date		= $this->input->get_post('search_date');
			//$search_time		= $this->input->get_post('search_time');
			$search_pg			= $this->input->get_post('search_pg');
			$search_pf	= $this->input->get_post('search_pf');
			$hidden_option_identifier	= $this->input->get_post('hidden_option_identifier');

			$search_distance	= $this->input->get_post('search_distance');
			$search_cu			= $this->input->get_post('search_cu');
			$search_beauty		= $this->input->get_post('search_beauty');
			$search_fitness		= $this->input->get_post('search_fitness');
			$search_hair		= $this->input->get_post('search_hair');
			$search_makeup		= $this->input->get_post('search_makeup');
			$search_massage		= $this->input->get_post('search_massage');
			$search_mens		= $this->input->get_post('search_mens');
			$search_nails		= $this->input->get_post('search_nails');
			$search_tanning		= $this->input->get_post('search_tanning');
			$search_teeth		= $this->input->get_post('search_teeth');
			$search_waxing		= $this->input->get_post('search_waxing');
			$user_rating		= $this->input->get_post('user_rating');

			$search_time_other		= $this->input->get_post('search_time_other');
			$search_time_restaurant		= $this->input->get_post('search_time_restaurant');

			

			if(is_array($search_pg)) $search_pg = implode(",", $search_pg);
			if(is_array($search_distance)) $search_distance = implode(",", $search_distance);
			if(is_array($search_cu)) $search_cu = implode(",", $search_cu);
			if(is_array($search_beauty)) $search_beauty = implode(",", $search_beauty);
			if(is_array($search_fitness)) $search_fitness = implode(",", $search_fitness);
			if(is_array($search_hair)) $search_hair = implode(",", $search_hair);
			if(is_array($search_makeup)) $search_makeup = implode(",", $search_makeup);
			if(is_array($search_massage)) $search_massage = implode(",", $search_massage);
			if(is_array($search_mens)) $search_mens = implode(",", $search_mens);
			if(is_array($search_nails)) $search_nails = implode(",", $search_nails);
			if(is_array($search_tanning)) $search_tanning = implode(",", $search_tanning);
			if(is_array($search_teeth)) $search_teeth = implode(",", $search_teeth);
			if(is_array($search_waxing)) $search_waxing = implode(",", $search_waxing);
			if(is_array($user_rating)) $user_rating = implode(",", $user_rating);

			if(is_array($search_time_other)) $search_time = implode(",", $search_time_other);
			if(is_array($search_time_restaurant)) $search_time = implode(",", $search_time_restaurant);



			$sorttby	= $this->input->get_post('hidden_sort_by');




			
			$postdata['search_term'] = $search_term;
			$postdata['search_date'] = $search_date;
			$postdata['search_time'] = $search_time;
			$postdata['search_pg'] = $search_pg;
			$postdata['search_pf'] = $search_pf;
			$postdata['hidden_option_identifier'] = $hidden_option_identifier;
			
			$postdata['search_distance'] = $search_distance;
			$postdata['search_cu'] = $search_cu;
			$postdata['search_beauty'] = $search_beauty;
			$postdata['search_fitness'] = $search_fitness;
			$postdata['search_hair'] = $search_hair;
			$postdata['search_makeup'] = $search_makeup;
			$postdata['search_massage'] = $search_massage;
			$postdata['search_mens'] = $search_mens;
			$postdata['search_nails'] = $search_nails;
			$postdata['search_tanning'] = $search_tanning;
			$postdata['search_teeth'] = $search_teeth;
			$postdata['search_waxing'] = $search_waxing;
			$postdata['user_rating'] = $user_rating;

			
	
			
			$day = date("l", time());

			/*$sql = "SELECT DISTINCT a.id FROM merchant_offers a  
					LEFT JOIN merchant_offers_available_time b ON a.id = b.offer_id 
					LEFT JOIN merchant_businessprofile c ON a.merchant_id = c.user_id 
					WHERE  1 = 1 ";*/
			
			$sess_user_data = $this->session->all_userdata();
			$lat_searched = $sess_user_data['latitude'];
			$lan_searched = $sess_user_data['longitude'];


			if(isset($sess_user_data['user_city_id']))
			{
				$user_city_name = $sess_user_data['user_city_name'];
				$user_city_id = $sess_user_data['user_city_id'];
			}
			else
			{
				$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
				$default_city_data = $default_city_query->result();
				$user_city_name = $default_city_data[0]->city_name;
				$user_city_id = $default_city_data[0]->id;
			}
			
			if($search_date != '')
			{
				if(trim($search_date) == 'Today')
				{
					$search_date_x = date("Y-m-d");
				}
				else
				{
					$explode_search_date = explode("/", $search_date);
					$search_date_x = $explode_search_date[2]."-".$explode_search_date[1]."-".$explode_search_date[0];
				}
				$daysearched = strtolower(date("l", strtotime($search_date_x)));
			}


			$sql = "SELECT DISTINCT a.id, (3959 * acos( cos( radians(".$lat_searched.") ) * cos( radians( c.lat ) ) * cos( radians( c.long ) - radians(".$lan_searched.") ) + sin( radians(".$lat_searched.") ) * sin( radians( c.lat ) ) ) ) AS distance, (SELECT MAX(x.to_hours)
FROM merchant_offers_available_time x
WHERE x.offer_id = a.id AND x.available_day = '".$daysearched."' 
) AS maxtime FROM merchant_offers a  
					LEFT JOIN merchant_offers_available_time b ON a.id = b.offer_id 
					LEFT JOIN merchant_businessprofile c ON a.merchant_id = c.user_id 
					WHERE  1 = 1 AND a.display_status = '1'  AND c.city = '".$user_city_id."' ";

			if($search_term != '' && $search_term != 'All')
			{
				$sql_part_jk = "";
				if($hidden_option_identifier != '')
				{
					if($hidden_option_identifier != '0')
					{
						$sql_part_jk .= " AND ( FIND_IN_SET(".$hidden_option_identifier.",business_type) OR a.offer_title LIKE '%".$search_term."%' ) " ;
					}
					else
					{
						$sql_part_jk .= " AND a.offer_title LIKE '%".$search_term."%'";
					}
				}
				else
				{
					$sql_part_jk .= " AND ( a.offer_title LIKE '%".$search_term."%' OR c.business_name LIKE '%".$search_term."%' )" ;
				}
				$sql .= $sql_part_jk;
			}
			
			/*if((bool)$this->config->item('test_mode'))
			{
				echo "search_date--->".$search_date;
				$explode_search_date = explode("/", $search_date);
				$search_date = $explode_search_date[2]."-".$explode_search_date[1]."-".$explode_search_date[0];
				echo "search_date--->".$search_date;
			}*/
			if($search_date != '')
			{
				if(trim($search_date) == 'Today')
				{
					$sql .= " AND ('".date("Y-m-d")."' BETWEEN a.offer_running_from AND a.offer_running_to )";
					$sess_user_data['search_date'] = date("Y-m-d");
				}
				else
				{
					$explode_search_date = explode("/", $search_date);
					$search_date = $explode_search_date[2]."-".$explode_search_date[1]."-".$explode_search_date[0];
					$sql .= " AND ('".date("Y-m-d", strtotime($search_date))."' BETWEEN a.offer_running_from AND a.offer_running_to )";
					$sess_user_data['search_date'] = date("Y-m-d", strtotime($search_date));
				}
				$this->session->set_userdata($sess_user_data);			
				
				//$sql .= " AND ('".$search_date."' BETWEEN a.offer_running_from AND a.offer_running_to )";
				$day = date("l", strtotime($search_date));

				$sql .= " AND (available_day = '".strtolower($day)."' || a.everyday_of_week = '1' )";
			}

			if($search_time != '')
			{
				$sql_time = "";
				$explode_search_time = explode(",", $search_time);
				foreach($explode_search_time as $thesearch_time)
				{
					if(trim($thesearch_time) == 'Breakfast')
					{
						$search_time_from = "0600";
						$search_time_to = "1200";
					}
					else if(trim($thesearch_time) == 'Lunch')
					{
						$search_time_from = "1200";
						$search_time_to = "1700";
					}
					else if(trim($thesearch_time) == 'Dinner')
					{
						$search_time_from = "1800";
						$search_time_to = "2359";
					}
					else if(trim($thesearch_time) == 'Morning')
					{
						$search_time_from = "0600";
						$search_time_to = "1200";
					}
					else if(trim($thesearch_time) == 'Around Lunch')
					{
						$search_time_from = "1200";
						$search_time_to = "1500";
					}
					else if(trim($thesearch_time) == 'Afternoonish')
					{
						$search_time_from = "1500";
						$search_time_to = "1800";
					}
					else if(trim($thesearch_time) == 'Evening')
					{
						$search_time_from = "1800";
						$search_time_to = "2359";
					}
					$sql_time .= "  ( ('".$search_time_from."' <= from_hours AND '".$search_time_to."' <= to_hours AND '".$search_time_to."' >= from_hours ) OR ('".$search_time_from."' >= from_hours AND '".$search_time_from."' <= to_hours AND '".$search_time_to."' >= to_hours ) OR ('".$search_time_from."' >= from_hours AND '".$search_time_to."' <= to_hours ) OR ('".$search_time_from."' <= from_hours AND '".$search_time_to."' >= to_hours ) ) OR ";
				}
				if($sql_time != "")
				{
					$sql_time = substr($sql_time, 0, -3);
					$sql .= " AND ( ".$sql_time." ) ";
				}
				
				


			}

			if($search_pg != '')
			{
				$arr_search_pg = explode(",", $search_pg);
				if(is_array($arr_search_pg))
				{
					$sql_pg = "";
					foreach($arr_search_pg as $thesearch_pg)
					{
						$sql_pg .= " c.price_guide = '".$thesearch_pg."' OR ";
					}
					if($sql_pg != "")
					{
						$sql_pg = substr($sql_pg, 0, -3);
						$sql .= " AND ( ".$sql_pg." ) ";
					}
				}
			}

			if($search_pf != '')
			{
				$arr_search_pf = explode(",", $search_pf);
				if(is_array($arr_search_pf))
				{
					$sql_pf = "";
					foreach($arr_search_pf as $thesearch_pf)
					{
						$sql_pf .= " c.perfect_for = '".$thesearch_pf."' OR ";
					}
					if($sql_pf != "")
					{
						$sql_pf = substr($sql_pf, 0, -3);
						$sql .= " AND ( ".$sql_pf." ) ";
					}
				}
			}

			if($user_rating != '')
			{
				$sql .= " AND a.average_rating IN (".$user_rating.") ";
			}

			
			$having_used = '0';
			if($search_distance != '')
			{
				$arr_search_distance = explode(",", $search_distance);
				if(is_array($arr_search_distance))
				{
					$sql_dist = "";
					foreach($arr_search_distance as $thesearch_dist)
					{
						$sql_dist .= " distance <= ".$thesearch_dist." OR ";
					}
					if($sql_dist != "")
					{
						$sql_dist = substr($sql_dist, 0, -3);
						$sql .= " HAVING ( ".$sql_dist." ) ";
						$having_used = '1';
					}
				}
			}

			if(trim($search_date) == 'Today' || $search_date == date("Y-m-d"))
			{
				$current_mil_time = date("Hi");
				if($having_used == '0')
				{
					$sql .= " HAVING maxtime > ".$current_mil_time." ";
				}
				else if($having_used == '1')
				{
					$sql .= " AND maxtime > ".$current_mil_time." ";
				}
			}

			
			$sorttbynewest = "";
			$sorttbyclosest = "";
			$sorttbyaz = "";
			$sorttbyza = "";
			$sorttbyrating = "";
			if($sorttby == 'Newest')
			{
				$sql .= " ORDER BY a.id DESC ";
				$sorttbynewest = " selected='selected' ";
			}
			else if($sorttby == 'Closest')
			{
				$sql .= " ORDER BY distance ASC ";
				$sorttbyclosest = " selected='selected' ";
			}
			else if($sorttby == 'A-Z')
			{
				$sql .= " ORDER BY a.offer_title ASC ";
				$sorttbyaz = " selected='selected' ";
			}
			else if($sorttby == 'Z-A')
			{
				$sql .= " ORDER BY a.offer_title DESC ";
				$sorttbyza = " selected='selected' ";
			}
			else if($sorttby == 'Rating')
			{
				$sql .= " ORDER BY a.average_rating DESC ";
				$sorttbyrating = " selected='selected' ";
			}
			

			if((bool)$this->config->item('test_mode'))
			{
				//echo $sql; //die;
			}
			//echo $sql; 

			$query = $this->db->query($sql);
			$search_records = "";
			foreach($query->result() as $this_offer)
			{
				$this_offer_details = $this->offers->get_offer_details($this_offer->id);
				array_push($return_array, $this_offer_details);
				$search_records .= $this_offer->id.", ";

				$this->users->update_results_viewed_offer($this_offer->id);
			}
			if($search_records != "")
			{
				$search_records = substr($search_records, 0 , -2);
				$sessiondata = $this->session->all_userdata();
				$session_id = $sessiondata['session_id'];
				$del_prev_search = "DELETE FROM temp_search_data WHERE 	session_id = '".$session_id."' ";
				$query_del = $this->db->query($del_prev_search);

				$ins_new_search = "INSERT INTO temp_search_data (session_id, search_results) VALUES ('".$session_id."', '".$search_records."' ) ";
				$query_ins = $this->db->query($ins_new_search);
			}
		}

		$data = array('searchdata' => $return_array, 'postdata' => $postdata, 'price_guide_list_options' => $price_guide_list_options->result(), 'business_types' => $business_types->result(), 'services' => $services->result(), 'perfect_for_list_options' => $perfect_for_list_options->result(), 'beauty_options' => $query_beauty->result(), 'fitness_options' => $query_fitness->result(), 'hair_options' => $query_hair->result(), 'makeup_options' => $query_makeup->result(), 'massage_options' => $query_massage->result(), 'mens_options' => $query_mens->result(), 'nails_options' => $query_nails->result(), 'tanning_options' => $query_tanning->result(), 'teeth_options' => $query_teeth->result(), 'waxing_options' => $query_waxing->result(), 'sorttby' => $sorttby);
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */




//        print_r($data);
//        exit;


		$this->load->view('/home/searchresults', $data);
		
	}

	public function ajaxsearchresults()
	{	
		$data['title'] = 'ONDI FOOD';
		$return_array = array();

				// USED IN SEARCH FORM
		$this->db->select('pg_id, pg_name');
		$this->db->order_by("pg_id", "desc");
		$price_guide_list_options = $this->db->get('price_guide');
		

		$this->db->select('pf_id, pf_name');
		$perfect_for_list_options = $this->db->get('perfect_for');

		$this->db->select('type_id, type_name');
		$business_types = $this->db->get('business_types');

		$this->db->select('service_id, service');
		$services = $this->db->get('services');

		$sql_beauty = "SELECT id, option_value FROM business_types_options WHERE business_type = '3' ORDER BY option_value ASC ";
		$query_beauty = $this->db->query($sql_beauty);

		$sql_fitness = "SELECT id, option_value FROM business_types_options WHERE business_type = '4' ORDER BY option_value ASC ";
		$query_fitness = $this->db->query($sql_fitness);

		$sql_hair = "SELECT id, option_value FROM business_types_options WHERE business_type = '5' ORDER BY option_value ASC ";
		$query_hair = $this->db->query($sql_hair);

		$sql_makeup = "SELECT id, option_value FROM business_types_options WHERE business_type = '6' ORDER BY option_value ASC ";
		$query_makeup = $this->db->query($sql_makeup);

		$sql_massage = "SELECT id, option_value FROM business_types_options WHERE business_type = '7' ORDER BY option_value ASC ";
		$query_massage = $this->db->query($sql_massage);

		$sql_mens = "SELECT id, option_value FROM business_types_options WHERE business_type = '8' ORDER BY option_value ASC ";
		$query_mens = $this->db->query($sql_mens);

		$sql_nails = "SELECT id, option_value FROM business_types_options WHERE business_type = '9' ORDER BY option_value ASC ";
		$query_nails = $this->db->query($sql_nails);

		$sql_tanning = "SELECT id, option_value FROM business_types_options WHERE business_type = '10' ORDER BY option_value ASC ";
		$query_tanning = $this->db->query($sql_tanning);

		$sql_teeth = "SELECT id, option_value FROM business_types_options WHERE business_type = '11' ORDER BY option_value ASC ";
		$query_teeth = $this->db->query($sql_teeth);

		$sql_waxing = "SELECT id, option_value FROM business_types_options WHERE business_type = '12' ORDER BY option_value ASC ";
		$query_waxing = $this->db->query($sql_waxing);
		// USED IN SEARCH FORM
		
		$postdata = array();

		
		if($this->input->post('searchact')){
			//print_r($_POST); 
			$search_term		= $this->input->post('search_term');
			$search_date		= $this->input->post('search_date');
			$search_time		= $this->input->post('search_time');
			$search_pg			= $this->input->post('search_pg');
			$search_pf	= $this->input->post('search_pf');
			$hidden_option_identifier	= $this->input->post('hidden_option_identifier');

			$search_distance	= $this->input->post('search_distance');
			$search_cu			= $this->input->post('search_cu');
			$search_beauty		= $this->input->post('search_beauty');
			$search_fitness		= $this->input->post('search_fitness');
			$search_hair		= $this->input->post('search_hair');
			$search_makeup		= $this->input->post('search_makeup');
			$search_massage		= $this->input->post('search_massage');
			$search_mens		= $this->input->post('search_mens');
			$search_nails		= $this->input->post('search_nails');
			$search_tanning		= $this->input->post('search_tanning');
			$search_teeth		= $this->input->post('search_teeth');
			$search_waxing		= $this->input->post('search_waxing');
			$user_rating		= $this->input->post('user_rating');

			$sorttby	= $this->input->post('sorttby');




			
			$postdata['search_term'] = $search_term;
			$postdata['search_date'] = $search_date;
			$postdata['search_time'] = $search_time;
			$postdata['search_pg'] = $search_pg;
			$postdata['search_pf'] = $search_pf;
			$postdata['hidden_option_identifier'] = $hidden_option_identifier;
			
			$postdata['search_distance'] = $search_distance;
			$postdata['search_cu'] = $search_cu;
			$postdata['search_beauty'] = $search_beauty;
			$postdata['search_fitness'] = $search_fitness;
			$postdata['search_hair'] = $search_hair;
			$postdata['search_makeup'] = $search_makeup;
			$postdata['search_massage'] = $search_massage;
			$postdata['search_mens'] = $search_mens;
			$postdata['search_nails'] = $search_nails;
			$postdata['search_tanning'] = $search_tanning;
			$postdata['search_teeth'] = $search_teeth;
			$postdata['search_waxing'] = $search_waxing;
			$postdata['user_rating'] = $user_rating;
			

			
			$day = date("l", time());

			/*$sql = "SELECT DISTINCT a.id FROM merchant_offers a  
					LEFT JOIN merchant_offers_available_time b ON a.id = b.offer_id 
					LEFT JOIN merchant_businessprofile c ON a.merchant_id = c.user_id 
					WHERE  1 = 1 ";*/
			
			$sess_user_data = $this->session->all_userdata();
			$lat_searched = $sess_user_data['latitude'];
			$lan_searched = $sess_user_data['longitude'];


			if(isset($sess_user_data['user_city_id']))
			{
				$user_city_name = $sess_user_data['user_city_name'];
				$user_city_id = $sess_user_data['user_city_id'];
			}
			else
			{
				$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
				$default_city_data = $default_city_query->result();
				$user_city_name = $default_city_data[0]->city_name;
				$user_city_id = $default_city_data[0]->id;
			}

			$sql = "SELECT DISTINCT a.id, (3959 * acos( cos( radians(".$lat_searched.") ) * cos( radians( c.lat ) ) * cos( radians( c.long ) - radians(".$lan_searched.") ) + sin( radians(".$lat_searched.") ) * sin( radians( c.lat ) ) ) ) AS distance FROM merchant_offers a  
					LEFT JOIN merchant_offers_available_time b ON a.id = b.offer_id 
					LEFT JOIN merchant_businessprofile c ON a.merchant_id = c.user_id 
					WHERE  1 = 1 AND a.display_status = '1'  AND c.city = '".$user_city_id."' ";

			if($search_term != '' && $search_term != 'All')
			{
				$sql_part_jk = "";
				if($hidden_option_identifier != '')
				{
					if($hidden_option_identifier != '0')
					{
						$sql_part_jk .= " AND ( FIND_IN_SET(".$hidden_option_identifier.",business_type) OR a.offer_title LIKE '%".$search_term."%' ) " ;
					}
					else
					{
						$sql_part_jk .= " AND a.offer_title LIKE '%".$search_term."%'";
					}
				}
				else
				{
					$sql_part_jk .= " AND a.offer_title LIKE '%".$search_term."%'";
				}
				$sql .= $sql_part_jk;
			}

			if($search_date != '')
			{
				if(trim($search_date) == 'Today')
				{
					$sql .= " AND ('".date("Y-m-d")."' BETWEEN a.offer_running_from AND a.offer_running_to )";
					$sess_user_data['search_date'] = date("Y-m-d");
				}
				else
				{
					$sql .= " AND ('".date("Y-m-d", strtotime($search_date))."' BETWEEN a.offer_running_from AND a.offer_running_to )";
					$sess_user_data['search_date'] = date("Y-m-d", strtotime($search_date));
				}
				$this->session->set_userdata($sess_user_data);			
				
				//$sql .= " AND ('".$search_date."' BETWEEN a.offer_running_from AND a.offer_running_to )";
				$day = date("l", strtotime($search_date));

				$sql .= " AND (available_day = '".strtolower($day)."' || a.everyday_of_week = '1' )";
			}

			if($search_time != '')
			{
				if(trim($search_time) == 'Breakfast')
				{
					$search_time_from = "0600";
					$search_time_to = "1200";
				}
				else if(trim($search_time) == 'Lunch')
				{
					$search_time_from = "1200";
					$search_time_to = "1700";
				}
				else if(trim($search_time) == 'Dinner')
				{
					$search_time_from = "1800";
					$search_time_to = "2359";
				}
				else if(trim($search_time) == 'Morning')
				{
					$search_time_from = "0600";
					$search_time_to = "1200";
				}
				else if(trim($search_time) == 'Around Lunch')
				{
					$search_time_from = "1200";
					$search_time_to = "1500";
				}
				else if(trim($search_time) == 'Afternoonish')
				{
					$search_time_from = "1500";
					$search_time_to = "1800";
				}
				else if(trim($search_time) == 'Evening')
				{
					$search_time_from = "1800";
					$search_time_to = "2359";
				}
				
				
				
				//$sql .= " AND ( '".$search_time."' BETWEEN b.from_hours AND b.to_hours )";

				$sql .= " AND ( ('".$search_time_from."' <= from_hours AND '".$search_time_to."' <= to_hours AND '".$search_time_to."' >= from_hours ) OR ('".$search_time_from."' >= from_hours AND '".$search_time_from."' <= to_hours AND '".$search_time_to."' >= to_hours ) OR ('".$search_time_from."' >= from_hours AND '".$search_time_to."' <= to_hours ) OR ('".$search_time_from."' <= from_hours AND '".$search_time_to."' >= to_hours ) )";


			}

			if($search_pg != '')
			{
				$arr_search_pg = explode(",", $search_pg);
				if(is_array($arr_search_pg))
				{
					$sql_pg = "";
					foreach($arr_search_pg as $thesearch_pg)
					{
						$sql_pg .= " c.price_guide = '".$thesearch_pg."' OR ";
					}
					if($sql_pg != "")
					{
						$sql_pg = substr($sql_pg, 0, -3);
						$sql .= " AND ( ".$sql_pg." ) ";
					}
				}
			}

			if($search_pf != '')
			{
				$arr_search_pf = explode(",", $search_pf);
				if(is_array($arr_search_pf))
				{
					$sql_pf = "";
					foreach($arr_search_pf as $thesearch_pf)
					{
						$sql_pf .= " c.perfect_for = '".$thesearch_pf."' OR ";
					}
					if($sql_pf != "")
					{
						$sql_pf = substr($sql_pf, 0, -3);
						$sql .= " AND ( ".$sql_pf." ) ";
					}
				}
			}

			if($user_rating != '')
			{
				$sql .= " AND a.average_rating IN (".$user_rating.") ";
			}

			
			
			if($search_distance != '')
			{
				$arr_search_distance = explode(",", $search_distance);
				if(is_array($arr_search_distance))
				{
					$sql_dist = "";
					foreach($arr_search_distance as $thesearch_dist)
					{
						$sql_dist .= " distance <= ".$thesearch_dist." OR ";
					}
					if($sql_dist != "")
					{
						$sql_dist = substr($sql_dist, 0, -3);
						$sql .= " HAVING ( ".$sql_dist." ) ";
					}
				}
			}

			

			

			$sorttbynewest = "";
			$sorttbyclosest = "";
			$sorttbyaz = "";
			$sorttbyza = "";
			$sorttbyrating = "";
			if($sorttby == 'Newest')
			{
				$sql .= " ORDER BY a.id DESC ";
				$sorttbynewest = " selected='selected' ";
			}
			else if($sorttby == 'Closest')
			{
				$sql .= " ORDER BY distance ASC ";
				$sorttbyclosest = " selected='selected' ";
			}
			else if($sorttby == 'A-Z')
			{
				$sql .= " ORDER BY a.offer_title ASC ";
				$sorttbyaz = " selected='selected' ";
			}
			else if($sorttby == 'Z-A')
			{
				$sql .= " ORDER BY a.offer_title DESC ";
				$sorttbyza = " selected='selected' ";
			}
			else if($sorttby == 'Rating')
			{
				$sql .= " ORDER BY a.average_rating DESC ";
				$sorttbyrating = " selected='selected' ";
			}

			if((bool)$this->config->item('test_mode'))
			{
				//echo $sql; //die;
			}
			//echo $sql;

			$query = $this->db->query($sql);
			$search_records = "";
			foreach($query->result() as $this_offer)
			{
				$this_offer_details = $this->offers->get_offer_details($this_offer->id);
				array_push($return_array, $this_offer_details);
				$search_records .= $this_offer->id.", ";

				$this->users->update_results_viewed_offer($this_offer->id);
			}
			if($search_records != "")
			{
				$search_records = substr($search_records, 0 , -2);
				$sessiondata = $this->session->all_userdata();
				$session_id = $sessiondata['session_id'];
				$del_prev_search = "DELETE FROM temp_search_data WHERE 	session_id = '".$session_id."' ";
				$query_del = $this->db->query($del_prev_search);

				$ins_new_search = "INSERT INTO temp_search_data (session_id, search_results) VALUES ('".$session_id."', '".$search_records."' ) ";
				$query_ins = $this->db->query($ins_new_search);
			}
		}

		$data = array('searchdata' => $return_array, 'postdata' => $postdata, 'price_guide_list_options' => $price_guide_list_options->result(), 'business_types' => $business_types->result(), 'services' => $services->result(), 'perfect_for_list_options' => $perfect_for_list_options->result(), 'beauty_options' => $query_beauty->result(), 'fitness_options' => $query_fitness->result(), 'hair_options' => $query_hair->result(), 'makeup_options' => $query_makeup->result(), 'massage_options' => $query_massage->result(), 'mens_options' => $query_mens->result(), 'nails_options' => $query_nails->result(), 'tanning_options' => $query_tanning->result(), 'teeth_options' => $query_teeth->result(), 'waxing_options' => $query_waxing->result());
		
		$searchdata = $return_array;
		//print_r($data);

		$sessiondata = $this->session->all_userdata();
		$session_id = $sessiondata['session_id'];

		$return_str = '<div class="compare1_content">
    	<div class="compare_view">
        	<div class="sorted_by" id="div_sorted_by" >
            	<p>Sort by:</p>
				<span class="sort_dd">
                	


					<div class="custom_select_ajax">

					<select class="revostyled" tabindex="4" id="sortby" id="sortby" onchange="search_offers(this.value);">
                        <option value="Newest" '.$sorttbynewest.'>Newest</option>
                        <option value="Closest" '.$sorttbyclosest.' >Closest</option>
                        <option value="A-Z" '.$sorttbyaz.' >A-Z</option>
						<option value="Z-A" '.$sorttbyza.' >Z-A</option>
						<option value="Rating" '.$sorttbyrating.' >Rating</option>
					</select>
					</div>


                </span>
            </div>
            <div class="list_view">
            	<ul>
                	<li><a href="javascript:void(0);" onclick="list_view();"><img src="'.$this->config->item('base_url').'public/images/list_view_image.png" alt=""> List view</a></li>';

					if(sizeof($searchdata)>0)
					{
						$return_str .= '<li><a href="javascript:void(0);" onclick="map_view(\''.$session_id.'\');"><img src="'.$this->config->item('base_url').'public/images/map_view.png" alt=""> Map view</a></li>';
					}

                $return_str .= '</ul>
            </div>
        </div>
        <div class="compare1_content_detail" id="listsearchresults" >';
			if(sizeof($searchdata)>0)
			{
				$return_str .= '<ul>';
				$numshort = 0;
				foreach($searchdata as $search)
				{
					$numshort++; 
					$how_many_left = $this->offers->how_many_left($search->offer_id);
					$merchant_data = $this->offers->get_merchant_id($search->offer_id);
					//$how_many_left = sizeof($how_many_left_data);
					if($numshort%2==0){ $classx = ' class="gray" '; } else { $classx = '';} 
					
					$return_str .= '<li '.$classx.' ><div class="image"><a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'"><img src="'.$this->config->item('base_url').$search->profile_picture.'" alt="" width="112" height="110" /></a></div><div class="address"><h3><a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'">'.$search->business_name.'</a></h3><h4>'.$search->suburb.'</h4><p>'.$this->users->display_merchant_business($search->business_type).'</p><div class="cradit"><p>'.$search->pg_name.'<span></span></p><img src="'.$this->config->item('base_url').'public/images/'.$this->offers->get_offer_star_ratings($search->offer_id).'.png" alt="" /></div></div><div class="name"><table><tr><td><a href="'.base_url("merchantoffers/detail/?m=".base64_encode($merchant_data->merchant_id)).'">'.$search->offer_title.'</a></td></tr></table></div><div class="value"><p>$'.$search->price.' <span>(Value $'.$search->price_normally.')</span></p></div><div class="book"><a href="'.base_url("booking/index/?oid=".base64_encode($search->offer_id)).'"><img src="'.$this->config->item('base_url').'public/images/book_btn.jpg" alt=""></a>';
					
					if($how_many_left>0)
					{
						$return_str .= '<div class="only_left">Only '.$how_many_left.' left</div>';
					}
					$return_str .= '</div><div class="remove"><div class="share"><p>Share</p><span class="nav_social"><a target="_blank" href="http://www.facebook.com/sharer.php?u='.$this->offers->get_offer_url($search->offer_id).'&t=<'.urlencode($search->offer_title).'"><img src="'.$this->config->item('base_url').'public/images/cus_fb.png" alt="" /></a><a target="_blank" href="https://twitter.com/home?status='.urlencode($search->offer_title).'%20-%20'.$this->offers->get_offer_url($search->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_twitter.png" alt="" /></a><a target="_blank" href="https://plus.google.com/share?url='.$this->offers->get_offer_url($search->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_g+.png" alt="" /></a></span></div><a class="remove_s" id="anc_removefrom_shortlist_'.$search->offer_id.'" href="javascript:void(0);" onclick="removefrom_shortlist('.$search->offer_id.');" ';
					
					if($this->offers->check_offer_inshortlist($search->offer_id))
					{
						$return_str .= ' style="display:block;" ';
					} 
					else 
					{ 
						$return_str .= ' style="display:none;" ';
					} 

					$return_str .= ' >Remove</a><a class="add_s" id="anc_addto_shortlist_'.$search->offer_id.'" href="javascript:void(0);" onclick="addto_shortlist('.$search->offer_id.');" ';
					
					if($this->offers->check_offer_inshortlist($search->offer_id))
					{
						$return_str .= ' style="display:none;" ';
					} 
					else
					{ 
						$return_str .= ' style="display:block;" ';
					} 
					$return_str .= ' >Shortlist</a></div></li>';
				}
				$return_str .= ' </ul> ';
			} 
			else
			{ 
				$return_str .= '<p class="no_record">No records found</p>';
			} 
        $return_str .= '</div></div>';
		echo $return_str;
	}

	public function ajaxofferslookup()
	{
		$what = $_GET['what'];
		$explode_what = explode(" ", $what);
		$sess_user_data = $this->session->all_userdata();
		if(isset($sess_user_data['user_city_id']))
		{
			$user_city_name = $sess_user_data['user_city_name'];
			$user_city_id = $sess_user_data['user_city_id'];
		}
		else
		{
			$default_city_query = $this->db->get_where('city', array('default_city' => '1'));
			$default_city_data = $default_city_query->result();
			$user_city_name = $default_city_data[0]->city_name;
			$user_city_id = $default_city_data[0]->id;
		}

		$sql_search = "SELECT DISTINCT b.business_name FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id WHERE 1 = 1 ";

		$sql_part = "";
		for($i=0; $i<=count($explode_what)-1; $i++)
		{
			$sql_part .= " a.offer_title like '%".$explode_what[$i]."%' OR ";
			$sql_part .= " b.business_name like '%".$explode_what[$i]."%' OR ";
		}
		if($sql_part != "")
		{
			$sql_part = substr($sql_part, 0, -3);
			$sql_search .= " AND ( ".$sql_part." ) "; 
		}
		
		$today = date("Y-m-d");
		$todayplusseven = date("Y-m-d", strtotime("+7 day"));
		$sql_search .= " AND 
		(
			(offer_running_from <= '".$today."' AND a.offer_running_to >= '".$todayplusseven."') OR 
			(offer_running_from >= '".$today."' AND a.offer_running_to >= '".$todayplusseven."' AND offer_running_from <= '".$todayplusseven."') OR 
			(offer_running_from <= '".$today."' AND a.offer_running_to <= '".$todayplusseven."' AND offer_running_to >= '".$today."') OR 
			(offer_running_from >= '".$today."' AND a.offer_running_to <= '".$todayplusseven."') 
		)	
		" ;
		
		$sql_search .= " AND b.city = '".$user_city_id."' ";
		$sql_search .= " ORDER BY b.business_name ASC ";
		
		//echo $sql_search;
		
		$query = $this->db->query($sql_search);
		foreach($query->result() as $this_offer)
		{
			//echo $this_offer->offer_title."###".$this_offer->offer_title."|";
			echo $this_offer->business_name."###".$this_offer->business_name."|";
		}
	}
	
	public function addtoshortlist()
	{
		$oid = $_POST['oid'];
		$session_id = $this->session->userdata('session_id');
		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];

		$sql_count = "SELECT id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND offer_id = '".$oid."'  AND session_id = '".$session_id."' ";
		$query_count = $this->db->query($sql_count);
		$count = $query_count->num_rows(); 
		if($count==0)
		{
			$sql_insert = "INSERT INTO temp_shortlists SET ip_add = '".$ip_add."', offer_id = '".$oid."', session_id = '".$session_id."' ";
			$query_insert = $this->db->query($sql_insert);
			echo "added";
		}
	}

	public function removefromshortlist()
	{
		$oid = $_POST['oid'];
		$session_id = $this->session->userdata('session_id');
		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];

		$sql_count = "SELECT id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND offer_id = '".$oid."'  AND session_id = '".$session_id."' ";
		$query_count = $this->db->query($sql_count);
		$count = $query_count->num_rows(); 
		if($count>0)
		{
			$sql_insert = "DELETE FROM temp_shortlists WHERE ip_add = '".$ip_add."' AND offer_id = '".$oid."'  AND session_id = '".$session_id."' ";
			$query_insert = $this->db->query($sql_insert);
			echo "removed";
		}
	}
	function _404(){
		$this->load->view("home/my404_view");
	}

	public function removeshortlisthome()
	{
		$oid = $_REQUEST['oid'];
		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];

		$sql_count = "SELECT id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND offer_id = '".$oid."'  AND session_id = '".$session_id."' ";
		$query_count = $this->db->query($sql_count);
		$count = $query_count->num_rows(); 
		if($count>0)
		{
			$sql_delete = "DELETE FROM temp_shortlists WHERE ip_add = '".$ip_add."' AND offer_id = '".$oid."'  AND session_id = '".$session_id."' ";
			$query_delete = $this->db->query($sql_delete);
		}
		$user_id = $this->CI_auth->logged_id();
		if($user_id != '')
		{
			$sql_count = "SELECT sid FROM user_shortlists WHERE user_id = '".$user_id."'  AND offer_id = '".$oid."' ";
			$query_count = $this->db->query($sql_count);
			$count = $query_count->num_rows(); 
			if($count>0)
			{
				$sql_delete_1 = "DELETE FROM user_shortlists WHERE user_id = '".$user_id."'  AND offer_id = '".$oid."' ";
				$query_delete_1 = $this->db->query($sql_delete_1);
			}	
		}
		redirect('/home/myshortlists', 'refresh');	
	}

	public function changereadstatus()
	{
		$id = $_POST['id'];
		//print_r($_POST);
		$sess_user_data = $this->session->all_userdata();
		//echo $sess_user_data['user_type'];
		if($sess_user_data['user_type']=='1')
		{
			$sql_status = "SELECT customer_read_status FROM user_notifications WHERE id = '".$id."'";
			$exe_status = $this->db->query($sql_status);
			$data_status = $exe_status->result();
			$pre_read_status = $data_status[0]->customer_read_status;
			if($pre_read_status == '0')
			{
				$new_read_status = '1';
				$return_message = 'Mark as unread';
			}
			else
			{
				$new_read_status = '0';
				$return_message = 'Mark as Read';
			}
			$sql_update = "UPDATE user_notifications SET customer_read_status = '".$new_read_status."' WHERE id = '".$id."'";
			$exe_update = $this->db->query($sql_update);
			echo $return_message;
		}
		else
		{
			$sql_status = "SELECT merchant_read_status FROM user_notifications WHERE id = '".$id."'";
			$exe_status = $this->db->query($sql_status);
			$data_status = $exe_status->result();
			$pre_read_status = $data_status[0]->merchant_read_status;
			//print_r($data_status);
			if($pre_read_status == '0')
			{
				$new_read_status = '1';
				$return_message = 'Mark as unread';
			}
			else
			{
				$new_read_status = '0';
				$return_message = 'Mark as Read';
			}
			$sql_update = "UPDATE user_notifications SET merchant_read_status = '".$new_read_status."' WHERE id = '".$id."'";
			$exe_update = $this->db->query($sql_update);
			echo $return_message;
		}
	}

		public function makenotificationrequest()
		{
			$user_notifications_data = $this->users->get_user_notification_data();
			$unread_notifications = $user_notifications_data['unread'];
			$notifications = $user_notifications_data['notifications_data'];
			$sess_user_data = $this->session->all_userdata();

			if($sess_user_data['user_type']=='1')
			{
				$notifications_page_url = base_url("customeraccount/mynotifications/");
			} 
			else 
			{
				$notifications_page_url = base_url("merchantaccount/mynotifications/");
			} 

			if($unread_notifications>0)
			{
				$return_str = '<a href="'.$notifications_page_url.'" class="comment_nav"><font id="num_unreadnotify">'.$unread_notifications.'</font><span>Notification</span></a>';
			}
			else
			{
				$return_str = '<a href="'.$notifications_page_url.'" class="comment_nav_inactive"><span></span><font id="num_unreadnotify">&nbsp;</font></a>';
			}
			$return_str .= '<div class="notification"><div class="dd_arrow_small"><img src="'.$this->config->item('base_url').'public/images/dd_arrow_small.png" alt="" /></div><h3>Notifications</h3><ul>';
			$ni=0;
			foreach($notifications as $this_notification)
			{
				$ni++;
				if($sess_user_data['user_type']=='1')
				{
					$read_status = $this_notification->customer_read_status;
				}
				else
				{
					$read_status = $this_notification->merchant_read_status;
				}
				$return_str .= '<li ';
				if($read_status=='0')
				{ 
					$return_str .=  'class="active"'; 
				} 
				$return_str .= ' > ';
				if($this_notification->profile_message == '1')
				{
					$return_str .= '<div class="left_note"><img src="'.$this->config->item('base_url').'public/images/notification.png" alt="" /></div>';
				} 
				else 
				{ 
					$return_str .= '<div class="left_note"><div class="calender">23</div></div>';
				} 
				$return_str .= '<div class="right_note">'.$this_notification->message.'</div><div class="date_note">'.$this->customclass->_ago(strtotime($this_notification->added_date)).'  / <span ';
				if($read_status=='1')
				{ 
					$return_str .= ' class="normal" '; 
				} 
				else 
				{ 
					$return_str .= ' class="bold" '; 
				} 
				$return_str .= ' onclick="change_read_status('.$this_notification->id.');" id="span_notification_'.$this_notification->id.'" >Mark as ';
				if($read_status=='1')
				{ 
					$return_str .= ' unread '; 
				} 
				else 
				{ 
					$return_str .= ' Read ';
				} 
				$return_str .= '</span> </div></li>';
				if($ni ==5) break;
			}
			$return_str .= '</ul>';
			
			if(sizeof($notifications)>5)
			{
				$return_str .= '<span class="read_more">';
				$sess_user_data = $this->session->all_userdata();

				if($sess_user_data['user_type']=='1')
				{
					$return_str .= ' <a href="'.base_url("customeraccount/mynotifications/").'">Load More</a> ';
				} 
				else 
				{
					$return_str .= ' <a href="'.base_url("merchantaccount/mynotifications/").'">Load More</a> ';
				} 
				$return_str .= ' </span>';
			}
			$return_str .= ' </div>';
			echo $return_str.'~~~~~'.$unread_notifications;
	}

	public function about()
	{
		$id = 1;
		$sql_page = "SELECT * FROM content_page WHERE page_id = '".$id."'";
		$exe_page = $this->db->query($sql_page);
		$data_page = $exe_page->result();
		$data = array('pagedata' => $data_page);

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('/home/page', $data);
	}

	public function contact()
	{
		/*$id = 2;
		$sql_page = "SELECT * FROM content_page WHERE page_id = '".$id."'";
		$exe_page = $this->db->query($sql_page);
		$data_page = $exe_page->result();
		$data = array('pagedata' => $data_page);
		$this->load->view('/home/page', $data);*/
		if($this->input->post('submit')){
			$contact_name = $this->input->post('contact_name');
			$email_address = $this->input->post('email_address');
			$contact_message = $this->input->post('contact_message');

			$subject = "Contact Us";
			/*$message = "<table>
			<tr>
				<td>Contact Name:</td>
				<td>".$contact_name."</td>
			</tr>
			<tr>
				<td>Email Address:</td>
				<td>".$email_address."</td>
			</tr>
			<tr>
				<td>Message:</td>
				<td>".$contact_message."</td>
			</tr>
			</table>";*/



			$message1 = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
				 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
				  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear Admin, <br/>The contact details of the user are</div>
				</li>

				<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
				  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Name</div>
				  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$contact_name.'</div>
				</li>
				<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
				  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Email</div>
				  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$email_address.'</div>
				</li>
				<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
				  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Message</div>
				  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$contact_message.'</div>
				</li>
				</ul>';

				
				$message = '<!DOCTYPE html>
				<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
				<head>
				<meta charset="utf-8">
				<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>ONDI FOOD</title>
				</head>
				<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
				<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
				  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
					<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
					<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
					 <img src="'.$this->config->item('base_url').'public/mailerimages/contact.jpg">
					</div>
					<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
					  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
						<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message1.'</span></div>
					</div>
				  </div>
				</div>
				</body>
				</html>
				';










			$to = "info@ondi.com";
			$this->offers->send_mail($to, $subject, $message);
			$data['success_message'] = "Thanks! We will contact you soon.";
		}
		$data['page_title'] = "Contact";

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$this->load->view('/home/contact', $data);
	}

	public function terms()
	{
		$id = 3;
		$sql_page = "SELECT * FROM content_page WHERE page_id = '".$id."'";
		$exe_page = $this->db->query($sql_page);
		$data_page = $exe_page->result();
		$data = array('pagedata' => $data_page);

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('/home/page', $data);
	}

	public function privacy()
	{
		$id = 4;
		$sql_page = "SELECT * FROM content_page WHERE page_id = '".$id."'";
		$exe_page = $this->db->query($sql_page);
		$data_page = $exe_page->result();
		$data = array('pagedata' => $data_page);

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$this->load->view('/home/page', $data);
	}


	public function faq()
	{
		$sql_faqcat = "SELECT * FROM faq_category ";
		$exe_faqcat = $this->db->query($sql_faqcat);
		$data_faqcat = $exe_faqcat->result();

		
		/*$sql_page = "SELECT a . * , b.category FROM faq a LEFT JOIN faq_category b ON a.faq_category = b.cat_id WHERE a.display_status = '1' ORDER BY b.cat_id ASC , a.display_status ASC ";
		$exe_page = $this->db->query($sql_page);
		$data_page = $exe_page->result();
		$data = array('faqdata' => $data_page, 'catdata' => $data_faqcat);*/

		$data = array('catdata' => $data_faqcat);


		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('/home/faq', $data);
	}
	
	public function thanks()
	{
		$data = array('title' => "Thanks");

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$this->load->view('/home/thanks', $data);
	}

	public function makefooteralerts()
	{
		$currentDate = time();
		$minutesdiff = 2;
		//$minutesdiff = 43200;
		$pastDate = $currentDate-(60*$minutesdiff);
		$fromDate = date("Y-m-d H:i", $pastDate);
		$toDate = date("Y-m-d H:i");
		$query = "SELECT b.offer_title, c.business_name, d.city_name FROM orders a LEFT JOIN merchant_offers b ON a.offer_id = b.id LEFT JOIN merchant_businessprofile c ON b.merchant_id = c.user_id LEFT JOIN city d ON c.city = d.id WHERE a.order_status = '1' AND ( a.order_date >= '".$fromDate."' AND a.order_date <= '".$toDate."' )";
		$exe_query = $this->db->query($query);
		$data_order = $exe_query->result();

		$return_str = '<div class="head"><span>Alert</span><span class="close"><a href="javascript:void(0);" onclick="hide_footer_notifications();" >X</a></span></div>';
		$numorder=0;
		foreach($data_order as $thisorder)
		{
			$return_str .= ' <div class="notice">Someone in <strong>'.stripslashes($thisorder->city_name).'</strong> just booked  <strong>'.stripslashes($thisorder->offer_title).'</strong> at <strong>'.stripslashes($thisorder->business_name).'</strong></div>';
			$numorder++;
		}
		if($numorder>0)
		{
			echo $return_str;
		}
		else
		{
			echo "";
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */