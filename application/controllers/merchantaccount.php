<?php
class Merchantaccount extends CI_Controller {	
		
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass'));	
		
		

		$this->load->helper(array('form', 'url'));
		//$this->load->library('encrypt');
		$this->load->database();
		$this->load->model(array('users','CI_auth','CI_encrypt', 'offers','Admin_Model'));
						
		if($this->CI_auth->check_logged()===FALSE)
		{			
			$this->CI_auth->redirect_nonlogged_user();

		}else{

			$sess_user_data = $this->session->all_userdata();
			if($sess_user_data['user_type']!='2' && $sess_user_data['user_type']!='3')
			{
				$this->CI_auth->redirect_nonlogged_user();				
			}
			
		}	
		
		
	}
	public function index()
	{		
		
	}
	public function contactinfo()
	{	
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			if(!in_array("1",$data['logged_user_access_sections']))
			{
				redirect('/merchantaccount/welcomeuser', 'refresh');	
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */


		
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();
		$user_id = $this->users->get_main_merchant_id();


		$this->db->select('position_id, position_value');
		$condition=array('delete_status' => '0'); //  Note: Zero is for active, 1 is for deleted
		$this->db->where($condition);
		$positionquery = $this->db->get('user_position');
		$data['positiondata'] = $positionquery->result();		
		
		$data['title'] = 'Services on demand - Ondi.com';	
		
		$this->db->select('email, position, position_other, phone, contact_name, terms_condition, merchant_steps_completed');
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		$data['user_data'] = $query->result();		
		$merchant_steps_completed=$data['user_data'][0]->merchant_steps_completed;
		

		$this->form_validation->set_rules('contact_name', 'Contact Name', 'trim|required');
		$this->form_validation->set_rules('position', 'Position', 'trim|required');		
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');	
		//$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|matches[confirm_email]');		
		//$this->form_validation->set_rules('confirm_email', 'Confirm Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('terms_condition', 'Terms & Condition', 'trim|required');
		

		if ($this->form_validation->run() == FALSE) {

			$this->load->view('/merchantaccount/contactinfo', $data);
		}
		else{

			
			if($this->input->post('contact_action')){
				
				$contact_name = $this->input->post('contact_name');
				$position = $this->input->post('position');
				$position_other = $this->input->post('position_other');
				$email = $this->input->post('email');
				$phone = $this->input->post('phone');
				
				$terms_condition = $this->input->post('terms_condition');
				
				if($merchant_steps_completed==0)
				{
					$data = array(
						   'contact_name' => $contact_name,
						   'position' => $position,
						   'position_other' => $position_other,
						   'email' => $email,
						   'phone' => $phone,
						   'terms_condition' => $terms_condition,
						   'merchant_steps_completed' => '1'
							);

				}else{

					$data = array(
					   'contact_name' => $contact_name,
					   'position' => $position,
					   'position_other' => $position_other,
					   'email' => $email,
					   'phone' => $phone,
					   'terms_condition' => $terms_condition
					);

				}
				
				//print_r($data);
				//die;
				$this->db->where('user_id', $user_id);
				$this->db->update('users', $data);
				
				$user_steps_completed = $this->users->user_steps_completed($user_id);
				
				if($user_steps_completed>2)
				{
					redirect('/merchantaccount/viewcontactinfo', 'refresh');
				}
				else
				{
					redirect('/merchantaccount/businessprofile', 'refresh');
				}
			}	
			

		}	

	}
	public function businessprofile()
	{
								
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();		
		/* USER ACCESS TYPE CHECK DATA END */
				
		$user_id=''; // hard coded as of now
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		
		$this->db->select('merchant_steps_completed');
		$merchant_steps_completed_query = $this->db->get_where('users', array('user_id' => $user_id));
		$data_merchant_steps_completed = $merchant_steps_completed_query->result();
		$merchant_steps_completed = $data_merchant_steps_completed[0]->merchant_steps_completed;
		if($merchant_steps_completed == '0')
		{
			redirect('/merchantaccount/contactinfo', 'refresh');
		}

		$this->db->select('id, city_name');
		$cityquery = $this->db->get('city');

		
		$this->db->select('state, state_id');
		$statequery = $this->db->get('states');

		//$this->db->select('type_id, type_name');
		//$business_types = $this->db->get('business_types');
		$sql_business_types = "SELECT * FROM business_types WHERE (display_status = '1' OR  merchant_id = '".$user_id."') and delete_status = '0' ORDER BY type_name ASC ";
		$business_types = $this->db->query($sql_business_types);
		//$business_types = $query_business_types->result();


		$this->db->select('pg_id, pg_name');
		$this->db->order_by("pg_id", "desc");
		$price_guide_list_options = $this->db->get('price_guide');
		
		

		$this->db->select('*');
		$userquery_check = $this->db->get_where('merchant_businessprofile', array('user_id' => $user_id));
		$bt_check = $userquery_check->result();
		if(sizeof($bt_check)==0)
		{
			$ins_mp = array('user_id'=>$user_id);
			$this->db->insert('merchant_businessprofile', $ins_mp);		
		}

		$this->db->select('*');
		$userquery = $this->db->get_where('merchant_businessprofile', array('user_id' => $user_id));
		//echo $this->db->last_query(); die;

		//$this->db->select('*');
		//$userquery = $this->db->get('merchant_businessprofile');

		//$this->db->select('*');
		//$opening_hours_query = $this->db->get_where('merchant_opening_hours', array('user_id' => $user_id)); 
		
		$condition=array('a.user_id' => $user_id);
		$this->db->select('a.*');
		$this->db->from('merchant_opening_hours as a');
		$this->db->join('days as b', 'b.day_name = a.day', 'left');
		//$this->db->order_by("b.day_display_order", "asc");
		$this->db->order_by('a.time_group ASC, b.day_display_order asc, a.id asc'); 
		$this->db->where($condition);
		$opening_hours_query = $this->db->get();
		//echo $this->db->last_query(); die;
		//$opening_hours_querydata = $opening_hours_query->result();
		//print_r($opening_hours_querydata); die;




		/*$bt = $userquery->result();
		$cat_array = explode(",", $bt[0]->business_type);
		$this->db->select('service_id, service');
		$this->db->where_in('business_type', $cat_array);
		$servicesquery = $this->db->get('services');
		$dataservices = $servicesquery->result();*/

		$bt = $userquery->result();
		$cat_array = explode(",", $bt[0]->business_type);
		//$this->db->select('id, option_value');
		//$this->db->where_in('business_type', $cat_array);
		//$servicesquery = $this->db->get('business_types_options');
		//echo $this->db->last_query(); die;
		//$dataservices = $servicesquery->result();
		
		
		if($bt[0]->business_type != '')
		{
			$query_services = "SELECT id, option_value FROM business_types_options WHERE (business_type IN (".$bt[0]->business_type.") AND display_status = 1) OR ( merchant_id = '".$user_id."' AND display_status = 0) ";
			$servicesquery = $this->db->query($query_services);
			$dataservices = $servicesquery->result();
		}


		$service_offered_array = explode(",", $bt[0]->service_offered);
		$this->db->select('id, option_value');
		$this->db->where_in('id', $service_offered_array);
		$service_offered_query = $this->db->get('business_types_options');
		if((bool)$this->config->item('test_mode'))
		{
			//echo $this->db->last_query(); die;
		}
		$dataservicesoffered = $service_offered_query->result();


		$perfect_for_array = explode(",", $bt[0]->perfect_for);
		$this->db->select('pf_id, pf_name');
		$this->db->where_in('pf_id', $perfect_for_array);
		$perfect_for_query = $this->db->get('perfect_for');
		//echo $this->db->last_query(); die;
		$dataperfectfor = $perfect_for_query->result();
		//print_r($dataperfectfor);




		$this->db->select('type_name');
		$this->db->where_in('type_id', $cat_array);
		$businesstypesqueryjk = $this->db->get('business_types');
		$databusinesstypesqueryjk = $businesstypesqueryjk->result();



		/*$this->db->select('pf_id, pf_name');
		$condition=array('delete_status' => '0'); //  Note: Zero is for active, 1 is for deleted
		$this->db->where($condition);
		$perfect_for_list_options = $this->db->get('perfect_for');*/

		if($bt[0]->business_type != '')
		{
			$query_pf = "SELECT * FROM perfect_for WHERE (business_type IN (".$bt[0]->business_type.") AND delete_status = 0) OR pf_name='N/A' ";
			$pfquery = $this->db->query($query_pf);
			$datapf = $pfquery->result();
		}


		if($logged_user_access_type=='3')
		{

			$data = array('city_options' => $cityquery->result(), 'state_options' => $statequery->result(), 'userdata' => $userquery->result(), 'business_types' => $business_types->result(), 'price_guide_list_options' => $price_guide_list_options->result(), 'perfect_for_list_options' => $datapf, 'title' => 'Services on demand - Ondi.com', 'opening_hours_query' => $opening_hours_query->result(), 'dataservices' => $dataservices, 'logged_user_access_type' =>$logged_user_access_type, 'logged_user_access_sections'=>$this->users->get_user_access_sections(), 'databusinesstypesqueryjk' => $databusinesstypesqueryjk, 'dataservicesoffered' => $dataservicesoffered, 'dataperfectfor' => $dataperfectfor);

		}else{

			$data = array('city_options' => $cityquery->result(), 'state_options' => $statequery->result(), 'userdata' => $userquery->result(), 'business_types' => $business_types->result(), 'price_guide_list_options' => $price_guide_list_options->result(), 'perfect_for_list_options' => $datapf, 'title' => 'Services on demand - Ondi.com', 'opening_hours_query' => $opening_hours_query->result(), 'dataservices' => $dataservices, 'logged_user_access_type' =>$logged_user_access_type, 'logged_user_access_sections'=>array(), 'databusinesstypesqueryjk' => $databusinesstypesqueryjk, 'dataservicesoffered' => $dataservicesoffered, 'dataperfectfor' => $dataperfectfor);	

		}
			
		
		$this->form_validation->set_rules('business_name', 'Business Name', 'trim|required');
		$this->form_validation->set_rules('website_address', 'Website Address', 'trim|required');
		$this->form_validation->set_rules('street_address', 'Street Address', 'trim|required');
		$this->form_validation->set_rules('suburb', 'Suburb', 'trim|required');
		$this->form_validation->set_rules('post_code', 'Post Code', 'trim|required');
		$this->form_validation->set_rules('city', 'City', 'trim|required');
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('business_phone', 'Business phone', 'trim|required');
		$this->form_validation->set_rules('price_guide', 'Price Guide', 'trim|required');
		//$this->form_validation->set_rules('perfect_for', 'Perfect For', 'trim|required');
		
		/*Check if the form passed its validation */
		if ($this->form_validation->run() == FALSE) {
			
			$this->load->view('/merchantaccount/businessprofile', $data);
		}
		else {
			if($this->input->post('submit')){
				//print_r($_POST);
				//print_r($_FILES);
				//exit();
				$business_name = $this->input->post('business_name');
				$website_address = $this->input->post('website_address');
				$street_address = $this->input->post('street_address');
				$suburb = $this->input->post('suburb');
				$post_code = $this->input->post('post_code');
				$city = $this->input->post('city');
				$state = $this->input->post('state');
				$business_phone = $this->input->post('business_phone');
				$mobile_number = $this->input->post('mobile_number');
				$facebook = $this->input->post('facebook');
				$twitter = $this->input->post('twitter');
				$linkedin = $this->input->post('linkedin');
				$googleplus = $this->input->post('googleplus');
				$business_type = $this->input->post('choosen_business_types');
				
				



				// INSERT ADDITIONAL CATEGORIES IF ANY /////
				$num_category = $this->input->post('num_category');
				for($nso = 1; $nso<=$num_category; $nso++)
				{
					$additional_business_category = $this->input->post('additional_cat_'.$nso);
					if(trim($additional_business_category) != '')
					{
						$sql_count_duplicate = "SELECT type_id FROM business_types WHERE (type_name = '".$additional_business_category."' AND display_status = '1') OR ( type_name = '".$additional_business_category."' AND merchant_id = '".$user_id."' )";
						$query_count_duplicate = $this->db->query($sql_count_duplicate);
						$count_duplicate = $query_count_duplicate->num_rows();
						if($count_duplicate==0)
						{						
							$insert_add_cat = array('type_name'=>$additional_business_category, 'display_status' => '0', 'merchant_id' => $user_id);
							$this->db->insert('business_types', $insert_add_cat);	
							$newcategoryid = $this->db->insert_id();

							//$emailmessage = "A new category ".$additional_business_category." added. Kindly approve from admin panel.";
							
							
							$message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
							 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear Admin, <br/>A new category '.$additional_business_category.' added. Kindly approve from admin panel.</div>
							</li>
							</ul>';

							
							$emailmessage = '<!DOCTYPE html>
							<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
							<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<title>ONDI FOOD</title>
							</head>
							<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
							<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
							  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
								<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
								<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
								 <img src="'.$this->config->item('base_url').'public/mailerimages/admin_notification.jpg">
								</div>
								<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
								  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
									<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message.'</span></div>
								</div>
							  </div>
							</div>
							</body>
							</html>
							';
							
							
							
							
							
							
							$this->offers->send_mail("info@ondi.com", "New Category Added", $emailmessage);
							//$this->offers->send_mail("jitendra.revolute@gmail.com", "New Category Added", $emailmessage);

							if($business_type != '')
							{
								$business_type = $business_type.",".$newcategoryid;
							}
							else
							{
								$business_type = $newcategoryid;
							}
						}
						else
						{
							$data_check_cat = $query_count_duplicate->result();
							$type_id = $data_check_cat[0]->type_id;

							if($business_type != '')
							{
								$explode_business_category = explode(",", $business_type);
								if (!in_array($type_id, $explode_business_category)) {
									$business_type = $business_type.",".$type_id;
								}
							}
							else
							{
								$business_type = $type_id;
							}
						}
					}
				}
				// INSERT ADDITIONAL CATEGORIES IF ANY /////

				$old_profile_picture = $this->input->post('old_profile_picture');
				$old_supporting_picture = $this->input->post('old_supporting_picture');
				$old_widescreen_picture = $this->input->post('old_widescreen_picture');
				$old_pdf_menu = $this->input->post('old_pdf_menu');
				
				$service_offered = '';
				$services_offered = $this->input->post('services_offered');
				for($nso = 0; $nso<=sizeof($services_offered); $nso++)
				{
					if($services_offered[$nso] != '')
					{
						$service_offered .= trim($services_offered[$nso]).',';
					}
				}
				if($service_offered != '') $service_offered = substr($service_offered, 0, -1);
				
				// INSERT ADDITIONAL SERVICES OFFERED IF ANY /////
				$num_so = $this->input->post('num_so');
				for($nso = 1; $nso<=$num_so; $nso++)
				{
					$additional_so = $this->input->post('additional_so_'.$nso);
					if(trim($additional_so) != '')
					{
						$sql_count_duplicate_so = "SELECT id FROM business_types_options WHERE (option_value = '".$additional_so."' AND display_status = '1') OR ( option_value = '".$additional_so."' AND merchant_id = '".$user_id."' )";
						$query_count_duplicate_so = $this->db->query($sql_count_duplicate_so);
						$count_duplicate_so = $query_count_duplicate_so->num_rows();
						if($count_duplicate_so==0)
						{						
							$insert_add_cat = array('option_value'=>$additional_so, 'display_status' => '0', 'merchant_id' => $user_id);
							$this->db->insert('business_types_options', $insert_add_cat);	
							$newsoid = $this->db->insert_id();

							//$emailmessage = "A new service offered ".$additional_so." added. Kindly approve from admin panel.";

							$message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
							 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear Admin, <br/>A new service offered '.$additional_so.' added. Kindly approve from admin panel.</div>
							</li>
							</ul>';

							
							$emailmessage = '<!DOCTYPE html>
							<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
							<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<title>ONDI FOOD</title>
							</head>
							<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
							<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
							  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
								<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
								<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
								 <img src="'.$this->config->item('base_url').'public/mailerimages/admin_notification.jpg">
								</div>
								<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
								  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
									<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message.'</span></div>
								</div>
							  </div>
							</div>
							</body>
							</html>
							';


							$this->offers->send_mail("info@ondi.com", "New service offered added", $emailmessage);
							//$this->offers->send_mail("jitendra.revolute@gmail.com", "New service offered added", $emailmessage);

							if($service_offered != '')
							{
								$service_offered = $service_offered.",".$newsoid;
							}
							else
							{
								$service_offered = $newsoid;
							}
						}
					}
				}
				// INSERT ADDITIONAL SERVICES OFFERED IF ANY /////




				$price_guide = $this->input->post('price_guide');
				$perfect_for = $this->input->post('perfect_for');

				$confirmation_methods_array = $this->input->post('confirmation_methods');
				if(is_array($confirmation_methods_array))
				{
					$confirmation_methods = implode(",", $confirmation_methods_array);
				}
				else
				{
					$confirmation_methods = '';
				}

				$array_perfect_for = $this->input->post('perfect_for');
				if(is_array($array_perfect_for))
				{
					$perfect_for = implode(",", $array_perfect_for);
				}
				else
				{
					$perfect_for = '';
				}
				
				$sql_cityname = "SELECT city_name FROM city WHERE id ='".$city."' ";
				$exe_cityname = $this->db->query($sql_cityname);
				$data_cityname = $exe_cityname->result();
				$city_name = $data_cityname[0]->city_name;

				$sql_statename = "SELECT state FROM states WHERE state_id ='".$state."' ";
				$exe_statename = $this->db->query($sql_statename);
				$data_statename = $exe_statename->result();
				$state_name = $data_statename[0]->state;

				$about_business = $this->input->post('about_business');
				$parking_details = $this->input->post('parking_details');
				$street_address_map = str_replace(" ", "+", $street_address);
				$suburb_map = str_replace(" ", "+", $suburb);
				$post_code_map = str_replace(" ", "+", $post_code);
				$city_name_map = str_replace(" ", "+", $city_name);
				$state_name_map = str_replace(" ", "+", $state_name);

				$url='http://maps.googleapis.com/maps/api/geocode/json?address='.$street_address_map.'+'.$suburb_map.'+'.$post_code_map.'+'.$city_name_map.'+'.$state_name_map.'&sensor=false';
				//$source = file_get_contents($url);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
				$response = curl_exec($ch);
				curl_close($ch);
				$obj = json_decode($response);

				
				if((bool)$this->config->item('test_mode'))
				{
					//echo $url;
					//print_r($response); die;
				}
				
				
				if(isset($obj))
				{
					$lat = $obj->results[0]->geometry->location->lat;
					$long = $obj->results[0]->geometry->location->lng;
				}
				else
				{
					$lat = "-35.283333";
					$long = "149.216667";
				}




				/*
				
				$config_image['upload_path'] = 'public/uploads/';
				$config_image['allowed_types'] = 'gif|jpg|png';
				$config_image['max_size']	= '1000';
				$config_image['max_width']  = '500';
				$config_image['max_height']  = '400';
				$this->load->library('upload', $config_image);
				$this->upload->initialize($config_image);
				$this->upload->set_allowed_types('*');
				
				if ($this->upload->do_upload('profile_picture')) {
					$data_profile_picture = $this->upload->data();
					$image_profile_picture = $data_profile_picture['file_name'];
					$profile_picture = $config_image['upload_path'].$data_profile_picture['file_name'];
				}
				else
				{
					$profile_picture = $old_profile_picture;
					//$profile_picture = '';
				}

				if ($this->upload->do_upload('supporting_picture')) {
					$data_supporting_picture = $this->upload->data();
					$image_supporting_picture = $data_supporting_picture['file_name'];
					$supporting_picture = $config_image['upload_path'].$data_supporting_picture['file_name'];
				} 
				else
				{
					$supporting_picture = $old_supporting_picture;
					//$supporting_picture = '';
				}

				if ($this->upload->do_upload('widescreen_picture')) {
					$data_widescreen_picture = $this->upload->data();
					$image_widescreen_picture = $data_widescreen_picture['file_name'];
					$widescreen_picture = $config_image['upload_path'].$data_widescreen_picture['file_name'];
				}
				else
				{
					$widescreen_picture = $old_widescreen_picture;
					//$supporting_picture = '';
				}


				*/
				
				$temp_profile_picture = $this->input->post('temp_profile_picture');
				$temp_supporting_picture = $this->input->post('temp_supporting_picture');
				$temp_widescreen_picture = $this->input->post('temp_widescreen_picture');

				$old_profile_picture = $this->input->post('old_profile_picture');
				$old_supporting_picture = $this->input->post('old_supporting_picture');
				$old_widescreen_picture = $this->input->post('old_widescreen_picture');

				if($temp_profile_picture != "")
				{
					$sql_temp_pp = "SELECT * FROM temp_images WHERE id ='".$temp_profile_picture."' ";
					$exe_temp_pp = $this->db->query($sql_temp_pp);
					$count_temp_pp = $exe_temp_pp->num_rows();
					if($count_temp_pp>0)
					{
						foreach($exe_temp_pp->result() as $tmp)
						{
							$profile_picture = $tmp->img_path;

							$sql_del= "DELETE FROM temp_images WHERE id ='".$temp_profile_picture."' ";
							$exe_del = $this->db->query($sql_del);	
							@unlink($old_profile_picture);
						}
					}
				}
				else
				{
					$profile_picture = $old_profile_picture;
				}
				
				
				if($temp_supporting_picture != "")
				{
					$sql_temp_pp = "SELECT * FROM temp_images WHERE id ='".$temp_supporting_picture."' ";
					$exe_temp_pp = $this->db->query($sql_temp_pp);
					$count_temp_pp = $exe_temp_pp->num_rows();
					if($count_temp_pp>0)
					{
						foreach($exe_temp_pp->result() as $tmp)
						{
							$supporting_picture = $tmp->img_path;
							$sql_del= "DELETE FROM temp_images WHERE id ='".$temp_supporting_picture."' ";
							$exe_del = $this->db->query($sql_del);	
							@unlink($old_supporting_picture);
						}
					}
				}
				else
				{
					$supporting_picture = $old_supporting_picture;
				}
				
				if($temp_widescreen_picture != "")
				{
					$sql_temp_pp = "SELECT * FROM temp_images WHERE id ='".$temp_widescreen_picture."' ";
					$exe_temp_pp = $this->db->query($sql_temp_pp);
					$count_temp_pp = $exe_temp_pp->num_rows();
					if($count_temp_pp>0)
					{
						foreach($exe_temp_pp->result() as $tmp)
						{
							$widescreen_picture = $tmp->img_path;
							$sql_del= "DELETE FROM temp_images WHERE id ='".$temp_widescreen_picture."' ";
							$exe_del = $this->db->query($sql_del);	
							@unlink($old_widescreen_picture);
						}
					}
				}
				else
				{
					$widescreen_picture = $old_widescreen_picture;
				}
				//die;




				$config_pdf['upload_path'] = 'public/uploads/pdf/';
				$config_pdf['allowed_types'] = 'pdf';
				$config_pdf['max_size']	= '1000';
				$this->load->library('upload', $config_pdf);
				$this->upload->initialize($config_pdf);
				$this->upload->set_allowed_types('*');
				
				if ($this->upload->do_upload('pdf_menu')) {
					$data_pdf_menu = $this->upload->data();
					//print_r($data_pdf_menu);
					$file_pdf_menu = $data_pdf_menu['file_name'];
					$pdf_menu = $config_pdf['upload_path'].$file_pdf_menu;
				} 
				else
				{
					$pdf_menu = $old_pdf_menu;
					//$pdf_menu = '';
				}
				//echo $file_pdf_menu['file_name'];

				$data_insert = array(
				   'user_id' => $user_id,
				   'business_name' => $business_name,
				   'website_address' => $website_address,
				   'street_address' => $street_address,
				   'suburb' => $suburb,
				   'post_code' => $post_code,
				   'city' => $city,
				   'state' => $state,
				   'business_phone' => $business_phone,
				   'mobile_number' => $mobile_number,
				   'facebook' => $facebook,
				   'twitter' => $twitter,
				   'linkedin' => $linkedin,
				   'googleplus' => $googleplus,
				   'business_type' => $business_type,
				   'service_offered' => $service_offered,
				   'price_guide' => $price_guide,
				   'perfect_for' => $perfect_for,
				   'confirmation_methods' => $confirmation_methods,
				   'about_business' => $about_business,
				   'parking_details' => $parking_details,
				   'profile_picture' => $profile_picture,
				   'supporting_picture' => $supporting_picture,
				   'widescreen_picture' => $widescreen_picture,
				   'pdf_menu' => $pdf_menu,
				   'lat' => $lat,
				   'long' => $long
				);
				$data_update = array(
				   'business_name' => $business_name,
				   'website_address' => $website_address,
				   'street_address' => $street_address,
				   'suburb' => $suburb,
				   'post_code' => $post_code,
				   'city' => $city,
				   'state' => $state,
				   'business_phone' => $business_phone,
				   'mobile_number' => $mobile_number,
				   'facebook' => $facebook,
				   'twitter' => $twitter,
				   'linkedin' => $linkedin,
				   'googleplus' => $googleplus,
				   'business_type' => $business_type,
				   'service_offered' => $service_offered,
				   'price_guide' => $price_guide,
				   'perfect_for' => $perfect_for,
				   'confirmation_methods' => $confirmation_methods,
				   'about_business' => $about_business,
				   'parking_details' => $parking_details,
				   'profile_picture' => $profile_picture,
				   'supporting_picture' => $supporting_picture,
				   'widescreen_picture' => $widescreen_picture,
				   'pdf_menu' => $pdf_menu,
				   'lat' => $lat,
				   'long' => $long
				);
				
				if((bool)$this->config->item('test_mode'))
				{
					//print_r($data_update); die;
				}

				$query = $this->db->get_where('merchant_businessprofile', array('user_id' => $user_id)); 
				$count= $query->num_rows(); 
				//echo $count; die;
				if($count === 0)
				{ 
					$this->db->insert('merchant_businessprofile', $data_insert);
				}
				else
				{
					$this->db->where('user_id', $user_id);
					$this->db->update('merchant_businessprofile', $data_update);
				}
				

				//print_r($_POST);
				//exit();
				for($no=1; $no<=7; $no++)
				{
					if(isset($_POST['oh_'.$no]))
					{
						$arroh = $_POST['oh_'.$no];
						$selected_days_for_this_time_group = "";
						for($jk=0; $jk<sizeof($arroh); $jk++)
						{
							$dayrecord = $arroh[$jk];
							$from = $this->input->post('from_'.$no);
							$from_ampm = $this->input->post('from_ampm_'.$no);
							$to = $this->input->post('to_'.$no);
							$to_ampm = $this->input->post('to_ampm_'.$no);
							$timinings_id = $this->input->post('timinings_id_'.$no);
							$time_group = $this->input->post('time_group_'.$no);

							if($dayrecord =='Mo') $day = "monday";
							else if($dayrecord =='Tu') $day = "tuesday";
							else if($dayrecord =='We') $day = "wednesday";
							else if($dayrecord =='Th') $day = "thursday";
							else if($dayrecord =='Fr') $day = "friday";
							else if($dayrecord =='Sa') $day = "saturday";
							else if($dayrecord =='Su') $day = "sunday";
							
							$selected_days_for_this_time_group .= "'".$day."',";

							if($time_group == "")
							{
								// INSERT ENTRY FOR NEW TIME GROUP
								$time_group = $no;
								$query_insert_oh = "INSERT INTO merchant_opening_hours SET user_id = '".$user_id."', day = '".$day."' , from_hour = '".$from."' , from_ampm = '".$from_ampm."' , to_hour = '".$to."' , to_ampm = '".$to_ampm."' , time_group = '".$time_group."' ";
								$exe_query_insert_oh = $this->db->query($query_insert_oh);



								//////////// VIKAS CODE STARTS /////////////
								$last_opening_insert_id = $this->db->insert_id();	
								$distinct_offer_id = $this->users->get_distinct_offer_id_having_moh_id();
								foreach($distinct_offer_id as $offer_val)
								{
									$offer_id = $offer_val->offer_id;
									if(!empty($offer_id))
									{
										if($update_existing_offers=='1')
										{
											//***** inserting opening hours in existing merchant_offers_available_time start *******//
											if($from_ampm=='am' || $from_ampm=='AM')
											{
												if($from==12)
												{
													$from_hours = '0000';
												}
												else
												{
													$from_hours = $from * 100;
													if($from>=1 && $from<=9)
													{
														$from_hours = '0'.$from_hours;
													}
												}
											}
											else if($from_ampm=='pm' || $from_ampm=='PM')
											{
												if($from==12)
												{
													$from_hours = '1200';
												}
												else
												{
													$from_hours = 1200 + ($from * 100);								
												}
											}						
											if($to_ampm=='am' || $to_ampm=='AM')
											{
												if($to==12)
												{
													$to_hours = '0000';
												}
												else
												{
													$to_hours = $to * 100;
													if($to>=1 && $to<=9)
													{
														$to_hours = '0'.$to_hours;
													}
												}
											}
											else if($to_ampm=='pm' || $to_ampm=='PM')
											{
												if($to==12)
												{
													$to_hours = '1200';
												}
												else
												{
													$to_hours = 1200 + ($to * 100);								
												}
											}
											$days_display_order='';
											if($day=='monday')
											{
												$days_display_order = '1';
											}
											else if($day=='tuesday')
											{
												$days_display_order = '2';
											}
											else if($day=='wednesday')
											{
												$days_display_order = '3';
											}
											else if($day=='thursday')
											{
												$days_display_order = '4';
											}
											else if($day=='friday')
											{
												$days_display_order = '5';
											}
											else if($day=='saturday')
											{
												$days_display_order = '6';
											}
											else if($day=='sunday')
											{
												$days_display_order = '7';
											}
											$available_time_data = array(
												'offer_id' => $offer_id,
												'merchant_id' => $user_id,
												'available_day' => $day,
												'time_from' => $from,
												'time_from_am_pm' => $from_ampm,
												'from_hours' => $from_hours,
												'time_to' => $to,
												'time_to_am_pm' => $to_ampm,
												'to_hours' => $to_hours,
												'quantity' => '1',
												'max_pax' => '',
												'added_date' => date('Y-m-d H:i:s'),
												'available_time_slots_capacity' => '3',
												'days_display_order' => $days_display_order,
												'moh_id' => $last_opening_insert_id
											);										
											if(!empty($offer_id))
											{
												$this->db->insert('merchant_offers_available_time', $available_time_data);
											}																
											//***** inserting opening hours in existing merchant_offers_available_time start *******//
										}
									}
								}
								////////// VIKAS CODE ENDS //////////
							}
							else
							{
								// UPDATE ENTRY FOR OLD TIME GROUP
								$query_select_db_oh = "SELECT id FROM merchant_opening_hours WHERE time_group = '".$time_group."' AND user_id = '".$user_id."' AND  day = '".$day."' ";
								$exe_query_select_db_oh = $this->db->query($query_select_db_oh);
								$data_query_select_db_oh = $exe_query_select_db_oh->result();
								$entry_id = $data_query_select_db_oh[0]->id;
								if($entry_id != '')
								{
									$query_update_oh = "UPDATE merchant_opening_hours SET from_hour = '".$from."' , from_ampm = '".$from_ampm."' , to_hour = '".$to."' , to_ampm = '".$to_ampm."'  WHERE time_group = '".$time_group."' AND user_id = '".$user_id."' AND  day = '".$day."' ";
									$exe_query_update_oh = $this->db->query($query_update_oh);
									/////////  VIKAS CODE STARTS ///////////
									//*************************** updating existing merchant_offers_available_time start ***************************//
									if($from_ampm=='am' || $from_ampm=='AM')
									{
										if($from==12)
										{
											$from_hours = '0000';
										}
										else
										{
											$from_hours = $from * 100;
											if($from>=1 && $from<=9)
											{
												$from_hours = '0'.$from_hours;
											}
										}
									}
									else if($from_ampm=='pm' || $from_ampm=='PM')
									{
										if($from==12)
										{
											$from_hours = '1200';
										}
										else
										{
											$from_hours = 1200 + ($from * 100);								
										}
									}						
									if($to_ampm=='am' || $to_ampm=='AM')
									{
										if($to==12)
										{
											$to_hours = '0000';
										}
										else
										{
											$to_hours = $to * 100;
											if($to>=1 && $to<=9)
											{
												$to_hours = '0'.$to_hours;
											}
										}
									}
									else if($to_ampm=='pm' || $to_ampm=='PM')
									{
										if($to==12)
										{
											$to_hours = '1200';

										}
										else
										{
											$to_hours = 1200 + ($to * 100);								
										}
									}
									$available_time_data = array(							
										'time_from' => $from,
										'time_from_am_pm' => $from_ampm,
										'from_hours' => $from_hours,
										'time_to' => $to,
										'time_to_am_pm' => $to_ampm,
										'to_hours' => $to_hours,
										'added_date' => date('Y-m-d H:i:s')							
									);
									$this->db->where(array('merchant_id'=>$user_id, 'available_time_slots_capacity'=>3, 'moh_id'=>$timinings_id ));
									$this->db->update('merchant_offers_available_time', $available_time_data);							
									//*************************** updating existing merchant_offers_available_time end ***************************//
									/////////   VIKAS CODE ENDS //////////////
								}
								else
								{
									$query_insert_oh = "INSERT INTO merchant_opening_hours SET user_id = '".$user_id."', day = '".$day."' , from_hour = '".$from."' , from_ampm = '".$from_ampm."' , to_hour = '".$to."' , to_ampm = '".$to_ampm."' , time_group = '".$time_group."' ";
									$exe_query_insert_oh = $this->db->query($query_insert_oh);
									//////////// VIKAS CODE STARTS /////////////
									$last_opening_insert_id = $this->db->insert_id();	
									$distinct_offer_id = $this->users->get_distinct_offer_id_having_moh_id();
									foreach($distinct_offer_id as $offer_val)
									{
										$offer_id = $offer_val->offer_id;
										if(!empty($offer_id))
										{
											if($update_existing_offers=='1')
											{
												//***** inserting opening hours in existing merchant_offers_available_time start *******//
												if($from_ampm=='am' || $from_ampm=='AM')
												{
													if($from==12)
													{
														$from_hours = '0000';
													}
													else
													{
														$from_hours = $from * 100;
														if($from>=1 && $from<=9)
														{
															$from_hours = '0'.$from_hours;
														}
													}
												}
												else if($from_ampm=='pm' || $from_ampm=='PM')
												{
													if($from==12)
													{
														$from_hours = '1200';
													}
													else
													{
														$from_hours = 1200 + ($from * 100);								
													}
												}						
												if($to_ampm=='am' || $to_ampm=='AM')
												{
													if($to==12)
													{
														$to_hours = '0000';
													}
													else
													{
														$to_hours = $to * 100;
														if($to>=1 && $to<=9)
														{
															$to_hours = '0'.$to_hours;
														}
													}
												}
												else if($to_ampm=='pm' || $to_ampm=='PM')
												{
													if($to==12)
													{
														$to_hours = '1200';
													}
													else
													{
														$to_hours = 1200 + ($to * 100);								
													}
												}
												$days_display_order='';
												if($day=='monday')
												{
													$days_display_order = '1';
												}
												else if($day=='tuesday')
												{
													$days_display_order = '2';
												}
												else if($day=='wednesday')
												{
													$days_display_order = '3';
												}
												else if($day=='thursday')
												{
													$days_display_order = '4';
												}
												else if($day=='friday')
												{
													$days_display_order = '5';
												}
												else if($day=='saturday')
												{
													$days_display_order = '6';
												}
												else if($day=='sunday')
												{
													$days_display_order = '7';
												}
												$available_time_data = array(
													'offer_id' => $offer_id,
													'merchant_id' => $user_id,
													'available_day' => $day,
													'time_from' => $from,
													'time_from_am_pm' => $from_ampm,
													'from_hours' => $from_hours,
													'time_to' => $to,
													'time_to_am_pm' => $to_ampm,
													'to_hours' => $to_hours,
													'quantity' => '1',
													'max_pax' => '',
													'added_date' => date('Y-m-d H:i:s'),
													'available_time_slots_capacity' => '3',
													'days_display_order' => $days_display_order,
													'moh_id' => $last_opening_insert_id
												);										
												if(!empty($offer_id))
												{
													$this->db->insert('merchant_offers_available_time', $available_time_data);
												}																
												//***** inserting opening hours in existing merchant_offers_available_time start *******//
											}
										}
									}
									////////// VIKAS CODE ENDS //////////
								}
							}
						}
						// DELETE THE RECORD FROM DB IF USER NOT SELECTED THE DAY FOR THIS TIME GROUP
						if($selected_days_for_this_time_group != '') 
						{
							$selected_days_for_this_time_group = substr($selected_days_for_this_time_group, 0, -1);
							$query_select_deleteable_oh = "SELECT id FROM merchant_opening_hours WHERE time_group = '".$time_group."' AND user_id = '".$user_id."' AND  day NOT IN (".$selected_days_for_this_time_group.")";
							$exe_query_select_deleteable_oh = $this->db->query($query_select_deleteable_oh);
							$data_query_select_deleteable_oh = $exe_query_select_deleteable_oh->result();
							foreach($data_query_select_deleteable_oh as $this_deleteable_oh)
							{
								$id_to_del = $this_deleteable_oh->id;
								$query_delete_oh = "DELETE FROM merchant_opening_hours WHERE id = '".$id_to_del."' ";
								$exe_qquery_delete_oh = $this->db->query($query_delete_oh);
							}
						}
					}
					else
					{
						$time_group = $_POST['time_group_'.$no];
						if($time_group != "")
						{
							$query_delete_oh = "DELETE FROM merchant_opening_hours WHERE time_group = '".$time_group."' AND user_id = '".$user_id."' ";
							$exe_qquery_delete_oh = $this->db->query($query_delete_oh);
						}
						
					}
				}

				if($merchant_steps_completed == '1')
				{
					$steps_completed_update = array(
							'merchant_steps_completed' => '2'
					);
					$this->db->where('user_id', $user_id);
					$this->db->update('users', $steps_completed_update);
					redirect('/merchantaccount/bankingdetails', 'refresh');
				}
				else
				{
					redirect('/merchantaccount/viewbusinessprofile', 'refresh');
				}
				
			}
		}


		
	}

	public function bankingdetails()
	{	
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		
		$data['title'] = 'Services on demand - Ondi.com';		
		
		// $user_id = $this->CI_auth->logged_id();	
		$user_id = $this->users->get_main_merchant_id();

		$this->db->select('branch_name, account_name, bsb, account_number, abn, merchant_steps_completed');
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		$data['user_data'] = $query->result();
		$merchant_steps_completed=$data['user_data'][0]->merchant_steps_completed;	
		

		if($merchant_steps_completed==1)
		{
			redirect('/merchantaccount/businessprofile', 'refresh');
		}

		
		$this->form_validation->set_rules('branch_name', 'Branch name', 'trim|required');
		$this->form_validation->set_rules('account_name', 'Account name', 'trim|required');		
		$this->form_validation->set_rules('bsb', 'BSB', 'trim|required');		
		$this->form_validation->set_rules('account_number', 'Account Number', 'trim|required');
		$this->form_validation->set_rules('abn', 'ABN', 'trim|required');
		

		if ($this->form_validation->run() == FALSE) {

			$this->load->view('/merchantaccount/bankingdetails', $data);
		}
		else{

			
			if($this->input->post('bank_detail_action')){
				
				$branch_name = $this->input->post('branch_name');
				$account_name = $this->input->post('account_name');
				$bsb = $this->input->post('bsb');
				$account_number = $this->input->post('account_number');
				$abn = $this->input->post('abn');
				
				
				if($merchant_steps_completed==2)
				{
					$data = array(
						   'branch_name' => $branch_name,
						   'account_name' => $account_name,
						   'bsb' => $bsb,
						   'account_number' => $account_number,
						   'abn' => $abn,
						   'merchant_steps_completed' => 3
						);
				$this->db->where('user_id', $user_id);
				$this->db->update('users', $data);
				redirect('/merchantaccount/profilecompleted', 'refresh');

				}else{

					$data = array(
						   'branch_name' => $branch_name,
						   'account_name' => $account_name,
						   'bsb' => $bsb,
						   'account_number' => $account_number,
						   'abn' => $abn
						);
				$this->db->where('user_id', $user_id);
				$this->db->update('users', $data);
				redirect('/merchantaccount/viewbankingdetails', 'refresh');

				}

			}			

		}
		

	}

	public function profilecompleted()
	{
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$data['users_data'] = $this->users->get_userslist();
		$this->load->view('/merchantaccount/profilecompleted', $data);
	}

	public function welcomeuser()
	{
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */


		$data['users_data'] = $this->users->get_userslist();
		$this->load->view('/merchantaccount/welcomeuser', $data);
	}

	public function getservices()
	{
		$bt = $this->input->post('bt');
		if($bt != '')
		{
			$cat_array = explode(",", $bt);
			$this->db->select('id, option_value');
			$this->db->where_in('business_type', $cat_array);
			$servicesquery = $this->db->get('business_types_options');
			//echo $this->db->last_query();		
			$dataservices = $servicesquery->result();
			$business_type_list = array();
			
			$strreturn = '<ul id="checkboxesgroupso">';
			foreach($dataservices as $row)  
			{
				$chk = '';
				if(in_array('.$row->id.', '.$array_service_offered.'))
				{ 
					$chk =  "checked='checked'"; 
				}
				$strreturn .= ' <li><input class="class_services_offered"  type="checkbox" value="'.$row->id.'" name="services_offered[]" '.$chk.' title="'.$row->option_value.'" onclick="change_so(this);" >'.$row->option_value.'</li>';
			}
			$strreturn .= ' </ul> ';

			if($selectedservicesoffered == '') $selectedservicesoffered = " *Please select";
			$return = '<span><a href="javascript:void(0);" onclick="uncheckall_bp(\'checkboxesgroupso\');">Clear all..</a></span>'.$strreturn.' ';
		}
		else
		{
			$return = '<ul id="checkboxesgroupso"></ul> ';
		}

		
        

		
		echo $return;
	}


	public function getperfectfor()
	{
		$bt = $this->input->post('bt');
		$sql_pf = "SELECT * FROM perfect_for WHERE delete_status = '0' AND business_type IN (".$bt.")  OR pf_name='N/A' ORDER BY pf_name ASC ";
		$exe_pf = $this->db->query($sql_pf);
		$data_pf = $exe_pf->result();
		
		
		


		/*$strreturn = '<img src="'.$this->config->item('base_url').'public/images/select_flex_arrow.png" border="0"/><div id="div_perfect_for">*Perfect For</div><div class="drop" id="perfectfor_drop3">';

		if(sizeof($data_pf)>0){
			$strreturn .= '<span><a href="javascript:void(0);" onclick="uncheckall_pf(\'checkboxesgrouppf\');">Clear all..</a></span>';
		}
		$strreturn .= '<ul id="checkboxesgrouppf">';*/
		
		foreach($data_pf as $row) 
		{
			$strreturn .= '<li><input class="class_services_offered" type="checkbox" value="'.$row->pf_id.'" name="perfect_for[]" title="'.$row->pf_name.'" onclick="change_pf(this);">'.$row->pf_name.'</li>';
		} 
		//$strreturn .= '</ul></div>';

		echo $strreturn;
	}


	public function getperfectforsearch()
	{
		$bt = $this->input->post('bt');
		$sql_pf = "SELECT * FROM perfect_for WHERE delete_status = '0' AND pf_name != 'N/A' AND business_type IN (".$bt.") ORDER BY pf_name ASC ";
		$exe_pf = $this->db->query($sql_pf);
		$data_pf = $exe_pf->result();
		$strreturn = '<li><h4><a href="javascript:void(0);" onclick="uncheckall(\'submenu_nav5\');">Clear all</a></h4></li>';
		foreach($data_pf as $row)  
		{
			$strreturn .= '<li><input type="checkbox" class="dd_check search_pf" value="'.$row->pf_id.'" name="search_pf[]" ><p>'.$row->pf_name.'</p></li>';
		}
		echo $strreturn;
	}
	
	public function userslist()
	{
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$user_id='';
		$user_id=$this->CI_auth->logged_id();

		$data['title'] = 'Services on demand - Ondi.com';
		$data['users_data'] = $this->users->get_userslist();
		$this->load->view('/merchantaccount/userslist', $data);
	}
	public function getaccessoptions()
	{		
		$access_options_arr=array();
		$access_options = $this->input->post('access_options');
		$access_options_arr = explode(",",$access_options);			
					
		$section_type_result = $this->users->get_section_type();
		$i=1;
		$str='';
		foreach($section_type_result as $section_type_val)
		{
			$sections_result=$this->users->get_sections($section_type_val->id);
			$str .= '<div class="box'.$i.' box"><h2>'.stripslashes($section_type_val->title).'</h2><ul>';						
			foreach($sections_result as $sections_val)
			{
				
				if (in_array($sections_val->id, $access_options_arr)) 
				{
					$selected = 'checked';
				}else{
					$selected = '';
				}
											
				$str .= '<li><input name="access_sections[]" type="checkbox" value="'.$sections_val->id.'" '.$selected.'>'.stripslashes($sections_val->section_title).'</li>';				 
				$i++; 
			}						
			$str .='</ul></div>';
		}
		
		echo $str;
	}
	public function addusers()
	{	
				
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		
		$id = '';
		$user_id='';		
		
		$id = $this->input->post('muid');
		if(!empty($id))
		{
			$data['muid']=$id;
		}	
		
		$data['title'] = 'Services on demand - Ondi.com';
		$data['access_type_result'] = $this->users->get_access_type();

		
		if($this->input->post('add_user_act'))
		{
			$this->form_validation->set_rules('user_email', 'Email', 'trim|required|valid_email');		
			if(empty($id))
			{
				$this->form_validation->set_rules('user_password', 'Password', 'trim|required');
			}		
			$this->form_validation->set_rules('access_type', 'Access Type', 'trim|required');
		}		
		################################### IN UPDATE MODE ######################################
		if(!empty($id))
		{
			$merchant_user_data = $this->users->get_merchant_user_data($id);			
			$data['merchant_user_data'] = $merchant_user_data;			
			$user_id = $merchant_user_data[0]->merchant_user_id;
		}
		################################### IN UPDATE MODE ######################################


		if ($this->form_validation->run() == FALSE) {

			

		}else{
			
			if($this->input->post('add_user_act')){

				$user_email = $this->input->post('user_email');
				$user_password = $this->input->post('user_password');
				$access_type = $this->input->post('access_type');				

				$access_sections='';
				if($this->input->post('access_sections')>0)
				{
					$access_sections = implode(",",$this->input->post('access_sections'));
					
				}
				
				//$user_password=$this->encrypt->encode($user_password);				
				
				$user_email_count = $this->users->users_count_by_email($user_email, $user_id, "2,3");
								
				if($user_email_count>0)
				{					
					if(empty($id))
					{
						$this->session->set_flashdata('email_already_exist', 'Email address already exist. Please try with someother email address.');	
						redirect('/merchantaccount/addusers/', 'refresh'); // while update
					}else{
						
						$this->session->set_flashdata('user_message', 'Email address already exist. Please try with someother email address.');
						redirect('/merchantaccount/userslist/', 'refresh'); // while update
					}					

				}else{									
					
					if(!empty($id))
					{														
						
						################################### IN UPDATE MODE ######################################						
						
						if(!empty($user_password))
						{
							$rand_salt = $this->CI_encrypt->genRndSalt();					
							$encrypt_pass = $this->CI_encrypt->encryptUserPwd($user_password, $rand_salt);

							$user_update_data = array('email' => $user_email, 'password' => $encrypt_pass, 'salt'=>$rand_salt);

						}else{
							$user_update_data = array('email' => $user_email);							
						}						
						$this->db->where('user_id', $user_id);
						$this->db->update('users', $user_update_data);
						

						$merchant_user_update_data = array('user_access_type' => $access_type, 'user_access_sections' =>$access_sections);
						$this->db->where('id', $id);
						$this->db->update('merchant_users', $merchant_user_update_data);						
						
						//------------------------- sending password to user start -------------------------------------//						
						if(!empty($user_password))
						{
							
							// SEND EMAIL TO USER
							/*$message = "";
							$message .= "";
							$message .= "<br>";			
							$message .= "<br>";
							$message .= "<br>";
							$message .= "Username/Email : ".$user_email;
							$message .= "<br>";
							$message .= "Password : ".$user_password;
							
							$message .= "<br>";
							$message .= "<br>";
							$message .= "Thank you<br>";
							$message .= "Ondi Food<br>";*/

							/*$html_mailer = '<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;"><body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff"><div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;"><div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;"><div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="<?php echo $config['base_url']; ?>public/mailerimages/mailer_logo2.jpg" ></div><div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;"><div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 5px; border: 1px solid #c0bdbd;">'.$message.'</div></div></div></div></body></html>';*/


							$message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
							 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear User, <br/>Your merchant account details are</div>
							</li>

							<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Email</div>
							  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$user_email.'</div>
							</li>
							<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Password</div>
							  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$user_password.'</div>
							</li>
							</ul>';

							
							$html_mailer = '<!DOCTYPE html>
							<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
							<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<title>ONDI FOOD</title>
							</head>
							<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
							<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
							  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
								<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
								<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
								 <img src="'.$this->config->item('base_url').'public/mailerimages/merchant_account.jpg">
								</div>
								<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
								  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
									<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message.'</span></div>
								</div>
							  </div>
							</div>
							</body>
							</html>
							';





							/*
							$this->email->from('info@ondi.com', 'ONDI');
							$this->email->to($user_email);
							$this->email->subject('Forgot Password?');
							$this->email->message($html_mailer);
							$this->email->send();
							*/

							$subject = "User Updated Login Detail";
							$this->offers->send_mail($user_email, $subject, $html_mailer);

						}
						//------------------------- sending password to user start -------------------------------------//	
						
						
						$this->session->set_flashdata('user_message', 'Updated successfully.');
						redirect('/merchantaccount/userslist/', 'refresh');

						################################### IN UPDATE MODE ######################################
						
							
					}else{
						
																		
						$merchant_id = $this->CI_auth->logged_id(); // here don't use get_main_merchant_id() function because we need to save the logged in user id						
						$sess_user_data = $this->session->all_userdata();					
						
						/*
						if($sess_user_data['user_type']=='2')
						{
							$main_merchant_id = $merchant_id;
					
						}else if($sess_user_data['user_type']=='3'){

							$main_merchant_id = $this->users->get_main_merchant_id();
							
						}
						*/
						

						$main_merchant_id = $this->users->get_main_merchant_id();						

						$rand_salt = $this->CI_encrypt->genRndSalt();					
						$encrypt_pass = $this->CI_encrypt->encryptUserPwd($user_password, $rand_salt);
						
						$user_type='3';
						$user_data = array('email' => $user_email, 'password' => $encrypt_pass, 'salt'=>$rand_salt, 'user_type'=>$user_type);
						$this->db->insert('users', $user_data);					
						$insert_id = $this->db->insert_id();
					
						$merchant_user_data = array('merchant_id'=>$merchant_id, 'user_id'=>$insert_id, 'user_access_type' => $access_type, 'user_access_sections' =>$access_sections, 'display_status'=> 1, 'main_merchant_id'=>$main_merchant_id);
						$this->db->insert('merchant_users',$merchant_user_data);

						
						//------------------------- sending password to user start -------------------------------------//

						// SEND EMAIL TO USER
						/*$message = "";
						$message .= "";
						$message .= "<br>";			
						$message .= "<br>";
						$message .= "<br>";
						$message .= "Username/Email : ".$user_email;
						$message .= "<br>";
						$message .= "Password : ".$user_password;
						
						$message .= "<br>";
						$message .= "<br>";
						$message .= "Thank you<br>";
						$message .= "Ondi Food<br>";

						$html_mailer = '<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;"><body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff"><div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;"><div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;"><div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="<?php echo $config['base_url']; ?>public/mailerimages/mailer_logo2.jpg" ></div><div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;"><div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 5px; border: 1px solid #c0bdbd;">'.$message.'</div></div></div></div></body></html>';*/


						$message = '<ul style=" float: left; width: 100%; list-style-type: none; margin: 12px 0; padding: 0px; border: 0;">
							 <li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 100%; font-size: 19px; color: #6b6766; text-align: left; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Dear User, <br/>Your merchant account details are</div>
							</li>

							<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Email</div>
							  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$user_email.'</div>
							</li>
							<li style=" float: left; width: 100%; margin: 12px 0; padding: 0; border: 0;">
							  <div style=" float: left; width: 80px; font-size: 19px; color: #6b6766; text-align: right; line-height: 36px; margin: 0 22px 0 0; padding: 0; border: 0;" align="left">Password</div>
							  <div style=" color: #2D2D2D; float: left; font-size: 28px; font-weight: bold; line-height: 36px; text-align: left; width: 590px; word-wrap: break-word; margin: 0; padding: 0; border: 0;" align="left">'.$user_password.'</div>
							</li>
							</ul>';

							
							$html_mailer = '<!DOCTYPE html>
							<html style="overflow: hidden; overflow-y: scroll;  margin: 0; padding: 0; border: 0;">
							<head>
							<meta charset="utf-8">
							<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
							<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
							<title>ONDI FOOD</title>
							</head>
							<body style="font-size: 12px; color: #181818; font-family: arial; overflow: hidden;  background-color: #fff; margin: 0; padding: 0; border: 0;" bgcolor="#fff">
							<div style=" width: 100%; height: auto; float: left; position: relative; background-color: #fff; margin: 0; padding: 0; border: 0;">
							  <div style=" width: 800px; height: auto; margin: 30px auto; padding: 0; border: 0;">
								<div style="padding:0px 0px; margin:0px 0px 20px 0px; text-indent:25px; text-align:right"><img src="'.$this->config->item('base_url').'public/mailerimages/mailer_logo2.jpg" ></div>
								<div style=" width: 100%; height: auto; float: left; border-top:1px solid #2d2d2d; border-bottom:1px solid #2d2d2d; margin: 0; padding: 15px 0px; ">
								 <img src="'.$this->config->item('base_url').'public/mailerimages/merchant_account.jpg">
								</div>
								<div style=" width: 100%; height: auto; float: left; margin: 30px 0 0; padding: 0; border: 0;">
								  <div style=" width: 798px; height: auto; float: left; margin: 0; padding: 5px 0px; border: 1px solid #c0bdbd;">
									<div style=" height: auto; float: left; width: 92%; font-size: 28px; color: #2d2d2d; font-weight: bold; margin: 0; padding: 25px 32px 0px; "><span style=" font-size: 19px; color: #6b6766; font-weight: normal; margin: 0; padding: 0; border: 0;">'.$message.'</span></div>
								</div>
							  </div>
							</div>
							</body>
							</html>
							';

						/*
						$this->email->from('info@ondi.com', 'ONDI');
						$this->email->to($user_email);
						$this->email->subject('Forgot Password?');
						$this->email->message($html_mailer);
						$this->email->send();
						*/

						$subject = "User Login Detail";
						$this->offers->send_mail($user_email, $subject, $html_mailer);

						//------------------------- sending password to user start -------------------------------------//					
						
						
						$this->session->set_flashdata('user_message', 'Added successfully.');
						redirect('/merchantaccount/userslist/', 'refresh');
						
					}										
					
				}			

			}
		}
		
		$this->load->view('/merchantaccount/addusers', $data);

	}
	public function statistics()
	{
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		
		$user_id='';
		// $merchant_id=$this->CI_auth->logged_id();
		$merchant_id=$this->users->get_main_merchant_id();


		$data['title'] = 'Services on demand - Ondi.com';
		
		$recent_booking_date = $this->users->most_recent_booking_date();
		$booking_date = $recent_booking_date[0]->booking_date;	
				
		$data['recent_booking_date'] = $booking_date;
		
		$data['count_last_30days_offers'] = $this->users->count_statistics_for_offers('30days');
		$data['count_lifetime_offers'] = $this->users->count_statistics_for_offers('lifetime');


		$data['count_last_30days_sales'] = $this->users->count_statistics_for_sales('30days');
		$data['count_lifetime_sales'] = $this->users->count_statistics_for_sales('lifetime');


		$count_last_30days_revenue = $this->users->count_statistics_for_revenue('30days');		
		$data['count_last_30days_revenue'] = $count_last_30days_revenue[0]->total_revenue;

		$count_lifetime_revenue = $this->users->count_statistics_for_revenue('lifetime');
		$data['count_lifetime_revenue'] = $count_lifetime_revenue[0]->total_revenue;

		
		
		$data['average_commission_30days'] = $this->users->get_average_commission('30days');		
		$data['average_commission_lifetime'] = $this->users->get_average_commission('lifetime');

		
		$data['conversion_rate_30days'] = $this->users->get_conversion_rate('30days');
		$data['conversion_rate_lifetime'] = $this->users->get_conversion_rate('lifetime');
		
		$data['booking_list_view'] = $this->users->get_bookings_list_by_date(strtotime($booking_date));
		$data['count_merchant_best_sellers'] = $this->users->count_merchant_best_sellers();		
		$data['merchant_best_sellers'] = $this->users->get_merchant_best_sellers();		
		
		$this->load->view('/merchantaccount/statistics', $data);
	}
	
	public function addoffers($offer_id="")
	{	
		
		/*
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';
		exit();
		*/
		
		
		if(!empty($offer_id) || isset($_GET['offer_id']) && $_GET['offer_id']!="" && isset($_GET['act']) && $_GET['act']=='rerun')
		{
			if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
			{
				$this->users->del_merchant_offer_temp($sess_user_data['offer_temp_id']);
				$this->users->del_merchant_offer_available_time_temp($sess_user_data['offer_temp_id']);
				$array_items = array('offer_temp_id' => '', 'offer_main_id'=>'');
				$this->session->unset_userdata($array_items);
			}
		}

		$sess_user_data = $this->session->all_userdata();		

		$data['title'] = 'Services on demand - Ondi.com';
		$offer_rerun_id = "";

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();

			if($logged_user_access_type=='1'  || $logged_user_access_type=='2' || in_array("5",$data['logged_user_access_sections']))
			{
				
			}else{
				
				redirect('/home', 'refresh');
				exit();
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$data['merchant_liveoffers'] = $this->users->get_offers('liveoffers',$offer_search);

		$different_days_data=array();
		
		if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
		{
			$offer_id = $sess_user_data['offer_temp_id']; // fetching data from offer temp table.
			$offer_detail = $this->users->get_offer_detail($offer_id,'merchant_offers_temp'); 	
			$data['offer_detail'] = $offer_detail;

		}else{

			
			// ?offer_id=174&act=rerun
			$offer_rerun_id = $this->input->post('offer_rerun_id');
			
			if( (isset($_GET['offer_id']) && $_GET['offer_id']!="" && isset($_GET['act']) && $_GET['act']=='rerun') || !empty($offer_rerun_id) )
			{
				
				if(isset($_GET['offer_id']) && $_GET['offer_id']!="")
				{
					$offer_rerun_id = $_GET['offer_id'];
				}				
								
				if(is_numeric($offer_rerun_id))
				{
					$count_offer_id = $this->users->count_offer_id('merchant_offers',$offer_rerun_id);
					if($count_offer_id>0)
					{
						$offer_detail = $this->users->get_offer_detail($offer_rerun_id); // when user view offer while rerun	
						$data['offer_detail'] = $offer_detail;
						$data['offer_action_rerun'] = '1';
						
						if(isset($_GET['offer_id']) && $_GET['offer_id']!="")
						{
							$data['offer_rerun_id'] = $offer_rerun_id;
						}

					}else{
						
						redirect('/merchantaccount/offerhistory', 'refresh');
						exit();
						
					}					

				}else{					

					redirect('/merchantaccount/offerhistory', 'refresh');
					exit();
				}								
				

			}else{

				if($offer_id!="")
				{			
					$offer_detail = $this->users->get_offer_detail($offer_id); // when user view offer in edit mode 	
					$data['offer_detail'] = $offer_detail;
						
				}else{

					$offer_id = $this->input->post('offer_id');			
					if(!empty($offer_id))
					{
						$offer_detail = $this->users->get_offer_detail($offer_id);	// when user update the offer in edit mode 
						$data['offer_detail'] = $offer_detail;
					}
				}

			}


		}	
		
		
		
		
		if($offer_id!="" || !empty($offer_rerun_id))
		{
						
			$atsc='0';
			if(isset($offer_detail[0]->all_day_same_time) && $offer_detail[0]->all_day_same_time=='1')
			{
				$atsc = '1'; // available time slots capacity
			}
			else if(isset($offer_detail[0]->different_days_different_time) && $offer_detail[0]->different_days_different_time=='1')
			{
				$atsc = '2'; // available time slots capacity

			}
			else if( isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours=='1' )
			{

				$atsc = '3'; // available time slots capacity
			}			
			
			
			if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
			{
				
				$different_days_data = $this->users->get_different_days_data($offer_id,$atsc,'merchant_offers_available_time_temp');

			}else{				
				
				if(!empty($offer_rerun_id))
				{					
					$different_days_data = $this->users->get_different_days_data($offer_rerun_id,$atsc); // In rerun situation

				}else{
					
					$different_days_data = $this->users->get_different_days_data($offer_id,$atsc); // Whhile update mode
				}

				//echo '<pre>';
				//print_r($different_days_data);
				//echo '<pre>';
				//exit();

			}						
			$data['different_days_data'] = $different_days_data;

		}
		

		$merchant_opening_hours = array();
		$merchant_opening_hours = $this->users->get_merchant_opening_hours();		
		$data['merchant_opening_hours'] = $merchant_opening_hours;

		$merchant_business_type_str = $this->users->get_merchant_business_type();
		
		$merchant_business_type = array();
		if(!empty($merchant_business_type_str))
		{
			$merchant_business_type = explode(",",$merchant_business_type_str);
			$data['merchant_business_type'] = $merchant_business_type;
		}

		$max_pax_display_check = 0;
		if(isset($merchant_business_type))
		{	
			if(in_array('1',$merchant_business_type))
			{
				$max_pax_display_check = 1;
			}

		}
		

		/*
		$merchant_opening_hours_days_array = array();
		foreach($merchant_opening_hours as $opening_hours_val){
			
			if($opening_hours_val->day!="")
			{
				array_push($merchant_opening_hours_days_array,$opening_hours_val->day);
			}

		}
		*/

		

				
		$this->form_validation->set_rules('offer_title', 'Title', 'trim|required');		
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		$this->form_validation->set_rules('price_normally', 'Price Normally', 'trim|required');
		//$this->form_validation->set_rules('offer_description', 'Short description', 'trim|required');
		$this->form_validation->set_rules('fine_print', 'Fine Print', 'trim');
		
		/*
		$this->form_validation->set_rules('all_day_time_from', '', 'trim');
		$this->form_validation->set_rules('all_day_time_from_am_pm', '', 'trim');
		$this->form_validation->set_rules('all_day_time_to', '', 'trim');		
		$this->form_validation->set_rules('all_day_time_to_am_pm', '', 'trim');
		$this->form_validation->set_rules('all_day_quantity', '', 'trim');
		$this->form_validation->set_rules('all_day_max_pax', '', 'trim');
		*/
		

		if ($this->form_validation->run() == FALSE) {

			//$this->load->view('/merchantaccount/addoffers', $data);
		}
		else{				
				
								
				$merchant_id = '';
				// $merchant_id = $this->CI_auth->logged_id();	
				$merchant_id = $this->users->get_main_merchant_id();	


				$offer_check_by_admin='0'; // 1 means admin need to check first then offer will be published.




				/////////////////////////////////// 4.6.2014 ////////////////////////////////////////////////////////////////
				//////////////////  Check the global settings offer_check_by_admin is set to 1 or 0 /////////////////////////
				$sql_global_offer_check_by_admin = "SELECT offer_check_by_admin FROM ondi_admin WHERE mgmt_id = '1' ";
				$exe_global_offer_check_by_admin = $this->db->query($sql_global_offer_check_by_admin);
				$data_global_offer_check_by_admin = $exe_global_offer_check_by_admin->result();
				$offer_check_by_admin = $data_global_offer_check_by_admin[0]->offer_check_by_admin;
				
				//////////////////  Check the merchant specific settings offer_approval is set to 1 or 0 /////////////////////////
				$sql_merchant_offer_check_by_admin = "SELECT offer_approval FROM users WHERE user_id = '".$merchant_id."' ";
				$exe_merchant_offer_check_by_admin = $this->db->query($sql_merchant_offer_check_by_admin);
				$data_merchant_offer_check_by_admin = $exe_merchant_offer_check_by_admin->result();
				$offer_approval = $data_merchant_offer_check_by_admin[0]->offer_approval;
			
				if($offer_approval == '2') // decline by admin for merchant
				{
					$offer_check_by_admin='1'; //means admin need to check first then offer will be published.
				}
				else if($offer_approval == '1') // accept by admin for merchant
				{
					$offer_check_by_admin='0'; //means offer will be published automatically.
				}
				/////////////////////////////////// 4.6.2014 ////////////////////////////////////////////////////////////////





				$offer_running_today='';
				$offer_multiple_dates='';
				$offer_running_from='';
				$offer_running_to='';
				$remaining_opening_hours_days_count=0;
				$remaining_opening_hours_days_count = $this->input->post('remaining_opening_hours_days_count');

				$offer_title = $this->input->post('offer_title');
				$price = $this->input->post('price');
				$price = str_replace("$","",$price);
				$price_normally = $this->input->post('price_normally');
				$price_normally = str_replace("$","",$price_normally);
				$offer_description = $this->input->post('offer_description');				
				$fine_print = $this->input->post('fine_print');				
				
				
				$offer_running_from = $this->input->post('offer_running_from');
				//m/d/Y
				if($offer_running_from!="")
				{
					$offer_running_from_arr = explode("/",$offer_running_from);
					
					$d='';
					$m='';
					$y='';
					if(isset($offer_running_from_arr[0]))
					{
						$d = $offer_running_from_arr[0];
					}
					if(isset($offer_running_from_arr[1]))
					{
						$m = $offer_running_from_arr[1];
					}
					if($offer_running_from_arr[2])
					{
						$y = $offer_running_from_arr[2];
					}
					if($d!='' && $m!='' && $y!='')
					{							
						$offer_running_from = $y.'-'.$m.'-'.$d;
					}						

				}				
				
				if($this->input->post('offer_running_today')===FALSE)
				{ 
					$offer_running_today='';
				}
				else
				{ 
					$offer_running_today = '1';
					// If Today only checkbox selected  then we are storing current date in offer_running_from and offer_running_to		
					
					//$offer_running_from = date('Y-m-d'); 
					//$offer_running_to = date('Y-m-d');	
					
					$offer_running_from = $offer_running_from; 
					$offer_running_to = $offer_running_from;	
				}				
				
				if($this->input->post('offer_multiple_dates')===FALSE)
				{
					$offer_multiple_dates = '';

				}else{
					
					$offer_multiple_dates = '1';				
					
					$offer_running_to = $this->input->post('offer_running_to');
					if($offer_running_to!="")
					{
						$offer_running_to_arr = explode("/",$offer_running_to);
						
						$d='';
						$m='';
						$y='';
						if(isset($offer_running_to_arr[0]))
						{
							$d = $offer_running_to_arr[0];
						}
						if(isset($offer_running_to_arr[1]))
						{
							$m = $offer_running_to_arr[1];
						}
						if($offer_running_to_arr[2])
						{
							$y = $offer_running_to_arr[2];
						}
						if($d!='' && $m!='' && $y!='')
						{							
							$offer_running_to = $y.'-'.$m.'-'.$d;
						}						

					}

					if($offer_running_from!="" && $offer_running_to!="" && strtotime($offer_running_from)>strtotime($offer_running_to) )
					{
						$this->session->set_flashdata('add_offer_message', 'Please check From and To date.');
						redirect('/merchantaccount/addoffers', 'refresh');
					}

				}				
				
				$everyday_of_week='';
				if($this->input->post('everyday_of_week')===FALSE)
				{
					$everyday_of_week='';
				}else{
					$everyday_of_week='1';
				}


				$select_specific_days='';
				if($this->input->post('select_specific_days')===FALSE)
				{
					$select_specific_days = '';
				}else{
					$select_specific_days = '1';
				}
				
				$specific_days_week='';
				$specific_days_week_array = array();
				if($this->input->post('specific_days_week')===FALSE)
				{
					$specific_days_week = '';
					
				}else{					
					
					$specific_days_week = implode(",",$this->input->post('specific_days_week'));
					$specific_days_week_array = $this->input->post('specific_days_week');

				}
				
				$all_day_same_time='';
				if($this->input->post('all_day_same_time')===FALSE)
				{
					$all_day_same_time = '';

				}else{

					$all_day_same_time = '1';
				}

												
				$different_days_different_time='';
				if($this->input->post('different_days_different_time')===FALSE)
				{
					$different_days_different_time = '';
				}else{
					$different_days_different_time = '1';
				}				
				
				$all_opening_hours='';
				if($this->input->post('all_opening_hours')===FALSE)
				{
					$all_opening_hours = '';
				}else{
					$all_opening_hours = '1';
				}
				
				
				$display_status_str = $this->input->post('display_status');				
				
				if($display_status_str=='publish_offer')
				{
					$display_status = '1';
				}else if($display_status_str=='save_draft')
				{
					$display_status = '2';
				}else if($display_status_str=='preview_offer')
				{
					$display_status = '3';
				}
				else if($display_status_str=='trash')
				{
					$display_status = '0';
				}
				else if($display_status_str=='save_add_another_offer')
				{
					$display_status = '1';
				}
				
				if($offer_check_by_admin=='1' && $display_status_str!='preview_offer')
				{
					$display_status = '4';
				}
				
									
				
				//----------------- FETCHING All the same time --------------------------------//
				$all_day_time_from = $this->input->post('all_day_time_from');				
				$all_day_time_from_am_pm = $this->input->post('all_day_time_from_am_pm');

				$all_day_time_to = $this->input->post('all_day_time_to');
				$all_day_time_to_am_pm = $this->input->post('all_day_time_to_am_pm');

				$all_day_quantity = $this->input->post('all_day_quantity');
				$all_day_max_pax = $this->input->post('all_day_max_pax');
				//----------------- FETCHING All the same time --------------------------------//
				
								
				//----------------- OFFER IMAGE FUNCTIONALITY START --------------------//
				$offer_image="";
				$old_offer_picture = $this->input->post('old_offer_picture');
				
				$temp_offer_picture = $this->input->post('temp_offer_picture');
				
				if(!empty($temp_offer_picture))
				{

					$sql_temp_pp = "SELECT * FROM temp_offer_images WHERE id ='".$temp_offer_picture."' ";
					$exe_temp_pp = $this->db->query($sql_temp_pp);
					$count_temp_pp = $exe_temp_pp->num_rows();
					if($count_temp_pp>0)
					{
						foreach($exe_temp_pp->result() as $tmp)
						{
							$offer_image = $tmp->img_path;
						}
					}

				}
				
				if($offer_image == "") 
				{
					$offer_image = $old_offer_picture;
				}

				//----------------- OFFER IMAGE FUNCTIONALITY END ---------------------//
				
				$offer_data = array(
				   'merchant_id' => $merchant_id,
				   'offer_title' => $offer_title,
				   'price' => $price,
				   'price_normally' => $price_normally,				   
				   'offer_description' => $offer_description,
					'fine_print' => $fine_print,
					
					'offer_running_today' => $offer_running_today,

					'offer_multiple_dates' => $offer_multiple_dates,

					'offer_running_from' => $offer_running_from,
					'offer_running_to' => $offer_running_to,
					'everyday_of_week' => $everyday_of_week,					
					'select_specific_days' => $select_specific_days,
					'specific_days_week' => $specific_days_week,
					'all_day_same_time' => $all_day_same_time,
					'all_day_time_from' => $all_day_time_from,
					'all_day_time_from_am_pm' => $all_day_time_from_am_pm,
					'all_day_time_to' => $all_day_time_to,
					'all_day_time_to_am_pm' => $all_day_time_to_am_pm,
					'all_day_quantity'=>$all_day_quantity,
					'all_day_max_pax'=>$all_day_max_pax,
					'different_days_different_time'=>$different_days_different_time,
					'all_opening_hours'=>$all_opening_hours,
					'display_status' => $display_status,
					'offer_image' => $offer_image
				);
					
				if($display_status_str=='preview_offer')
				{
					$offer_temp_id = "";				
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{
						$offer_temp_id = $sess_user_data['offer_temp_id'];							
						$this->db->where(array('id'=>$offer_temp_id, 'merchant_id'=>$merchant_id));
						$this->db->update('merchant_offers_temp', $offer_data);				
						//echo $this->db->last_query();						

					}else{

						$this->db->insert('merchant_offers_temp',$offer_data);
						$offer_temp_id = $this->db->insert_id();

						// initializing temp offer id
						if($offer_id!="")
						{
							//echo '....1...............';
							$newdata = array('offer_temp_id'  => $offer_temp_id, 'offer_main_id' => $offer_id);
							$this->session->set_userdata($newdata);

						}else{

							//echo '....2...............';
							$newdata = array('offer_temp_id'  => $offer_temp_id);
							$this->session->set_userdata($newdata);
						}

						//echo 'vdeveloper<br>';
						//$sess_user_data = $this->session->all_userdata();
						//print_r($sess_user_data);
						//exit();
						
					}


				}else{

															
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{
						
												
						if(isset($sess_user_data['offer_main_id']) && $sess_user_data['offer_main_id']!="") // For edit mode
						{
							$offer_main_id = $sess_user_data['offer_main_id'];
							$this->db->where(array('id'=>$offer_main_id, 'merchant_id'=>$merchant_id));
							$this->db->update('merchant_offers', $offer_data);					
							//echo $this->db->last_query();

						}else{ // Add mode

							$this->db->insert('merchant_offers',$offer_data);
							$offer_insert_id = $this->db->insert_id();
							$offer_id = $offer_insert_id;

						}


					}else{

						
						if($offer_id!="") // For edit mode when preview is not enabled
						{

							$this->db->where(array('id'=>$offer_id, 'merchant_id'=>$merchant_id));
							$this->db->update('merchant_offers', $offer_data);					
							//echo $this->db->last_query();

						}else{ // For add mode when preview is not enabled

							$this->db->insert('merchant_offers',$offer_data);
							$offer_insert_id = $this->db->insert_id();
						}
					}


				}							
				
				
				//exit();
				############################ INSERTING DATA INTO Available time slots and capacity start ###############################
								
				$timeinfoarray = array();
				$filtered_selected_days = array();
				
				$todays_day="";
				//$todays_day=strtolower(date("l"));
				$todays_day=strtolower(date('l',strtotime($offer_running_from)));
				$monday=0;
				$tuesday=0;
				$wednesday=0;
				$thursday=0;
				$friday=0;
				$saturday=0;
				$sunday=0;

				$week_days_array=array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
				$days_between_multiple_dates_array=array();

				$multiple_date_difference=0;
				if($offer_running_to!="" && $offer_running_from!="" && $offer_multiple_dates=='1')
				{
					
					$multiple_date_difference = $this->users->date_difference($offer_running_to,$offer_running_from);

					while( strtotime($offer_running_from) <= strtotime($offer_running_to) )
					{						
						//date('Y-m-d', strtotime($from_date));								
						$current_day =  strtolower(date('l',strtotime($offer_running_from)));
						
						if($current_day=='monday')
						{
							if($monday==0)
							{
								array_push($days_between_multiple_dates_array,'monday');
							}
							$monday++;

						}
						else if($current_day=='tuesday'){
							
							if($tuesday==0)
							{
								array_push($days_between_multiple_dates_array,'tuesday');
							}
							$tuesday++;
						}
						else if($current_day=='wednesday'){
							
							if($wednesday==0)
							{
								array_push($days_between_multiple_dates_array,'wednesday');
							}
							
							$wednesday++;
						}
						else if($current_day=='thursday'){
							
							if($thursday==0)
							{
								array_push($days_between_multiple_dates_array,'thursday');
							}							
							$thursday++;
						}
						else if($current_day=='friday'){
							
							if($friday==0)
							{
								array_push($days_between_multiple_dates_array,'friday');
							}
							$friday++;
						}
						else if($current_day=='saturday'){
							
							if($saturday==0)
							{
								array_push($days_between_multiple_dates_array,'saturday');
							}
							$saturday++;
						}
						else if($current_day=='sunday'){
							
							if($sunday==0)
							{
								array_push($days_between_multiple_dates_array,'sunday');
							}
							$sunday++;
						}

						$offer_running_from = date('Y-m-d', strtotime($offer_running_from . ' + 1 day'));
						
					}

				}		
				
				if($offer_running_today=='1' && $all_day_same_time=='1') // If Today only and All the same time
				{
					//-------------------------- WE ARE INSERTING JUST ONE RECORD i.d TODAY'S DATE => START -------------------------------//
					
					if($all_day_time_from!="" && $all_day_time_from_am_pm!="" && $all_day_time_to!="" && $all_day_time_to_am_pm!="" && $all_day_quantity!="")
					{	
							
							$timeinfoarray = array(
								"available_day" => $todays_day, 
								"time_from" => $all_day_time_from,
								"time_from_am_pm" => $all_day_time_from_am_pm,
								"time_to" => $all_day_time_to,
								"time_to_am_pm" => $all_day_time_to_am_pm,
								"quantity" => $all_day_quantity,
								"max_pax" => $all_day_max_pax,
								"available_time_slots_capacity" => '1',
								"moh_id" => ""
							);	
							
							array_push($filtered_selected_days,$timeinfoarray);						

					}
					//-------------------------- WE ARE INSERTING JUST ONE RECORD i.d TODAY'S DATE => START -------------------------------//


					
				}
				else if($offer_running_today=='1' && $all_opening_hours=='1') // If Today only and All opening hours
				{					
					//echo '1...........';
					if(($offer_id!="" || !empty($offer_rerun_id)) &&  isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours=='1' && $all_opening_hours=='1')
					{
						//echo '2...........';
						if(isset($different_days_data) && count($different_days_data)>0)
						{							
							//echo '3...........';
							$merchant_opening_hours = $different_days_data;
						}

					}

					//echo '<pre>';
					//print_r($different_days_data);
					//echo '</pre>';

					//echo '<pre>';
					//print_r($merchant_opening_hours);
					//echo '</pre>';

					//echo count($merchant_opening_hours);
					
					
					
					if(count($merchant_opening_hours)>0)
					{
						//echo '...........+...............';
						foreach($merchant_opening_hours as $opening_hours_val)
						{
							$moh_id = $opening_hours_val->id;
							//echo '<br>';
							
							$available_day = $this->input->post('available_day_open_hrs_hd_'.$moh_id);
							$time_from = $this->input->post('time_from_open_hrs_'.$moh_id);
							$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$moh_id);				
							$time_to = $this->input->post('time_to_open_hrs_'.$moh_id);
							$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$moh_id);
							$quantity = $this->input->post('quantity_open_hrs_'.$moh_id);			
							$max_pax = $this->input->post('max_pax_open_hrs_'.$moh_id);

							
							if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && $available_day==$todays_day)
							{
									
																		
									$timeinfoarray = array(
										"available_day" => $available_day,
										"time_from" => $time_from,
										"time_from_am_pm" => $time_from_am_pm,
										"time_to" => $time_to,
										"time_to_am_pm" => $time_to_am_pm,
										"quantity" => $quantity,
										"max_pax" => $max_pax,
										"available_time_slots_capacity" => '3',
										"moh_id" => $moh_id
									);
									
									array_push($filtered_selected_days,$timeinfoarray);
							}

						}

					}
					
					//------------------ Remaining opening hours days start -----------------------------//												
						if(count($remaining_opening_hours_days_count)>0)
						{

							for($new_sno=1;$new_sno<=$remaining_opening_hours_days_count;$new_sno++)
							{																
								$available_day = $this->input->post('available_day_open_hrs_hd_'.$new_sno);
								$time_from = $this->input->post('time_from_open_hrs_'.$new_sno);
								$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$new_sno);				
								$time_to = $this->input->post('time_to_open_hrs_'.$new_sno);
								$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$new_sno);
								$quantity = $this->input->post('quantity_open_hrs_'.$new_sno);			
								$max_pax = $this->input->post('max_pax_open_hrs_'.$new_sno);

								if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && $quantity>0 && $available_day==$todays_day)
								{
										
										$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => '3',
											"moh_id" => $new_sno
										);										
										array_push($filtered_selected_days,$timeinfoarray);
								}

							}

						}							
						//------------------ Remaining opening hours days end -----------------------------//
					

				}
				else if($offer_running_today=='1' && $different_days_different_time=='1') // New update 05-12-2014
				{

					$current_day_array = array($todays_day);
					for($n=1;$n<=21;$n++)
					{							
						$available_day = $this->input->post('available_day_'.$n);
						$time_from = $this->input->post('time_from_'.$n);
						$time_from_am_pm = $this->input->post('time_from_am_pm_'.$n);
						$time_to = $this->input->post('time_to_'.$n);
						$time_to_am_pm = $this->input->post('time_to_am_pm_'.$n);
						$quantity = $this->input->post('quantity_'.$n);
						$max_pax = $this->input->post('max_pax_'.$n);
						
						if(in_array($available_day, $current_day_array) && $available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
						{									
							$timeinfoarray = array(
										"available_day" => $available_day,
										"time_from" => $time_from,
										"time_from_am_pm" => $time_from_am_pm,
										"time_to" => $time_to,
										"time_to_am_pm" => $time_to_am_pm,
										"quantity" => $quantity,
										"max_pax" => $max_pax,
										"available_time_slots_capacity" => "2",
										"moh_id" => ""
									);							
							
							array_push($filtered_selected_days, $timeinfoarray);

						}
						
						
					}

										
					if(isset($different_days_data) && count($different_days_data)>0)
					{
																				
						foreach($different_days_data as $different_days_val)
						{
							$n = $different_days_val->id;
							$available_day = $this->input->post('available_day_edt_'.$n);
							$time_from = $this->input->post('time_from_edt_'.$n);
							$time_from_am_pm = $this->input->post('time_from_edt_am_pm_'.$n);
							$time_to = $this->input->post('time_to_edt_'.$n);
							$time_to_am_pm = $this->input->post('time_to_edt_am_pm_'.$n);
							$quantity = $this->input->post('quantity_edt_'.$n);
							$max_pax = $this->input->post('max_pax_edt_'.$n);
							
							if(in_array($available_day, $current_day_array) && $available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
							{									
								$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => "2",
											"moh_id" => ""
										);							
								
								array_push($filtered_selected_days, $timeinfoarray);

							}

						}						

					}

					/*
					echo '<pre>';
					print_r($different_days_data);
					echo '</pre>';
					exit();
					*/
					
				}
				else if($offer_multiple_dates=='1'){ // Multiple dates
					
					
				if($everyday_of_week=='1')
				{
					// IF EVERYDAY SELECTED 
					
					if($everyday_of_week=='1' && $all_day_same_time=='1') // Everyday && All the same time
					{
						if($multiple_date_difference>=7)
						{
							// We need insert all days having All the same time information

							if($all_day_time_from!="" && $all_day_time_from_am_pm!="" && $all_day_time_to!="" && $all_day_time_to_am_pm!="" && $all_day_quantity!="")
							{
								
								if(count($week_days_array)>0)
								{
									foreach($week_days_array as $week_days_val)
									{
										
										$timeinfoarray = array(
												"available_day" => $week_days_val,
												"time_from" => $all_day_time_from,
												"time_from_am_pm" => $all_day_time_from_am_pm,
												"time_to" => $all_day_time_to,
												"time_to_am_pm" => $all_day_time_to_am_pm,
												"quantity" => $all_day_quantity,
												"max_pax" => $all_day_max_pax,
												"available_time_slots_capacity" => "1",
												"moh_id" => ""
											);

											array_push($filtered_selected_days, $timeinfoarray);

									}

								}

							}


						}else{

							
							// Need to count the which days are appearing between from and to dates.

							if($all_day_time_from!="" && $all_day_time_from_am_pm!="" && $all_day_time_to!="" && $all_day_time_to_am_pm!="" && $all_day_quantity!="")
							{
									if(count($days_between_multiple_dates_array)>0)
									{	
										foreach($days_between_multiple_dates_array as $days_between_multiple_dates_value)
										{									
											
											$timeinfoarray = array(
												"available_day" => $days_between_multiple_dates_value,
												"time_from" => $all_day_time_from,
												"time_from_am_pm" => $all_day_time_from_am_pm,
												"time_to" => $all_day_time_to,
												"time_to_am_pm" => $all_day_time_to_am_pm,
												"quantity" => $all_day_quantity,
												"max_pax" => $all_day_max_pax,
												"available_time_slots_capacity" => "1",
												"moh_id" => ""
											);	
											
											array_push($filtered_selected_days, $timeinfoarray);

										}

									}
								

							}

							


						}
					}
					else if($everyday_of_week=='1' && $different_days_different_time=='1') // Everyday && Different days at different times
					{
						
						for($n=1;$n<=21;$n++)
						{							
							$available_day = $this->input->post('available_day_'.$n);
							$time_from = $this->input->post('time_from_'.$n);
							$time_from_am_pm = $this->input->post('time_from_am_pm_'.$n);
							$time_to = $this->input->post('time_to_'.$n);
							$time_to_am_pm = $this->input->post('time_to_am_pm_'.$n);
							$quantity = $this->input->post('quantity_'.$n);
							$max_pax = $this->input->post('max_pax_'.$n);
							
							if(in_array($available_day, $days_between_multiple_dates_array) && $available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
							{									
								$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => "2",
											"moh_id" => ""
										);							
								
								array_push($filtered_selected_days, $timeinfoarray);

							}
							
							
						}

						
						
						if(isset($different_days_data) && count($different_days_data)>0)
						{
																					
							foreach($different_days_data as $different_days_val)
							{
								$n = $different_days_val->id;
								$available_day = $this->input->post('available_day_edt_'.$n);
								$time_from = $this->input->post('time_from_edt_'.$n);
								$time_from_am_pm = $this->input->post('time_from_edt_am_pm_'.$n);
								$time_to = $this->input->post('time_to_edt_'.$n);
								$time_to_am_pm = $this->input->post('time_to_edt_am_pm_'.$n);
								$quantity = $this->input->post('quantity_edt_'.$n);
								$max_pax = $this->input->post('max_pax_edt_'.$n);
								
								if(in_array($available_day, $days_between_multiple_dates_array) && $available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
								{									
									$timeinfoarray = array(
												"available_day" => $available_day,
												"time_from" => $time_from,
												"time_from_am_pm" => $time_from_am_pm,
												"time_to" => $time_to,
												"time_to_am_pm" => $time_to_am_pm,
												"quantity" => $quantity,
												"max_pax" => $max_pax,
												"available_time_slots_capacity" => "2",
												"moh_id" => ""
											);							
									
									array_push($filtered_selected_days, $timeinfoarray);

								}

							}

							

						}
						

					}
					else if($everyday_of_week=='1' && $all_opening_hours=='1')
					{
																									
						if( ($offer_id!="" || !empty($offer_rerun_id)) &&  isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours=='1' && $all_opening_hours=='1')
						{
								
							
							if(isset($different_days_data) && count($different_days_data)>0)
							{
								$merchant_opening_hours = $different_days_data;								
							}

						}
						
						
						if(count($merchant_opening_hours)>0)
						{

														
							foreach($merchant_opening_hours as $opening_hours_val)
							{
								$moh_id = $opening_hours_val->id;
								
								$available_day = $this->input->post('available_day_open_hrs_hd_'.$moh_id);
								$time_from = $this->input->post('time_from_open_hrs_'.$moh_id);
								$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$moh_id);				
								$time_to = $this->input->post('time_to_open_hrs_'.$moh_id);
								$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$moh_id);
								$quantity = $this->input->post('quantity_open_hrs_'.$moh_id);			
								$max_pax = $this->input->post('max_pax_open_hrs_'.$moh_id);

								if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && in_array($available_day,$days_between_multiple_dates_array))
								{
										
										$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => '3',
											"moh_id" => $moh_id
										);										
										array_push($filtered_selected_days,$timeinfoarray);
								}

							}

								/*
								echo '<pre>';
								print_r($merchant_opening_hours);
								echo '</pre>';
								exit();
								*/

						}


						//------------------ Remaining opening hours days start -----------------------------//												
						if(count($remaining_opening_hours_days_count)>0)
						{

							for($new_sno=1;$new_sno<=$remaining_opening_hours_days_count;$new_sno++)
							{																
								$available_day = $this->input->post('available_day_open_hrs_hd_'.$new_sno);
								$time_from = $this->input->post('time_from_open_hrs_'.$new_sno);
								$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$new_sno);				
								$time_to = $this->input->post('time_to_open_hrs_'.$new_sno);
								$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$new_sno);
								$quantity = $this->input->post('quantity_open_hrs_'.$new_sno);			
								$max_pax = $this->input->post('max_pax_open_hrs_'.$new_sno);

								if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && $quantity>0 && in_array($available_day,$days_between_multiple_dates_array))
								{
										
										$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => '3',
											"moh_id" => $new_sno
										);										
										array_push($filtered_selected_days,$timeinfoarray);
								}

							}

						}							
						//------------------ Remaining opening hours days end -----------------------------//
						
						
						
					}



				}else if($select_specific_days=='1')
				{
								
					
					$new_specific_days_week_array = array();					
					if(count($specific_days_week_array)>0 && count($days_between_multiple_dates_array)>0)
					{
						foreach($specific_days_week_array as $dayname)
						{
							if(in_array($dayname, $days_between_multiple_dates_array))
							{
								array_push($new_specific_days_week_array, $dayname);
							}
						
						}
					}

					$count_new_specific_days_week_array = count($new_specific_days_week_array);

					/*
					echo '<pre>';
					print_r($new_specific_days_week_array);
					echo '</pre>';

					echo $count_new_specific_days_week_array;
					exit();
					*/

					
					if($all_day_same_time=='1' && $count_new_specific_days_week_array>0)
					{
						
						/*
						echo '1................<br>';						
						echo $all_day_time_from.'aaaaaaaaaaaaaaaaaaaaaaaaa <br>';
						echo $all_day_time_from_am_pm.'bbbbbbbbbbbbbbbbbbbbbb <br>';
						echo $all_day_time_to.'ccccccccccccccccccccc <br>';
						echo $all_day_time_to_am_pm.'dddddddddddddd <br>';
						echo $all_day_quantity.'eeeeeeeeeeeeeeeee <br>';
						*/			

						
						if($all_day_time_from!="" && $all_day_time_from_am_pm!="" && $all_day_time_to!="" && $all_day_time_to_am_pm!="" && $all_day_quantity!="")
						{
							//echo '........start........';							
							foreach($new_specific_days_week_array as $daysname)
							{
								//echo '1'.$vj.'................<br>';
								$timeinfoarray = array(
									"available_day" => $daysname,
									"time_from" => $all_day_time_from,
									"time_from_am_pm" => $all_day_time_from_am_pm,
									"time_to" => $all_day_time_to,
									"time_to_am_pm" => $all_day_time_to_am_pm,
									"quantity" => $all_day_quantity,
									"max_pax" => $all_day_max_pax,
									"available_time_slots_capacity" => "1",
									"moh_id" => ""
								);
								
								array_push($filtered_selected_days, $timeinfoarray);
								

							}

						}			
						

					}
					else if($different_days_different_time=='1' && $count_new_specific_days_week_array>0)
					{							
						
							//echo '2................';
						
						for($n=1;$n<=21;$n++)
						{						
							$available_day = $this->input->post('available_day_'.$n);
							$time_from = $this->input->post('time_from_'.$n);
							$time_from_am_pm = $this->input->post('time_from_am_pm_'.$n);
							$time_to = $this->input->post('time_to_'.$n);
							$time_to_am_pm = $this->input->post('time_to_am_pm_'.$n);
							$quantity = $this->input->post('quantity_'.$n);
							$max_pax = $this->input->post('max_pax_'.$n);

							if($available_day!="" && in_array($available_day,$new_specific_days_week_array) && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
							{										
								$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => "2",
											"moh_id" => ""
										);	
										
								array_push($filtered_selected_days, $timeinfoarray);

							}

						}	
						
						// For edit mode start here

						if(isset($different_days_data) && count($different_days_data)>0)
						{
																					
							foreach($different_days_data as $different_days_val)
							{
								$n = $different_days_val->id;
								$available_day = $this->input->post('available_day_edt_'.$n);
								$time_from = $this->input->post('time_from_edt_'.$n);
								$time_from_am_pm = $this->input->post('time_from_edt_am_pm_'.$n);
								$time_to = $this->input->post('time_to_edt_'.$n);
								$time_to_am_pm = $this->input->post('time_to_edt_am_pm_'.$n);
								$quantity = $this->input->post('quantity_edt_'.$n);
								$max_pax = $this->input->post('max_pax_edt_'.$n);
								
								if(in_array($available_day, $days_between_multiple_dates_array) && $available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="")
								{									
									$timeinfoarray = array(
												"available_day" => $available_day,
												"time_from" => $time_from,
												"time_from_am_pm" => $time_from_am_pm,
												"time_to" => $time_to,
												"time_to_am_pm" => $time_to_am_pm,
												"quantity" => $quantity,
												"max_pax" => $max_pax,
												"available_time_slots_capacity" => "2",
												"moh_id" => ""
											);							
									
									array_push($filtered_selected_days, $timeinfoarray);

								}

							}

							

						}


						// For edit mode end here


					}
					else if($all_opening_hours=='1' && $count_new_specific_days_week_array>0)
					{

						//echo '3................';
						
						if( ($offer_id!="" || !empty($offer_rerun_id) ) &&  isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours=='1' && $all_opening_hours=='1')
						{
							if(isset($different_days_data) && count($different_days_data)>0)
							{
								/*
								foreach($different_days_data as $different_days_val)
								{
									$moh_id = $opening_hours_val->id;

								}
								*/
								$merchant_opening_hours = $different_days_data;
							}

						}
						
						
						if(count($merchant_opening_hours)>0)
						{

							foreach($merchant_opening_hours as $opening_hours_val)
							{
								$moh_id = $opening_hours_val->id;
								
								$available_day = $this->input->post('available_day_open_hrs_hd_'.$moh_id);
								$time_from = $this->input->post('time_from_open_hrs_'.$moh_id);
								$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$moh_id);				
								$time_to = $this->input->post('time_to_open_hrs_'.$moh_id);
								$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$moh_id);
								$quantity = $this->input->post('quantity_open_hrs_'.$moh_id);			
								$max_pax = $this->input->post('max_pax_open_hrs_'.$moh_id);

								if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && $quantity>0 && in_array($available_day,$new_specific_days_week_array))
								{
										
										$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => '3',
											"moh_id" => $moh_id
										);										
										array_push($filtered_selected_days,$timeinfoarray);
								}

							}

						}

						
						//------------------ Remaining opening hours days start -----------------------------//												
						if(count($remaining_opening_hours_days_count)>0)
						{

							for($new_sno=1;$new_sno<=$remaining_opening_hours_days_count;$new_sno++)
							{																
								$available_day = $this->input->post('available_day_open_hrs_hd_'.$new_sno);
								$time_from = $this->input->post('time_from_open_hrs_'.$new_sno);
								$time_from_am_pm = $this->input->post('time_from_am_pm_open_hrs_'.$new_sno);				
								$time_to = $this->input->post('time_to_open_hrs_'.$new_sno);
								$time_to_am_pm = $this->input->post('time_to_am_pm_open_hrs_'.$new_sno);
								$quantity = $this->input->post('quantity_open_hrs_'.$new_sno);			
								$max_pax = $this->input->post('max_pax_open_hrs_'.$new_sno);

								if($available_day!="" && $time_from!="" && $time_from_am_pm!="" && $time_to!="" && $time_to_am_pm!="" && $quantity>0 && in_array($available_day,$new_specific_days_week_array))
								{
										
										$timeinfoarray = array(
											"available_day" => $available_day,
											"time_from" => $time_from,
											"time_from_am_pm" => $time_from_am_pm,
											"time_to" => $time_to,
											"time_to_am_pm" => $time_to_am_pm,
											"quantity" => $quantity,
											"max_pax" => $max_pax,
											"available_time_slots_capacity" => '3',
											"moh_id" => $new_sno
										);										
										array_push($filtered_selected_days,$timeinfoarray);
								}

							}

						}							
						//------------------ Remaining opening hours days end -----------------------------//

					}

						

				}



				}

				
						
						/*
						echo '........old........';
						echo '<pre>';
						print_r($filtered_selected_days);
						echo '</pre>';
						exit();
						*/					
						
				
				
				//------- after all filteration inserting data into table start ---------//				
				
				$sess_user_data = $this->session->all_userdata();

				/*
				echo '<pre>';
				print_r($sess_user_data);
				echo '</pre>';
				exit();
				*/

				
				/*
				if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
				{
					$table_moat = 'merchant_offers_available_time_temp';
					$table_mo = ''; // merchant offers
					$offer_id = $sess_user_data['offer_temp_id'];					
					$offer_temp_id = $sess_user_data['offer_temp_id'];										

				}else{

					$table_moat = 'merchant_offers_available_time';	
					$table_mo = ''; // merchant offers
				}
				*/

					
				


				if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
				{										
					// $offer_temp_id = $sess_user_data['offer_temp_id'];	
					
					//$offer_id = $sess_user_data['offer_temp_id'];
					
					/*
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="" && $display_status_str=='preview_offer')
					{						
						$offer_insert_id = $sess_user_data['offer_temp_id'];	
					}
					*/
					
					/*
					echo '...................***...<br>';
					echo $offer_id;
					echo '<br>';
					echo $offer_insert_id;
					exit();
					*/
					

				}
				
				if($display_status_str=='preview_offer')
				{
					$table_moat = 'merchant_offers_available_time_temp';
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{
						$this->users->del_available_different_days($sess_user_data['offer_temp_id'],'merchant_offers_available_time_temp'); // temporary table
					}	

				}else{

					$table_moat = 'merchant_offers_available_time';	

					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{						
						$this->users->del_available_different_days($sess_user_data['offer_main_id'],'merchant_offers_available_time'); // main table
						$this->users->del_available_different_days($sess_user_data['offer_temp_id'],'merchant_offers_available_time_temp'); // temp table

					}
					
					if(!empty($offer_insert_id))
					{
						$this->users->del_available_different_days($offer_insert_id); // preview not enabled a single time

					}else{

						if(!empty($offer_id))
						{
							$this->users->del_available_different_days($offer_id); // preview not enabled a single time
						}
						
					}
					
				}

					
				
				//echo '<pre>';
				//print_r($sess_user_data);
				//echo '</pre>';


				
				
				if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
				{
					if($display_status_str=='preview_offer')
					{
						//echo '1<br>';
						$offer_insert_id = $sess_user_data['offer_temp_id'];

					}else{	
						
						

						if(isset($sess_user_data['offer_main_id']) && $sess_user_data['offer_main_id']!="")
						{
							//echo '2<br>';
							$offer_insert_id = $sess_user_data['offer_main_id'];
						}
						else{

							if(empty($offer_insert_id))
							{	

								//echo '3<br>';
								$offer_insert_id = $sess_user_data['offer_temp_id'];

							}
							
						}
					}
					
				}else{

					if(!empty($offer_id))
					{
						// echo '4';
						$offer_insert_id = $offer_id;
					}
					

				}

				/*				
				echo '<pre>';
				print_r($filtered_selected_days);
				echo '</pre>';
				exit();
				*/
				
				foreach($filtered_selected_days as $key => $value )
				{

					if(isset($value['available_day']) && $value['available_day']!="" && isset($value['time_from']) && $value['time_from']!="" && isset($value['time_to']) && $value['time_to']!="") 
					{
						$from_hours = '';
						$to_hours = '';
						
						$dayname = $value['available_day'];
						$time_from = $value['time_from'];
						$time_from_am_pm = $value['time_from_am_pm'];

						$from_timeval = $time_from.' '.$time_from_am_pm;
						$from_hours  = date("Hi", strtotime($from_timeval));

						
						/*
						if($time_from_am_pm=='am' || $time_from_am_pm=='AM')
						{
							
							if($time_from==12)
							{
								$from_hours = '0000';

							}else{

								$from_hours = $time_from * 100;
								if($time_from>=1 && $time_from<=9)
								{
									$from_hours = '0'.$from_hours;
								}
							}

						}
						else if($time_from_am_pm=='pm' || $time_from_am_pm=='PM')
						{
							
							if($time_from==12)
							{
								$from_hours = '1200';

							}else{
								$from_hours = 1200 + ($time_from * 100);								
							}

						}
						*/

						$time_to = $value['time_to'];
						$time_to_am_pm = $value['time_to_am_pm'];

						$to_timeval = $time_to.' '.$time_to_am_pm;
						$to_hours  = date("Hi", strtotime($to_timeval));

						/*
						if($time_to_am_pm=='am' || $time_to_am_pm=='AM')
						{
							
							if($time_to==12)
							{
								$to_hours = '0000';

							}else{

								$to_hours = $time_to * 100;
								if($time_to>=1 && $time_to<=9)
								{
									$to_hours = '0'.$to_hours;
								}
							}

						}
						else if($time_to_am_pm=='pm' || $time_to_am_pm=='PM')
						{
							
							if($time_to==12)
							{
								$to_hours = '1200';

							}else{
								$to_hours = 1200 + ($time_to * 100);								
							}

						}
						*/						

						$quantity = $value['quantity'];
						$max_pax = $value['max_pax'];
						
						if($max_pax_display_check==0)
						{
							$max_pax = 0;
						}
						
						$available_time_slots_capacity = $value['available_time_slots_capacity'];

						$moh_id = $value['moh_id'];						
						
						$chk_available_different_days=0;
						$chk_available_different_days = $this->users->count_available_different_days($dayname,$from_hours,$to_hours,$offer_insert_id,$table_moat);
						
						if($chk_available_different_days==0)
						{

							$days_display_order='';
							
							if($dayname=='monday')
							{
								$days_display_order = '1';

							}else if($dayname=='tuesday'){

								$days_display_order = '2';
							}
							else if($dayname=='wednesday'){

								$days_display_order = '3';
							}
							else if($dayname=='thursday'){

								$days_display_order = '4';

							}
							else if($dayname=='friday'){

								$days_display_order = '5';
							}
							else if($dayname=='saturday'){

								$days_display_order = '6';
							}
							else if($dayname=='sunday'){

								$days_display_order = '7';
							}
							
							$available_time_data = array(
							'offer_id' => $offer_insert_id,
							'merchant_id' => $merchant_id,
							'available_day' => $dayname,
							'time_from' => $time_from,
							'time_from_am_pm' => $time_from_am_pm,
							'from_hours' => $from_hours,
							'time_to' => $time_to,
							'time_to_am_pm' => $time_to_am_pm,
							'to_hours' => $to_hours,
							'quantity' => $quantity,
							'max_pax' => $max_pax,
							'added_date' => date('Y-m-d H:i:s'),
							'available_time_slots_capacity' => $available_time_slots_capacity,
							'days_display_order' => $days_display_order,
							'moh_id' => $moh_id
							);

							if(!empty($offer_insert_id))
							{
								$this->db->insert($table_moat, $available_time_data);
							}
							

						}
					}

				}

				//------- after all filteration inserting data into table end ---------//

				############################ INSERTING DATA INTO Available time slots and capacity end ###############################		
				


				##################### SENDING EMAIL TO MERCHANT IF ADMIN APPROVAL IS REQUIRED ######################
				if($offer_check_by_admin=='1')
				{

					
				}
				##################### SENDING EMAIL TO MERCHANT IF ADMIN APPROVAL IS REQUIRED ######################
				
				
				
				if($this->input->post('display_status')=='save_add_another_offer')
				{
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{
						$this->users->del_merchant_offer_temp($offer_temp_id);
						$this->users->del_merchant_offer_available_time_temp($offer_temp_id);

						$array_items = array('offer_temp_id' => '', 'offer_main_id'=>'');
						$this->session->unset_userdata($array_items);
					}					
					
					$this->session->set_flashdata('offer_message', 'Added successfully.');
					redirect('/merchantaccount/addoffers', 'refresh');

				}
				else if($this->input->post('display_status')=='preview_offer'){
															
					
					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{

						//echo '2................................';
						//exit();						
						
						$url = $this->config->base_url()."merchantoffers/preview/".$sess_user_data['offer_temp_id'];
						

						//echo '<script>window.open("'.$url.'", "_blank"); window.location.href="'.$this->config->base_url().'merchantaccount/addoffers/'.$sess_user_data['offer_temp_id'].'"; </script>';	

						echo '<script>window.open("'.$url.'", "_blank");</script>';	
						echo '<script>window.location.href="'.$this->config->base_url().'merchantaccount/addoffers/'.$sess_user_data['offer_temp_id'].'";</script>';	
						
						//redirect('/merchantaccount/addoffers/'.$sess_user_data['offer_temp_id'], 'refresh');											
						exit();

					}
					
					
					
				
				}else{
					

					if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
					{
						$this->users->del_merchant_offer_temp($offer_temp_id);
						$this->users->del_merchant_offer_available_time_temp($offer_temp_id);

						$array_items = array('offer_temp_id' => '', 'offer_main_id'=>'');
						$this->session->unset_userdata($array_items);
					}
					
					$this->session->set_flashdata('offer_message', 'Added successfully.');
					redirect('/merchantaccount/liveoffers', 'refresh');
				}
				
				

		 }

		 //echo '<pre>';
		 //print_r($data['merchant_opening_hours']);
		 //echo '</pre>';
		 //exit();
		 
		$this->load->view('/merchantaccount/addoffers', $data);

	}
	
	public function trashoffer($offer_id="")
	{
		
		$sess_user_data = $this->session->all_userdata();
		if(isset($sess_user_data['offer_temp_id']) && $sess_user_data['offer_temp_id']!="")
		{
			$offer_temp_id = $sess_user_data['offer_temp_id'];
			$this->users->del_merchant_offer_temp($offer_temp_id);
			$this->users->del_merchant_offer_available_time_temp($offer_temp_id);

			$array_items = array('offer_temp_id' => '', 'offer_main_id'=>'');
			$this->session->unset_userdata($array_items);

		}
		else{

			if(!empty($offer_id))
			{
				$merchant_id = '';
				// $merchant_id=$this->CI_auth->logged_id();
				$merchant_id=$this->users->get_main_merchant_id();
				
				$this->db->delete('merchant_offers', array('id' => $offer_id, 'merchant_id'=> $merchant_id ));
				$this->db->delete('merchant_offers_available_time', array('offer_id' => $offer_id, 'merchant_id'=> $merchant_id ));			
				$this->db->where('id', $offer_id);
				
				//$offer_data = array('display_status' => '0');
				//$this->db->where(array('id'=>$offer_id, 'merchant_id'=>$merchant_id));
				//$this->db->update('merchant_offers', $offer_data);				

				//echo $this->db->last_query(); die;
			}

		}

		$this->session->set_flashdata('add_offer_message', 'Offer deleted successfully.');
		redirect('/merchantaccount/liveoffers', 'refresh');
		
		
	}
	public function bookingscalender()
	{
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			if($logged_user_access_type=='2'  || ($logged_user_access_type=='3' && in_array("6",$data['logged_user_access_sections'])) )
			{
				
			}else{				
				redirect('/home', 'refresh');
				exit();
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		
		$user_id='';
		
		//$user_id=$this->CI_auth->logged_id();		
		//$data['users_data'] = $this->users->get_userslist();
		
		$offer_search="";
		$offer_search = $this->input->post('offer_search');
		$data['offer_search']=$offer_search;		
		$data['title']='Services on demand - Ondi.com';
		$data['current_bookings_dates'] = $this->users->get_current_bookings_dates($offer_search);
		$this->load->view('/merchantaccount/bookingscalender', $data);
	}
	public function liveoffers()
	{
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$offer_search="";
		$offer_search = $this->input->post('offer_search');
		$data['offer_search']=$offer_search;
		$data['title'] = 'Services on demand - Ondi.com';	
		// parameters
		// 1. display status
		// 2. search input value
		$data['offers_data'] = $this->users->get_offers('liveoffers',$offer_search);		
		$this->load->view('/merchantaccount/liveoffers', $data);
	}
	public function offerhistory()
	{
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$offer_search="";
		$offer_search = $this->input->post('offer_search');
		$data['offer_search']=$offer_search;
		$data['title'] = 'Services on demand - Ondi.com';	
		// parameters
		// 1. display status
		// 2. search input value
		$data['offers_data'] = $this->users->get_offers('offerhistory',$offer_search);		
		$this->load->view('/merchantaccount/offerhistory', $data);
	}
	public function updatebusshours()
	{
		/*
		echo '<pre>';
		print_r($_POST);
		echo '</pre>';
		*/

		//$this->users->get_distinct_offer_id_having_moh_id();

		$merchant_id = '';	
		// $merchant_id=$this->CI_auth->logged_id();
		$merchant_id=$this->users->get_main_merchant_id();
		
		$update_existing_offers = $this->input->post('update_existing_offers');

		$entered_buss_hrs_count=0;
		$entered_buss_hrs_count = $this->input->post('entered_buss_hrs_count');
		if($entered_buss_hrs_count>0)
		{
			$num=0;		

			for($i=1;$i<=$entered_buss_hrs_count;$i++)
			{
				$merchant_opening_hours_id = $this->input->post('merchant_opening_hours_id_'.$i);				
				$buss_upd_hrs_available_day = $this->input->post('buss_upd_hrs_available_day_'.$i);
				$buss_upd_hrs_time_from = $this->input->post('buss_upd_hrs_time_from_'.$i);
				$buss_upd_hrs_time_to = $this->input->post('buss_upd_hrs_time_to_'.$i);
				
				$buss_upd_hrs_time_from_am_pm = strtolower($this->input->post('buss_upd_hrs_time_from_am_pm_'.$i));
				$buss_upd_hrs_time_to_am_pm = strtolower($this->input->post('buss_upd_hrs_time_to_am_pm_'.$i));
				
				if($buss_upd_hrs_available_day!="" && $buss_upd_hrs_time_from!="" && $buss_upd_hrs_time_to!="" && $buss_upd_hrs_time_from_am_pm!="" && $buss_upd_hrs_time_to_am_pm!="")
				{

						$opentime_insert = array(
							'user_id' => $merchant_id,
							'day' => $buss_upd_hrs_available_day,
							'from_hour' => $buss_upd_hrs_time_from,
							'from_ampm' => $buss_upd_hrs_time_from_am_pm,
							'to_hour' => $buss_upd_hrs_time_to,
							'to_ampm' => $buss_upd_hrs_time_to_am_pm
						);						
						
						$opentime_update = array(
							'day' => $buss_upd_hrs_available_day,
							'from_hour' => $buss_upd_hrs_time_from,
							'from_ampm' => $buss_upd_hrs_time_from_am_pm,
							'to_hour' => $buss_upd_hrs_time_to,
							'to_ampm' => $buss_upd_hrs_time_to_am_pm
						);

						$this->db->where('user_id', $merchant_id);
						$this->db->where('id', $merchant_opening_hours_id);
						$this->db->update('merchant_opening_hours', $opentime_update);
						$num++;


						
						if($update_existing_offers=='1')
						{
							//*************************** updating existing merchant_offers_available_time start ***************************//
							
							if($buss_upd_hrs_time_from_am_pm=='am' || $buss_upd_hrs_time_from_am_pm=='AM')
							{
								
								if($buss_upd_hrs_time_from==12)
								{
									$from_hours = '0000';

								}else{

									$from_hours = $buss_upd_hrs_time_from * 100;
									if($buss_upd_hrs_time_from>=1 && $buss_upd_hrs_time_from<=9)
									{
										$from_hours = '0'.$from_hours;
									}
								}

							}
							else if($buss_upd_hrs_time_from_am_pm=='pm' || $buss_upd_hrs_time_from_am_pm=='PM')
							{
								
								if($buss_upd_hrs_time_from==12)
								{
									$from_hours = '1200';

								}else{
									$from_hours = 1200 + ($buss_upd_hrs_time_from * 100);								
								}

							}						

							if($buss_upd_hrs_time_to_am_pm=='am' || $buss_upd_hrs_time_to_am_pm=='AM')
							{
								
								if($buss_upd_hrs_time_to==12)
								{
									$to_hours = '0000';

								}else{

									$to_hours = $buss_upd_hrs_time_to * 100;
									if($buss_upd_hrs_time_to>=1 && $buss_upd_hrs_time_to<=9)
									{
										$to_hours = '0'.$to_hours;
									}
								}

							}
							else if($buss_upd_hrs_time_to_am_pm=='pm' || $buss_upd_hrs_time_to_am_pm=='PM')
							{
								
								if($buss_upd_hrs_time_to==12)
								{
									$to_hours = '1200';

								}else{
									$to_hours = 1200 + ($buss_upd_hrs_time_to * 100);								
								}

							}

							$available_time_data = array(							
							'time_from' => $buss_upd_hrs_time_from,
							'time_from_am_pm' => $buss_upd_hrs_time_from_am_pm,
							'from_hours' => $from_hours,
							'time_to' => $buss_upd_hrs_time_to,
							'time_to_am_pm' => $buss_upd_hrs_time_to_am_pm,
							'to_hours' => $to_hours,
							'added_date' => date('Y-m-d H:i:s')							
							);
							
							// $this->db->where(array('merchant_id'=>$merchant_opening_hours_id, 'available_day'=>$buss_upd_hrs_available_day, 'available_time_slots_capacity'=>3));

							$this->db->where(array('merchant_id'=>$merchant_id, 'available_time_slots_capacity'=>3, 'moh_id'=>$merchant_opening_hours_id ));
							$this->db->update('merchant_offers_available_time', $available_time_data);							

							//*************************** updating existing merchant_offers_available_time end ***************************//
						}

					
				}			
				

			}		


		}

		//-------------- inserting new available time start ---------------------//

			//$existing_buiss_opening_hours_data = $this->users->get_different_days_data("","","","1"); // do not remove 1

			//get_distinct_offer_id_having_moh_id()

			$distinct_offer_id = $this->users->get_distinct_offer_id_having_moh_id();

			$insert_num=0;

			$monday=0;
			$tuesday=0;
			$wednesday=0;
			$thursday=0;
			$friday=0;
			$saturday=0;
			$sunday=0;
								
			for($i=1;$i<=10;$i++)
			{
				$buss_ins_hrs_available_day = $this->input->post('buss_ins_hrs_available_day_'.$i);
				if($buss_ins_hrs_available_day=='monday')
				{
					$monday++;
				}
				if($buss_ins_hrs_available_day=='tuesday')
				{
					$tuesday++;
				}
				if($buss_ins_hrs_available_day=='wednesday')
				{
					$wednesday++;
				}
				if($buss_ins_hrs_available_day=='thursday')
				{
					$thursday++;
				}
				if($buss_ins_hrs_available_day=='friday')
				{
					$friday++;
				}
				if($buss_ins_hrs_available_day=='saturday')
				{
					$saturday++;
				}
				if($buss_ins_hrs_available_day=='sunday')
				{
					$sunday++;
				}
			}


			for($i=1;$i<=10;$i++)
			{
								
				$buss_ins_hrs_available_day = $this->input->post('buss_ins_hrs_available_day_'.$i);
				$buss_ins_hrs_time_from = $this->input->post('buss_ins_hrs_time_from_'.$i);
				$buss_ins_hrs_time_to = $this->input->post('buss_ins_hrs_time_to_'.$i);
				$buss_ins_hrs_time_from_am_pm = strtolower($this->input->post('buss_ins_hrs_time_from_am_pm_'.$i));
				$buss_ins_hrs_time_to_am_pm = strtolower($this->input->post('buss_ins_hrs_time_to_am_pm_'.$i));

				if($buss_ins_hrs_available_day!="" && $buss_ins_hrs_time_from!="" && $buss_ins_hrs_time_from_am_pm!="" && $buss_ins_hrs_time_to!="" && $buss_ins_hrs_time_to_am_pm!="" && ( ($buss_ins_hrs_available_day=='monday' && $monday<=1) || ($buss_ins_hrs_available_day=='tuesday' && $tuesday<=1) || ($buss_ins_hrs_available_day=='wednesday' && $wednesday<=1) || ($buss_ins_hrs_available_day=='thursday' && $thursday<=1) || ($buss_ins_hrs_available_day=='friday' && $friday<=1) || ($buss_ins_hrs_available_day=='saturday' && $saturday<=1) || ($buss_ins_hrs_available_day=='sunday' && $sunday<=1) ) )
				{

						$opentime_insert = array(
							'user_id' => $merchant_id,
							'day' => $buss_ins_hrs_available_day,
							'from_hour' => $buss_ins_hrs_time_from,
							'from_ampm' => $buss_ins_hrs_time_from_am_pm,
							'to_hour' => $buss_ins_hrs_time_to,
							'to_ampm' => $buss_ins_hrs_time_to_am_pm
						);
						
						
						$query_opentime = $this->db->get_where('merchant_opening_hours', array('user_id' => $merchant_id, 'day' => $buss_ins_hrs_available_day, 'from_hour'=>$buss_ins_hrs_time_from, 'from_ampm'=>$buss_ins_hrs_time_from_am_pm, 'to_hour'=>$buss_ins_hrs_time_to, 'to_ampm'=>$buss_ins_hrs_time_to_am_pm )); 

						//echo $this->db->last_query();
						//exit();						

						$count_opentime = $query_opentime->num_rows(); 
						if($count_opentime === 0)
						{ 
							
							$this->db->insert('merchant_opening_hours', $opentime_insert);							
							$last_opening_insert_id = $this->db->insert_id();	
							
							foreach($distinct_offer_id as $offer_val)
							{
								$offer_id = $offer_val->offer_id;
								if(!empty($offer_id))
								{
									
									if($update_existing_offers=='1')
									{
							
										//***** inserting opening hours in existing merchant_offers_available_time start *******//
										
										if($buss_ins_hrs_time_from_am_pm=='am' || $buss_ins_hrs_time_from_am_pm=='AM')
										{
											
											if($buss_ins_hrs_time_from==12)
											{
												$from_hours = '0000';

											}else{

												$from_hours = $buss_ins_hrs_time_from * 100;
												if($buss_ins_hrs_time_from>=1 && $buss_ins_hrs_time_from<=9)
												{
													$from_hours = '0'.$from_hours;
												}
											}

										}
										else if($buss_ins_hrs_time_from_am_pm=='pm' || $buss_ins_hrs_time_from_am_pm=='PM')
										{
											
											if($buss_ins_hrs_time_from==12)
											{
												$from_hours = '1200';

											}else{
												$from_hours = 1200 + ($buss_ins_hrs_time_from * 100);								
											}

										}						

										if($buss_ins_hrs_time_to_am_pm=='am' || $buss_ins_hrs_time_to_am_pm=='AM')
										{
											
											if($buss_ins_hrs_time_to==12)
											{
												$to_hours = '0000';

											}else{

												$to_hours = $buss_ins_hrs_time_to * 100;
												if($buss_ins_hrs_time_to>=1 && $buss_ins_hrs_time_to<=9)
												{
													$to_hours = '0'.$to_hours;
												}
											}

										}
										else if($buss_ins_hrs_time_to_am_pm=='pm' || $buss_ins_hrs_time_to_am_pm=='PM')
										{
											
											if($buss_ins_hrs_time_to==12)
											{
												$to_hours = '1200';

											}else{
												$to_hours = 1200 + ($buss_ins_hrs_time_to * 100);								
											}

										}


										$days_display_order='';
							
										if($buss_ins_hrs_available_day=='monday')
										{
											$days_display_order = '1';

										}else if($buss_ins_hrs_available_day=='tuesday'){

											$days_display_order = '2';
										}
										else if($buss_ins_hrs_available_day=='wednesday'){

											$days_display_order = '3';
										}
										else if($buss_ins_hrs_available_day=='thursday'){

											$days_display_order = '4';

										}
										else if($buss_ins_hrs_available_day=='friday'){

											$days_display_order = '5';
										}
										else if($buss_ins_hrs_available_day=='saturday'){

											$days_display_order = '6';
										}
										else if($buss_ins_hrs_available_day=='sunday'){

											$days_display_order = '7';
										}
										
										$available_time_data = array(
																'offer_id' => $offer_id,
																'merchant_id' => $merchant_id,
																'available_day' => $buss_ins_hrs_available_day,
																'time_from' => $buss_ins_hrs_time_from,
																'time_from_am_pm' => $buss_ins_hrs_time_from_am_pm,
																'from_hours' => $from_hours,
																'time_to' => $buss_ins_hrs_time_to,
																'time_to_am_pm' => $buss_ins_hrs_time_to_am_pm,
																'to_hours' => $to_hours,
																'quantity' => '1',
																'max_pax' => '',
																'added_date' => date('Y-m-d H:i:s'),
																'available_time_slots_capacity' => '3',
																'days_display_order' => $days_display_order,
																'moh_id' => $last_opening_insert_id
																);										

										if(!empty($offer_id))
										{
											$this->db->insert('merchant_offers_available_time', $available_time_data);
											//echo $this->db->last_query();
											//exit();
										}																

										//***** inserting opening hours in existing merchant_offers_available_time start *******//
									}
									
								}

							}

							$insert_num++;
						}
					
				}				

			}			


			if($num++)
			{
				echo 'Business hours updated successfully!';
			}

		//-------------- inserting new available time end ---------------------//

	}
	public function viewbusinesshours()
	{
		$data['merchant_opening_hours'] = $this->users->get_merchant_opening_hours();
		
		$merchant_business_type_str = $this->users->get_merchant_business_type();
		
		$merchant_business_type = array();
		if(!empty($merchant_business_type_str))
		{
			$merchant_business_type = explode(",",$merchant_business_type_str);
			$data['merchant_business_type'] = $merchant_business_type;
		}
		
		$this->load->view('/merchantaccount/viewbusinesshours', $data);
	}
	public function bookingslist()
	{
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();			
			if($logged_user_access_type=='2'  || ($logged_user_access_type=='3' && in_array("6",$data['logged_user_access_sections'])) )
			{
				
			}else{				
				redirect('/home', 'refresh');
				exit();
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$offer_search="";
		$offer_search = $this->input->post('offer_search');
		$data['offer_search']=$offer_search;		
		$data['title']='Services on demand - Ondi.com';
		$data['current_bookings_dates'] = $this->users->get_current_bookings_dates($offer_search);
		$this->load->view('/merchantaccount/bookingslist', $data);
	}
	public function editbookingtime()
	{		
		
		// $merchant_id=$this->CI_auth->logged_id();
		$merchant_id=$this->users->get_main_merchant_id();

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();			
			if($logged_user_access_type=='2'  || ($logged_user_access_type=='3' && in_array("7",$data['logged_user_access_sections'])) )
			{
				
			}else{				
				redirect('/home', 'refresh');
				exit();
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$booking_date = $this->input->post('booking_date');

		$booking_time = $this->input->post('booking_time');
		$booking_time_ampm = $this->input->post('booking_time_ampm');		
		$offer_id = $this->input->post('offer_id'); 
		$id = $this->input->post('id'); // order_id
		
		$booking_date = $this->input->post('booking_date');
		if($booking_date!="")
		{
			$booking_date_arr = explode("/",$booking_date);
			
			$d='';
			$m='';
			$y='';
			if(isset($booking_date_arr[0]))
			{
				$d = $booking_date_arr[0];
			}
			if(isset($booking_date_arr[1]))
			{
				$m = $booking_date_arr[1];
			}
			if($booking_date_arr[2])
			{
				$y = $booking_date_arr[2];
			}
			if($d!='' && $m!='' && $y!='')
			{							
				$booking_date = $y.'-'.$m.'-'.$d;
			}					

		}

		if(!empty($booking_time))
		{
			$booking_time = date("Hi", strtotime($booking_time.' '.$booking_time_ampm));
		}
		
		$data_update = array('booking_date'=>$booking_date, 'booking_time'=>$booking_time, 'booking_time_ampm'=>$booking_time_ampm);		
		
		/*
		echo '<pre>';
		print_r($data_update);
		echo '</pre>';
		*/
		
		$this->db->where(array('order_id'=>$id, 'offer_id'=>$offer_id));		
		$this->db->update('orders', $data_update);
		
		echo 'Updated successfully.';

	}
	public function bookingshistory()
	{
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();

			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			if($logged_user_access_type=='2'  || ($logged_user_access_type=='3' && in_array("8",$data['logged_user_access_sections'])) )
			{
				
			}else{				
				redirect('/home', 'refresh');
				exit();
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$offer_search="";
		$offer_search = $this->input->post('offer_search');
		$data['offer_search']=$offer_search;		
		
		$data['title']='Services on demand - Ondi.com';

		$data['history_bookings_dates'] = $this->users->get_history_bookings_dates($offer_search);		
		$this->load->view('/merchantaccount/bookingshistory', $data);
	}
	public function notificationoptions()
	{
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();			
		$logged_user_access_sections = array();		
		if($logged_user_access_type=='3')
		{
			$logged_user_access_sections = $this->users->get_user_access_sections();
		}

		/* USER ACCESS TYPE CHECK DATA END */

		// $logged_user_id = $this->CI_auth->logged_id();
		$logged_user_id = $this->users->get_main_merchant_id();

		if($this->input->post('notification_action'))
		{

			$notification_options='';
			if($this->input->post('notification_options')>0)
			{					
				$notification_options = implode(",",$this->input->post('notification_options'));
				$notification_data = array('merchant_notification_options	' => $notification_options );		
				$this->db->where('user_id', $logged_user_id);
				$this->db->update('users', $notification_data);		
			}

		}		
		
		$notification_options = $this->users->get_notification_options();
		$merchant_notification_options='';
		$user_detail = $this->users->get_user_detail($logged_user_id);
		
		/*
			echo '<pre>';
			print_r($user_detail);
			echo '</pre>';
		*/

		$merchant_notification_options_arr = array();
		$merchant_notification_options = $user_detail[0]->merchant_notification_options;
		if($merchant_notification_options!="")
		{
			$merchant_notification_options_arr = explode(",",$merchant_notification_options);
		}
		
		$data=array('title'=>'Notification options', 'logged_user_access_type'=>$logged_user_access_type, 'logged_user_access_sections'=>$logged_user_access_sections, 'notification_options'=>$notification_options, 'merchant_notification_options'=>$merchant_notification_options_arr);

		$this->session->set_flashdata('user_message', 'Updated successfully.');
		redirect('/merchantaccount/notificationoptions/', 'refresh');

		$this->load->view('/merchantaccount/notificationoptions', $data);
	}
	
	public function viewbusinessprofile()
	{
		$user_id=''; // hard coded as of now
		
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		
		$this->db->select('merchant_steps_completed');
		$merchant_steps_completed_query = $this->db->get_where('users', array('user_id' => $user_id));
		$data_merchant_steps_completed = $merchant_steps_completed_query->result();
		$merchant_steps_completed = $data_merchant_steps_completed[0]->merchant_steps_completed;
		if($merchant_steps_completed == '0')
		{
			redirect('/merchantaccount/contactinfo', 'refresh');
		}


		$this->db->select('a.*, b.state as state_name, c.pg_name, d.pf_name');
		$this->db->from('merchant_businessprofile as a');
		$this->db->join('states as b', 'a.state = b.state_id', 'left');
		$this->db->join('price_guide as c', 'a.price_guide = c.pg_id', 'left');
		$this->db->join('perfect_for as d', 'a.perfect_for = d.pf_id', 'left');

		$condition=array('a.user_id' => $user_id);
		$this->db->where($condition);
		$userquery = $this->db->get();
		
		$userdata = $userquery->result();
		$business_type = $userdata[0]->business_type;
		$service_offered = $userdata[0]->service_offered;
		$perfect_for = $userdata[0]->perfect_for;

		if($business_type != '')
		{
			$business_type_array = explode(",", $business_type);
			$this->db->select('type_name');
			$this->db->from('business_types');

			$bussiness_condition=array('delete_status' => '0'); // as per new update.
			$this->db->where($bussiness_condition);

			$this->db->where_in('type_id', $business_type_array);
			$business_typequery = $this->db->get();
			$data_business_type = $business_typequery->result();
		}

		if($service_offered != '')
		{
			$service_offered_array = explode(",", $service_offered);
			$this->db->select('option_value');
			$this->db->from('business_types_options');
			$this->db->where_in('id', $service_offered_array);
			$service_offeredquery = $this->db->get();
			$data_service_offered = $service_offeredquery->result();
		}

		if($perfect_for != '')
		{
			$perfect_for_array = explode(",", $perfect_for);
			$this->db->select('pf_name');
			$this->db->from('perfect_for');
			$this->db->where_in('pf_id', $perfect_for_array);
			$perfect_forquery = $this->db->get();
			$data_perfect_for = $perfect_forquery->result();
		}

		//print_r($data_perfect_for);

		//$this->db->select('*');
		//$opening_hours_query = $this->db->get_where('merchant_opening_hours', array('user_id' => $user_id));
		
		$condition=array('a.user_id' => $user_id);
		$this->db->select('a.*');
		$this->db->from('merchant_opening_hours as a');
		$this->db->join('days as b', 'b.day_name = a.day', 'left');
		//$this->db->order_by("b.day_display_order", "asc");
		$this->db->order_by('b.day_display_order asc, a.id asc');
		$this->db->where($condition);
		$opening_hours_query = $this->db->get();


		
		$data = array('userdata' => $userdata, 'logged_user_access_type'=>$logged_user_access_type, 'data_business_type' => $data_business_type, 'data_service_offered' => $data_service_offered, 'opening_hours_query' => $opening_hours_query->result(), 'data_perfect_for' => $data_perfect_for);	

		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			
		}
		/* USER ACCESS TYPE CHECK DATA END */

		$data['title'] = 'Services on demand - Ondi.com';
		$this->load->view('/merchantaccount/viewbusinessprofile', $data);
	}

	public function viewcontactinfo()
	{	
		
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
			if(!in_array("1",$data['logged_user_access_sections']))
			{
				redirect('/merchantaccount/welcomeuser', 'refresh');	
			}
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();		
		$user_id=$this->users->get_main_merchant_id();
		
		$data['title'] = 'Services on demand - Ondi.com';	
		
		$this->db->select('email, position, position_other, phone, contact_name, terms_condition, merchant_steps_completed');
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		$data['user_data'] = $query->result();		
		$merchant_steps_completed=$data['user_data'][0]->merchant_steps_completed;
		

		$this->load->view('/merchantaccount/viewcontactinfo', $data);		

	}

	public function viewbankingdetails()
	{	
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$user_id='';
		// $user_id=$this->CI_auth->logged_id();
		$user_id=$this->users->get_main_merchant_id();
		$data['title'] = 'Services on demand - Ondi.com';
		
		$this->db->select('branch_name, account_name, bsb, account_number, abn, merchant_steps_completed');
		$query = $this->db->get_where('users', array('user_id' => $user_id));
		$data['user_data'] = $query->result();
		$merchant_steps_completed=$data['user_data'][0]->merchant_steps_completed;	
		if($merchant_steps_completed==1)
		{
			redirect('/merchantaccount/businessprofile', 'refresh');
		}
		$this->load->view('/merchantaccount/viewbankingdetails', $data);

	}

	public function mynotifications()
	{
		/* USER ACCESS TYPE CHECK DATA START */
		$logged_user_access_type = $this->users->get_user_access_type();
		$data['logged_user_access_type'] = $logged_user_access_type;
		$data['logged_user_access_sections'] = array();
		if($logged_user_access_type=='3')
		{
			$data['logged_user_access_sections'] = $this->users->get_user_access_sections();
		}
		/* USER ACCESS TYPE CHECK DATA END */
		
		$user_notifications_data = $this->users->get_user_notification_data();
		$unread_notifications = $user_notifications_data['unread'];
		$notifications = $user_notifications_data['notifications_data'];
		//$data = array('notifications' =>  $notifications);
		$data['notifications'] = $notifications;
		$this->load->view('/merchantaccount/mynotifications', $data);
	}

	public function invoice()
	{
		if(isset($_GET['bid']))
		{
			$bid = base64_decode($_GET['bid']);
		}
		$condition=array('a.order_id' => $bid);
		$this->db->select('a.*, b.offer_title, b.price, b.offer_description, c.business_name, c.website_address, c.street_address, c.suburb, c.post_code, d.state');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->join('states as d', 'c.state = d.state_id', 'left');
		$this->db->where($condition);
		$orders_query = $this->db->get();
		$data = array('dataorders' =>  $orders_query->result());
		$this->load->view('/customeraccount/invoice', $data);
	}	

}
?>