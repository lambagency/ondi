<?php
class Customeraccount extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session', 'ip2location', 'customclass', 'pdfcrowd'));
		$this->load->helper(array('form', 'url'));
		//$this->load->library('encrypt');
		$this->load->database();
		$this->load->model(array('users', 'offers'));		
		$this->load->model(array('users','CI_auth','CI_encrypt','Admin_Model'));

		if($this->CI_auth->check_logged()===FALSE)
		{			
			$this->CI_auth->redirect_nonlogged_user();

		}else{

			$sess_user_data = $this->session->all_userdata();
			if($sess_user_data['user_type']!='1')
			{
				$this->CI_auth->redirect_nonlogged_user();				
			}
			
		}
	}
	public function index()
	{		
		
	}
	public function editcontactinfo()
	{	
		$user_id='';
		$user_id=$this->CI_auth->logged_id();

		$data['title'] = 'CONTACT INFO ONDI FOOD';

		$this->db->select('user_id, first_name, last_name, email, mobile_number, postcode, birthday, customer_picture, gender, subscribe_newsletter, interests, confirmation_methods');
		$userquery = $this->db->get_where('users', array('user_id' => $user_id));

		$this->db->select('id, interest');
		$condition=array('delete_status' => '0');	
		$this->db->where($condition);
		$interestquery = $this->db->get('user_interests');

		$this->db->select('type_id, cctype, icon');
		$cctypequery = $this->db->get('creditcard_types');


		$this->db->select('id, cc_category');
		$cccategoryquery = $this->db->get('creditcard_categories');

		//$this->db->select('*');
		//$creditcard_details_query = $this->db->get_where('creditcard_details', array('user_id' => $user_id)); 
		//print_r($creditcard_details_query); die;

		$condition=array('a.user_id' => $user_id);
		$this->db->select('a.id, a.display_number, a.expiry_month, a.expiry_year, a.cc_category, a.scheme, b.cc_category as category');
		$this->db->from('user_creditcard_refernces as a');
		$this->db->join('creditcard_categories as b', 'a.cc_category = b.cc_category', 'left');
		$this->db->where($condition);
		$creditcard_details_query = $this->db->get();		



		$data = array('userdata' => $userquery->result(), 'interestdata' => $interestquery->result(), 'cctypedata' => $cctypequery->result(), 'cccarddata' => $creditcard_details_query->result(), 'cccategoryquery' => $cccategoryquery->result());	

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');		
		$this->form_validation->set_rules('email', 'Email (your login)', 'trim|required|valid_email');		
		$this->form_validation->set_rules('mobile_number', 'Mobile Number', 'trim|required');
		$this->form_validation->set_rules('postcode', 'Post Code', 'trim|required');
		$this->form_validation->set_rules('gender', 'Gender', 'trim|required');
		
		if($this->input->post('password') != ""){
			$this->form_validation->set_rules('password', 'Password', 'trim|matches[cpassword]');
		}
		
		if(isset($_POST))
		{
			if ($this->form_validation->run() == FALSE) {
				//$this->load->view('/customeraccount/editcontactinfo', $data);
			}
		}
		

			//print_r($_FILES); die;
			
			if($this->input->post('submit')){
				
				$first_name = $this->input->post('first_name');
				$last_name = $this->input->post('last_name');
				$email = $this->input->post('email');
				$mobile_number = $this->input->post('mobile_number');
				$postcode = $this->input->post('postcode');
				$birthday = $this->input->post('bday');
				$password = $this->input->post('password');


				if($password != '')
				{
					$encrypt_pass="";
					$rand_salt = $this->CI_encrypt->genRndSalt();					
					$encrypt_pass = $this->CI_encrypt->encryptUserPwd($password, $rand_salt);
				}

				if($birthday != '')
				{
					list($dd,$mm,$yy) = explode('/',$birthday);
					$birthday = sprintf('%s/%s/%s',$mm,$dd,$yy);
					if($birthday != '0000-00-00')
					{
						$birthday = date("Y-m-d", strtotime($birthday));
					}
					else
					{
						$birthday = "0000-00-00";
					}
				}
				else
				{
					$birthday = "0000-00-00";
				}

				//echo "birthday--->".$birthday; die;

				
				$gender = $this->input->post('gender');
				$old_profile_picture = $this->input->post('old_profile_picture');
				$subscribe_newsletter = $this->input->post('subscribe_newsletter');

				$interests_array = $this->input->post('interests');
				if(is_array($interests_array))
				{
					$interests = implode(",", $interests_array);
				}
				else
				{
					$interests = '';
				}

				/*$confirmation_methods_array = $this->input->post('confirmation_methods');
				if(is_array($confirmation_methods_array))
				{
					$confirmation_methods = implode(",", $confirmation_methods_array);
				}
				else
				{
					$confirmation_methods = '';
				}*/
				$confirmation_methods = 'email';


				/*$config_image['upload_path'] = 'public/uploads/';
				$config_image['allowed_types'] = 'gif|jpg|png|jpeg';
				$config_image['max_size']	= '500';
				$config_image['max_width']  = '150';
				$config_image['max_height']  = '150';
				$this->load->library('upload', $config_image);
				$this->upload->initialize($config_image);
				$this->upload->set_allowed_types('*');
				
				//print_r($_FILES['profile_picture']['size']); die;
				if($_FILES['profile_picture']['size'] != '')
				{
					if ($this->upload->do_upload('profile_picture')) {
						$data_profile_picture = $this->upload->data();
						$image_profile_picture = $data_profile_picture['file_name'];
						$profile_picture = $config_image['upload_path'].$data_profile_picture['file_name'];
					}
					else
					{
						//$profile_picture = $old_profile_picture;
						//$data['uploaderror'] = 'Please upload image of Max Size 150px X 150px, Max 500KB';
						//$this->load->view('/customeraccount/editcontactinfo', $data);
						redirect('/customeraccount/contactinfo/?upload=error', 'refresh');
					}
				}
				else
				{
					$profile_picture = $old_profile_picture;
				}*/

				$temp_profile_picture = $this->input->post('temp_profile_picture');
				$sql_temp_pp = "SELECT * FROM temp_images WHERE id ='".$temp_profile_picture."' ";
				$exe_temp_pp = $this->db->query($sql_temp_pp);
				$count_temp_pp = $exe_temp_pp->num_rows();
				if($count_temp_pp>0)
				{
					foreach($exe_temp_pp->result() as $tmp)
					{
						$profile_picture = $tmp->img_path;
					}
				}

				if($profile_picture == "") 
				{
					$profile_picture = $old_profile_picture;
				}


				//echo "profile_picture-->".$profile_picture; die;

				if($password != '')
				{
					$data = array(
						   'first_name' => $first_name,
						   'last_name' => $last_name,
						   'email' => $email,
						   'mobile_number' => $mobile_number,
						   'postcode' => $postcode,
						   'birthday' => $birthday,
						   'gender' => $gender,
						   'customer_picture' => $profile_picture,
						   'subscribe_newsletter' => $subscribe_newsletter,
						   'interests' => $interests,
						   'password' => $encrypt_pass,
						   'salt'=>$rand_salt,
						   'confirmation_methods' => $confirmation_methods
						);
				}
				else
				{
					$data = array(
						   'first_name' => $first_name,
						   'last_name' => $last_name,
						   'email' => $email,
						   'mobile_number' => $mobile_number,
						   'postcode' => $postcode,
						   'birthday' => $birthday,
						   'gender' => $gender,
						   'customer_picture' => $profile_picture,
						   'subscribe_newsletter' => $subscribe_newsletter,
						   'interests' => $interests,
						   'confirmation_methods' => $confirmation_methods
						);
				}
				//print_r($data); die;
				$this->db->where('user_id', $user_id);
				$this->db->update('users', $data);


				///////////////  CREDIT CARD DETAILS START ///////////////////
				$num_cards = $this->input->post('num_cards');
				//echo $num_cards; die;
				for($no=1; $no<=$num_cards; $no++)
				{
					$cctype = $this->input->post('cctype_'.$no);
					$card_no = $this->input->post('card_no_'.$no);
					$card_name = $this->input->post('card_name_'.$no);
					$exp_month = $this->input->post('exp_month_'.$no);
					$exp_year = $this->input->post('exp_year_'.$no);
					$cvv = $this->input->post('cvv_'.$no);
					$cc_category = $this->input->post('cc_category_'.$no);

					$cc_address = $this->input->post('cc_address_'.$no);
					$cc_city = $this->input->post('cc_city_'.$no);
					$cc_country = $this->input->post('cc_country_'.$no);

					//echo $cc_category; die;
					
					if($card_no != '')
					{
						$test = true;
						$auth['u'] = (($test === true) ? 'LctjEveidUI8_GA9JO_r6w' : 'xxxxLivePrivateKeyxxxx'); //Private Key from PIN
						$auth['p'] = ''; //API calls use empty password
						//$url_card = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/cards';
						$url_card = 'https://' . (($test === true) ? 'test-' : '') . 'api.pin.net.au/1/customers';


						$post_card = array();
						$post_card['email'] = $email;
						$post_card['card[number]'] = $card_no;
						$post_card['card[expiry_month]'] = $exp_month;
						$post_card['card[expiry_year]'] = $exp_year;
						$post_card['card[cvc]'] = $cvv;
						$post_card['card[name'] = $card_name;
						$post_card['card[address_line1'] = $cc_address;
						//$post_card['address_line2'] = ""
						$post_card['card[address_city'] = $cc_city;
						//$post_card['address_postcode'] = "";
						//$post_card['address_state'] = "";
						$post_card['card[address_country'] = $cc_country;
						//print_r($post_card); die;

						// Create a curl handle
						$ch = curl_init($url_card);
						curl_setopt($ch, CURLOPT_USERPWD, $auth['u'] . ":" . $auth['p']); //authenticate
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); //make sure it returns a response
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // allow https verification if true
						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // allow https verification if true
						curl_setopt($ch, CURLOPT_POST, 1); //tell it we are posting
						curl_setopt($ch, CURLOPT_POSTFIELDS, $post_card); //tell it what to post
						curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json')); //response comes back as json
						$response_card = curl_exec($ch);
						if(!curl_errno($ch)){$info = curl_getinfo($ch);}
						curl_close($ch);
						$response_card = json_decode($response_card, true);
						if((bool)$this->config->item('test_mode'))
						{
							//print_r($response_card);
							//die;
						}
						
						
						if(!empty($response_card['response']['token']) && $response_card['response']['token'] != "")
						{	
							$cust_token = $response_card['response']['token'];
							$cust_email = $response_card['response']['email'];

							
							$cc_token = $response_card['response']['card']['token'];
							$display_number = $response_card['response']['card']['display_number'];
							$expiry_month = $response_card['response']['card']['expiry_month'];
							$expiry_year = $response_card['response']['card']['expiry_year'];
							$scheme = $response_card['response']['card']['scheme'];
							$card_owner = $response_card['response']['card']['name'];
							$card_address_line1 = $response_card['response']['card']['address_line1'];
							$card_address_line2 = $response_card['response']['card']['address_line2'];
							$card_address_city = $response_card['response']['card']['address_city'];
							$card_address_postcode = $response_card['response']['card']['address_postcode'];
							$card_address_state = $response_card['response']['card']['address_state'];
							$card_address_country = $response_card['response']['card']['address_country'];


							$user_id=$this->CI_auth->logged_id();
							$sql_insert_cc = "INSERT INTO user_creditcard_refernces(user_id, ref_id, cc_category, display_number, expiry_month, expiry_year, scheme, card_owner, added_date, cust_token, cust_email, address_line1, address_line2, address_city, address_postcode, address_state, address_country) VALUES ('".$user_id."', '".$cc_token."', '".$cc_category."', '".$display_number."', '".$expiry_month."', '".$expiry_year."', '".$scheme."', '".$card_owner."', now(), '".$cust_token."', '".$cust_email."', '".$card_address_line1."', '".$card_address_line2."', '".$card_address_city."', '".$card_address_postcode."', '".$card_address_state."', '".$card_address_country."')";
							$query_insert_cc = $this->db->query($sql_insert_cc);
						}
		
						if(!empty($response_card['error']) && $response_card['error'] == "invalid_resource")
						{
							//print_r($response_card);
							//die;
							$this->session->set_flashdata('error_message', 'Please enter all the mandatory card details.');		
							redirect('/customeraccount/editcontactinfo', 'refresh');		
						}



					}

				}
				/////////////// CREDIT CARD DETAILS END ///////////////////


				redirect('/customeraccount/contactinfo', 'refresh');	
				//$this->load->view('/customeraccount/editcontactinfo', $data);
			}	
			else
			{
				$this->load->view('/customeraccount/editcontactinfo', $data);
			}
			
	}

	public function contactinfo()
	{	
		$user_id='';
		$user_id=$this->CI_auth->logged_id();

		$data['title'] = 'CONTACT INFO ONDI FOOD';
		

		$this->db->select('first_name, last_name, email, mobile_number, postcode, birthday, customer_picture, gender, subscribe_newsletter, interests, confirmation_methods, facebook_id');
		$userquery = $this->db->get_where('users', array('user_id' => $user_id));

		//$this->db->select('*');
		//$creditcard_details_query = $this->db->get_where('creditcard_details', array('user_id' => $user_id)); 
		$condition=array('a.user_id' => $user_id, 'a.added_date >' => date("Y-m-d", strtotime('-30 days')));
		$this->db->select('a.id, a.display_number, a.expiry_month, a.expiry_year, a.cc_category, a.scheme, b.cc_category as category');
		$this->db->from('user_creditcard_refernces as a');
		$this->db->join('creditcard_categories as b', 'a.cc_category = b.cc_category', 'left');
		$this->db->where($condition);
		$creditcard_details_query = $this->db->get();		
		//echo $this->db->last_query(); die;


		$data = array('userdata' => $userquery->result(), 'cccarddata' => $creditcard_details_query->result());	

		$this->load->view('/customeraccount/contactinfo', $data);
	}


	/*public function shortlists1()
	{	
		$user_id='8';
		$data['title'] = 'SHORTLISTS ONDI FOOD';

		$condition=array('a.user_id' => $user_id);
		$this->db->select('a.offer_id, b.offer_title, b.offer_description, b.price, b.price_normally, c.business_name, c.suburb, c.business_type, d.pg_name');
		$this->db->from('user_shortlists as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'c.user_id = b.merchant_id', 'left');
		$this->db->join('price_guide as d', 'd.pg_id = c.price_guide', 'left');
		$this->db->where($condition);
		$shortlist_details_query = $this->db->get();		

		$data = array('shortlistdata' => $shortlist_details_query->result());	
		$this->load->view('/customeraccount/shortlists', $data);
	}*/


	public function shortlists()
	{	
		$user_id='';
		$user_id=$this->CI_auth->logged_id();

		$data['title'] = 'ONDI FOOD';
		

		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];


		$sql_count = "SELECT offer_id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND session_id = '".$session_id."' ";
		$query_count = $this->db->query($sql_count);
		$count = $query_count->num_rows();
		if($count>0)
		{
			$shortlist_offers =  array();
			foreach($query_count->result() as $tmp)
			{
				$oid = $tmp->offer_id;
				if($user_id != '')
				{
					$sql_count_shortlist = "SELECT sid FROM user_shortlists WHERE user_id = '".$user_id."' AND offer_id = '".$oid."' ";
					$query_count_shortlist = $this->db->query($sql_count_shortlist);
					$count_shortlist = $query_count_shortlist->num_rows();
					if($count_shortlist==0)
					{
						$sql_insert = "INSERT INTO user_shortlists(user_id, offer_id ) VALUES ('".$user_id."', '".$oid."')";
						$query_count = $this->db->query($sql_insert);

						$sql_delete = "DELETE FROM temp_shortlists WHERE ip_add = '".$ip_add."' AND session_id = '".$session_id."' AND offer_id = '".$oid."' ";
						$query_delete = $this->db->query($sql_delete);
					}
				}
			}
		}
		$condition=array('user_id' => $user_id);
		$this->db->select('offer_id');
		$this->db->from('user_shortlists');
		$this->db->where($condition);
		$shortlist_details_query = $this->db->get();	
		$shortlist_offers = $shortlist_details_query->result();
		
		$return_array = array();
		
		foreach($shortlist_offers as $this_offer)
		{
			$this_offer_details = $this->offers->get_offer_details($this_offer->offer_id);
			array_push($return_array, $this_offer_details);
		}

		$data = array('shortlistdata' => $return_array);	
		$this->load->view('/customeraccount/shortlists', $data);
	}
	
	public function removeshortlist()
	{
		if(isset($_GET['oid']))
		{
			$oid = $_GET['oid'];
			$user_id='';
			$user_id=$this->CI_auth->logged_id();
			
			$tables = array('user_shortlists');
			$condition=array('user_id' => $user_id, 'offer_id' => $oid);
			//print_r($condition);
			$this->db->where($condition);
			
			$this->db->delete($tables);
			//$this->db->last_query(); die;
				
			redirect('/customeraccount/shortlists', 'refresh');
		}
		else
		{
			redirect('/customeraccount/shortlists', 'refresh');
		}
	}

	public function emailpreferences()
	{
		$user_id='';
		$user_id=$this->CI_auth->logged_id();

		$data['title'] = 'ONDI FOOD';

		if($this->input->post('submit')){
			$subscribe_lastminuteoffers = '0';
			$subscribe_occasions = '0';
			$subscribe_news_updates = '0';
			$subscribe_rewards = '0';
			$subscribe_surveys = '0';
			$subscribe_updated_on = date("Y-m-d");
			
			//print_r($_POST); die;

			$unsubscribefromall = $this->input->post('unsubscribefromall');
			if($unsubscribefromall == '')
			{
				$subscribe_lastminuteoffers = $this->input->post('subscribe_lastminuteoffers');
				$subscribe_occasions = $this->input->post('subscribe_occasions');
				$subscribe_news_updates = $this->input->post('subscribe_news_updates');
				$subscribe_rewards = $this->input->post('subscribe_rewards');
				$subscribe_surveys = $this->input->post('subscribe_surveys');
			}

			$data_update = array(
				   'subscribe_lastminuteoffers' => $subscribe_lastminuteoffers,
				   'subscribe_occasions' => $subscribe_occasions,
				   'subscribe_news_updates' => $subscribe_news_updates,
				   'subscribe_rewards' => $subscribe_rewards,
				   'subscribe_surveys' => $subscribe_surveys,
				   'subscribe_updated_on' => $subscribe_updated_on
			);
			$this->db->where('user_id', $user_id);
			$this->db->update('users', $data_update);
		}

		$this->db->select('subscribe_lastminuteoffers, subscribe_occasions, subscribe_news_updates, subscribe_rewards, subscribe_surveys, subscribe_updated_on, subscribe_newsletter');
		$userquery = $this->db->get_where('users', array('user_id' => $user_id));
		$data = array('userdata' => $userquery->result());	
		$this->load->view('/customeraccount/emailpreferences', $data);	
	}

	public function mybookings()
	{
		$user_id='';
		$user_id=$this->CI_auth->logged_id();
		$client = new Pdfcrowd("chanchal", "3f184b96ded6ac6d2170304278d59025"); // only 100 tokens are allowed
		// CURRENT ORDERS //
		//$condition=array('a.user_id' => $user_id, 'b.offer_running_to >=' => date("Y-m-d"), 'a.order_status' => '1', 'a.booking_code != ' => '');
		$condition=array('a.user_id' => $user_id, 'a.booking_date >=' => date("Y-m-d"), 'a.order_status' => '1', 'a.booking_code != ' => '');

		$this->db->select('a.order_id, a.offer_id, a.number_of_people, a.booking_date, a.booking_time, a.total_price as total_price, a.booking_code, a.order_date, a.discount as discount, b.offer_title, , b.merchant_id, b.price as price, b.offer_running_to, c.business_name, c.profile_picture, c.street_address, c.suburb, c.state, c.post_code, a.invoice_file_name');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		
		$this->db->where($condition);
		$currentorders_query = $this->db->get();
		//echo $this->db->last_query(); //die;
		$currentorders = $currentorders_query->result();
		
		//$condition1=array('a.user_id' => $user_id, 'b.offer_running_to <' => date("Y-m-d"));
		$condition1=array('a.user_id' => $user_id);
		
		$search_post_data = array();
		$search_post_data['keyword'] = '';
		$search_post_data['filter_month'] = '';
		$search_post_data['date_from'] = '';
		$search_post_data['date_to'] = '';
		$keyword = '';

		$condition1['a.order_status = '] = '1' ;
		$condition1['a.booking_code != '] = '' ;
		$condition1['a.booking_date < '] = date('Y-m-d');

		if($this->input->post('filterbooking')){
			$keyword = $this->input->post('keyword');
			$filter_month = $this->input->post('filter_month');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');

			$search_post_data['keyword'] = $keyword;
			$search_post_data['filter_month'] = $filter_month;
			$search_post_data['date_from'] = $date_from;
			$search_post_data['date_to'] = $date_to;
			
			//print_r($_POST);
			//$condition1['a.order_status = '] = '1' ;
			//$condition1['a.booking_code != '] = '' ;
			
			if($keyword != '')
			{
				//$condition1['b.offer_title like '] = "%".$keyword."% " ;
				//$condition1['c.business_name like '] = "%".$keyword."%" ;
			}
			if($filter_month != '')
			{
				$condition1['MONTH(a.booking_date) = '] = $filter_month ;
			}
			if($date_from != '')
			{
				$explode_from = explode("/", $date_from);
				$new_date_from = $explode_from[2]."-".$explode_from[1]."-".$explode_from[0];
				$search_post_data['date_from'] = $new_date_from;
				$condition1['a.booking_date >= '] = date('Y-m-d', strtotime($new_date_from));
			}
			if($date_to != '')
			{
				$explode_to = explode("/", $date_to);
				$new_date_to = $explode_to[2]."-".$explode_to[1]."-".$explode_to[0];
				$search_post_data['date_to'] = $new_date_to;
				$condition1['a.booking_date <= '] = date('Y-m-d', strtotime($new_date_to));
			}
		}
		//print_r($condition1);
		// BOOKING HISTORY //
		

		$this->db->select('a.order_id, a.offer_id, a.number_of_people, a.booking_date, a.booking_time, a.total_price as total_price, a.booking_code, a.order_date, a.discount as discount, b.offer_title, , b.merchant_id, b.price as price, b.offer_running_to, c.business_name, c.profile_picture, c.street_address, c.suburb, c.state, c.post_code, a.invoice_file_name');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->where($condition1);
		if($keyword != '')
		{
			$this->db->where("(b.offer_title LIKE '%".$keyword."%' OR c.business_name LIKE '%".$keyword."%')", NULL, FALSE);
		}
		$this->db->order_by("a.booking_date", "desc");
		$pastorders_query = $this->db->get();
		
		if((bool)$this->config->item('test_mode'))
		{
			//print_r($condition1);
			//echo $this->db->last_query(); die;
		}

		$pastorders = $pastorders_query->result();


		
		$data = array('currentorders' => $currentorders, 'pastorders' => $pastorders, 'search_post_data' => $search_post_data);
		
		$this->load->view('/customeraccount/mybookings', $data);	
	}
	

	public function sortrecord()
	{
		$column = $this->input->post('column');
		$type = $this->input->post('type');
		$orderby = $this->input->post('orderby');
		$user_id=$this->CI_auth->logged_id();

		$offer_column_order = 'asc';
		$booking_date_column_order = 'asc';
		$booking_time_column_order = 'asc';
		$number_of_people_column_order = 'asc';
		$total_price_column_order = 'asc';
		$return_string  = '';

		if($column =='offer')
		{
			if($orderby =='asc')
			{
				$this->db->order_by("b.offer_title", "asc");
				$offer_column_order = 'desc';
			}
			else if($orderby =='desc')
			{
				$this->db->order_by("b.offer_title", "desc");
				$offer_column_order = 'asc';
			}
		}
		else if($column =='booking_date')
		{
			if($orderby =='asc')
			{
				$this->db->order_by("a.booking_date", "asc");
				$booking_date_column_order = 'desc';
			}
			else if($orderby =='desc')
			{
				$this->db->order_by("a.booking_date", "desc");
				$booking_date_column_order = 'asc';
			}
		}
		else if($column =='booking_time')
		{
			if($orderby =='asc')
			{
				$this->db->order_by("a.booking_time", "asc");
				$booking_time_column_order = 'desc';
			}
			else if($orderby =='desc')
			{
				$this->db->order_by("a.booking_time", "desc");
				$booking_time_column_order = 'asc';
			}
		}
		else if($column =='number_of_people')
		{
			if($orderby =='asc')
			{
				$this->db->order_by("a.number_of_people", "asc");
				$number_of_people_column_order = 'desc';
			}
			else if($orderby =='desc')
			{
				$this->db->order_by("a.number_of_people", "desc");
				$number_of_people_column_order = 'asc';
			}
		}
		else if($column =='total_price')
		{
			if($orderby =='asc')
			{
				$this->db->order_by("a.total_price", "asc");
				$total_price_column_order = 'desc';
			}
			else if($orderby =='desc')
			{
				$this->db->order_by("a.total_price", "desc");
				$total_price_column_order = 'asc';
			}
		}


		if($type =='current')
		{
			$condition=array('a.user_id' => $user_id, 'b.offer_running_to >=' => date("Y-m-d"));
			$this->db->select('a.order_id, a.offer_id, a.number_of_people, a.booking_date, a.booking_time, a.total_price, a.booking_code, a.order_date, a.discount, b.offer_title, b.price, b.offer_running_to, c.business_name, c.profile_picture, c.street_address, c.suburb, c.state, c.post_code');
			$this->db->from('orders as a');
			$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
			$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
			$this->db->where($condition);
			$currentorders_query = $this->db->get();
			$currentorders = $currentorders_query->result();
			
			$return_string  .= '<div class="head"><table class="table_customer"><tr class="table_head"><td><div class="offer_head" onclick="sortrecord(\'offer\', \'current\', \''.$offer_column_order.'\');">Offer</div></td><td><div class="booked_head" onclick="sortrecord(\'booking_date\', \'current\', \''.$booking_date_column_order.'\');">Booked for</div></td><td><div class="time_head"  onclick="sortrecord(\'booking_time\', \'current\', \''.$booking_time_column_order.'\');">Time</div></td><td><div class="quantity_head"   onclick="sortrecord(\'number_of_people\', \'current\', \''.$number_of_people_column_order.'\');">Quantity</div></td><td><div class="total_head"  onclick="sortrecord(\'total_price\', \'current\', \''.$total_price_column_order.'\');">Total</div></td><td class="no_bg"><div class="status_head">Status</div></td><td class="no_bg"><div class="blank_head">&nbsp;</div></td></tr></table></div><div class="detail"><table class="table_customer">';
			foreach($currentorders as $thecurrent)
			{
				$return_string  .= '<tr class="new_col_blank"><td colspan="4"><div class="blank_div">&nbsp;&nbsp;&nbsp;</div></td></tr><tr><td class="left_table_column"><div class="offer_head">'.$thecurrent->business_name.'<br/> <strong>'.$thecurrent->offer_title.' $'.$thecurrent->price.'</strong></div></td><td class="abc"><div class="booked_head">'.date("l, M jS, Y", strtotime($thecurrent->booking_date)).'</div><div class="time_head">'.$thecurrent->booking_time.'</div><div class="quantity_head">'.$thecurrent->number_of_people.' pax</div><div class="total_head">$'.$thecurrent->total_price.'</div></td><td class="book_btn"><div class="status_head"><a href="javascript:void(0);">Booked</a></div></td><td><div class="blank_head"><div class="buttons"><span class="share"><span class="nav_social"><a  target="_blank" href="http://www.facebook.com/sharer.php?u='.$this->offers->get_offer_url($thecurrent->offer_id).'&t='.urlencode($thecurrent->offer_title).'"><img src="'.$this->config->item('base_url').'public/images/cus_fb.png" alt="" /></a><a  target="_blank" href="https://twitter.com/home?status='.urlencode($thecurrent->offer_title).'%20-%20'.$this->offers->get_offer_url($thecurrent->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_twitter.png" alt="" /></a><a  target="_blank"  href="https://plus.google.com/share?url='.$this->offers->get_offer_url($thecurrent->offer_id).'"><img src="'.$this->config->item('base_url').'public/images/cus_g+.png" alt="" /></a></span></span></div><a href="javascript:void(0);" onclick="viewmoreaboutorder('.$thecurrent->order_id.');" id="ancviewmoreaboutorder_'.$thecurrent->order_id.'" ><img src="'.$this->config->item('base_url').'public/images/up_cus_share1.png" alt="" /></a></div></td></tr><tr class="new_col"><td colspan="4"><div class="add_calander" id="moredetailorder_'.$thecurrent->order_id.'" style="display:none;"><div class="left_img"><img src="'.$this->config->item('base_url').$thecurrent->profile_picture.'" alt="" width="42" /><p><a href="#">+ Add to calendar</a> <span><a target="_blank" href="https://maps.google.com/maps?daddr='.$thecurrent->street_address.' '.$thecurrent->suburb.' '.$thecurrent->state.' '.$thecurrent->post_code.'">+ Get directions</a></span></p></div><div class="right_img"><p>Booking #: <span>'.$thecurrent->booking_code.'</span>Booked on: <span>'.date("d/m/Y", strtotime($thecurrent->order_date)).'</span>   Invoice: <span>Download</span>   Discount of: <span>$'.$thecurrent->discount.'</span></p></div></div></td></tr>';
			}
			$return_string  .= '</table></div>';
		}
		else if($type =='history')
		{
			$condition=array('a.user_id' => $user_id, 'b.offer_running_to <' => date("Y-m-d"));
			
			$keyword = $this->input->post('keyword');
			$filter_month = $this->input->post('filter_month');
			$date_from = $this->input->post('date_from');
			$date_to = $this->input->post('date_to');
			
			

			if($keyword != '')
			{
				$condition['b.offer_title like '] = "%".$keyword."%" ;
			}
			if($filter_month != '')
			{
				$condition['MONTH(a.booking_date) = '] = $filter_month ;
			}
			if($date_from != '')
			{
				$condition['a.booking_date >= '] = date('Y-m-d', strtotime($date_from));
			}
			if($date_to != '')
			{
				$condition['a.booking_date <= '] = date('Y-m-d', strtotime($date_to));
			}
			
			$this->db->select('a.order_id, a.offer_id, a.number_of_people, a.booking_date, a.booking_time, a.total_price, a.booking_code, a.order_date, a.discount, b.offer_title, b.price, b.offer_running_to, c.business_name, c.profile_picture, c.street_address, c.suburb, c.state, c.post_code');
			$this->db->from('orders as a');
			$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
			$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
			$this->db->where($condition);
			$pastorders_query = $this->db->get();
			$pastorders = $pastorders_query->result();
			
			$return_string  .= '<div class="head"><table class="table_customer"><tr class="table_head"><td><div class="offer_head" onclick="sortrecord(\'offer\', \'history\', \''.$offer_column_order.'\');" >Offer</div></td><td><div class="booked_head" onclick="sortrecord(\'booking_date\', \'history\', \''.$booking_date_column_order.'\');" >Booked for</div></td><td><div class="time_head" onclick="sortrecord(\'booking_time\', \'history\', \''.$booking_time_column_order.'\');" >Time</div></td><td><div class="quantity_head" onclick="sortrecord(\'number_of_people\', \'history\', \''.$number_of_people_column_order.'\');" >Quantity</div></td><td><div class="total_head" onclick="sortrecord(\'total_price\', \'history\', \''.$total_price_column_order.'\');" >Total</div></td><td><div class="status_head">Status</div></td><td class="no_bg"><div class="blank_head">Review</div></td></tr></table></div><div class="detail"><table class="table_customer">';

			foreach($pastorders as $thepast)
			{
				$return_string  .= '<tr class="new_col_blank"><td colspan="4"><div class="blank_div">&nbsp;&nbsp;&nbsp;</div></td></tr><tr><td class="left_table_column"><div class="offer_head">'.$thepast->business_name.' <br/><strong>'.$thepast->offer_title.' $'.$thepast->price.'</strong></div></td><td class="abc"><div class="booked_head">'.date("l, M jS, Y", strtotime($thepast->booking_date)).'</div><div class="time_head">'.$thepast->booking_time.'</div><div class="quantity_head">'.$thepast->number_of_people.' pax</div><div class="total_head">$'.$thepast->total_price.'</div></td><td class="book_btn"><div class="status_head"><a href="javascript:void(0);">Done</a></div></td><td><div class="blank_head"><div class="buttons"><div class="rate_it"><span class="rate_it_star"><img src="'.$this->config->item('base_url').'public/images/five_star.png" alt="" /></span><p><a href="'.$this->offers->get_offer_url($thepast->offer_id).'">Rate it</a></p></div></div><a href="javascript:void(0);" onclick="viewmoreaboutorder('.$thepast->order_id.');" id="ancviewmoreaboutorder_'.$thepast->order_id.'" ><img src="'.$this->config->item('base_url').'public/images/up_cus_share1.png" alt="" /></a></div></td></tr><tr class="new_col"><td colspan="4"><div class="add_calander" id="moredetailorder_'.$thepast->order_id.'" style="display:none;"><div class="left_img"><img src="'.$this->config->item('base_url').$thepast->profile_picture.'" alt="" width="42" /></div><div class="right_img"><p>Booking #: <span>'.$thepast->booking_code.'</span>    Booked on: <span>'.date("d/m/Y", strtotime($thepast->order_date)).'</span>   Invoice: <span>Download</span>   Discount of: <span>$'.$thepast->discount.'</span></p></div></div></td></tr>';

			}
			$return_string  .= '</table></div>';
		}


		echo $return_string;
	}
	
	public function removecc()
	{
		if(isset($_GET['cid']))
		{
			$cid = base64_decode($_GET['cid']);
			$user_id='';
			$user_id=$this->CI_auth->logged_id();
			$tables = array('user_creditcard_refernces');
			$condition=array('user_id' => $user_id, 'id' => $cid);
			$this->db->where($condition);
			$this->db->delete($tables);
			redirect('/customeraccount/contactinfo', 'refresh');
		}
		else
		{
			redirect('/customeraccount/contactinfo', 'refresh');
		}
	}


	public function invoice()
	{
		if(isset($_GET['bid']))
		{
			$bid = base64_decode($_GET['bid']);
		}
		$condition=array('a.order_id' => $bid);
		$this->db->select('a.*, b.offer_title, b.price, b.offer_description, c.business_name, c.website_address, c.street_address, c.suburb, c.post_code, d.state');
		$this->db->from('orders as a');
		$this->db->join('merchant_offers as b', 'a.offer_id = b.id', 'left');
		$this->db->join('merchant_businessprofile as c', 'b.merchant_id = c.user_id', 'left');
		$this->db->join('states as d', 'c.state = d.state_id', 'left');
		$this->db->where($condition);
		$orders_query = $this->db->get();
		$data = array('dataorders' =>  $orders_query->result());
		$this->load->view('/customeraccount/invoice', $data);
	}

	public function downloadinvoice()
	{
		if(isset($_GET['bid']))
		{
			$bid = $_GET['bid'];
		}
		try
		{   
			// create an API client instance
			$client = new Pdfcrowd("chanchal", "3f184b96ded6ac6d2170304278d59025"); // only 100 tokens are allowed
			// convert a web page and store the generated PDF into a $pdf variable
			//$pdf = $client->convertURI('http://www.google.com/');
			$pdf = $client->convertURI($this->config('base_url').'/customeraccount/invoice/?bid='.$bid);
			// set HTTP response headers
			//header("Content-Type: application/pdf");
			//header("Cache-Control: no-cache");
			//header("Accept-Ranges: none");
			//header("Content-Disposition: attachment; filename=\"".time()."\"");

			// send the generated PDF 
			echo $pdf;
		}
		catch(PdfcrowdException $why)
		{
			echo "Pdfcrowd Error: " . $why;
		}
	}

	public function ajaxratings()
	{
		if(isset($_POST['oid']) && isset($_POST['rating']))
		{
			$oid = $_POST['oid'];
			$rating = $_POST['rating'];
			$user_id='';
			$user_id=$this->CI_auth->logged_id();
			
			$sql_insert = "INSERT INTO offers_ratings(offer_id, rating, user_id, added_date) VALUES ('".$oid."', '".$rating."', '".$user_id."', now())";
			$query_insert = $this->db->query($sql_insert);

			$sql_average = "SELECT AVG(rating) as averagerating FROM offers_ratings WHERE offer_id = '".$oid."' ";
			$query_average = $this->db->query($sql_average);
			$data_average = $query_average->result();
			$averagerating = ceil($data_average[0]->averagerating);

			$sql_update = "UPDATE merchant_offers SET average_rating = '".$averagerating."' WHERE id = '".$oid."' ";
			$query_update = $this->db->query($sql_update);

			echo "success";
		}
		else
		{
			echo "error";
		}	
	}

	public function mynotifications()
	{
		$user_notifications_data = $this->users->get_user_notification_data();
		
		if((bool)$this->config->item('test_mode'))
		{
			//print_r($user_notifications_data);
		}
		$unread_notifications = $user_notifications_data['unread'];
		$notifications = $user_notifications_data['notifications_data'];
		$data = array('notifications' =>  $notifications);
		$this->load->view('/customeraccount/mynotifications', $data);
	}
}
?>