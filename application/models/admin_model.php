<?php
class Admin_Model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}
	public function get_admin_detail($id="")
	{
		$this->db->select("a.*");		
		$this->db->from('ondi_admin as a');		
		$condition=array('a.mgmt_id' => $id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_content_pages()
	{

		$this->db->select("p.*");		
		$this->db->from('content_page as p');		
		//$condition=array('p.page_status' => '1');	
		$condition=array();
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_content_pages_detail($id="")
	{
		$this->db->select("p.*");		
		$this->db->from('content_page as p');		
		$condition=array('p.page_id' => $id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_faq_category()
	{
		$this->db->select("fc.*");		
		$this->db->from('faq_category as fc');		
		//$condition=array('p.page_status' => '1');	
		$condition=array();
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_faq_category_detail($id="")
	{
		$this->db->select("*");		
		$this->db->from('faq_category');	
		
		if($id !="" && $id !="0")
		{
			$condition=array('cat_id' => $id);		
			$this->db->where($condition);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		$faqs = $query->result();
		
		//print_r($faqs);
		//die;
		
		return $faqs;
	}
	public function count_faq_category($faq_category="",$cat_id="")
	{
		$this->db->from('faq_category');
		
		if(!empty($cat_id))
		{
			$condition=array('category' => $faq_category, 'cat_id !=' => $cat_id );
		}else{

			$condition=array('category' => $faq_category);
		}		
		$this->db->where($condition);	
		//echo $this->db->last_query();
		return $this->db->count_all_results();
	}
	public function get_faq_by_category($cat_id="")
	{		
		$this->db->select("f.*");		
		$this->db->from('faq as f');
		
		if($cat_id!="")
		{
			$condition=array('f.faq_category' => $cat_id);			
			$this->db->where($condition);
		}
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		//exit();
		return $query->result();

	}
	public function get_faq_detail($faq_id="")
	{

		$this->db->select("f.*");		
		$this->db->from('faq as f');		
		$condition=array('f.faq_id' => $faq_id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function delete_faq($faq_id="")
	{		
		$condition=array('faq_id' => $faq_id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('faq');
	}
	public function delete_faq_category($cat_id="")
	{
		$ci = & get_instance();	

		$cat_del_qry = "delete from faq_category where cat_id = '".$cat_id."' ";
		$cat_del_result = $ci->db->query($cat_del_qry);

		$faq_del_qry = "delete from faq where faq_category = '".$cat_id."' ";
		$faq_del_result = $ci->db->query($faq_del_qry);
	}
	public function get_merchants_without_password()
	{

		$this->db->select("u.*");		
		$this->db->from('users as u');		
		$condition=array('u.password' => '');	
		$this->db->where($condition);
		$this->db->order_by('u.user_id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_merchants_with_password($merchant_search="", $city_search="")
	{

		/*$this->db->select("u.*, b.business_name");		
		$this->db->from('users as u');		
		$this->db->join('merchant_businessprofile as b', 'u.user_id = b.user_id', 'left');
		$condition=array('u.password != ' => '', 'u.user_type != ' => '1');
		$this->db->where($condition);
		$this->db->order_by('u.user_id','desc');

		if(!empty($merchant_search))
		{
			$this->db->like('contact_name', $merchant_search);
			//$this->db->or_like('u.last_name', $merchant_search);
		}
		$query = $this->db->get();
		echo $this->db->last_query();
		
		*/
		
		$sql = "SELECT u.*, b.*, u.user_id as usrid FROM (users as u) LEFT JOIN merchant_businessprofile as b ON u.user_id = b.user_id WHERE u.password != '' AND u.user_type != '1' ";
		if(!empty($merchant_search))
		{
			$sql .= " AND (contact_name like '%".$merchant_search."%' OR business_name like '%".$merchant_search."%'  )";
		}
		if(!empty($city_search))
		{
			$sql .= " AND b.city = '".$city_search."' ";
		}
		$sql .= " ORDER BY u.user_id desc ";
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_customers($customer_search="", $cols="", $orderset="")
	{		
		/*
		$this->db->select("u.*");		
		$this->db->from('users as u');		
		$condition=array('u.user_type ' => '1');	
		$this->db->where($condition);


		$this->db->order_by('u.user_id','desc');
		if(!empty($customer_search))
		{
			$this->db->like('u.first_name', $customer_search);
			$this->db->or_like('u.last_name', $customer_search);
		}
		$query = $this->db->get();
		echo $this->db->last_query();
		*/
		$str="";
		if(!empty($customer_search))
		{
			$str .= " and (u.first_name like '%".$customer_search."%') OR (u.last_name like '%".$customer_search."%') ";
		}

		$sql = "SELECT u.* FROM (users as u) WHERE u.user_type = '1' $str ";
		if(!empty($cols) && !empty($orderset))
		{
			$sql .= " ORDER BY u.".$cols." ".$orderset;
		}
		else
		{
			$sql .= " ORDER BY u.user_id desc, u.first_name asc, u.last_name asc ";
		}
		//echo $sql;
		$query = $this->db->query($sql);
		return $query->result();
	}
	public function get_paymentcategories()
	{
		$this->db->select("*");		
		$this->db->from('creditcard_categories');		
		$this->db->order_by('id','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_businesstypes()
	{
		$this->db->select("*");		
		$this->db->from('business_types');	
		
		$condition=array('display_status' => '1', 'delete_status' => '0');	
		$this->db->where($condition);
		
		$this->db->order_by('type_name','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_businesstypes_admin()
	{
		$this->db->select("*");		
		$this->db->from('business_types');	
		
		//$condition=array('display_status' => '1', 'delete_status' => '0');	
		//$this->db->where($condition);

		$condition=array('delete_status' => '0');	
		$this->db->where($condition);
		
		$this->db->order_by('type_name','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_serviceoffered($business_type="")
	{
		$this->db->select("*");		
		$this->db->from('services');	
		if($business_type != "")
		{
			$condition=array('business_type' => $business_type);	
			$this->db->where($condition);
		}
		$this->db->order_by('service','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_serviceoffered_admin($business_type="")
	{
		$this->db->select("*");		
		$this->db->from('business_types_options');	
		if($business_type != "")
		{
			$condition=array('business_type' => $business_type);	
			$this->db->where($condition);
		}
		$this->db->order_by('option_value','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}	
	public function get_perfectfor($business_type="")
	{
		$this->db->select("*");		
		$this->db->from('perfect_for');		
		
		if(!empty($business_type))
		{
			$condition=array('delete_status' => '0', 'business_type' => $business_type); // as per new update.
		}else{
			$condition=array('delete_status' => '0'); // as per new update.
		}		
		$this->db->where($condition);	
		
		$this->db->order_by('pf_name','asc');
		$query = $this->db->get();
		$this->db->last_query();
		//exit();
		return $query->result();
	}
	public function delete_perfect_for($id)
	{
		$ci = & get_instance();
		$tagUpdQry = "update perfect_for set delete_status = '1' where 1 and pf_id = '".$id."' ";		
		$tagUpdResult = $ci->db->query($tagUpdQry);
	}
	public function get_blogcategories()
	{
		$this->db->select("*");		
		$this->db->from('blog_category');		
		$this->db->order_by('category','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_blogposts($blog_category="")
	{
		$this->db->select("*");		
		$this->db->from('blog_posts');	
		
		if($blog_category != "")
		{
			$this->db->where("FIND_IN_SET('$blog_category',post_category) !=", 0);
		}
		$this->db->order_by('post_id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query(); 
		return $query->result();
	}	
	public function get_blogtags()
	{
		$this->db->select("*");		
		$this->db->from('blog_tags');
		
		$condition=array('delete_status' => '0'); // as per new update.
		$this->db->where($condition);	

		$this->db->order_by('tag_name','asc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_blogcomments()
	{
		$this->db->select("a.*, b.post_title, c.email, a.email as guestemail");		
		$this->db->from('blog_post_comments a');	
		$this->db->join('blog_posts as b', 'a.post_id = b.post_id', 'left');
		$this->db->join('users as c', 'a.user_id = c.user_id', 'left');
		$this->db->order_by('a.comment_id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_whatsondemand($city_id)
	{
		$this->db->select("*");		
		$this->db->from('city');	
		$condition=array('id' => $city_id); // as per new update.
		$this->db->where($condition);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_alloffers()
	{
		$this->db->select("*");		
		$this->db->from('merchant_offers');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_alloffers_cityspecific($city_id)
	{
		/*$this->db->select("a.*");		
		$this->db->from('merchant_offers a');	
		$this->db->join('merchant_businessprofile as b', 'a.merchant_id = b.user_id', 'left');
		$condition=array('b.city' => $city_id); // as per new update.
		$this->db->where($condition);
		$query = $this->db->get();**
		echo $this->db->last_query();
		return $query->result();*/

		$sql = "SELECT a.* FROM merchant_offers a LEFT JOIN merchant_businessprofile as b ON a.merchant_id = b.user_id WHERE b.city = '".$city_id."' ";


		$qryStr = " AND ( ";
		$k=1;
		for($booking_date=strtotime(date('Y-m-d')); $booking_date<strtotime("+7 day", strtotime(date('Y-m-d'))); $booking_date)
		{
			$running_date = date('Y-m-d',$booking_date);
			
			$qryStr .= " ('$running_date' BETWEEN a.offer_running_from AND a.offer_running_to)";
			
			if($k<=6)
			{
				$qryStr .= " OR ";
			}
			$k++;			

			$booking_date = strtotime("+1 day", $booking_date);	
		}
		$qryStr .= " ) ";
		$sql .= $qryStr;
	
		$query = $this->db->query($sql);
		//echo $this->db->last_query();
		return $query->result();
	}
	public function count_check_promo_code($code="",$id="")
	{				
		$this->db->from('promo_code');	
		if(!empty($id))
		{
			
			$condition=array('code' => $code, 'id !=' =>$id);

		}else{

			$condition=array('code' => $code);
		}		
		$this->db->where($condition);		
		//echo $this->db->last_query();		
		return $this->db->count_all_results();
	}
	public function count_check_promo_id($id="")
	{
		$this->db->from('promo_code');	
		$condition=array('id' => $id);
		$this->db->where($condition);	
		$this->db->last_query();		
		return $this->db->count_all_results();
	}
	public function get_promo_codes($id="")
	{
		$this->db->select("*");	
		if($id!="")
		{
			$condition=array('id' => $id);
			$this->db->where($condition);
		}
		$this->db->from('promo_code');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function delete_promo_code($id="")
	{		
		$condition=array('id' => $id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('promo_code');
	}
	public function count_check_city_name($city_name="",$id="")
	{				
		$this->db->from('city');	
		if(!empty($id))
		{
			
			$condition=array('city_name' => $city_name, 'id !=' =>$id);

		}else{

			$condition=array('city_name' => $city_name);
		}		
		$this->db->where($condition);	
		//$this->db->get();
		//echo $this->db->last_query();	
		//exit();
		return $this->db->count_all_results();
	}
	public function count_check_by_title($table="",$field_name="",$field_value="",$id_name="",$id_value="")
	{				
		$this->db->from($table);	
		if(!empty($id_name))
		{
			
			$condition=array($field_name => $field_value, $id_name.' !=' =>$id_value);

		}else{

			$condition=array($field_name => $field_value);
		}		
		$this->db->where($condition);	
		//$this->db->get();
		//echo $this->db->last_query();	
		//exit();
		return $this->db->count_all_results();
	}

	public function count_check_by_title_escape_del_status($table="",$field_name="",$field_value="",$id_name="",$id_value="")
	{				
		$this->db->from($table);	
		if(!empty($id_name))
		{
			
			$condition=array($field_name => $field_value, $id_name.' !=' =>$id_value, 'delete_status' => '0' );

		}else{

			$condition=array($field_name => $field_value, 'delete_status' => '0' );
		}		
		$this->db->where($condition);	
		//$this->db->get();
		//echo $this->db->last_query();	
		//exit();
		return $this->db->count_all_results();
	}

	public function count_check_by_id($table="",$field_name="",$id="")
	{
		$this->db->from($table);	
		$condition=array($field_name => $id);
		$this->db->where($condition);	
		$this->db->last_query();		
		return $this->db->count_all_results();
	}
	public function get_cities($id="")
	{
		$this->db->select("*");	
		if($id!="")
		{
			$condition=array('id' => $id);
			$this->db->where($condition);
		}
		$this->db->from('city');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_interests($id="")
	{
		$this->db->select("*");	
		if($id!="")
		{
			$condition=array('id' => $id, 'delete_status' => '0');
			$this->db->where($condition);
		}else{

			$condition=array('delete_status' => '0');
			$this->db->where($condition);
		}
		$this->db->from('user_interests');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function get_viewuserpositions($id="")
	{
		$this->db->select("*");	
		if($id!="")
		{
			$condition=array('position_id' => $id);
			$this->db->where($condition);
		}else{

			$condition=array('display_status' => '1', 'delete_status' => '0');
			$this->db->where($condition);
		}
		$this->db->from('user_position');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function delete_city($id)
	{
		$condition=array('id' => $id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('city');

	}	
	public function delete_record($table="",$field_name="", $id="")
	{
		$condition=array($field_name => $id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete($table);

	}
	public function delete_post_comment($c_id="")
	{		
		$condition=array('comment_id' => $c_id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('blog_post_comments');
	}

	public function approve_post_comment($c_id="")
	{
		$ci = & get_instance();
		$tagUpdQry = "UPDATE blog_post_comments SET display_status = '1' WHERE comment_id = '".$c_id."' ";		
		$tagUpdResult = $ci->db->query($tagUpdQry);
	}

	public function delete_post($id="")
	{		
		$condition=array('post_id' => $id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('blog_posts');


		$condition=array('post_id' => $id);
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete('blog_post_comments');
	}
	public function delete_blog_category($c_id="")
	{
		$ci = & get_instance();
		
		$this->db->select("*");		
		$condition=array('post_category' => $c_id);
		$this->db->where($condition);		
		$this->db->from('blog_posts');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		$post_result = $query->result();		

		if(count($post_result)>0)
		{
			foreach($post_result as $post_val)
			{				
				
				// deleting post comments
				$comment_condition=array('post_id' => $post_val->post_id);
				$this->db->where($comment_condition);					
				$this->db->delete('blog_post_comments');			

			}
			// deleting posts as per category id
			$post_condition=array('post_category' => $c_id);
			$this->db->where($post_condition);					
			$this->db->delete('blog_posts');

		}
				
		$blog_cat_condition=array('id' => $c_id);
		$this->db->where($blog_cat_condition);
		//$this->db->last_query();	
		$this->db->delete('blog_category');		

	}
	public function delete_blog_tag($id)
	{
		$ci = & get_instance();
		$tagUpdQry = "update blog_tags set delete_status = '1' where 1 and tag_id = '".$id."' ";		
		$tagUpdResult = $ci->db->query($tagUpdQry);
	}
	public function delete_business_type($id)
	{
		$ci = & get_instance();
		$tagUpdQry = "update business_types set delete_status = '1' where 1 and type_id = '".$id."' ";		
		$tagUpdResult = $ci->db->query($tagUpdQry);
	}
	public function delete_record_temp($table="",$field_name="", $id="")
	{
		$ci = & get_instance();
		$delUpdQry = "update $table set delete_status = '1' where $field_name = '".$id."' ";		
		$delUpdResult = $ci->db->query($delUpdQry);
	}
	public function get_lybinfo()
	{
		$this->db->select("a.*");		
		$this->db->from('lyb_boxes as a');		
		$condition=array('a.id' => '1');		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_signupinfo()
	{
		$this->db->select("a.*");		
		$this->db->from('signup_boxes as a');		
		$condition=array('a.id' => '1');		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function get_dashboard_search_data($keyword="",$order_from="",$order_to="",$amount_from="",$amount_to="")
	{
		$ci = & get_instance();

		$str = "";
		if(!empty($keyword))
		{
			$str .= " and (";
			
			$str .= " u.first_name like '%".$keyword."%' ";
			$str .= " OR ";
			$str .= " u.contact_name like '%".$keyword."%' ";
			$str .= " OR ";
			$str .= " mo.offer_title like '%".$keyword."%' ";
			$str .= " OR ";
			$str .= " o.booking_code like '%".$keyword."%' ";
			$str .= " OR ";
			$str .= " mbp.business_name like '%".$keyword."%' ";
			$str .= ") ";
		}
		if(!empty($order_from) && !empty($order_to))
		{
			$str .= " and (";
			$str .= " DATE_FORMAT(o.order_date, '%Y-%m-%d') between '".$order_from."' and '".$order_to."' ";
			$str .= ") ";
		}

		if(!empty($amount_from) && !empty($amount_to))
		{
			$str .= " and (";
			$str .= " o.total_price between '".$amount_from."' and '".$amount_to."' ";
			$str .= ") ";
		}

		$query = "select o.order_date, o.total_price, o.order_status, o.booking_code, u.first_name, u.last_name, u.contact_name, mo.offer_title from orders as o left join merchant_offers as mo on o.offer_id = mo.id left join users as u on o.user_id = u.user_id left join merchant_businessprofile as mbp on u.user_id = mbp.user_id where 1 $str order by o.order_id desc  ";
		//exit();
		$exe_query = $ci->db->query($query);
		$data_offer = $exe_query->result();
		return $data_offer;		
		
	}
	function convert2link($string)
	{
		$string = strtolower($string);
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '�';
		$special_chars[] = '?';
		$special_chars[] = '.';
		$special_chars[] = ':';
		$special_chars[] = ',';
		$special_chars[] = '_';
		$special_chars[] = '-';
		$special_chars[] = '+';
		$special_chars[] = '&';
		$special_chars[] = '/';
		$special_chars[] = '\\';
		$special_chars[] = ' ';
		$special_chars[] = '"';
		$special_chars[] = '#';
		$special_chars[] = '%';
		$special_chars[] = "'";
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '_';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '_';
		$special_chars2[] = '_';
		$special_chars2[] = '';
		$special_chars2[] = '_';
		$special_chars2[] = '_';
		$special_chars2[] = '-';
		$special_chars2[] = '_';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$special_chars2[] = '';
		$string = str_replace($special_chars,$special_chars2,$string);
		return $string;
	}


	public function get_subscribers($id="")
	{
		$this->db->select("*");	
		if($id!="")
		{
			$condition=array('id' => $id);
			$this->db->where($condition);
		}
		$this->db->from('newsletter_subscribers');	
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function get_all_offers($offer_search="", $ds="")
	{
		$query = "SELECT a.id as offerid, a.offer_title, a.price, a.display_status, b.business_name FROM merchant_offers a LEFT JOIN merchant_businessprofile b ON a.merchant_id = b.user_id ";
		if($offer_search !="")
		{
			$query .= " WHERE a.offer_title LIKE '%".$offer_search."%' OR  b.business_name LIKE '%".$offer_search."%' ";	
		}
		else if($ds !="")
		{
			$query .= " WHERE a.display_status = '".$ds."' ";	
		}
		$query .= " ORDER BY a.display_status DESC, a.id DESC";
		//echo $query;
		$exe_query = $this->db->query($query);
		$data_offer = $exe_query->result();
		return $data_offer;		
	}
	public function get_cityimages($slide_city="")
	{
		$this->db->select("a.*, b.city_name");		
		$this->db->from('slideshow_images a');	
		$this->db->join('city b', 'a.city = b.id', 'left');

		if($slide_city != "")
		{
			$this->db->where("a.city", $slide_city);
		}
		$this->db->order_by('a.id','desc');
		$query = $this->db->get();
		//echo $this->db->last_query(); die;
		return $query->result();
	}	

	public function delete_slideimage($id)
	{
		$this->db->select("slideshow_image");		
		$this->db->from('slideshow_images');	
		$this->db->where("id", $id);
		$query = $this->db->get();
		$data_slideshow_images = $query->result();
		$slideshow_image = $data_slideshow_images[0]->slideshow_image;
		@unlink($slideshow_image);

		$condition=array('id' => $id);
		$this->db->where($condition);
		$this->db->delete('slideshow_images');
	}	

	public function get_things_i_love($str)
	{
		if($str == "") $str = '0';
		$query = "SELECT interest FROM user_interests WHERE id IN (".$str.")";
		$exe_query = $this->db->query($query);
		$data_offer = $exe_query->result();
		$returnstr = "";
		foreach($data_offer as $theoffer)
		{
			$the_interest = $theoffer->interest;
			if($the_interest != "")
			{
				$returnstr .= $the_interest.", ";
			}
		}
		if($returnstr != "")
		{	
			$returnstr = substr($returnstr, 0, -2);
		}
		//echo "returnstr--->".$returnstr;
		return $returnstr;
	}
}