<?php
class Ondiblog extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function getchildcomments($comment_id="")
	{
		if($comment_id != "")
		{
			$sql_comments = "SELECT * FROM blog_post_comments WHERE parent_comment_id = '".$comment_id."' AND display_status  = '1' ";
			$exe_comments = $this->db->query($sql_comments);
			$data_comments = $exe_comments->result();
			return $data_comments;
		}
	}

	public function getarchievemonths()
	{
		$sql_comments = "SELECT DISTINCT DATE_FORMAT(added_date, '%M %Y') as arcmonths FROM blog_posts";
		$exe_comments = $this->db->query($sql_comments);
		$data_comments = $exe_comments->result();
		return $data_comments;	
	}


}