<?php
class CI_auth extends CI_Model {
 
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->database();
		$this->load->helper('url');
		$this->load->model(array('CI_encrypt'));
	}
 
	function process_login($login_array_input = NULL){
		//if(!isset($login_array_input) OR count($login_array_input) != 2)
		//return false;
		//set its variable
		
		$username="";
		$password="";
		$user_type="";
		$remember_me="";
		
		if(isset($login_array_input[0]))
		{
			$username = $login_array_input[0];
		}
		if(isset($login_array_input[1]))
		{
			$password = $login_array_input[1];
		}
		if(isset($login_array_input[2]))
		{
			$user_type = $login_array_input[2];
		}
		if(isset($login_array_input[3]))
		{
			$remember_me = $login_array_input[3];
		}		

		// select data from database to check user exist or not?
		$sql_1 =  "SELECT * FROM users WHERE email= '".$username."' ";
		if($user_type != '')
		{
			if($user_type == '1')
			{	
				$sql_1 .=  " AND user_type = '".$user_type."' ";
			}
			else 
			{
				$sql_1 .=  " AND ( user_type = '2' OR user_type = '3' ) ";
			}
		}
		$sql_1 .=  " AND user_status = '1' ";
		$sql_1 .=  " LIMIT 1 ";
		//echo $sql_1; die;
		//$query = $this->db->query("SELECT * FROM users WHERE email= '".$username."' LIMIT 1");
		$query = $this->db->query($sql_1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			//print_r($row); die;
			//$user_id = $row->ID;
			//$user_pass = $row->password;
			//$user_salt = $row->salt;
			$user_id = $row->user_id;
			$user_pass = $row->password;			
			$user_salt = $row->salt;

			//echo "<br/>".$password;
			//echo "<br/>".$user_salt;
			//echo "<br/>".$user_pass;

			if($this->CI_encrypt->encryptUserPwd( $password,$user_salt) === $user_pass){
				$this->session->set_userdata('logged_user', $user_id);
				//echo "<br/>login success"; die;
				return true;
			}
			else
			{
				//echo "<br/>login failed"; die;
			}
			return false;
		}
		return false;
	}

	function process_login_social($login_array_input = NULL){
		$customer_email = "";
		$social_type = "";
		if(isset($login_array_input[0]))
		{
			$customer_email = $login_array_input[0];
		}
		if(isset($login_array_input[1]))
		{
			$social_type = $login_array_input[1];
		}

		// select data from database to check user exist or not?
		$sql_1 =  "SELECT * FROM users WHERE email= '".$customer_email."' ";
		if($social_type != '')
		{
			if($social_type == 'facebook')
			{	
				$sql_1 .=  " AND facebook_id != '' ";
			}
			else if($social_type == 'google')
			{	
				$sql_1 .=  " AND google_user = '1' ";
			}
			else if($social_type == 'twitter')
			{	
				$sql_1 .=  " AND twitter_id != '' ";
			}
		}
		$sql_1 .=  " AND user_status = '1' ";
		$sql_1 .=  " LIMIT 1 ";
		$query = $this->db->query($sql_1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			$user_id = $row->user_id;
			$this->session->set_userdata('logged_user', $user_id);
			return true;
		}
		else
		{
			return false;
		}
	}

	function admin_process_login($login_array_input = NULL){
		
		//if(!isset($login_array_input) OR count($login_array_input) != 2)
		//return false;
		//set its variable
		
		$admin_email="";
		$admin_password="";
				
		if(isset($login_array_input[0]))
		{
			$admin_email = $login_array_input[0];
		}
		if(isset($login_array_input[1]))
		{
			$admin_password = $login_array_input[1];
		}		

		// select data from database to check user exist or not?
		$sql_1 =  "SELECT * FROM ondi_admin WHERE mgmt_email = '".$admin_email."' ";
		$query = $this->db->query($sql_1);
		if ($query->num_rows() > 0)
		{
			$row = $query->row();
			//$user_id = $row->ID;
			//$user_pass = $row->password;
			//$user_salt = $row->salt;
			
			$user_id = $row->mgmt_id; // admin autoincrement id
			$user_pass = $row->mgmt_pwd;			
			$user_salt = $row->salt;
			
			if($this->CI_encrypt->encryptUserPwd($admin_password,$user_salt) === $user_pass){
				
				$this->session->set_userdata('admin_logged_user', $user_id);

				return true;
			}
			return false;
		}
		return false;
	}
 
	function check_logged(){
		return ($this->session->userdata('logged_user'))?TRUE:FALSE;
	}
	
	function admin_check_logged(){
		return ($this->session->userdata('admin_logged_user'))?TRUE:FALSE;
	}
	function admin_logged_id(){
			return ($this->admin_check_logged())?$this->session->userdata('admin_logged_user'):'';
	} 
	function logged_id(){
		return ($this->check_logged())?$this->session->userdata('logged_user'):'';
		/*if($this->check_logged())
		{
			$sess_user_data = $this->session->all_userdata();	
			if($sess_user_data['user_type']=='2')
			{
				return $sess_user_data['logged_user'];
			}
			else if($sess_user_data['user_type']=='3')
			{
				$sql_get_parent = "SELECT main_merchant_id FROM merchant_users WHERE user_id = '".$sess_user_data['logged_user']."' ";
				$query_get_parent = $this->db->query($sql_get_parent);
				$data_get_parent = $query_get_parent->result();
				$main_merchant_id = $data_get_parent[0]->main_merchant_id;
				return $main_merchant_id;				
			}
		}
		else
		{
			return '';
		}*/
	}

	function redirect_nonlogged_user(){
		redirect('/signup/', 'refresh');
	}
	
	function redirect_logged_user(){
		
		$sess_user_data = $this->session->all_userdata();		
		if($sess_user_data['user_type']=='1'){  // customer

			redirect('/customeraccount/contactinfo/', 'refresh');
			
		}else if($sess_user_data['user_type']=='2' || $sess_user_data['user_type']=='3'){  // merchant
		
			redirect('/merchantaccount/contactinfo/', 'refresh');

		}else{

			$this->CI_auth->redirect_nonlogged_user();
		}
	}


}