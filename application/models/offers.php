<?php
class Offers extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->load->helper(array('form', 'url'));

	}
	public function get_offer_star_ratings($offer_id)
	{						
		/*$sql = "SELECT rating FROM offers_ratings WHERE offer_id = '".$offer_id."' ";
		$exec = $this->db->query($sql);
		
		$num = 0;
		$rate = 0;
		
		$count = $exec->num_rows();
		if($count>0)
		{
			foreach($exec->result() as $tmp)
			{
				$rate += $tmp->rating;
				$num++;
			}
		}

		if($num>0)
		{
			$rating = ceil($rate/$num);
		}
		else
		{
			$rating = 0;	
		}
		
		
		//$rating = rand(1, 5);
		return $rating;*/

		$sql_average = "SELECT average_rating FROM merchant_offers WHERE id = '".$offer_id."' ";
		$query_average = $this->db->query($sql_average);
		$data_average = $query_average->result();
		//print_r($data_average);
		$averagerating = ceil($data_average[0]->average_rating);
		return $averagerating;
	}	

	public function get_offer_details($offer_id)
	{						
		$condition=array('b.id' => $offer_id);
		
		//$this->db->select('b.id as offer_id, b.offer_title, b.merchant_id, b.offer_description, ROUND(b.price) as price, ROUND(b.price_normally) as price_normally, c.business_name, c.suburb, c.business_type, c.profile_picture, d.pg_name');

		$this->db->select('b.id as offer_id, b.offer_title, b.merchant_id, b.offer_description, b.price as price, b.price_normally as price_normally, c.business_name, c.suburb, c.business_type, c.profile_picture, d.pg_name, b.offer_image');

		$this->db->from('merchant_offers as b');
		$this->db->join('merchant_businessprofile as c', 'c.user_id = b.merchant_id', 'left');
		$this->db->join('price_guide as d', 'd.pg_id = c.price_guide', 'left');
		$this->db->where($condition);
		$offer_details_query = $this->db->get();		
		$offer_data = $offer_details_query->result();
		
		if($offer_data[0]->offer_image!="")
		{
			$offer_data[0]->profile_picture = $offer_data[0]->offer_image;
		}
		//print_r($offer_data); die;
		return $offer_data[0];
	}
	public function get_offer_url($offer_id)
	{						
		$sql = "SELECT merchant_id FROM merchant_offers WHERE id = '".$offer_id."' ";
		$exec = $this->db->query($sql);
		$data = $exec->result();
		$merchant_id = $data[0]->merchant_id;
		$base_url = $this->config->base_url();
		return $base_url."merchantoffers/detail/?m=".base64_encode($merchant_id);
	}	



	public function how_many_left($offer_id)
	{
		$today = strtolower(date("l"));
		$today_date = date("Y-m-d");

		$condition=array('offer_id' => $offer_id, 'available_day' => $today);
		$this->db->select('quantity');
		$this->db->from('merchant_offers_available_time');
		$this->db->where($condition);
		$offer_details_query = $this->db->get();	
		//echo $this->db->last_query();
		$offer_data = $offer_details_query->result();
		$quantity = $offer_data[0]->quantity;
		

		$sql_numpersonsbooked = "SELECT SUM(number_of_people) as numpersonsbooked FROM orders WHERE offer_id = '".$offer_id."'  AND booking_date = '".$today_date."' AND order_status = '1' ";
		$query_numpersonsbooked = $this->db->query($sql_numpersonsbooked);
		//$numpersonsbooked = $query_numpersonsbooked->num_rows();
		$booking_data = $query_numpersonsbooked->result();
		$numpersonsbooked = $booking_data[0]->numpersonsbooked;
		//print_r($offer_data);
		$numleft = $quantity - $numpersonsbooked;
		
		if($numleft<10)
		{
			return $numleft;
		}
		else
		{
			return "";
		}

		//return $offer_data;
	}

	public function check_offer_inshortlist($offer_id)
	{
		$ci = & get_instance();
		$user_id=$ci->CI_auth->logged_id();
		$sess_user_data = $ci->session->all_userdata();
		$session_id = $ci->session->userdata('session_id');
		$shortlist_offers = array();
		$ip_add = $_SERVER['REMOTE_ADDR'];
		$session_user_data = $this->session->all_userdata();
		$session_id = $session_user_data['session_id'];

		if($user_id == '')
		{
			$sql_count = "SELECT offer_id FROM temp_shortlists WHERE ip_add = '".$ip_add."'  AND session_id = '".$session_id."' ";
			$query_count = $ci->db->query($sql_count);
			$count = $query_count->num_rows();
			if($count>0)
			{
				foreach($query_count->result() as $tmp)
				{
					$oid = $tmp->offer_id;
					array_push($shortlist_offers, $oid);
				}
			}
		}
		else
		{
			$sql_count = "SELECT offer_id FROM user_shortlists WHERE user_id = '".$user_id."' ";
			$query_count = $ci->db->query($sql_count);
			$count = $query_count->num_rows();
			if($count>0)
			{
				foreach($query_count->result() as $tmp)
				{
					$oid = $tmp->offer_id;
					array_push($shortlist_offers, $oid);
				}
			}
		}
		
		
		if(in_array($offer_id, $shortlist_offers))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function get_offer_merchant_details($merchant_id)
	{
		$ci = & get_instance();
		$sql_merchant = "	SELECT a.*, b.*, c.city_name, d.state, e.pg_name 
							FROM users a 
							LEFT JOIN merchant_businessprofile b ON a.user_id = b.user_id 
							LEFT JOIN city c ON b.city = c.id 
							LEFT JOIN states d ON b.state = d.state_id 
							LEFT JOIN price_guide e ON b.price_guide = e.pg_id 
							WHERE a.user_id = '".$merchant_id."' ";
		$exe_merchant = $ci->db->query($sql_merchant);
		$data_merchant = $exe_merchant->result();
		return $data_merchant[0];
	}

	public function get_merchant_opening_hours($merchant_id, $day)
	{
		$ci = & get_instance();
		$sql_merchant = "	SELECT from_hour, from_ampm, to_hour, to_ampm 
							FROM merchant_opening_hours 
							WHERE user_id = '".$merchant_id."' AND day = '".$day."' ";
		$exe_merchant = $ci->db->query($sql_merchant);
		$data_merchant = $exe_merchant->result();
		//print_r($data_merchant); die;
		return $data_merchant;
	}

	public function get_merchant_offers($merchant_id)
	{
		$ci = & get_instance();

		$qryStr = " AND ( ";
		$k=1;
		for($booking_date=strtotime(date('Y-m-d')); $booking_date<strtotime("+7 day", strtotime(date('Y-m-d'))); $booking_date)
		{
			$running_date = date('Y-m-d',$booking_date);
			
			$qryStr .= " ('$running_date' BETWEEN a.offer_running_from AND a.offer_running_to)";
			
			if($k<=6)
			{
				$qryStr .= " OR ";
			}
			$k++;			

			$booking_date = strtotime("+1 day", $booking_date);	
		}
		$qryStr .= " ) ";

		$search_date_x = date("Y-m-d");
		$daysearched = strtolower(date("l", strtotime($search_date_x)));
		$current_mil_time = date("Hi");

		$sql_merchant = "	SELECT a.*, (SELECT MAX(x.to_hours)
FROM merchant_offers_available_time x
WHERE x.offer_id = a.id AND x.available_day = '".$daysearched."' 
) AS maxtime  
							FROM merchant_offers a 
							WHERE a.display_status = '1' AND a.merchant_id = '".$merchant_id."' ".$qryStr." HAVING maxtime > ".$current_mil_time." ORDER BY a.id DESC ";						

		$exe_merchant = $ci->db->query($sql_merchant);
		
		if((bool)$this->config->item('test_mode'))
		{
			//echo $this->db->last_query();
		}
		//echo $this->db->last_query();
		
		$data_merchant = $exe_merchant->result();
		return $data_merchant;
	}
	public function get_merchant_offers_preview($merchant_id,$offer_id="",$tbl="")
	{
		$ci = & get_instance();
		$str="";

		if($tbl!="")
		{

		}else{

			$tbl = 'merchant_offers';
		}


		if(!empty($offer_id))
		{
			$str .= " and id = '".$offer_id."' ";
		}
		$sql_merchant = "	SELECT a.* 
							FROM ".$tbl." a 
							WHERE a.merchant_id = '".$merchant_id."' ".$str;
		$exe_merchant = $ci->db->query($sql_merchant);
		$data_merchant = $exe_merchant->result();
		return $data_merchant;
	}

	public function get_merchant_id($offer_id)
	{
		$ci = & get_instance();
		$sql_merchant = "	SELECT a.merchant_id 
							FROM merchant_offers a 
							WHERE a.id = '".$offer_id."' ";
		$exe_merchant = $ci->db->query($sql_merchant);
		$data_merchant = $exe_merchant->result();
		return $data_merchant[0];
	}

	public function get_merchant_reviews($merchant_id)
	{
		$ci = & get_instance();
		$sql_merchant = "	SELECT a.* 
							FROM merchant_offers a 
							WHERE a.merchant_id = '".$merchant_id."' ";
		$exe_merchant = $ci->db->query($sql_merchant);
		$count = $exe_merchant->num_rows();
		$num = 0;
		$rate = 0;
		if($count>0)
		{
			foreach($exe_merchant->result() as $tmp)
			{
				$oid = $tmp->id;
				$thisrate = $this->get_offer_star_ratings($oid);
				if($thisrate>0)
				{
					$rate += $thisrate;
					$num++;
				}
			}
		}
		if($num>0)
		{
			$rating = ceil($rate/$num);
		}
		else
		{
			$rating = 0;	
		}
		return $rating;
	}

	public function get_offer_ratings_by_user($offer_id, $user_id)
	{						
		$sql = "SELECT rating FROM offers_ratings WHERE offer_id = '".$offer_id."' AND user_id = '".$user_id."' ";
		$exec = $this->db->query($sql);
		
		$num = 0;
		$rate = 0;
		
		$count = $exec->num_rows();
		if($count>0)
		{
			foreach($exec->result() as $tmp)
			{
				$rate += $tmp->rating;
				$num++;
			}
		}

		if($num>0)
		{
			$rating = ceil($rate/$num);
		}
		else
		{
			$rating = 0;	
		}
		
		
		//$rating = rand(1, 5);
		return $rating;
	}	

	public function send_mail($to, $subject, $message)
	{
        if($this->config->item('test_mode'))
            return true;

		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From: ONDI <info@ondi.com>' . "\r\n";
		mail($to, $subject, $message, $headers);
	}

	public function sendSMS($content) {
        $ch = curl_init('https://www.smsbroadcast.com.au/api-adv.php');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec ($ch);
        curl_close ($ch);
        return $output;    
    }

}