<?php
class Users extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	//*************************************** common functions start **************************************//	
	function get_week_days()
	{
		$timestamp = strtotime('next Monday');
		$days = array();
		
		for ($i = 0; $i < 7; $i++) 
		{
			 //$days[] = strftime('%a', $timestamp);	 
			 //$days[] = strtolower(strftime('%A', $timestamp));

			 $days[$i][0] = strftime('%a', $timestamp);
			 $days[$i][1] = strtolower(strftime('%A', $timestamp));

			 $timestamp = strtotime('+1 day', $timestamp);		 
		}
		return $days;
	}
	function get_am_pm_dropdown($field_name="",$class="",$val="")
	{
		$chk='';
		$option_am='';
		$option_pm='';	
		if($val!="")
		{
			if($val=='AM' || $val=='am')
			{
				//$chk = 'selected';			
				$option_am = '<option value="am" selected>AM</option>';
				$option_pm = '<option value="pm">PM</option>';

			}else if($val=='PM' || $val=='pm')
			{
				//$chk = 'selected';
				$option_am = '<option value="am">AM</option>';
				$option_pm = '<option value="pm" selected>PM</option>';
			}
			
		}else{

			//$chk='';
			$option_am = '<option value="am">AM</option>';
			$option_pm = '<option value="pm">PM</option>';
		}	
		
		$str = '<select name="'.$field_name.'" class="'.$class.'">';
		//$str .= '<option value=""></option>';
		$str .= $option_am;
		$str .= $option_pm;
		$str .= '</select>';	
		return $str;

	}
	function get_time_dropdown($field_name="",$class="",$val="",$blankfield="")
	{
		/*
		$chk = '';
		if(strtolower($blankfield)=='to')
		{
			$blank = 'To';

		}else{
			
			$blank = 'From';
		}
		$str = '<select name="'.$field_name.'" class="'.$class.'">';
		$str .= '<option value="">'.$blank.'</option>';
				for($f=1; $f<=12; $f++){
					if($val!="" && $val==$f){ $chk = 'selected'; }else{ $chk = ''; }
					$str .= '<option value="'.$f.'" '.$chk.' >'.$f.'</option>';
				}                            	
		$str .= '</select>';
		return $str;
		*/

		$chk = '';
		if(strtolower($blankfield)=='to')
		{
			$blank = 'To';

		}
		else if(strtolower($blankfield)=='select')
		{
			$blank = 'Select';
		}
		else{
			
			$blank = 'From';
		}
		$str = '<select name="'.$field_name.'" class="'.$class.'">';
		$str .= '<option value="">'.$blank.'</option>';
				
				$start = strtotime('12:00 AM');
				$end   = strtotime('11:59 AM');
				for($i = $start;$i<=$end;$i+=900){

					$f = date('h:i', $i);					
					if($val!="" && $val==$f){ $chk = 'selected'; }else{ $chk = ''; }
					$str .= '<option value="'.$f.'" '.$chk.' >'.$f.'</option>';
				}
		$str .= '</select>';
		return $str;

	}
	function get_time_dropdown_booking($field_name="",$class="",$val="",$blankfield="")
	{		
		$chk = '';
		if(strtolower($blankfield)=='to')
		{
			$blank = '';

		}else{
			
			$blank = 'Select';
		}
		$str = '<select name="'.$field_name.'" class="'.$class.'">';
		$str .= '<option value="">'.$blank.'</option>';
				
				$start = strtotime('12:00 AM');
				$end   = strtotime('11:59 AM');
				for($i = $start;$i<=$end;$i+=900){

					$f = date('h:i', $i);					
					if($val!="" && $val==$f){ $chk = 'selected'; }else{ $chk = ''; }
					$str .= '<option value="'.$f.'" '.$chk.' >'.$f.'</option>';
				}
		$str .= '</select>';
		return $str;

	}
	function get_date_mdy($date_ymd)
	{
		// here we are converting date from Y-m-d to m/d/y
		$mdy = "";
		if($date_ymd!='0000-00-00')
		{
			$date_ymd_arr = explode("-",$date_ymd);		
			if(count($date_ymd_arr)>0)
			{
				$mdy = $date_ymd_arr[1] . '/' . $date_ymd_arr[2] . '/' . $date_ymd_arr[0];
			}	
		}
		return $mdy;
	}
	function get_date_dmy($date_ymd)
	{
		// here we are converting date from Y-m-d to m/d/y
		$mdy = "";
		if($date_ymd!='0000-00-00')
		{
			$date_ymd_arr = explode("-",$date_ymd);		
			if(count($date_ymd_arr)>0)
			{
				$mdy = $date_ymd_arr[2] . '/' . $date_ymd_arr[1] . '/' . $date_ymd_arr[0];
			}	
		}
		return $mdy;
	}
	//*************************************** common functions start **************************************//

	public function get_userslist()
	{						
		$merchant_id = '';
		$merchant_id = $this->CI_auth->logged_id();	// do not use get_main_merchant_id() method

		$this->db->select('mu.id, u.email, t.access_type_title');
		$this->db->from('merchant_users as mu');
		$this->db->join('users as u', 'mu.user_id = u.user_id', 'left');
		$this->db->join('user_access_type as t','mu.user_access_type = t.id','left');

		$sess_user_data = $this->session->all_userdata();						
		if($sess_user_data['user_type']=='2') // top level merchant
		{
			$condition=array('mu.main_merchant_id' => $merchant_id);
	
		}else if($sess_user_data['user_type']=='3'){ // this is for sub-admin

			$condition=array('mu.merchant_id' => $merchant_id);			
		}	
		
		$this->db->where($condition);
		// $this->db->order_by('o.booking_date','asc');
		$query = $this->db->get();		
		// echo $this->db->last_query();
		return $query->result();
	}	
	public function get_access_type()
	{
		$this->db->select('id,access_type_title');
		$this->db->from('user_access_type');
		$condition=array('display_status' => 1);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result();

	}
	/*public function users_count_by_email($email="", $user_id="")
	{
		$this->db->from('users');
		
		if(!empty($user_id))
		{
			$condition=array('email' => $email, 'user_id !=' => $user_id );
		}else{

			$condition=array('email' => $email);
		}		
		$this->db->where($condition);	
		//echo $this->db->last_query();
		return $this->db->count_all_results();
	}*/

	public function users_count_by_email($email="", $user_id="", $user_type="")
	{
		$query = "SELECT user_id FROM users WHERE email = '".$email."' AND user_type IN (".$user_type.")";
		if(!empty($user_id))
		{
			$query .= " AND user_id != '".$user_id."' ";
		}
		$exe_query = $this->db->query($query);
		$result =  $exe_query->result(); 
		//print_r($result); //die;
		//echo count($result); die;
		return sizeof($result);
	}	

	public function get_section_type()
	{
		$this->db->select('id,title');
		$this->db->from('merchant_section_types');
		$condition=array('display_status' => 1);
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_sections($id="")
	{
		$this->db->select('id,section_title');
		$this->db->from('merchant_sections');
		$condition=array('display_status' => 1, 'section_type_id' => $id );
		$this->db->where($condition);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_merchant_user_data($id="")
	{		
		if($id!="")
		{
			$this->db->select('u.password, u.email, mu.user_access_type, mu.user_access_sections, mu.user_id as merchant_user_id');
			$this->db->from('merchant_users as mu');
			$this->db->join('users as u', 'mu.user_id = u.user_id', 'left');
			$condition=array('mu.id' => $id);
			$this->db->where($condition);
			$query = $this->db->get();		
			return $query->result();
		}

	}
	public function get_offers($offer_list_type="",$offer_search="")
	{
				
		// $display_status
		$display_status='';		

		$merchant_id = '';	
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		$ci = & get_instance();		
		$current_date = date('Y-m-d');	
		$qryStr = "";
		if($offer_search!="")
		{				
			$qryStr .= " and o.offer_title like '%".addslashes($offer_search)."%' ";
		}
		
		if($offer_list_type=='liveoffers')
		{	
			//$qryStr .= " and ('".$current_date."' between o.offer_running_from and o.offer_running_to) ";		
			$qryStr .= " and ('".$current_date."' between o.offer_running_from and o.offer_running_to OR o.offer_running_from > '".$current_date."' ) ";
		}
		else if($offer_list_type=='offerhistory')
		{
			$qryStr .= " and o.offer_running_to < '".$current_date."' ";	
		}
		
		
		//$query = "select o.id, o.offer_title, o.offer_running_today, o.offer_multiple_dates, o.offer_running_from, o.offer_running_to, o.offer_description, o.fine_print, o.everyday_of_week, o.select_specific_days, o.specific_days_week, o.display_status as offer_display_status, o.price, o.price_normally, o.results_viewed, o.details_viewed, ((((select sum(number_of_people) from orders where offer_id = o.id and order_status = '1') /(select sum(quantity) from merchant_offers_available_time where offer_id = o.id))*100)) as consumption from merchant_offers as o where 1 and o.merchant_id = '".$merchant_id."' and o.display_status != '3' ".$qryStr." order by o.id desc";

		$query = "select o.id, o.offer_title, o.offer_running_today, o.offer_multiple_dates, o.offer_running_from, o.offer_running_to, o.offer_description, o.fine_print, o.everyday_of_week, o.select_specific_days, o.specific_days_week, o.display_status as offer_display_status, o.price, o.price_normally, o.results_viewed, o.details_viewed,(select sum(number_of_people) from orders where offer_id = o.id and order_status = '1') as total_number_of_people,
		(select sum(quantity) from merchant_offers_available_time where offer_id = o.id) as total_quantity from merchant_offers as o where 1 and o.merchant_id = '".$merchant_id."' and o.display_status != '3' ".$qryStr." order by o.id desc";
		
		$exe_query = $ci->db->query($query);
		$data_offer = $exe_query->result();
		return $data_offer;


	}
	public function get_offer_detail($offer_id="",$tbl="")
	{			
		$merchant_id = '';
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		if($tbl!="")
		{
			$table = $tbl;

		}else{

			$table = 'merchant_offers';
		}
		
		//$this->db->select("o.id, o.offer_title, o.offer_running_today, o.offer_multiple_dates, o.offer_running_from, o.offer_running_to");
		$this->db->select("o.*");
		$this->db->from($table.' as o');
		$condition=array('o.id' => $offer_id, 'o.merchant_id'=>$merchant_id);		
		$this->db->where($condition);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}
	public function get_different_days_data($offer_id="",$available_time_slots_capacity="",$tbl="")
	{
		$merchant_id = '';
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		$table = "";
		if($tbl!="")
		{
			$table = $tbl;

		}else{

			$table = 'merchant_offers_available_time';
		}

		
		$this->db->select("moat.*");		
		$this->db->from($table.' as moat');

		if($available_time_slots_capacity!="")
		{
			$condition=array('moat.offer_id' => $offer_id, 'available_time_slots_capacity' => $available_time_slots_capacity, 'moat.merchant_id'=>$merchant_id);	
		}else{

			$condition=array('moat.offer_id' => $offer_id, 'moat.merchant_id'=>$merchant_id);	
		}
		
		
		$this->db->where($condition);
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();

	}
	public function count_available_different_days($available_day="",$from_hours="",$to_hours="",$offer_id="",$tbl="")
	{
		$merchant_id = '';
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		if($tbl!="")
		{
			
		}else{

			$tbl = 'merchant_offers_available_time';
		}		
		$this->db->from($tbl);		
		$condition=array('available_day' => $available_day, 'from_hours'=>$from_hours, 'to_hours'=>$to_hours, 'offer_id' => $offer_id, 'merchant_id =' => $merchant_id );
		$this->db->where($condition);		
		//$this->db->last_query();
		return $this->db->count_all_results();

	}
	public function del_available_different_days($offer_id="",$tbl="")
	{
		$merchant_id = '';
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		if($tbl!="")
		{			

		}else{

			$tbl = 'merchant_offers_available_time';
		}

		$tables = array($tbl);
		$condition=array('offer_id' => $offer_id, 'merchant_id =' => $merchant_id );
		$this->db->where($condition);
		//$this->db->last_query();	
		$this->db->delete($tables);
	}
	public function get_merchant_opening_hours()
	{
		$merchant_id = '';
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$this->db->select("moh.*");		
		$this->db->from('merchant_opening_hours as moh');		
		$condition=array('moh.user_id' => $merchant_id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}

	public function display_merchant_business($commaseperated_business_types)
	{
		$cat_array = explode(",", $commaseperated_business_types);
		$this->db->select('type_name');
		$this->db->where_in('type_id', $cat_array);
		$servicesquery = $this->db->get('business_types');
		$dataservices = $servicesquery->result();
		$str = "";
		foreach($dataservices as $thestr)
		{
			$str .= $thestr->type_name.", ";
		}
		if($str != "") $str = substr($str, 0, -2);
		return $str;
	}
	public function get_user_detail($user_id="")
	{		
		$this->db->select("u.*");		
		$this->db->from('users as u');		
		$condition=array('u.user_id' => $user_id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		return $query->result();
	}	
	public function get_current_bookings_dates($offer_search="")
	{
		// we are fetching next 7 days booking dates from order table.
		
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		//$merchant_id = '1';

		$current_date = date('Y-m-d');		
		$date_after_7_day = date('Y-m-d',strtotime("+7 day", strtotime($current_date)));
		
		$this->db->select("o.booking_date, o.order_id");		
		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1', 'o.booking_date >='=> $current_date, 'o.booking_date <=' =>$date_after_7_day);	
		$this->db->group_by('o.booking_date');		
		$this->db->where($condition);
		$this->db->order_by('o.booking_date','asc');
		if($offer_search!="")
		{
			$this->db->like('mo.offer_title', $offer_search);
		}		
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		//exit();

		return $query->result();		
		
	}
	public function count_merchant_offers_by_date($booking_date="")
	{
		
		$booking_date = date('Y-m-d',$booking_date);	
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		/*
		$this->db->from('merchant_offers as mo');		
		$condition=array('mo.merchant_id ' => $merchant_id, 'mo.display_status' => 1);		
		$this->db->where($condition);
		$this->db->where($booking_date," BETWEEN mo.offer_running_from AND mo.offer_running_to");
		//return $this->db->count_all_results();
		*/		 

		$qry = "select * from merchant_offers as mo where mo.merchant_id = '".$merchant_id."' and mo.display_status = '1' and '".$booking_date."' BETWEEN mo.offer_running_from AND mo.offer_running_to ";		
		$row=$this->db->query($qry);
		return $row->num_rows;		
		
	}	

	public function date_difference($end_date, $start_date)
	{
		$qry = "SELECT DATEDIFF('".$end_date."', '".$start_date."') as days";		
		$row=$this->db->query($qry);
		
		$result = $row->result();
		return $result[0]->days;

	}
	public function most_recent_booking_date()
	{		
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$this->db->select("o.booking_date, o.order_id");		
		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');	
		$this->db->group_by('o.booking_date');		
		$this->db->where($condition);
		$this->db->order_by('o.order_id','desc');
		$this->db->limit(1,0);
		$query = $this->db->get();		
		//echo $this->db->last_query();		
		//exit();
		return $query->result();		
	}
	
	public function get_bookings_list_by_date($booking_date="")
	{
		// we are fetching all records from order table whose date is in parameter.

		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();		

		//$current_date = date('Y-m-d');		
		//$date_after_7_day = date('Y-m-d',strtotime("+7 day", strtotime($current_date)));
				
		$booking_date = date('Y-m-d', $booking_date);
		
		$this->db->select("o.booking_date, o.order_id, o.offer_id, o.booking_time, o.booking_time_ampm, o.number_of_people, o.offerprice, o.total_price, o.booking_code, o.order_date, mo.offer_title, u.first_name, u.last_name");	
		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'left');
		$this->db->join('users as u', 'o.user_id = u.user_id', 'left');
		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1', 'o.booking_date'=>$booking_date);
		
		$this->db->where($condition);
		$this->db->order_by('o.booking_date','asc');
		
		$query = $this->db->get();		
		
		//if((bool)$this->config->item('test_mode'))
		//{												
			//echo $this->db->last_query();
			//exit();
		//}

		return $query->result();

	}

	public function count_merchant_best_sellers()
	{
		// $merchant_id = $this->CI_auth->logged_id();	
		$merchant_id = $this->users->get_main_merchant_id();	

		$this->db->select("*");	
		$this->db->from('orders as o');
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');		
		$this->db->where($condition);
		
		//$query = $this->db->get();		
		//echo $this->db->last_query();
		//exit();
		//return $query->result();

		return $this->db->count_all_results();		

	}	
	public function get_merchant_best_sellers()
	{
		
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();
		
		// previous query		
		
		/* 
		$this->db->select("o.booking_date, o.order_id, o.offer_id, o.booking_time, o.booking_time_ampm, o.number_of_people, o.offerprice, o.total_price, o.booking_code, o.order_date, mo.offer_title, mo.price as merchant_offer_price, mo.offer_running_from, mo.offer_running_to, (SELECT COUNT(*) FROM orders AS ord WHERE ord.offer_id = o.offer_id) as total_offer_count, (select ( (o.number_of_people/sum(moat.quantity))*100 ) from merchant_offers_available_time as moat where moat.offer_id = o.offer_id) as consumption ");
		*/

		$this->db->select("o.booking_date, o.order_id, o.offer_id, o.booking_time, o.booking_time_ampm, o.number_of_people, o.offerprice, o.total_price, o.booking_code, o.order_date, mo.offer_title, mo.price as merchant_offer_price, mo.offer_running_from, mo.offer_running_to, (SELECT COUNT(*) FROM orders AS ord WHERE ord.offer_id = o.offer_id) as total_offer_count");

		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');			
		$this->db->where($condition);
		$this->db->group_by('o.offer_id');
		$this->db->order_by('total_offer_count','desc');
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		//exit();

		return $query->result();
	}
	public function get_history_bookings_dates($offer_search="")
	{
		// we are fetching next 7 days booking dates from order table.
		
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$current_date = date('Y-m-d');		
		//$date_after_7_day = date('Y-m-d',strtotime("+7 day", strtotime($current_date)));
		
		$this->db->select("o.booking_date, o.order_id");		
		$this->db->from('orders as o');	
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'left');		
		
		$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1', 'o.booking_date <'=> $current_date);	
		
		$this->db->group_by('o.booking_date');		
		$this->db->where($condition);
		$this->db->order_by('o.booking_date','asc');
		if($offer_search!="")
		{
			$this->db->like('mo.offer_title', $offer_search);
		}		
		$query = $this->db->get();
		//echo $this->db->last_query();
		//exit();
		return $query->result();		
		
	}

	public function count_statistics_for_offers($param="")
	{		
		$ci = & get_instance();
		$merchant_id = $this->users->get_main_merchant_id();
		
		$qry="";
		if($param=='30days')
		{
			
			$i=0;
			$qry .= " AND (";
			for($booking_date=strtotime(date('Y-m-d')); $booking_date>strtotime("-30 day", strtotime(date('Y-m-d'))); $booking_date)
			{
				$i++;
				$compare_date = date('Y-m-d',$booking_date);	
				$qry .= " ('$compare_date' BETWEEN mo.offer_running_from AND mo.offer_running_to) ";
				if($i<30)
				{
					$qry .= " OR ";
				}		
				$booking_date = strtotime("-1 day", $booking_date);
			}
			$qry .= ") ";

		}else if($param=='lifetime'){

			$qry="";
		}

		$query = "select count(*) as offer_count from  merchant_offers as mo where mo.merchant_id = '".$merchant_id."' and mo.display_status = '1' ".$qry;
		//exit();
		$result = $ci->db->query($query);
		$result_count = $result->result();
		if(isset($result_count[0]->offer_count))
		{
			return $result_count[0]->offer_count;

		}else{

			return 0;
		}

	}

	public function count_statistics_for_sales($param="")
	{
		
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$current_date = date('Y-m-d');		
		$date_before_30_day = date('Y-m-d',strtotime("-30 day", strtotime($current_date)));	
		
		$this->db->select("o.order_id");		
		$this->db->from('orders as o');		
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');			
		
		if($param=='30days')
		{
			$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1', 'o.order_date  >=' =>$date_before_30_day, 'o.order_date <=' =>$current_date);	

		}else if($param=='lifetime'){
			
			$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');
		}
		$this->db->where($condition);

		//$query = $this->db->get();		
		//echo $this->db->last_query();
		//exit();
		
		return $this->db->count_all_results();	


	}
	public function count_statistics_for_revenue($param="")
	{



		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$current_date = date('Y-m-d');
		$date_before_30_day = date('Y-m-d',strtotime("-30 day", strtotime($current_date)));	
		
		$this->db->select("o.order_id, sum(o.total_price)-sum(o.merchant_commission) as total_revenue");		
		$this->db->from('orders as o');		
		$this->db->join('merchant_offers as mo', 'o.offer_id = mo.id', 'inner');
		
		if($param=='30days')
		{
			$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1', 'o.order_date  >=' =>$date_before_30_day, 'o.order_date <=' =>$current_date);	

		}else if($param=='lifetime'){
			
			$condition=array('mo.merchant_id' => $merchant_id, 'o.order_status'=>'1');
		}
		$this->db->where($condition);

		$query = $this->db->get();	

		//echo $this->db->last_query();
		//exit();
		
		// return $this->db->count_all_results();
		return $query->result();
	}		
	public function get_main_merchant_id()
	{
		$logged_user_id = $this->CI_auth->logged_id(); // do not disable
		
		$sess_user_data = $this->session->all_userdata();		
		
		if($sess_user_data['user_type']=='2')
		{
			$main_merchant_id = $logged_user_id; // Merchant Account
			
	
		}else if($sess_user_data['user_type']=='3'){

			// Sub admin
			
			$this->db->select("mu.main_merchant_id");		
			$this->db->from('merchant_users as mu');
			$condition=array('mu.user_id' => $logged_user_id);
			$this->db->where($condition);
			$query = $this->db->get();		
			$result = $query->result();
			$main_merchant_id = $result[0]->main_merchant_id;	
			
		}
		return $main_merchant_id;

		
	}	
	public function get_subadmin_data()
	{
		$logged_user_id = $this->CI_auth->logged_id();	

		$this->db->select("mu.user_access_type, mu.user_access_sections");		
		$this->db->from('merchant_users as mu');
		$condition=array('mu.user_id' => $logged_user_id);
		$this->db->where($condition);
		$query = $this->db->get();	
		return $query->result();	

	}	
	public function get_user_access_type()
	{
		$sess_user_data = $this->session->all_userdata();						
		if($sess_user_data['user_type']=='2') // top level merchant
		{
			return '1';
	
		}else if($sess_user_data['user_type']=='3'){ // this is for sub-admin

			$subadmin_data	= $this->get_subadmin_data();
			return $subadmin_data[0]->user_access_type;
		}

	}
	public function get_user_access_sections()
	{
		$subadmin_data	= $this->get_subadmin_data();
		$user_access_sections = $subadmin_data[0]->user_access_sections;
		if($user_access_sections!="")
		{
			$access_sections_array = explode(",",$user_access_sections);
			return $access_sections_array;
		}

	}
	public function get_notification_options()
	{
		$this->db->select("*");		
		$this->db->from('notification_options');		
		//$this->db->where();
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		//exit();

		return $query->result();
	}


	public function user_steps_completed($user_id)
	{		
		$this->db->select("u.merchant_steps_completed");		
		$this->db->from('users as u');		
		$condition=array('u.user_id' => $user_id);		
		$this->db->where($condition);
		
		$query = $this->db->get();
		//echo $this->db->last_query();
		$data = $query->result();
		$merchant_steps_completed = $data[0]->merchant_steps_completed;
		return $merchant_steps_completed;
	}	
	public function del_merchant_offer_temp($offer_temp_id="")
	{
		$ci = & get_instance();
		if($offer_temp_id!="")
		{	
			$qry = "delete from merchant_offers_temp where id = '".$offer_temp_id."' ";
			$result = $ci->db->query($qry);
		}

	}
	public function del_merchant_offer_available_time_temp($offer_temp_id="")
	{
		$ci = & get_instance();
		if($offer_temp_id!="")
		{
			$qry = "delete from merchant_offers_available_time_temp where offer_id = '".$offer_temp_id."' ";
			$result = $ci->db->query($qry);
		}

	}
	public function count_offer_id($tbl="",$offer_id="")
	{
		if($tbl!="")
		{
			$ci = & get_instance();
			// $merchant_id = $this->CI_auth->logged_id();
			$merchant_id = $this->users->get_main_merchant_id();

			$query = "select count(*) as offer_count from ".$tbl." where merchant_id = '".$merchant_id."' and id = '".$offer_id."' ";
			
			$result = $ci->db->query($query);
			$result_count = $result->result();
			if(isset($result_count[0]->offer_count))
			{
				return $result_count[0]->offer_count;
			}else{
				return 0;
			}

		}
		else{

			return 0;
		}

	}
	public function get_average_commission($param="")
	{
		$ci = & get_instance();
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$current_date = date('Y-m-d');		
		$date_before_30_day = date('Y-m-d',strtotime("-30 day", strtotime($current_date)));	

		// 'mo.offer_running_from  >=' =>$date_before_30_day, 'mo.offer_running_to <=' =>$current_date

		$qryStr = "";
		if($param=='30days')
		{
			//$query = "select round((sum(o.merchant_commission)/count(*))) as average_commission from merchant_offers as mo inner join orders as o on mo.id = o.offer_id where 1 and mo.merchant_id = '".$merchant_id."' and (o.booking_date >= '".$date_before_30_day."' and o.booking_date <= '".$current_date."') and o.order_status = '1' ";		
			
			$qryStr .= " and (o.booking_date >= '".$date_before_30_day."' and o.booking_date <= '".$current_date."') ";
			
		}
		else if($param=='lifetime'){
			
			$qryStr = "";
		}

		$query = "select round((sum(o.merchant_commission)/count(*))) as average_commission from merchant_offers as mo inner join orders as o on mo.id = o.offer_id where 1 and mo.merchant_id = '".$merchant_id."' and o.order_status = '1' ".$qryStr;
		
		$result = $ci->db->query($query);
		$result_data = $result->result();
		if(isset($result_data[0]->average_commission))
		{
			return $result_data[0]->average_commission;
			
		}
		
	}
	public function get_conversion_rate($param="")
	{
		$ci = & get_instance();
		// $merchant_id = $this->CI_auth->logged_id();
		$merchant_id = $this->users->get_main_merchant_id();

		$current_date = date('Y-m-d');		
		$date_before_30_day = date('Y-m-d',strtotime("-30 day", strtotime($current_date)));	

		$qryStr = "";
		if($param=='30days')
		{			
			$qryStr .= " and (o.booking_date >= '".$date_before_30_day."' and o.booking_date <= '".$current_date."') ";
			
		}
		else if($param=='lifetime'){
			
			$qryStr = "";
		}		

		$query = "select o.offer_id as order_offer_id, o.number_of_people from merchant_offers as mo inner join orders as o on mo.id = o.offer_id where 1 and mo.merchant_id = '".$merchant_id."' and o.order_status = '1' ".$qryStr;
		$result = $ci->db->query($query);
		$result_order_data = $result->result();		

		$number_of_people=0;
		$offer_id_arr = array();
		foreach($result_order_data as $val)
		{
			if($val->number_of_people!="")
			{
				$number_of_people = $number_of_people + $val->number_of_people;
			}		
			
			if($val->order_offer_id!="")
			{
				array_push($offer_id_arr,$val->order_offer_id);
			}
			
		}		

		$offer_id_str = "";
		if(count($offer_id_arr)>0)
		{
			$offer_id_str = implode(",",$offer_id_arr);
		}
		$total_quantity = 0;
		if($offer_id_str!="")
		{
			$moatQry = "select sum(moat.quantity) as total_quantity from merchant_offers_available_time as moat where offer_id in (".$offer_id_str.") ";
			$moatResult = $ci->db->query($moatQry);
			$moatResultData = $moatResult->result();
			$total_quantity = $moatResultData[0]->total_quantity;		

		}

		$conversion_rate=0;
		if($number_of_people>0 && $total_quantity>0)
		{
			$conversion_rate = ($number_of_people/$total_quantity)*100;

		}
		return round($conversion_rate,2);	
		
		
	}	

	public function get_distinct_offer_id_having_moh_id()
	{		
		$ci = & get_instance();
		
		$merchant_id = '';		
		$merchant_id = $this->users->get_main_merchant_id();	
		
		$moatQry = "select distinct moat.offer_id from merchant_offers_available_time as moat where moat.merchant_id = '".$merchant_id."' and moh_id != '0' ";
		$moatResult = $ci->db->query($moatQry);
		$moatResultData = $moatResult->result();
		return $moatResultData;


	}
	public function update_results_viewed_offer($offer_id="")
	{
		$ci = & get_instance();
		$query = "update merchant_offers set results_viewed = results_viewed + 1 where id = '".$offer_id."' ";
		$result = $ci->db->query($query);
		// $data = $result->result();

	}
	public function update_details_viewed_offer($offer_id="")
	{

		$ci = & get_instance();
		$query = "update merchant_offers set details_viewed = details_viewed + 1 where id = '".$offer_id."' ";
		$result = $ci->db->query($query);
		// $data = $result->result();

	}

	public function get_position_value($position)
	{
		$ci = & get_instance();
		$posQry = "SELECT position_value FROM user_position WHERE position_id = '".$position."' ";
		$posResult = $ci->db->query($posQry);
		$posResultData = $posResult->result();
		return $posResultData[0]->position_value;
	}


	public function get_notification_method($user_id, $user_type)
	{
		$ci = & get_instance();
		if($user_type=='1')
		{
			$posQry = "SELECT confirmation_methods FROM users WHERE user_id = '".$user_id."' ";
			$posResult = $ci->db->query($posQry);
			$posResultData = $posResult->result();	
			return $posResultData[0]->confirmation_methods;
		}
		else
		{
			$posQry = "SELECT confirmation_methods FROM merchant_businessprofile WHERE user_id = '".$user_id."' ";
			$posResult = $ci->db->query($posQry);
			$posResultData = $posResult->result();	
			return $posResultData[0]->confirmation_methods;
		}
	}

	public function get_user_notification_data()
	{		
		$sess_user_data = $this->session->all_userdata();
		$unread = 0;
		if($sess_user_data['user_type']=='1')
		{
			$customer_id = $this->CI_auth->logged_id();
			$sql_notifications = "SELECT * FROM user_notifications WHERE customer_id = '".$customer_id."' ORDER BY customer_read_status ASC, id DESC ";
		}
		else
		{
			$merchant_id = $this->users->get_main_merchant_id();
			$sql_notifications = "SELECT * FROM user_notifications WHERE merchant_id = '".$merchant_id."' ORDER BY merchant_read_status ASC, id DESC ";
		}
		$exe_notifications = $this->db->query($sql_notifications);
		$notifications_data = $exe_notifications->result();	
		
		foreach($notifications_data as $notification)
		{
			if($sess_user_data['user_type']=='1')
			{
				if($notification->customer_read_status == '0')
				{
					$unread++;
				}
			}
			else
			{
				if($notification->merchant_read_status == '0')
				{
					$unread++;
				}
			}
		}
		
		$return_array = array();
		$return_array['notifications_data'] = $notifications_data;
		$return_array['unread'] = $unread;

		return $return_array;
	}	

	public function getuserimage($user_id)
	{
		$userdetails = $this->get_user_detail($user_id);
		$user_type = $userdetails[0]->user_type;
		$user_image = "";
		if($user_type == '1')
		{
			$user_image = $userdetails[0]->customer_picture;
		}
		else
		{
			$merchantetails = $this->get_merchant_user_data($user_id);
			$user_image = $userdetails[0]->profile_picture;
		}
		if($user_image == "")
		{
			$user_image = "public/uploads/user_images/user_small_no_image.jpg";
		}
		return $user_image;
	}
	public function number_of_people_booked($offer_id="")
	{
		
			$ci = & get_instance();		
			$posQry = "SELECT sum(o.number_of_people) as total_number_of_people FROM orders as o WHERE o.offer_id = '".$offer_id."' and o.order_status = '1' ";		
			$posResult = $ci->db->query($posQry);
			$posResultData = $posResult->result();	
			return $posResultData[0]->total_number_of_people;
				
	}
	public function total_quantity_available($offer_id="")
	{
		$ci = & get_instance();		
		$posQry = "SELECT sum(moat.quantity) as total_quantity FROM merchant_offers_available_time as moat WHERE moat.offer_id = '".$offer_id."' ";
		
		$posResult = $ci->db->query($posQry);
		$posResultData = $posResult->result();	
		return $posResultData[0]->total_quantity;
			
	}
	public function get_merchant_business_type()
	{
		$ci = & get_instance();
		$merchant_id = $this->users->get_main_merchant_id();
		
		$posQry = "SELECT business_type FROM merchant_businessprofile WHERE user_id = '".$merchant_id."' ";
		$posResult = $ci->db->query($posQry);
		$posResultData = $posResult->result();	
		return $posResultData[0]->business_type;
		
		
	}

}