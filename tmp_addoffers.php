
<?php


// different days counting start
$days_between_multiple_dates_array = array();

if (isset($offer_detail[0]->offer_running_from)) {
$offer_running_from2 = $offer_detail[0]->offer_running_from;
}

if (isset($offer_detail[0]->offer_running_to)) {
$offer_running_to2 = $offer_detail[0]->offer_running_to;
}

if ($offer_running_from2 != "" && $offer_running_to2 != "") {
$monday = 0;
$tuesday = 0;
$wednesday = 0;
$thursday = 0;
$friday = 0;
$saturday = 0;
$sunday = 0;

while (strtotime($offer_running_from2) <= strtotime($offer_running_to2)) {
//date('Y-m-d', strtotime($from_date));
$current_day = strtolower(date('l', strtotime($offer_running_from2)));

if ($current_day == 'monday') {
if ($monday == 0) {
array_push($days_between_multiple_dates_array, 'monday');
}
$monday++;

} else if ($current_day == 'tuesday') {

if ($tuesday == 0) {
array_push($days_between_multiple_dates_array, 'tuesday');
}
$tuesday++;
} else if ($current_day == 'wednesday') {

if ($wednesday == 0) {
array_push($days_between_multiple_dates_array, 'wednesday');
}

$wednesday++;
} else if ($current_day == 'thursday') {

if ($thursday == 0) {
array_push($days_between_multiple_dates_array, 'thursday');
}
$thursday++;
} else if ($current_day == 'friday') {

if ($friday == 0) {
array_push($days_between_multiple_dates_array, 'friday');
}
$friday++;
} else if ($current_day == 'saturday') {

if ($saturday == 0) {
array_push($days_between_multiple_dates_array, 'saturday');
}
$saturday++;
} else if ($current_day == 'sunday') {

if ($sunday == 0) {
array_push($days_between_multiple_dates_array, 'sunday');
}
$sunday++;
}

$offer_running_from2 = date('Y-m-d', strtotime($offer_running_from2 . ' + 1 day'));

}

}





$all_day_time_from = "";
$all_day_time_from_am_pm = "am"; // by default we are asuming AM
$all_day_time_to = "";
$all_day_time_to_am_pm = "am"; // by default we are asuming AM
$all_day_quantity = "";
$all_day_max_pax = "";

$count_different_days_data = 0;
if (isset($different_days_data)) {
$count_different_days_data = count($different_days_data);
if ($count_different_days_data > 0) {

$all_day_time_from = $different_days_data[0]->time_from;
$all_day_time_from_am_pm = $different_days_data[0]->time_from_am_pm;
$all_day_time_to = $different_days_data[0]->time_to;
$all_day_time_to_am_pm = $different_days_data[0]->time_to_am_pm;
$all_day_quantity = $different_days_data[0]->quantity;
$all_day_max_pax = $different_days_data[0]->max_pax;

}

foreach ($different_days_data as $different_days_val) {

if ($different_days_val->available_day == 'monday') {
$available_day_monday_diff_days = $different_days_val->available_day;

$time_from_monday_diff_days = $different_days_val->time_from;
$time_from_am_pm_monday_diff_days = $different_days_val->time_from_am_pm;

$time_to_monday_diff_days = $different_days_val->time_to;
$time_to_am_pm_monday_diff_days = $different_days_val->time_to_am_pm;

$quantity_monday_diff_days = $different_days_val->quantity;
$max_pax_monday_diff_days = $different_days_val->max_pax;


}


}
}

?>





<li id="what_day_of_week_li" style="<?php echo $is_multiple_dates ? 'display:block;' : 'display:none;'; ?>">
    <h4>What day of the week?</h4>


    <?php
    $everyday_of_week       = isset($offer_detail[0]->everyday_of_week) && (int)$offer_detail[0]->everyday_of_week;
    $select_specific_days   = isset($offer_detail[0]->select_specific_days) && (int)$offer_detail[0]->select_specific_days;
    $hide_everyday_of_week  = true; // I have no idea why this was done..
    ?>

    <input type="hidden" name="everyday_of_week" id="everyday_of_week" value=""<?php echo $everyday_of_week && !isset($offer_rerun_id) ? " checked='checked'" : ""; ?>>
    <input type="hidden" name="select_specific_days" id="select_specific_days" value="" <?php echo $select_specific_days && !isset($offer_rerun_id) ? " checked='checked'" : ""; ?>>

    <div class="container" id="container_daywise">
        <?php

        $specific_days_week     = isset($offer_detail[0]->specific_days_week) && $offer_detail[0]->specific_days_week;
        $specific_days_week_arr = explode(",", strtolower($specific_days_week));
        foreach($specific_days_week_arr as $key => $val)
            $specific_days_week_arr[$key] = trim($val);



        $p = 1;
        foreach ($week_days as $val) {
            $specific_days_exist = count($days_between_multiple_dates_array) > 0 && in_array($val[1], $days_between_multiple_dates_array) ? 1 : 0;
            ?>

            <span class="daywise" id="daywise_<?= $p ?>" style="<?php echo $specific_days_exist ? 'display:block;' : 'display:none;'; ?>">
                <input class="specific_days_week_class" name="specific_days_week[]" id="specific_days_week_<?= $val[1] ?>" type="checkbox" value="<?= $val[1] ?>" <?php echo in_array($val[1], $specific_days_week_arr) ? ' checked="checked"' : ''; ?>>

                <p><span><?= $val[0] ?></span></p>
            </span>

            <?php
            $p++;
        }
        ?>
    </div>
</li>


<li id="available_time_slots_and_capacity_li" <?php if ((isset($offer_detail[0]->all_day_same_time) && $offer_detail[0]->all_day_same_time == '1') || (isset($offer_detail[0]->different_days_different_time) && $offer_detail[0]->different_days_different_time == '1') || (isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours == '1')) {
    if (isset($offer_rerun_id) && $offer_rerun_id != "" && $is_multiple_dates) { ?> style="display:none;"  <?php } else {
    }
} else { ?> style="display:none;" <?php } ?> >
<h4>Available time slots and capacity:</h4>

<div class="main_con" style="display: none">


    <div class="container" id="container_all_day_same_time"><input name="all_day_same_time" id="all_day_same_time"
                                                                   type="checkbox"
                                                                   value="" <?php if (isset($offer_detail[0]->all_day_same_time) && $offer_detail[0]->all_day_same_time == '1' && !isset($offer_rerun_id)) {
            echo 'checked';
        } ?> >

        <p><span>
						<!-- All the same time --> Select times</span></p></div>


    <div class="container"
         id="allthe_same_time_div" <?php if (isset($offer_detail[0]->all_day_same_time) && $offer_detail[0]->all_day_same_time == '1' && !isset($offer_rerun_id)) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> >

        <!-- <span class="all_bg"><input type="text" value="All" class="all" readonly /></span> -->

                            <span class="from_date">
							<!--<p class="large">from</p>-->

							<!-- <input type="text" id="all_day_time_from" name="all_day_time_from" value="<?php if (isset($offer_detail[0]->all_day_time_from)) {
                                echo stripslashes($offer_detail[0]->all_day_time_from);
                            } ?>" placeholder="11:00" class="time" onkeypress="return isNumberKey(event, this.id);" /> -->


							<div class="from_dd">
                                <?php echo $time_dropdown = $this->users->get_time_dropdown('all_day_time_from', 'revostyled all_day_time_from_dropdown', $all_day_time_from); ?>
                            </div>


                         <div class="am_pm">
                             <?php
                             $from_am_pm = '';
                             if (isset($offer_detail[0]->all_day_time_to_am_pm)) {
                                 $from_am_pm = stripslashes($offer_detail[0]->all_day_time_to_am_pm);
                             }

                             echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('all_day_time_from_am_pm', 'revostyled', $all_day_time_from_am_pm);

                             ?>

                         </div>

							</span>
                            <span class="to_date">
							<!--<p class="large">to</p>-->

							<!-- <input type="text" name="all_day_time_to" id="all_day_time_to" value="<?php if (isset($offer_detail[0]->all_day_time_to)) {
                                echo stripslashes($offer_detail[0]->all_day_time_to);
                            } ?>" placeholder="11:45" class="time" onkeypress="return isNumberKey(event, this.id);" /> -->

							<div class="from_dd">
                                <?php echo $time_dropdown = $this->users->get_time_dropdown('all_day_time_to', 'revostyled all_day_time_to_dropdown', $all_day_time_to, 'to'); ?>
                            </div>

                                <?php
                                $to_am_pm = '';
                                if (isset($offer_detail[0]->all_day_time_to_am_pm)) {
                                    $to_am_pm = stripslashes($offer_detail[0]->all_day_time_to_am_pm);
                                }
                                echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('all_day_time_to_am_pm', 'revostyled', $all_day_time_to_am_pm);
                                ?>
							</span>
        <span class="qty"><p class="large">Qty</p><input type="text" name="all_day_quantity" id="all_day_quantity"
                                                         value="<?php echo $all_day_quantity; ?>"
                                                         class="qty_val"/></span>

        <span class="max_pax"
              id="all_day_max_pax_span_id" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
                class="large2">Max <br/>pax</p><input type="text" name="all_day_max_pax" id="all_day_max_pax"
                                                      value="<?php if ($all_day_max_pax != '0') {
                                                          echo $all_day_max_pax;
                                                      } ?>" placeholder="-" class="max"/></span>

    </div>
</div>

<div class="main_con"
     id="main_con_diff_days_time" <?php if (isset($offer_detail[0]->different_days_different_time) && $offer_detail[0]->different_days_different_time == '1') {
    echo 'style="display:block;"';
} ?> >

<div class="container" id="main_con_diff_days_time_container">

    <input name="different_days_different_time" id="different_days_different_time" type="checkbox"
           value="" <?php if (isset($offer_detail[0]->different_days_different_time) && $offer_detail[0]->different_days_different_time == '1' && !isset($offer_rerun_id)) {
        echo 'checked';
    } ?>>

    <p>
						<span id="dddt_span_id">
						<?php if ($is_running_today) { ?>
                            Same day at different times
                        <?php } else { ?>
                            Different days at different times
                        <?php } ?>
						</span></p></div>


<div
    id="all_diff_days_div" <?php if (isset($offer_detail[0]->different_days_different_time) && $offer_detail[0]->different_days_different_time == '1' && !isset($offer_rerun_id)) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> >

<!-------------------------------------------- FOR EDIT -------------------------------->

<input type="hidden" name="count_different_days_data" id="count_different_days_data"
       value="<?= $count_different_days_data ?>">

<?php
$moat_edt_id = '';
$diff_days_id_array = array();
if (isset($different_days_data) && count($different_days_data) > 0) {
    $m = 1;
    $l = 0;

    foreach ($different_days_data as $different_days_val) {
        $l++;
        //echo $different_days_val->available_day;
        $moat_edt_id = $different_days_val->id;
        array_push($diff_days_id_array, $moat_edt_id);

        $available_day = $different_days_val->available_day;

        $time_from = $different_days_val->time_from;
        $time_from_am_pm = $different_days_val->time_from_am_pm;

        $time_to = $different_days_val->time_to;
        $time_to_am_pm = $different_days_val->time_to_am_pm;

        $quantity = $different_days_val->quantity;
        $max_pax = $different_days_val->max_pax;

        if ($available_day == 'monday') {
            $dayname = str_replace("monday", "Mon", $available_day);
        }
        if ($available_day == 'tuesday') {
            $dayname = str_replace("tuesday", "Tue", $available_day);
        }
        if ($available_day == 'wednesday') {
            $dayname = str_replace("wednesday", "Wed", $available_day);
        }
        if ($available_day == 'thursday') {
            $dayname = str_replace("thursday", "Thu", $available_day);
        }
        if ($available_day == 'friday') {
            $dayname = str_replace("friday", "Fri", $available_day);
        }
        if ($available_day == 'saturday') {
            $dayname = str_replace("saturday", "Sat", $available_day);
        }
        if ($available_day == 'sunday') {
            $dayname = str_replace("sunday", "Sun", $available_day);
        }


        ?>

        <!-- onmouseover="showDaysOptionsEdt('<?= $m ?>')" onmouseout="hideDaysOptionsEdt()" -->
        <div class="container" id="diff_days_div_id_edt_<?= $moat_edt_id ?>" style="display:block;">
            <div class="all_bg">

                <!-- <input type="hidden" name="available_day_edt_<?= $moat_edt_id ?>" id="available_day_edt_<?= $moat_edt_id ?>" value="<?= $available_day ?>" placeholder="Day" class="all"  />

									<input type="text" name="available_day_edt_view_<?= $moat_edt_id ?>" id="available_day_edt_view_<?= $moat_edt_id ?>" value="<?= $dayname ?>" placeholder="Day" class="all available_day_edt_view_class_<?= $l ?>" onclick="showDaysOptionsEdt('<?= $m ?>')" /> 	 -->

                <input type="hidden" name="available_day_name_edt_<?= $moat_edt_id ?>"
                       id="available_day_name_edt_<?= $moat_edt_id ?>" value="<?= $available_day ?>"/>

                <input type="hidden" name="moat_edt_id_<?= $m ?>" id="moat_edt_id_<?= $m ?>"
                       value="<?= $moat_edt_id ?>">

                <div class="days_drop" id="available_day_edt_option_<?= $m ?>" style="display:block;">

                    <select name="available_day_edt_<?= $moat_edt_id ?>" id="available_day_edt_<?= $moat_edt_id ?>"
                            class="revostyled available_day_edt_view_class_<?= $l ?>">
                        <?php foreach ($week_days as $val) { ?>

                            <option
                                value="<?php echo $val[1]; ?>" <?php if (isset($available_day) && $val[1] == $available_day) {
                                echo "selected";
                            } ?>  ><?php echo $val[0]; ?></option>


                        <?php } ?>
                    </select>


                </div>

            </div>

								<span class="from_date">
								<!--<p class="large">from</p>-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_edt_' . $moat_edt_id, 'revostyled time_from_edt_dropdown_' . $l, $time_from); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_edt_am_pm_' . $moat_edt_id, 'revostyled', $time_from_am_pm); ?>
								</span>
								<span class="to_date">
								<!--<p class="large">to</p>	-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_edt_' . $moat_edt_id, 'revostyled time_to_edt_dropdown_' . $l, $time_to, 'to'); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_edt_am_pm_' . $moat_edt_id, 'revostyled', $time_to_am_pm); ?>
								</span>
            <span class="qty"><p class="large">Qty</p><input type="text" name="quantity_edt_<?= $moat_edt_id ?>"
                                                             id="quantity_edt_<?= $moat_edt_id ?>"
                                                             value="<?= $quantity ?>"
                                                             class="qty_val quantity_edt_<?= $l ?>"/></span>


            <span
                class="max_pax" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
                    class="large2">Max <br/>pax</p><input name="max_pax_edt_<?= $moat_edt_id ?>"
                                                          id="max_pax_edt_<?= $moat_edt_id ?>" type="text"
                                                          value="<?php if ($max_pax != "0") {
                                                              echo $max_pax;
                                                          } ?>" placeholder="-"
                                                          class="max max_pax_edt_<?= $l ?>"/></span>

        </div>


        <?php
        $m++;
    }
}

$diff_days_id_str = "";
if (count($diff_days_id_array) > 0) {
    $diff_days_id_str = implode("|", $diff_days_id_array);
}
?>

<input type="hidden" name="all_different_days_data_ids" id="all_different_days_data_ids"
       value="<?= $diff_days_id_str ?>">
<!-------------------------------------------- FOR EDIT -------------------------------->


<!-- onmouseover="showDaysOptions('1')" onmouseout="hideDaysOptions()" -->
<div class="container" id="1_diff_days_div_id" style="display:block;">
    <div class="all_bg">

        <!--  <input type="hidden" name="available_day_1" id="available_day_1" value="" placeholder="Day" class="all"  />

									<input type="text" name="available_day_view_1" id="available_day_view_1" value="" placeholder="Day" class="all" onclick="showDaysOptions('1')" />  -->

        <?php $i = 1; ?>

        <div class="days_drop" id="available_day_option_1" style="display:block;">
            <!-- <ul>
											<?php foreach ($week_days as $val) { ?>
												<li><a href="javascript:void(0)" onclick="set_available_day('<?= $val[0] ?>', '<?= $val[1] ?>','1')"><?= $val[0] ?></a></li>
											<?php } ?>
										</ul> -->

            <select name="available_day_1" id="available_day_1" class="revostyled">
                <option value="">Day</option>
                <?php foreach ($week_days as $val) { ?>
                    <option value="<?php echo $val[1]; ?>"><?php echo $val[0]; ?></option>
                <?php } ?>
            </select>
        </div>


    </div>

								<span class="from_date">
								<!--<p class="large">from</p>-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_1', 'revostyled time_from_dropdown_1', ''); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_1', 'revostyled'); ?>
								</span>
								<span class="to_date">
								<!--<p class="large">to</p>	-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_1', 'revostyled time_to_dropdown_1', '', 'to'); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_1', 'revostyled'); ?>
								</span>
    <span class="qty"><p class="large">Qty</p><input type="text" name="quantity_1" id="quantity_1" value=""
                                                     class="qty_val"/></span>
    <span
        class="max_pax" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
            class="large2">Max <br/>pax</p><input name="max_pax_1" id="max_pax_1" type="text" value="" placeholder="-"
                                                  class="max"/></span>
</div>


<?php
$display_style = '';
$z = 1;
for ($i = 2; $i <= 21; $i++) {

    if ($i <= 3) {
        $display_style = 'style="display:block;"'; // for edit mode

    } else {

        $display_style = 'style="display:none;"';
    }

    ?>

    <!-- onmouseover="showDaysOptions('<?= $i ?>')" onmouseout="hideDaysOptions()" -->
    <div class="container" id="<?= $i ?>_diff_days_div_id" <?= $display_style ?> >
        <div class="all_bg">

            <!-- <input type="hidden" name="available_day_<?= $i ?>" id="available_day_<?= $i ?>" value="" placeholder="Day" class="all" />
									<input type="text" name="available_day_view_<?= $i ?>" id="available_day_view_<?= $i ?>" value="" placeholder="Day" class="all" onclick="showDaysOptions('<?= $i ?>')" /> -->

            <div class="days_drop" id="available_day_option_<?= $i ?>" style="display:block;">
                <!-- <ul>
											<?php foreach ($week_days as $val) { ?>
												<li><a href="javascript:void(0)" onclick="set_available_day('<?= $val[0] ?>', '<?= $val[1] ?>','<?= $i ?>')"><?= $val[0] ?></a></li>
											<?php } ?>
										</ul> -->

                <select name="available_day_<?= $i ?>" id="available_day_<?= $i ?>" class="revostyled">
                    <option value="">Day</option>
                    <?php foreach ($week_days as $val) { ?>
                        <option value="<?php echo $val[1]; ?>"><?php echo $val[0]; ?></option>
                    <?php } ?>
                </select>

            </div>


        </div>
								<span class="from_date">
								<!--<p class="large">from</p>-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_from_' . $i, 'revostyled time_from_dropdown_' . $i, ''); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_' . $i, 'revostyled'); ?>
								</span>
								<span class="to_date">
								<!--<p class="large">to</p>-->
                                <div class="from_dd">
                                    <?php echo $time_dropdown = $this->users->get_time_dropdown('time_to_' . $i, 'revostyled time_to_dropdown_' . $i, '', 'to'); ?>
                                </div>
                                    <?php echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_' . $i, 'revostyled'); ?>
								</span>
        <span class="qty"><p class="large">Qty</p><input type="text" name="quantity_<?= $i ?>" id="quantity_<?= $i ?>"
                                                         value="" class="qty_val"/></span>
        <span
            class="max_pax" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
                class="large2">Max <br/>pax</p><input name="max_pax_<?= $i ?>" id="max_pax_<?= $i ?>" type="text"
                                                      value="" placeholder="-" class="max"/></span>
    </div>

    <?php $z++;
} ?>


<div class="container" id="add_time_slot" style="display:block;">
    <span class="add_time_slot"><a href="javascript:void(0)" onclick="showhide_different_day()">+ add time
            slot</a></span>
</div>


</div>
</div>


<div class="main_con"  style="display: none">
<div class="container" id="container_all_opening_hours">
    <div class="left" id="all_opening_hours_left_div"><input name="all_opening_hours" id="all_opening_hours"
                                                             type="checkbox"
                                                             value="" <?php if (isset($offer_detail[0]->all_opening_hours) && !isset($offer_rerun_id)) {
            if ($offer_detail[0]->all_opening_hours == '1') {
                echo 'checked';
            }
        } ?>>

        <p><span>All opening hours</span></p></div>


    <div
        class="edit_merchant" <?php if (isset($offer_detail[0]->id)) { ?> style="display:block;" <?php } else { ?> style="display:block;" <?php } ?> >
        <a href="javascript:void(0)" onclick="edit_business_opening_hours()"><img alt=""
                                                                                  src="<?php echo base_url() ?>public/images/edit_merchant.png">Edit</a>
    </div>


</div>

<!-- All opening hours days start -->









































<!-- BUSINESS OPENING HOURS -->
<div id="business_opening_hrs_info_div"></div>
<!-- BUSINESS OPENING HOURS -->


<div
    id="all_opening_hours_div" <?php if (isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours == '1' && !isset($offer_rerun_id)) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> >
<?php

//echo count($different_days_data);

if (isset($offer_detail[0]->all_opening_hours) && $offer_detail[0]->all_opening_hours == '1' && isset($different_days_data) && count($different_days_data) > 0) {
    $added_open_hrs_day_array = array(); // already added day name array


//***********************************************************************************************************//


    $merchant_opening_hours_data_array = array();
    if (isset($merchant_opening_hours) && count($merchant_opening_hours) > 0) {
        $k = 0;
        foreach ($merchant_opening_hours as $opening_hours_val) {
            $available_day_open_hrs = $opening_hours_val->day;

            /*
		$time_from_open_hrs = $opening_hours_val->from_hour;
		$time_from_am_pm_open_hrs = $opening_hours_val->from_ampm;

		$time_to_open_hrs = $opening_hours_val->to_hour;
		$time_to_am_pm_open_hrs = $opening_hours_val->to_ampm;

		$quantity_open_hrs="";
		$max_pax_open_hrs="";
		*/

            if ($available_day_open_hrs != "") {
                $merchant_opening_hours_data_array[$k]['day'] = $opening_hours_val->day;
                $merchant_opening_hours_data_array[$k]['from_hour'] = $opening_hours_val->from_hour;
                $merchant_opening_hours_data_array[$k]['from_ampm'] = $opening_hours_val->from_ampm;
                $merchant_opening_hours_data_array[$k]['to_hour'] = $opening_hours_val->to_hour;
                $merchant_opening_hours_data_array[$k]['to_ampm'] = $opening_hours_val->to_ampm;
                $merchant_opening_hours_data_array[$k]['quantity'] = "";
                $merchant_opening_hours_data_array[$k]['max_pax'] = "";

                $days_display_order = '';

                if ($available_day_open_hrs == 'monday') {
                    $days_display_order = '1';

                } else if ($available_day_open_hrs == 'tuesday') {

                    $days_display_order = '2';
                } else if ($available_day_open_hrs == 'wednesday') {

                    $days_display_order = '3';
                } else if ($available_day_open_hrs == 'thursday') {

                    $days_display_order = '4';

                } else if ($available_day_open_hrs == 'friday') {

                    $days_display_order = '5';
                } else if ($available_day_open_hrs == 'saturday') {

                    $days_display_order = '6';
                } else if ($available_day_open_hrs == 'sunday') {

                    $days_display_order = '7';
                }
                $merchant_opening_hours_data_array[$k]['moh_id'] = "";
                $merchant_opening_hours_data_array[$k]['display_order'] = $days_display_order;

                $k++;
            }

        }

    }


    $added_opening_hours_data_array = array(); // for data which are alrady aded
    if (isset($different_days_data) && count($different_days_data) > 0) {

        $k = 0;
        foreach ($different_days_data as $different_days_val) {

            $available_day_open_hrs = $different_days_val->available_day;

            /*
		array_push($added_open_hrs_day_array,$available_day_open_hrs);

		$time_from_open_hrs = $different_days_val->time_from;
		$time_from_am_pm_open_hrs = $different_days_val->time_from_am_pm;

		$time_to_open_hrs = $different_days_val->time_to;
		$time_to_am_pm_open_hrs = $different_days_val->time_to_am_pm;

		$quantity_open_hrs=$different_days_val->quantity;
		$max_pax_open_hrs=$different_days_val->max_pax;


		if($available_day_open_hrs=='monday'){ $dayname = str_replace("monday","Mon",$available_day_open_hrs); }
		if($available_day_open_hrs=='tuesday'){ $dayname = str_replace("tuesday","Tue",$available_day_open_hrs); }
		if($available_day_open_hrs=='wednesday'){ $dayname = str_replace("wednesday","Wed",$available_day_open_hrs); }
		if($available_day_open_hrs=='thursday'){ $dayname = str_replace("thursday","Thu",$available_day_open_hrs); }
		if($available_day_open_hrs=='friday'){ $dayname = str_replace("friday","Fri",$available_day_open_hrs); }
		if($available_day_open_hrs=='saturday'){ $dayname = str_replace("saturday","Sat",$available_day_open_hrs); }
		if($available_day_open_hrs=='sunday'){ $dayname = str_replace("sunday","Sun",$available_day_open_hrs); }
		$moh_id = $different_days_val->id; // merchant opening hours id

		*/

            if ($available_day_open_hrs != "") {

                $added_opening_hours_data_array[$k]['day'] = $different_days_val->available_day;
                $added_opening_hours_data_array[$k]['from_hour'] = $different_days_val->time_from;
                $added_opening_hours_data_array[$k]['from_ampm'] = $different_days_val->time_from_am_pm;
                $added_opening_hours_data_array[$k]['to_hour'] = $different_days_val->time_to;
                $added_opening_hours_data_array[$k]['to_ampm'] = $different_days_val->time_to_am_pm;
                $added_opening_hours_data_array[$k]['quantity'] = $different_days_val->quantity;
                $added_opening_hours_data_array[$k]['max_pax'] = $different_days_val->max_pax;

                $days_display_order = '';

                if ($available_day_open_hrs == 'monday') {
                    $days_display_order = '1';

                } else if ($available_day_open_hrs == 'tuesday') {

                    $days_display_order = '2';
                } else if ($available_day_open_hrs == 'wednesday') {

                    $days_display_order = '3';
                } else if ($available_day_open_hrs == 'thursday') {

                    $days_display_order = '4';

                } else if ($available_day_open_hrs == 'friday') {

                    $days_display_order = '5';
                } else if ($available_day_open_hrs == 'saturday') {

                    $days_display_order = '6';
                } else if ($available_day_open_hrs == 'sunday') {

                    $days_display_order = '7';
                }
                $added_opening_hours_data_array[$k]['moh_id'] = $different_days_val->id;
                $added_opening_hours_data_array[$k]['display_order'] = $days_display_order;

                $k++;

            }

        }


    }

    /*
echo '<pre>';
print_r($merchant_opening_hours_data_array);
echo '</pre>';

echo '<pre>';
print_r($added_opening_hours_data_array);
echo '</pre>';
echo '<br>................<br>';
*/

    $final_opening_hours_data_array = array(); // AFter filteration
    if (count($merchant_opening_hours_data_array) > 0) {

        $b = 0;
        foreach ($merchant_opening_hours_data_array as $merchant_val) {
            $day_exist_check = 0;

            //echo $merchant_val['day'];
            if (count($added_opening_hours_data_array) > 0) {
                foreach ($added_opening_hours_data_array as $added_val) {
                    if ($merchant_val['day'] == $added_val['day']) {
                        $day_exist_check = 1;

                        // assigning data after comparison start
                        $added_day = "";
                        $added_day = $added_val['day'];
                        $final_opening_hours_data_array[$b]['available_day'] = $added_day;
                        $final_opening_hours_data_array[$b]['time_from'] = $added_val['from_hour'];
                        $final_opening_hours_data_array[$b]['time_from_am_pm'] = $added_val['from_ampm'];
                        $final_opening_hours_data_array[$b]['time_to'] = $added_val['to_hour'];
                        $final_opening_hours_data_array[$b]['time_to_am_pm'] = $added_val['to_ampm'];
                        $final_opening_hours_data_array[$b]['quantity'] = $added_val['quantity'];
                        $final_opening_hours_data_array[$b]['max_pax'] = $added_val['max_pax'];

                        $days_display_order = '';
                        if ($added_day == 'monday') {
                            $days_display_order = '1';

                        } else if ($added_day == 'tuesday') {

                            $days_display_order = '2';
                        } else if ($added_day == 'wednesday') {

                            $days_display_order = '3';
                        } else if ($added_day == 'thursday') {

                            $days_display_order = '4';

                        } else if ($added_day == 'friday') {

                            $days_display_order = '5';
                        } else if ($added_day == 'saturday') {

                            $days_display_order = '6';
                        } else if ($added_day == 'sunday') {

                            $days_display_order = '7';
                        }
                        $final_opening_hours_data_array[$b]['moh_id'] = $added_val['moh_id'];
                        $final_opening_hours_data_array[$b]['display_order'] = $days_display_order;
                        // assigning data after comparison start

                        break;

                    } else {

                        $day_exist_check = 0;
                    }
                }

            }

            if ($day_exist_check == 0) {

                $merchant_day = "";
                $merchant_day = $merchant_val['day'];
                $final_opening_hours_data_array[$b]['available_day'] = $merchant_day;
                $final_opening_hours_data_array[$b]['time_from'] = $merchant_val['from_hour'];
                $final_opening_hours_data_array[$b]['time_from_am_pm'] = $merchant_val['from_ampm'];
                $final_opening_hours_data_array[$b]['time_to'] = $merchant_val['to_hour'];
                $final_opening_hours_data_array[$b]['time_to_am_pm'] = $merchant_val['to_ampm'];
                $final_opening_hours_data_array[$b]['quantity'] = $merchant_val['quantity'];
                $final_opening_hours_data_array[$b]['max_pax'] = $merchant_val['max_pax'];

                $days_display_order = '';
                if ($merchant_day == 'monday') {
                    $days_display_order = '1';

                } else if ($merchant_day == 'tuesday') {

                    $days_display_order = '2';
                } else if ($merchant_day == 'wednesday') {

                    $days_display_order = '3';
                } else if ($merchant_day == 'thursday') {

                    $days_display_order = '4';

                } else if ($merchant_day == 'friday') {

                    $days_display_order = '5';
                } else if ($merchant_day == 'saturday') {

                    $days_display_order = '6';
                } else if ($merchant_day == 'sunday') {

                    $days_display_order = '7';
                }
                $final_opening_hours_data_array[$b]['moh_id'] = $merchant_val['moh_id'];
                $final_opening_hours_data_array[$b]['display_order'] = $days_display_order;

            }

            $b++;
        }

    }


//################FOR SORTING PRICE START HERE###########//

    $newarray = array();
    foreach ($final_opening_hours_data_array as $key => $row) {
        $newarray[$key] = $row['display_order'];
    }
    array_multisort($newarray, SORT_ASC, $final_opening_hours_data_array);

//################FOR SORTING PRICE END HERE###########//

    /*
echo '<pre>';
print_r($final_opening_hours_data_array);
echo '</pre>';
echo '<br>................<br>';
*/

//**********************************************************************************************************//


    $new_sno = 0; // new id
    if (isset($final_opening_hours_data_array) && count($final_opening_hours_data_array) > 0) {


        foreach ($final_opening_hours_data_array as $final_days_val) {


            $available_day_open_hrs = $final_days_val['available_day'];

            $time_from_open_hrs = $final_days_val['time_from'];
            $time_from_am_pm_open_hrs = $final_days_val['time_from_am_pm'];

            $time_to_open_hrs = $final_days_val['time_to'];
            $time_to_am_pm_open_hrs = $final_days_val['time_to_am_pm'];

            $quantity_open_hrs = $final_days_val['quantity'];
            $max_pax_open_hrs = $final_days_val['max_pax'];

            if ($available_day_open_hrs == 'monday') {
                $dayname = str_replace("monday", "Mon", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'tuesday') {
                $dayname = str_replace("tuesday", "Tue", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'wednesday') {
                $dayname = str_replace("wednesday", "Wed", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'thursday') {
                $dayname = str_replace("thursday", "Thu", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'friday') {
                $dayname = str_replace("friday", "Fri", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'saturday') {
                $dayname = str_replace("saturday", "Sat", $available_day_open_hrs);
            }
            if ($available_day_open_hrs == 'sunday') {
                $dayname = str_replace("sunday", "Sun", $available_day_open_hrs);
            }

            $moh_id = $final_days_val['moh_id']; // merchant opening hours id

            $open_hrs_style = '';
            if (empty($moh_id) || $moh_id == "") {
                $new_sno++;
                $moh_id = $new_sno;
                $open_hrs_style = 'style="display:none;"';

            } else {

                $open_hrs_style = 'style="display:block;"';
            }

            if ($available_day_open_hrs != "") {


                ?>

                <div class="container <?= $available_day_open_hrs ?>_open_hrs_div_id" <?= $open_hrs_style ?> >
                    <div class="all_bg">
                        <input type="hidden" name="available_day_open_hrs_hd_<?= $moh_id ?>"
                               id="available_day_open_hrs_hd_<?= $moh_id ?>" value="<?= $available_day_open_hrs ?>"
                               readonly class="all"/>
                        <input type="text" name="available_day_open_hrs_<?= $moh_id ?>"
                               id="available_day_open_hrs_<?= $moh_id ?>" value="<?= $dayname ?>" readonly class="all"/>
                    </div>
						<span class="from_date">
						<!--<p class="large">from</p>-->
						<div class="from_dd">
                            <?php //echo $time_dropdown = $this->users->get_time_dropdown('time_from_open_hrs_'.$moh_id,'revostyled '.$available_day_open_hrs.'_from_dropdown',$time_from_open_hrs); ?>
                            <input type="text" name="" id="" value="<?= $time_from_open_hrs ?>" readonly class="all"/>
                            <input type="hidden" name="time_from_open_hrs_<?= $moh_id ?>"
                                   id="time_from_open_hrs_<?= $moh_id ?>" value="<?= $time_from_open_hrs ?>" readonly
                                   class="all <?php echo $available_day_open_hrs ?>_from_dropdown"/>
                        </div>
                            <?php //echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_open_hrs_'.$moh_id,'revostyled ',$time_from_am_pm_open_hrs); ?>
                            <input type="text" name="" id="" value="<?= $time_from_am_pm_open_hrs ?>" readonly
                                   class="all"/>
						<input type="hidden" name="time_from_am_pm_open_hrs_<?= $moh_id ?>"
                               id="time_from_am_pm_open_hrs_<?= $moh_id ?>" value="<?= $time_from_am_pm_open_hrs ?>"
                               readonly class="all"/>
						</span>
						<span class="to_date"><!--<p class="large">to</p>-->
						<div class="from_dd">
                            <?php //echo $time_dropdown = $this->users->get_time_dropdown('time_to_open_hrs_'.$moh_id, 'revostyled '.$available_day_open_hrs.'_to_dropdown',$time_to_open_hrs,'to'); ?>
                            <input type="text" name="" id="" value="<?= $time_to_open_hrs ?>" readonly class="all"/>
                            <input type="hidden" name="time_to_open_hrs_<?= $moh_id ?>"
                                   id="time_to_open_hrs_<?= $moh_id ?>" value="<?= $time_to_open_hrs ?>" readonly
                                   class="all <?= $available_day_open_hrs ?>_to_dropdown"/>
                        </div>
                            <?php // echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_open_hrs_'.$moh_id,'revostyled ',$time_to_am_pm_open_hrs); ?>
                            <input type="text" name="" id="" value="<?= $time_to_am_pm_open_hrs ?>" readonly
                                   class="all"/>
						<input type="hidden" name="time_to_am_pm_open_hrs_<?= $moh_id ?>"
                               id="time_to_am_pm_open_hrs_<?= $moh_id ?>" value="<?= $time_to_am_pm_open_hrs ?>"
                               readonly class="all"/>
						</span>
                    <span class="qty"><p class="large">Qty</p><input type="text" name="quantity_open_hrs_<?= $moh_id ?>"
                                                                     id="quantity_open_hrs_<?= $moh_id ?>"
                                                                     value="<?= $quantity_open_hrs ?>"
                                                                     class="qty_val <?= $available_day_open_hrs ?>_qty_val_open_hrs"/></span>
                    <span
                        class="max_pax" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
                            class="large2">Max <br/>pax</p><input name="max_pax_open_hrs_<?= $moh_id ?>"
                                                                  id="max_pax_open_hrs_<?= $moh_id ?>" type="text"
                                                                  value="<?php if ($max_pax_open_hrs != "0") {
                                                                      echo $max_pax_open_hrs;
                                                                  } ?>" placeholder="-"
                                                                  class="max <?= $available_day_open_hrs ?>_max_open_hrs"/></span>
                </div>


            <?php

            }


        }


    }
    ?>

    <input type="hidden" name="remaining_opening_hours_days_count" id="remaining_opening_hours_days_count"
           value="<?= $new_sno ?>">
<?php


} else {

if (isset($merchant_opening_hours) && count($merchant_opening_hours) > 0) {
foreach ($merchant_opening_hours as $opening_hours_val) {
$available_day_open_hrs = $opening_hours_val->day;

$time_from_open_hrs = $opening_hours_val->from_hour;
$time_from_am_pm_open_hrs = $opening_hours_val->from_ampm;

$time_to_open_hrs = $opening_hours_val->to_hour;
$time_to_am_pm_open_hrs = $opening_hours_val->to_ampm;

$quantity_open_hrs = "";
$max_pax_open_hrs = "";

if ($available_day_open_hrs == 'monday') {
    $dayname = str_replace("monday", "Mon", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'tuesday') {
    $dayname = str_replace("tuesday", "Tue", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'wednesday') {
    $dayname = str_replace("wednesday", "Wed", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'thursday') {
    $dayname = str_replace("thursday", "Thu", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'friday') {
    $dayname = str_replace("friday", "Fri", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'saturday') {
    $dayname = str_replace("saturday", "Sat", $available_day_open_hrs);
}
if ($available_day_open_hrs == 'sunday') {
    $dayname = str_replace("sunday", "Sun", $available_day_open_hrs);
}

$moh_id = $opening_hours_val->id; // merchant opening hours id

if ($available_day_open_hrs != "") {
?>

<div class="container <?= $available_day_open_hrs ?>_open_hrs_div_id" style="display:none;">
    <div class="all_bg">
        <input type="hidden" name="available_day_open_hrs_hd_<?= $moh_id ?>"
               id="available_day_open_hrs_hd_<?= $moh_id ?>" value="<?= $available_day_open_hrs ?>"
               readonly class="all"/>
        <input type="text" name="available_day_open_hrs_<?= $moh_id ?>"
               id="available_day_open_hrs_<?= $moh_id ?>" value="<?= $dayname ?>" readonly
               class="all <?= $available_day_open_hrs ?>_available_day_open_hrs"/>
    </div>
		<span class="from_date">

        <div class="from_dd">
            <?php //echo $time_dropdown = $this->users->get_time_dropdown('time_from_open_hrs_'.$moh_id,'revostyled '.$available_day_open_hrs.'_from_dropdown' ,$time_from_open_hrs); ?>
            <input type="text" name="" id="" value="<?= $time_from_open_hrs ?>" readonly class="all"/>
            <input type="hidden" name="time_from_open_hrs_<?= $moh_id ?>" id="time_from_open_hrs_<?= $moh_id ?>"
                   value="<?= $time_from_open_hrs ?>" readonly
                   class="all <?php echo $available_day_open_hrs ?>_from_dropdown"/>
        </div>

            <?php //echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_from_am_pm_open_hrs_'.$moh_id,'revostyled ',$time_from_am_pm_open_hrs); ?>
            <input type="text" name="" id="" value="<?= $time_from_am_pm_open_hrs ?>" readonly class="all"/>
		<input type="hidden" name="time_from_am_pm_open_hrs_<?= $moh_id ?>" id="time_from_am_pm_open_hrs_<?= $moh_id ?>"
               value="<?= $time_from_am_pm_open_hrs ?>" readonly class="all"/>
		</span>

		<span class="to_date">
        <div class="from_dd">
            <?php // echo $time_dropdown = $this->users->get_time_dropdown('time_to_open_hrs_'.$moh_id,'revostyled '.$available_day_open_hrs.'_to_dropdown',$time_to_open_hrs,'to'); ?>
            <input type="text" name="" id="" value="<?= $time_to_open_hrs ?>" readonly class="all"/>
            <input type="hidden" name="time_to_open_hrs_<?= $moh_id ?>" id="time_to_open_hrs_<?= $moh_id ?>"
                   value="<?= $time_to_open_hrs ?>" readonly class="all <?= $available_day_open_hrs ?>_to_dropdown"/>
        </div>

            <?php //echo $am_pm_dropdown = $this->users->get_am_pm_dropdown('time_to_am_pm_open_hrs_'.$moh_id,'revostyled ',$time_to_am_pm_open_hrs); ?>

            <input type="text" name="" id="" value="<?= $time_to_am_pm_open_hrs ?>" readonly class="all"/>
		<input type="hidden" name="time_to_am_pm_open_hrs_<?= $moh_id ?>" id="time_to_am_pm_open_hrs_<?= $moh_id ?>"
               value="<?= $time_to_am_pm_open_hrs ?>" readonly class="all"/>
		</span>
                    <span class="qty"><p class="large">Qty</p><input type="text" name="quantity_open_hrs_<?= $moh_id ?>"
                                                                     id="quantity_open_hrs_<?= $moh_id ?>"
                                                                     value="<?= $quantity_open_hrs ?>"
                                                                     class="qty_val <?= $available_day_open_hrs ?>_qty_val_open_hrs"/></span>
                    <span
                        class="max_pax" <?php if ($max_pax_display_check == 1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> ><p
                            class="large2">Max <br/>pax</p><input name="max_pax_open_hrs_<?= $moh_id ?>"
                                                                  id="max_pax_open_hrs_<?= $moh_id ?>" type="text"
                                                                  value="<?= $max_pax_open_hrs ?>" placeholder="-"
                                                                  class="max <?= $available_day_open_hrs ?>_max_open_hrs"/></span>
</div>
<?php
}
}
}

}

