$(document).ready(function() {
    
    $('#logo').click(function(e) {
        e.preventDefault();
        $('html, body').stop().animate({ scrollTop: 0 }, 1000, 'linear');
        // replace 'linear' with 'swing' to see different easing
    });
	
	$('#up').click(function(e) {
        e.preventDefault();
        $('html, body').stop().animate({ scrollTop: 0 }, 1000, 'linear');
        // replace 'linear' with 'swing' to see different easing
    });
	
	$('.refine_search').click(function(){
		$(".filter1_bg").toggle();          
	});
	
	$('.more_options').click(function(){
		$(".more_option").toggle();          
	});
	
	$('.select_city').click(function(){
		$(".city").toggle(); 
		
		if($(".city").css('display') == 'block')
		{
			$('.nav_bg').css({'height':'auto'});
		} 
		else 
		{
			$('.nav_bg').css({'height':'100px'});	
		}         
	});
	
	
	/*MERCHANT 9*/
	
	
	$('#expand_all').click(function(){
		$('.table_customer_data').css('display','block');	
		$('.add_calander').css('display','block');
		
	});	
	
	$('#collapse_all').click(function(){
		$('.table_customer_data').css('display','none');	
		$('.add_calander').css('display','none');
		
	});	
	
	$('#collapse_1').click(function(){
		$('#table_customer_data1').toggle();
		
		if($("#table_customer_data1").css('display') == 'block')
		{
			$('#collapse_1').text('- Collapse');
		} 
		else 
		{
			$('#collapse_1').text('+ Expand');	
		}	
		
	});	
	
	
	$('#arrow_1').click(function(){
		$('#add_calander_1').toggle();
		
		if($("#add_calander_1").css('display') == 'block')
		{
			$('#arrow_1').addClass('active');
		} 
		else 
		{
			$('#arrow_1').removeClass('active');	
		}	
		
	});	
	
	
	$('#filter_1').click(function(){
		$('#submenu_nav1').toggle();		
	});	
	$('#filter_2').click(function(){
		$('#submenu_nav2').toggle();		
	});	
	$('#filter_3').click(function(){
		$('#submenu_nav3').toggle();		
	});	
	$('#filter_4').click(function(){
		$('#submenu_nav4').toggle();		
	});	
	$('#filter_5').click(function(){
		$('#submenu_nav5').toggle();		
	});	
	$('#filter_6').click(function(){
		$('#submenu_nav6').toggle();		
	});	
	
	
	

	
});

/*=======================TOP HEADER==========================*/

$(document).ready(function() {
		$('.login_dd_main input:checkbox:not([safari])').checkbox();
		$('.login_dd_main input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
		
		$('.login_dd_main input:checkbox:not([safari])').checkbox();
		$('.login_dd_main input[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
		
		$('.forgot_pass_content .forgot_pass_detail ul li input:checkbox:not([safari])').checkbox();
		$('.forgot_pass_content .forgot_pass_detail ul liinput[safari]:checkbox').checkbox({cls:'jquery-safari-checkbox'});
	
});

$(function() {
		$( "#datepicker" ).datepicker();
		$( "#anim" ).change(function() {
			$( "#datepicker" ).datepicker( "option", "showAnim", $( this ).val() );
		});
});

$(function() {
		$( "#datepicker2" ).datepicker();
		$( "#anim" ).change(function() {
			$( "#datepicker2" ).datepicker( "option", "showAnim", $( this ).val() );
		});
});


$(document).ready(function(){
	var height=$('.kwicks').height();
	var height2=$('.services_bg ul li').height();
	
	$(".kwicks").css('height',height2+'px');
	
	if ( height2 > 600) {
		
		$('.services_bg ul li').mouseenter(function(){	
			$(".kwicks").css('height',parseInt(height2+100)+'px');	
		});
	
		$('.services_bg ul li').mouseleave(function(){	
			$(".kwicks").css('height',height2+'px');
		});
		
	} else {
		
	}
	
	
	

});



$(document).ready(function(){
	var height=$(window).height();
		$("#slide1").css('min-height',height+'px');
	
	$('.log_in').click(function(){
		$(".login_dd_bg").toggle();
		
		if($(".login_dd_bg").css('display') == 'block')
		{
			$('.nav_bg').css({'height':'auto'});
		} 
		else 
		{
			$('.nav_bg').css({'height':'100px'});	
		}
				  
	});
	
	
		
			
}); 

 

$(window).resize(function() {
	var height=$(window).height();
	$("#slide1").css('min-height',height+'px');
});


$(window).scroll(function() {
   
   var scrollVal = $(this).scrollTop();
   var height_w=$(window).height();
   
	if ( scrollVal > height_w-100) {
		$('.nav_bg').css({'position':'fixed','top' :'0px'});
	} else {
		$('.nav_bg').css({'position':'absolute','top':'auto'});
	}
	
});


$(window).scroll(function() {
   var scrollVal = $(this).scrollTop();
   var height_y=$(window).height();
   
	if ( scrollVal > 0) {
		$('.nav_bg2').css({'position':'fixed','top' :'0px'});
	} else {
		$('.nav_bg2').css({'position':'relative','top':'auto'});
	}
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.signup_top_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.signup_top_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.customer_yello_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.customer_yello_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.search_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.search_bg').css({'margin-top':'0px'});
		
	}
	
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.merchant_header_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.merchant_header_bg').css({'margin-top':'0px'});
		
	}
	
	if($(".nav_bg2").css('position') == 'fixed'){
		
		$('.popolo_banner_bg').css({'margin-top':'100px'});
		
	} else {
		
		$('.popolo_banner_bg').css({'margin-top':'0px'});
		
	}
	
	
	
	
});





/*=======================TOP HEADER==========================*/




