<?php
$listid = "5ad0609b795407a5f7a9f6eadde0450d";
$api_key = "b787b627b42e19a007c7a3fde93a2d72";
$email = "developerrevo@gmail.com";
$name = "Adam James";
$ResubscribeOption = true;
$CustomFields[] = array('Key'=>"Custom Key", 'Value'=>"Custom Value");

require_once('csrest_subscribers.php');
$wrap = new CF7CM_CS_REST_Subscribers($listid, $api_key);
//print_r($wrap);
//die;
$result = $wrap->add(array(
    'EmailAddress' => $email,
    'Name' => $name,
    'CustomFields' => array(
        array(
            'Key' => 'Field 1 Key',
            'Value' => 'Field Value'
        )
    ),
    'Resubscribe' => false
));

echo "Result of POST /api/v3.1/subscribers/{list id}.{format}\n<br />";
if($result->was_successful()) {
    echo "Subscribed with code ".$result->http_status_code;
} else {
    echo 'Failed with code '.$result->http_status_code."\n<br /><pre>";
    var_dump($result->response);
    echo '</pre>';
}
?>