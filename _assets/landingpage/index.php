<?php
$display_message = "";
if(isset($_POST['sub']) && $_POST['sub']=='DO IT' )
{
	$email = $_POST['email'];
	$to = "info@ondi.com";
	$subject = 'Keep me in the loop';
	$message = '
	<html>
	<head>
	  <title>Keep me in the loop</title>
	</head>
	<body>
	  <p>Here is the email address of the visitor</p>
	  <p>'.$email.'</p>
	</body>
	</html>
	';

	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: ONDI <info@ondi.com>' . "\r\n";
	mail($to, $subject, $message, $headers);

	$display_message = "Thank you. You will receive an email when we are about to launch.";
}

?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale = 1.0, user-scalable = no">
<title>ONDI FOOD</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/reset.css" rel="stylesheet" type="text/css" />
<!--[if lte IE 6]><link rel="stylesheet" href="css/ie6.css" type="text/css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" href="css/ie7.css" type="text/css" /><![endif]-->
<!--[if IE 8]><link rel="stylesheet" href="css/ie8.css" type="text/css" /><![endif]-->
<!--[if IE 9]><link rel="stylesheet" href="css/ie9.css" type="text/css" /><![endif]-->
<!--[if lt IE 9]><script src="assets/js/html5.js"></script><![endif]-->
<!--[if lt IE 8]>
<div style=' clear: both; text-align:center; position: relative;'>
	<a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode"><img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." /></a>
</div>
<![endif]-->

<script type="text/javascript" src="assets/js/jquery.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<!--CUSTOM FUNCTION-->
<script type="text/javascript" src="assets/js/common.js"></script>

<!--FOR PARALAX SCROLLING-->
<!--<script type="text/javascript" src="assets/stellar/jquery.stellar.min.js"></script>
<script type="text/javascript" src="assets/stellar/waypoints.min.js"></script>
<script type="text/javascript" src="assets/stellar/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="assets/stellar/scripts.js"></script>-->

<link rel="stylesheet" href="assets/bgstretcher/css/bgstretcher.css" />

</head>

<body class="">
<div class="header_top" id="slide1" > 
    <div class="soon">
      <img src="../../images/soon.png">
    </div>
    <?php if($display_message != ''){?>
    	<div class="display_message" style="color:#FFFFFF;"><?=$display_message?></div>
    <?php } ?>
  <div class="nav_bg">
    <div class="nav_inner">
      <div class="header">
        <!--End of logo-->
        	<div class="keep">
				
            	<form name="landingform" method="post" action="" onsubmit="return checklandingform();" >
					<div class="left">
						<img src="../../images/keep.jpg">
					</div>
					<input name="email" id="email" type="text" class="mail_box">
					<input name="sub" type="submit" class="do_button" value="DO IT">
				</form>
            </div>
        	
        <!--End of nav--> 
      </div>
      <!--End of header--> 
      
    </div>
    
  </div>
  <!--End of nav_bg--> 
  
</div>



<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script> 
<script type="text/javascript" src="assets/bgstretcher/js/jquery-1.11.0.min.js"></script> 

<!--FOR HOME PAGE SLIDER--> 
<script type="text/javascript" src="assets/bgstretcher/js/jquery-bgstretcher-3.1.2.min.js"></script>


<!--FOR WHATS HOTS SECTION-->


<!--FOR DATE PICKER-->


<!--FOR LOGIN CHECKBOX-->


<script type="text/javascript" src="assets/radio/jquery.checkbox.min.js"></script> 

<!--FOR GENDER SLIDER-->

<script src="assets/gender/jquery-ui.js" type="text/javascript"></script> 

<!--Price Selector content--> 
<script type="text/javascript">

		
		$(document).ready(function() {
			$("body").bgStretcher({
				images: ["images/slide1.jpg"],
				slideShow: true,
				transitionEffect: "none",
				//sequenceMode:"random",
				//sliderCallbackFunc:"pause",
				//slideDirection: "E",
				imageWidth: 1500,
				imageHeight: 700
				//imageWidth: 2000,
				//imageHeight: 1000
				
			});
});
</script>



</body>
</html>
