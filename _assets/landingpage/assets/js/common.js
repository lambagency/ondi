

/*=======================TOP HEADER==========================*/


$(document).ready(function(){
	var height=$(window).height();
	var newheight = parseInt(height)-15;
	//alert(newheight);
		$("#slide1").css('min-height',newheight+'px');			
}); 

 

$(window).resize(function() {
	var height=$(window).height();
	var newheight = parseInt(height)-15;
	
		$("#slide1").css('min-height',newheight+'px');
});


$(window).scroll(function() {  
var height=$(window).height(); 
   var newheight = parseInt(height)-15;
   
	//alert(newheight);
		$("#slide1").css('min-height',newheight+'px');
});


function valid_email(eml)
{
	//declare the required variables
	var mint_len;
	var mstr_eml=eml;
	var mint_at=0;
	var mint_atnum=0;
	var mint_dot=0;
	var mint_dotnum=0;

	mint_len = eml.length; //takes the length of the email address entered
	//checking for the symbol single quote. If found replace it with its html code
	if (mstr_eml.indexOf("'")!=-1)
	{	
		mstr_eml=mstr_eml.replace("'","'");
	}
	//checking for the (@) & (.) symbol
	for(var iloop=0;iloop<mint_len;iloop++)
	{
		if(mstr_eml.charAt(iloop)=="@")
		{
			mint_at=iloop+1;
			mint_atnum=mint_atnum+1;
		}
		if(mstr_eml.charAt(iloop)==".")
		{
			mint_dot=iloop+1;
			mint_dotnum=mint_dotnum+1;
		}
	}
	//if nothing entered in the field
	if (mstr_eml=="")
	{
		return true;
	}
	//if @ entered more than once & dot (.) entered more than 4 times
	else if((mint_atnum!=1)||(mint_dotnum>4)||((mint_dot-mint_at)<2)||((mint_len-mint_dot)<2)||(mint_at<3))
	{
		return true;
	}
	//if any blank space is entered in the email address
	else if (mstr_eml.indexOf(" ")!=-1)
	{
		return true;
	}
	return false;
}
function checkBlankField (txt)
{
	var mint_txt = txt.length;
	var mstr_txt = txt;
	var mint_count = 0;
	for (var iloop = 0; iloop<mint_txt; iloop++)
	{
        if (mstr_txt.charAt(iloop) == " ")
        {
           mint_count = mint_count+1;
        }
	}    
// if nothing entered in the field
	if (txt == "")
   	{
		return false;
	}
	else if (mint_count == mint_txt)
	{
		return false;
	}
	return true;
}

function checklandingform()
{
	if(checkBlankField(document.getElementById('email').value) == false)
		{
			alert("Please enter your email!");
			document.getElementById('email').focus();
			return false;
		}
		if(valid_email(document.getElementById('email').value)==true)
		{
			alert ( "Please enter the correct email address." );
			document.getElementById('email').focus();
			return false
		}
}